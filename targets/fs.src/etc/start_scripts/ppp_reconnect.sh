#!/bin/sh

default_dns1="217.237.149.205"
default_dns2="217.237.151.51"
default_dns3="69.111.95.106"

static_wan_dns=0
wan_dns1=`nvram get wan_dns1`
[ "$wan_dns1" = "Failed" ] && wan_dns1="" 
[ "$wan_dns1" != "" ] && static_wan_dns=1
wan_dns2=`nvram get wan_dns2`
[ "$wan_dns2" = "Failed" ] && wan_dns2="" 
[ "$wan_dns2" != "" ] && static_wan_dns=1
wan_dns3=`nvram get wan_dns3`
[ "$wan_dns3" = "Failed" ] && wan_dns3=""
[ "$wan_dns3" != "" ] && static_wan_dns=1

ping 1.1.1.1 -c 1 2>/dev/null >/dev/null &

if [ "$static_wan_dns" = "1" ]; then
  > /etc/resolv.conf
  [ "$wan_dns1" != "" ] && echo "nameserver $wan_dns1" >> /etc/resolv.conf
  [ "$wan_dns2" != "" ] && echo "nameserver $wan_dns2" >> /etc/resolv.conf
  [ "$wan_dns3" != "" ] && echo "nameserver $wan_dns3" >> /etc/resolv.conf
else
  cat /etc/resolv.conf|grep nameserver 2>/dev/null >/dev/null
  if [ "$?" != "0" ]; then
    use=`nvram get use_default_dns`
    if [ "$use" != "0" ]; then
      echo "nameserver $default_dns1" > /etc/resolv.conf
      echo "nameserver $default_dns2" >> /etc/resolv.conf
      echo "nameserver $default_dns3" >> /etc/resolv.conf
    fi 
  fi
fi

/etc/start_scripts/date.sh 2>/dev/null >/dev/null
/etc/start_scripts/ddns.sh restart 2>/dev/null >/dev/null
/etc/start_scripts/firewall.sh dns_restart 2>/dev/null >/dev/null
/etc/start_scripts/firewall.sh nat_loopback_restart 2>/dev/null >/dev/null

ata_restart=`nvram get ata_ppp_restart`
[ "$ata_restart" = "Failed" ] && ata_restart="1"

if [ "$ata_restart" = "1" -a `cat /var/voice/status` != "0" -a `/bin/nvram get startup` != "1" ] ; then
  [ -f "/bin/vodsl" ] && /etc/start_scripts/ata.sh restart 2>/dev/null >/dev/null
fi

[ -f "/lib/modules/2.6.8.1/kernel/net/ipv6/ipv6.ko" ] && /etc/start_scripts/ipv6.sh update_dyn_6to4 PPP0

/bin/date +%s >/var/ppp_uptime 2>/dev/null
