#!/bin/sh

#WLAN configuration script by Patrick Schmidt, <coolman1982@users.sourceforge.net>

RET_STRING=""
KEY_STRING=""
CONNECT_STRING=""
WEP_DEFAULT="37373538393436333231353630"

#GENERAL PARAMS
#power management
wl_PM_def="0#2#0"
#antenna diversity
wl_antdiv_def="0#3#3"
#transmit antenna
wl_txant_def="0#3#3"
#transmit power
wl_pwr_percent1_def="0#100#100"

#ieee_80211 PARAMS
wl_rate_def="-1#54#-1"
#multicast data rate
wl_mrate_def="-1#54#-1"
#wlan channel to use
wl_channel_def="1#14#1"
wl_country_def="##ALL"
wl_shortslot_override_def="-1#1#-1"
wl_shortslot_restrict_def="0#1#0"
wl_gmode_def="0#3#1"
wl_gmode_protection_control_def="0#2#0"
wl_gmode_protection_cts_def="0#1#1"
wl_gmode_protection_override_def="-1#1#-1"
wl_scb_timeout_def="1#1440#60"
#0 = none,1 = non wlan,	2 = wlan manual,3 = wlan automatic
wl_interference_def="0#3#3"
wl_bi_def="10#65535#100"
wl_dtim_def="1#255#1"
wl_cwmin_def="1#255#31"
wl_cwmax_def="256#2047#1023"
wl_fragthresh_def="256#2346#2346"
wl_rtsthresh_def="0#2347#2347"
wl_frameburst_def="0#1#0"

#network and infrastructure settings
#infra or adhoc mode
wl_infra_def="0#1#1"
#wlan-ap or wlan-client
wl_ap_def="0#1#1"
#1=hide the network from active scans
wl_close_def="0#1#0"
#Bridging
wl_wet_def="0#1#1"

#security and ssid settings
#ssid to use
wl_essid_def="##LABOBOX"
#wpa rekeying intervall
wl_wpa_gtk_rekey_def="180#7200#3600"
#wlan crypto method
#known values are 0=none,1=wep,2=tkip,4=aes,6=tkip+aes,8=software
wl_wsec_def="0#8#1"
#wpa auth method
#known values are 1=disabled,2=wpa,4=wpa-psk,64=wpa2,66=wpa+wpa2,128=wpa2-psk,132=wpa-psk+wpa2-psk
wl_wpa_auth_def="1#132#0"
#wpa preshared key, only when wpa used
wl_wpa_psk_def="##whatever"
#radius-server ip, only when wpa or wpa2 used
wl_radius_ipaddr_def="##127.0.0.1"
#radius-server port, only when wpa or wpa2 used
wl_radius_port_def="1#65535#1812"
#radius-server shared secret, only when wpa or wpa2 used
wl_radius_key_def="##secure"
wl_wep_index_def="0#3#0"

#address settings, when wl0 is not in bridge
wl_ipaddr_def="##192.168.10.1"
wl_ipmask_def="##255.255.255.0"

wl_macmode_def="0#2#0"

wl_lazywds_def="0#1#1"
wl_wds_on_def="0#1#0"

wl_watch_def="0#1#1"
wl_watch_handler_def="##/usr/bin/wlan_watch.sh"
wl_watch_intervall_def="1#999#5"

exec_cmd()
{
  echo "$1" >>$DEBUG_FILE 2>>$DEBUG_FILE && /bin/sh -c "$1" >>$DEBUG_FILE 2>>$DEBUG_FILE
  return 0
}

isNumeric()
{
  [ $1 -eq 0 ] 2>/dev/null || [ $1 -eq 1 ] 2>/dev/null && return 0
  a="$1"
  let "a-=1" 2>/dev/null || return 1
  [ $a -eq -1 ] && return 1 || return 0
}


get_default_wep_key()
{
  KEY_STRING=$WEP_DEFAULT
  return 0
}

# --------------------------------------------------------- #
# check_wep_key()                                           #
# Checks given parameter for correct WEP-Key                #
# Parameters: $1=var_name, $2=value                         #
# Returns: 0 if correct ASCII-Key or correct HEX-Key        #
#          and maybe string is returned to var_name      #
#           , 1 if error occurs                             #
# --------------------------------------------------------- #
check_wep_key()
{
  len=${#1}
  #check for ASCII
  if [ $len -eq 5 ] || [ $len -eq 13 ]; then
    KEY_STRING=$1
    return 0
  #check if HEX
  elif [ $len -eq 10 ] || [ $len -eq 26 ]; then
    key=`echo $1|sed -e 's/[^0-9a-fA-F]//g'|tr '[a-z]' '[A-Z]'`
    #check len after substitution against old len
    [ ${#key} -eq $len ] && KEY_STRING=$key && return 0 || return 1
  #error
  else
    return 1
  fi
}

# --------------------------------------------------------- #
# check_value()                                             #
# Checks given var_name and var_value for correctness       #
# Parameters: $1=var_name, $2=var_value                     #
# Returns: 0 if OK or 1 if error occurs	                    #
#          and var_value in $RET_STRING                     #
# --------------------------------------------------------- #
check_value()
{
  [ "$1" = "" ] && return 1
  postfix="_def"
  h=`echo $1$postfix`
  h=`eval echo \\$$h`
  #check if value is empty and return std_value
  if [ "$2" = "" ] || [ "$2" = "Failed" ] 
  then
    RET_STRING=`echo $h|sed -e 's/.*#.*#\(.*\)/\1/g'`
    return 0
  fi
  min=`echo $h|sed -e 's/\(.*\)#.*#.*/\1/g'`
  max=`echo $h|sed -e 's/.*#\(.*\)#.*/\1/g'`
  std=`echo $h|sed -e 's/.*#.*#\(.*\)/\1/g'`
  RET_STRING=$2
  isNumeric "$2"
  #if value is numeric check for min-max range
#echo "val=$2 min=$min max=$max std=$std"
  if [ $? -eq 0 ];
  then
    isNumeric $min || isNumeric $max || return 0
    [ $min -le $2 ] 2>/dev/null && [ $max -ge $2 ] 2>/dev/null || RET_STRING=$std
    return 0
  else
    isNumeric $min || isNumeric $max && RET_STRING=$std
    return 0
  fi
}

# --------------------------------------------------------- #
# set_wlctl_value()                                         #
# gets given parameter from NVRAM and set it with wlctl     #
# Parameters: $1=parameter_name                             #
# Returns: 0 or 1 if error occurs 			    #
# --------------------------------------------------------- #
set_wlctl_value()
{
  [ "$1" = "" ] && return 1
  param=`echo $1|sed -e 's/^wl_//g'`
  var=`nvram get $1`;check_value $1 "$var" && exec_cmd "wlctl $param \"$RET_STRING\"" 2>/dev/null
  return 0
}

wpa_nr_to_string()
{
  eval wpanr="\$$1"
  wpa_string=""
  case "$wpanr" in
              2)
               wpa_string="wpa"
               ;;
              4)
               wpa_string="wpapsk"
               ;;
              32)
               wpa_string="wpa"
               ;;
              64)
               wpa_string="wpa2"
               ;;
              66)
               wpa_string="wpa2"
               ;;
              128)
               wpa_string="wpa2psk"
               ;;
              132)
               wpa_string="wpa2psk"
               ;;
              *)
               echo "wpa_nr_to_string() unknown WPA auth number $wpanr" >>$DEBUG_FILE
               wpa_string="wpanone"
               ;;
  esac
  
  eval $1=\"$wpa_string\" 2>/dev/null
}

#setup general parameters
setup_general()
{
  echo "setup_general()" >>$DEBUG_FILE
  set_wlctl_value "wl_country"
  var_list="wl_antdiv wl_txant wl_pwr_percent1"
  [ "$infra" = "0" ] || [ "$ap" = "0" ] && var_list=$var_list" wl_PM"
  for var in $var_list
  do
    set_wlctl_value $var
  done
}

#setup iee80211 parameters
setup_ieee80211()
{
  echo "setup_ieee80211()" >>$DEBUG_FILE
  
  var_list="wl_rate wl_mrate wl_channel wl_cwmin wl_cwmax wl_fragthresh wl_rtsthresh wl_shortslot_override"
  
  wl_gmode=`nvram get wl_gmode`;check_value "wl_gmode" $wl_gmode && wl_gmode=$RET_STRING
  exec_cmd "ifconfig wl0 down" 2>/dev/null
  exec_cmd "wlctl gmode $wl_gmode" 2>/dev/null
  if [ $wl_gmode -gt 0 ]; then
    echo "setup_ieee80211() detected G-mode or Mixed-mode...adding G-mode specific parameters" >>$DEBUG_FILE
    var_list=$var_list" wl_gmode_protection_override wl_gmode_protection_control wl_gmode_protection_cts wl_frameburst"
  fi
  if [ "$infra" = "1" ] && [ "$ap" = "1" ]; then
    echo "setup_ieee80211() detected AP-mode...adding AP-mode specific parameters" >>$DEBUG_FILE
    var_list=$var_list" wl_scb_timeout wl_bi wl_dtim wl_shortslot_restrict"
  fi
  for var in $var_list
  do
    set_wlctl_value $var
  done
}

setup_bridging()
{
  echo "setup_bridging()" >>$DEBUG_FILE
  
  #if bridging is enabled
  if [ "$bridge" = "1" ];
  then
    echo "bridging is enabled" >>$DEBUG_FILE
    #enable HW_Bridging
    exec_cmd "wlctl wet 1" 2>/dev/null
    ifconfig br0 >/dev/null 2>/dev/null || exec_cmd "brctl addbr br0"
    exec_cmd "brctl addif br0 wl0" 2>/dev/null
    exec_cmd "brctl setfd br0 5" 2>/dev/null
  else
    echo "bridging is disabled" >>$DEBUG_FILE
    exec_cmd "wlctl wet 0" 2>/dev/null
    exec_cmd "brctl delif br0 wl0" 2>/dev/null
  fi
    exec_cmd "ifconfig wl0 up" 2>/dev/null
    exec_cmd "ifconfig eth0 up" 2>/dev/null
  return 0
}

#setup infrastructure settings
setup_infra()
{
  echo "setup_infra()" >>$DEBUG_FILE

  #if adhoc_mode then force client mode and dont set ssid
  if [ "$infra" = "0" ]
  then
    echo "detected adhoc-mode" >>$DEBUG_FILE
    exec_cmd "wlctl up" 2>/dev/null
    exec_cmd "wlctl ap 0" 2>/dev/null
    exec_cmd "wlctl infra 0" 2>/dev/null
  #if infrastructure mode
  else
    echo "detected infrastructure-mode" >>$DEBUG_FILE
    #setup ap or client mode and ssid if ap
    exec_cmd "wlctl infra 1" 2>/dev/null
    if [ "$ap" = "1" ]; then 
      exec_cmd "wlctl up" 2>/dev/null
      set_wlctl_value "wl_essid"
      usleep 200000
      exec_cmd "wlctl ap 1" 2>/dev/null
      set_wlctl_value "wl_close"
    else
      exec_cmd "wlctl up" 2>/dev/null
      exec_cmd "wlctl ap 0" 2>/dev/null
    fi
  fi
  return 0
}

#setup security
setup_security()
{
  echo "setup_security()" >>$DEBUG_FILE
  
  [ "$infra" = "1" ] && imode="bss" || imode="ibss"
  #get auth mode
  wsec=`nvram get wl_wsec`;check_value "wl_wsec" $wsec && wsec=$RET_STRING
  #get essid
  essid=`nvram get wl_essid`;check_value "wl_essid" "$essid" && essid=`echo $RET_STRING|sed -e "s/./'&'/g"`
  #always set open system auth
  exec_cmd "wlctl auth 0" 2>/dev/null

  #no encryption
  if [ "$wsec" = "0" ]; then
    echo "no encryption is set" >>$DEBUG_FILE
    wepstatus=0
    wsec_restrict=0
    wpa_auth=0
    eap=0
    final_cmd=""
    [ "$ap" = "0" ] && final_cmd="wlctl join \"$essid\" imode $imode amode open" && CONNECT_STRING=$final_cmd
  #WEP-encryption
  elif [ "$wsec" = "1" ]; then
    echo "WEP-encryption is set" >>$DEBUG_FILE
    wepstatus=1
    wsec_restrict=1
    wpa_auth=0
    eap=0
    final_cmd=""
    if [ "$infra" = "0" ] || [ "$ap" = "0" ]; then
      index=`nvram get wl_wep_index`;check_value "wl_wep_index" $index && index=$RET_STRING
      key=`nvram get wl_wep$index`
      check_wep_key $key || get_default_wep_key || return 1
      #if key is in hex then put every character in single quotes to avoid problems
      KEY_STRING=`echo $KEY_STRING|sed -e "s/./'&'/g"`
      final_cmd="wlctl join \"$essid\" key $KEY_STRING imode $imode amode shared"
      CONNECT_STRING=$final_cmd
    else
      i=0
      j=0
      f=0
      while [ $i -lt 4 ]
      do
        exec_cmd "wlctl rmwep $i" 2>/dev/null
        key=`nvram get wl_wep$i`
        check_wep_key $key
	if [ $? -ne 0 ]; then
	  let "f+=1"
	  let "i+=1"
	  continue
	fi
        KEY_STRING=`echo $KEY_STRING|sed -e "s/./'&'/g"`
        exec_cmd "wlctl addwep $j $KEY_STRING" 2>/dev/null
        let "i+=1"
        let "j+=1"
      done
      if [ $f -eq 4 ]; then
        echo "ERROR: wep encryption is set but no wep keys found...setting default key" >>$DEBUG_FILE
        get_default_wep_key
        KEY_STRING=`echo $KEY_STRING|sed -e "s/./'&'/g"`
        exec_cmd "wlctl addwep 0 $KEY_STRING" 2>/dev/null
      fi
        
      index=`nvram get wl_wep_index`;check_value "wl_wep_index" $index && index=$RET_STRING
      if [ $index -gt $j ]; then
        echo "WARNING: wep_key_index is set to $index but only $j valid keys found, setting index to 0" >>$DEBUG_FILE
        index=0
      fi
      exec_cmd "wlctl primary_key $index" 2>/dev/null
    fi
  #if adhoc is enabled, error at this position
  elif [ "$infra" = "0" ]; then
    echo "ERROR: WPA(2)/PSK(2)-authentication is set in adhoc-mode, setting no encryption" >>$DEBUG_FILE
    wepstatus=0
    wsec_restrict=0
    wpa_auth=0
    eap=0
    final_cmd="wlctl join \"$essid\" imode $imode amode open"
    CONNECT_STRING=$final_cmd
  else
    echo "WPA(2)/PSK(2)-authentication is set" >>$DEBUG_FILE
    use_radius=0
    wpa_auth=`nvram get wl_wpa_auth`;check_value "wl_wpa_auth" "$wpa_auth" && wpa_auth=$RET_STRING
    [ "$wpa_auth" = "2" ] || [ "$wpa_auth" = "64" ] || [ "$wpa_auth" = "66" ] && use_radius=1 
    laniface=""
    [ "$bridge" = "1" ] && laniface="-l br0 "
    radius=""
    key=""
    if [ "$use_radius" = "1" ]; then
      key=`nvram get wl_radius_key`;check_value "wl_radius_key" "$key" && key=`echo "$RET_STRING"|sed -e "s/./'&'/g"`
      key="-r $key"
      ip=`nvram get wl_radius_ipaddr`;check_value "wl_radius_ipaddr" $ip && ip=$RET_STRING
      port=`nvram get wl_radius_port`;check_value "wl_radius_port" $port && port=$RET_STRING
      radius="-h $ip -p $port -t 36000"
    else
      key=`nvram get wl_wpa_psk`;check_value "wl_wpa_psk" "$key" && key=`echo "$RET_STRING"|sed -e "s/./'&'/g"`
      key="-k $key"
    fi
    role="-A"
    if [ "$ap" = "0" ]; then
      role="-S"
      laniface=""
      exec_cmd "ebtables -t broute -F" 2>/dev/null
      if [ "$bridge" = "1" ]; then
        exec_cmd "ebtables -t broute -A BROUTING -p arp -j ACCEPT" 2>/dev/null
        exec_cmd "ebtables -t broute -A BROUTING -p ipv4 -j ACCEPT" 2>/dev/null
        exec_cmd "ebtables -t broute -A BROUTING -p ppp_disc -j ACCEPT" 2>/dev/null
        exec_cmd "ebtables -t broute -A BROUTING -p ppp_ses -j ACCEPT" 2>/dev/null
        exec_cmd "ebtables -t broute -A BROUTING -p atalk -j ACCEPT" 2>/dev/null
        exec_cmd "ebtables -t broute -A BROUTING -p aarp -j ACCEPT" 2>/dev/null
        exec_cmd "ebtables -t broute -A BROUTING -p ipx -j ACCEPT" 2>/dev/null
        exec_cmd "ebtables -t broute -A BROUTING -p netbeui -j ACCEPT" 2>/dev/null
        exec_cmd "ebtables -t broute -A BROUTING -j DROP" 2>/dev/null
      fi
      wpa_string="$wpa_auth"
      wpa_nr_to_string "wpa_string"
      CONNECT_STRING="wlctl join \"$essid\" imode $imode amode $wpa_string"
    fi
    rekey=`nvram get wl_wpa_gtk_rekey`;check_value "wl_wpa_gtk_rekey" $rekey && rekey=$RET_STRING
    
    wepstatus=0
    wsec_restrict=1
    eap=1
    final_cmd="nas -H 34954 -P /var/nas.lan.pid $laniface-i wl0 $role -m $wpa_auth $key -s "$essid" -w $wsec -g $rekey $radius &"
  fi
  
  exec_cmd "wlctl wepstatus $wepstatus" 2>/dev/null
  exec_cmd "wlctl wsec $wsec" 2>/dev/null
  exec_cmd "wlctl wsec_restrict $wsec_restrict" 2>/dev/null
  exec_cmd "wlctl wpa_auth $wpa_auth" 2>/dev/null
  exec_cmd "wlctl eap $eap" 2>/dev/null
  exec_cmd "$final_cmd" 2>/dev/null
  #sleep when nas should be started and wait some seconds and check if nas is really running
  if [ $wsec -gt 1 ]; then
    echo "checking if nas is really running..." >>$DEBUG_FILE
    sleep 1
    pidof nas >/dev/null 2>/dev/null
    while [ $? -ne 0 ]; do
      echo "ERROR: nas isn't running...trying to restart" >>$DEBUG_FILE
      usleep 500000
      pidof nas >/dev/null 2>/dev/null || exec_cmd "$final_cmd" 2>/dev/null
      sleep 1
      pidof nas >/dev/null 2>/dev/null
    done
  fi
  return 0
}

setup_ipaddr()
{
  echo "setup_ipaddr()" >>$DEBUG_FILE
  
  if [ "$bridge" = "0" ];
  then
    ip=`nvram get wl_ipaddr`;check_value "wl_ipaddr" $ip && ip=$RET_STRING
    if [ "$ip" = "dhcp" ]; then
      kill -9 `cat /var/run/udhcp_wlan.pid 2>/dev/null` >/dev/null 2>/dev/null
      mkdir -p /var/run
      usleep 500000
      exec_cmd "udhcpc -i wl0 -b -p /var/run/udhcp_wlan.pid -s /etc/start_scripts/udhcpc_set.sh" 2>/dev/null
    else
      mask=`nvram get wl_ipmask`;check_value "wl_ipmask" $mask && mask=$RET_STRING
      exec_cmd "ifconfig wl0 $ip netmask $mask up" 2>/dev/null
      gw=`nvram get wl_gw` && [ "$gw" != "" ] && exec_cmd "route add default gw $gw" 2>/dev/null
    fi
  fi
  
  return 0
}

setup_macfilter()
{
  echo "setup_macfilter()" >>$DEBUG_FILE

  if [ "$infra" = "1" ] && [ "$ap" = "1" ]; then
    set_wlctl_value "wl_macmode"
    mac_list=`nvram get wl_mac`
    [ "$mac_list" = "Failed" ] && mac_list="none"
    exec_cmd "wlctl mac $mac_list"
  else
    echo "setup_macfilter() ignoring mac_filter because no AP-mode detected" >>$DEBUG_FILE
    exec_cmd "wlctl macmode 0"
    exec_cmd "wlctl mac none"
  fi
  return 0
}

setup_wds()
{
  echo "setup_wds()" >>$DEBUG_FILE
  
  if [ "$infra" = "1" ] && [ "$ap" = "1" ]; then
    wds_on=`nvram get wl_wds_on`;check_value "wl_wds_on" $wds_on && wds_on=$RET_STRING
    if [ "$wds_on" = "1" ]; then
      set_wlctl_value "wl_lazywds"
      mac_list=`nvram get wl_wds`
      [ "$mac_list" = "Failed" ] && mac_list="none"
      exec_cmd "wlctl wds $mac_list"
    else
      exec_cmd "wlctl wds none"
    fi
  else
    echo "setup_wds() ignoring WDS setting because no AP-mode detected" >>$DEBUG_FILE
  fi
  return 0
}

#new since 0.3.7
start_watch()
{
  echo "start_watch()" >>$DEBUG_FILE
  if [ "$ap" = "1" ]; then
    echo "no need to start watch script because in AP mode" >>$DEBUG_FILE
    return 0
  else
    watch_start=`nvram get wl_watch`;check_value "wl_watch" $watch_start && watch_start=$RET_STRING
    if [ "$watch_start" = "1" ]; then
      watch_script=`nvram get wl_watch_handler`;check_value "wl_watch_handler" $watch_script && watch_script=$RET_STRING
      watch_intervall=`nvram get wl_watch_intervall`;check_value "wl_watch_intervall" $watch_intervall && watch_intervall=$RET_STRING
      
      CONNECT_STRING="'$CONNECT_STRING'"
      exec_cmd "$watch_script $CONNECT_STRING $watch_intervall &"
    fi
  fi
}

#new since 0.3.7
stop_watch()
{
  echo "stop_watch()" >>$DEBUG_FILE
  watch_script=`nvram get wl_watch_handler`;check_value "wl_watch_handler" $watch_script && watch_script=$RET_STRING
  watch_script=`echo $watch_script|sed -e 's/.*\///g'`
  exec_cmd "killall -9 $watch_script"
}

##########MAIN FUNCTION#########


#setting up debugging
dbg=`nvram get wl_debug`
if [ "$dbg" = "1" ]; then
  DEBUG_FILE=`nvram get wl_debug_file`
  [ "$DEBUG_FILE" = "Failed" ] && DEBUG_FILE="/var/wlan.log"
elif [ "$DEBUG" = "1" ]; then 
  [ "$DEBUG_FILE" != "" ] && DEBUG_FILE="/var/wlan.log"
else
  DEBUG_FILE="/dev/null"
fi

/etc/start_scripts/wlan_present.sh
if [ $? -ne 0 ]; then
  echo "WLAN hardware is not present!"
  exit 0;
fi

case "$1" in
	start)
		echo >$DEBUG_FILE
	        echo "Starting WLAN"
	        start=`nvram get wl_up`
                #unload all wlan dependent things to save power and reduce thermal
	        if [ "$start" = "0" ]; then 
                  exec_cmd "wlctl down"
	          exec_cmd "killall -9 nas"
	          exec_cmd "rmmod wl"
                  exit 0
                fi
		is_loaded=`lsmod|grep wl`
		[ "$is_loaded" = "" ] && exec_cmd "insmod /lib/modules/2.6.8.1/extra/wl.ko"

	        exec_cmd "wlctl down"
	        exec_cmd "wlctl clk 1"
	        
                #first getting some parameters which are needed by almost all subroutines
	        infra=`nvram get wl_infra`;check_value "wl_infra" $infra && infra=$RET_STRING
	        ap=`nvram get wl_ap`;check_value "wl_ap" $ap && ap=$RET_STRING
	        bridge=`nvram get wl_wet`;check_value "wl_wet" $bridge && bridge=$RET_STRING
	        
	        setup_general
                setup_infra
                setup_ieee80211
                setup_bridging
                setup_security
                usleep 500000
                setup_infra
                setup_macfilter
                setup_wds
                usleep 500000
                setup_ipaddr
                start_watch
		;;
	stop)
		echo >$DEBUG_FILE
	        echo "Stopping WLAN"
	        exec_cmd "wlctl down"
	        exec_cmd "brctl delif br0 wl0"
	        exec_cmd "ifconfig wl0 down"
	        exec_cmd "killall -9 nas"
	        kill -9 `cat /var/run/udhcp_wlan.pid 2>/dev/null` >/dev/null 2>/dev/null
		echo "rmmod wl &" >>$DEBUG_FILE 2>>$DEBUG_FILE
	        rmmod wl >/dev/null 2>/dev/null &
		sleep 1
		;;
	restart)
		echo >$DEBUG_FILE
		exec_cmd "wlctl down"
		exec_cmd "wlctl disassoc"
                exec_cmd "brctl delif br0 wl0"
                exec_cmd "ifconfig wl0 down"
                exec_cmd "killall -9 nas"
                kill -9 `cat /var/run/udhcp_wlan.pid 2>/dev/null` >/dev/null 2>/dev/null
                stop_watch
		$0 start
		;;
	*)
		echo "Usage: wlan.sh {start|stop|restart}"
		exit 1
		;;
esac

exit 0

