#!/bin/sh
#
# Loads and executes custom script from nvram
#
#

case "$1" in
	start)
	        echo "Loading custom script"
		custom_start=`/bin/nvram get custom_start`
                [ "$custom_start" = "Failed" ] || [ "$custom_start" != "1" ] && exit 0
		  
                /bin/nvram getfile custom_script=/var/custom_script.sh >/dev/null && /bin/sh /var/custom_script.sh || echo "ERROR: no custom_scipt in NVRAM"
		;;
	stop)
	        echo "Custom script cant be stopped"
		;;
	restart)
		$0 stop
		$0 start
		;;
	*)
		echo "Usage: /etc/start_scripts/custom.sh {start|stop|restart}"
		exit 1
		;;
esac

exit 0

