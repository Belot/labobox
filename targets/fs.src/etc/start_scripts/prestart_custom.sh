#!/bin/sh
#
# Loads and executes prestart custom script from nvram
#
#

case "$1" in
	start)
	        echo "Loading prestart custom script"
		custom_start=`/bin/nvram get prestart_custom_start`
                [ "$custom_start" = "Failed" ] || [ "$custom_start" != "1" ] && exit 0
		  
                /bin/nvram getfile prestart_custom_script=/var/prestart_custom_script.sh >/dev/null && /bin/sh /var/prestart_custom_script.sh $2 || echo "ERROR: no prestart_custom_scipt in NVRAM"
		;;
	stop)
	        echo "Custom script cant be stopped"
		;;
	restart)
		$0 stop
		$0 start
		;;
	*)
		echo "Usage: /etc/start_scripts/prestart_custom.sh {start|stop|restart}"
		exit 1
		;;
esac

exit 0

