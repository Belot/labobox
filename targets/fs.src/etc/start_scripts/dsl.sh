#!/bin/sh

atm_vpi=`nvram get atm_vpi`
[ "$atm_vpi" = "Failed" ] && atm_vpi=8
atm_vci=`nvram get atm_vci`
[ "$atm_vci" = "Failed" ] && atm_vci=35

atm_sec=`nvram get atm_sec`
[ "$atm_sec" = "Failed" ] && atm_sec=0
atm_vpi_sec=`nvram get atm_vpi_sec`
[ "$atm_vpi_sec" = "Failed" ] && atm_vpi_sec=1
atm_vci_sec=`nvram get atm_vci_sec`
[ "$atm_vci_sec" = "Failed" ] && atm_vci_sec=34

dsl_trellis=`nvram get dsl_trellis`
[ "$dsl_trellis" = "Failed" ] && dsl_trellis="1"
[ "$dsl_trellis" = "1" ] && dsl_trellis="on" || dsl_trellis="off"

dsl_bitswap=`nvram get dsl_bitswap`
[ "$dsl_bitswap" = "Failed" ] && dsl_bitswap="1"
[ "$dsl_bitswap" = "1" ] && dsl_bitswap="on" || dsl_bitswap="off"

dsl_sra=`nvram get dsl_sra`
[ "$dsl_sra" = "Failed" ] && dsl_sra="0"
[ "$dsl_sra" = "1" ] && dsl_sra="on" || dsl_sra="off"

dsl_start=`nvram get dsl_start`
[ "$dsl_start" = "Failed" ] && dsl_start=0

dsl_mod=`nvram get dsl_mod`
[ "$dsl_mod" = "Failed" ] && dsl_mod="a"
[ "$dsl_mod" != "a" -a "$dsl_mod" != "d" -a "$dsl_mod" != "l" -a "$dsl_mod" != "t" -a "$dsl_mod" != "2" -a "$dsl_mod" != "p" -a "$dsl_mod" != "2m" -a "$dsl_mod" != "pm" ] && dsl_mod="a"

dsl_lpair=`nvram get dsl_lpair`
[ "$dsl_lpair" = "Failed" ] && dsl_lpair="i"
[ "$dsl_lpair" != "i" -a "$dsl_lpair" != "o" ] && dsl_lpair="i"

dsl_user=`nvram get dsl_user`
[ "$dsl_user" = "Failed" ] && dsl_user="user"

dsl_pass=`nvram get dsl_pass`
[ "$dsl_pass" = "Failed" ] && dsl_pass="pass"

dsl_mtu=`nvram get dsl_mtu`
[ "$dsl_mtu" = "Failed" ] && dsl_mtu=1492

dsl_snr=`nvram get dsl_snr`
[ "$dsl_snr" = "Failed" ] && dsl_snr=100

dsl_static_ip=`nvram get dsl_static_ip`
[ "$dsl_static_ip" = "Failed" ] && dsl_static_ip=''

ppp_auth=`nvram get ppp_auth`
[ "$ppp_auth" = "Failed" ] && ppp_auth=0

dsl_autoconnect=`nvram get dsl_autoconnect`
[ "$dsl_autoconnect" = "Failed" ] && dsl_autoconnect=1

dsl_persistent_conn=`nvram get dsl_persistent_conn`
[ "$dsl_persistent_conn" = "Failed" ] && dsl_persistent_conn=1

dsl_idle_timeout=`nvram get dsl_idle_timeout`
[ "$dsl_idle_timeout" = "Failed" ] && dsl_idle_timeout=300

aal=`nvram get atm_aal`
[ "$aal" = "Failed" ] && aal="aal5"

aal_sec=`nvram get atm_aal_sec`
[ "$aal_sec" = "Failed" ] && aal_sec="aal5"

atm_vcc="0.$atm_vpi.$atm_vci"
pvc_vcc="0.$atm_vpi.$atm_vci"
nas_vcc=$atm_vpi"_"$atm_vci
pppdc="$atm_vpi.$atm_vci.1"

atm_vcc_sec="0.$atm_vpi_sec.$atm_vci_sec"
pvc_vcc_sec="0.$atm_vpi_sec.$atm_vci_sec"
nas_vcc_sec=$atm_vpi_sec"_"$atm_vci_sec
pppdc_sec="$atm_vpi_sec.$atm_vci_sec.1"

ppp_start=`nvram get ppp_start`
[ "$ppp_start" = "Failed" ] && ppp_start=1

ppp_device=`nvram get ppp_device`
[ "$ppp_device" = "Failed" ] && ppp_device="dsl"

if [ "$ppp_device" = "dsl" ]; then
  ppp_dev=nas_$nas_vcc
elif [ "$ppp_device" = "lan" ]; then
  ppp_dev="eth0"
elif [ "$ppp_device" = "wlan" ]; then
  ppp_dev="wl0"
else
  ppp_dev=$ppp_device
fi


ppp_start_sec=`nvram get ppp_start_sec`
[ "$ppp_start_sec" = "Failed" ] && ppp_start_sec=0

ppp_device_sec=`nvram get ppp_device_sec`
[ "$ppp_device_sec" = "Failed" ] && ppp_device_sec="dsl"

if [ "$ppp_device_sec" = "dsl" ]; then
  ppp_dev_sec=nas_$nas_vcc_sec
elif [ "$ppp_device_sec" = "lan" ]; then
  ppp_dev_sec="eth0"
elif [ "$ppp_device_sec" = "wlan" ]; then
  ppp_dev_sec="wl0"
else
  ppp_dev_sec=$ppp_device_sec
fi

[ "$ppp_dev_sec" = "$ppp_dev" ] && echo "ignoring secondary ppp-connection because primary and secondary device are equal" && ppp_start_sec=0

# Enable AUTOSCAN encapsulation (LLC or VC-MUX)
#encap="llcencaps"
#encap="vcmux_routed"
#encap="vcmux_bridged8023"
encap="unknown"

ppp_start()
{
  if [ "$ppp_start" = "1" ] && ( [ "$ppp_device" != "dsl" ] || ( [ "$ppp_device" = "dsl" ] && [ "$dsl_start" = "1" ] ) ); then
    # static IP
    IP=""
    [ "$dsl_static_ip" != "" ] && IP="-q $dsl_static_ip"
    # Server name
    Server=""
    [ "$dsl_server_name" != "" ] && Server="-r $dsl_server_name"
    # Service name
    Service=""
  #  [ "$dsl_service_name" != "" ] && Service="-S $dsl_service_name"
  
    # permanent connection
    Connection=""
    if [ "$dsl_persistent_conn" = "1" ]; then
      Connection="-P 1"
    else
      # auto connect
      Autoconn="-k 0"
      [ "$dsl_autoconnect" = "1" ] && Autoconn="-k 1"
      # idle timeout
      Timeout="-o $dsl_idle_timeout -P 0"
  
      Connection="$Autoconn $Timeout"
    fi
  
    # Password
    Password=""
    [ "$dsl_pass" != "" ] && Password="-p $dsl_pass"
  
    # session info
    sessinfo=`cat /proc/var/fyi/wan/ppp_${nas_vcc}_1/sessinfo`
    [ "$sessinfo" != "" ] && sessinfo="-m "$sessinfo
  
  
    mkdir -p /var/fyi/sys
    rm -f /var/fyi/sys/dns

    #new since 0.3.8, always create standard ppp-Device because
    #vodsl sometimes have problems with other VPI/VCI values
    bcm_helper -s ppp_1_32_1
    
    bcm_helper -s ppp_${nas_vcc}_1
    (echo 1 > /proc/var/fyi/wan/ppp_${nas_vcc}_1/wanup) >/dev/null 2>/dev/null
    (echo 6 > /proc/var/fyi/wan/ppp_${nas_vcc}_1/status) >/dev/null 2>/dev/null
    
    #echo "starting ppp"
    pppd -c $pppdc  -i $ppp_dev -u $dsl_user $Password $Server -f $ppp_auth $IP $Connection $Service -M $dsl_mtu $sessinfo >/dev/null 2>/dev/null &
    if [ "$dsl_persistent_conn" = "1" ]; then
      ppp_watch -c ppp_${nas_vcc}_1 -s /etc/start_scripts/ppp_reconnect.sh &
    else
      ppp_watch -c ppp_${nas_vcc}_1 -t -s /etc/start_scripts/ppp_reconnect.sh &
      (echo 3 > /proc/var/fyi/wan/ppp_${nas_vcc}_1/daemonstatus) >/dev/null 2>/dev/null
    fi
  fi
}

ppp_start2()
{
  if [ "$ppp_start_sec" = "1" ] && ( [ "$ppp_device_sec" != "dsl" ] || ( [ "$ppp_device_sec" = "dsl" ] && [ "$dsl_start" = "1" ] ) ); then

  dsl_user_sec=`nvram get dsl_user_sec`
  [ "$dsl_user_sec" = "Failed" ] && dsl_user_sec="user"

  dsl_pass_sec=`nvram get dsl_pass_sec`
  [ "$dsl_pass_sec" = "Failed" ] && dsl_pass_sec="pass"

  dsl_mtu_sec=`nvram get dsl_mtu_sec`
  [ "$dsl_mtu_sec" = "Failed" ] && dsl_mtu_sec=1492

  dsl_static_ip_sec=`nvram get dsl_static_ip_sec`
  [ "$dsl_static_ip_sec" = "Failed" ] && dsl_static_ip_sec=''

  ppp_auth_sec=`nvram get ppp_auth_sec`
  [ "$ppp_auth_sec" = "Failed" ] && ppp_auth_sec=0

  dsl_autoconnect_sec=`nvram get dsl_autoconnect_sec`
  [ "$dsl_autoconnect_sec" = "Failed" ] && dsl_autoconnect_sec=1

  dsl_persistent_conn_sec=`nvram get dsl_persistent_conn_sec`
  [ "$dsl_persistent_conn_sec" = "Failed" ] && dsl_persistent_conn_sec=1

  dsl_idle_timeout_sec=`nvram get dsl_idle_timeout_sec`
  [ "$dsl_idle_timeout_sec" = "Failed" ] && dsl_idle_timeout_sec=300


    # static IP
    IP=""
    [ "$dsl_static_ip_sec" != "" ] && IP="-q $dsl_static_ip_sec"
    # Server name
    Server=""
    [ "$dsl_server_name_sec" != "" ] && Server="-r $dsl_server_name_sec"
    # Service name
    Service=""
  #  [ "$dsl_service_name" != "" ] && Service="-S $dsl_service_name"
  
    # permanent connection
    Connection=""
    if [ "$dsl_persistent_conn_sec" = "1" ]; then
      Connection="-P 1"
    else
      # auto connect
      Autoconn="-k 0"
      [ "$dsl_autoconnect" = "1" ] && Autoconn="-k 1"
      # idle timeout
      Timeout="-o $dsl_idle_timeout -P 0"
  
      Connection="$Autoconn $Timeout"
    fi
  
    # Password
    Password=""
    [ "$dsl_pass_sec" != "" ] && Password="-p $dsl_pass_sec"
  
    # session info
    sessinfo=`cat /proc/var/fyi/wan/ppp_${nas_vcc_sec}_1/sessinfo`
    [ "$sessinfo" != "" ] && sessinfo="-m "$sessinfo
  
  
    mkdir -p /var/fyi/sys
    rm -f /var/fyi/sys/dns

    bcm_helper -t ppp_${nas_vcc_sec}_1
    (echo 1 > /proc/var/fyi/wan/ppp_${nas_vcc_sec}_1/wanup) >/dev/null 2>/dev/null
    (echo 6 > /proc/var/fyi/wan/ppp_${nas_vcc_sec}_1/status) >/dev/null 2>/dev/null
    
    #echo "starting ppp"
    pppd -c $pppdc_sec  -i $ppp_dev_sec -u $dsl_user_sec $Password $Server -f $ppp_auth_sec $IP $Connection $Service -M $dsl_mtu_sec $sessinfo >/dev/null 2>/dev/null &
    if [ "$dsl_persistent_conn_sec" != "1" ]; then
      (echo 3 > /proc/var/fyi/wan/ppp_${nas_vcc_sec}_1/daemonstatus) >/dev/null 2>/dev/null
    fi
  fi
}


start()
{
  if [ "$dsl_start" = "1" ]; then
    KERNELVER=`uname -r`
    lsmod|grep atmapi >/dev/null 2>/dev/null
    if [ "$?" -ne 0 ]; then
      insmod /lib/modules/$KERNELVER/extra/atmapi.ko
      insmod /lib/modules/$KERNELVER/extra/blaadd.ko
      cd /var

      annex=`/bin/nvram get dsl_annex`
      if [ "$annex" = "Failed" ]; then
	[ -f /etc/adsl/adsl_phy_B.bin ] && annex="B" || annex="A"
      fi

      if [ "$annex" = "A" ]; then
        if [ -f /etc/adsl/adsl_phy_A.bin ]; then
          ln -s /etc/adsl/adsl_phy_A.bin adsl_phy.bin 
        else
          echo "dsl.sh: ERROR...ANNEX_A configured but image doesn't support this"
	  ln -s /etc/adsl/adsl_phy_B.bin adsl_phy.bin
        fi
      else
        if [ -f /etc/adsl/adsl_phy_B.bin ]; then 
          ln -s /etc/adsl/adsl_phy_B.bin adsl_phy.bin
        else
          echo "dsl.sh: ERROR...ANNEX_B configured but image doesn't support this"
          ln -s /etc/adsl/adsl_phy_A.bin adsl_phy.bin
        fi
      fi

      insmod /lib/modules/$KERNELVER/extra/adsldd.ko
    fi
  
    # ADSL init
    adslctl start --mod $dsl_mod --bitswap $dsl_bitswap --snr $dsl_snr --sra $dsl_sra --lpair $dsl_lpair --trellis $dsl_trellis
  
    # ATM init
    atmctl start --pqs 150
    atmctl operate intf --state 0 enable
  
    atmctl operate vcc --add $atm_vcc $aal 1 $encap --addq $atm_vcc 64 1 --addq $atm_vcc 64 2 --addq $atm_vcc 64 3
  
    pvc2684d &
    sleep 2
    pvc2684ctl -a -v $pvc_vcc  -f &
    sleep 2
    base_mac=`ifconfig eth0|grep HWaddr|awk '{print $5;}'`
    mac_6=`echo $base_mac|sed -e 's/:/ /g'|awk '{print $6}'`
    mac_5=`echo $base_mac|sed -e 's/:/ /g'|awk '{print $5}'`
    mac_4=`echo $base_mac|sed -e 's/:/ /g'|awk '{print $4}'`
    mac_3=`echo $base_mac|sed -e 's/:/ /g'|awk '{print $3}'`
    mac_2=`echo $base_mac|sed -e 's/:/ /g'|awk '{print $2}'`
    mac_1=`echo $base_mac|sed -e 's/:/ /g'|awk '{print $1}'`
    let h=0x$mac_4$mac_5$mac_6
    let h=$h+2
    nas_mac=`printf "%06X" $h|sed -e 's/\(..\)\(..\)\(..\)/\1:\2:\3/'`
    nas_mac=`echo $mac_1:$mac_2:$mac_3:$nas_mac`
    ifconfig nas_$nas_vcc hw ether $nas_mac
    ifconfig nas_$nas_vcc up
    brctl addif br0 nas_$nas_vcc

    #secondardy ATM connection
    if [ "$atm_sec" = "1" ]; then
      atmctl operate vcc --add $atm_vcc_sec $aal_sec 1 $encap --addq $atm_vcc_sec 64 1 --addq $atm_vcc_sec 64 2 --addq $atm_vcc_sec 64 3
      
      #if we use ppp on secondary ATM-connection then start pvc with filtering option '-f'
      if [ "$ppp_device_sec" = "dsl" ]; then
	pvc2684ctl -a -v $pvc_vcc_sec  -f &
      else
	pvc2684ctl -a -v $pvc_vcc_sec &
      fi

      sleep 2
      let h=0x$mac_4$mac_5$mac_6
      let h=$h+3
      nas_mac=`printf "%06X" $h|sed -e 's/\(..\)\(..\)\(..\)/\1:\2:\3/'`
      nas_mac=`echo $mac_1:$mac_2:$mac_3:$nas_mac`
      ifconfig nas_$nas_vcc_sec hw ether $nas_mac
      ifconfig nas_$nas_vcc_sec up
      brctl addif br0 nas_$nas_vcc_sec
    fi
  fi

  dsl=0
  if [ "$ppp_start" = "1" ] && ( [ "$ppp_device" != "dsl" ] || ( [ "$ppp_device" = "dsl" ] && [ "$dsl_start" = "1" ] ) ); then
    #new since 0.3.8, always create standard ppp-Device because
    #vodsl sometimes have problems with other VPI/VCI values
    bcm_helper -s ppp_1_32_1

    bcm_helper -s ppp_${nas_vcc}_1
    (echo 1 > /proc/var/fyi/wan/ppp_${nas_vcc}_1/wanup) >/dev/null 2>/dev/null
    (echo 6 > /proc/var/fyi/wan/ppp_${nas_vcc}_1/status) >/dev/null 2>/dev/null

    [ "$dsl_start" = "1" ] && adslctl connection --up && dsl=1
    ppp_start
  else
    [ "$dsl_start" = "1" ] && adslctl connection --up && dsl=1
  fi

  if [ "$ppp_start_sec" = "1" ] && ( [ "$ppp_device_sec" != "dsl" ] || ( [ "$ppp_device_sec" = "dsl" ] && [ "$dsl_start" = "1" ] ) ); then

    bcm_helper -t ppp_${nas_vcc_sec}_1
    (echo 1 > /proc/var/fyi/wan/ppp_${nas_vcc_sec}_1/wanup) >/dev/null 2>/dev/null
    (echo 6 > /proc/var/fyi/wan/ppp_${nas_vcc_sec}_1/status) >/dev/null 2>/dev/null

    [ $dsl -eq 0 ] && adslctl connection --up
    ppp_start2
  else
    [ $dsl -eq 0 ] && adslctl connection --up
  fi
}

stop()
{
  if [ "$ppp_device" = "dsl" ]; then
    killall -9 pppd >/dev/null 2>/dev/null
    killall -9 ppp_watch >/dev/null 2>/dev/null
    ledtool 3 0
    (echo 3 > /proc/var/fyi/wan/ppp_${nas_vcc}_1/daemonstatus) >/dev/null 2>/dev/null
  fi

  if [ "$dsl_start" = "1" ]; then
    atmctl stop >/dev/null 2>/dev/null
    adslctl connection --down >/dev/null 2>/dev/null
    adslctl stop >/dev/null 2>/dev/null
    brctl delif br0 nas_$nas_vcc >/dev/null 2>/dev/null
    killall -9 pvc2684d >/dev/null 2>/dev/null
  fi
  ledtool 2 0
}

ppp_restart()
{
  killall -9 pppd >/dev/null 2>/dev/null
  killall -9 ppp_watch >/dev/null 2>/dev/null
  (echo 3 > /proc/var/fyi/wan/ppp_${nas_vcc}_1/daemonstatus) >/dev/null 2>/dev/null
  (echo 3 > /proc/var/fyi/wan/ppp_${nas_vcc_sec}_1/daemonstatus) >/dev/null 2>/dev/null
  ledtool 3 0
  delay=1
  [ "$1" != "" ] && delay=$1
  sleep $delay
  ppp_start
  ppp_start2
}

ppp_reconnect()
{
  killall -9 ppp_watch >/dev/null 2>/dev/null
  ledtool 3 0
  (echo 0 > /proc/var/fyi/wan/ppp_${nas_vcc}_1/wanup) >/dev/null 2>/dev/null
  (echo 0 > /proc/var/fyi/wan/ppp_${nas_vcc_sec}_1/wanup) >/dev/null 2>/dev/null
  sleep 5
  delay=0
  [ "$1" != "" ] && delay=$1
  sleep $delay
  (echo 3 > /proc/var/fyi/wan/ppp_${nas_vcc}_1/daemonstatus) >/dev/null 2>/dev/null
  (echo 3 > /proc/var/fyi/wan/ppp_${nas_vcc_sec}_1/daemonstatus) >/dev/null 2>/dev/null

  if [ "$dsl_persistent_conn" = "1" ]; then
    ppp_watch -c ppp_${nas_vcc}_1 -s /etc/start_scripts/ppp_reconnect.sh &
  else
    ppp_watch -c ppp_${nas_vcc}_1 -t -s /etc/start_scripts/ppp_reconnect.sh &
  fi

  (echo 1 > /proc/var/fyi/wan/ppp_${nas_vcc}_1/wanup) >/dev/null 2>/dev/null
  (echo 1 > /proc/var/fyi/wan/ppp_${nas_vcc_sec}_1/wanup) >/dev/null 2>/dev/null
}

case "$1" in
        start)
		ledtool 2 0
                stop
                start
                ;;
        stop)
                stop
                ;;
        ppp_restart)
		ppp_restart "$2"
		;;
        ppp_reconnect)
		ppp_reconnect "$2"
		;;
        restart)
                stop
                sleep 3 
                start
                ;;
        *)
                echo "Usage: /etc/start_scripts/dsl.sh {start|stop|restart|ppp_restart|ppp_reconnect}"
                exit 1
                ;;
esac

exit 0
