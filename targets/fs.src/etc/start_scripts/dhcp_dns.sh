#!/bin/sh
#
# Start/stops dnsmasq
#
#


case "$1" in
	start)
	        echo "Starting dnsmasq"
	        /etc/start_scripts/dhcp_conf.sh
	        /etc/start_scripts/dns_conf.sh
		dhcp_start=`/bin/nvram get dhcp_start`
		dns_start=`/bin/nvram get dns_start`
                if [ "$dhcp_start" = "1" ] || [ "$dns_start" = "1" ]; then
                  dnsmasq -C /etc/dnsmasq.conf &
                  #[ "$dns_start" = "1" ] && iptables -t nat -I PREROUTING -p udp --dport 53 -j ACCEPT
                else
                  $0 stop
                fi
		;;
	stop)
	        echo "Stopping dnsmasq"
		kill `cat /var/run/dnsmasq.pid >/dev/null 2>/dev/null` >/dev/null 2>/dev/null
		kill -9 `/bin/pidof dnsmasq` >/dev/null 2>/dev/null
		#iptables -t nat -D PREROUTING -p udp --dport 53 -j ACCEPT >/dev/null 2>/dev/null
		;;
	restart)
		$0 stop &
		sleep 2
		$0 start
		;;
	*)
		echo "Usage: /etc/start_scripts/dhcp_dns.sh {start|stop|restart}"
		exit 1
		;;
esac

exit 0
