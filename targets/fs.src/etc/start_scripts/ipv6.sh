#!/bin/sh

FORWARDING="/proc/sys/net/ipv6/conf/all/forwarding"
export RADVD_CONF="/var/radvd.conf"
export CUSTOM_TUN="/var/ipv6_custom_dyn_tun.sh"
PARALLEL="/var/ipv6_para_gsfhgdagadg"
RADVD_PID="/var/run/radvd.pid"

# $1=nvram variable, $2=default value
get_nvram()
{
  x=`/bin/nvram get $1`
  shift
  [ "$x" = "Failed" -o "$x" = "" ] && x="$@"
  echo $x
}

###########DEBUG#############
export IPv6_DEBUG=`get_nvram ipv6_debug 0`
export IPv6_DEBUG_FILE=`get_nvram ipv6_debug_file /var/ipv6.log`
[ "$IPv6_DEBUG" = "1" ] && > $IPv6_DEBUG_FILE
#############################

exec_cmd()
{
  if [ "$IPv6_DEBUG" = "1" ]; then
    echo "$1" >> $IPv6_DEBUG_FILE
    /bin/sh -c "$1 2>> $IPv6_DEBUG_FILE >> $IPv6_DEBUG_FILE"
  else
    /bin/sh -c "$1 2> /dev/null > /dev/null"
  fi
}

#generate 64bit Unique Local Unicast address prefix
gen_ula()
{
  ula=`get_nvram ipv6_ula_prefix`
  if [ "$ula" = "" ]; then
    prefix="FD"
    subnet_id=`hexdump -n 2 -e '/1 "%02X"'  /dev/urandom`
    site_id=`hexdump -n 1 -e '/1 "%02X"' /dev/urandom`:`hexdump -n 2 -e '/1 "%02X"' /dev/urandom`:`hexdump -n 2 -e '/1 "%02X"' /dev/urandom`
    ula=${prefix}${site_id}":"${subnet_id}

    /bin/nvram set ipv6_ula_prefix=$ula >/dev/null
    [ "$STARTUP" != "1" ] && /bin/nvram commit >/dev/null
  fi
  echo -n $ula
}


#generate the EIU-64 Identifier for a given device (eth0, wl0, br0, etc)
gen_eui64()
{
  device=$1
  base_mac=`ifconfig $device|grep HWaddr|awk '{print $5}'|tr : ' '`
  mac_6=`echo $base_mac|awk '{print $6}'`
  mac_5=`echo $base_mac|awk '{print $5}'`
  mac_4=`echo $base_mac|awk '{print $4}'`
  mac_3=`echo $base_mac|awk '{print $3}'`
  mac_2=`echo $base_mac|awk '{print $2}'`

  eui64="02"${mac_2}":"${mac_3}"FF:FE"${mac_4}":"${mac_5}${mac_6}
  echo $eui64
}

# conver numeriacal ascii strings like "0", "54" or "234" to hex
ascii_to_hex()
{
  [ "$1" -lt 0 -o "$1" -gt 255 ] && return 1
  let x=1+$1/16
  let y=1+$1%16
  A="0123456789ABCDEF"
  h1=`expr substr $1 $x 1`
  h2=`expr substr $1 $y 1`

  echo -n $h1$h2
  return 0
}

#generate a 2002:aabb:ccdd::/48 6to4 tunnel prefix from IPv4 address a.b.c.d
gen_2002_prefix()
{
  ipv4=`echo "$1"|sed -n -e 's/[^0-9]*\([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\)[^0-9]*/\1/p'`
  [ "$ipv4" = "" ] && return 1
  echo -n $(printf '2002:%X%02X:%X:%02X:' $(echo $ipv4 | tr . ' '))
  return 0
}


# generate radvd configuration file
gen_radvd_conf()
{
 ipv6_adv_minint=`get_nvram ipv6_adv_minint 3`
 ipv6_adv_maxint=`get_nvram ipv6_adv_maxint 10`
 ipv6_adv_mobile=`get_nvram ipv6_adv_mobile off`
 ipv6_ula_mobile=`get_nvram ipv6_ula_mobile off`
 ipv6_adv_mtu=`get_nvram ipv6_adv_mtu 0`

 [ "$ipv6_adv_mtu" -lt 1280 ] && ipv6_adv_mtu=1280
 [ "$ipv6_adv_mtu" -gt 1500 ] && ipv6_adv_mtu=1500

 cat << EOF_CONF > $RADVD_CONF
interface br0
{
  AdvSendAdvert on;
  IgnoreIfMissing on;
  MinRtrAdvInterval $ipv6_adv_minint;
  MaxRtrAdvInterval $ipv6_adv_maxint;
  AdvDefaultPreference low;
  AdvHomeAgentFlag $ipv6_adv_mobile;
  AdvLinkMTU $ipv6_adv_mtu;

EOF_CONF

 # configure Unique Local Unicast address
 use_ula=`get_nvram ipv6_use_ula 1`
 if [ "$use_ula" = "1" ]; then
   ula_prefix=`/bin/nvram get ipv6_ula_prefix`
   cat << EOF_CONF >> $RADVD_CONF
  prefix ${ula_prefix}::/64
  {
    AdvOnLink on;
    AdvAutonomous on;
    AdvRouterAddr $ipv6_ula_mobile;
  };

EOF_CONF

 fi

 # add IPv6 routes

 # additional route record:
 # record:= <advertise on/off>,<ipv6 address and prefix>,<on-link>,<mobile>,<AdvAutonomous>,
 #          <AdvValidLifetime>,<AdvPreferredLifetime>,<gateway>,<device>,<metrik>
 # additional route:= <record>[~<record>]*
 # <advertise on/off>:= 0-off, 1-on, 2-on (advertise but set no route)

 ipv6_add_routes=`get_nvram ipv6_add_routes`

 if [ "$ipv6_add_routes" != "" ]; then
   echo $ipv6_add_routes |sed -e 's/~/\n/g'|sed -n -e '/^$/!p'| \
   awk '{ split($0,a,/[,]/); 
          advertise=a[1]; 
          if(advertise < 1){continue;} 
          ipv6_addr=a[2]; 
          on_link=a[3]; 
          mobile=a[4]; 
          advauto=a[5]; 
          validtime=a[6]; 
          preftime=a[7]; 

          if(on_link=="1"){str_link="on"}else{str_link="off"}
          if(mobile=="1"){str_mobile="on"}else{str_mobile="off"}
          if(advauto=="1"){str_advauto="on"}else{str_advauto="off"}
          if(validtime!="0"){str_validtime="    AdvValidLifetime " validtime ";\n" }
            else{str_validtime=""} 
          if(preftime!="0"){str_preftime="    AdvPreferredLifetime " preftime ";\n" } 
            else{str_preftime=""} 

          print "  prefix " ipv6_addr
          print "  {"
          print "    AdvOnLink " str_link ";"
          print "    AdvAutonomous " str_advauto ";"
          print "    AdvRouterAddr " str_mobile ";"
          print str_validtime str_preftime "  };\n"
        }' >> $RADVD_CONF
 fi

## close the config file with "echo '};' >> $RADVD_CONF" outside this function

}

# add automatic 6to4 tunnel
add_6to4_auto_tunnel()
{
  if [ "$IPv6_DEBUG" = "1" ]; then
    echo "add_6to4_auto_tunnel()" >> $IPv6_DEBUG_FILE
  fi

  [ "`/bin/nvram get ipv6_6to4_auto_start`" != "1" ] && return

  # auto 6to4 tun record:= <advertise on/off>,<local_ipv4|dyn_local_ipv4 from used device>,
  #                        <local_ipv6_subnet>,<route>,<ttl>,<metric>,<on-link>,<mobile>,
  #                        <AdvAutonomous>,<AdvValidLifetime>,<AdvPreferredLifetime>,
  #                        <route_dev>,<route_metric>,<behind_NAT>,<NAT_addr>,<MTU>
  # dyn_local_ipv4 from used device:= 'dyn:'<device> ; e.g. "dyn:ppp_1_32_1"
  # local_ipv6_subnet:="0000" to "FFFF" -- 16 Bit

  ipv6_6to4_auto=`get_nvram ipv6_6to4_auto`
  if [ "$ipv6_6to4_auto" != "" ]; then
   echo $ipv6_6to4_auto |sed -e 's/~/\n/g'|sed -n -e '/^$/!p'| \
   awk 'function exec_cmd(x)
          {
            if(ENVIRON["IPv6_DEBUG"]=="1")
            {
              dbf=ENVIRON["IPv6_DEBUG_FILE"];
              print x >> dbf;
              system(x " >>" dbf " 2>>" dbf)
            }
            else
            {
              system(x " >/dev/null 2>/dev/null");
            }
          }
        { split($0,a,/[,]/)
          advertise=a[1]
          local_ipv4=a[2]
          ipv6_subnet=a[3]
          route=a[4]
          ttl=a[5]
          metric=a[6]
          on_link=a[7]
          mobile=a[8]
          advauto=a[9]
          validtime=a[10]
          preftime=a[11]
          route_dev=a[12]
          route_metrik=a[13]
          behind_nat=a[14]
          nat_addr=a[15]
          mtu=a[16]

          if(local_ipv4 ~ /^dyn/)
          {
            split(local_ipv4,h,/:/)
            split("br0,wl0,PPP0", ifnames, ",");
            dev=ifnames[h[2]+1];

            if(dev=="PPP0")
            {
              cmd = "atm_vpi=`/bin/nvram get atm_vpi`; [ \"$atm_vpi\" = \"Failed\" ] && atm_vpi=1; echo $atm_vpi";
              cmd | getline;
              atm_vpi=$0;
              close(cmd);

              cmd = "atm_vci=`/bin/nvram get atm_vci`; [ \"$atm_vci\" = \"Failed\" ] && atm_vci=32; echo $atm_vci";
              cmd | getline;
              atm_vci=$0;
              close(cmd);

              cmd = "cat /proc/var/fyi/wan/ppp_" atm_vpi "_" atm_vci "_1/ipaddress";
              cmd | getline;
              local_ipv4=$0;
              close(cmd)
            }
            else
            {
              cmd = "ifconfig " dev "|grep inet|sed -e \"s/:/ /\"";
              cmd | getline;
              local_ipv4=$3;
              close(cmd);
            }
          }

          if(behind_nat=="1" && nat_addr ~ /^dyn/)
          {
            split(nat_addr,h,/:/)
            split("br0,wl0", ifnames, ",");
            dev=ifnames[h[2]+1];

            cmd = "ifconfig " dev "|grep inet|sed -e \"s/:/ /\"";
            cmd | getline;
            nat_addr=$3;
            close(cmd);
          }

          split(local_ipv4,ip,"."); 
          prefix=sprintf("2002:%X%02X:%X%02X:", ip[1], ip[2], ip[3], ip[4]);
          local_ipv6=prefix ":1/48"

          if(behind_nat=="1"){ local_ipv4=nat_addr }

          # add tunnel

          cmd="ip tunnel add tun6to4_auto mode sit remote any local " local_ipv4 " ttl " ttl;
          exec_cmd(cmd);

          if(mtu==""){mtu_str=""}else{mtu_str="mtu " mtu;}

          cmd="ip link set tun6to4_auto " mtu_str " up";
          exec_cmd(cmd);

          cmd="ip -6 addr flush to 2002::/16 dev tun6to4_auto";
          exec_cmd(cmd);
          cmd="ip -6 addr add " local_ipv6 " dev tun6to4_auto";
          exec_cmd(cmd);
          cmd="ip -6 addr add " prefix ipv6_subnet "::1/64 dev " route_dev;
          exec_cmd(cmd);

          if(metric==""){metric=250;}
          cmd="route -A inet6 add " route " gw ::192.88.99.1 dev tun6to4_auto metric " metric;
          exec_cmd(cmd);

          # add route
          local_ipv6_subnet=prefix ipv6_subnet "::/64"

          # add radvd entry
          if(advertise=="0"){ exit; }

          if(on_link=="1"){str_link="on"}else{str_link="off"}
          if(mobile=="1"){str_mobile="on"}else{str_mobile="off"}
          if(advauto=="1"){str_advauto="on"}else{str_advauto="off"}
          if(validtime!="0"){str_validtime="    AdvValidLifetime " validtime ";\n" }
            else{str_validtime=""} 
          if(preftime!="0"){str_preftime="    AdvPreferredLifetime " preftime ";\n" } 
            else{str_preftime=""} 

          if(dev=="PPP0")
          {
            print "  prefix 0:0:0:" ipv6_subnet "::/64" >> ENVIRON["RADVD_CONF"]
          }
          else
          {
            print "  prefix " local_ipv6_subnet         >> ENVIRON["RADVD_CONF"]
          }
          print "  {"                                 >> ENVIRON["RADVD_CONF"]
          print "    AdvOnLink " str_link ";"         >> ENVIRON["RADVD_CONF"]
          print "    AdvAutonomous " str_advauto ";"  >> ENVIRON["RADVD_CONF"]
          print "    AdvRouterAddr " str_mobile ";"   >> ENVIRON["RADVD_CONF"]
          if(dev=="PPP0")
          {
            print "    Base6to4Interface ppp_" atm_vpi "_" atm_vci "_1;" >> ENVIRON["RADVD_CONF"]
          }
          print str_validtime str_preftime "  };\n"   >> ENVIRON["RADVD_CONF"]
        }'
  fi
}


# remove automatic 6to4 tunnel
del_6to4_auto_tunnel()
{
  if [ "$IPv6_DEBUG" = "1" ]; then
    echo "del_6to4_auto_tunnel()" >> $IPv6_DEBUG_FILE
  fi

  if [ `ip tunnel | grep tun6to4_auto | wc -l` -gt 0 ]; then
    OLD_IFS=$IFS
    for value in `ip -6 route | grep "2002:" | cut -f 1,2,3 -d " "`; do
      exec_cmd "ip -6 route del $value"
    done
    IFS=$OLD_IFS

    exec_cmd "ip -6 route del 2000::/3 via ::192.88.99.1 dev tun6to4_auto"

    for i in `ifconfig tun6to4_auto|grep inet6|grep "2002:"|cut -f 13 -d " "`; do exec_cmd "ip addr del $i dev tun6to4_auto"; done

    exec_cmd "ip -6 addr flush to 2002::/16 dev tun6to4_auto"
    exec_cmd "ip link set tun6to4_auto down"
    exec_cmd "ip tunnel del tun6to4_auto"
  fi

  for i in `ifconfig br0|grep inet6|grep "2002:"|cut -f 13 -d " "`; do exec_cmd "ip addr del $i dev br0"; done
  exec_cmd "ip -6 addr flush to 2002::/16 dev br0"
}

update_dyn_6to4_auto()
{
  if [ "$IPv6_DEBUG" = "1" ]; then
    echo "update_dyn_6to4_auto()" >> $IPv6_DEBUG_FILE
  fi

  export dyn_device="$1"

  [ "`/bin/nvram get ipv6_6to4_auto_start`" != "1" ] && return

  ipv6_6to4_auto=`get_nvram ipv6_6to4_auto`
  if [ "$ipv6_6to4_auto" != "" ]; then
   echo $ipv6_6to4_auto | \
   awk '{
          split($0,a,/[,]/);
          local_ipv4=a[2];
          if(local_ipv4 ~ /^dyn/)
          {
            split(local_ipv4,h,/:/)
            split("br0,wl0,PPP0", ifnames, ",");
            dev=ifnames[h[2]+1];

            dyn_dev=ENVIRON["dyn_device"]
            if(dyn_dev==dev){ exit 0; }
          }
          exit 1;
        }'

    if [ "$?" = "0" ]; then
      del_6to4_auto_tunnel
      radvd_start=`get_nvram ipv6_radvd_start 1`
      if [ "$radvd_start" = "1" ]; then
        #dirty hack, we won't change the config file by add_6to4_auto_tunnel
        RADVD_CONF_TEMP=$RADVD_CONF
        export RADVD_CONF=/dev/null

        add_6to4_auto_tunnel

        #restore the name of the config file
        export RADVD_CONF=$RADVD_CONF_TEMP
        exec_cmd "kill -HUP `cat $RADVD_PID`"
      else
        add_6to4_auto_tunnel
      fi
    fi

  fi
}

start()
{
  if [ "$IPv6_DEBUG" = "1" ]; then
    echo "start()" >> $IPv6_DEBUG_FILE
  fi

  ipv6_start=`get_nvram ipv6_start 0`
  [ "$ipv6_start" != "1" ] && return

  lsmod |grep ipv6 >/dev/null || exec_cmd "modprobe ipv6"
  echo 1 > $FORWARDING

  # configure Unique Local Unicast address
  use_ula=`get_nvram ipv6_use_ula 1`
  if [ "$use_ula" = "1" ]; then
    ula_prefix=`gen_ula`
    ula_host=`gen_eui64 br0`
    ula=${ula_prefix}":"${ula_host}"/64"

    exec_cmd "ip addr add $ula dev br0"
  fi

  # configure and start radvd
  radvd_start=`get_nvram ipv6_radvd_start 1`
  if [ "$radvd_start" = "1" ]; then
    gen_radvd_conf
    add_6to4_auto_tunnel

    echo '};' >> $RADVD_CONF
    exec_cmd "radvd -p $RADVD_PID -C $RADVD_CONF"
  else
    add_6to4_auto_tunnel
  fi

  # add IPv6 address

  # additional ipv6 address record:
  # record:= <ipv6 address and prefic>,<interface>
  # ipv6 address records:= <record>[,<record>]

  ipv6_add_addr=`get_nvram ipv6_add_addr`
  if [ "$ipv6_add_addr" != "" ]; then
   echo $ipv6_add_addr |sed -e 's/~/\n/g'|sed -n -e '/^$/!p'| \
   awk 'function exec_cmd(x)
          {
            if(ENVIRON["IPv6_DEBUG"]=="1")
            {
              dbf=ENVIRON["IPv6_DEBUG_FILE"];
              print x >> dbf;
              system(x " >>" dbf " 2>>" dbf)
            }
            else
            {
              system(x " >/dev/null 2>/dev/null");
            }
          }
        { split($0,a,/[,]/)
          ipv6_addr=a[1]
          iface=a[2]
          split("br0,wl0", ifnames, ",");
          cmd="ip addr add " ipv6_addr " dev " ifnames[iface+1]
          exec_cmd(cmd)
        }'
  fi

  # add 6to4 tunnels

  # 6to4 tunnel record:= <advertise on/off>,<remote_ipv4>,
  #                      <local_ipv4|dyn_local_ipv4 from used device>,
  #                      <local_ipv6>,<local_ipv6_subnet>,<route>,<ttl>,<advertise>,<metric>
  #                      <mtu>
  # dyn_local_ipv4 from used device:= 'dyn:'<device> ; e.g. "dyn:ppp_1_32_1"
  # 6to4 tunnels:= <record>[,<record>]
  ipv6_6to4_tunnel=`get_nvram ipv6_6to4_tunnel`
  if [ "$ipv6_6to4_tunnel" != "" ]; then
    echo $ipv6_6to4_tunnel |sed -e 's/~/\n/g'|sed -n -e '/^$/!p'| \
    awk 'function exec_cmd(x)
          {
            if(ENVIRON["IPv6_DEBUG"]=="1")
            {
              dbf=ENVIRON["IPv6_DEBUG_FILE"];
              print x >> dbf;
              system(x " >>" dbf " 2>>" dbf)
            }
            else
            {
              system(x " >/dev/null 2>/dev/null");
            }
          }
        BEGIN{ tun_id=0 }
        { split($0,a,/[,]/)
          remote_ipv4=a[1]
          local_ipv4=a[2]
          local_ipv6=a[3]
          local_ipv6_subnet=a[4]
          route=a[5]
          ttl=a[6]
          metric=a[8]
          mtu=a[9]

          if(local_ipv4 ~ /^dyn/)
          {
            split(local_ipv4,h,/:/)
            split("br0,wl0,PPP0", ifnames, ",");
            dev=ifnames[h[2]+1];

            if(dev=="PPP0")
            {
              cmd = "atm_vpi=`/bin/nvram get atm_vpi`; [ \"$atm_vpi\" = \"Failed\" ] && atm_vpi=1; atm_vci=`/bin/nvram get atm_vci`; [ \"$atm_vci\" = \"Failed\" ] && atm_vci=32; cat /proc/var/fyi/wan/ppp_$\{atm_vpi\}_$\{atm_vci\}_1/ipaddress";
              cmd | getline;
              local_ipv4=$0;
              close(cmd")
            }
            else
            {
              cmd = "ifconfig " dev "|grep inet|sed -e \"s/:/ /\"";
              cmd | getline;
              local_ipv4=$3;
              close(cmd);
            }
          }

          tun_id++;
          cmd="ip tunnel add tun6to4_" tun_id " mode sit remote " remote_ipv4 " local " local_ipv4 " ttl " ttl;
          exec_cmd(cmd);

          if(mtu==""){mtu_str=""}else{mtu_str="mtu " mtu;}
          cmd="ip link set tun6to4_" tun_id " " mtu_str " up";
          exec_cmd(cmd);

          cmd="ip addr add " local_ipv6 " dev tun6to4_" tun_id;
          exec_cmd(cmd);

          if(metric==""){metric=250;}
          cmd="route -A inet6 add " route " dev tun6to4_" tun_id " metric " metric;
          exec_cmd(cmd);;
        }'
  fi


  # IPv6 routes
  ipv6_add_routes=`get_nvram ipv6_add_routes`
  if [ "$ipv6_add_routes" != "" ]; then
    echo $ipv6_add_routes |sed -e 's/~/\n/g'|sed -n -e '/^$/!p'| \
    awk 'function exec_cmd(x)
          {
            if(ENVIRON["IPv6_DEBUG"]=="1")
            {
              dbf=ENVIRON["IPv6_DEBUG_FILE"];
              print x >> dbf;
              system(x " >>" dbf " 2>>" dbf)
            }
            else
            {
              system(x " >/dev/null 2>/dev/null");
            }
          }
        {
          split($0,a,/[,]/); 
          advertise=a[1]; 
          if(advertise=="2"){continue;}
          ipv6_addr=a[2]; 
          on_link=a[3]; 
          mobile=a[4]; 
          advauto=a[5]; 
          validtime=a[6]; 
          preftime=a[7]; 
          gateway=a[8];
          device=a[9];
          metrik=a[10];

          if(device=="" && gateway=="" ){continue;}
          if(device=="" || device=="none"){device_str=""}else
          { 
            #split("br0,wl0", ifnames, ",");
            #device_str="dev " ifnames[device+1];
            device_str="dev " device;
          }
          if(metrik==""){metrik_str=""}else{metrik_str="metric " metrik;}

          if(metrik=="")
          {
            if(gateway==""){gateway_str=""}else{gateway_str="via " gateway;}
            metrik_str="";
            cmd="ip route add " ipv6_addr " " gateway_str " " device_str " " metrik_str;
          }
          else
          {
            if(gateway==""){gateway_str=""}else{gateway_str="gw " gateway;}
            cmd="route -A inet6 add " ipv6_addr " " gateway_str " " device_str " " metrik_str;
          }
          exec_cmd(cmd);
        }'
  fi

}

stop()
{
  if [ "$IPv6_DEBUG" = "1" ]; then
    echo "stop()" >> $IPv6_DEBUG_FILE
  fi

  # stop forwarding and radvd
  echo 0 > $FORWARDING
  exec_cmd "killall radvd"

  # remove Unicast Local Unicast address
  ula_host=`gen_eui64 br0`
  ula_prefix=`/bin/nvram get ipv6_ula_prefix`
  ula=${ula_prefix}":"${ula_host}"/64"
  exec_cmd "ip addr del $ula dev br0"
  exec_cmd "ip route del ${ula_prefix}::/64 dev br0"

  # remove IPv6 additional addresses
  ipv6_add_addr=`get_nvram ipv6_add_addr`
  if [ "$ipv6_add_addr" != "" ]; then
    echo $ipv6_add_addr |sed -e 's/~/\n/g'|sed -n -e '/^$/!p'| \
    awk 'function exec_cmd(x)
          {
            if(ENVIRON["IPv6_DEBUG"]=="1")
            {
              dbf=ENVIRON["IPv6_DEBUG_FILE"];
              print x >> dbf;
              system(x " >>" dbf " 2>>" dbf)
            }
            else
            {
              system(x " >/dev/null 2>/dev/null");
            }
          }
        { split($0,a,/[,]/)
          ipv6_addr=a[1]
          iface=a[2]
          split("br0,wl0", ifnames, ",");

          cmd="ip addr del " ipv6_addr " dev " ifnames[iface+1];
          exec_cmd(cmd);
        }'
  fi
  ## delete all pending IPv6 addresses
  # don't delete link local addresses fe80::...
  for i in `ifconfig br0|grep inet6|grep -v "fe80::"|cut -f 13 -d " "`; do exec_cmd "ip addr del $i dev br0"; done
  for i in `ifconfig eth0|grep inet6|grep -v "fe80::"|cut -f 13 -d " "`; do exec_cmd "ip addr del $i dev eth0"; done
  for i in `ifconfig wl0|grep inet6|grep -v "fe80::"|cut -f 13 -d " "`; do exec_cmd "ip addr del $i dev wl0"; done

  # remove IPv6 routes
  ipv6_add_routes=`get_nvram ipv6_add_routes`
  if [ "$ipv6_add_routes" != "" ]; then
    echo $ipv6_add_routes |sed -e 's/~/\n/g'|sed -n -e '/^$/!p'| \
    awk 'function exec_cmd(x)
          {
            if(ENVIRON["IPv6_DEBUG"]=="1")
            {
              dbf=ENVIRON["IPv6_DEBUG_FILE"];
              print x >> dbf;
              system(x " >>" dbf " 2>>" dbf)
            }
            else
            {
              system(x " >/dev/null 2>/dev/null");
            }
          }
         { split($0,a,/[,]/); 
           advertise=a[1]; 
           ipv6_addr=a[2]; 
           on_link=a[3]; 
           mobile=a[4]; 
           advauto=a[5]; 
           validtime=a[6]; 
           preftime=a[7]; 
           gateway=a[8];
           device=a[9];
           metrik=a[10];

           if(device=="" && gateway=="" ){continue;}

           cmd="ip route del " ipv6_addr;
           exec_cmd(cmd);
         }'
  fi
  ## remove all pending IPv6 routes
  for i in `ip -6 route|sed -e 's/default/::\/0/'|cut -f 1 -d " "`; do exec_cmd "ip route del $i"; done

  del_6to4_auto_tunnel

  # remove 6to4 tunnels
  ipv6_6to4_tunnel=`get_nvram ipv6_6to4_tunnel`
  if [ "$ipv6_6to4_tunnel" != "" ]; then
    echo $ipv6_6to4_tunnel |sed -e 's/~/\n/g'|sed -n -e '/^$/!p'| \
    awk 'function exec_cmd(x)
          {
            if(ENVIRON["IPv6_DEBUG"]=="1")
            {
              dbf=ENVIRON["IPv6_DEBUG_FILE"];
              print x >> dbf;
              system(x " >>" dbf " 2>>" dbf)
            }
            else
            {
              system(x " >/dev/null 2>/dev/null");
            }
          }
        BEGIN{ tun_id=0 }
        { split($0,a,/[,]/)
          remote_ipv4=a[1]
          local_ipv4=a[2]
          local_ipv6=a[3]
          local_ipv6_subnet=a[4]
          route=a[5]
          ttl=a[6]
          metric=a[8]

          tun_id++;
          cmd="ip -6 route del " route " dev tun6to4_" tun_id;
          exec_cmd(cmd);

          cmd="ip link set tun6to4_" tun_id " down";
          exec_cmd(cmd);

          cmd="ip tunnel del tun6to4_" tun_id;
          exec_cmd(cmd);
        }'
  fi

  ## remove all pending tun6to_4 devices
  for i in `ip tunnel|grep tun6to4_|cut -f 1 -d :`; do 
    exec_cmd "ip link set $i down"
    exec_cmd "ip tunnel del $i"
  done
}

update_dyn_6to4()
{
  dyn_device="$1"
  export dyn_device
  ipv6_6to4_tunnel=`get_nvram ipv6_6to4_tunnel`
  if [ "$ipv6_6to4_tunnel" != "" ]; then
    echo $ipv6_6to4_tunnel |sed -e 's/~/\n/g'|sed -n -e '/^$/!p'| \
    awk 'function exec_cmd(x)
          {
            if(ENVIRON["IPv6_DEBUG"]=="1")
            {
              dbf=ENVIRON["IPv6_DEBUG_FILE"];
              print x >> dbf;
              system(x " >>" dbf " 2>>" dbf)
            }
            else
            {
              system(x " >/dev/null 2>/dev/null");
            }
          }
        BEGIN{ tun_id=0 }
        { split($0,a,/[,]/)
          remote_ipv4=a[1]
          local_ipv4=a[2]
          local_ipv6=a[3]
          local_ipv6_subnet=a[4]
          route=a[5]
          ttl=a[6]
          metric=a[8]
          mtu=a[9]
          dyn_dev=ENVIRON["dyn_device"]

          tun_id++;

          dev=""
          if(local_ipv4 ~ /^dyn/)
          {
            split(local_ipv4,h,/:/);
            split("br0,wl0,PPP0", ifnames, ",");
            dev=ifnames[h[2]+1];

            if(dev!=dyn_dev){ continue; }

            if(dev=="PPP0")
            {
              cmd = "atm_vpi=`/bin/nvram get atm_vpi`; [ \"$atm_vpi\" = \"Failed\" ] && atm_vpi=1; atm_vci=`/bin/nvram get atm_vci`; [ \"$atm_vci\" = \"Failed\" ] && atm_vci=32; cat /proc/var/fyi/wan/ppp_$\{atm_vpi\}_$\{atm_vci\}_1/ipaddress";
              cmd | getline;
              local_ipv4=$0;
              close(cmd);
            }
            else
            {
              cmd = "ifconfig " dev "|grep inet|sed -e \"s/:/ /\"";
              cmd | getline;
              local_ipv4=$3;
              close(cmd);
            }

            cmd = "ip tunnel|grep tun6to4_" tun_id "";
            cmd | getline;
            old_local_ipv4=$6; 
            close(cmd);

            if(old_local_ipv4 == local_ipv4) { continue; }
          }
          else { continue; }

          #delete old route and tunnel

          cmd="ip -6 route del " route " dev tun6to4_" tun_id;
          exec_cmd(cmd);

          cmd="ip link set tun6to4_" tun_id " down";
          exec_cmd(cmd);

          cmd="ip tunnel del tun6to4_" tun_id;
          exec_cmd(cmd);

          #add new tunnel

          cmd="ip tunnel add tun6to4_" tun_id " mode sit remote " remote_ipv4 " local " local_ipv4 " ttl " ttl;
          exec_cmd(cmd);

          if(mtu==""){mtu_str=""}else{mtu_str="mtu " mtu;}
          cmd="ip link set tun6to4_" tun_id " " mtu_str " up";
          exec_cmd(cmd);

          cmd="ip addr add " local_ipv6 " dev tun6to4_" tun_id;
          exec_cmd(cmd);

          if(metric==""){metric=250;}
          cmd="route -A inet6 add " route " dev tun6to4_" tun_id " metric " metric;
          exec_cmd(cmd);

          cmd="RET=`/bin/nvram getfile ipv6_custom_dyn_tun=" ENVIRON["CUSTOM_TUN"] "`; [ \"$RET\" != \"Failed\" ] && ( chmod +x " ENVIRON["CUSTOM_TUN"] "; export DEV=" dev "; export LOCAL=" local_ipv4 "; export REMOTE=" remote_ipv4 "; /bin/sh " ENVIRON["CUSTOM_TUN"] " )"
          exec_cmd(cmd);
        }'
  fi

}

case "$1" in
        start)
            echo "Starting IPv6"
            > $PARALLEL
            start
            rm $PARALLEL
            ;;
        stop)
            echo "Stopping IPv6"
            stop
            ;;
        restart)
            stop
            start
            ;;
        update_dyn_6to4)
            [ "$2" = "" ] && echo "device not specified!" && exit 1
            echo "update 6to4 tunnels with dynamic address for device $2"
            while [ -f $PARALLEL ]; do sleep 1; done
            update_dyn_6to4_auto $2
            update_dyn_6to4 $2
            ;;
        *)
            echo "Usage: /etc/start_scripts/ipv6.sh {start|stop|restart|update_dyn_6to4}"
            exit 1
            ;;
esac

exit 0
