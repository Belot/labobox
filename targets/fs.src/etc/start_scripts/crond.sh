#!/bin/sh
#
# Start/stops crond
#

CRONDIR="/var/spool/cron/crontabs"
CRONTAB=$CRONDIR"/root"

case "$1" in
    start)
      echo "Starting crond"

      mkdir -p $CRONDIR
      touch $CRONTAB

      #start crond if not running
      if [ "`pidof crond`" != ]; then
          crond
      else
        echo "crond is already running..."
      fi
    ;;
  stop)
        echo "Stopping crond"
        killall crond 2>/dev/null
        killall -9 crond 2>/dev/null
      ;;
  restart)
      $0 stop
      $0 start
      ;;
  *)
      echo "Usage: /etc/start_scripts/crond.sh {start|stop|restart}"
      exit 1
      ;;
esac
    
exit 0
    