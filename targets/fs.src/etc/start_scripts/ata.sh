#!/bin/sh
# analog Telefon Adapter

#. /webs/cgi-bin/functions.sh

STATUS_DIR="/var/voice/"
STATUS_FILE=$STATUS_DIR"status"
PSI_FILE="/var/psi.xml"
PARTIAL_FILE="/var/tcm_partial.conf"
LOG_FILE="/var/vodsl.log"

STAT_DEINIT=0
STAT_INIT=1
STAT_INIT_IN_PROGR=2
STAT_DEINIT_IN_PROGR=3

TIME_TO_INIT=5
TIME_TO_DEINIT=6

UPDATE_ALL=64
UPDATE_FILE="/var/tcm_updateoption"
#UPDATE_PID="/var/Bcm_cmtask.pid"
#SIGUSR2=12

#WAN_IFACE="ls /proc/var/fyi/wan/ppp*"
#WIFACE="ls /proc/var/fyi/wan/"
#PPP_STATUS_FILE="`$WAN_IFACE`/daemonstatus"
PPP_OK=4

atm_vpi=`nvram get atm_vpi`
[ "$atm_vpi" = "Failed" ] && atm_vpi=1
atm_vci=`nvram get atm_vci`
[ "$atm_vci" = "Failed" ] && atm_vci=32

PPP_STATUS_FILE="/proc/var/fyi/wan/ppp_${atm_vpi}_${atm_vci}_1/daemonstatus"
WIFACE="ppp_${atm_vpi}_${atm_vci}_1"
WAN_STATUS_FILE="/proc/var/fyi/wan/ppp_${atm_vpi}_${atm_vci}_1/status"

ENA="enable"
DIS="disable"

voip_dsl_wait=`nvram get voip_dsl_wait`
[ "$voip_dsl_wait" = "Failed" ] && voip_dsl_wait=0

partial_init()
{
  part_default="0137 0180 0190 0800 0900"
  part_default_n=5
  mustsave=0
  i=0
  j=1
  num=0
  while [ $i -le 19 ]
  do  
    number=`nvram get ata_partn$i`
    [ $j -le $part_default_n ] && [ "$number" = "Failed" ] && number=`echo $part_default|awk '{print $'$j';}'` && nvram set ata_partn$i=$number >/dev/null && nvram set ata_partc$i=1 >/dev/null
    [ "$number" = "Failed" ] && number=""
    [ "$number" != "" ] && let num=$num+1
    let i=$i+1
    let j=$i+1
  done
  echo "num=$num" > $PARTIAL_FILE

  i=0
  while [ $i -le 19 ]
  do  
    number=`nvram get ata_partn$i`
    line=`nvram get ata_partc$i`
 
    [ "$number" = "Failed" ] && number=""
    [ "$line" = "Failed" ] && line=0

    if [ "$number" != "" ]; then
      echo $number >> $PARTIAL_FILE
      echo $line >> $PARTIAL_FILE
    fi
      let i=$i+1
  done
}

psi_tree_init()
{
  echo -n "create psi tree..."
  echo -ne "<psitree>\n<TecomVoipCfg>\n" > $PSI_FILE
  pri_select=`nvram get ata_pri_select`
  [ "$pri_select" = "Failed" ] && pri_select="0"

  sec_select=`nvram get ata_sec_select`
  [ "$sec_select" = "Failed" ] && sec_select="1"

  enbl_2line_dial=`nvram get ata_enbl_2line_dial`
  [ "$enbl_2line_dial" = "Failed" ] && enbl_2line_dial="1"
  [ "$enbl_2line_dial" = "1" ] && enbl_2line_dial=$ENA || enbl_2line_dial=$DIS

  number_hide=`nvram get ata_number_hide`
  [ "$number_hide" = "Failed" ] && number_hide="0"
  [ "$number_hide" = "1" ] && number_hide=$ENA || number_hide=$DIS

  enable_clip=`nvram get ata_enable_clip`
  [ "$enable_clip" = "Failed" ] && enable_clip="0"
  [ "$enable_clip" = "1" ] && enable_clip=$ENA || enable_clip=$DIS

  anklopfen=`nvram get ata_enable_anklopfen`
  [ "$anklopfen" = "Failed" ] && anklopfen="0"
  [ "$anklopfen" = "1" ] && anklopfen=$ENA || anklopfen=$DIS

  analog=`nvram get ata_enable_analog`
  [ "$analog" = "Failed" ] && analog="1"
  [ "$analog" = "1" ] && analog=$ENA || analog=$DIS

  fallback=`nvram get ata_enable_fallback`
  [ "$fallback" = "Failed" ] && fallback="1"
  [ "$fallback" = "1" ] && fallback=$ENA || fallback=$DIS

  echo '<portInfo pri_select="'$pri_select'" sec_select="'$sec_select'" enbl_2line_dial="'$enbl_2line_dial'" numberHide="'$number_hide'" enable_CLIP="'$enable_clip'" enable_Anklopfen="'$anklopfen'" enable_Analog="'$analog'" enable_Fallback="'$fallback'" />' >> $PSI_FILE

  echo -n '<emergency' >> $PSI_FILE
  i=0
  while [ $i -le 9 ]; do
    num=`nvram get ata_emgn$i`
    desc=`nvram get ata_emgd$i`
    [ "$num" = "Failed" ] && num=""
    [ "$desc" = "Failed" ] && desc=""

    echo -n ' emg_number'$i'="'$num'" emg_desc'$i'="'$desc'"' >> $PSI_FILE

    let i=$i+1
  done
  echo '/>' >> $PSI_FILE

   echo -n '<quick' >> $PSI_FILE

  [ "$1" = "" ] && end=99 || end=$1
  i=0
  while [ $i -le $end ]; do
    num=`nvram get ata_qn$i`
    desc=`nvram get ata_qd$i`
    line=`nvram get ata_qs$i`
    [ "$num" = "Failed" ] && num=""
    [ "$desc" = "Failed" ] && desc=""
    [ "$line" = "Failed" ] && line="0"
    if [ $line -eq 1 ]; then line="pstn"
    else if [ $line -eq 2 ]; then line="both"
      else line="voip"; fi
    fi

    echo -n ' qn'$i'="'$num'" qd'$i'="'$desc'" qs'$i'="'$line'"' >> $PSI_FILE

    let i=$i+1
  done

  #second loop for the rest with reduced instructions
  let i=$end+1
  while [ $i -le 99 ]; do
    echo -n ' qn'$i'="''" qd'$i'="''" qs'$i'="'voip'"' >> $PSI_FILE
    let i=$i+1
  done

  echo '/>' >> $PSI_FILE

  echo -ne "</TecomVoipCfg>\n</psitree>\n" >> $PSI_FILE
  echo "ok"
} 

#vodsl_update()
#{
#  echo -n "Update ata..."
#  partial_init
#  psi_tree_init
#  echo $UPDATE_ALL > $UPDATE_FILE
#  kill -$SIGUSR2 `cat $UPDATE_PID`
#  echo "ok"
#}

vodsl_init()
{
  if [ ! -d $STATUS_DIR ]; then
    mkdir -p $STATUS_DIR
    echo $STAT_DEINIT > $STATUS_FILE
    echo "ok"
  fi
  ls $WAN_STATUS_FILE >/dev/null 2>/dev/null
  [ "$?" != "0" ] && bcm_helper -s ppp_${atm_vpi}_${atm_vci}_1 && bcm_helper -s ppp_1_32_1
}


#set_iptables()
#{
#  iface=$2
#  port=$3
#  [ $port -lt 1 ] && port=5060
#  [ $port -gt 65535 ] && port=5060
#  [ "$1" = "" ] && [ $iface = "" ] && return 
#  if [ "$1" = "add" ]; then
#    is_fwservice_port "udp" $port
#    [ "$?" = "1" ] && add_fwservice_port "udp" $port
#  else
#    is_fwservice_port "udp" $port
#    ret=$?
#    while [ "$ret" = "0" ]; do
#      remove_fwservice_port "udp" $port
#      is_fwservice_port "udp" $port
#      ret=$?
#    done
#  fi
#
#  [ "$STARTUP" != "1" ] && /etc/start_scripts/firewall.sh services_restart
 
##  if [ $action != "" ] && [ $iface != "" ]; then
##    iptables $action INPUT -i $iface -p udp --dport $port -j ACCEPT 2>/dev/null 
##  else
##    echo "ata.sh Error: set_iptables()"
##  fi
#}

vodsl_start()
{
  echo -n "Start ata..."
  voip_start=`nvram get voip_start`
  [ "$voip_start" = "Failed" ] && voip_start=0
  [ "$voip_start" = "0" ] && echo "Phone disabled" && exit 0
  echo $STAT_INIT_IN_PROGR > $STATUS_FILE

  echo $UPDATE_ALL > $UPDATE_FILE

  nvram getfile voip_partial=$PARTIAL_FILE >/dev/null
  if [ $? -ne 0 ]; then
    partial_init
    nvram setfile voip_partial=$PARTIAL_FILE >/dev/null
  fi

  nvram getfile voip_psi_tree=$PSI_FILE >/dev/null
  if [ $? -ne 0 ]; then
      psi_tree_init
      nvram setfile voip_psi_tree=$PSI_FILE >/dev/null
  fi

  wan_up=0
  dsl_start=`nvram get dsl_start`

  if [ "$voip_dsl_wait" = "1" ]; then
    while [ "$dsl_start" = "1" ] && [ "`cat $PPP_STATUS_FILE`" != $PPP_OK ]; do
      sleep 1
    done
  fi

  [ "$dsl_start" = "1" ] && [ "`cat $PPP_STATUS_FILE`" = $PPP_OK ] && wan_up=1

  if [ $wan_up -eq 1 ]; then
    iface=$WIFACE
  else
    iface="br0"
  fi

  ata_ext1=`nvram get ata_ext1`
  [ "$ata_ext1" = "Failed" ] && ata_ext1=""
  [ "$ata_ext1" != "" ] && ata_ext1="--x1 $ata_ext1 --x2 $ata_ext1"

  ata_uname=`nvram get ata_uname`
  [ "$ata_uname" = "Failed" ] && ata_uname="name"

  ata_pass=`nvram get ata_pass`
  [ "$ata_pass" = "Failed" ] && ata_pass="pass"
  [ "$ata_pass" != "" ] && ata_pass="--pwd1 $ata_pass --pwd2 $ata_pass"

  ata_reg=`nvram get ata_reg`
  [ "$ata_reg" = "Failed" ] && ata_reg="0.0.0.0"

  ata_regport=`nvram get ata_regport`
  [ "$ata_regport" = "Failed" ] && ata_regport="5060"

  ata_prox=`nvram get ata_prox`
  [ "$ata_prox" = "Failed" ] && ata_prox="$ata_reg"

  ata_proxport=`nvram get ata_proxport`
  [ "$ata_proxport" = "Failed" ] && ata_proxport="5060"

  ata_domain=`nvram get ata_domain`
  [ "$ata_domain" = "Failed" ] && ata_domain="domain"

  ata_vad=`nvram get ata_vad`
  [ "$ata_vad" = "Failed" ] && ata_vad=0

  ata_bovc=`nvram get ata_bovc`
  [ "$ata_bovc" = "Failed" ] && ata_bovc=0


#  set_iptables add $iface $ata_regport

  ata_start=`nvram get ata_start`
  [ "$ata_start" = "Failed" ] && ata_start=0
  if [ $ata_start -eq 1 ]; then
      echo 6 > $WAN_STATUS_FILE
  fi

#  if [ $wan_up -eq 1 ]; then
#    proxy_option=""
#  else
   proxy_option="-p ${ata_prox}:${ata_proxport}"
#  fi

  voip_country=`nvram get voip_country`
[ "$voip_country" = "Failed" ] && voip_country="DEU"

  if [ "`nvram get ata_debug`" = "1" ]; then
    vodsl sipstart $proxy_option -r ${ata_reg}:${ata_regport} -i $iface -u $ata_uname -n $ata_domain -v $ata_vad -o $ata_bovc -w $voip_country -f 1 $ata_ext1 $ata_pass > $LOG_FILE 2> $LOG_FILE &
  else
    vodsl sipstart $proxy_option -r ${ata_reg}:${ata_regport} -i $iface -u $ata_uname -n $ata_domain -v $ata_vad -o $ata_bovc -w $voip_country -f 1 $ata_ext1 $ata_pass &
  fi

  /etc/start_scripts/firewall.sh sip_restart

  sleep $TIME_TO_INIT
  echo $STAT_INIT > $STATUS_FILE
  echo "ok"
}

vodsl_stop()
{
  echo -n "Stopping ata..."
  if [ `cat $STATUS_FILE` -ne 0 ]; then
    echo $STAT_DEINIT_IN_PROGR > $STATUS_FILE

    vodsl sipstop 2>/dev/null

    wan_up=0
    dsl_start=`nvram get dsl_start`
    [ "$dsl_start" = "1" ] && [ "`cat $PPP_STATUS_FILE`" = $PPP_OK ] && wan_up=1

    if [ $wan_up -eq 1 ]; then
      iface=$WIFACE
    else
      iface="br0"
    fi

    ata_regport=`nvram get ata_regport`
    [ "$ata_regport" = "Failed" ] && ata_regport="5060"
#    set_iptables del $iface $ata_regport
#    kill `pidof fakeonline` > /dev/null 2> /dev/null

    sleep $TIME_TO_DEINIT
    echo $STAT_DEINIT > $STATUS_FILE
    echo "ok"
  else
    echo "not running"
  fi
}



case "$1" in
        start)
		vodsl_init
		vodsl_stop
		vodsl_start
                ;;
        stop)
		vodsl_init
		vodsl_stop
		/etc/start_scripts/call_mon.sh stop
                ;;
        restart)
		vodsl_init
		vodsl_stop
		vodsl_start
		/etc/start_scripts/call_mon.sh restart
                ;;
	update_psi)
		#wait for running update process
		#if [ -f /var/.update_psi ]; then
		#  i=0
		#  while [ $i -le 20 ]
		#  do
		 #   [ -f /var/.update_psi ] && sleep 1 || break
		#  done
		#fi
		#touch /var/.update_psi
		psi_tree_init $LAST_QUICK
                nvram setfile voip_psi_tree=$PSI_FILE >/dev/null
		#nvram commit
		#rm /var/.update_psi
		;;
	update_partial)
		partial_init
                nvram setfile voip_partial=$PARTIAL_FILE >/dev/null
		;;
        *)
                echo "Usage: /etc/start_scripts/ata.sh {start|stop|restart|update_psi|update_partial}"
                echo "Default using start..."
                $0 start
                exit 1
                ;;
esac

exit 0
