#!/bin/sh
#
# Start/stops vodsl call monitor
#
#
PATH=/bin/:$PATH

if [ ! -f "/bin/vodsl" ]; then
  echo "Voice is not present in this image...aborting ata"
  exit 0
fi

case "$1" in
	start)
	        echo "Starting vodsl call monitor"
		voip_start=`nvram get voip_start`
		[ "$voip_start" = "Failed" ] && voip_start=0
  		[ "$voip_start" = "0" ] && echo "Phone disabled...no need for call monitor" && exit 0
		start=`nvram get vodsl_monitor`
                [ "$start" = "0" ] && exit 0
		
                server_start=`nvram get call_mon_start`
		server_port=`nvram get call_mon_port`
		server=""
		
                [ "$server_port" != "Failed" ] && server_port="-p $server_port" || server_port=""
		[ "$server_start" = "1" ] && server=$server_port || server="-n"
		
		time_fmt=`nvram get call_mon_timefmt`
                ( [ "$time_fmt" = "Failed" ] || [ "$time_fmt" = "" ] ) && time_fmt="utc"
                
                clip_delay=`nvram get call_mon_clipdelay`
                ( [ "$clip_delay" = "Failed" ] || [ "$clip_delay" = "" ] ) && clip_delay=800
		
		vodsl_monitor -s /usr/bin/call_mon_event.sh $server -t $time_fmt -c $clip_delay -D
		;;
	stop)
	        echo "Stopping vodsl call monitor"
		killall vodsl_monitor >/dev/null 2>/dev/null
		i=0
		while [ "`pidof vodsl_monitor`" -ne 0 ]
		do
		  [ $i -ge 10 ] && killall -9 vodsl_monitor
		  let i=$i+1
		  usleep 100000
		done
		;;
	restart)
		if [ "`pidof vodsl_monitor`" != "" ]; then
		  $0 stop
		  usleep 400000
                fi
		$0 start
		;;
	*)
		echo "Usage: /etc/start_scripts/call_mon.sh {start|stop|restart}"
		exit 1
		;;
esac

exit 0
