#!/bin/sh
#set -x
#debug=echo

failsafe()
{
  echo "Failsafe...using old mount points without mini_fo"
  umount /proc >/dev/null 2>/dev/null
  umount /rom/proc >/dev/null 2>/dev/null
  umount /opt >/dev/null 2>/dev/null
  umount /rom/opt >/dev/null 2>/dev/null
  umount /rom >/dev/null 2>/dev/null
  umount /var >/dev/null 2>/dev/null
  umount /rom/var >/dev/null 2>/dev/null

  mount -t proc proc /proc 2>/dev/null >/dev/null 2>/dev/null
  mount -t jffs2 /dev/mtdblock/3 /opt >/dev/null 2>/dev/null
  mount -t tmpfs -o size=500k tmpfs /var >/dev/null 2>/dev/null
  
  exec /sbin/init < /dev/console > /dev/console 2> /dev/console
  exec /bin/sh
}


PATH=/sbin:/bin:/usr/bin:/usr/sbin
export PATH

mount -t proc proc /proc

#dynamically create device-nodes because we use devfs
mkdir -p /dev/pts
mknod /dev/pts/0 c 2 0
mknod /dev/pts/1 c 2 1
mknod /dev/pts/2 c 2 2
mknod /dev/pts/3 c 2 3
mknod /dev/pts/4 c 2 4
mknod /dev/pts/5 c 2 5
mknod /dev/pts/6 c 2 6
mknod /dev/pts/7 c 2 7
mknod /dev/pts/8 c 2 8
mknod /dev/pts/9 c 2 9
mknod /dev/ptmx c 5 2
mknod /dev/ptyp0 c 2 0
mknod /dev/ptyp1 c 2 1
mknod /dev/ptyp2 c 2 2
mknod /dev/ptyp3 c 2 3
mknod /dev/ptyp4 c 2 4
mknod /dev/ptyp5 c 2 5
mknod /dev/ptyp6 c 2 6
mknod /dev/ptyp7 c 2 7
mknod /dev/ptyp8 c 2 8
mknod /dev/ptyp9 c 2 9

mknod /dev/ttyp0 c 3 0
mknod /dev/ttyp1 c 3 1
mknod /dev/ttyp2 c 3 2
mknod /dev/ttyp3 c 3 3
mknod /dev/ttyp4 c 3 4
mknod /dev/ttyp5 c 3 5
mknod /dev/ttyp6 c 3 6
mknod /dev/ttyp7 c 3 7
mknod /dev/ttyp8 c 3 8
mknod /dev/ttyp9 c 3 9

mknod /dev/tty0 c 4 0
mknod /dev/tty1 c 4 1
mknod /dev/tty2 c 4 2
mknod /dev/tty3 c 4 3
mknod /dev/tty4 c 4 4
mknod /dev/tty5 c 4 5
mknod /dev/tty6 c 4 6
mknod /dev/tty7 c 4 7
mknod /dev/tty8 c 4 8
mknod /dev/tty9 c 4 9
mknod /dev/ttyS0 c 4 64

mknod /dev/bcmatm0 c 205 0
mknod /dev/brcmboard c 206 0
mknod /dev/bcmvdsl0 c 207 0
mknod /dev/bcmadsl0 c 208 0
mknod /dev/bcmendpoint0 c 209 0
mknod /dev/bcmaal20 c 210 0
mknod /dev/bcmles0 c 211 0
mknod /dev/bcm c 212 0

#check if nvram is ok, if not reset it
/bin/nvram show >/dev/null
if [ $? -ne 0 ]; then
  echo "Resetting NVRAM"
  /bin/nvram reset >/dev/null
fi


mini_fo=`nvram get mini_fo`
[ "$mini_fo" = "Failed" ] && mini_fo="jffs"

if [ `nvram get jffs_clear` = "1" ]; then
  echo -n "Force jffs erase..."
  mtd_erase /dev/mtd/3 >/dev/null 2>/dev/null
  nvram del jffs_clear >/dev/null 2>/dev/null
  nvram commit >/dev/null
  echo "Done"
fi


jffs_failed=0
mount -t jffs2 /dev/mtdblock/3 /opt >/dev/null 2>/dev/null
if [ $? -ne 0 ]; then
        echo "Failed."
        echo -n "Erasing and remounting jffs-partition..."
	mtd_erase /dev/mtd/3 >/dev/null 2>/dev/null
	mount -t jffs2 /dev/mtdblock/3 /opt >/dev/null 2>/dev/null
	if [ $? -ne 0 ]; then
	  echo "Failed."
	  jffs_failed=1
	  mini_fo="ram"
        fi
fi

echo -n "Mounting tmpfs at /var..." 
mount -t tmpfs -o size=500k tmpfs /var >/dev/null 2>/dev/null
if [ $? -ne 0 ]; then
    echo "Failed."
    failsafe
fi
echo "Ok."


if [ "$mini_fo" = "ram" ]; then
  echo "Using RAMFS for storage"
  
  mkdir -p /var/mini_fo
  echo -n "merging / and /var/mini_fo... "
  mount -t mini_fo -o base=/,sto=/var/mini_fo mini_fo /rom >/dev/null 2>/dev/null
  if [ $? -ne 0 ]; then
    echo "Failed."
    failsafe
  fi
  echo "Ok."
elif [ "$mini_fo" = "jffs" ]; then
  
  if [ `nvram get mini_fo_clear` = "1" ]; then
    echo -n "Force mini_fo erase..."
    rm -rf /opt/mini_fo >/dev/null 2>/dev/null
    nvram del mini_fo_clear >/dev/null 2>/dev/null
    nvram commit >/dev/null
    echo "Done"
  fi

  echo "Using JFFS-partition for storage"
  
  mkdir -p /opt/mini_fo
  echo -n "merging / and /opt/mini_fo... "
  mount -t mini_fo -o base=/,sto=/opt/mini_fo mini_fo /rom >/dev/null 2>/dev/null
  if [ $? -ne 0 ]; then
    echo "Failed."
    failsafe
  fi
  echo "Ok."
else
  failsafe
fi

echo -n "Pivoting to mini_fo..."

mount -o move /proc /rom/proc >/dev/null 2>/dev/null
pivot_root /rom /rom/rom >/dev/null 2>/dev/null
[ $jffs_failed -eq 0 ] && mount -o move /rom/opt /opt 2>/dev/null
mount -o move /rom/var /var 2>/dev/null
mount -o move /rom/dev /dev 2>/dev/null

echo "Ok."


exec /sbin/init < /dev/console > /dev/console 2> /dev/console
exec /bin/sh
