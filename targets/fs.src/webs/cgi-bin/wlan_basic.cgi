#!/bin/sh

from=wlan_basic.cgi
action=0

. ./login.cgi

/etc/start_scripts/wlan_present.sh
if [ $? -ne 0 ]; then
# WLAN hardware is not present!
cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY>
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>WLAN configuration</DIV>
<DIV id=c_title></div>
<DIV id=c_std><b style="color: rgb(255, 17, 17);">WLAN hardware is not present!</b></div>
<DIV id=c_leer></DIV>
</DIV>
<DIV id="c_foot"></DIV>
<DIV id=i_Frame>
</DIV>
</DIV></BODY></HTML>
HTML
exit 0
fi

f_pfad="WLAN Settings / Basic settings"
f_name="Basic Settings"
HOME="wlan_index.cgi"
. ./functions.sh

PATH=.:$PATH

errmsg=""
mustsave=0
####### Save & Run #######
if [ "$action" -eq 2 ] || [ "$action" -eq 3 ]; then

  [ "$form_wl_up" = "0" ] && wl_up="0" || wl_up="1"
  if [ "$sel_mode" = "adhoc" ]; then 
    wl_infra="0"
    wl_ap="0"
  else 
    wl_infra="1"
    [ "$sel_mode" = "ap" ] && wl_ap="1" || wl_ap="0"
  fi
  http_unquote_all "text_essid"
  [ "$form_wl_close" = "1" ] && wl_close="1" || wl_close="0" 
  [ "$form_wl_wet" = "0" ] && wl_wet="0" || wl_wet="1"

  [ "$wl_up" != "`nvram get wl_up`" ] && res=`nvram set wl_up=$wl_up` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$wl_infra" != "`nvram get wl_infra`" ] && res=`nvram set wl_infra=$wl_infra` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$wl_ap" != "`nvram get wl_ap`" ] && res=`nvram set wl_ap=$wl_ap` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$sel_gmode" != "`nvram get wl_gmode`" ] && res=`nvram set wl_gmode=$sel_gmode` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$text_essid" != "`nvram get wl_essid`" ] && res=`nvram set wl_essid="$text_essid"` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$sel_channel" != "`nvram get wl_channel`" ] && res=`nvram set wl_channel=$sel_channel` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$wl_close" != "`nvram get wl_close`" ] && res=`nvram set wl_close=$wl_close` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$wl_wet" != "`nvram get wl_wet`" ] && res=`nvram set wl_wet=$wl_wet` && mustsave=1 && [ "$res" != "OK" ] && err=1
  if [ "$wl_wet" = "0" ]; then 
    myerr=0
    if [ "$check_dhcp" = "1" ]; then
      wl_ipaddr="dhcp"
      wl_ipmask=`nvram get wl_ipmask`
      [ "$wl_ipmask" = "Failed" ] && wl_ipmask="255.255.255.0"
    else
      wl_ipaddr="$text_ip"
      wl_ipaddr=`isip.sh $wl_ipaddr`
      [ "$wl_ipaddr" = "ERROR" ] && errmsg="$errmsg Invalid IP-address!"'\n' && myerr=1
      wl_ipmask="$text_mask"
      wl_ipmask=`isip.sh $wl_ipmask`
      [ "$wl_ipmask" = "ERROR" ] && errmsg="$errmsg Invalid subnet-mask!"'\n' && myerr=1
      wl_gw="$text_gw"
      if [ "$wl_gw" != "" ]; then
        wl_gw=`isip.sh $wl_gw`
        [ "$wl_gw" = "ERROR" ] && errmsg="$errmsg Invalid gateway address!"'\n' && myerr=1
      fi
    fi
    if [ "$myerr" -ne 1 ]; then
      [ "$wl_ipaddr" != "`nvram get wl_ipaddr`" ] && res=`nvram set wl_ipaddr=$wl_ipaddr` && mustsave=1 && [ "$res" != "OK" ] && err=1
      [ "$wl_ipmask" != "`nvram get wl_ipmask`" ] && res=`nvram set wl_ipmask=$wl_ipmask` && mustsave=1 && [ "$res" != "OK" ] && err=1
      [ "$wl_gw" != "`nvram get wl_gw`" ] && res=`nvram set wl_gw=$wl_gw` && mustsave=1 && [ "$res" != "OK" ] && err=1
    fi
   fi

  [ "$err" -ne 0 ] && errmsg="$errmsg Write to NVRam failed!"'\n'
  
  [ "$mustsave" -eq 1 ] && nvram commit >/dev/null && [ "$action" -eq 3 ] && /etc/start_scripts/wlan.sh restart >/dev/null 2>/dev/null
fi
#############

set_select()
{
  var=$1
  default=$2
  value=`nvram get $var`
  [ "$value" = "Failed" ] && value=$default 
  if [ $value -eq 1 ]; then
    eval `echo ${var}_on=$ON`
    eval `echo ${var}_off=''`
  else
    eval `echo ${var}_on=''`
    eval `echo ${var}_off=$ON`
  fi
}

set_select2()
{
  var=$1
  value=$2
  [ "$value" = "" ] && return 1
  if [ $value -eq 1 ]; then
    eval `echo ${var}_on=$ON`
    eval `echo ${var}_off=''`
  else
    eval `echo ${var}_on=''`
    eval `echo ${var}_off=$ON`
  fi
}

ON='checked=\"checked\"'
SEL="selected"
#get wl mac adress
wl0_mac=`ifconfig wl0 2>/dev/null|grep HWaddr|awk {'print $5;'}`
[ "$wl0_mac" = "" ] && wl0_mac="not in use"



#########on change###########
if [ "$action" -eq  5 ]; then
  #select if wlan is up or down
  set_select2 "wl_up" $form_wl_up
  #get wlan mode
  [ "$sel_mode" = "adhoc" ] && sel_mode_adhoc=$SEL
  [ "$sel_mode" = "ap" ] && sel_mode_ap=$SEL
  [ "$sel_mode" = "client" ] && sel_mode_client=$SEL
  #get wlan network mode
  case "$sel_gmode" in
                0)
                    sel_gmode_bonly=$SEL
                    ;;
                1)
                    sel_gmode_mixed=$SEL
                    ;;
                2)
                    sel_gmode_gonly=$SEL
                    ;;
                3)
                    sel_gmode_gperf=$SEL
                    ;;
                *)
                    ;;
  esac
  #get essid
  essid=$text_essid
  #get channel
  channel=$sel_channel
  #get network hiding
  [ "$form_wl_close" = "" ] && set_select "wl_close" 0 || set_select2 "wl_close" "$form_wl_close"
  #get bridging
  set_select2 "wl_wet" $form_wl_wet
  
  if [ "$sel_mode_ap" = "" ] && [ "$wl_wet_on" = "" ]; then
    if [ "$check_dhcp" = "" ]; then
      var=`nvram get wl_ipaddr`
      [ "$var" = "dhcp" ] && check_dhcp_on="1"
    else
      check_dhcp_on="$check_dhcp"
    fi
    if [ "$check_dhcp" = "1" ]; then 
      wl_ipaddr=""
      wl_ipmask=""
      wl_gw=""
    else
      wl_ipaddr="$text_ip"
      wl_ipmask="$text_mask"
      wl_gw="$text_gw"
    fi
  fi
else
  #select if wlan is up or down
  set_select "wl_up" 1
  #get wlan mode
  var=`nvram get wl_infra`
  [ "$var" = "Failed" ] && var=1
  if [ "$var" = "0" ]; then
    sel_mode_adhoc=$SEL
  else
    var=`nvram get wl_ap`
    [ "$var" = "Failed" ] && var=1
    [ "$var" = "1" ] && sel_mode_ap=$SEL || sel_mode_client=$SEL
  fi
  #get wlan network mode
  var=`nvram get wl_gmode`
  [ "$var" = "Failed" ] && var=1
  case "$var" in
                0)
                    sel_gmode_bonly=$SEL
                    ;;
                1)
                    sel_gmode_mixed=$SEL
                    ;;
                2)
                    sel_gmode_gonly=$SEL
                    ;;
                3)
                    sel_gmode_gperf=$SEL
                    ;;
                *)
                    ;;
  esac
  #get essid
  essid=`nvram get wl_essid`
  [ "$essid" = "Failed" ] && essid="BITSWITCHER"
  #get channel
  channel=`nvram get wl_channel`
  [ "$channel" = "Failed" ] && channel="1"
  #get network hiding
  set_select "wl_close" 0
  #get bridging
  set_select "wl_wet" 1
  
  #get ip-settings if not ap-mode
  if [ "$wl_wet_on" = "" ]; then
    wl_ipaddr=`nvram get wl_ipaddr`
    [ "$wl_ipaddr" = "Failed" ] && wl_ipaddr="192.168.10.1"
    if [ "$wl_ipaddr" = "dhcp" ] && [ "$sel_mode_ap" = "" ]; then 
      check_dhcp_on=$ON
      wl_ipaddr=""
      wl_ipmask=""
      wl_gw=""
    else
      wl_ipmask=`nvram get wl_ipmask`
      [ "$wl_ipmask" = "Failed" ] && wl_ipmask="255.255.255.0"
      wl_gw=`nvram get wl_gw`
      [ "$wl_gw" = "Failed" ] && wl_gw=""
    fi
  fi
fi

cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function showErr() {
  var err="$errmsg";
  if(err!=""){alert(err);}
}
function dhcp_change() {
  if(document.getElementById("check_dhcp").checked==true){
    document.getElementById("text_ip").disabled=true;
    document.getElementById("text_mask").disabled=true;
    document.getElementById("text_gw").disabled=true;
  }
  else{
    document.getElementById("text_ip").disabled=false;
    document.getElementById("text_mask").disabled=false;
    document.getElementById("text_gw").disabled=false;
}
}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();dhcp_change();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>$f_pfad</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<DIV id=c_titel>$f_name - device wl0 HwAddr: $wl0_mac</DIV>
<DIV id=c_leer>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0) >
<td width="20"><input type="radio" name="form_wl_up" value="0" $wl_up_off></td>
<td width="40">Off</td>
<td width="20"><input type="radio" name="form_wl_up" value="1" $wl_up_on></td>
<td>On</td>
</tr></table>
</DIV>
<DIV id=c_titel>Settings</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0)>
<td width="150">WLAN mode</td>
<td>
  <select id="sel_mode" name="sel_mode" size="1" onchange="subm(5);">
    <option value="ap" $sel_mode_ap>AP</option>
    <option value="client" $sel_mode_client>Client</option>
    <option value="adhoc" $sel_mode_adhoc>AdHoc</option>
  </select>
</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0)>
<td width="150">WLAN network mode</td>
<td>
  <select name="sel_gmode" size="1">
    <option value="0" $sel_gmode_bonly>B-only</option>
    <option value="1" $sel_gmode_mixed>Mixed</option>
    <option value="2" $sel_gmode_gonly>G-Only</option>
    <option value="3" $sel_gmode_gperf>G-Performance</option>
  </select>
</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpaddig="0" cellspacing="2">
<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0) >
<td width="150">Network name (ESSID):</td>
<td><input id="essid" name="text_essid" size="25" maxlength="70" type="text" value="$essid"></td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(5) onmouseout=i_showElem(0)>
<td width="150">WLAN channel</td>
<td>
  <select name="sel_channel" size="1">
HTML

freq=2412
sel=""
for  i in 1 2 3 4 5 6 7 8 9 10 11 12 13 14
do
   freq_print=`echo $freq|sed -e 's/^./&./g'`
   [ "$i" = "$channel" ] && sel="selected"
   echo "    <option value=\"$i\" $sel>$i - $freq_print GHz</option>"
   sel=""
   let "freq+=5"
done

cat <<HTML

  </select>
</td>
</tr></table></DIV>
HTML

if [ "$sel_mode_ap" = $SEL ]; then
  echo '<DIV id=c_std>' \
  '<table border="0" cellpadding="0" cellspacing="2">' \
  '<tr onmouseover=i_showElem(5) onmouseout=i_showElem(0)><td width="150">Hide the network</td>'
  echo "<td width=\"20\"><input type=\"radio\" id=\"form_wl_close\" name=\"form_wl_close\" value=\"0\" $wl_close_off></td>"
  echo '<td width="40">Off</td>'
  echo "<td width=\"20\"><input type=\"radio\" id=\"form_wl_close\" name=\"form_wl_close\" value=\"1\" $wl_close_on></td>"
  echo '<td>On</td>' \
  '</tr></table>' \
  '</DIV>'
fi

cat <<HTML
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(6) onmouseout=i_showElem(0)><td width="150">Device configuration</td>
<td width="20"><input type="radio" name="form_wl_wet" value="0" $wl_wet_off onchange="subm(5);"></td>
<td width="40">seperate</td>
<td width="20"><input type="radio" name="form_wl_wet" value="1" $wl_wet_on onchange="subm(5);"></td>
<td>bridged</td>
</tr></table>
</DIV>
HTML

if [ "$wl_wet_on" = "" ]; then
  if [ "$sel_mode_ap" = "" ]; then
    echo '<DIV id=c_std>' \
    '<table border="0" cellpadding="0" cellspacing="2">' \
    '<tr onmouseover=i_showElem(7) onmouseout=i_showElem(0)><td width="150">Get IP-address via DHCP</td>'
    echo "<td><input type=\"checkbox\" id=\"check_dhcp\" name=\"check_dhcp\" value=\"1\" $check_dhcp_on onchange=\"dhcp_change();\"></td>"
    echo '</tr></table></DIV>'
  fi
  echo '<DIV id=c_std>' \
  '<table border="0" cellpadding="0" cellspacing="2">' \
  '<tr onmouseover=i_showElem(8) onmouseout=i_showElem(0)><td width="150">IP-adress</td>'
  echo "<td><input id=\"text_ip\" name=\"text_ip\" size=\"15\" maxlength=\"15\" type=\"text\" value=\"$wl_ipaddr\"></td>"
  echo '</tr></table></DIV>'
  echo '<DIV id=c_std>' \
  '<table border="0" cellpadding="0" cellspacing="2">' \
  '<tr onmouseover=i_showElem(9) onmouseout=i_showElem(0)><td width="150">Subnet mask</td>'
  echo "<td><input id=\"text_mask\" name=\"text_mask\" size=\"15\" maxlength=\"15\" type=\"text\" value=\"$wl_ipmask\"></td>"
  echo '</tr></table></DIV>'
  echo '<DIV id=c_std>' \
  '<table border="0" cellpadding="0" cellspacing="2">' \
  '<tr onmouseover=i_showElem(10) onmouseout=i_showElem(0)><td width="150">Standard gateway</td>'
  echo "<td><input id=\"text_gw\" name=\"text_gw\" size=\"15\" maxlength=\"15\" type=\"text\" value=\"$wl_gw\"></td>"
  echo '</tr></table></DIV>'
fi

cat <<HTML
</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>WLAN Basic</h2>
<p>Configure basic wireless settings.</p>
</DIV>
<DIV id=i_content1>
<h2>WLAN</h2>
<p>Turn the wireless device <b>on</b> or <b>off</b>.</p>
</DIV>
<DIV id=i_content2>
<h2>WLAN mode</h2>
<p>Select the mode the WLAN-device should operate.</p>
</DIV>
<DIV id=i_content3>
<h2>WLAN network mode</h2>
<p>Select the network mode in which the device should work.</p>
</DIV>
<DIV id=i_content5>
<h2>WLAN channel</h2>
<p>Set the WLAN channel to use.</p>
</DIV>
<DIV id=i_content4>
<h2>Hide the network</h2>
<p>Select to broadcast the ESSID or not (hide).</p>
</DIV>
<DIV id=i_content6>
<h2>Device configuration</h2>
<p>Set device to <b>bridge</b> or <b>seperate</b> mode.</p>
</DIV>
<DIV id=i_content7>
<h2>Get IP-address via DHCP</h2>
<p>Enable to get IP-Adress on WLAN-interface automatically via DHCP.</p>
</DIV>
<DIV id=i_content8>
<h2>IP-Adress</h2>
<p>Enter a static IP-address to use on WLAN-interface.</p>
</DIV>
<DIV id=i_content9>
<h2>Subnet mask</h2>
<p>Enter subnet mask corresponding with entered IP-address.</p>
</DIV>
<DIV id=i_content10>
<h2>Standard gateway</h2>
<p>Enter standard gateway to use.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> DNS-servers could be entered in LAN-settings.</p>
</DIV>
</DIV></BODY></HTML>
