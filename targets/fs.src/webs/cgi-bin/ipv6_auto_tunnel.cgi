#!/bin/sh

action=0
mustsave=0

. ./functions.sh
. ./login.cgi

HOME="ipv6_index.cgi"

ipv6_auto=`nvram get ipv6_6to4_auto`
[ "$ipv6_auto" = "Failed" ] && ipv6_auto=""

err=0

################################ Save & Run #################################
if [ "$action" -eq 3 ] || [ "$action" -eq 2 ]; then

  http_unquote local_addr
  http_unquote local_ipv6_addr
#  http_unquote local_ipv6_net
  http_unquote route


  if [ "$local_stat_dyn" = "0" ]; then
	[ "local_addr" = "" ] && errmsg="$errmsg Public IPv4 address not specified!"'\n' && err=1
	if [ "$err" = "0" ]; then
	is_ipaddr $local_addr
	ret=$?
	[ "$ret" != "0" ] && errmsg="$errmsg Public IPv4 address invalid (${local_addr})!"'\n' && err=1
	fi
  else
	[ "$device" = "" ] && errmsg="$errmsg Unknown error!"'\n' && err=1
	local_addr="dyn:${device}"
  fi
  
  if [ "$behind_nat" = "1" ]; then
    if [ "$local_nat_dyn" = "0" ]; then
      [ "local_nat_addr" = "" ] && errmsg="$errmsg Local IPv4 address not specified!"'\n' && err=1
      if [ "$err" = "0" ]; then
        is_ipaddr $local_nat_addr
        ret=$?
        [ "$ret" != "0" ] && errmsg="$errmsg Local IPv4 address invalid (${local_nat_addr})!"'\n' && err=1
      fi
    else
      [ "$device_nat" = "" ] && errmsg="$errmsg Unknown error!"'\n' && err=1
      local_nat_addr="dyn:${device_nat}"
    fi
  fi

   [ "$local_ipv6_net" = "" ] && errmsg="$errmsg Local IPv6 subnet not specified!"'\n' && err=1
   if [ "$err" = "0" ]; then
     h=`echo "$local_ipv6_net"| sed -e 's/[^0-9a-fA-F]//g'`
     [ "$h" != "$local_ipv6_net" ] && errmsg="$errmsg Local IPv6 subnet contains invalid characters!"'\' && err=1
     [ "`expr length $local_ipv6_net`" -gt 4 ] && errmsg="$errmsg Local IPv6 subnet is too long!"'\n' && err=1
   fi

  [ "$route" = "" ] && errmsg="$errmsg Route not specified!"'\n' && err=1
  if [ "$err" = "0" ]; then
    is_ipv6_addr_with_prefix $route
    ret=$?
    [ "$ret" != "0" ] && ipv6_errmsg=`get_ipv6_err_string $ret` && errmsg="$errmsg Invalid route (${route} -- $ipv6_errmsg)!"'\n' && err=1
  fi

  if [ "$metric" != "" ]; then
    isNumeric "$metric"
    if [ "$?" -eq "0" ]; then
      [ "$metric" -lt 0 ] && errmsg="$errmsg Metric is invalid ($metric)!"'\n' && err=1
    else
      errmsg="$errmsg Metric is invalid ($metric)!"'\n'
      err=1
    fi 
  fi

  isNumeric "$ttl"
  if [ "$?" -eq "0" ]; then
    [ "$ttl" -lt 1 ] && errmsg="$errmsg TTL is too small ($ttl) set it to 64!"'\n' && ttl=64
    [ "$ttl" -gt 255 ] && errmsg="$errmsg TTL is too large ($ttl) set it to 64!"'\n' && ttl=64
  else
    errmsg="$errmsg TTL is invalid ($ttl)!"'\n'
    err=1
  fi 

  isNumeric "$mtu"
  if [ "$?" -eq "0" ]; then
    [ "$mtu" -lt 1280 ] && errmsg="$errmsg MTU is too small ($ttl) set it to 1280!"'\n' && mtu=1280
    [ "$mtu" -gt 1500 ] && errmsg="$errmsg MTU is too large ($ttl) set it to 1280!"'\n' && mtu=1280
  else
    errmsg="$errmsg MTU is invalid ($mtu)!"'\n'
    err=1
  fi 
  
  isNumeric "$rad_lifetime"
  if [ "$?" -eq "0" ]; then
      [ "$rad_lifetime" -lt 0 ] && errmsg="$errmsg Valid lifetime is invalid ($rad_lifetime) set it to 2592000!"'\n' && rad_lifetime="2592000"
      
      [ "$rad_lifetime" -gt 4294967295 ] && errmsg="$errmsg Valid lifetime is invalid ($rad_lifetime) set it to 2592000!"'\n' && rad_lifetime="2592000"
  else
      errmsg="$errmsg Valid lifetime is invalid ($rad_lifetime) set it to 2592000!"'\n'
      rad_lifetime="2592000"
  fi 
    
  isNumeric "$rad_preftime"
  if [ "$?" -eq "0" ]; then
    [ "$rad_preftime" -lt 0 ] && errmsg="$errmsg Preferred lifetime is invalid ($rad_preftime) set it to 604800!"'\n' && rad_preftime="604800"
    
    [ "$rad_preftime" -gt 4294967295 ] && errmsg="$errmsg Preferred lifetime is invalid ($rad_preftime) set it to 604800!"'\n' && rad_preftime="604800"
  else
      errmsg="$errmsg Preferred lifetime is invalid ($rad_preftime) set it to 604800!"'\n'
      rad_preftime="604800"
  fi

  [ "$advertise" != "1" ] &&  advertise="0"
  
  case $route_dev in
  0) route_dev="br0" ;;
  1) route_dev="wl0" ;;
  *) route_dev="br0" ;;
  esac
  
  if [ "$err" = "0" ]; then
  ipv6_auto="$advertise,$local_addr,$local_ipv6_net,$route,$ttl,$metric,1,$rad_mobile,$rad_auto,$rad_lifetime,$rad_preftime,$route_dev,$route_metric,$behind_nat,$local_nat_addr,$mtu"
    
    
    res="OK"
    [ "$ipv6_auto" != "`nvram get ipv6_6to4_auto`" ] && res=`nvram set ipv6_6to4_auto=$ipv6_auto` && mustsave=1
    
    
    [ "$auto_tun_start" != "`nvram get ipv6_6to4_auto_start`" ] && res=`nvram set ipv6_6to4_auto_start=$auto_tun_start` && mustsave=1

    [ "$res" != "OK" ] && errmsg="$errmsg Write to NVRam failed!"'\n'
  fi
  
  [ "$action" -eq 3 ] && /etc/start_scripts/ipv6.sh restart >/dev/null 2>/dev/null
  [ "$mustsave" = "1" ] && nvram commit >/dev/null
fi

##############################################


ipv6_auto=`nvram get ipv6_6to4_auto`
[ "$ipv6_auto" = "Failed" ] && ipv6_auto=""

if [ "$ipv6_auto" != "" ]; then
  eval `echo "$ipv6_auto" | \
  awk '{
        split($0,a,",");
        
        # ToDo: static and dynamic devices
        
        split("advertise,local_addr,local_ipv6_net,route,ttl,metric,on_link,rad_mobile,rad_auto,rad_lifetime,rad_preftime,route_dev,route_metric,behind_nat,local_nat_addr,mtu",b,",");
        
        for(i=1; i<=16; i++)
        {
          print b[i] "=\"" a[i] "\"; ";
        }
      }'`
else

  advertise=1
  local_addr=""
  local_ipv6_net="1"
  route_dev="br0"
  route="::/0"
  ttl=64
  mtu=1280
  rad_mobile=0
  rad_lifetime=25
  rad_preftime=15
  rad_auto=1
  metric=""
  route_metric=""
  behind_nat=0
  local_nat_addr=""
fi

sel='checked="checked"'
SEL='selected="selected"'
[ "$advertise" = "1" ] && advertise_sel=$sel || advertise_sel=""
[ "$rad_mobile" = "1" ] && rad_mobile_sel=$sel || rad_mobile_sel=""
[ "$rad_auto" = "1" ] && rad_auto_sel=$sel || rad_auto_sel=""
[ "$behind_nat" = "1" ] && behind_nat_sel=$sel || behind_nat_sel=""


auto_tun_start_off=$sel
auto_tun_start_on=""
auto_tun_start=`nvram get ipv6_6to4_auto_start`
[ "$auto_tun_start" = "Failed" ] && auto_tun_start=0
[ "$auto_tun_start" = "1" ] && auto_tun_start_on=$sel && auto_tun_start_off=""


local_addr_stat_sel=$sel
local_addr_dyn_sel=""
[ "`expr substr $local_addr 1 4`" = "dyn:" ] && local_addr_dyn_sel=$sel && local_addr_stat_sel=""
if [ "$local_addr_dyn_sel" != "" ]; then
  case "$local_addr" in
   "dyn:0") device_br_sel=$SEL ;;
   "dyn:1") device_wl_sel=$SEL ;;
   "dyn:2") device_ppp_sel=$SEL ;;
   *) device_br_sel=$SEL ;;
  esac
  local_addr=""
fi

local_nat_stat_sel=$sel
local_nat_dyn_sel=""
[ "`expr substr $local_nat_addr 1 4`" = "dyn:" ] && local_nat_dyn_sel=$sel && local_nat_stat_sel=""
if [ "$local_nat_dyn_sel" != "" ]; then
  case "$local_nat_addr" in
    "dyn:0") device_nat_br_sel=$SEL ;;
    "dyn:1") device_nat_wl_sel=$SEL ;;
    *) device_nat_br_sel=$SEL ;;
  esac
  local_nat_addr=""
fi

case "$route_dev" in
  "br0") route_dev_br_sel=$SEL ;;
  "bl") route_dev_wl_sel=$SEL ;;
  *) route_dev_br_sel=$SEL ;;
esac

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function showErr() {
var err='$errmsg';
if(err!=""){alert(err);} }
function js_local(state) {
 if(state) {
  document.getElementById("local_addr").style.visibility="hidden";
  document.getElementById("device").style.visibility="visible";
 }
 else {
  document.getElementById("local_addr").style.visibility="visible";
  document.getElementById("device").style.visibility="hidden";
 }
}
function js_nat(state) {
 if(state) {
  document.getElementById("local_nat_addr").style.visibility="hidden";
  document.getElementById("device_nat").style.visibility="visible";
 }
 else {
  document.getElementById("local_nat_addr").style.visibility="visible";
  document.getElementById("device_nat").style.visibility="hidden";
 }
}
function js_nat_sel(state) {
 if(!state) {
  document.getElementById("local_nat_dyn").disabled=true;
  document.getElementById("local_nat_dyn2").disabled=true;
  document.getElementById("local_nat_addr").disabled=true;
  document.getElementById("device_nat").disabled=true;
 }
 else {
  document.getElementById("local_nat_dyn").disabled=false;
  document.getElementById("local_nat_dyn2").disabled=false;
  document.getElementById("local_nat_addr").disabled=false;
  document.getElementById("device_nat").disabled=false;
 }
}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();js_nat_sel('`expr substr $behind_nat_sel 1 1 `'!='');js_nat('`expr substr $local_nat_dyn_sel 1 1 `'!='');js_local('`expr substr $local_addr_dyn_sel 1 1 `'!='');">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>IPv6 settings / Automatic IPv6 6to4 tunnel</DIV>
<DIV id=c_titel>Automatic 6to4 tunnel</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<input type="hidden" name="mustsave" value="$mustsave">
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0) >
<td width="140">Automatic 6to4:</td>
<td width="20"><input type="radio" name="auto_tun_start" value="0" $auto_tun_start_off></td>
<td width="100">off</td>
<td width="20"><input type="radio" name="auto_tun_start" value="1" $auto_tun_start_on></td>
<td>on</td>
</td></tr>
</table>
</div>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0) >
<td width="140">Public IPv4 address:</td>
<td width="20"><input type="radio" name="local_stat_dyn" value="0" $local_addr_stat_sel onchange="js_local(false);"></td>
<td width="100">static address</td>
<td width="20"><input type="radio" name="local_stat_dyn" value="1" $local_addr_dyn_sel onchange="js_local(true);"></td>
<td>address from device</td>
</td></tr>
</table>
</div>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0) >
<td width="140"></td>
<td><input type="input" name="local_addr" id='local_addr' size="17" maxlength="15" value="$local_addr"></td>
<td><select style="visibility:hidden" name="device" id="device" size="1">
<option value="0" $device_br_sel>Bridge/LAN</option>
<option value="1" $device_wl_sel>WLAN</option>
<option value="2" $device_ppp_sel>PPP0</option>
</select></td></tr>
</table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0) >
<td width="140">Behind NAT:</td><td>
<input type="checkbox" name="behind_nat" $behind_nat_sel value ="1" onchange="js_nat_sel(this.checked);"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0) >
<td width="140">Local IPv4 address:</td>
<td width="20"><input type="radio" name="local_nat_dyn" id="local_nat_dyn" value="0" $local_nat_stat_sel onchange="js_nat(false);"></td>
<td width="100">static address</td>
<td width="20"><input type="radio" name="local_nat_dyn" id="local_nat_dyn2" value="1" $local_nat_dyn_sel onchange="js_nat(true);"></td>
<td>address from device</td>
</td></tr>
</table>
</div>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0) >
<td width="140"></td>
<td><input type="input" name="local_nat_addr" id='local_nat_addr' size="17" maxlength="15" value="$local_nat_addr"></td>
<td><select style="visibility:hidden" name="device_nat" id="device_nat" size="1">
<option value="0" $device_nat_br_sel>Bridge/LAN</option>
<option value="1" $device_nat_wl_sel>WLAN</option>
</select></td></tr>
</table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(5) onmouseout=i_showElem(0) >
<td width="140">Local IPv6 subnet:</td>
<td>2002:aabb:ccdd:<input type="input" name="local_ipv6_net" size="5" maxlength="4" value="$local_ipv6_net">::/64</td>
</td></tr>
</table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(6) onmouseout=i_showElem(0) >
<td width="140">Local device:</td>
<td><select name="route_dev" size="1">
<option value="0" $route_dev_br_sel>Bridge/LAN</option>
<option value="1" $route_dev_wl_sel>WLAN</option>
</select></td></tr>
</table>
</DIV>
<div id="c_leer"></div><DIV id=c_std></div>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(7) onmouseout=i_showElem(0) >
<td width="140">Route:</td>
<td><input type="input" name="route" size="40" maxlength="35" value="$route"></td>
</td></tr>
</table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(8) onmouseout=i_showElem(0) >
<td width="140">TTL:</td>
<td><input type="input" name="ttl" size="5" maxlength="3" value="$ttl"></td>
</td></tr>
</table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(16) onmouseout=i_showElem(0) >
<td width="140">MTU:</td>
<td><input type="input" name="mtu" size="5" maxlength="4" value="$mtu"></td>
</td></tr>
</table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(9) onmouseout=i_showElem(0) >
<td width="140">Advertise:</td><td>
<input type="checkbox" name="advertise" value="1" $advertise_sel></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(10) onmouseout=i_showElem(0) >
<td width="140">Mobile:</td><td>
<input type="checkbox" name="rad_mobile" value="1" $rad_mobile_sel></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(11) onmouseout=i_showElem(0) >
<td width="140">Auto configuration:</td><td>
<input type="checkbox" name="rad_auto" value="1" $rad_auto_sel></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(12) onmouseout=i_showElem(0) >
<td width="140">Valid lifetime:</td><td>
<input type="input" name="rad_lifetime" size="12" maxlength="10" value="$rad_lifetime"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(13) onmouseout=i_showElem(0) >
<td width="140">Preferred lifetime:</td><td>
<input type="input" name="rad_preftime" size="12" maxlength="10" value="$rad_preftime"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(14) onmouseout=i_showElem(0) >
<td width="140">Metric:</td>
<td><input type="input" name="metric" size="5" maxlength="5" value="$metric"></td>
</td></tr>
</table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(15) onmouseout=i_showElem(0) >
<td width="140">Metric local subnet:</td>
<td><input type="input" name="route_metric" size="5" maxlength="5" value="$route_metric"></td>
</td></tr>
</table>
</DIV>
</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>IPv6 6to4 tunnels</h2>
<p><b>Automatic IPv6 6to4 tunnel</b></p>
<p>Automatic IPv6 6to4 tunnel assign a 2002::/16 IPv6 address in combination with the
public IPv4 address to the router and uses public relay routers (192.88.99.1) to gain 
access to the IPv6 internet.</p>
<p>6to4 does not facilitate interoperation between IPv4-only hosts and 
IPv6-only hosts. 6to4 is simply a transparent mechanism used as a transport layer 
between IPv6 nodes.</p>
</DIV>
<DIV id=i_content1>
<h2>Automatic 6to4 tunnel</h2>
<p><b>Automatic 6to4</b></p>
<p>Turn automatic 6to4 tunnel on or off.</p>
</DIV>
<DIV id=i_content2>
<h2>Automatic 6to4 tunnel</h2>
<p><b>Public IPv4 address</b></p>
<p>Your public IPv4 address for the local tunnel endpoint. You can set a static IP address,
or choose a device to dynamically query the ip address to use.</p>
<p>Use the <i>address from device</i> option if you use a dynamically allocated
public IPv4 address from DHCP or PPP.</p>
</DIV>
<DIV id=i_content3>
<h2>Automatic 6to4 tunnel</h2>
<p><b>Behind NAT</b></p>
<p>This indicates if the router is behind a second NAT router.</p>
<p>6to4 can only work if the NAT router forwards <b>protocol 41</b> to this router!</p>
</DIV>
<DIV id=i_content4>
<h2>Automatic 6to4 tunnel</h2>
<p><b>Local IPv4 address</b></p>
<p>Your local IPv4 address for the local tunnel endpoint behind NAT. You can set a 
static IP address, or choose a device to dynamically query the ip address to use.</p>
<p>Use the <i>address from device</i> option if you use a dynamically allocated
public IPv4 address from DHCP.</p>
</DIV>
<DIV id=i_content5>
<h2>Automatic 6to4 tunnel</h2>
<p><b>Local IPv6 subnet</b></p>
<p>Add the prefix part of the local IPv6 subnet.</p>
<p>Values: 1 - FFFF</p>
<p>Default: 1</p>
</DIV>
<DIV id=i_content6>
<h2>Automatic 6to4 tunnel</h2>
<p><b>Local device</b></p>
<p>Specify the device the local IPv6 network is connected to.</p>
</DIV>
<DIV id=i_content7>
<h2>Automatic 6to4 tunnel</h2>
<p><b>Route</b></p>
<p>Set the route entry for this tunnel.</p>
<p>Example: ::/0 (all IPv6 traffic through the tunnel)</p>
</DIV>
<DIV id=i_content8>
<h2>Automatic 6to4 tunnel</h2>
<p><b>TTL</b></p>
<p>The TTL field is reduced by one on every hop. If it reach the value 0 
the packet will be droped.</p>
<p>Values: 1-255</p>
<p>Default: 64</p>
</DIV>
<DIV id=i_content9>
<h2>Automatic 6to4 tunnel</h2>
<p><b>Advertise</b></p>
<p>Set it to advertise this route for auto configuration.</p>
<p>Default: on</p>
</DIV>
<DIV id=i_content10>
<h2>Automatic 6to4 tunnel</h2>
<p><b>Mobile</b></p>
<p>When set, indicates that in router advertisements the address of interface 
is sent instead of network 
prefix, as is required by Mobile IPv6. When set, minimum limits specified by 
Mobile IPv6 are used for MinRtrAdvInterval and MaxRtrAdvInterval. </p>
<p>Default: off</p>
</DIV>
<DIV id=i_content11>
<h2>Automatic 6to4 tunnel</h2>
<p><b>Auto configuration</b></p>
<p>When set, indicates that this prefix can be used for autonomous address 
configuration as specified in RFC 2462. </p>
<p>Default: on</p>
</DIV>
<DIV id=i_content12>
<h2>Automatic 6to4 tunnel</h2>
<p><b>Valid lifetime</b></p>
<p>The length of time in seconds (relative to the time the packet is sent) 
that the prefix is valid for the purpose of on-link determination. The symbolic 
value infinity represents infinity (i.e. a value of all one bits 
(0xffffffff = 4294967295)).</p>
<p>Default: 25 seconds (for changing public ipv4 address)</p>
</DIV>
<DIV id=i_content13>
<h2>Automatic 6to4 tunnel</h2>
<p><b>Preferred lifetime</b></p>
<p>The length of time in seconds (relative to the time the packet is sent) 
that addresses generated from the prefix via stateless address autoconfiguration 
remain preferred. The symbolic value infinity represents infinity (i.e. a value 
of all one bits (0xffffffff = 4294967295)).</p>
<p>Default: 15 seconds (for changing public ipv4 address)</p>
</DIV>
<DIV id=i_content14>
<h2>Automatic 6to4 tunnel</h2>
<p><b>Metric</b></p>
<p>The metric for the route via the 6to4 tunnel. Let the metric field 
empty to use the default metric or
type a numeric value. The lower the metric, the higher is the route's priority.
</p>
<p>Values: >=0, empty</p>
<p>Default: empty</p>
</DIV>
<DIV id=i_content15>
<h2>Automatic 6to4 tunnel</h2>
<p><b>Metric local subnet</b></p>
<p>The metric for the route to the local subnet. 
Let the metric field empty to use the default metric or
type a numeric value. The lower the metric, the higher is the route's priority.
</p>
<p>Values: >=0, empty</p>
<p>Default: empty</p>
</DIV>
<DIV id=i_content16>
<h2>Automatic 6to4 tunnel</h2>
<b>MTU</b>
<p>The MTU of the auto 6to4 tunnel. </p>
<p>Values: 1280-1500</p>
<p>Default: 1280</p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML

a=0
