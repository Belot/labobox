#!/bin/sh

. ./login.cgi
. ./functions.sh
HOME="admin_index.cgi"


if [ "$action" = "3" ]; then
 len=${#passwd1}
 http_unquote passwd1
 http_unquote passwd2
 passwd1=`echo $passwd1|sed -e 's/"//g'`
 passwd2=`echo $passwd2|sed -e 's/"//g'`
 passwd1=`echo $passwd1|sed -e 's/\\\//g'`
 passwd2=`echo $passwd2|sed -e 's/\\\//g'`
 err=0
 ERROR=""
 [ "$passwd1" != "$passwd2" ] && ERROR="Passwords don't match!" && err=1
 [ ${#passwd1} -ne "$len" ] && ERROR=$ERROR"\nInvalid characters in password string!" && err=1

 if [ "$err" = "0" ]; then
  echo "root:${passwd1}"|chpasswd -m >/dev/null
  nvram setfile etc_passwd=/var/passwd >/dev/null
  nvram commit >/dev/null
  /etc/start_scripts/passwd.sh
 fi

 [ "$err" = "0" ] && ERROR=$ERROR"\nPassword changed." || ERROR=$ERROR"\nPassword is unchanged."
fi

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
<SCRIPT type="text/JavaScript">
function showErr() {
var err="$ERROR";
if(err!=""){alert(err);}
}
</SCRIPT>
</HEAD>
<BODY onload="showErr();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Administration / System passord</DIV>
<div id="c_titel">System password</div>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<div id="c_std">
<table border="0" cellpadding="4" cellspacing="0">
    <tr onmouseover="i_showElem(1)" onmouseout="i_showElem(0)"> <td width="150">System password:</td><td><input type="password" name="passwd1" size="15" maxlength="20"></td></td>
<td></td></tr></table>
</div>
<div id="c_std">
<table border="0" cellpadding="4" cellspacing="0">
    <tr onmouseover="i_showElem(2)" onmouseout="i_showElem(0)"> <td width="150">Confirm password:</td><td><input type="password" name="passwd2" size="15" maxlength="20"></td></td>
<td></td></tr></table>
</div>
<DIV id=c_leer></DIV>
<DIV id="c_foot"></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Run &lt;&lt;</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>System password</h2>
<p>Change the system password.</p>
</DIV>
<DIV id=i_content1>
<h2>System password</h2>
<p><b>System password</b><br>
Enter the new system password.</p>
<p>The password can contain letters, digits and some special characters ('+', '-', '.', '@')</p>
</DIV>
<DIV id=i_content2>
<h2>System passwor</h2>
<p><b>Confirm password</b><br>
Confirm the new system password.</p>
<p>The password can contain letters, digits and some special characters ('+', '-', '.', '@')</p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML

a=0
