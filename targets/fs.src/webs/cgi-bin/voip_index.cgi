#!/bin/sh

from=voip_index.cgi

. ./login.cgi
HOME="index.cgi"

voip_start=`/bin/nvram get voip_start`
voip_on='Off'
if [ "$voip_start" = "Failed" ]; then
  voip_start="0"
fi
[ "$voip_start" -eq "1" ] && voip_on='On'


cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
EOF_HTML

echo '<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>' \
    '<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>' \
    '<LINK rel="stylesheet" href="../style.css" type="text/css">' \
    '<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->' \
    '</HEAD>' \
    '<BODY onselectstart="return false">' \
    '<DIV id=c_Frame>' \
    '<DIV id=c_border>' \
    '<DIV id=c_pfad>Phone settings</DIV>' \
    '<DIV id=c_titel>VoIP settings</DIV>' \
    '<DIV id=c_link>' \
    '<table border="0" cellpadding="4" cellspacing="0">' \
    ' <tr>' \
    "	<td width=\"200\"><a href=\"voip_conf.cgi\" onmouseover=i_showElem(1) onmouseout=i_showElem(0) target=\"hcti\"><b>&gt;&gt; </b>VoIP settings</a></td>" \
    '<td>' \
    $voip_on \
    '</td> </tr>' \
    '</table>' \
    '</DIV>' \
    '<DIV id=c_linklast>' \
    '<table border="0" cellpadding="4" cellspacing="0">' \
    "<tr> <td width=\"200\"><a href=\"voip_options.cgi\" onmouseover=i_showElem(2) onmouseout=i_showElem(0) target=\"hcti\"><b>&gt;&gt; </b>Phone options</a></td>" \
    '</tr>  </table>' \
    '</DIV>' \
    '<DIV id="c_leer"></DIV>' \
    '<div id="c_titel">Dial settings</div>' \
    '<div id="c_link">' \
    '<table border="0" cellpadding="4" cellspacing="0">' \
    "<tr> <td width=\"200\"><a href=\"voip_emerg.cgi\" onmouseover=\"i_showElem(3)\" onmouseout=\"i_showElem(0)\" target=\"hcti\"><b>&gt;&gt; </b>Emergency call</a></td>" \
    '<td></td></tr></table>' \
    '</div>' \
    '<div id="c_link">' \
    '<table border="0" cellpadding="4" cellspacing="0">' \
    "<tr><td width=\"200\"><a href=\"voip_part.cgi\" onmouseover=\"i_showElem(4)\" onmouseout=\"i_showElem(0)\" target=\"hcti\"><b>&gt;&gt; </b>Dial rules</a></td>" \
    '</tr></table>' \
    '</div>' \
    '<div id="c_link">' \
    '<table border="0" cellpadding="4" cellspacing="0">' \
    "<tr><td width=\"200\"><a href=\"voip_quick.cgi\" onmouseover=\"i_showElem(5)\" onmouseout=\"i_showElem(0)\" target=\"hcti\"><b>&gt;&gt; </b>Quick dial</a></td>" \
    '</tr></table>' \
    '</div>' \
    '<DIV id="c_leer"></DIV>' \
    '<div id="c_titel">Call log</div>' \
    '<div id="c_link">' \
    '<table border="0" cellpadding="4" cellspacing="0">' \
    "<tr> <td width=\"200\"><a href=\"voip_call_log_in.cgi\" onmouseover=\"i_showElem(6)\" onmouseout=\"i_showElem(0)\" target=\"hcti\"><b>&gt;&gt; </b>Incoming calls</a></td>" \
    '<td></td></tr></table>' \
    '</div>' \
    '<div id="c_link">' \
    '<table border="0" cellpadding="4" cellspacing="0">' \
    "<tr><td width=\"200\"><a href=\"voip_call_log_out.cgi\" onmouseover=\"i_showElem(7)\" onmouseout=\"i_showElem(0)\" target=\"hcti\"><b>&gt;&gt; </b>Outgoing calls</a></td>" \
    '</tr></table>' \
    '</div>' 

if [ "$voip_start" -eq "1" ]; then
regstat=`cat /var/regstat 2>/dev/null`
confstat=`cat /var/voice/status 2>/dev/null`
status="not connected"
[ "$regstat" = "0" ] && [ "$confstat" = "1" ] && status="connection established"
cat << HTML_EOF
<DIV id="c_leer"></DIV>
<div id="c_titel">Status</div>
<div id="c_std">
    <table border="0" cellpadding="4" cellspacing="0">
    <tr><td width="200">VoIP connection:</td><td>$status</td>
    </tr></table>
</div>
HTML_EOF
fi


echo '<DIV id="c_foot"></DIV>' \
    '</DIV></DIV>' \
    '<DIV id=i_Frame>' \
    '<DIV id=i_content>' \
    '<h2>Phone settings</h2>' \
    '<p></p>' \
    '</DIV>' \
    '<DIV id=i_content1>' \
    '<h2>VoIP settings</h2>' \
    '<p><b>VoIP settings</b><br>' \
    'Configure your VoIP account.</p>' \
    '</DIV>' \
    '<DIV id=i_content2>' \
    '<h2>VoIP settings</h2>' \
    '<p><b>Phone options</b><br>' \
    'Set advanced phone options.</p>' \
    '</DIV>' \
    '<DIV id=i_content3>' \
    '<h2>Dial settings</h2>' \
    '<p><b>Emergency call</b><br>' \
    'Save emergency call phone numbers.</p>' \
    '</DIV>' \
    '<DIV id=i_content4>' \
    '<h2>Dial settings</h2>' \
    '<p><b>Dial rules</b><br>' \
    'Configure which connection (either PSTN or VoIP) should be used to call a specified phone number.</p>' \
    '</DIV>' \
    '<DIV id=i_content5>' \
    '<h2>Dial settings</h2>' \
    '<p><b>Quick dial</b><br>' \
    'Save some quick dial phone numbers.</p>' \
    '</DIV>' \
    '<DIV id=i_content6>' \
    '<h2>Call log</h2>' \
    '<p><b>Incoming calls</b><br>' \
    'Here is the logging for incoming calls.</p>' \
    '</DIV>' \
    '<DIV id=i_content7>' \
    '<h2>Call log</h2>' \
    '<p><b>Outgoing calls</b><br>' \
    'Here is the logging for outgoing calls.</p>' \
    '</DIV>' \
    '</DIV></BODY></HTML>'
