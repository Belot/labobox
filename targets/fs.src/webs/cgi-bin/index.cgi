#!/bin/sh

export PATH="/sbin:/bin:/usr/bin:/usr/sbin"
. ./functions.sh

cmd_info="/bin/adslctl info --stats"

/sbin/lsmod|grep adsldd > /dev/null
if [ $? -eq 0 ]; then
  dsl_upstream=`$cmd_info|grep Rate|awk '{print $4}'`
  dsl_downstream=`$cmd_info|grep Rate|awk '{print $3}'`
else
  dsl_upstream=""
  dsl_downstream=""
fi 

ON="On"
OFF="Off"
LEASE_FILE="/var/dnsmasq.leases"

voip_present=0
[ -f "/bin/vodsl" ] && voip_present=1

cpu_load=`cat /proc/loadavg |awk '{x=$2*100; print x;}'`

dsl_start=`nvram get dsl_start`
[ "$dsl_start" = "Failed" ] && dsl_start=0

ppp_start=`nvram get ppp_start`
[ "$ppp_start" = "Failed" ] && ppp_start=1
( [ "$ppp_start" = "1" ] || [ "$dsl_start" = "1" ] ) && wan_enabled=$ON || wan_enabled=$OFF

ppp_device=`nvram get ppp_device`
[ "$ppp_device" = "Failed" ] && ppp_device="dsl"

case "$ppp_device" in
                dsl)
                    ppp_device="DSL"
                    ;;
                lan)
                    ppp_device="LAN"
                    ;;
                wlan)
                    ppp_device="WLAN"
                    ;;
                *)
                    ppp_device="DSL"
                    ;;
esac

if ( [ "$dsl_start" = "1" ] || [ "$ppp_start" = "1" ] ); then

  wan_status=`adslctl info --state|grep Status|sed -e 's/Status://'`
  echo $wan_status|grep Showtime >/dev/null 2>/dev/null
  [ "$?" = "0" ] && wan_status="connected"
  
  [ "$wan_status" = "" ] && wan_status="Off"

  atm_vpi=`nvram get atm_vpi`
  [ "$atm_vpi" = "Failed" ] && atm_vpi=1
  atm_vci=`nvram get atm_vci`
  [ "$atm_vci" = "Failed" ] && atm_vci=32

  daemonstatus=`cat /proc/var/fyi/wan/ppp_${atm_vpi}_${atm_vci}_1/daemonstatus 2>/dev/null`
  wan_uptime=""
  if [ "$daemonstatus" -ge "0" ] && [ "$daemonstatus" -le "3" ]; then
    ppp_status="try to connect"
  elif [ "$daemonstatus" = "4" ]; then
    ppp_status="connected"
    wan_uptime=`cat /var/ppp_uptime 2>/dev/null`
    now=`/bin/date +%s`
    let diff=now-wan_uptime
    if [ $diff -gt 157680000 ]; then
      wan_uptime="unknown"
    else
      wan_uptime=`echo $now $wan_uptime|awk '{h=0; m=0; s=0; diff=$1-$2; if(int(diff/3600)>0){h=int(diff/3600); diff-=h*3600;} if(int(diff/60)>0){m=int(diff/60); diff-=m*60;  if(m<10){m="0"m;} } s=diff; if(s<10){s="0"s;}  print h":"m":"s;}'`
    fi
  elif [ "$daemonstatus" = "7" ]; then
    ppp_status="authentication failed"
  else
    ppp_status="down"
  fi

   [ "$daemonstatus" = "4" ] && wan_ip=`cat /proc/var/fyi/wan/ppp_${atm_vpi}_${atm_vci}_1/ipaddress 2>/dev/null`
fi

lan_ip=`ifconfig br0|grep "inet addr"|sed -n -e  's/^.*addr:\([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\).*Bcast.*/\1/p' 2>/dev/null`

lan_gw=`route -n|grep -e "^0.0.0.0"|grep br0|awk '{print $2;}' 2>/dev/null`


/etc/start_scripts/wlan_present.sh
if [ $? -eq 0 ]; then
# WLAN hardware is not present!

 wl_up=`nvram get wl_up`
 [ "$wl_up" = "Failed" ] && wl_up=1
 [ "$wl_up" = "1" ] && wlan_enabled=$ON || wlan_enabled=$OFF
 if [ "$wl_up" = "1" ]; then
  wl_essid=`wlctl essid|sed -e 's/.*\"\(.*\)\".*/\1/g' 2>/dev/null`
  ap=`wlctl ap 2>/dev/null`
  wl_mode=`wlctl infra 2>/dev/null`
  if [ "$wl_mode" = "0" ]; then
    wl_mode="AdHoc"
  elif [ "$ap" = "1" ]; then 
    wl_mode="AP"
  else
    wl_mode="Client"
  fi
  wl_channel=`wlctl channel|grep current|awk {'print $4;'} 2>/dev/null`

  rssi=`wlctl status 2>/dev/null|grep RSSI|sed -e 's/.*RSSI: \(.*\) dBm.noise.*/\1/g'`
  noise=`wlctl status 2>/dev/null|grep RSSI|sed -e 's/.*noise: \(.*\) dBm.*/\1/g'`
  let wl_qual=$rssi-1*$noise
 fi
fi

mem_total=`free |grep Mem|awk '{print $2;}'`
mem_used=`free |grep Mem|awk '{print $3;}'`
#mem_free=`free |grep Mem|awk '{print $4;}'`
let mem_per=$mem_used*100
let mem_per=$mem_per/$mem_total

nvram_use=`nvram show|grep "table_size="|sed -e 's/table_size=//'`
nvram_comp_size=`nvram compression info|grep "compressed_table_size="|sed -e 's/compressed_table_size=//'`
if [ "$nvram_comp_size" != "" ]; then
 nvram_uncomp_size=$nvram_use
 nvram_use=$nvram_comp_size
# nvram_comp_ratio=`nvram compression info|grep "compression_ratio="|sed -e 's/compression_ratio=//'`
fi

nvram_total=32767
#let nvram_free=$nvram_total-$nvram_use
let nvram_per=$nvram_use*100
let nvram_per=$nvram_per/$nvram_total

df -h|grep "/dev/mtdblock/3" >/dev/null 2>/dev/null
if [ "$?" = "0" ]; then
  j_size=`df -h|grep "/dev/mtdblock/3"|awk '{print $2;}'`
  j_used=`df -h|grep "/dev/mtdblock/3"|awk '{print $3;}'`
#  j_free=`df -h|grep "/dev/mtdblock/3"|awk '{print $4;}'`
  j_per=`df -h|grep "/dev/mtdblock/3"|awk '{gsub("%",""); print $5;}'`
else
  no_jffs2=1
fi


fw_start=`/bin/nvram get fw_start`
fw_on='Off'
[ "$fw_start" = "Failed" ] && fw_start="1"
[ "$fw_start" -eq "1" ] && fw_on='On'
wan_if=`nvram get fw_wan_if`
[ "$wan_if" = "Failed" ] && wan_if="ppp"
case "$wan_if" in
              "ppp")
                  wan_if="DSL/PPP"
                  ;;
              "br0")
                  wan_if="LAN/BRIDGE"
                  ;;
              "wl0")
                  wan_if="WLAN"
                  ;;
              *)
                  ;;
esac
ip_total=`cat /proc/sys/net/ipv4/netfilter/ip_conntrack_max 2>/dev/null`
ip_use=`wc -l /proc/net/ip_conntrack 2>/dev/null|awk {'print $1;'}`
if [ "$ip_total" = "" ] || [ "$ip_total" = "0" ]; then
  ip_total="no limit specified"
  ip_per=0
else
  let ip_per=$ip_use*100 2>/dev/null
  let ip_per=$ip_per/$ip_total 2>/dev/null
fi


# Name, NVRMA-Start-Variable, Default
service_status()
{
  local_status=`nvram get $2`
  [ "$local_status" = "Failed" ] && local_status=$3
  [ "$local_status" = "0" ] && local_status=$OFF || local_status=$ON
cat << EOF_HTML
<div id=c_std>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td width="100">$1</td><td>$local_status</td></td>
<td></td></tr></table>
</div>
EOF_HTML
}


cat << EOF_HTML 
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt"type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<style type="text/css">
span { margin-left:25px; text-decoration:none; font-weight:normal; }
#box_left {
	float:left; margin-right:1px;
} 
</style>
<!--[if IE]><style type="text/css">
@import url(../ie.css); 
#index_box { 
        width:300px;
}
#c_border {
        POSITION: absolute;
}
</style><![endif]-->
</HEAD>
<BODY>
<DIV id=c_Frame>
 <DIV id=c_border>
  <DIV id=c_pfad>BitSwitcher / Home</DIV>

  <div id=box_left>
   <div id=index_box>
    <DIV id=c_titel>WAN</DIV>
EOF_HTML
if [ "$wan_enabled" = $ON ]; then
cat << EOF_HTML 
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">DSL status:</td><td>$wan_status</td></td>
     <td></td></tr></table>
    </div>
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">Upstream:</td><td>$dsl_upstream</td></td>
     <td></td></tr></table>
    </div>
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">Downstream:</td><td>$dsl_downstream</td></td>
     <td></td></tr></table>
    </div>
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">PPP state:</td><td>$ppp_status</td></tr></table>
    </div>
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">PPP device:</td><td>$ppp_device</td></tr></table>
    </div>
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">WAN IP:</td><td>$wan_ip</td></tr></table>
    </div>
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">up since:</td><td>$wan_uptime</td></tr></table>
    </div>
EOF_HTML
else
cat << EOF_HTML
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">WAN status:</td><td>$wan_enabled</td></tr></table>
    </div>
EOF_HTML
fi
echo "   </div>"

/etc/start_scripts/wlan_present.sh
if [ $? -eq 0 ]; then
# WLAN hardware is not present!
cat << EOF_HTML

   <div id=index_box>
    <DIV id=c_titel>WLAN</DIV>
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">WLAN status:</td><td>$wlan_enabled</td></tr></table>
    </div>
EOF_HTML
if [ "$wlan_enabled" = $ON ]; then
cat << EOF_HTML 
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr ><td width="100">Mode:</td><td>$wl_mode</td></tr></table>
    </div>
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">SSID:</td><td>$wl_essid</td></tr></table>
    </div>
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">Channel:</td><td>$wl_channel</td></tr></table>
    </div>
EOF_HTML
  if [ "$ap" = "0" ]; then
  echo '      <DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr><td width="100">SNR:</td><td>'
    [ "$wl_qual" = "" ] && wl_qual=0
    HBar2 150 18 $wl_qual 40 "dBm"
    #HBar 150 18 $wl_qual "dBm"
  echo '       </td></tr></table></DIV>'
  fi
fi
 echo "   </div>"
fi

cat << EOF_HTML

   <div id=index_box>
    <DIV id=c_titel>System status</DIV>
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">Memory total:</td><td>${mem_total}k</td></tr></table>
    </div>
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">Memory used:</td><td width="50">${mem_used}k</td>
     <td>
EOF_HTML
HBar 100 18 $mem_per
cat << EOF_HTML
     </td>
     </tr></table>
    </div>
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">NVRAM total:</td><td>$nvram_total</td></tr></table>
    </div>
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">NVRAM used:</td><td width="50">$nvram_use</td>
     <td>
EOF_HTML
HBar 100 18 $nvram_per
cat << EOF_HTML
      </td></tr></table>
     </div>
EOF_HTML
if [ "$no_jffs2" = "1" ]; then
cat << EOF_HTML
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">JFFS-Partion:</td><td>not available</td></tr></table>
    </div>
EOF_HTML
else
cat << EOF_HTML
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">JFFS-Partion:</td><td>$j_size</td></tr></table>
    </div>
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">JFFS used:</td><td width="50">$j_used</td>
     <td>
EOF_HTML
  HBar 100 18 $j_per
cat << EOF_HTML
     </td></tr></table>
    </div>
EOF_HTML
fi

cat << EOF_HTML
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">CPU load last:</td><td width="50">5 min.</td>
     <td>
EOF_HTML
  HBar 100 18 $cpu_load "+"
cat << EOF_HTML
     </td></tr></table>
    </div>
   </div>
  </div>

  <div id=box_right>

   <div id=index_box>
    <DIV id=c_titel>LAN</DIV>
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">LAN IP:</td><td>$lan_ip</td></tr></table>
    </div>
EOF_HTML
if [ "$lan_gw" != ""  ]; then
cat << EOF_HTML 
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">Gateway:</td><td>$lan_gw</td></tr></table>
    </div>
EOF_HTML
fi

cat << EOF_HTML
   </div>

   <div id=index_box>
    <DIV id=c_titel>Services</DIV>
EOF_HTML
[ $voip_present -eq 1 ] && service_status "Phone:" "voip_start" 0
service_status "DHCP:" "dhcp_start" 1
service_status "DNS:" "dns_start" 1
service_status "Telnet:" "telnet_start" 1
service_status "SSH:" "ssh_start" 1
service_status "HTTP-Proxy:" "stproxy_start" 0
[ $voip_present -eq 1 ] && service_status "Call monitor:" "call_mon_start" 0
service_status "NTP:" "ntp_start" 0
service_status "Dynamic DNS:" "ddns_start" 0

cat << EOF_HTML
   </div>

   <div id=index_box>
    <DIV id=c_titel>Firewall</DIV>
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">Status:</td><td>$fw_on</td></tr></table>
    </div>
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">WAN interface:</td><td>$wan_if</td></tr></table>
    </div>
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">Max IP conn.:</td><td>$ip_total</td></tr></table>
    </div>
    <div id=c_std>
     <table border="0" cellpadding="4" cellspacing="0">
     <tr><td width="100">Active IP-conn.:</td><td>$ip_use</td>
     <td>
EOF_HTML
HBar 100 18 $ip_per
cat << EOF_HTML
     </td></tr></table>
    </div>
   </div>
  </div>


  <div id=index_box2>
   <DIV id=c_titel>DHCP clients</DIV>
   <div style="margin-left:20px;">
    <table border="0" cellpadding="4" cellspacing="0" >
    <tr><td width="130"><b>MAC address</b></td><td width="120"><b>IP address</b></td>
    <td width="120"><b>Hostname</b></td><td width="120"><b>Lease</b> [h:mm:ss]</td></tr>
EOF_HTML
if [ -f $LEASE_FILE ]; then
  awk "BEGIN{t0=`date +%s`;}"'{lease=$1; mac=$2; ip=$3; hostname=$4; t=lease-t0; vz=""; if(t<0){t=-t; vz="-";} h=int(t/3600); t=t-h*3600; m=int(t/60); t=t-m*60; s=t; printf "<tr><td>%s</td><td>%s</td><td>%s</td><td>%s%d:%02d:%02d</td></tr>\n",mac,ip,hostname,vz,h,m,s;}' $LEASE_FILE
else
  echo "<tr><td>No data available</td></tr>"
fi
cat << EOF_HTML 
    </table>
   </div>
  </div>

  <DIV id=c_foot></DIV>
 </div>
</div>
<DIV id=i_Frame>
 <DIV id=i_content>
  <h2>Home</h2>
  <p></p>
 </DIV>
</DIV>
</BODY></HTML>
EOF_HTML






