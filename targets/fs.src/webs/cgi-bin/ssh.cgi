#!/bin/sh

# Development stuff, set 'ssh_debug=1' if you want to test this script from /opt/webs
if [ "$ssh_debug" -ne 1 ]; then
  cgi_dir="."
  web_dir=".."
else
  cgi_dir="/webs/cgi-bin"
  web_dir="../.."
fi

from=ssh.cgi
action=0
. $cgi_dir/login.cgi

f_pfad="Services / SSH"
f_name="SSH"
f_text="The feature  &quot;sshd;&quot; is not included!"
f_bin="/bin/dropbear"
. $cgi_dir/feature.cgi
. $cgi_dir/functions.sh

auth_file="/var/dropbear/authorized_keys"

#### Formular-Auswertung #######################################################
mustsave=0

#RENEW KEYS
if [ "$action" -eq 4 ]; then
  /bin/dropbearkey -t dss -s 1024 -f /var/tmp_dss >/dev/null 2>/dev/null
  /bin/nvram setfile ssh_dss_key=/tmp/tmp_dss && rm -f /var/tmp_dss && mustsave=1 || err=1
  /bin/dropbearkey -t rsa -s 1024 -f /var/tmp_rsa /var/tmp_dss >/dev/null 2>/dev/null
  /bin/nvram setfile ssh_rsa_key=/tmp/tmp_rsa && rm -f /var/tmp_rsa && mustsave=1 || err=1
fi
#SAVE
if [ "$action" -eq 2 ] || [ "$action" -eq 3 ]; then
  [ "$R1" !=  "`/bin/nvram get ssh_start`" ] && res=`nvram set ssh_start=$R1` && mustsave=1 && [ "$res" != "OK" ] && err=1

  ssh_port=`/bin/nvram get ssh_port`
  if [ "$T1" != "$ssh_port" ]; then
    if [ "$T1" -ge "1" -o "$T1" -le "65534" ]; then
      is_fwservice_port "tcp" "$ssh_port" && remove_fwservice_port "tcp" "$ssh_port" && fw_restart=1 && mustsave=1
      res=`/bin/nvram set ssh_port=$T1` && mustsave=1 && [ "$res" != "OK" ] && err=1
    fi
  fi

  if [ "$form_ssh_fw_open" = "1" ]; then
    is_fwservice_port "tcp" "$T1" || add_fwservice_port "tcp" "$T1" && mustsave=1 && fw_restart=1
  else
    is_fwservice_port "tcp" "$T1" && remove_fwservice_port "tcp" "$T1" && mustsave=1 && fw_restart=1
    [ `nvram get fw_services_tcp` = "Failed" ] && res=`nvram set fw_services_tcp=""` && mustsave=1 && fw_restart=1 && [ "$res" != "OK" ] && err=1
  fi
  
  [ "$form_issue" !=  "`/bin/nvram get ssh_issue`" ] && res=`nvram set ssh_issue=$form_issue` && mustsave=1 && [ "$res" != "OK" ] && err=1

  # This sed script dequotes the URL encoded 'authorized_keys' string
  authorized_keys="$(echo -e "$(echo "$authorized_keys"|sed -e "
    s/+/ /g
    s/%0[dD]%0[aA]/%0A/g
    s/%\([0-9a-fA-F]\{2\}\)/\\\x\1/g
    ")"|sed -e "
    s/^ *//g
    s/ *$//g
  ")"

  # Get the authorized keys from nvram
  ssh_authorized_keys=`nvram get ssh_authorized_keys`
  [ "$?" -ne 0 ] && ssh_authorized_keys=''

  # Check if the authorized keys have been changed
  if [ "$authorized_keys" != "$ssh_authorized_keys" ]; then
    # Check if the user removed all keys
    if [ -z "$authorized_keys" ]; then
      # All keys have been removed, remove the auth file
      rm -f $auth_file
      # and also remove nvram parameter
      nvram del ssh_authorized_keys
    # The user changed the authorized keys
    else
      # write auth file
      echo "$authorized_keys" >$auth_file
      # set the proper rights (important!)
      chmod 600 $auth_file
      # and also write nvram parameter
      nvram setfile ssh_authorized_keys=$auth_file
    fi
    # we have to update the nvram
    mustsave=1
  fi

  # Always enable password logings if no authorized keys are set
  if [ -z "$authorized_keys" ]; then
    form_ssh_pw_login=0
  fi
  
  # Check if the 'disable password logins' parameter has been changed
  dplogin=`/bin/nvram get ssh_disable_plogin`
  if [ "$form_ssh_pw_login" != "$dplogin" ]; then
    nvram set ssh_disable_plogin=$form_ssh_pw_login
    mustsave=1
  fi

fi
#RUN or RENEW
if [ "$action" -eq 3 ] || [ "$action" -eq 4 ]; then
  [ "$mustsave" -eq 1 ] || [ "$action" -eq 4 ] && /etc/start_scripts/ssh.sh restart >/dev/null
  [ "$fw_restart" -eq 1 ] && /etc/start_scripts/firewall.sh services_restart >/dev/null
fi
################################################################################

[ "$err" -ne 0 ] && errmsg="$errmsg Write to NVRam failed!\n"
[ "$mustsave" -eq 1 ] && nvram commit >/dev/null

ssh_start=`/bin/nvram get ssh_start`
[ "$ssh_start" = "Failed" ] && ssh_start="1"
[ "$ssh_start" = "1" ] && ssh_on='checked="checked"' || ssh_off='checked="checked"'

ssh_port=`/bin/nvram get ssh_port`
[ "$ssh_port" = "Failed" ] && ssh_port="22"

is_fwservice_port "tcp" "$ssh_port" && fw_open='checked="checked"' || fw_closed='checked="checked"'
[ `nvram get fw_services_tcp` = "Failed" ] && [ `nvram get ssh_port` = "Failed" ] && fw_open='checked="checked"'

ssh_dss_set=`/bin/nvram showkeys|/bin/grep ssh_dss`
[ -n "$ssh_dss_set" ] && ssh_dss_set='checked="checked"' || ssh_dss_set=''

ssh_rsa_set=`/bin/nvram showkeys|/bin/grep ssh_rsa`
[ -n "$ssh_rsa_set" ] && ssh_rsa_set='checked="checked"' || ssh_rsa_set=''

issue=`/bin/nvram get ssh_issue`
[ "$issue" = "Failed" ] && issue="1"
[ "$issue" = "1" ] && issue_on='checked="checked"' || issue_off='checked="checked"'

# Retrieve authorized keys from nvram
ssh_authorized_keys=`/bin/nvram get ssh_authorized_keys`
[ "$?" -ne 0 ] && ssh_authorized_keys=''

# Retrieve password login disable parameter from nvram
dplogin=`/bin/nvram get ssh_disable_plogin`
[ "$?" -ne 0 ] && dplogin='0'
[ "$dplogin" = "0" ] && pw_allow='checked="checked"' || pw_deny='checked="checked"'

cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>

<SCRIPT language="JavaScript" src="${web_dir}/js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="${web_dir}/js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function showErr() {
  var err="$errmsg";
  if(err!=""){alert(err);}
}
function ssh_change() {
  if(document.getElementById("form_ssh_off").checked==true){
    document.getElementById("form_ssh_fw_open_off").disabled=true;
    document.getElementById("form_ssh_fw_open_on").disabled=true;
    document.getElementById("form_ssh_pw_login_off").disabled=true;
    document.getElementById("form_ssh_pw_login_on").disabled=true;
    document.getElementById("T1").disabled=true;
  }
  else{
    document.getElementById("form_ssh_fw_open_off").disabled=false;
    document.getElementById("form_ssh_fw_open_on").disabled=false;
    document.getElementById("form_ssh_pw_login_off").disabled=false;
    document.getElementById("form_ssh_pw_login_on").disabled=false;
    document.getElementById("T1").disabled=false;
  }
}
</SCRIPT>
<LINK rel="stylesheet" href="${web_dir}/style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(${web_dir}/ie.css);</style><![endif]-->
</HEAD>
<BODY onload="ssh_change();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Services / SSH</DIV>
<DIV id=c_titel>SSHd</DIV>
<DIV id=c_std>
<form id="xform" name="xform" method="post">
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0)>
<td width="110">SSH</td>
<td width="20"><input type="radio" id="form_ssh_off" name="R1" value="0" onchange="ssh_change();" $ssh_off></td>
<td width="80">Off</td>
<td width="20"><input type="radio" id="form_ssh_off" name="R1" value="1" onchange="ssh_change();" $ssh_on></td>
<td>On</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0)>
<td width="110">Firewall</td>
<td width="20"><input type="radio" id="form_ssh_fw_open_off" name="form_ssh_fw_open" value="0" $fw_closed></td>
<td width="80">Close</td>
<td width="20"><input type="radio" id="form_ssh_fw_open_on" name="form_ssh_fw_open" value="1" $fw_open></td>
<td>Open</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(9) onmouseout=i_showElem(0)>
<td width="110">Passwords</td>
<td width="20"><input type="radio" id="form_ssh_pw_login_off" name="form_ssh_pw_login" value="1" $pw_deny></td>
<td width="80">Deny</td>
<td width="20"><input type="radio" id="form_ssh_pw_login_on" name="form_ssh_pw_login" value="0" $pw_allow></td>
<td>Allow</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(10) onmouseout=i_showElem(0)>
<td width="110">Use issue</td>
<td width="20"><input type="radio" id="form_issue" name="form_issue" value="0" $issue_off></td>
<td width="80">Off</td>
<td width="20"><input type="radio" id="form_issue" name="form_issue" value="1" $issue_on></td>
<td>On</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0)>
<td width="110">SSH-Host Keys</td>
<td width="20"><input type="checkbox" name="C1" $ssh_dss_set disabled></td>
<td width="80">DSS-Key set</td>
<td width="20"><input type="checkbox" name="C2" $ssh_rsa_set disabled></td>
<td>RSA-Key set</td>
</tr></table>
</DIV>
<DIV id=c_last>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0)>
<td width="110">SSH-Port</td>
<td width="100"><input type="text" id="T1" name="T1" size="5" maxlength="5" value="$ssh_port"></td>
<td width="20"></td>
<td>Port-Number</td>
<input id="action" type="hidden" name="action" value="0">
</tr></table>
</DIV>
<p>
<fieldset onmouseover=i_showElem(8) onmouseout=i_showElem(0) style="width: 400px; margin-left: 15px;">
<legend>Authorized Keys</legend>
<textarea id="authorized_keys" name="authorized_keys" rows="3" cols="60" style="font-family:Courier, Courier New" wrap="off">$ssh_authorized_keys</textarea>
</fieldset>
</p>
</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but2 onclick="subm(4);" onmouseover="rahmen(1,2);i_showElem(5)" onmouseout="rahmen(0,2);i_showElem(0)">new Keys</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover="rahmen(1,3);i_showElem(6)" onmouseout="rahmen(0,3);i_showElem(0)">Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover="rahmen(1,4);i_showElem(7)" onmouseout="rahmen(0,4);i_showElem(0)">Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>SSH</h2>
<p>Configure settings for SSH</p>
</DIV>
<DIV id=i_content1>
<h2>SSH</h2>
<p><b>Start/Stop</b><br>Start SSH-service Yes/No</p>
<p><img src="${web_dir}/pic_i_hinweis.gif" border="0"><br /> Setting is checked on next Reboot</p>
</DIV>
<DIV id=i_content2>
<h2>SSH</h2>
<p><b>Firewall</b><br>Open SSH-Port for public access</p>
<p><img src="${web_dir}/pic_i_hinweis.gif" border="0"><br /> Setting controls only access via WAN interface</p>
</DIV>
<DIV id=i_content3>
<h2>SSHd</h2>
<p><b>SSH-Keys</b><br>Shows if SSH-Keys are set</p>
<p><img src="${web_dir}/pic_i_hinweis.gif" border="0"><br /> Only informative, click <Renew Keys> to save new ones</p>
</DIV>
<DIV id=i_content4>
<h2>SSHd</h2>
<p><b>SSH-Port</b><br>Set on which TCP-Port SSHd should listen</p>
<p><img src="${web_dir}/pic_i_hinweis.gif" border="0"><br /> Enter Valid Port-Number from 1-65534, default=22</p>
</DIV>
<DIV id=i_content5>
<h2>SSHd</h2>
<p><b>Renew Keys</b><br>Generates new SSH-Keys and saves them to NVRAM</p>
<p><img src="${web_dir}/pic_i_hinweis.gif" border="0"><br /> Restarts SSHd immediatly with using new keys</p>
</DIV>
<DIV id=i_content6>
<h2>SSHd</h2>
<p><b>Save</b><br>Saves changed settings to NVRAM</p>
<p><img src="${web_dir}/pic_i_hinweis.gif" border="0"><br /> Changes only occur when restarting SSH</p>
</DIV>
<DIV id=i_content7>
<h2>SSHd</h2>
<p><b>Save&amp;Run</b><br>Save changed settings to NVRAM and restart SSH with new settings</p>
<p><img src="${web_dir}/pic_i_hinweis.gif" border="0"><br /> Changes are safed permanently</p>
</DIV>
<DIV id=i_content8>
<h2>SSH</h2>
<p><b>Authorized SSH Keys</b><br>Specify authorized SSH public keys</p>
<p><img src="${web_dir}/pic_i_hinweis.gif" border="0"><br /> Paste your SSH public keys here, one piece each line. Key changes apply immediately after saving.</p>
</DIV>
<DIV id=i_content9>
<h2>SSH</h2>
<p><b>Allow Password Logins</b><br>Allow or deny SSH password logins</p>
<p><img src="${web_dir}/pic_i_hinweis.gif" border="0"><br /> Disallowing SSH password logins is only reasonable if you specify one or more authorized SSH public keys.</p>
</DIV>
<DIV id=i_content10>
<h2>SSH</h2>
<p><b>Use issue file</b><br>Use a welcome message called "issue" when logging in via ssh</p>
<p><img src="${web_dir}/pic_i_hinweis.gif" border="0"><br /> Some clients have problems with long welcome strings. You can disable this if you have problems with this.</p>
</DIV>
</DIV></BODY></HTML>
