#!/bin/sh

discon_start=`/bin/nvram get disconnect_start`
discon_on='Off'
if [ "$discon_start" = "Failed" ]; then
  discon_start="0"
fi
[ "$discon_start" -eq "1" ] && discon_on='On'

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onselectstart="return false">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Services / Additional</DIV>

<DIV id=c_titel>Additional functions</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
 <tr>
    <td width="200"><a href="wol.cgi" onmouseover=i_showElem(1) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>Wake-on-LAN</a></td></tr>
</table>
</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
    <tr> <td width="200"><a href="disconnect.cgi" onmouseover=i_showElem(2) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>Auto-Disconnect/Reboot</a></td>
    <td>$discon_on</td>
</tr></table>
</DIV>
<DIV id="c_foot"></DIV>
</DIV></DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Additional</h2>
<p>Setup additional services.</p>
</DIV>
<DIV id=i_content1>
<h2>Additional functions</h2>
<p><b>Wake-on-LAN</b><br>
Start Wake-on-LAN capable Computer over the LAN.</p>
</DIV>
<DIV id=i_content2>
<h2>Additional functions</h2>
<p><b>Auto-Disconnect/Reboot</b><br>
Time-driven DSL Auto-Disconnect and Reconnect or Reboot</p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML

a=0