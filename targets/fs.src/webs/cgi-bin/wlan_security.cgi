#!/bin/sh

from=wlan_basic.cgi
action=0

. ./login.cgi

/etc/start_scripts/wlan_present.sh
if [ $? -ne 0 ]; then
# WLAN hardware is not present!
cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY>
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>WLAN configuration</DIV>
<DIV id=c_title></div>
<DIV id=c_std><b style="color: rgb(255, 17, 17);">WLAN hardware is not present!</b></div>
<DIV id=c_leer></DIV>
</DIV>
<DIV id="c_foot"></DIV>
<DIV id=i_Frame>
</DIV>
</DIV></BODY></HTML>
HTML
exit 0
fi

f_pfad="WLAN Settings / Security settings"
f_name="Security Settings"
HOME="wlan_index.cgi"
. ./functions.sh

PATH=.:$PATH

KEY_STRING=""
WEP_DEFAULT="37373538393436333231353630"
errmsg=""
mustsave=0

#get infrastructure mode
wl_infra=`nvram get wl_infra`
[ "$wl_infra" = "Failed" ] && wl_infra=1

check_wep_key()
{
  [ "$1" = "" ] && return 1
  len=${#1}
#check for ASCII
  if [ "$len" -eq "5" ] || [ "$len" -eq "13" ]; then
    KEY_STRING="$1"
    return 0
#check if HEX
  elif [ "$len" -eq "10" ] || [ "$len" -eq "26" ]; then
    mykey=`echo $1|sed -e 's/[^0-9a-fA-F]//g'|tr '[a-z]' '[A-Z]'`
#check len after substitution against old len
    [ ${#mykey} -eq "$len" ] && KEY_STRING="$mykey" && return 0 || return 1
#error
  else
    return 1
  fi
}

isNumeric()
{
  [ $1 -eq 0 ] 2>/dev/null && return 0
  a="$1"
  let "a-=1" 2>/dev/null || return 1
  [ $a -eq -1 ] && return 1 || return 0
}


####### Save & Run #######
if [ "$action" -eq 2 ] || [ "$action" -eq 3 ]; then
  if [ "$sel_secmode" = "0" ]; then
    #NONE
    wl_wsec="0"
  elif [ "$sel_secmode" = "1" ]; then
    #WEP
    wl_wsec="1"
    j=0
    for i in 0 1 2 3
    do
      key="echo \$text_wep_$i"
      key=`eval $key`
      if [ "$key" != "" ]; then
        let="j=i+1"
        check_wep_key $key
        if [ $? -ne 0 ]; then
          errmsg="$errmsg WEP-key($j) is invalid"'\n'
          continue
        fi
        [ "$KEY_STRING" != "`nvram get wl_wep$i`" ] && res=`nvram set wl_wep$i=$KEY_STRING` && mustsave=1 && [ "$res" != "OK" ] && err=1
      elif [ $i -gt 0 ]; then
        [ "`nvram get wl_wep$i`" != "" ] && res=`nvram set wl_wep$i=` && mustsave=1 && [ "$res" != "OK" ] && err=1
      fi
    done

    [ "$form_wep_index" != "`nvram get wl_wep_index`" ] && res=`nvram set wl_wep_index=$form_wep_index` && mustsave=1 && [ "$res" != "OK" ] && err=1
  else
    #WPA
    wl_wsec="$sel_wpa_alg"
    [ "$sel_secmode" != "`nvram get wl_wpa_auth`" ] && res=`nvram set wl_wpa_auth=$sel_secmode` && mustsave=1 && [ "$res" != "OK" ] && err=1
    if [ "$sel_secmode" = "2" ] || [ "$sel_secmode" = "64" ] || [ "$sel_secmode" = "66" ]; then
      #RADIUS
      wl_radius_ipaddr="$text_radius_ip"
      wl_radius_ipaddr=`isip.sh $wl_radius_ipaddr`
      [ "$wl_radius_ipaddr" = "ERROR" ] && errmsg="$errmsg Invalid Radius IP-address!"'\n' || [ "$wl_radius_ipaddr" != "`nvram get wl_radius_ipaddr`" ] && res=`nvram set wl_radius_ipaddr=$wl_radius_ipaddr` && mustsave=1 && [ "$res" != "OK" ] && err=1
      if [ "$text_radius_port" -ge "1" -o "$text_radius_port" -le "65534" ]; then 
        [ "$text_radius_port" != "`nvram get wl_radius_port`" ] && res=`nvram set wl_radius_port=$text_radius_port` && mustsave=1 && [ "$res" != "OK" ] && err=1
      else
        errmsg="$errmsg Invalid Radius port!"'\n'
      fi
      http_unquote_all "text_radius_key"
      text_radius_key=`echo -e "$text_radius_key"|tr -d '[:space:]'`
      [ "$text_radius_key" != "`nvram get $wl_radius_key`" ] && res=`nvram set wl_radius_key=$text_radius_key` && mustsave=1 && [ "$res" != "OK" ] && err=1
    else
      #PSK
      http_unquote_all "text_wpa_psk"
      #text_wpa_psk=`echo -e "$text_wpa_psk"|tr -d '[:space:]'`
      [ "$text_wpa_psk" != "`nvram get wl_wpa_psk`" ] && res=`nvram set wl_wpa_psk="$text_wpa_psk"` && mustsave=1 && [ "$res" != "OK" ] && err=1
    fi
    isNumeric "$text_wpa_rekey"
    if [ "$?" -eq "0" ]; then
      [ "$text_wpa_rekey" != "`nvram get wl_wpa_gtk_rekey`" ] && res=`nvram set wl_wpa_gtk_rekey=$text_wpa_rekey` && mustsave=1 && [ "$res" != "OK" ] && err=1 
    else
      errmsg="$errmsg WPA-rekey intervall is not numeric!"'\n'
    fi
  fi
  [ "$wl_wsec" != "`nvram get wl_wsec`" ] && res=`nvram set wl_wsec=$wl_wsec` && mustsave=1 && [ "$res" != "OK" ] && err=1

  [ "$err" -ne 0 ] && errmsg="$errmsg Write to NVRam failed!"'\n'
  
  [ "$mustsave" -eq 1 ] && nvram commit >/dev/null && [ "$action" -eq 3 ] && /etc/start_scripts/wlan.sh restart >/dev/null 2>/dev/null
fi
#############

ON='checked=\"checked\"'
SEL="selected"


#########on change###########
if [ "$action" -eq  5 ]; then
  wl_wsec="$sel_secmode"
  if [ "$wl_wsec" -gt "1" ]; then
    wpa_alg="$sel_wpa_alg"
    if [ "$wpa_alg" = "" ]; then
      wpa_alg=`nvram get wl_wsec`
      [ "$wpa_alg" = "Failed" ] && wpa_alg=2
    fi
    case "$wpa_alg" in
                2)
                    sel_secmode_tkip=$SEL
                    ;;
                4)
                    sel_secmode_aes=$SEL
                    ;;
                6)
                    sel_secmode_tkip_aes=$SEL
                    ;;
                *)
                    sel_secmode_tkip=$SEL
                    ;;
    esac
    if [ "$wl_wsec" = "2" ] || [ "$wl_wsec" = "64" ] || [ "$wl_wsec" = "66" ]; then
      use_radius=1
      wl_radius_ipaddr="$text_radius_ip"
      wl_radius_port="$text_radius_port"
      wl_radius_key="$text_radius_key"
      if [ "$wl_radius_ipaddr" = "" ]; then
        wl_radius_ipaddr=`nvram get wl_radius_ipaddr`
        [ "$wl_radius_ipaddr" = "Failed" ] && wl_radius_ipaddr="127.0.0.1"
      fi
      if [ "$wl_radius_port" = "" ]; then
        wl_radius_port=`nvram get wl_radius_port`
        [ "$wl_radius_port" = "Failed" ] && wl_radius_port="1812"
      fi
      if [ "$wl_radius_key" = "" ]; then
        wl_radius_key=`nvram get wl_radius_key`
        [ "$wl_radius_key" = "Failed" ] && wl_radius_key="secret"
      fi
    else
      wl_wpa_psk="$text_wpa_psk"
      if [ "$wl_wpa_psk" = "" ]; then
        wl_wpa_psk=`nvram get wl_wpa_psk`
        [ "$wl_wpa_psk" = "Failed" ] && wl_wpa_psk="whatever"
      fi
    fi
    wl_wpa_gtk_rekey="$text_wpa_rekey"
    if [ "$wl_wpa_gtk_rekey" = "" ]; then
        wl_wpa_gtk_rekey=`nvram get wl_wpa_gtk_rekey`
        [ "$wl_wpa_gtk_rekey" = "Failed" ] && wl_wpa_gtk_rekey=3600
    fi
  elif [ "$wl_wsec" = "1" ]; then
    for i in 0 1 2 3
    do
      key="wep_$i=\$text_wep_$i"
      eval $key
      key="echo \$wep_$i"
      key=`eval $key`
      if [ "$key" = "" ]; then
        key=`nvram get wl_wep_$i`
        [ "$key" = "Failed" ] && key=""
        [ "$i" = "0" ] && [ "$key" = "" ] && key=$WEP_DEFAULT
        var="wep_$i=\$key"
        eval $var
      fi
    done

    if [ "$sel_wep_mode" = "" ]; then
      for i in 0 1 2 3
      do
        key="echo \$wep_$i"
        len=`eval $key`
        [ "$len" != "" ] && break
      done
      [ "$len" = "" ] && sel_wep_mode=26 || sel_wep_mode=`echo ${#len}`
    fi

    case "$sel_wep_mode" in
               5)
                 sel_wepmode_64_ascii=$SEL
                 ;;
               10)
                 sel_wepmode_64_hex=$SEL
                 ;;
               13)
                 sel_wepmode_128_ascii=$SEL
                 ;;
               26)
                 sel_wepmode_128_hex=$SEL
                 ;;
               *)
                 sel_wepmode_128_hex=$SEL
                 sel_wep_mode=26
                 ;;
      esac

    wep_len="$sel_wep_mode"
    [ "$form_wep_index0" = "1" ] && wl_wep_index="$form_wep_index0"
    [ "$form_wep_index1" = "1" ] && wl_wep_index="$form_wep_index1"
    [ "$form_wep_index2" = "1" ] && wl_wep_index="$form_wep_index2"
    [ "$form_wep_index3" = "1" ] && wl_wep_index="$form_wep_index3"
    [ "$wl_wep_index" = "" ] && wl_wep_index=0
  fi
  
  case "$wl_wsec" in
                0)
                    sel_secmode_none=$SEL
                    ;;
                1)
                    sel_secmode_wep=$SEL
                    ;;
                 2)
                    sel_secmode_wpa=$SEL
                    ;;
                 4)
                    sel_secmode_psk=$SEL
                    ;;
                 64)
                    sel_secmode_wpa2=$SEL
                    ;;
                 66)
                    sel_secmode_wpa_wpa2=$SEL
                    ;;
                 128)
                    sel_secmode_psk2=$SEL
                    ;;
                 132)
                    sel_secmode_psk_psk2=$SEL
                    ;;
                *)
                    sel_secmode_psk=$SEL
                    ;;
  esac

#normal load
else

  #get wlan encryption
  wl_wsec=`nvram get wl_wsec`
  [ "$wl_wsec" = "Failed" ] && wl_wsec=1
  [ "$wl_infra" = "0" ] && [ "$wl_wsec" -gt "1" ] && wl_wsec=0
  
  case "$wl_wsec" in
                0)
                    sel_secmode_none=$SEL
                    ;;
                1)
                    sel_secmode_wep=$SEL
                    ;;
                2)
                    sel_secmode_tkip=$SEL
                    ;;
                4)
                    sel_secmode_aes=$SEL
                    ;;
                6)
                    sel_secmode_tkip_aes=$SEL
                    ;;
                *)
                    sel_secmode_tkip=$SEL
                    ;;
  esac
  if [ "$wl_wsec" = "1" ]; then
    i=0
    first=-1
    while [ $i -lt 4 ]
    do
      key=`nvram get wl_wep$i`
      [ "$i" = "0" ] && [ "$key" = "" ] && key=$WEP_DEFAULT
      [ "$i" = "0" ] && [ "$key" = "Failed" ] && key=$WEP_DEFAULT
      check_wep_key $key
      if [ $? -ne 0 ]; then
        let "i+=1"
        continue
      fi
      eval `echo wep_${i}=${KEY_STRING}`
      [ $first -eq -1 ] && first=$i
      let "i+=1"
    done
    wep_len="wep_len=\${#wep_$first}"
    eval $wep_len
    case "$wep_len" in
                5)
                    sel_wepmode_64_ascii=$SEL
                    ;;
                10)
                    sel_wepmode_64_hex=$SEL
                    ;;
                13)
                    sel_wepmode_128_ascii=$SEL
                    ;;
                26)
                    sel_wepmode_128_hex=$SEL
                    ;;
                *)
                    sel_wepmode_128_hex=$SEL
                    ;;
    esac
    wl_wep_index=`nvram get wl_wep_index`
    [ "$wl_wep_index" = "Failed" ] || [ "$wl_wep_index" = "" ] && wl_wep_index=0
  
  elif [ "$wl_wsec" -gt 1 ]; then
    wpa_auth=`nvram get wl_wpa_auth`
    [ "$wpa_auth" = "Failed" ] && wpa_auth=4
    case "$wpa_auth" in
                2)
                    sel_secmode_wpa=$SEL
                    ;;
                4)
                    sel_secmode_psk=$SEL
                    ;;
                64)
                    sel_secmode_wpa2=$SEL
                    ;;
                66)
                    sel_secmode_wpa_wpa2=$SEL
                    ;;
                128)
                    sel_secmode_psk2=$SEL
                    ;;
                132)
                    sel_secmode_psk_psk2=$SEL
                    ;;
                *)
                    sel_secmode_psk=$SEL
                    ;;
    esac
    wl_wpa_gtk_rekey=`nvram get wl_wpa_gtk_rekey`
    [ "$wl_wpa_gtk_rekey" = "Failed" ] && wl_wpa_gtk_rekey=3600
    if [ "$wpa_auth" = "2" ] || [ "$wpa_auth" = "64" ] || [ "$wpa_auth" = "66" ]; then
      use_radius=1
      wl_radius_ipaddr=`nvram get wl_radius_ipaddr`
      [ "$wl_radius_ipaddr" = "Failed" ] && wl_radius_ipaddr="127.0.0.1"
      wl_radius_port=`nvram get wl_radius_port`
      [ "$wl_radius_port" = "Failed" ] && wl_radius_port="1812"
      wl_radius_key=`nvram get wl_radius_key`
      [ "$wl_radius_key" = "Failed" ] && wl_radius_key="secret"
    else
      wl_wpa_psk=`nvram get wl_wpa_psk`
      [ "$wl_wpa_psk" = "Failed" ] && wl_wpa_psk="whatever"
    fi
  fi

fi

cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>

<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function showErr() {
  var err="$errmsg";
  if(err!=""){alert(err);}
}
function change_wep_format() {
  if(!document.getElementById("sel_wepmode")){
    return;
  }
  size=document.getElementById("sel_wepmode").value;
  for (var i = 0; i < 4; i++){
    obj=document.getElementById("text_wep_"+i);
    obj.size=size;
    //obj.setAttribute=('maxlength',size);
    if(obj.value.length>size){
      obj.value=obj.value.slice(0,size);
    }
  }
}
function check_button(id) {
  var input=document.getElementById(id);
  if(!input){ return;}
  mode=document.getElementById("sel_wepmode").value;
  if(mode=="10" || mode=="26"){
    input.value = input.value.replace(/[^A-F,0-9,a-f]/g, "");
  }
}
function do_pass_show_radius(state)
{
        var OldInput = document.getElementById('text_radius_key');
        if(!OldInput) return;

        var val = OldInput.value;
        var val_maxlength = OldInput.maxlength;
        var val_size = OldInput.size;
        var parent = OldInput.parentNode;
        var sibling = OldInput.nextSibling;
        var newInput = document.createElement('input');
        newInput.setAttribute('value', val);
        newInput.setAttribute('name', 'text_radius_key');
        newInput.setAttribute('id', 'text_radius_key');
        newInput.setAttribute('maxlength', val_maxlength);
        newInput.setAttribute('size', val_size);

        if (state == true)
                newInput.setAttribute('type', 'text');
        else
                newInput.setAttribute('type', 'password');

        parent.removeChild(OldInput);
        parent.insertBefore(newInput, sibling);
        newInput.focus();
}
function do_pass_show_psk(state)
{
        var OldInput = document.getElementById('text_wpa_psk');
        if(!OldInput) return;

        var val = OldInput.value;
        var val_maxlength = OldInput.maxlength;
        var val_size = OldInput.size;
        var parent = OldInput.parentNode;
        var sibling = OldInput.nextSibling;
        var newInput = document.createElement('input');
        newInput.setAttribute('value', val);
        newInput.setAttribute('name', 'text_wpa_psk');
        newInput.setAttribute('id', 'text_wpa_psk');
        newInput.setAttribute('maxlength', val_maxlength);
        newInput.setAttribute('size', val_size);

        if (state == true)
                newInput.setAttribute('type', 'text');
        else
                newInput.setAttribute('type', 'password');

        parent.removeChild(OldInput);
        parent.insertBefore(newInput, sibling);
        newInput.focus();
}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();change_wep_format();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>$f_pfad</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<DIV id=c_titel>$f_name</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0)>
<td width="150">Security mode</td>
<td>
  <select id="sel_secmode" name="sel_secmode" size="1" onchange="subm(5);">
    <option value="0" $sel_secmode_none>None</option>
    <option value="1" $sel_secmode_wep>WEP</option>
HTML
if [ "$wl_infra" = "1" ]; then
  echo "<option value=\"2\" $sel_secmode_wpa>WPA-Enterprise</option>"
  echo "<option value=\"4\" $sel_secmode_psk>WPA-PSK</option>"
  echo "<option value=\"64\" $sel_secmode_wpa2>WPA2-Enterprise</option>"
  echo "<option value=\"66\" $sel_secmode_wpa_wpa2>WPA- + WPA2-Enterprise mixed</option>"
  echo "<option value=\"128\" $sel_secmode_psk2>WPA2-PSK</option>"
  echo "<option value=\"132\" $sel_secmode_psk_psk2>WPA- + WPA2-PSK mixed</option>"
fi
cat <<HTML
  </select>
</td></tr></table></DIV>
HTML
if [ "$wl_wsec" = "1" ]; then
  echo '<DIV id=c_std>' \
  '<table border="0" cellpadding="0" cellspacing="2">'
  echo '<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0)>' \
  '<td width="150">Standard WEP-Key</td>'
  for i in 0 1 2 3
  do
    [ "$i" = "$wl_wep_index" ] && check="$ON" || check=""
    let "j=i+1"
    echo "<td width=\"20\"><input type=\"radio\" name=\"form_wep_index\" value=\"$i\" $check></td>"
    echo "<td width=\"20\">$j</td>"
  done
  echo '</tr></table></DIV><DIV id=c_std><table border="0" cellpadding="0" cellspacing="2">' \
  '<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0)><td width="150">Key length/format</td>' \
  '<td><select id="sel_wepmode" name="sel_wepmode" size="1" onchange="change_wep_format();">'
  echo "<option value=\"5\" $sel_wepmode_64_ascii>64 bits/5 ASCII chars</option>"
  echo "<option value=\"10\" $sel_wepmode_64_hex>64 bits/10 HEX digits</option>"
  echo "<option value=\"13\" $sel_wepmode_128_ascii>128 bits/13 ASCII chars</option>"
  echo "<option value=\"26\" $sel_wepmode_128_hex>128 bits/26 HEX digits</option>"
  echo '</select></td></tr></table></DIV>'
  for i in 0 1 2 3
  do
    let "j=i+1"
    key="echo \$wep_$i"
    key=`eval $key`
    echo '<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2">'
    echo "<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0)><td width=\"150\">WEP-key $j</td>"
    echo "<td><input id=\"text_wep_$i\" name=\"text_wep_$i\" size=\"$wep_len\" maxlength=\"26\" value=\"$key\" onkeyup=\"check_button('text_wep_$i');\">"
    echo '</td></tr></table></DIV>'
  done
elif [ "$wl_wsec" -gt "1" ]; then
  echo '<DIV id=c_std>' \
  '<table border="0" cellpadding="0" cellspacing="2">'
  echo '<tr onmouseover=i_showElem(5) onmouseout=i_showElem(0)>' \
  '<td width="150">Encryption algorithm</td>' \
  '<td><select id="sel_wpa_alg" name="sel_wpa_alg" size="1">'
  echo "<option value=\"2\" $sel_secmode_tkip>TKIP</option>"
  echo "<option value=\"4\" $sel_secmode_aes>AES</option>"
  echo "<option value=\"6\" $sel_secmode_tkip_aes>TKIP+AES</option></select></td>"
  echo '</tr></table></DIV><DIV id=c_std><table border="0" cellpadding="0" cellspacing="2">'
  if [ "$use_radius" = "1" ]; then
    echo '<tr onmouseover=i_showElem(6) onmouseout=i_showElem(0)><td width="150">Radius IP-address</td>'
    echo "<td><input id=\"text_radius_ip\" name=\"text_radius_ip\" size=\"15\" maxlength=\"15\" value=\"$wl_radius_ipaddr\"></td>"
    echo '</tr></table></DIV><DIV id=c_std><table border="0" cellpadding="0" cellspacing="2">'
    echo '<tr onmouseover=i_showElem(7) onmouseout=i_showElem(0)><td width="150">Radius port</td>'
    echo "<td><input id=\"text_radius_port\" name=\"text_radius_port\" size=\"5\" maxlength=\"5\" value=\"$wl_radius_port\"></td>"
    echo '</tr></table></DIV><DIV id=c_std><table border="0" cellpadding="0" cellspacing="2">'
    echo '<tr onmouseover=i_showElem(8) onmouseout=i_showElem(0)><td width="150">Radius shared secret</td>'
    echo "<td><input id=\"text_radius_key\" type=\"password\" name=\"text_radius_key\" size=\"25\" maxlength=\"63\" value=\"$wl_radius_key\"></td>"
    echo '<td><input id="pass_show" type="checkbox" onchange="do_pass_show_radius(this.checked);">show</td>'
  else
    echo '<tr onmouseover=i_showElem(9) onmouseout=i_showElem(0)><td width="150">WPA shared key</td>'
    echo "<td><input id=\"text_wpa_psk\" name=\"text_wpa_psk\" type=\"password\" size=\"25\" maxlength=\"63\" value=\"$wl_wpa_psk\"></td>"
    echo '<td><input id="pass_show" type="checkbox" onchange="do_pass_show_psk(this.checked);">show</td>'
  fi
  echo '</tr></table></DIV><DIV id=c_std><table border="0" cellpadding="0" cellspacing="2">' \
  '<tr onmouseover=i_showElem(10) onmouseout=i_showElem(0)><td width="150">WPA-key renewal intervall</td>'
  echo "<td><input id=\"text_wpa_rekey\" name=\"text_wpa_rekey\" size=\"4\" maxlength=\"4\" value=\"$wl_wpa_gtk_rekey\">"
  echo '</td></tr></table></DIV>'
fi


cat <<HTML
</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>WLAN Security</h2>
<p>Configure encryption settings.</p>
</DIV>
<DIV id=i_content1>
<h2>Security mode</h2>
<p>Select the security mode depending on your basic settings.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br />WPA(-PSK) or WPA2(-PSK) is not available in AdHoc-mode.</p>
</DIV>
<DIV id=i_content2>
<h2>Standard WEP-Key</h2>
<p>Set the standard WEP-key to use.</p>
</DIV>
<DIV id=i_content3>
<h2>Key length/format</h2>
<p>Select the key length und format to enter it.</p>
</DIV>
<DIV id=i_content4>
<h2>WEP-keys 1-4</h2>
<p>Enter at maximum 4 WEP-keys to save.</p>
</DIV>
<DIV id=i_content5>
<h2>Encryption algorithm</h2>
<p>Select WPA encryption algorithm.</p>
</DIV>
<DIV id=i_content6>
<h2>Radius-Server IP-address</h2>
<p>Enter IP-address of the RADIUS-Server to use.</p>
</DIV>
<DIV id=i_content7>
<h2>Radius-Server port</h2>
<p>Enter UDP-port of the RADIUS-Server.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Enter valid port-number from 1-65534, default=1812</p>
</DIV>
<DIV id=i_content8>
<h2>Radius-Server shared secret</h2>
<p>Enter shared secret for communication with the RADIUS-server.</p>
</DIV>
<DIV id=i_content9>
<h2>WPA shared key</h2>
<p>Enter the preshared key to use.</p>
</DIV>
<DIV id=i_content10>
<h2>WPA-key renewal intervall</h2>
<p>Enter WPA-rekey intervall in seconds.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> range 180-7200, default=3600</p>
</DIV>
</DIV></BODY></HTML>
