#!/bin/sh

action=0

. ./login.cgi

f_pfad="Firewall Settings / DMZ"
f_name="DMZ"
HOME="fw_index.cgi"
. ./functions.sh

PATH=.:$PATH

errmsg=""
mustsave=0


####### Save & Run #######
if [ "$action" -eq 2 ] || [ "$action" -eq 3 ]; then
  err=0
  if [ "$form_fw_dmz" = "1" ]; then
    ip=`isip.sh $text_ip`
    [ "$ip" = "ERROR" ] && errmsg="$errmsg Invalid DMZ-host IP-address!"'\n' && err=1
    [ "$err" != "1" ] && [ "$ip" != "`nvram get fw_dmz_ip`" ] && res=`nvram set fw_dmz_ip=$ip` && mustsave=1
  fi
  [ "$form_fw_dmz" != "`nvram get fw_dmz`" -a $err -eq 0 ] && res=`nvram set fw_dmz=$form_fw_dmz` && mustsave=1

  [ "$mustsave" -eq 1 ] && nvram commit >/dev/null && [ "$action" -eq 3 ] && /etc/start_scripts/firewall.sh dmz_restart >/dev/null 2>/dev/null
fi

ON='checked=\"checked\"'
SEL="selected"

set_select()
{
  var=$1
  default=$2
  value=`nvram get $var`
  [ "$value" = "Failed" ] && value=$default 
  if [ $value -eq 1 ]; then
    eval `echo ${var}_on=$ON`
    eval `echo ${var}_off=''`
  else
    eval `echo ${var}_on=''`
    eval `echo ${var}_off=$ON`
  fi
}


set_select "fw_dmz" 0
dmz_ip=`nvram get fw_dmz_ip`
[ "$dmz_ip" = "Failed" ] && dmz_ip=""

cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function showErr() {
  var err="$errmsg";
  if(err!=""){alert(err);}
}
function enable_host(state)
{
        var input = document.getElementById('text_ip');
        if(!input) return;

        if (state == true)
	{
                input.disabled=false;
		input.focus();
	}	
        else
                input.disabled=true;

}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();enable_host('$fw_dmz_on'!='');">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>$f_pfad</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<DIV id=c_titel>DMZ settings</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0)><td width="200">DMZ</td>
<td width="20"><input type="radio" id="dmz_off" name="form_fw_dmz" value="0" onchange="enable_host(false);" $fw_dmz_off></td>
<td width="40">Off</td>
<td width="20"><input type="radio" id="dmz_on" name="form_fw_dmz" value="1" onchange="enable_host(true);" $fw_dmz_on></td>
<td>On</td>
</tr></table>
</DIV>
<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0)>
<td width="200">DMZ-host IP-address</td>
<td><input type="text" id="text_ip" name="text_ip" size="15" maxlength="15" value="$dmz_ip"></td>
</tr></table></DIV>
</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>General settings</h2>
<p>Configure general firewall parameters like the WAN-interface to use and other basic options.</p>
</DIV>
<DIV id=i_content1>
<h2>SPI firewall</h2>
<p>Turn the demilitarized zone (DMZ) <b>on</b> or <b>off</b>.<br>The purpose of DMZ is to make a private 
host completely reachable from the internet (WAN-interface).</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br />  Activating the DMZ means, any traffic not matched by protocol- or port-forwarding rules will 
be redirected to the DMZ-host.</p>
</DIV>
<DIV id=i_content2>
<h2>DMZ-host IP-address</h2>
<p>Enter valid private IP-address of your DMZ-host.</p>
</DIV>
</DIV></BODY></HTML>

