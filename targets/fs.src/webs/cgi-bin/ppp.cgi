#!/bin/sh

HOME="wan_index.cgi"

. ./login.cgi
. ./functions.sh

############################## Save & Run ##################################

ON='checked=\"checked\"'

postfix=""
ppp_conn_prim=$ON
if [ "$ppp_conn_nr" = "1" ]; then
    postfix="_sec" 
    ppp_conn_prim=""
    ppp_conn_sec=$ON
fi

if [ "$action" -eq 3 ] || [ "$action" -eq 2 ]; then
  err=0

  http_unquote_all user
  dsl_user="$user"

  http_unquote_all pass
  dsl_pass="$pass"
  #echo "user=$user, pass=$pass" >/tmp/ppp_debug

  if [ "$postfix" = "" ]; then
    http_unquote wan_dns1
    http_unquote wan_dns2
    http_unquote wan_dns3
    if [ "$wan_dns1" != "" ]; then
      is_ipaddr $wan_dns1
      [ "$?" = "1" ] && wan_dns1="" && errmsg="$errmsg DNS 1: IP address was invalid!"'\n' && err=1
    fi
    if [ "$wan_dns2" != "" ]; then
      is_ipaddr $wan_dns2
      [ "$?" = "1" ] && wan_dns2="" && errmsg="$errmsg DNS 2: IP address was invalid!"'\n' && err=1
    fi
    if [ "$wan_dns3" != "" ]; then
      is_ipaddr $wan_dns3
      [ "$?" = "1" ] && wan_dns3="" && errmsg="$errmsg DNS 3: IP address was invalid!"'\n' && err=1
    fi
  fi

  if [ "$static_ip" != "" ]; then
    is_ipaddr $static_ip
    [ "$?" = "1" ] && static_ip="" && errmsg="$errmsg Static IP Address: IP address was invalid!"'\n' && err=1
  fi

  [ "$persistent" = "on" ] && persistent=1 || persistent=0
  [ "$autoconnect" = "on" ] && autoconnect=1 || autoconnect=0

  timeout=`echo $timeout|sed -e 's/\([^0-9]\)//g'`
  [ "$mtu" = "" ] && mtu=300
  [ $mtu -lt 30 ] && mtu=300
  [ $mtu -gt 3600 ] && mtu=300

  mtu=`echo $mtu|sed -e 's/\([^0-9]\)//g'`
  [ "$mtu" = "" ] && mtu=1492
  [ $mtu -lt 512 ] && mtu=512
  [ $mtu -gt 9000 ] && mtu=1492


  if [ "$err" -eq 0 ]; then
  err=0
  mustsave=0
  
  [ "$ppp_start" != "`nvram get ppp_start$postfix`" ] && res=`nvram set ppp_start$postfix=$ppp_start` && mustsave=1  && [ "$res" != "OK" ] && err=1
  [ "$ppp_device" != "`nvram get ppp_device$postfix`" ] && res=`nvram set ppp_device$postfix=$ppp_device` && mustsave=1  && [ "$res" != "OK" ] && err=1
  
  [ "$dsl_user" != "`nvram get dsl_user$postfix`" ] && res=`nvram set dsl_user$postfix="$dsl_user"` && mustsave=1  && [ "$res" != "OK" ] && err=1
  [ "$dsl_pass" != "`nvram get dsl_pass$postfix`" ] && res=`nvram set dsl_pass$postfix="$dsl_pass"` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$static_ip" != "`nvram get dsl_static_ip$postfix`" ] && res=`nvram set dsl_static_ip$postfix=$static_ip` && mustsave=1 && [ "$res" != "OK" ] && err=1

  if [ "$postfix" = "" ]; then
    [ "$wan_dns1" != "`nvram get wan_dns1`" ] && res=`nvram set wan_dns1=$wan_dns1` && mustsave=1 && [ "$res" != "OK" ] && err=1
    [ "$wan_dns2" != "`nvram get wan_dns2`" ] && res=`nvram set wan_dns2=$wan_dns2` && mustsave=1 && [ "$res" != "OK" ] && err=1
    [ "$wan_dns3" != "`nvram get wan_dns3`" ] && res=`nvram set wan_dns3=$wan_dns3` && mustsave=1 && [ "$res" != "OK" ] && err=1
  fi

  [ "$persistent" != "`nvram get dsl_persistent_conn$postfix`" ] && res=`nvram set dsl_persistent_conn$postfix=$persistent` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$autoconnect" != "`nvram get dsl_autoconnect$postfix`" ] && res=`nvram set dsl_autoconnect$postfix=$autoconnect` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$timeout" != "`nvram get dsl_idle_timeout$postfix`" ] && res=`nvram set dsl_idle_timeout$postfix=$timeout` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$mtu" != "`nvram get dsl_mtu$postfix`" ] && res=`nvram set dsl_mtu$postfix=$mtu` && mustsave=1 && [ "$res" != "OK" ] && err=1

  [ "$mustsave" -eq 1 ] && nvram commit >/dev/null 
  [ "$err" -ne 0 ] && errmsg="$errmsg Write to NVRam failed!"'\n'

  [ "$action" -eq 3 ] && /etc/start_scripts/dsl.sh ppp_restart >/dev/null 2>/dev/null
  fi
fi

SEL="selected"

ppp_off='checked="checked"'
ppp_on=""
ppp_start=`nvram get ppp_start$postfix`
[ "$ppp_start" = "1" ] && ppp_on='checked="checked"' && ppp_off=''


ppp_device=`nvram get ppp_device$postfix`
[ "$ppp_device" = "Failed" ] && ppp_device="dsl"

case "$ppp_device" in
                dsl)
                    sel_pppdev_dsl=$SEL
                    ;;
                lan)
                    sel_pppdev_lan=$SEL
                    ;;
                wlan)
                    sel_pppdev_wlan=$SEL
                    ;;
                *)
                    sel_pppdev_dsl=$SEL
                    ;;
esac


persistent='checked="checked"'
dsl_persistent_conn=`nvram get dsl_persistent_conn$postfix`
[ "$dsl_persistent_conn" = "0" ] && persistent=''

dsl_off='checked="checked"'
dsl_on=""
dsl_start=`nvram get dsl_start`
[ "$dsl_start" = "1" ] && dsl_on='checked="checked"' && dsl_off=''

autoconnect='checked="checked"'
dsl_autoconnect=`nvram get dsl_autoconnect$postfix`
[ "$dsl_autoconnect" = "0" ] && autoconnect=''


ppp_auth_auto='selected="checked"'
ppp_auth_pap=''
ppp_auth_chap=''
ppp_auth_mschap=''
ppp_auth=`nvram get ppp_auth$postfix`
[ "$ppp_auth" = "1" ] && ppp_auth_auto='' && ppp_auth_pap='selected="checked"'
[ "$ppp_auth" = "2" ] && ppp_auth_auto='' && ppp_auth_chap='selected="checked"'
[ "$ppp_auth" = "3" ] && ppp_auth_auto='' && ppp_auth_mschap='selected="checked"'


dsl_user=`nvram get dsl_user$postfix`
[ "$dsl_user" = "Failed" ] && dsl_user="user"

dsl_pass=`nvram get dsl_pass$postfix`
[ "$dsl_pass" = "Failed" ] && dsl_pass="pass"

dsl_static_ip=`nvram get dsl_static_ip$postfix`
[ "$dsl_static_ip" = "Failed" ] && dsl_static_ip=''

dsl_idle_timeout=`nvram get dsl_idle_timeout$postfix`
[ "$dsl_idle_timeout" = "Failed" ] && dsl_idle_timeout=300

dsl_mtu=`nvram get dsl_mtu$postfix`
[ "$dsl_mtu" = "Failed" ] && dsl_mtu=1492

if [ "$postfix" = "" ]; then
  wan_dns1=`nvram get wan_dns1`
  [ "$wan_dns1" = "Failed" ] && wan_dns1=""
  wan_dns2=`nvram get wan_dns2`
  [ "$wan_dns2" = "Failed" ] && wan_dns2=""
  wan_dns3=`nvram get wan_dns3`
  [ "$wan_dns3" = "Failed" ] && wan_dns3=""
fi

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function showErr() {
var err="$errmsg";
if(err!=""){alert(err);}
js_permanent(document.getElementById("persistent").checked);
}
function js_permanent(state) {
if(state) { 
document.getElementById("tr_autoconnect").style.visibility="hidden";
document.getElementById("tr_timeout").style.visibility="hidden";
}
else {
document.getElementById("tr_autoconnect").style.visibility="visible";
document.getElementById("tr_timeout").style.visibility="visible";
}
}
function do_pass_show(state)
{
  if(state) {
    document.getElementById("pass").type="text";
  }
  else {
    document.getElementById("pass").type="password";
  }
}

</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Network Settings / DSL/WAN</DIV>
<DIV id=c_titel>PPP/Dial-In settings</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(19) onmouseout=i_showElem(0) >
<td width="150">PPP-Connection:</td>
<td width="20"><input type="radio" name="ppp_conn_nr" value="0" onchange="subm(5);" $ppp_conn_prim></td>
<td width="40">Primary</td>
<td width="20"><input type="radio" name="ppp_conn_nr" value="1" onchange="subm(5);" $ppp_conn_sec></td>
<td>Secondary</td>
</tr></table>
</DIV>
<DIV id=c_leer></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0) >
<td width="150">Enabled:</td>
<td width="20"><input type="radio" name="ppp_start" value="1" $ppp_on></td>
<td width="40">On</td>
<td width="20"><input type="radio" name="ppp_start" value="0" $ppp_off></td>
<td>Off</td>
</tr></table>
</DIV>

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(20) onmouseout=i_showElem(0) >
<td width="150">PPP-Device:</td>
<td><select name="ppp_device">
EOF_HTML
if [ "$postfix" = "" ]; then
  if [ `nvram get ppp_start_sec` = "1" ]; then
    echo "<option value=\"dsl\" $sel_pppdev_dsl>DSL(1)</option>"
    dev=`nvram get ppp_device_sec`
    [ "$dev" = "Failed" ] && dev="dsl"
    [ "$dev" != "lan" ] && echo "<option value=\"lan\" $sel_pppdev_lan>LAN</option>"
    [ "$dev" != "wlan" ] && echo "<option value=\"wlan\" $sel_pppdev_wlan>WLAN</option>"
  else
    echo "<option value=\"dsl\" $sel_pppdev_dsl>DSL(1)</option>"
    echo "<option value=\"lan\" $sel_pppdev_lan>LAN</option>"
    echo "<option value=\"wlan\" $sel_pppdev_wlan>WLAN</option>"
  fi
else
  if [ `nvram get ppp_start` = "1" ]; then
    echo "<option value=\"dsl\" $sel_pppdev_dsl>DSL(2)</option>"
    dev=`nvram get ppp_device`
    [ "$dev" = "Failed" ] && dev="dsl"
    [ "$dev" != "lan" ] && echo "<option value=\"lan\" $sel_pppdev_lan>LAN</option>"
    [ "$dev" != "wlan" ] && echo "<option value=\"wlan\" $sel_pppdev_wlan>WLAN</option>"
  else
    echo "<option value=\"dsl\" $sel_pppdev_dsl>DSL(2)</option>"
    echo "<option value=\"lan\" $sel_pppdev_lan>LAN</option>"
    echo "<option value=\"wlan\" $sel_pppdev_wlan>WLAN</option>"
  fi
fi
cat << EOF_HTML

</select>
</td></tr></table></DIV> 

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0) >
<td width="150">User name:</td>
<td><input id="user" name="user" size="30" maxlength="50" type="text" value="$dsl_user"></td>
</td></tr></table></DIV> 
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0) >
<td width="150">Password:</td>
<td><input id="pass" name="pass" size="30" maxlength="50" type="password" value="$dsl_pass"></td>
<td><input id="pass_show" type="checkbox" onchange="do_pass_show(this.checked);">show</td>
</td></tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0) >
<td width="150">Static IP Address:</td>
<td><input id="static_ip" name="static_ip" size="20" maxlength="15" type="text" value="$dsl_static_ip"></td>
</td></tr></table></DIV> 

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(5) onmouseout=i_showElem(0) >
<td width="150">Persistent connection:</td>
<td><input id="persistent" name="persistent" type="checkbox" $persistent onchange="js_permanent(this.checked);"></td>
</tr></table></DIV>

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(6) onmouseout=i_showElem(0) id="tr_autoconnect" name="tr_autoconnect">
<td width="150">Automatic reconnect:</td>
<td><input id="autoconnect" name="autoconnect" type="checkbox" $autoconnect></td>
</tr></table></DIV>

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(7) onmouseout=i_showElem(0) id="tr_timeout" name="tr_timeout">
<td width="150">Idle timeout:</td>
<td><input id="timeout" name="timeout" size="10" maxlength="5" type="text" value="$dsl_idle_timeout"> in seconds</td>
</tr></table></DIV> 
EOF_HTML

if [ "$postfix" = "" ]; then
cat << EOF_HTML
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(14) onmouseout=i_showElem(0) >
<td width="150">DNS 1:</td>
<td><input id="wan_dns1" name="wan_dns1" size="20" maxlength="15" type="text" value="$wan_dns1"></td>
</tr></table></DIV> 
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(14) onmouseout=i_showElem(0) >
<td width="150">DNS 2:</td>
<td><input id="wan_dns2" name="wan_dns2" size="20" maxlength="15" type="text" value="$wan_dns2"></td>
</tr></table></DIV> 
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(14) onmouseout=i_showElem(0) >
<td width="150">DNS 3:</td>
<td><input id="wan_dns3" name="wan_dns3" size="20" maxlength="15" type="text" value="$wan_dns3"></td>
</tr></table></DIV> 
EOF_HTML
fi

cat << EOF_HTML
<DIV id=c_leer></div>
<DIV id=c_titel>Advanced PPP settings</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(8) onmouseout=i_showElem(0) >
<td width="150">MTU:</td>
<td><input id="mtu" name="mtu" size="10" maxlength="4" type="text" value="$dsl_mtu"></td>
</tr></table></DIV> 

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(9) onmouseout=i_showElem(0) >
<td width="150">Authentication Method:</td>
<td><select name="ppp_auth">
<option value="0" $ppp_auth_auto>Auto</option>
<option value="1" $ppp_auth_pap>PAP</option>
<option value="2" $ppp_auth_chap>CHAP</option>
<option value="3" $ppp_auth_mschap>MSCHAP</option>
</select>
</td></tr></table></DIV> 

</form>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save </DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run </DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>PPP/Dial-In settings</h2>
<p>Setup your PPP/Dial-In connection.</p>
</DIV>
<DIV id=i_content1>
<h2>PPP/Dial-In settings</h2>
<p><b>PPP-Connection</b><br>
Use a PPP-connection</p>
<p><b>On</b> PPP-connection will be established over configured PPP-Device.<p>
<p><b>Off</b> No PPP-Connection will be used.<p>
</DIV>
<DIV id=i_content2>
<h2>PPP/Dial-In settings</h2>
<p><b>User name</b><br>
<p>Enter your user name for the DSL connection.</p>
</DIV>
<DIV id=i_content3>
<h2>PPP/Dial-In settings</h2>
<p><b>Password</b><br>
<p>Enter your password for the DSL connection.</p>
</DIV>
<DIV id=i_content4>
<h2>PPP/Dial-In settings</h2>
<p><b>Static IP Address</b><br>
<p>If you have an static IP address enter it here. If you get an dynamic IP address empty this field.</p>
</DIV>
<DIV id=i_content5>
<h2>Basic settings</h2>
<p><b>Persistent connection</b><br>
<p>Enable this if you want to keep the connection unlimited alive.</p>
</DIV>
<DIV id=i_content6>
<h2>PPP/Dial-In settings</h2>
<p><b>Automatic reconnect</b><br>
<p></p>
</DIV>
<DIV id=i_content7>
<h2>PPP/Dial-In settings</h2>
<p><b>Idle timeout</b><br>
<p>After idle timeout the connection is closed. The idle timeout is a time in seconds.</p>
<p>Valid values: 30-3600</p>
</DIV>
<DIV id=i_content8>
<h2>Advanced PPP settings</h2>
<p><b>MTU</b><br>
<p>Maximum Transfer Unit (MTU) for the PPP link.</p>
<p>Valid values: 512-9000</p>
<p>Default: 1492</p>
</DIV>
<DIV id=i_content9>
<h2>Advanced PPP settings</h2>
<p><b>Authentication Method</b><br>
<p><b>Auto</b> - automatically choose the best method.</p>
<p><b>PAP</b> - Password Authentication Protocol</p>
<p><b>CHAP</b> - Challenge Handshake Authentication Protocol</p>
<p><b>MSCHAP</b> - Microsoft Challenge Handshake Authentication Protocol</p>
<p>Default: <b>Auto</b></p>
</DIV>
<DIV id=i_content14>
<h2>PPP/Dial-In settings</h2>
<p><b>DNS 1-3</b><br>
<p>Enter static DNS servers here.</p>
<p>To use dynamic DNS server detected by PPP daemon free this fields.</p>
</DIV>
<DIV id=i_content19>
<h2>PPP-Connection</h2>
<p>Choose which PPP-Connection to configure.</p>
<p>Default: Primary</p>
<p>Use this if your provider requires a secondary PPP-connection for VOIP or IPTV.</p>
</DIV>
<DIV id=i_content20>
<h2>PPP/Dial-In Settings</h2>
<p><b>PPP device</b><br>
<p>Sets which device should be used for PPP Dial-In.</p>
<p>Default: DSL</p>
<p>If you dont use the internal but an external DSL-Modem you should choose <b>LAN</b> for this.</p>
<p>If secondary PPP-Connection is choosed DSL means the secondary ATM-connection if activated.</p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML


