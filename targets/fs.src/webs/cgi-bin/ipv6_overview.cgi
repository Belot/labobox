#!/bin/sh

. ./login.cgi
. ./functions.sh
HOME="ipv6_index.cgi"

#generate the EIU-64 Identifier for a given device (eth0, wl0, br0, etc)
# taken from /etc/start_scripts/ipv6.sh
gen_eui64()
{
  device=$1
  base_mac=`ifconfig $device|grep HWaddr|awk '{print $5}'`
  mac_6=`echo $base_mac|sed -e 's/:/ /g'|awk '{print $6}'`
  mac_5=`echo $base_mac|sed -e 's/:/ /g'|awk '{print $5}'`
  mac_4=`echo $base_mac|sed -e 's/:/ /g'|awk '{print $4}'`
  mac_3=`echo $base_mac|sed -e 's/:/ /g'|awk '{print $3}'`
  mac_2=`echo $base_mac|sed -e 's/:/ /g'|awk '{print $2}'`
  mac_1=`echo $base_mac|sed -e 's/:/ /g'|awk '{print $1}'`

  eui64="02"${mac_2}":"${mac_3}"FF:FE"${mac_4}":"${mac_5}${mac_6}
  echo $eui64
}

ipv6_start=`nvram get ipv6_start`
[ "$ipv6_start" = "Failed" ] && ipv6_start=0

use_ula=`nvram get ipv6_use_ula`
[ "$use_ula" = "Failed" ] && use_ula=1
if [ "$use_ula" = "1" ]; then
  ula_host=`gen_eui64 br0`
  ula_prefix=`nvram get ipv6_ula_prefix`
  ula=${ula_prefix}":"${ula_host}"/64"
fi

rad_start=`nvram get ipv6_radvd_start`
[ "$rad_start" = "Failed" ] && rad_start=1

if [ "$rad_start" = "1" ]; then
  adv_min=`nvram get ipv6_adv_minint`
  [ "$adv_min" = "Failed" ] && adv_min=3
  adv_max=`nvram get ipv6_adv_maxint`
  [ "$adv_max" = "Failed" ] && adv_max=10
  rad_mtu=`nvram get ipv6_adv_mtu`
  [ "$rad_mtu" = "Failed" ] && rad_mtu=1280
  home_agent=`nvram get ipv6_adv_mobile`
  [ "$home_agent" = "Failed" ] && $home_agent='off'
fi


cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY>
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>IPv6 / IPv6 overview</DIV>
<div id="c_titel">Basic settings</div>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="160">IPv6:</td><td>
EOF_HTML

[ "$ipv6_start" = "1" ] && echo "on" || echo "off"

cat << EOF_HTML
</td></tr></table>
</DIV>
EOF_HTML

if [ "$ipv6_start" = "1" ]; then

cat << EOF_HTML
<div id="c_leer"></div>
<div id="c_titel">Unique Local Unicast Address (ULA)</div>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="160">use ULA:</td><td>
EOF_HTML

[ "$use_ula" = "1" ] && echo "on" || echo "off"

cat << EOF_HTML
</td></tr></table>
</DIV>
EOF_HTML

if [ "$use_ula" = "1" ]; then
cat << EOF_HTML
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="160">ULA prefix:</td>
<td>$ula_prefix</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="160">ULA address:</td>
<td>$ula</td>
</tr></table>
</DIV>
EOF_HTML
fi # use_ula

cat << EOF_HTML
<div id="c_leer"></div>
<div id="c_titel">Router Advertisement</div>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="160">send Router Advertisements:</td><td>
EOF_HTML

[ "$rad_start" = "1" ] && echo "on" || echo "off"

cat << EOF_HTML
</td></tr></table>
</DIV>
EOF_HTML

if [ "$rad_start" = "1" ]; then
cat << EOF_HTML
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="160">min. advertisement interval:</td><td>
$adv_min</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="160">max. advertisement interval:</td><td>
$adv_max</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="160">Link MTU:</td><td>
$rad_mtu</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="160">Home Agent:</td><td>
$home_agent</td>
</tr></table>
</DIV>
EOF_HTML
fi # rad_start

ipv6_add_addr=`nvram get ipv6_add_addr`
[ "$ipv6_add_addr" = "Failed" ] && ipv6_add_addr=""
if [ "$ipv6_add_addr" != "" ]; then
cat << EOF_HTML
<div id="c_leer"></div>
<div id="c_titel">additional IPv6 addresses</div>
<div id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr><td width="200"><b>Address</b></td><td><b>Device</b></td></tr>
EOF_HTML
echo $ipv6_add_addr |sed -e 's/~/\n/g'|sed -n -e '/^$/!p'| \
awk '{ split($0,a,/[,]/)
       ipv6_addr=a[1]
       iface=a[2]
       split("Bridge/LAN,WLAN", ifnames, ",");
       print "<tr><td>" ipv6_addr "</td><td>" ifnames[iface+1] "</td></tr>"
    }'

cat << EOF_HTML
</table>
</div>
EOF_HTML
fi # ipv6_add_addr

ipv6_add_routes=`nvram get ipv6_add_routes`
if [ "$ipv6_add_routes" != "Failed" -a "$ipv6_add_routes" != "" ]
then
cat << EOF_HTML
<div id="c_leer"></div>
<div id="c_titel">IPv6 routes</div>
<table style="border:none; padding-left:10px;">
<tr><td style="padding:5px;"><b>Route</b></td><td style="padding:5px;"><b>Gateway</b></td><td style="padding:5px;"><b>Device</b></td>
<td style="padding:5px;"><b>Metric</b></td><td style="padding:5px;"><b>Advertise</b></td><td style="padding:5px;"><b>On link</b></td>
<td style="padding:5px;"><b>Mobile</b></td><td style="padding:5px;"><b>auto conf.</b></td><td style="padding:5px;"><b>Valid</b></td>
<td style="padding:5px;"><b>Preffered</b></td></tr>
EOF_HTML
   echo $ipv6_add_routes |sed -e 's/~/\n/g'|sed -n -e '/^$/!p'| \
   awk '{
          split($0,a,/[,]/); 
          advertise=a[1]; 
          ipv6_addr=a[2]; 
          on_link=a[3]; 
          mobile=a[4]; 
          advauto=a[5]; 
          validtime=a[6]; 
          preftime=a[7]; 
	  gateway=a[8];
          device=a[9];
	  metrik=a[10];

          if(advertise=="2"){device=""; gateway="";}
	  if(device==""){device_str=""}else
          { 
            #split("br0,wl0", ifnames, ",");
            #device_str="dev " ifnames[device+1];
            device_str=device;
          }
	  if(advertise!="0"){adv="yes"}else{adv="no"}
	
        print "<tr><td style=\"padding:5px;\">" ipv6_addr "</td><td style=\"padding:5px;\">" gateway "</td><td style=\"padding:5px;\">" device_str "</td><td style=\"padding:5px;\">" metrik "</td><td style=\"padding:5px;\">" adv "</td>"
	  
          if(adv=="yes")
          {
          advauto_str=advauto?"yes":"no";
          on_link_str=on_link?"yes":"no";
          mobile_str=mobile?"yes":"no";
          print "<td style=\"padding:5px;\">" on_link_str "</td><td style=\"padding:5px;\">" mobile_str "</td><td style=\"padding:5px;\">" advauto_str "</td><td style=\"padding:5px;\">" validtime " sec</td><td style=\"padding:5px;\">" preftime " sec</td></tr>"
          }
          else
          {
	    print "<td></td><td></td><td></td><td></td><td></td></tr>"
          }
        }'
cat << EOF_HTML
</table>
EOF_HTML
fi #ipv6_add_routes


ipv6_6to4_tunnel=`nvram get ipv6_6to4_tunnel`
if [ "$ipv6_6to4_tunnel" != "Failed" -a "$ipv6_6to4_tunnel" != "" ]
then
cat << EOF_HTML
<div id="c_leer"></div>
<div id="c_titel">IPv6 6to4 tunnel</div>
<table style="border:none; padding-left:10px;">
<tr><td style="padding:5px;"><b>Remote address</b></td><td style="padding:5px;"><b>Local address</b></td><td style="padding:5px;"><b>IPv6 address</b></td>
<td style="padding:5px;"><b>Route</b></td><td style="padding:5px;"><b>TTL</b></td>
<td style="padding:5px;"><b>Metric</b></td><td style="padding:5px;"><b>Advertise</b></td></tr>
EOF_HTML
   echo $ipv6_6to4_tunnel |sed -e 's/~/\n/g'|sed -n -e '/^$/!p'| \
   awk '{ split($0,a,/[,]/)
          remote_ipv4=a[1]
          local_ipv4=a[2]
	  local_ipv6=a[3]
	  route=a[5]
	  ttl=a[6]
	  advertise=a[7]
          metric=a[8]
          
	  if(local_ipv4 ~ /^dyn/)
	  {
	    split(local_ipv4,h,/:/)
            split("br0,wl0,PPP0", ifnames, ",");
	    dev=ifnames[h[2]+1];
          }
	  else { dev=local_ipv4 }

          advertise_str=advertise?"yes":"no";

          print "<tr><td style=\"padding:5px;\">" remote_ipv4 "</td><td style=\"padding:5px;\">" dev "</td><td style=\"padding:5px;\">" local_ipv6 "</td><td style=\"padding:5px;\">" route "</td><td style=\"padding:5px;\">" ttl "</td><td style=\"padding:5px;\">" metric "</td><td style=\"padding:5px;\">" advertise_str "</td></tr>"
	}'
cat << EOF_HTML
</table>
EOF_HTML
fi # ipv6_6to4_tunnel


fi # ipv6_start

cat << EOF_HTML
<DIV id=c_leer></DIV>
<DIV id="c_foot"></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV></DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>IPv6 overview</h2>
</DIV>
</DIV></BODY></HTML>
EOF_HTML

a=0

