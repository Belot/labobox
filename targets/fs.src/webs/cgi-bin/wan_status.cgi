#!/bin/sh

. ./functions.sh
HOME="status_index.cgi"


atm_vpi=`nvram get atm_vpi`
[ "$atm_vpi" = "Failed" ] && atm_vpi=1
atm_vci=`nvram get atm_vci`
[ "$atm_vci" = "Failed" ] && atm_vci=32

daemonstatus=`cat /proc/var/fyi/wan/ppp_${atm_vpi}_${atm_vci}_1/daemonstatus 2>/dev/null`
if [ "$daemonstatus" = "0" ]; then
  ppp_status="try to connect (waiting for PADO)"
  ppp_help="PADO (PPPoE Active Discovery Offer) is the second frame in the PPP 4-way-handshake."
elif [ "$daemonstatus" = "1" ]; then
  ppp_status="try to connect (got PADO, waiting for PADS)"
  ppp_help="PADS (PPPoE Active Discovery Session-confirmation) is the last frame in the PPP 4-way-handshake."
elif [ "$daemonstatus" = "2" ]; then
  ppp_status="try to connect (got PADS, session ID confirmed)"
  ppp_help="PADS (PPPoE Active Discovery Session-confirmation) received, session ID confirmed. PADS ist the last frame in PPP handshake."
elif [ "$daemonstatus" = "3" ]; then
  ppp_status="down (no connection)"
elif [ "$daemonstatus" = "4" ]; then
  ppp_status="up (connection established)"
elif [ "$daemonstatus" = "7" ]; then
  ppp_status="authentication failed"
else
  ppp_status="down (unknown status)"
fi

if [ "$daemonstatus" = "4" ]; then
  wan_ip=`cat /proc/var/fyi/wan/ppp_${atm_vpi}_${atm_vci}_1/ipaddress 2>/dev/null`
  dns1=`cat /var/fyi/sys/dns|awk 'BEGIN{h=0}{h++; if(h==1){print $2; exit;}}' 2>/dev/null`
  dns2=`cat /var/fyi/sys/dns|awk 'BEGIN{h=0}{h++; if(h==2){print $2; exit;}}' 2>/dev/null`
  dns3=`cat /var/fyi/sys/dns|awk 'BEGIN{h=0}{h++; if(h==3){print $2; exit;}}' 2>/dev/null`
fi

cat << EOF_HTML 
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt"type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY>
<DIV id=c_border>
<DIV id=c_pfad>Status / PPP status</DIV>
<DIV id=c_titel>PPP status</DIV>

<div id="c_std">
<table border="0" cellpadding="4" cellspacing="0">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0)>
<td width="100">PPP state:</td><td>$ppp_status</td></td>
<td></td></tr></table>
</div>
EOF_HTML

if [ "$daemonstatus" = "4" ]; then
cat << EOF_HTML 
<div id="c_std">
<table border="0" cellpadding="4" cellspacing="0">
<tr >
<td width="100">WAN IP:</td><td>$wan_ip</td></td>
<td></td></tr></table>
</div>
EOF_HTML

  if [ "$dns1" != "" ]; then
cat << EOF_HTML 
<div id="c_std">
<table border="0" cellpadding="4" cellspacing="0">
<tr >
<td width="100">DNS 1:</td><td>$dns1</td></td>
<td></td></tr></table>
</div>
EOF_HTML
  fi
  if [ "$dns2" != "" ]; then
cat << EOF_HTML 
<div id="c_std">
<table border="0" cellpadding="4" cellspacing="0">
<tr >
<td width="100">DNS 2:</td><td>$dns2</td></td>
<td></td></tr></table>
</div>
EOF_HTML
  fi
  if [ "$dns3" != "" ]; then
cat << EOF_HTML 
<div id="c_std">
<table border="0" cellpadding="4" cellspacing="0">
<tr >
<td width="100">DNS 3:</td><td>$dns3</td></td>
<td></td></tr></table>
</div>
EOF_HTML
  fi
fi

cat << EOF_HTML 
</div>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>WAN status</h2>
<p></p>
</DIV>
<DIV id=i_content1>
<h2>WAN status</h2>
<p><b>PPP state</b></p>$ppp_help
</DIV>
</DIV>
</BODY></HTML>
EOF_HTML

a=0
