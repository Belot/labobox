#!/bin/sh

echo 'Content-type: text/html'
echo 'Connection: close'
echo ''
echo '<HTML><HEAD><TITLE></TITLE>'

echo '<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>' \
    '<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>' \
    '<LINK rel="stylesheet" href="../style.css" type="text/css">' \
    '<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->' \
    '</HEAD>' \
    '<BODY onselectstart="return false">' \
    '<DIV id=c_Frame>' \
    '<DIV id=c_border>' \
    '<DIV id=c_pfad>Plugins / Plugin Overview</DIV>' \
    '<DIV id=c_titel>Plugins</DIV>' \

cd plugins >/dev/null 2>/dev/null && ls -1 *.cgi >/dev/null 2>/dev/null
[ $? -ne 0 ] && no_plugins=1
if [ "$no_plugins" = "1" ]; then
echo '<div id="c_std"><table border="0" cellpadding="4" cellspacing="0">' \
       '<tr><td>No plugins installed!</td>' \
       '</tr></table>' \
       '</div>'
else
i=0    
for file in `ls -1 *.cgi`
do
  let i=$i+1
  name=`awk '{if(match($1,/#NAME:/)){ret=gsub($1,"",$0); print $0; exit;}}' $file`
  [ "$name" = "" ] && name=$file
  echo '<div id="c_link">' \
       '<table border="0" cellpadding="4" cellspacing="0">' \
       "<tr><td width=\"200\"><a href=\"plugins/$file\" onmouseover=\"i_showElem($i)\" onmouseout=\"i_showElem(0)\" target=\"hcti\"><b>&gt;&gt; </b>$name</a></td>" \
       '</tr></table>' \
       '</div>'
done  
fi
    
echo '<DIV id="c_foot"></DIV>' \
    '</DIV></DIV>' \
    '<DIV id=i_Frame>' \
    '<DIV id=i_content>' \
    '<h2>Plugins</h2>' \
    '<p>Here you can find custom plugins.</p>' \
    '<p>You get some more info, when moving the cursor over a menuentry.</p>' \
    '</DIV>' 

if [ "$no_plugins" != "1" ]; then
i=0
for file in `ls -1 *.cgi`
do
  let i=$i+1
  name=`awk '{if(match($1,/#NAME:/)){ret=gsub($1,"",$0); print $0; exit;}}' $file`
  [ "$name" = "" ] && name=$file
  desc=`awk '{if(match($1,/#DESCRIPTION:/)){ret=gsub($1,"",$0); print $0; exit;}}' $file`    
  echo "<DIV id=i_content$i>" \
       "<h2>$name</h2>" \
       "$desc" \
       '</DIV>' 
done
fi
    
echo '</DIV></BODY></HTML>'

