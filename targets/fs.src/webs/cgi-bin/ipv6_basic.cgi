#!/bin/sh

. ./login.cgi
. ./functions.sh
HOME="ipv6_index.cgi"

#generate the EIU-64 Identifier for a given device (eth0, wl0, br0, etc)
# taken from /etc/start_scripts/ipv6.sh
gen_eui64()
{
  device=$1
  base_mac=`ifconfig $device|grep HWaddr|awk '{print $5}'`
  mac_6=`echo $base_mac|sed -e 's/:/ /g'|awk '{print $6}'`
  mac_5=`echo $base_mac|sed -e 's/:/ /g'|awk '{print $5}'`
  mac_4=`echo $base_mac|sed -e 's/:/ /g'|awk '{print $4}'`
  mac_3=`echo $base_mac|sed -e 's/:/ /g'|awk '{print $3}'`
  mac_2=`echo $base_mac|sed -e 's/:/ /g'|awk '{print $2}'`
  mac_1=`echo $base_mac|sed -e 's/:/ /g'|awk '{print $1}'`

  eui64="02"${mac_2}":"${mac_3}"FF:FE"${mac_4}":"${mac_5}${mac_6}
  echo $eui64
}


if [ "$action" = "3" ] || [ "$action" = "2" ]; then

  err=0
  ERROR=""

  isNumeric "$rad_mtu"
  if [ "$?" -eq "0" ]; then
     [ "$rad_mtu" -lt 1280 ] && rad_mtu=1280 && ERROR="$ERROR MTU was out of range, set it to 1280.\n"
     [ "$rad_mtu" -gt 1500 ] && rad_mtu=1280 && ERROR="$ERROR MTU was out of range, set it to 1280.\n"
  else   
     rad_mtu=1280
     ERROR="$ERROR MTU was invalid, set it to 1280.\n"
  fi 
  
  isNumeric "$adv_min"
  if [ "$?" -eq "0" ]; then
    [ "$adv_min" -lt 3 ] && adv_min=3 && ERROR="$ERROR Minimum router advertisement too small, set it to 3.\n"
    [ "$adv_min" -gt 1350 ] && adv_min=3 && ERROR="$ERROR Minimum router advertisement interval too big, set it to 3.\n"
  else   
     adv_min=3
     ERROR="$ERROR  Minimum router advertisement interval was invalid, set it to 3.\n"
  fi 

  isNumeric "$adv_max"
  if [ "$?" -eq "0" ]; then
    [ "$adv_max" -lt 4 ] && adv_max=10 && ERROR="$ERROR Maximum router advertisement too small, set it to 10.\n"
    [ "$adv_max" -gt 1800 ] && adv_max=10 && ERROR="$ERROR Maximum router advertisement interval too big, set it to 10.\n"
  else   
     adv_max=10
     ERROR="$ERROR  Maximum router advertisement interval was invalid, set it to 10.\n"
  fi 
  
  [ "$adv_min" -gt "$adv_max" ] && ERROR="$ERROR Minimum router advertisement interval is larger than the maximum." && err=1

 mustsave=0
 if [ "$err" = "0" ]; then
    [ "$ipv6_start" != "`nvram get ipv6_start`" ] && res=`nvram set ipv6_start=$ipv6_start` && mustsave=1
    [ "$use_ula" != "`nvram get ipv6_use_ula`" ] && res=`nvram set ipv6_use_ula=$use_ula` && mustsave=1
    [ "$rad_start" != "`nvram get ipv6_radvd_start`" ] && res=`nvram set ipv6_radvd_start=$rad_start` && mustsave=1
    [ "$adv_min" != "`nvram get ipv6_adv_minint`" ] && res=`nvram set ipv6_adv_minint=$adv_min` && mustsave=1
    [ "$adv_max" != "`nvram get ipv6_adv_maxint`" ] && res=`nvram set ipv6_adv_maxint=$adv_max` && mustsave=1
    [ "$rad_home_agent" = "1" ] && rad_home_agent="on" || rad_home_agent="off"
    [ "$rad_home_agent" != "`nvram get ipv6_adv_mobile`" ] && res=`nvram set ipv6_adv_mobile=$rad_home_agent` && mustsave=1
    [ "$rad_mtu" != "`nvram get ipv6_adv_mtu`" ] && res=`nvram set ipv6_adv_mtu=$rad_mtu` && mustsave=1
    
    [ "$mustsave" -eq 1 ] && nvram commit >/dev/null 
    [ "$action" = "3" ] && /etc/start_scripts/ipv6.sh restart >/dev/null 2>/dev/null 
 fi
fi


ipv6_off='checked="checked"'
ipv6_on=""
ipv6_start=`nvram get ipv6_start`
[ "$ipv6_start" = "Failed" ] && ipv6_start=0
[ "$ipv6_start" = "1" ] && ipv6_on='checked="checked"' && ipv6_off=''

ula_off='checked="checked"'
ula_on=""
use_ula=`nvram get ipv6_use_ula`
[ "$use_ula" = "Failed" ] && use_ula=1
[ "$use_ula" = "1" ] && ula_on='checked="checked"' && ula_off=''
ula_host=`gen_eui64 br0`
ula_prefix=`nvram get ipv6_ula_prefix`
ula=${ula_prefix}":"${ula_host}"/64"

rad_off='checked="checked"'
rad_on=""
rad_start=`nvram get ipv6_radvd_start`
[ "$rad_start" = "Failed" ] && rad_start=1
[ "$rad_start" = "1" ] && rad_on='checked="checked"' && rad_off=''

adv_min=`nvram get ipv6_adv_minint`
[ "$adv_min" = "Failed" ] && adv_min=3
adv_max=`nvram get ipv6_adv_maxint`
[ "$adv_max" = "Failed" ] && adv_max=10
rad_mtu=`nvram get ipv6_adv_mtu`
[ "$rad_mtu" = "Failed" ] && rad_mtu=1280


home_agent_sel=""
home_agent=`nvram get ipv6_adv_mobile`
[ "$home_agent" = "on" ] && home_agent_sel='checked="checked"'


cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
<SCRIPT type="text/JavaScript">
function showErr() {
var err="$ERROR";
if(err!=""){alert(err);}
}
</SCRIPT>
</HEAD>
<BODY onload="showErr();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Network Settings / IPv6</DIV>
<div id="c_titel">Basic settings</div>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0) >
<td width="160">IPv6:</td>
<td width="20"><input type="radio" name="ipv6_start" value="1" $ipv6_on></td>
<td width="40">On</td>
<td width="20"><input type="radio" name="ipv6_start" value="0" $ipv6_off></td>
<td>Off</td>
</tr></table>
</DIV>
<div id="c_leer"></div>
<div id="c_titel">Unique Local Unicast Address (ULA)</div>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0) >
<td width="160">use ULA:</td>
<td width="20"><input type="radio" name="use_ula" value="1" $ula_on></td>
<td width="40">On</td>
<td width="20"><input type="radio" name="use_ula" value="0" $ula_off></td>
<td>Off</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0) >
<td width="160">ULA prefix:</td>
<td>$ula_prefix</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0) >
<td width="160">ULA address:</td>
<td>$ula</td>
</tr></table>
</DIV>
<div id="c_leer"></div>
<div id="c_titel">Router Advertisement</div>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(5) onmouseout=i_showElem(0) >
<td width="160">send Router Advertisements:</td>
<td width="20"><input type="radio" name="rad_start" value="1" $rad_on></td>
<td width="40">On</td>
<td width="20"><input type="radio" name="rad_start" value="0" $rad_off></td>
<td>Off</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(6) onmouseout=i_showElem(0) >
<td width="160">min. advertisement interval:</td><td>
<input type="input" name="adv_min" size="25" maxlength="4" value="$adv_min"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(7) onmouseout=i_showElem(0) >
<td width="160">max. advertisement interval:</td><td>
<input type="input" name="adv_max" size="25" maxlength="4" value="$adv_max"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(8) onmouseout=i_showElem(0) >
<td width="160">Link MTU:</td><td>
<input type="input" name="rad_mtu" size="25" maxlength="4" value="$rad_mtu"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(9) onmouseout=i_showElem(0) >
<td width="160">Home Agent:</td><td>
<input type="checkbox" name="rad_home_agent" value="1" $home_agent_sel></td>
</tr></table>
</DIV>
<DIV id=c_leer></DIV>
<DIV id="c_foot"></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV></DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Basic settings</h2>
<p>Setup the basic IPv6 settings.</p>
</DIV>
<DIV id=i_content1>
<h2>Basic settings</h2>
<b>IPv6</b>
<p>Turn IPv6 on or off.</p>
</DIV>
<DIV id=i_content2>
<h2>Unique Local Unicast Address</h2>
<b>Unique Local Unicast Address</b>
<p>The Unique Local IPv6 Unicast Addresses (ULA) is a address only for local use.
It is locally generated with a globally unique prefix. ULA replace the deprecated
site-local addresses. For more infomation see RFC4193.
</p>
<p>Default: on</p>
</DIV>
<DIV id=i_content3>
<h2>Unique Local Unicast Address</h2>
<b>ULA prefix</b>
<p>The locally generated ULA prefix.</p>
</DIV>
<DIV id=i_content4>
<h2>Unique Local Unicast Address</h2>
<b>ULA address</b>
<p>The ULA address of this router.</p>
</DIV>
<DIV id=i_content5>
<h2>Router Advertisement</h2>
<b>send Router Advertisements</b>
<p>Indicating whether or not the router sends periodic router advertisements and 
responds to router solicitations. This is needed for IPv6 auto configuration.
</p>
<p>Default: on</p>
</DIV>
<DIV id=i_content6>
<h2>Router Advertisement</h2>
<b>min. advertisement interval</b>
<p>
The minimum time allowed between sending unsolicited multicast router 
advertisements in seconds. 
</p>
<p>Values: 3-1350</p>
<p>Default: 3</p>
</DIV>
<DIV id=i_content7>
<h2>Router Advertisement</h2>
<b>max. advertisement interval</b>
<p>The maximum time allowed between sending unsolicited multicast router 
advertisements in seconds.</p>
<p>Values: 4-1800</p>
<p>Default: 10</p>
</DIV>
<DIV id=i_content8>
<h2>Router Advertisement</h2>
<b>Link MTU</b>
<p>The MTU is used in router advertisement messages to insure that all 
nodes on a link use the same MTU value in those cases where the link MTU 
is not well known. </p>
<p>Values: 1280-1500</p>
<p>Default: 1280</p>
</DIV>
<DIV id=i_content9>
<h2>Router Advertisement</h2>
<b>Home Agent</b>
<p>When set, indicates that sending router is able to serve as Mobile IPv6 Home 
Agent. When set, minimum limits specified by Mobile IPv6 are used for 
min. and max. advertisement interval. </p>
<p>Default: off</p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML

a=0

