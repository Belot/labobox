#!/bin/sh

action=0

. ./login.cgi

f_pfad="Firewall Settings / Protocol forwarding"
f_name="Protocol forwarding"
HOME="fw_index.cgi"
. ./functions.sh

PATH=.:$PATH

errmsg=""
mustsave=0

########################### Add #####################################
if [ "$action" -eq 10 ]; then
  rule_list=`nvram get fw_prot_forwarding`
  [ "$rule_list" = "Failed" ] && rule_list=""
  rule_list=$rule_list"###;"
  nvram set fw_prot_forwarding="$rule_list" >/dev/null
fi

################################## Del #####################################
if [ "$action" -eq 11 ] && [ "$rule_count" != "0" ]; then
  rule_list=`nvram get fw_prot_forwarding`
  [ "$rule_list" = "Failed" ] && rule_list=""
  rule_list=`echo $rule_list|sed -e 's/ /%20/g'|sed -e 's/;/ /g'`
  i=0
  new_list=""
  for rule in $rule_list
  do
    rule=`echo $rule|sed -e 's/%20/ /g'`
    [ "$i" = "$del_number" ] && let "i+=1" && continue
    new_list=$new_list"$rule;"
    let "i+=1"
  done
  nvram set fw_prot_forwarding="$new_list" >/dev/null
  nvram commit >/dev/null
fi

####### Save & Run #######
if [ "$rule_count" != "0" ] && ( [ "$action" -eq 2 ] || [ "$action" -eq 3 ] ); then

  #now go through all single port rules
  i=0
  myerr=0
  while [ $i -lt $rule_count ]
  do
    eval "prot=\$text_prot$i"
    prot=`echo $prot|sed -e 's/[^0-9]//g'`
    if [ "$prot" -lt "1" -o "$prot" -gt "255" ]; then 
      errmsg="$errmsg Protocol number in rule $i is invalid!"'\n'
      myerr=1
    fi
    eval "ip=\$text_privip$i"
    ip=`isip.sh $ip `
    [ "$ip" = "ERROR" ] && errmsg="$errmsg Invalid Private IP-address in rule $i!"'\n' && myerr=1
    http_unquote "text_name$i"
    eval "name=\$text_name$i"
    eval "ena=\$check_ena$i"
    rule="$name#$prot#$ip#$ena"
    new_list=$new_list"$rule;"
    let "i+=1"
  done
  [ "$myerr" = "0" ] && res=`nvram set fw_prot_forwarding="$new_list"` && mustsave=1 && [ "$res" != "OK" ] && err=1

  [ "$err" -ne 0 ] && errmsg="$errmsg Write to NVRam failed!"'\n'

  [ "$mustsave" -eq 1 ] && nvram commit >/dev/null && [ "$action" -eq 3 ] && /etc/start_scripts/firewall.sh protfw_restart >/dev/null 2>/dev/null
elif [ "$rule_count" = "0" ] && [ "$action" -eq 3 ]; then
  /etc/start_scripts/firewall.sh protfw_restart
fi

ON='checked=\"checked\"'
SEL="selected"

rule_list=`nvram get fw_prot_forwarding`
[ "$rule_list" = "Failed" ] && rule_list=""
rule_list=`echo $rule_list|sed -e 's/ /%20/g'|sed -e 's/;/ /g'`
#go through each rule

cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function showErr() {
  var err="$errmsg";
  if(err!=""){alert(err);}
}
function doAdd() {
  load();
  document.getElementById("action").value=10;
  document.getElementById("xform").submit();
}
function doDel(number) {
  var count=document.getElementById("rule_count").value;
  if(count==0){
    alert("There's no rule to delete!");
    return;
  }
  else{
    load();
    document.getElementById("action").value=11;
    document.getElementById("del_number").value=number;
    document.getElementById("xform").submit();
  }
}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>$f_pfad</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<DIV id=c_titel>Protocol forwarding rules</DIV>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<th width="30">Nr</th>
<th width="150">Rule name</th>
<th width="150">Protocol number</th>
<th width="120">Private IP</th>
<th width="50">Enabled</th>
<th></th>
</tr>
HTML
i=0
if [ "$rule_list" = "" ]; then
  echo '<tr><td colspan="5" align="center">no rules specified</td></tr>'
else
  for rule in $rule_list
  do
    rule=`echo $rule|sed -e 's/%20/ /g'`
    prot=`echo $rule|sed -e 's/.*#\(.*\)#.*#.*/\1/g'`
    name=`echo $rule|sed -e 's/\(.*\)#.*#.*#.*/\1/g'`
    dst_ip=`echo $rule|sed -e 's/.*#.*#\(.*\)#.*/\1/g'`
    ena=`echo $rule|sed -e 's/.*#.*#.*#\(.*\)/\1/g'`
    check_ena_on=""
    [ "$ena" = "1" ] && check_ena_on=$ON
    echo '<tr>'
    echo "<td><input id=\"text_nr$i\" name=\"text_nr$i\" size=\"3\" maxlength=\"4\" type=\"text\" value=\"$i\" disabled></td>"
    echo "<td onmouseover=i_showElem(1) onmouseout=i_showElem(0)><input id=\"text_name$i\" name=\"text_name$i\" size=\"15\" maxlength=\"25\" type=\"text\" value=\"$name\"></td>"
    echo "<td onmouseover=i_showElem(2) onmouseout=i_showElem(0) align=\"center\"><input id=\"text_prot$i\" name=\"text_prot$i\" size=\"5\" maxlength=\"3\" type=\"text\" value=\"$prot\"></td>"
    echo "<td onmouseover=i_showElem(4) onmouseout=i_showElem(0)><input id=\"text_privip$i\" name=\"text_privip$i\" size=\"16\" maxlength=\"16\" type=\"text\" value=\"$dst_ip\"></td>"
    echo "<td align=\"center\" onmouseover=i_showElem(6) onmouseout=i_showElem(0)><input type=\"checkbox\" id=\"check_ena$i\" name=\"check_ena$i\" value=\"1\" $check_ena_on></td>"
    echo "<td onmouseover=i_showElem(8) onmouseout=i_showElem(0) align=\"center\"><input type=\"button\" id=\"btn_del$i\" name=\"btn_del\" value=\"Delete\" onclick=\"doDel($i);\"></td>"
    echo '</tr>'
    let "i+=1"
  done
  [ "$i" = "0" ] && echo '<tr><td colspan="5" align="center">no rules specified</td></tr>'
fi

cat <<HTML
<tr>
<td onmouseover=i_showElem(7) onmouseout=i_showElem(0) colspan="7" align="center"><input type="button" id="btn_add" name="btn_add" value="Add" onclick="doAdd();"></td>
</tr>
</table>
<input id="rule_count" name="rule_count" type="hidden" value="$i">
<input id="del_number" name="del_number" type="hidden" value="$i">
</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Protocol forwarding</h2>
<p>Configure special protocol numbers which should be forwarded to a local machine.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br />This is often needed for internal VPN-servers which using GRE or IPsec.</p>
</DIV>
<DIV id=i_content1>
<h2>Rule name</h2>
<p>Enter a name for this rule. Only for better understanding when you watch this configuration later.</p>
</DIV>
<DIV id=i_content2>
<h2>Protocol number</h2>
<p>Enter IP protocol number which should be forwarded.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Enter Valid Protocol-Number from 1-255<br /><br />e.g. 47=GRE, used by Windows VPN<br />50=ESP, used by IPSec</p>
</DIV>
<DIV id=i_content4>
<h2>Private IP</h2>
<p>Enter the target IP-address where to forward the specified protocol.</p>
</DIV>
<DIV id=i_content6>
<h2>Enabled</h2>
<p>You can <b>enable</b> or <b>disable</b> a rule without deleting it.</p>
</DIV>
<DIV id=i_content7>
<h2>Add</h2>
<p>Add a new empty forwarding rule to the list you can edit.</p>
</DIV>
<DIV id=i_content8>
<h2>Delete</h2>
<p>Delete this rule from the list.</p>
</DIV>
</DIV></BODY></HTML>
