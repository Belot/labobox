#!/bin/sh

action=0

. ./login.cgi

f_pfad="Administration / Web interface"
f_name="Web interface"
HOME="admin_index.cgi"

PATH=.:$PATH

errmsg=""
mustsave=0

#### Formular-Auswertung #######################################################
#SAVE 
if [ "$action" -eq 2 ] || [ "$action" -eq 3 ]; then 
  [ "$form_syslog_start" != "`nvram get syslog_remote`" ] && res=`nvram set syslog_remote=$form_syslog_start` && mustsave=1 && [ "$res" != "OK" ] && err=1
  
  if [ "$form_syslog_start" = "1" ]; then
    if [ "$form_syslog_port" -ge "1" -o "$form_syslog_port" -le "65534" ]; then
      [ "$form_syslog_port" != "`nvram get syslog_remote_port`" ] && res=`nvram set syslog_remote_port=$form_syslog_port` && mustsave=1 && [ "$res" != "OK" ] && err=1
    else
      errmsg="$errmsg Invalid port number"'\n'
    fi

    [ "$form_syslog_ip" != "`nvram get syslog_remote_ip`" ] && res=`nvram set syslog_remote_ip=$form_syslog_ip` && mustsave=1 && [ "$res" != "OK" ] && err=1
  fi
  
  
  [ "$err" -ne "0" ] && errmsg="$errmsg Write to NVRam failed!"'\n'

  [ "$mustsave" -eq 1 ] && nvram commit >/dev/null && [ "$action" -eq 3 ] && /etc/start_scripts/syslog.sh restart >/dev/null 2>/dev/null
fi
################################################################################

[ "$mustsave" -eq 1 ] && nvram commit >/dev/null

syslog_start=`/bin/nvram get syslog_remote`
[ "$syslog_start" = "Failed" ] && syslog_start=0
[ "$syslog_start" = "1" ] && syslog_on='checked="checked"' || syslog_off='checked="checked"'

syslog_port=`/bin/nvram get syslog_remote_port`
[ "$syslog_port" = "Failed" ] && syslog_port="514"

syslog_ipaddr=`nvram get syslog_remote_ip`
[ "$syslog_ipaddr" = "Failed" ] && syslog_ipaddr=""


cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>

<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function syslog_change() {
  if(document.getElementById("form_syslog_start_off").checked==true){
    document.getElementById("form_syslog_port").disabled=true;
    document.getElementById("form_syslog_ip").disabled=true;
}
  else{
    document.getElementById("form_syslog_port").disabled=false;
    document.getElementById("form_syslog_ip").disabled=false;
}
}
function showErr() { 
  var err="$errmsg";
  if(err!=""){alert(err);} 
}
function subm(x){
  if(document.getElementById("form_syslog_start_on").value="1") {
    var ip=document.getElementById("form_syslog_ip").value;
    var port=document.getElementById("form_syslog_port").value;

    if(ip==""){alert("You have to enter an IP address!");return;}
    if(port==""){alert("You have to enter a port number!");return;}
    if(!isValidIpAddress(ip)){alert("IP address is invalid!");return;}
  }
  load();
  document.getElementById("action").value=x;
  document.getElementById("xform").submit();
}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();syslog_change();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>$f_pfad</DIV>
<DIV id=c_titel>Syslog server</DIV>
<DIV id=c_std>
<form id="xform" name="xform" method="post">
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0)>
<td width="200">Syslog server</td>
<td width="20"><input type="radio" id="form_syslog_start_off" name="form_syslog_start" value="0" onchange="syslog_change();" $syslog_off></td>
<td width="40">Off</td>
<td width="20"><input type="radio" id="form_syslog_start_on" name="form_syslog_start" value="1" onchange="syslog_change();" $syslog_on></td>
<td>On</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(2) onmouseout=i_showElem(0)>
<td width="200">Syslog server IP-address</td>
<td><input type="text" id="form_syslog_ip" name="form_syslog_ip" size="15" maxlength="15" value="$syslog_ipaddr"></td>
<input id="action" type="hidden" name="action" value="0">
</tr></table>
</DIV>
<DIV id=c_last>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0)>
<td width="200">Syslog server port</td>
<td ><input type="text" id="form_syslog_port" name="form_syslog_port" size="5" maxlength="5" value="$syslog_port"></td>
</tr></table>
</DIV>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover="rahmen(1,3);i_showElem(4)" onmouseout="rahmen(0,3);i_showElem(0)">Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover="rahmen(1,4);i_showElem(5)" onmouseout="rahmen(0,4);i_showElem(0)">Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Syslog server</h2>
<p>Configure to use a syslog server</p>
</DIV>
<DIV id=i_content1>
<h2>Syslog server</h2>
<p><b>Yes/No</b><br>Use a syslog server Yes/No</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Setting is checked on next Reboot</p>
</DIV>
<DIV id=i_content2>
<h2>Syslog server</h2>
<p><b>Syslog server IP</b><br>Enter the IP-address of the syslog server to use.</p>
</DIV>
<DIV id=i_content3>
<h2>Syslog server</h2>
<p><b>Syslog server port</b><br>Set on which UDP-Port the remote syslog server listens</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Enter Valid Port-Number from 1-65534, default=514</p>
</DIV>
<DIV id=i_content4>
<h2>Syslog server</h2>
<p><b>Save</b><br>Saves changed settings to NVRAM</p>
</DIV>
<DIV id=i_content5>
<h2>Web interface</h2>
<p><b>Save&amp;Run</b><br>Save changed settings to NVRAM and restart syslogd with new settings</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Changes are safed permanently</p>
</DIV>
</DIV></BODY></HTML>

