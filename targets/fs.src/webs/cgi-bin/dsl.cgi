#!/bin/sh

HOME="wan_index.cgi"
ANNEX_AB=0
[ -f /etc/adsl/adsl_phy_A.bin ] && [ -f /etc/adsl/adsl_phy_B.bin ] && ANNEX_AB=1

. ./login.cgi
. ./functions.sh

############################## Save & Run ##################################
if [ "$action" -eq 3 ] || [ "$action" -eq 2 ]; then

  atm_restart=0


  vpi=`echo $vpi|sed -e 's/\([^0-9]\)//g'`
  [ "$vpi" = "" ] && vpi=1
  [ $vpi -lt 0 ] && vpi=1
  [ $vpi -gt 255 ] && vpi=1

  vci=`echo $vci|sed -e 's/\([^0-9]\)//g'`
  [ "$vci" = "" ] && vci=32
  [ $vci -lt 32 ] && vci=32
  [ $vci -gt 35535 ] && vci=32

  vpi_sec=`echo $vpi_sec|sed -e 's/\([^0-9]\)//g'`
  [ "$vpi_sec" = "" ] && vpi_sec=1
  [ $vpi_sec -lt 0 ] && vpi_sec=1
  [ $vpi_sec -gt 255 ] && vpi_sec=1

  vci_sec=`echo $vci_sec|sed -e 's/\([^0-9]\)//g'`
  [ "$vci_sec" = "" ] && vci_sec=34
  [ $vci_sec -lt 32 ] && vci_sec=34
  [ $vci_sec -gt 35535 ] && vci_sec=34

  if [ "$err" -eq 0 ]; then
  err=0
  mustsave=0
  [ "$dsl_start" != "`nvram get dsl_start`" ] && res=`nvram set dsl_start=$dsl_start` && mustsave=1 && atm_restart=1 && [ "$res" != "OK" ] && err=1
  [ "$vpi" != "`nvram get atm_vpi`" ] && res=`nvram set atm_vpi=$vpi` && mustsave=1 && atm_restart=1 && [ "$res" != "OK" ] && err=1
  [ "$vci" != "`nvram get atm_vci`" ] && res=`nvram set atm_vci=$vci` && mustsave=1 && atm_restart=1 && [ "$res" != "OK" ] && err=1
  [ "$aal" != "`nvram get atm_aal`" ] && res=`nvram set atm_aal=$aal` && mustsave=1 && atm_restart=1 && [ "$res" != "OK" ] && err=1

  [ "$atm_sec" != "`nvram get atm_sec`" ] && res=`nvram set atm_sec=$atm_sec` && mustsave=1 && atm_restart=1 && [ "$res" != "OK" ] && err=1
  [ "$vpi_sec" != "`nvram get atm_vpi_sec`" ] && res=`nvram set atm_vpi_sec=$vpi_sec` && mustsave=1 && atm_restart=1 && [ "$res" != "OK" ] && err=1
  [ "$vci_sec" != "`nvram get atm_vci_sec`" ] && res=`nvram set atm_vci_sec=$vci_sec` && mustsave=1 && atm_restart=1 && [ "$res" != "OK" ] && err=1
  [ "$aal_sec" != "`nvram get atm_aal_sec`" ] && res=`nvram set atm_aal_sec=$aal_sec` && mustsave=1 && atm_restart=1 && [ "$res" != "OK" ] && err=1

  if [ $ANNEX_AB -eq 1 ]; then
    [ "$annex" != "`nvram get dsl_annex`" ] && res=`nvram set dsl_annex=$annex` && mustsave=1 && errmsg="Reboot the router to make the changes working."  && atm_restart=1 && [ "$res" != "OK" ] && err=1
  fi
  [ "$trellis" != "`nvram get dsl_trellis`" ] && res=`nvram set dsl_trellis=$trellis` && mustsave=1 && atm_restart=1 && [ "$res" != "OK" ] && err=1
  [ "$bitswap" != "`nvram get dsl_bitswap`" ] && res=`nvram set dsl_bitswap=$bitswap` && mustsave=1 && atm_restart=1 && [ "$res" != "OK" ] && err=1
  [ "$sra" != "`nvram get dsl_sra`" ] && res=`nvram set dsl_sra=$sra` && mustsave=1 && atm_restart=1 && [ "$res" != "OK" ] && err=1
  [ "$form_dsl_lpair" != "`nvram get dsl_lpair`" ] && res=`nvram set dsl_lpair=$form_dsl_lpair` && mustsave=1 && atm_restart=1 && [ "$res" != "OK" ] && err=1
  [ "$snr" != "`nvram get dsl_snr`" ] && res=`nvram set dsl_snr=$snr` && mustsave=1 && atm_restart=1 && [ "$res" != "OK" ] && err=1
  [ "$sel_dslmod" != "`nvram get dsl_mod`" ] && res=`nvram set dsl_mod=$sel_dslmod` && mustsave=1 && atm_restart=1 && [ "$res" != "OK" ] && err=1
  

  [ "$mustsave" -eq 1 ] && nvram commit >/dev/null 
  [ "$err" -ne 0 ] && errmsg="$errmsg Write to NVRam failed!"'\n'

  if [ "$atm_restart" = "1" ]; then
    [ "$action" -eq 3 ] && /etc/start_scripts/dsl.sh restart >/dev/null 2>/dev/null
  else
    [ "$action" -eq 3 ] && /etc/start_scripts/dsl.sh ppp_restart >/dev/null 2>/dev/null
  fi

  fi
fi

dsl_off='checked="checked"'
dsl_on=""
dsl_start=`nvram get dsl_start`
[ "$dsl_start" = "1" ] && dsl_on='checked="checked"' && dsl_off=''

aal_sel1='selected="checked"'
aal_sel2=''
aal_sel3=''
aal_sel4=''
aal_sel5=''
atm_aal=`nvram get atm_aal`
[ "$atm_aal" = "aal2" ] && aal_sel1='' && aal_sel2='selected="checked"'
[ "$atm_aal" = "aal0pkt" ] && aal_sel1='' && aal_sel3='selected="checked"'
[ "$atm_aal" = "aal0cell" ] && aal_sel1='' && aal_sel4='selected=checked"'
[ "$atm_aal" = "aaltransparent" ] && aal_sel1='' && aal_sel5='selected="checked"'

aal_sec_sel1='selected="checked"'
aal_sec_sel2=''
aal_sec_sel3=''
aal_sec_sel4=''
aal_sec_sel5=''
$atm_aal=`nvram get atm_aal`
[ "$atm_aal" = "aal2" ] && aal_sec_sel1='' && aal_sec_sel2='selected="checked"'
[ "$atm_aal" = "aal0pkt" ] && aal_sec_sel1='' && aal_sec_sel3='selected="checked"'
[ "$atm_aal" = "aal0cell" ] && aal_sec_sel1='' && aal_sec_sel4='selected=checked"'
[ "$atm_aal" = "aaltransparent" ] && aal_sec_sel1='' && aal_sec_sel5='selected="checked"'

if [ $ANNEX_AB -eq 1 ]; then
  annex_a=''
  annex_b='checked="checked"'
  annex=`nvram get dsl_annex`
  [ "$annex" = "Failed" ] && annex="B"
  [ "$annex" = "A" ] && annex_b='' &&  annex_a='checked="checked"'
else
  [ -f /etc/adsl/adsl_phy_A.bin ] && annex_b='' &&  annex_a='checked="checked"'
  [ -f /etc/adsl/adsl_phy_B.bin ] && annex_a='' &&  annex_b='checked="checked"'
fi

trellis_on='checked="checked"'
trellis_off=''
dsl_trellis=`nvram get dsl_trellis`
[ "$dsl_trellis" = "Failed" ] && dsl_trellis=1
[ "$dsl_trellis" = "0" ] && trellis_on='' &&  trellis_off='checked="checked"'

bitswap_on='checked="checked"'
bitswap_off=''
dsl_bitswap=`nvram get dsl_bitswap`
[ "$dsl_bitswap" = "Failed" ] && dsl_bitswap=1
[ "$dsl_bitswap" = "0" ] && bitswap_on='' &&  bitswap_off='checked="checked"'

sra_on='checked="checked"'
sra_off=''
dsl_sra=`nvram get dsl_sra`
[ "$dsl_sra" = "Failed" ] && dsl_sra=0
[ "$dsl_sra" = "0" ] && sra_on='' &&  sra_off='checked="checked"'

dsl_snr=`nvram get dsl_snr`
[ "$dsl_snr" = "Failed" ] && dsl_snr=100

atm_vpi=`nvram get atm_vpi`
[ "$atm_vpi" = "Failed" ] && atm_vpi=1
atm_vci=`nvram get atm_vci`
[ "$atm_vci" = "Failed" ] && atm_vci=32

atm_vpi_sec=`nvram get atm_vpi_sec`
[ "$atm_vpi_sec" = "Failed" ] && atm_vpi_sec=1
atm_vci_sec=`nvram get atm_vci_sec`
[ "$atm_vci_sec" = "Failed" ] && atm_vci_sec=34


ON='checked=\"checked\"'
set_select()
{
  var=$1
  default=$2
  value=`nvram get $var`
  [ "$value" = "Failed" ] && value=$default 
  if [ $value -eq 1 ]; then
    eval `echo ${var}_on=$ON`
    eval `echo ${var}_off=''`
  else
    eval `echo ${var}_on=''`
    eval `echo ${var}_off=$ON`
  fi
}

set_select "atm_sec" 0

SEL="selected"

dsl_mod=`nvram get dsl_mod`
[ "$dsl_mod" = "Failed" ] && dsl_mod="a"

case "$dsl_mod" in
                a)
                    sel_dslmod_all=$SEL
                    ;;
                d)
                    sel_dslmod_dmt=$SEL
                    ;;
                l)
                    sel_dslmod_lite=$SEL
                    ;;
                t)
                    sel_dslmod_t1=$SEL
                    ;;
                2)
                    sel_dslmod_adsl2=$SEL
                    ;;
                p)
                    sel_dslmod_adsl2plus=$SEL
                    ;;
                2m)
                    sel_dslmod_adsl2_m=$SEL
                    ;;
                pm)
                    sel_dslmod_adsl2plus_m=$SEL
                    ;;

                *)
                    sel_dslmod_all=$SEL
                    ;;
esac

dsl_lpair=`nvram get dsl_lpair`
[ "$dsl_lpair" = "Failed" ] && dsl_lpair="i"
[ "$dsl_lpair" = "o" ] && form_dsl_lpair_o='checked="checked"' || form_dsl_lpair_i='checked="checked"'


cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function subm(x){
  if(document.getElementById("atm_sec_on").value="1") {
    var vpi=document.getElementById("vpi").value;
    var vci=document.getElementById("vci").value;
    var vpi2=document.getElementById("vpi_sec").value;
    var vci2=document.getElementById("vci_sec").value;
    if(vpi==vpi2 && vci==vci2){
      alert("VPI and VCI of main and secondary ATM-connection must not be the same!");
      return;
    }
  }
  load();
  document.getElementById("action").value=x;
  document.getElementById("xform").submit();
}
function showErr() {
var err="$errmsg";
if(err!=""){alert(err);}
if( document.getElementById("persistent")!=undefined ){
  js_permanent(document.getElementById("persistent").checked);
 }
}
function js_permanent(state) {
if(state) { 
document.getElementById("tr_autoconnect").style.visibility="hidden";
document.getElementById("tr_timeout").style.visibility="hidden";
}
else {
document.getElementById("tr_autoconnect").style.visibility="visible";
document.getElementById("tr_timeout").style.visibility="visible";
}
}
function do_pass_show(state)
{
  if(state) {
    document.getElementById("pass").type="text";
  }
  else {
    document.getElementById("pass").type="password";
  }
}
function atm_sec_change() {
  if(document.getElementById("atm_sec_off").checked==true){
    document.getElementById("vpi_sec").disabled=true;
    document.getElementById("vci_sec").disabled=true;
    document.getElementById("aal_sec").disabled=true;
}
  else{
    document.getElementById("vpi_sec").disabled=false;
    document.getElementById("vci_sec").disabled=false;
    document.getElementById("aal_sec").disabled=false;
}
}

</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();atm_sec_change();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Network Settings / DSL/WAN</DIV>
<DIV id=c_titel>DSL Settings</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0) >
<td width="150">DSL/WAN:</td>
<td width="20"><input type="radio" name="dsl_start" value="1" $dsl_on></td>
<td width="40">On</td>
<td width="20"><input type="radio" name="dsl_start" value="0" $dsl_off></td>
<td>Off</td>
</tr></table>
</DIV>

<DIV id=c_leer></div>
<DIV id=c_titel>Primary ATM connection</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(10) onmouseout=i_showElem(0) >
<td width="150">VPI:</td>
<td><input id="vpi" name="vpi" size="10" maxlength="4" type="text" value="$atm_vpi"></td>
</tr></table></DIV>

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(11) onmouseout=i_showElem(0) >
<td width="150">VCI:</td>
<td><input id="vci" name="vci" size="10" maxlength="4" type="text" value="$atm_vci"></td>
</td></tr></table></DIV>

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(12) onmouseout=i_showElem(0) >
<td width="150">AAL type:</td>
<td><select name="aal">
<option value="aal5" $aal_sel1>aal5</option>
<option value="aal2" $aal_sel2>aal2</option>
<option value="aal0pkt" $aal_sel3>aal0pkt</option>
<option value="aal0cell" $aal_sel4>aal0cell</option>
<option value="aaltransparent" $aal_sel5>aaltransparent</option>
</select>
</td></tr></table></DIV> 

<DIV id=c_leer></div>
<DIV id=c_titel>Secondary ATM connection</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(20) onmouseout=i_showElem(0)><td width="150">Secondary ATM</td>
<td width="20"><input type="radio" id="atm_sec_off" name="atm_sec" value="0" $atm_sec_off onchange="atm_sec_change();"></td>
<td width="40">OFF</td>
<td width="20"><input type="radio" id="atm_sec_on" name="atm_sec" value="1" $atm_sec_on onchange="atm_sec_change();"></td>
<td>ON</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(10) onmouseout=i_showElem(0) >
<td width="150">VPI:</td>
<td><input id="vpi_sec" name="vpi_sec" size="10" maxlength="4" type="text" value="$atm_vpi_sec"></td>
</tr></table></DIV>

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(11) onmouseout=i_showElem(0) >
<td width="150">VCI:</td>
<td><input id="vci_sec" name="vci_sec" size="10" maxlength="4" type="text" value="$atm_vci_sec"></td>
</td></tr></table></DIV>

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(12) onmouseout=i_showElem(0) >
<td width="150">AAL type:</td>
<td><select id="aal_sec" name="aal_sec">
<option value="aal5" $aal_sec_sel1>aal5</option>
<option value="aal2" $aal_sec_sel2>aal2</option>
<option value="aal0pkt" $aal_sec_sel3>aal0pkt</option>
<option value="aal0cell" $aal_sec_sel4>aal0cell</option>
<option value="aaltransparent" $aal_sec_sel5>aaltransparent</option>
</select>
</td></tr></table></DIV> 


<DIV id=c_leer></div>
<DIV id=c_titel>Advanced DSL settings</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(19) onmouseout=i_showElem(0) >
<td width="150">Annex:</td>
EOF_HTML

if [ $ANNEX_AB -eq 1 ]; then
  echo "<td width=\"20\"><input type=\"radio\" name=\"annex\" value=\"A\" $annex_a></td>"
  echo '<td width="40">A</td>'
  echo "<td width=\"20\"><input type=\"radio\" name=\"annex\" value=\"B\" $annex_b></td>"
  echo '<td>B</td>'
else
  echo "<td width=\"20\"><input type=\"radio\" name=\"annex\" value=\"A\" $annex_a disabled></td>"
  echo '<td width="40">A</td>'
  echo "<td width=\"20\"><input type=\"radio\" name=\"annex\" value=\"B\" $annex_b disabled></td>"
  echo '<td>B</td>' 
fi

cat << EOF_HTML
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(18) onmouseout=i_showElem(0)>
<td width="150">Modulation mode:</td>
<td>
  <select name="sel_dslmod" size="1">
    <option value="a" $sel_dslmod_all>All modulations allowed (Multimode)</option>
    <option value="d" $sel_dslmod_dmt>G.DMT - adsl1</option>
    <option value="l" $sel_dslmod_lite>G.lite - adsl1</option>
    <option value="t" $sel_dslmod_t1>T1.413 - adsl1 </option>
    <option value="2" $sel_dslmod_adsl2>ADSL2</option>
    <option value="p" $sel_dslmod_adsl2plus>ADSL2+</option>
    <option value="2m" $sel_dslmod_adsl2_m>ADSL2 - with Annex M</option>
    <option value="pm" $sel_dslmod_adsl2plus_m>ADSL2+ - with Annex M</option>
  </select>
</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(13) onmouseout=i_showElem(0) >
<td width="150">Trellis:</td>
<td width="20"><input type="radio" name="trellis" value="1" $trellis_on></td>
<td width="40">On</td>
<td width="20"><input type="radio" name="trellis" value="0" $trellis_off></td>
<td>Off</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(15) onmouseout=i_showElem(0) >
<td width="150">Bit swap:</td>
<td width="20"><input type="radio" name="bitswap" value="1" $bitswap_on></td>
<td width="40">On</td>
<td width="20"><input type="radio" name="bitswap" value="0" $bitswap_off></td>
<td>Off</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(16) onmouseout=i_showElem(0) >
<td width="150">SRA:</td>
<td width="20"><input type="radio" name="sra" value="1" $sra_on></td>
<td width="40">On</td>
<td width="20"><input type="radio" name="sra" value="0" $sra_off></td>
<td>Off</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(17) onmouseout=i_showElem(0)>
<td width="150">Loop pair:</td>
<td width="20"><input type="radio" name="form_dsl_lpair" value="i" $form_dsl_lpair_i"></td>
<td width="40">inner</td>
<td width="20"><input type="radio" name="form_dsl_lpair" value="o" $form_dsl_lpair_o"></td>
<td>outer</td>
</tr></table>
</DIV>

<DIV id=c_last>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(21) onmouseout=i_showElem(0) >
<td width="150">Target SNR:</td>
<td><input id="snr" name="snr" size="10" maxlength="4" type="text" value="$dsl_snr"></td>
</td></tr></table></DIV>

</form>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save </DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run </DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>DSL/WAN</h2>
<p>Setup your DSL connection.</p>
</DIV>
<DIV id=i_content1>
<h2>Basic settings</h2>
<p><b>DSL/WAN</b><br>
Use DSL or turn the DSL/ATM modem off.</p>
<p><b>On</b> DSL/ATM is on and the DSL connection can be used.<p>
<p><b>Off</b> The DSL/ATM modem is off.<p>
</DIV>
<DIV id=i_content2>
<h2>Basic settings</h2>
<p><b>User name</b><br>
<p>Enter your user name for the DSL connection.</p>
</DIV>
<DIV id=i_content3>
<h2>Basic settings</h2>
<p><b>Password</b><br>
<p>Enter your password for the DSL connection.</p>
</DIV>
<DIV id=i_content4>
<h2>Basic settings</h2>
<p><b>Static IP Address</b><br>
<p>If you have an static IP address enter it here. If you get an dynamic IP address empty this field.</p>
</DIV>
<DIV id=i_content5>
<h2>Basic settings</h2>
<p><b>Persistent connection</b><br>
<p>Enable this if you want to keep the connection unlimited alive.</p>
</DIV>
<DIV id=i_content6>
<h2>Basic settings</h2>
<p><b>Automatic reconnect</b><br>
<p></p>
</DIV>
<DIV id=i_content7>
<h2>Basic settings</h2>
<p><b>Idle timeout</b><br>
<p>After idle timeout the connection is closed. The idle timeout is a time in seconds.</p>
<p>Valid values: 30-3600</p>
</DIV>
<DIV id=i_content8>
<h2>Advanced PPP settings</h2>
<p><b>MTU</b><br>
<p>Maximum Transfer Unit (MTU) for the PPP link.</p>
<p>Valid values: 512-9000</p>
<p>Default: 1492</p>
</DIV>
<DIV id=i_content9>
<h2>Advanced PPP settings</h2>
<p><b>Authentication Method</b><br>
<p><b>Auto</b> - automatically choose the best method.</p>
<p><b>PAP</b> - Password Authentication Protocol</p>
<p><b>CHAP</b> - Challenge Handshake Authentication Protocol</p>
<p><b>MSCHAP</b> - Microsoft Challenge Handshake Authentication Protocol</p>
<p>Default: <b>Auto</b></p>
</DIV>
<DIV id=i_content10>
<h2>Advanced ATM settings</h2>
<p><b>VPI</b><br>
<p>The ATM Virtual Path Identifier (VPI)</p>
<p>Valid values: 0-255</p>
<p>Default: 1</p>
</DIV>
<DIV id=i_content11>
<h2>Advanced ATM settings</h2>
<p><b>VCI</b><br>
<p>The ATM Virtual Channel Identifer</p>
<p>Valid values: 32-65535</p>
<p>Default: 32</p>
</DIV>
<DIV id=i_content12>
<h2>Advanced ATM settings</h2>
<p><b>AAL Type</b><br>
<p>ATM Adaptation Layer (AAL)</p>
<p>Default: aal5</p>
</DIV>
<DIV id=i_content13>
<h2>Advanced DSL settings</h2>
<p><b>Trellis</b><br>
<p></p>
<p>Default: on</p>
</DIV>
<DIV id=i_content14>
<h2>Basic settings</h2>
<p><b>DNS 1-3</b><br>
<p>Enter static DNS servers here.</p>
<p>To use dynamic DNS server detected by PPP daemon free this fields.</p>
</DIV>
<DIV id=i_content15>
<h2>Advanced DSL settings</h2>
<p><b>Bitswap</b><br>
<p>
The bit swap protocol re-deploys the allocation of bits
among the sub-carriers with no retrain of the modems
or change in the net data rates.
</p>
<p>Default: on</p>
</DIV>
<DIV id=i_content16>
<h2>Advanced DSL settings</h2>
<p><b>SRA</b><br>
<p>SRA - Seamless Rate Adaptation</p>
<p>Default: off</p>
</DIV>
<DIV id=i_content17>
<h2>Advanced DSL settings</h2>
<p><b>Loop pair</b><br>
<p>Sets the wire pair to use for loop test.</p>
<p>Default: inner</p>
</DIV>
<DIV id=i_content18>
<h2>Advanced DSL settings</h2>
<p><b>Modulation mode</b><br>
<p>Sets the accepted modulation modes on DSL line.</p>
<p>Default: All</p>
</DIV>
<DIV id=i_content20>
<h2>Secondary ATM connection</h2>
<p>Turn Secondary ATM connection <b>off</b> or <b>on</b>.</p>
<p>Default: Off</p>
</DIV>
<DIV id=i_content21>
<h2>Advanced DSL settings</h2>
<p><b>Target SNR</b><br>
<p>Valid values: 10-200</p>
<p>Default: 100</p>
</DIV>
EOF_HTML

if [ $ANNEX_AB -eq 1 ]; then
cat << EOF_HTML
<DIV id=i_content19>
<h2>Advanced DSL settings</h2>
<p><b>Annex</b><br>
<p>Choose the DSL Annex. </p>
<p>In Germany and in Bosnia and Herzegovina is only Annex B used. In all other
countries is mostly Annex A in use. In some countries (Switzerland, Austria, Belgium,
Netherlands and Scandinavia) is both offered.</p>
<p>After Annex switch reboot is necessary!</p>
<p>Default: B</p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML
else
ANNEX="B"
[ -f /etc/adsl/adsl_phy_A.bin ] && ANNEX="A"
cat << EOF_HTML
<DIV id=i_content19>
<h2>Advanced DSL settings</h2>
<p><b>Annex</b><br>
<p>Your image contains only <b>Annex $ANNEX</b> and therefore doesn't support Annex switching.</p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML

fi


