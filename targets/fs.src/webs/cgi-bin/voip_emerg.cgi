#!/bin/sh

from=voip_emerg.cgi

. ./login.cgi

HOME="voip_index.cgi"
. ./functions.sh

 #SAVE
if [ "$action" -eq 3 ] || [ "$action" -eq 2 ]; then 
 commit=0
 i=0
 while [ $i -le 9 ]
 do
  eval number=\$form_emgn$i
  eval desc=\$form_emgd$i

  [ "$number" != "" ] && number=`echo $number|sed -e 's/[^0-9]//g'`
  ret=`nvram get ata_emgn$i`
  if [ "$number" = "" ]; then 
    [ "$ret" != "Failed" ] && hret=`nvram set ata_emgn$i=$number` && commit=1 
  else if [ "$number" != "$ret" ]; then
          hret=`nvram set ata_emgn$i=$number` && commit=1 
        fi
  fi
  
  ret=`nvram get ata_emgd$i`
  if [ "$desc" = "" ]; then 
    [ "$ret" != "Failed" ] && hret=`nvram set ata_emgd$i=$desc` && commit=1 
  else if [ "$desc" != "$ret" ]; then
          http_unquote "desc"
          [ "$desc" != "$ret" ] && hret=`nvram set "ata_emgd$i=$desc"` && commit=1 
        fi
  fi

  let i=$i+1
 done
 [ $commit -eq 1 ] && nvram commit >/dev/null && /etc/start_scripts/ata.sh update_psi >/dev/null 2>/dev/null
fi

# RUN
if [ "$action" -eq 3 ]; then
  [ $commit -eq 1 ] && /etc/start_scripts/ata.sh restart >/dev/null 2>/dev/null
fi

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
EOF_HTML

echo '<SCRIPT language="JavaScript" src="../js_fade.txt"
type="text/JavaScript"></SCRIPT>' \
    '<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>' \
    '<SCRIPT type="text/JavaScript">' \
    'function subm(x) {' \
'load();' \
    'document.getElementById("action").value = x;' \
    'document.getElementById("xform").submit();' \
    '}' \
    '</SCRIPT>' \
    '<LINK rel="stylesheet" href="../style.css" type="text/css">' \
    '<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->' \
    '</HEAD>' \
    '<BODY>' \
    '<DIV id=c_Frame>' \
    '<DIV id=c_border>' \
    '<DIV id=c_pfad>Phone settings / emergency call</DIV>' \
    '<form id="xform" name="xform" method="post" onmouseover=i_showElem(1) onmouseout=i_showElem(0)>' \
    '<input id="action" type="hidden" name="action" value="0">' \
    '<table border="0" cellpadding="0" cellspacing="2" style="text-align: center;" >' \
    '<tr><td width="130">Phone number</td><td width="230">Description</td></tr>' \
    '</table>'
    
i=0
while [ $i -le 9 ]
do  
    number=`nvram get ata_emgn$i`
    desc=`nvram get ata_emgd$i`
    
    [ "$number" = "Failed" ] && number=""
    [ "$desc" = "Failed" ] && desc=""

echo '<DIV id=c_std>' \
    '<table border="0" cellpadding="0" cellspacing="2">' \
    "<td width=\"130\"><input size=\"16\" maxlength=\"16\" value=\"$number\" type=\"text\" name=\"form_emgn$i\" ></td>" \
    "<td width=\"230\"><input size=\"27\" maxlength=\"27\" value=\"$desc\" type=\"text\" name=\"form_emgd$i\" ></td>" \
    '</td></tr></table></DIV>'
    
    let i=$i+1
done  
    
echo '</form>' \
    '<DIV id=c_foot></DIV>' \
    '</DIV>' \
    '<DIV id=c_verzoeg1></DIV>' \
    '<DIV id=c_verzoeg2></DIV>' \
    '</DIV>' \
    '<DIV id=t_Frame>' \
    "<DIV id=t_but1 onclick='window.location.href=\"$HOME\";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>" \
    '<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save &lt;&lt;</DIV>' \
    '<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>' \
    '</DIV>' \
    '<DIV id=i_Frame>' \
    '<DIV id=i_content>' \
    '<h2>emergency call</h2>' \
    '<p>All registered emergency call phone numbers can be called even if they a prohibited by an other rule.</p>' \
    '</DIV>' \
    '<DIV id=i_content1>' \
    '<h2>emergency call</h2>' \
    '<p><b>Phone number</b><br>' \
    'enter here a emergency call phone number</p>' \
    '<p><b>Description</b><br>' \
    'Your description for the phone number</p>' \
    '</DIV>' \
    '</DIV></BODY></HTML>'
