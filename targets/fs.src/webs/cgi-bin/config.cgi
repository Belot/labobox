#!/bin/sh


CONFIG_TMP="/var/config.tmp"

if [ "$REQUEST_METHOD" = "GET" ]; then
  VARS=$QUERY_STRING
else
  VARS=`cat -`
fi
#VARS=`echo $VARS|sed -e 's/\W*$//g'`
VARS=`echo $VARS|tr -d [:space:]`
eval `echo $VARS|sed -e 's/&/\n/g'`


ret=`/bin/nvram saveconfig "key=$key" file=$CONFIG_TMP`
if [ "$ret" != "OK" ]; then
 ERROR=$ret;
 . ./save.cgi
 exit 0;
fi

FILENAME="config-bitswitcher.save"

echo 'Content-Type: application/octet-stream'
echo "Content-Disposition: attachment; filename=\"$FILENAME\""
echo ''
cat $CONFIG_TMP
rm -f $CONFIG_TMP