#!/bin/sh

from=wlan_basic.cgi
action=0

. ./login.cgi

/etc/start_scripts/wlan_present.sh
if [ $? -ne 0 ]; then
# WLAN hardware is not present!
cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY>
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>WLAN configuration</DIV>
<DIV id=c_title></div>
<DIV id=c_std><b style="color: rgb(255, 17, 17);">WLAN hardware is not present!</b></div>
<DIV id=c_leer></DIV>
</DIV>
<DIV id="c_foot"></DIV>
<DIV id=i_Frame>
</DIV>
</DIV></BODY></HTML>
HTML
exit 0
fi

f_pfad="WLAN Settings / Scan"
f_name="Scan"
HOME="wlan_index.cgi"
. ./functions.sh

PATH=.:$PATH

errmsg=""
mustsave=0

if [ "$action" -eq 3 ]; then
  wlctl scan -t active -a 100 >/dev/null 2>/dev/null
  sleep 2
fi

cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function showErr() {
  var err="$errmsg";
  if(err!=""){alert(err);}
}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY>
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>$f_pfad</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<DIV id=c_titel>last scanresults</DIV>
<table border="0" cellpadding="0" cellspacing="2" style="text-align:center;">
<tr>
<th width="200">SSID</th>
<th width="50">Mode</th>
<th width="160">MAC-Adress</th>
<th width="70">Channel</th>
<th width="70">Signal</th>
<th width="90">Security</th>
<th width="120">Cipher</th>
</tr>
HTML

wlctl scanresults 2>&1|awk 'BEGIN{ret=1;}
                       {if(/wlctl: wl driver adapter not found/){ ret=2; exit ret;} 
                        if(/^$/){if(SSID!="")
			{if(SEC==""){SEC=WEP;}
                         print "<tr><td><pre style=\"display:inline;\">" SSID "</pre></td><td>" MODE "</td><td>" MAC "</td><td>" CHANNEL "</td><td>"; 
                         system(". ./functions.sh; HBar2 70 18 " SNR " 40 dBm"); 
                         print "</td><td>" SEC "</td><td>" CIPHER "</td></tr>";
			 ret=0;
			 SSID=""; MODE=""; MAC=""; SIGNAL=""; NOISE=""; SEC=""; CIPHER=""; SNR="";}} 
			 if(/^SSID:/){SSID=$2; gsub(/^"/,"",SSID); gsub(/"$/,"",SSID);}
			 if(/^Mode:/){MODE=$2; SIGNAL=$4; NOISE=$7; if(MODE == "Ad"){MODE="Ad Hoc"; CHANNEL=$11; }else{CHANNEL=$10;} SNR=SIGNAL-NOISE;}
			 if(/^BSSID:/){MAC=$2; if(/WEP/){WEP="WEP or WPA2";}else{WEP="None";}} 
                         if(/unicast ciphers\(/){CIPHER=""; for(i=3; i<=NF; i++){CIPHER=CIPHER " " $(i);}}
			 if(/AKM Suites\(/){SEC=""; for(i=3; i<=NF; i++){SEC=SEC " " $(i);}}
			 }END{exit ret;}'
ret=$?
[ "$ret" = "1" ] && echo '<tr><td colspan="8" align="center">no scan results</td></tr>'
[ "$ret" = "2" ] && echo '<tr><td colspan="8" align="center"><b style="color:#ff1111;">WLAN is off!</b></td></tr>'

cat <<HTML
</table>
</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Scan now!</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Scan</h2>
<p>Scan for other WLAN-networks in your neighbourhood.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br />WLAN has to be enabled in <b>Basic Settings</b> to do a scan.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br />WPA2 is not supported by the Broadcom wireless tool and is showed as WEP.</p>
</DIV>
</DIV></BODY></HTML>
