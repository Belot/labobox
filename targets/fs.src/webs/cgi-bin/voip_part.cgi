#!/bin/sh

from=voip_part.cgi

. ./login.cgi

HOME="voip_index.cgi"
. ./functions.sh

 #SAVE
if [ "$action" -eq 3 ] || [ "$action" -eq 2 ]; then 
 commit=0
 i=0
 while [ $i -le 19 ]
 do
  eval number=\$form_partn$i
  eval line=\$form_partc$i

  [ "$number" != "" ] && number=`echo $number|sed -e 's/[^0-9]//g'`
  ret=`nvram get ata_partn$i`
  if [ "$number" = "" ]; then 
    [ "$ret" != "Failed" ] && hret=`nvram set ata_partn$i=$number` && commit=1 
  else if [ "$number" != "$ret" ]; then
          hret=`nvram set ata_partn$i=$number` && commit=1 
        fi
  fi

  ret=`nvram get ata_partc$i`
  if [ "$line" = "" ]; then 
    [ "$ret" != "Failed" ] && hret=`nvram set ata_partc$i=$line` && commit=1 
  else if [ "$line" != "$ret" ]; then
          hret=`nvram set ata_partc$i=$line` && commit=1 
        fi
  fi
 
  let i=$i+1
 done
 [ $commit -eq 1 ] && nvram commit >/dev/null && /etc/start_scripts/ata.sh update_partial >/dev/null 2>/dev/null
fi

# RUN
if [ "$action" -eq 3 ]; then
  [ $commit -eq 1 ] && /etc/start_scripts/ata.sh restart >/dev/null 2>/dev/null
fi

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
EOF_HTML

echo '<SCRIPT language="JavaScript" src="../js_fade.txt"
type="text/JavaScript"></SCRIPT>' \
    '<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>' \
    '<LINK rel="stylesheet" href="../style.css" type="text/css">' \
    '<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->' \
    '</HEAD>' \
    '<BODY>' \
    '<DIV id=c_Frame>' \
    '<DIV id=c_border>' \
    '<DIV id=c_pfad>Phone settings / dial rules</DIV>' \
    '<form id="xform" name="xform" method="post" onmouseover=i_showElem(1) onmouseout=i_showElem(0)>' \
    '<input id="action" type="hidden" name="action" value="0">' \
    '<table border="0" cellpadding="0" cellspacing="2" style="text-align: center;" >' \
    '<tr><td width="130">Phone number</td><td width="100">Connection</td></tr>' \
    '</table><div style="overflow: auto; height: 350px;">'
    
i=0
SEL="selected"
while [ $i -le 19 ]
do  
    number=`nvram get ata_partn$i`
    line=`nvram get ata_partc$i`
    
    [ "$number" = "Failed" ] && number=""
    [ "$line" = "Failed" ] && line=0

    voip=""
    pstn=""
    [ $line -eq 0 ] && voip=$SEL
    [ $line -eq 1 ] && pstn=$SEL

echo '<DIV id=c_std>' \
    '<table border="0" cellpadding="0" cellspacing="2" style="text-align: center;">' \
    "<tr><td width=\"130\"><input size=\"16\" maxlength=\"16\" value=\"$number\" type=\"text\" name=\"form_partn$i\" ></td>" \
    "<td width=\"100\"><select name=\"form_partc$i\" size=\"1\">" \
    "<option value=\"0\" $voip>VoIP</option><option value=\"1\" $pstn>PSTN</option>" \
    '</select></td></tr></table></DIV>'
    
    let i=$i+1
done  
    
echo '</div></form>' \
    '<DIV id=c_foot></DIV>' \
    '</DIV>' \
    '<DIV id=c_verzoeg1></DIV>' \
    '<DIV id=c_verzoeg2></DIV>' \
    '</DIV>' \
    '<DIV id=t_Frame>' \
    "<DIV id=t_but1 onclick='window.location.href=\"$HOME\";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>" \
    '<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save &lt;&lt;</DIV>' \
    '<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>' \
    '</DIV>' \
    '<DIV id=i_Frame>' \
    '<DIV id=i_content>' \
    '<h2>dial rules</h2>' \
    '<p>Here you can specify which connection (VoIP or PSTN) should be used to call a phone number.</p>' \
    '<p><img src="../pic_i_hinweis.gif" border="0"><br />Due to technical reasons some domains like 0137, 0180, 0190, 0800 and 0900 can only reached via PSTN.</p>' \
    '</DIV>' \
    '<DIV id=i_content1>' \
    '<h2>dial rules</h2>' \
    '<p><b>Phone number</b><br>' \
    'Phone number or the first digits of the phone number.</p>' \
    '<p><b>Connection</b><br>' \
    'Allow to call the phone number via VoIP or PSTN.</p>' \
    '</DIV>' \
    '</DIV></BODY></HTML>'
