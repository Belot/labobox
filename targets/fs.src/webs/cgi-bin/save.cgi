#!/bin/sh

from=save.cgi
action=0

. ./login.cgi

HOME="save_load_index.cgi"

echo 'Content-type: text/html'
echo 'Connection: close'
echo ''
echo '<HTML><HEAD><TITLE>Additional Features</TITLE>'

echo '<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>' \
'<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>' \
'<SCRIPT type="text/JavaScript">' \
'function subm(x) {' \
'document.getElementById("xform").submit();' \
'}' \
'function do_pass_show(state)' \
'{' \
'  if(state) {' \
'    document.getElementById("key").type="text";' \
'  }' \
'  else {' \
'    document.getElementById("key").type="password";' \
'  }' \
'}' \
'function showErr() {' \
"var err=\"$ERROR\";" \
'if(err!=""){alert(err);} }' \
'</SCRIPT>' \
'<LINK rel="stylesheet" href="../style.css" type="text/css">' \
'<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->' \
'</HEAD>' \
'<BODY onload="showErr();">' \
'<DIV id=c_Frame>' \
'<DIV id=c_border>' \
'<DIV id=c_pfad>Save &amp; Load / Save Settings</DIV>' \
'<DIV id=c_titel>Save Settings</DIV>' \
'<form id="xform" name="xform" method="post" action="config.cgi">' \
'<DIV id=c_last>' \
'<table border="0" cellpadding="0" cellspacing="2">' \
'<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0) >' \
'<td width="90">Password:</td>' \
'<td><input id="key" name="key" size="22" maxlength="20" type="password" value=""></td>' \
'<input id="action" type="hidden" name="action" value="0">' \
'<td><input id="pass_show" type="checkbox" onchange="do_pass_show(this.checked);">show</td>' \
'</tr></table>' \
'</DIV>' \
'</form>' \
'<DIV id=c_leer></DIV>' \
'<DIV id=c_foot></DIV>' \
'</DIV>' \
'<DIV id=c_verzoeg1></DIV>' \
'<DIV id=c_verzoeg2></DIV>' \
'</DIV>' \
'<DIV id=t_Frame>' \
"<DIV id=t_but1 onclick='window.location.href=\"$HOME\";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>" \
'<DIV id=t_but4 onclick="subm();" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &lt;&lt;</DIV>' \
'</DIV>' \
'<DIV id=i_Frame>' \
'<DIV id=i_content>' \
'<h2>Save Settings</h2>' \
'<p>Save a part of or the whole router configuration.</p>' \
'</DIV>' \
'<DIV id=i_content2>' \
'<h2>Save Settings</h2>' \
'<p><b>Password</b><br>' \
'Set optionally a password to encrypt the configuration file and protect the data.</p>' \
'<p><img src="../pic_i_hinweis.gif" border="0"><br /> The encryption is done with RC4-algorithm.<b>.</p>' \
'</DIV>' \
'</DIV></BODY></HTML>'



