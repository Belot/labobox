#!/bin/sh

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onselectstart="return false">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>System Settings / Save and Load</DIV>
<div id="c_titel">Save &amp; Load</div>
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr> <td width="200"><a href="save.cgi" onmouseover="i_showElem(3)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>Save Settings</a></td>
<td></td></tr></table>
</div>
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr><td width="200"><a href="load.cgi" onmouseover="i_showElem(4)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>Load Settings</a></td>
</tr></table>
</div>
<div id="c_linklast">
<table border="0" cellpadding="4" cellspacing="0">
    <tr><td width="200"><a href="reset.cgi" onmouseover="i_showElem(5)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>Reset BS-Settings</a></td>
</tr></table>
</div>
<DIV id="c_foot"></DIV>
</DIV></DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Save and Load</h2>
<p>Save, restore or reset router settings.</p>
</DIV>
<DIV id=i_content3>
<h2>Save &amp; Load</h2>
<p><b>Save settings</b><br>
Save router settings to a file.</p>
</DIV>
<DIV id=i_content4>
<h2>Save &amp; Load</h2>
<p><b>Load settings</b><br>
Load router settings from file.</p>
</DIV>
<DIV id=i_content5>
<h2>Save &amp; Load</h2>
<p><b>Reset BS-Settings</b><br>
Reset the BitSwitcher-part of the NVRAM.</p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML

a=0