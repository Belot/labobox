#!/bin/sh

from=stproxy.cgi
action=0

. ./login.cgi

f_pfad="Services / HTTP-Proxy"
f_name="HTTP-Proxy"
f_text="The feature  &quot;stproxy;&quot; is not included!"
f_bin="/bin/stproxy"
. ./feature.cgi
. ./functions.sh

[ "$R1" = "V1" ] && stproxy_start_submit="0"
[ "$R1" = "V2" ] && stproxy_start_submit="1"

stproxy_start=`/bin/nvram get stproxy_start`
stproxy_port=`cat /var/stproxy.conf|grep PROXY_PORT|cut -b 12-17`
stproxy_admin_port=`cat /var/stproxy.conf|grep ADMIN_PORT|cut -b 12-17`
stproxy_dns=`cat /var/stproxy.conf|grep DNS_SERVER|cut -b 12-31`
#### Formular-Auswertung #######################################################
#SAVE
ret=0
mustsave=0
if [ "$action" -eq 2 ] || [ "$action" -eq 3 ]; then 
  if [ "$stproxy_start" != "$stproxy_start_submit" ]; then
    res=`/bin/nvram set stproxy_start=$stproxy_start_submit`
    if [ "$res" != "OK" ]; then
      ret=1
    fi
    mustsave=1
  fi
  save_file="0"
  if [ "$stproxy_port" != "$T1" ]; then
    if [ "$T1" -ge "1" ] || [ "$T1" -le "65534" ]; then
      is_fwservice_port "tcp" "$T1" && remove_fwservice_port "tcp" "$T1" && fw_restart=1 && mustsave=1
      cat /var/stproxy.conf|sed -e "s/PROXY_PORT $stproxy_port/PROXY_PORT $T1/g" >/var/stproxy.conf.tmp
      if [ $? -eq 0 ]; then
        save_file=1
        cp -f /var/stproxy.conf.tmp /var/stproxy.conf >/dev/null
        rm -f /var/stproxy.conf.tmp >/dev/null
      fi
    fi
  fi
  if [ "$stproxy_admin_port" != "$T2" ]; then
    if [ "$T2" -ge "1" ] || [ "$T2" -le "65534" ]; then
      is_fwservice_port "tcp" "$T2" && remove_fwservice_port "tcp" "$T2" && fw_restart=1 && mustsave=1
      cat /var/stproxy.conf|sed -e "s/ADMIN_PORT $stproxy_admin_port/ADMIN_PORT $T2/g" >/var/stproxy.conf.tmp
      if [ $? -eq 0 ]; then
        save_file=1
        cp -f /var/stproxy.conf.tmp /var/stproxy.conf >/dev/null
        rm -f /var/stproxy.conf.tmp >/dev/null
      fi
    fi
  fi
  if [ "$stproxy_dns" != "$T3" ]; then
    cat /var/stproxy.conf|sed -e "s/DNS_SERVER $stproxy_dns/DNS_SERVER $T3/g" >/var/stproxy.conf.tmp
    if [ $? -eq 0 ]; then
      save_file=1
      cp -f /var/stproxy.conf.tmp /var/stproxy.conf >/dev/null
      rm -f /var/stproxy.conf.tmp >/dev/null
    fi
  fi
  if [ "$save_file" = "1" ]; then
    res=`/bin/nvram setfile stproxy_config=/var/stproxy.conf`
    mustsave=1
    if [ "$res" != "OK" ]; then
      ret=1
    fi
  fi
  
  if [ "$form_fw_open" = "1" ]; then
    is_fwservice_port "tcp" "$T1" || add_fwservice_port "tcp" "$T1" && mustsave=1 && fw_restart=1
  else
    is_fwservice_port "tcp" "$T1" && remove_fwservice_port "tcp" "$T1" && mustsave=1 && fw_restart=1
  fi
  
  if [ "$form_admin_fw_open" = "1" ]; then
    is_fwservice_port "tcp" "$T2" || add_fwservice_port "tcp" "$T2" && mustsave=1 && fw_restart=1
  else
    is_fwservice_port "tcp" "$T2" && remove_fwservice_port "tcp" "$T2" && mustsave=1 && fw_restart=1
  fi
fi
#RUN
if [ "$action" -eq 3 ]; then 
  ret=0
  if [ "$stproxy_start" !=  "$stproxy_start_submit" ] || [ "$save_file" = "1" ]; then
    res=`/etc/start_scripts/stproxy.sh restart`
  fi
  [ "$fw_restart" = "1" ] && /etc/start_scripts/firewall.sh services_restart >/dev/null
fi
################################################################################

[ "$mustsave" -eq 1 ] && nvram commit >/dev/null

stproxy_start=`/bin/nvram get stproxy_start`
[ "$stproxy_start" = "Failed" ] && stproxy_start="0"
[ "$stproxy_start" = "1" ] && stproxy_on='checked="checked"' || stproxy_off='checked="checked"'

if [ ! -r /var/stproxy.conf ]; then
  /etc/start_scripts/stproxy.sh write_config >/dev/null
fi
stproxy_port=`cat /var/stproxy.conf|grep PROXY_PORT|cut -b 12-17`
[ ! -n "$stproxy_port" ] && stproxy_port="8080"

stproxy_admin_port=`cat /var/stproxy.conf|grep ADMIN_PORT|cut -b 12-17`
[ ! -n "$stproxy_port" ] && stproxy_admin_port="6767"

stproxy_dns=`cat /var/stproxy.conf|grep DNS_SERVER|cut -b 12-31`
[ ! -n "$stproxy_port" ] && stproxy_dns="127.0.0.1"

is_fwservice_port "tcp" "$stproxy_port" && fw_open='checked="checked"' || fw_closed='checked="checked"'
is_fwservice_port "tcp" "$stproxy_admin_port" && admin_fw_open='checked="checked"' || admin_fw_closed='checked="checked"'


cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>

<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function stproxy_change() {
  if(document.getElementById("form_stproxy_off").checked==true){
    document.getElementById("form_fw_open_off").disabled=true;
    document.getElementById("form_fw_open_on").disabled=true;
    document.getElementById("form_admin_fw_open_off").disabled=true;
    document.getElementById("form_admin_fw_open_on").disabled=true;
    document.getElementById("T1").disabled=true;
    document.getElementById("T2").disabled=true;
    document.getElementById("T3").disabled=true;
  }
  else{
    document.getElementById("form_fw_open_off").disabled=false;
    document.getElementById("form_fw_open_on").disabled=false;
    document.getElementById("form_admin_fw_open_off").disabled=false;
    document.getElementById("form_admin_fw_open_on").disabled=false;
    document.getElementById("T1").disabled=false;
    document.getElementById("T2").disabled=false;
    document.getElementById("T3").disabled=false;
  }
}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="stproxy_change();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>$f_pfad</DIV>
<form id="xform" name="xform" method="post">
<DIV id=c_std>
<input id="action" type="hidden" name="action" value="0">
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0)>
<td width="125">HTTP-Proxy</td>
<td width="20"><input type="radio" id="form_stproxy_off" name="R1" value="V1" onchange="stproxy_change();" $stproxy_off></td>
<td width="40">Off</td>
<td width="20"><input type="radio" id="form_stproxy_on" name="R1" value="V2" onchange="stproxy_change();" $stproxy_on></td>
<td>On</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0)>
<td width="125">Firewall proxy-port</td>
<td width="20"><input type="radio" id="form_fw_open_off" name="form_fw_open" value="0" $fw_closed></td>
<td width="40">close</td>
<td width="20"><input type="radio" id="form_fw_open_on" name="form_fw_open" value="1" $fw_open></td>
<td>open</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0)>
<td width="125">Firewall admin-port</td>
<td width="20"><input type="radio" id="form_admin_fw_open_off" name="form_admin_fw_open" value="0" $admin_fw_closed></td>
<td width="40">close</td>
<td width="20"><input type="radio" id="form_admin_fw_open_on" name="form_admin_fw_open" value="1" $admin_fw_open></td>
<td>open</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0)>
<td width="125">Proxy-port</td>
<td width="100"><input type="text" id="T1" name="T1" size="5" maxlength="5" value="$stproxy_port"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(5) onmouseout=i_showElem(0)>
<td width="125">Admin-port</td>
<td width="100"><input type="text" id="T2" name="T2" size="5" maxlength="5" value="$stproxy_admin_port"></td>
</tr></table>
</DIV>
<DIV id=c_last>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(6) onmouseout=i_showElem(0)>
<td width="125">DNS-server</td>
<td width="100"><input type="text" id="T3" name="T3" size="15" maxlength="15" value="$stproxy_dns"></td>
</tr></table></form>
</DIV>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but3 onclick="subm(2);" onmouseover="rahmen(1,3);i_showElem(7)" onmouseout="rahmen(0,3);i_showElem(0)">Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover="rahmen(1,4);i_showElem(8)" onmouseout="rahmen(0,4);i_showElem(0)">Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>HTTP-Proxy</h2>
<p>Configure settings for stproxy</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Standard stproxy-user and -password is <b>stproxy/stproxy</b>.</p>
</DIV>
<DIV id=i_content1>
<h2>stproxy</h2>
<p><b>Start/Stop</b><br>Start stproxy Yes/No</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Setting is checked on next Reboot</p>
</DIV>
<DIV id=i_content2>
<h2>stproxy</h2>
<p><b>Firewall Proxy-Port</b><br>Open Proxy-Port for public access</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Setting controls only access for public IP-Adresses</p>
</DIV>
<DIV id=i_content3>
<h2>stproxy</h2>
<p><b>Firewall Admin-Port</b><br>Open Admin-Port for public access</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Setting controls only access for public IP-Adresses</p>
</DIV>
<DIV id=i_content4>
<h2>stproxy</h2>
<p><b>Proxy-Port</b><br>Set on which TCP-Port stproxy should listen for Web-Clients</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Enter Valid Port-Number from 1-65534, default=8080</p>
</DIV>
<DIV id=i_content5>
<h2>stproxy</h2>
<p><b>Admin-Port</b><br>Set on which TCP-Port stproxy-Web-Interface should be reachable</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Enter Valid Port-Number from 1-65534, default=6767</p>
</DIV>
<DIV id=i_content6>
<h2>stproxy</h2>
<p><b>DNS-Server</b><br>Set which DNS-Server stproxy should use for resolving Urls</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Enter Valid Ip-Adress from 0.0.0.0 to 255.255.255.255, default=127.0.0.1</p>
</DIV>
<DIV id=i_content7>
<h2>stproxy</h2>
<p><b>Save</b><br>Saves changed settings to NVRAM</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Changes only occur when restarting stproxy</p>
</DIV>
<DIV id=i_content8>
<h2>stproxy</h2>
<p><b>Save&amp;Run</b><br>Save changed settings to NVRAM and restart stproxy with new settings</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Changes are safed permanently</p>
</DIV>
</DIV></BODY></HTML>
