#!/bin/sh

from=dns.cgi
action=0

. ./login.cgi
. ./functions.sh

PATH=".:"$PATH
HOME="index.cgi"

DNSHOSTS=`nvram get dnshosts`
[ "$DNSHOSTS" = "Failed" ] && DNSHOSTS=""

DNSHOSTS6=`nvram get ipv6_dnshosts`
[ "$DNSHOSTS6" = "Failed" ] && DNSHOSTS6=""

err=0
mustsave=0
########################### Add #####################################
if [ "$action" -eq 10 ]; then
  
  if [ "$hostip" != "" ]; then
    hostip=`isip.sh $hostip`
    [ "$hostip" = "ERROR" ] && hostip="" && errmsg="$errmsg Invalid IP address!"'\n' && err=1
  fi

  hostname=`echo "$hostname"|sed -n -e 's/\([^A-Za-z0-9\-]\)*//gp'`

  [ "$hostip" = "" ] || [ "$hostname" = "" ] && errmsg="$errmsg You must specify a IP adresses and a Hostname!"'\n' && err=1

 if [ "$err" -eq 0 ]; then
 DNSHOSTS=$DNSHOSTS"$hostip,$hostname~"
 res=`nvram set dnshosts=$DNSHOSTS`
 mustsave=1 
 [ "$res" != "OK" ] && errmsg="$errmsg Write to NVRam failed!"'\n'

# duplikat check
 dup=`echo $DNSHOSTS|sed -e 's/~/\n/g'|cut -d ',' -f 1|sort|awk 'BEGIN{h=""}{if(h==""){h=$1; next;} if(h==$1){print $1;} h=$1;}'|tr  '\n' ' '`
 [ "$dup" != "" ] && errmsg="$errmsg Warning duplicate IP-Adress: $dup"'\n'
 dup=`echo $DNSHOSTS|sed -e 's/~/\n/g'|cut -d ',' -f 2|sort|awk 'BEGIN{h=""}{if(h==""){h=$1; next;} if(h==$1){print $1;} h=$1;}'|tr  '\n' ' '`
 [ "$dup" != "" ] && errmsg="$errmsg Warning duplicate Hostname: $dup"'\n'

 hostip=""
 hostname=""
 fi
fi

if [ "$action" -eq 20 ]; then
  http_unquote hostipv6
  if [ "$hostipv6" != "" ]; then
    is_ipv6_addr $hostipv6
    ret=$?
    [ "$ret" != "0" ] && ipv6_errmsg=`get_ipv6_err_string $ret` && errmsg="$errmsg Invalid IPv6 address (${hostipv6} -- ${ipv6_errmsg})!"'\n' && err=1
  fi
  
  http_unquote hostname6
  
  [ "$hostipv6" = "" ] || [ "$hostname6" = "" ] && errmsg="$errmsg You must specify a IPv6 adresses and a Hostname!"'\n' && err=1
  
  if [ "$err" -eq 0 ]; then
    DNSHOSTS6=$DNSHOSTS6"$hostipv6,$hostname6~"
    res=`nvram set ipv6_dnshosts=$DNSHOSTS6`
    mustsave=1 
    [ "$res" != "OK" ] && errmsg="$errmsg Write to NVRam failed!"'\n'
  
    # duplikat check
    dup=`echo $DNSHOSTS6|sed -e 's/~/\n/g'|cut -d ',' -f 1|sort|awk 'BEGIN{h=""}{if(h==""){h=$1; next;} if(h==$1){print $1;} h=$1;}'|tr  '\n' ' '`
    [ "$dup" != "" ] && errmsg="$errmsg Warning duplicate IPv6-Adress: $dup"'\n'
    dup=`echo $DNSHOSTS6|sed -e 's/~/\n/g'|cut -d ',' -f 2|sort|awk 'BEGIN{h=""}{if(h==""){h=$1; next;} if(h==$1){print $1;} h=$1;}'|tr  '\n' ' '`
    [ "$dup" != "" ] && errmsg="$errmsg Warning duplicate Hostname: $dup"'\n'
  fi
fi

################################## Del #####################################
if [ "$action" -eq 11 ] && [ "$select" != "" ]; then
 X=`echo "$select"|sed -e 's/%2C/,/g'|sed -e 's/%2c/,/g'`

 DNSHOSTS=`echo $DNSHOSTS|sed -e "s/$X~//"`
 res=`nvram set dnshosts=$DNSHOSTS`
 [ "$res" != "OK" ] && errmsg="$errmsg Write to NVRam failed!"'\n' || mustsave=1
fi

if [ "$action" -eq 21 ] && [ "$select6" != "" ]; then
  http_unquote select6
  X=`echo "$select6"|sed -e 's/%2C/,/g'|sed -e 's/%2c/,/g'`

  DNSHOSTS6=`echo $DNSHOSTS6|sed -e "s/$X~//"`
  res=`nvram set ipv6_dnshosts=$DNSHOSTS6`
  [ "$res" != "OK" ] && errmsg="$errmsg Write to NVRam failed!"'\n' || mustsave=1
fi

############################## Save & Run ##################################
if [ "$action" -eq 3 ]; then
  [ "$R1" = "V1" ] && dns_start="0"
  [ "$R1" = "V2" ] && dns_start="1"

 domain=`echo "$domain"|sed -n -e 's/\([^A-Za-z0-9\-\.]\)*//gp'`	
 [ "$domain" = "" ] && errmsg="$errmsg You must specify a name for the local domain!"'\n' && err=1

 if [ "$err" -eq 0 ]; then
  err=0
  [ "$dns_start" != "`nvram get dns_start`" ] && mustsave=1 && res=`nvram set dns_start=$dns_start` && [ "$res" != "OK" ] && err=1
  
  [ "$domain" != "`nvram get dns_domain`" ] && mustsave=1 && res=`nvram set dns_domain=$domain` && [ "$res" != "OK" ] && err=1
  
  [ "$filterwin2k" != "`nvram get dns_filterwin2k`" ] && mustsave=1 && res=`nvram set dns_filterwin2k=$filterwin2k` && [ "$res" != "OK" ] && err=1

  [ "$err" -ne 0 ] && errmsg="$errmsg Write to NVRam failed!"'\n'

  /etc/start_scripts/dhcp_dns.sh restart >/dev/null 2>/dev/null
  fi

fi

[ "$mustsave" -eq 1 ] && nvram commit >/dev/null

dns_start=`nvram get dns_start`
dns_on=''
dns_off='checked="checked"'
if [ "$dns_start" = "Failed" ]; then
  dns_start="1"
  dns_on='checked="checked"'
  dns_off=''
fi
[ "$dns_start" -eq 1 ] && dns_on='checked="checked"' && dns_off=''
[ "$dns_start" -ne 1 ] && dns_on='' && dns_off='checked="checked"'

domain=`nvram get dns_domain`
[ "$domain" = "Failed" ] && domain="lan"

filterwin2k=`nvram get dns_filterwin2k`
enable_filterwin2k_on='checked="checked"'
[ "$filterwin2k" = "0" ] && enable_filterwin2k_off='checked="checked"' && enable_filterwin2k_on=''


cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function subm(x) {
var domain=document.getElementById("domain").value;
if(domain!="" && !isNumorCharwiDashDot(domain)){alert("Invalid local domain name!");return;}
if(domain==""){alert("You must specify a name for the local domain!");return;}
load();
document.getElementById("action").value=x;
document.getElementById("xform").submit();
}
function doAdd() {
var hostip=document.getElementById("hostip").value;
var hostname=document.getElementById("hostname").value;
if(hostip!="" && !isValidIpAddress(hostip)){alert("IP address is invalid!");return;}
if(hostname!="" && !isNumorCharwiDash(hostname)){alert("Invalid Hostname!");return;}
if(hostip=="" || hostname==""){alert("You must specify a IP adresses and a Hostname!");return;}
load();
document.getElementById("action").value=10;
document.getElementById("xform").submit();
}
function doDel() {
load();
document.getElementById("action").value=11;
document.getElementById("xform").submit();
}
function showErr() {
var err="$errmsg";
if(err!=""){alert(err);} }
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Network settings / DNS</DIV>
<DIV id=c_titel>DNS-Server</DIV>
<form id="xform" name="xform" method="post">
<DIV id=c_last>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0) >
<input id="action" type="hidden" name="action" value="0">
<td width="20"><input type="radio" name="R1" value="V1" $dns_off></td>
<td width="40">Off</td>
<td width="20"><input type="radio" name="R1" value="V2" $dns_on></td>
<td>On</td>
</tr></table>
</DIV>
<DIV id=c_leer></DIV>
<DIV id=c_titel>DNS Options</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(6) onmouseout=i_showElem(0) >
<td width="100">local Domain:</td>
<td><input id="domain" name="domain" size="22" maxlength="63" type="text" value="$domain"></td>
</tr></table></DIV>
<DIV id=c_last>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(8) onmouseout=i_showElem(0)><td width="100">filterwin2k:</td>
<td width="20"><input type="radio" name="filterwin2k" value="0" $enable_filterwin2k_off></td>
<td width="40">Off</td>
<td width="20"><input type="radio" name="filterwin2k" value="1" $enable_filterwin2k_on></td>
<td>On</td></tr>
</table></DIV>
<DIV id=c_leer></DIV>
<DIV id=c_titel>Known Hosts</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0) >
<td width="40">IP:</td>
<td><input id="hostip" name="hostip" size="22" maxlength="15" type="text" value="$hostip"></td>
</tr></table></DIV>
<DIV id=c_last>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0) >
<td width="40">Name:</td>
<td><input id="hostname" name="hostname" size="22" maxlength="20" type="text" value="$hostname"></td>
<td width="120"></td><td><input type="button" name="add" value="Add" onclick="doAdd();"></td>
</tr></table></DIV>
<DIV id=c_leer></DIV>
<DIV id=c_last>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0) >
<td width="40">Host:</td>
<td><select name="select" size="1" style="width:340px">
EOF_HTML
echo $DNSHOSTS|sed -e 's/~/\n/g'|sed -n -e '/^$/!p'|sed -e 's/^\(.*\)$/<option value="\1">\1\2<\/option>/'

cat << EOF_HTML
</select></td></tr></table></DIV>
<DIV id=c_last><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(4) onmouseout=i_showElem(0) ><td width=335></td>
<td><input type="button" name="del" value="Del" onclick="doDel();"></td>
</tr></table></DIV>
EOF_HTML

if [ -f "/lib/modules/2.6.8.1/kernel/net/ipv6/ipv6.ko" ]; then
cat << EOF_HTML
<DIV id=c_leer></DIV>
<DIV id=c_titel>Known IPv6 Hosts</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(10) onmouseout=i_showElem(0) >
<td width="40">IPv6:</td>
<td><input id="hostipv6" name="hostipv6" size="40" maxlength="35" type="text"></td>
</tr></table></DIV>
<DIV id=c_last>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(11) onmouseout=i_showElem(0) >
<td width="40">Name:</td>
<td><input id="hostname6" name="hostname6" size="22" maxlength="20" type="text"></td>
<td width="120"></td><td><input type="button" name="add6" value="Add" onclick="subm(20);"></td>
</tr></table></DIV>
<DIV id=c_leer></DIV>
<DIV id=c_last>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(12) onmouseout=i_showElem(0) >
<td width="40">Host:</td>
<td><select name="select6" size="1" style="width:340px">
EOF_HTML

echo $DNSHOSTS6|sed -e 's/~/\n/g'|sed -n -e '/^$/!p'|sed -e 's/^\(.*\)$/<option value="\1">\1\2<\/option>/'

cat << EOF_HTML
</select></td></tr></table></DIV>
<DIV id=c_last><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(13) onmouseout=i_showElem(0) ><td width=335></td>
<td><input type="button" name="del6" value="Del" onclick="subm(21);"></td>
</tr></table></DIV>
EOF_HTML
fi # IPv6

cat << EOF_HTML
</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; un</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>DNS</h2>
<p>The Domain Name system (DNS) translate hostnames to IP addresses. You can use this DNS server to address hosts in your local network with their hostnames.</p>
</DIV>
<DIV id=i_content1>
<h2>DNS Server</h2>
Turn the DNS server <b>on</b> or <b>off</b>.</p>
</DIV>
<DIV id=i_content2>
<h2>Known Hosts</h2>
<p>Associate a IP address with a hostname.</p><p><b>IP</b><br>
Specify the IP-Address of the host.</p>
</DIV>
<DIV id=i_content3>
<h2>Known Hosts</h2>
<p>Associate a IP address with a hostname.</p><p><b>Name</b><br>
Specify a name for the host.</p>
</DIV>
<DIV id=i_content4>
<h2>Known Hosts</h2>
<p><b>Host</b><br>
Delete a Host from the DNS database.</p>
</DIV>
<DIV id=i_content6>
<h2>DNS Options</h2>
<p><b>local Domain</b><br>
Specify the name of the local Domain.</p>
</DIV>
<DIV id=i_content8>
<h2>DNS Options</h2>
<p><b>filterwin2k</b><br>
Later versions of windows make periodic DNS requests which do not get sensible answers from the public DNS and can cause problems by triggering dial-on-demand links. This flag turns on an option to filter such requests. The requests blocked are for records of types SOA and SRV, and type ANY where the requested name has underscores, to catch LDAP requests.
</p>
<p>Default: On</p>
</DIV>
<DIV id=i_content10>
<h2>Known IPv6 Hosts</h2>
<p>Associate a IPv6 address with a hostname.</p><p><b>IPv6</b><br>
Specify the IPv6-Address of the host.</p>
<p>Example: 2001:abcd:1234::1</p>
</DIV>
<DIV id=i_content11>
<h2>Known IPv6 Hosts</h2>
<p>Associate a IPv6 address with a hostname.</p><p><b>Name</b><br>
Specify a name for the host.</p>
</DIV>
<DIV id=i_content12>
<h2>Known IPv6 Hosts</h2>
<p><b>Host</b><br>
Delete a IPv6 Host from the DNS database.</p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML

a=0
