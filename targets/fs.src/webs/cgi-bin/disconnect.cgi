#!/bin/sh

from=disconnect.cgi
. ./login.cgi

HOME="additional_index.cgi"

[ "$R1" = "V1" ] && disconnect_start_submit="0"
[ "$R1" = "V2" ] && disconnect_start_submit="1"
[ "$R2" = "V1" ] && disconnect_reboot_submit="0"
[ "$R2" = "V2" ] && disconnect_reboot_submit="1"

disconnect_start=`/bin/nvram get disconnect_start`
disconnect_hour=`/bin/nvram get disconnect_hour`
disconnect_minute=`/bin/nvram get disconnect_minute`
disconnect_delay=`/bin/nvram get disconnect_delay`
disconnect_reboot=`/bin/nvram get disconnect_reboot`

#### Formular-Auswertung #######################################################
#SAVE
run="0" 
 if [ "$action" = "2" ] || [ "$action" = "3" ]; then 
  if [ "$disconnect_start" !=  "$disconnect_start_submit" ]; then
    res=`/bin/nvram set disconnect_start=$disconnect_start_submit`
    disconnect_start=$disconnect_start_submit
    run="1"
  fi
  if [ "$disconnect_reboot" !=  "$disconnect_reboot_submit" ]; then
    res=`/bin/nvram set disconnect_reboot=$disconnect_reboot_submit`
    disconnect_reboot=$disconnect_reboot_submit
    run="1"
  fi
  err=0
  errmsg=""
  h=""
  h=`echo $T1|sed -e 's/[^0-9]*//g'|awk '{match($0,/^0*[1-9]/);print substr($0,RSTART+RLENGTH-1);}'`
  if [ "$disconnect_hour" !=  "$h" ]; then
    [ -z "$h" ] || [ $h -lt 0 ] || [ $h -gt 23 ] && errmsg="invalid value for hour [0-23]"'\n' && err=1
    [ $err -eq 0 ] && res=`/bin/nvram set disconnect_hour=$h` && disconnect_hour=$h  && run="1"
  fi
  h=""
  h=`echo $T2|sed -e 's/[^0-9]*//g'|awk '{match($0,/^0*[1-9]/);print substr($0,RSTART+RLENGTH-1);}'`
  if [ "$disconnect_minute" !=  "$h" ]; then
    [ -z "$h" ] || [ $h -lt 0 ] || [ $h -gt 59 ] && errmsg="$errmsg invalid value for minute [0-59]"'\n' && err=1
    [ $err -eq 0 ] && res=`/bin/nvram set disconnect_minute=$h` && disconnect_minute=$h && run="1"
  fi
  h=""
  h=`echo $T3|sed -e 's/[^0-9]*//g'|awk '{match($0,/^0*[1-9]/);print substr($0,RSTART+RLENGTH-1);}'`
  if [ "$disconnect_delay" !=  "$h" ]; then
    [ -z "$h" ] || [ $h -lt 1 ] || [ $h -gt 3600 ] && errmsg="$errmsg invalid value for delay [1-3600]"'\n' && err=1
    [ $err -eq 0 ] && res=`/bin/nvram set disconnect_delay=$h` && disconnect_delay=$h  && run="1"
  fi
 fi
#RUN
 [ "$run" = "1" ] && /bin/nvram commit >/dev/null
 [ "$action" = "3" ] && [ "$run" = "1" ] && /etc/start_scripts/disconnect.sh restart >/dev/null 2>/dev/null
################################################################################

disconnect_on=''
disconnect_off='checked="checked"'
if [ "$disconnect_start" = "1" ]; then
  disconnect_start="1"
  disconnect_on='checked="checked"'
  disconnect_off=''
else
  disconnect_start="0"
fi

dis_reboot_on=''
dis_reboot_off='checked="checked"'
if [ "$disconnect_reboot" = "1" ]; then
  dis_reboot_on='checked="checked"'
  dis_reboot_off=''
fi

[ -z "$disconnect_hour" ] || [ $disconnect_hour = "Failed" ] || [ $disconnect_hour -lt 0 ] || [ $disconnect_hour -gt 24 ] && disconnect_hour="4" >/dev/null 2>/dev/null
[ -z "$disconnect_minute" ] || [ $disconnect_minute = "Failed" ] || [ $disconnect_minute -lt 0 ] || [ $disconnect_minute -gt 59 ] && disconnect_minute="0" >/dev/null 2>/dev/null
[ -z "$disconnect_delay" ] || [ "$disconnect_delay" = "Failed" ] || [ $disconnect_delay -lt 0 ] || [ $disconnect_delay -gt 59 ] && disconnect_delay="1" >/dev/null 2>/dev/null

echo 'Content-type: text/html'
echo 'Connection: close'
echo ''
echo '<HTML><HEAD>'

echo '<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>' \
    '<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>' \
    '<SCRIPT type="text/JavaScript">' \
    'function subm(x) {' \
    'var hour=document.getElementById("hour");' \
    'var minute=document.getElementById("minute");' \
    'var delay=document.getElementById("delay");' \
    'if(!isNaN(hour)){alert("hour value is invalid!");return;}'\
    'if(!isNaN(minute)){alert("minute value is invalid!");return;}'\
    'if(!isNaN(delay)){alert("delay value is invalid!");return;}'\
    'load();' \
    'document.getElementById("action").value = x;' \
    'document.getElementById("xform").submit();' \
    '}' \
    'function showErr() {' \
      "var err=\"$errmsg\";" \
      'if(err!=""){alert(err);} }' \
    '</SCRIPT>' \
    '<LINK rel="stylesheet" href="../style.css" type="text/css">' \
    '<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->' \
    '</HEAD>' \
    '<BODY onload="showErr();"' \
    '<DIV id=c_Frame>' \
    '<DIV id=c_border>' \
    '<DIV id=c_pfad>Additional functions / Auto disconnect/reboot</DIV>' \
    '<DIV id=c_titel>Auto disconnect/reboot</DIV>' \
    '<DIV id=c_std>' \
    '<form id="xform" name="xform" method="post">' \
    '<table border="0" cellpadding="0" cellspacing="2">' \
    '<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0)>' \
    '<td width=\"90\">Setting</td>' \
    "<td width=\"20\"><input type=\"radio\" name=\"R1\" value=\"V1\" $disconnect_off></td>" \
    '<td width="70">Off</td>' \
    "<td width=\"20\"><input type=\"radio\" name=\"R1\" value=\"V2\" $disconnect_on></td>" \
    '<td>On</td>' \
    '</tr></table>' \
    '</DIV>' \
    '<DIV id=c_std>' \
    '<table border="0" cellpadding="0" cellspacing="2">' \
    '<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0)>' \
    '<td width=\"90\">Action</td>' \
    "<td width=\"20\"><input type=\"radio\" name=\"R2\" value=\"V1\" $dis_reboot_off></td>" \
    '<td width="70">Disconnect</td>' \
    "<td width=\"20\"><input type=\"radio\" name=\"R2\" value=\"V2\" $dis_reboot_on></td>" \
    '<td>Reboot</td>' \
    '</tr></table>' \
    '</DIV>' \
    '<DIV id=c_std>' \
    '<table border="0" cellpadding="0" cellspacing="2">' \
    '<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0)>' \
    '<td width=\"90\">Time</td>' \
    "<td width=\"20\"><input type=\"text\" id =\"hour\" name=\"T1\" size=\"2\" maxlength=\"2\" value=\"$disconnect_hour\"></td>" \
    '<td width="40">HH</td>' \
    "<td width=\"20\"><input type=\"text\" id=\"minute\" name=\"T2\" size=\"2\" maxlength=\"2\" value=\"$disconnect_minute\"></td>" \
    '<td>MM</td>' \
    '</tr></table>' \
    '</DIV>' \
    '<DIV id=c_last>' \
    '<table border="0" cellpadding="0" cellspacing="2">' \
    '<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0)>' \
    '<td width=\"90\">Delay</td>' \
    "<td width=\"20\"><input type=\"text\" id=\"delay\" name=\"T3\" size=\"4\" maxlength=\"4\" value=\"$disconnect_delay\"></td>" \
    '<td width="40">seconds</td>' \
    '<input id="action" type="hidden" name="action" value="0">' \
    "<input type=\"hidden\" name=\"session_id\" value=\"$session_id\">" \
    '</tr></table></form>' \
    '</DIV>' \
    '<DIV id=c_leer></DIV>' \
    '<DIV id=c_foot></DIV>' \
    '</DIV>' \
    '<DIV id=c_verzoeg1></DIV>' \
    '<DIV id=c_verzoeg2></DIV>' \
    '</DIV>' \
    '<DIV id=t_Frame>' \
     "<DIV id=t_but1 onclick='window.location.href=\"$HOME\";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>" \
    '<DIV id=t_but3 onclick="subm(2);" onmouseover="rahmen(1,3);i_showElem(6)" onmouseout="rahmen(0,3);i_showElem(0)">Save</DIV>' \
    '<DIV id=t_but4 onclick="subm(3);" onmouseover="rahmen(1,4);i_showElem(7)" onmouseout="rahmen(0,4);i_showElem(0)">Save &amp; Run</DIV>' \
   '</DIV>' \
    '<DIV id=i_Frame>' \
    '<DIV id=i_content>' \
    '<h2>Auto disconnect</h2>' \
    '<p>Control scheduled disconnect of WAN-connection or reboot</p>' \
    '</DIV>' \
    '<DIV id=i_content1>' \
    '<h2>Auto disconnect/reboot</h2>' \
    '<p><b>On/Off</b><br>Do auto disconnect or reboot yes/no</p>' \
    '<p><img src="../pic_i_hinweis.gif" border="0"><br /> Setting is checked on next Reboot</p>' \
    '</DIV>' \
    '<DIV id=i_content2>' \
    '<h2>Auto disconnect/reboot</h2>' \
    '<p><b>Time setting</b><br>Set time for auto-disconnect</p>' \
    '<p><img src="../pic_i_hinweis.gif" border="0"><br /> values have to be in valid hour/minute range</p>' \
    '</DIV>' \
    '<DIV id=i_content3>' \
    '<h2>Auto disconnect/reboot</h2>' \
    '<p><b>Delay setting</b><br>Set delay between disconnect and reconnect</p>' \
    '<p><img src="../pic_i_hinweis.gif" border="0"><br /> value has to be between [1-3600]</p>' \
    '</DIV>' \
    '<DIV id=i_content4>' \
    '<h2>Auto disconnect/reboot</h2>' \
    '<p><b>Action setting</b><br>Choose between Disconnect or Reboot</p>' \
    '</DIV>' \
    '<DIV id=i_content6>' \
    '<h2>Auto disconnect/reboot</h2>' \
    '<p><b>Save</b><br>Saves changed settings to NVRAM</p>' \
    '<p><img src="../pic_i_hinweis.gif" border="0"><br /> Changes only occur when restarting disconnect</p>' \
    '</DIV>' \
    '<DIV id=i_content7>' \
    '<h2>Auto disconnect/reboot</h2>' \
    '<p><b>Save&amp;Run</b><br>Save changed settings to NVRAM and set new values for disconnect with new settings</p>' \
    '<p><img src="../pic_i_hinweis.gif" border="0"><br /> Changes are safed permanently</p>' \
    '</DIV>' \
    '</DIV></BODY></HTML>'

