#!/bin/sh

action=0
mustsave=0

. ./functions.sh
. ./login.cgi

HOME="ipv6_index.cgi"

ipv6_addr=`nvram get ipv6_add_addr`
[ "$ipv6_addr" = "Failed" ] && ipv6_addr=""

err=0
########################### Add #####################################
if [ "$action" -eq 10 ]; then
  
  http_unquote address
  [ "$address" = "" ] && errmsg="$errmsg IPv6 address not specified!"'\n' && err=1
  
  if [ "$err" = "0" ]; then
	is_ipv6_addr_with_prefix $address
	ret=$?
	[ "$ret" != "0" ] && ipv6_errmsg=`get_ipv6_err_string $ret` && errmsg="$errmsg Invalid IPv6 address (${address} -- ${ipv6_errmsg})!"'\n' && err=1
  fi

  [ "$device" = "" ] && errmsg="$errmsg Unknown error!"'\n' && err=1

 if [ "$err" = "0" ]; then
	ipv6_addr=$ipv6_addr"$address,$device~"
	res=`nvram set ipv6_add_addr=$ipv6_addr`
	mustsave=1 
	[ "$res" != "OK" ] && errmsg="$errmsg Write to NVRam failed!"'\n'
	
	# duplikat check
	dup=`echo $ipv6_addr|sed -e 's/~/\n/g'|cut -d ',' -f 1|sort|awk 'BEGIN{h=""}{if(h==""){h=$1; next;} if(h==$1){print $1;} h=$1;}'|tr  '\n' ' '`
	[ "$dup" != "" ] && errmsg="$errmsg Warning duplicate IPv6-Address: $dup"'\n'

 fi
fi


################################## Del #####################################

if [ "$action" -eq 11 ] && [ "$select" != "" ]; then
 http_unquote select
 X=`echo "$select"|sed -e 's/%2C/,/g'|sed -e 's/%2c/,/g'`

 ipv6_addr=`echo $ipv6_addr|sed -e "s-$X~--"`
 res=`nvram set ipv6_add_addr=$ipv6_addr`
 [ "$res" != "OK" ] && errmsg="$errmsg Write to NVRam failed!"'\n' || mustsave=1
fi

################################ Save & Run #################################
if [ "$action" -eq 3 ] || [ "$action" -eq 2 ]; then
  [ "$action" -eq 3 ] && /etc/start_scripts/ipv6.sh restart >/dev/null 2>/dev/null
  [ "$mustsave" = "1" ] && nvram commit >/dev/null
fi

ipv6_add_addr=`nvram get ipv6_add_addr`
[ "$ipv6_add_addr" = "Failed" ] && ipv6_add_addr=""

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function showErr() {
var err="$errmsg";
if(err!=""){alert(err);} }
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>IPv6 settings / additional IPv6 addresses</DIV>
<DIV id=c_titel>Add address</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<input type="hidden" name="mustsave" value="$mustsave">
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0) >
<td width="50">Address:</td>
<td>
<input type="input" name="address" size="40" maxlength="35" value=""></td>
</td></tr>
</table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0) >
<td width="50">Device:</td>
<td><select name="device" size="1">
<option value="0">Bridge/LAN</option>
<option value="1">WLAN</option>
</select></td></tr>
</table>
</DIV>
<DIV id=c_last><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(4) onmouseout=i_showElem(0) ><td width=335></td>
<td><input type="button" name="add" value="Add" onclick="subm(10);"></td>
</tr></table>
</DIV>
<div id="c_leer"></div>
<DIV id=c_titel>Delete address</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0) >
<td width="50">Address:</td>
<td><select name="select" size="1" style="width:340px">
EOF_HTML

if [ "$ipv6_add_addr" != "" ]; then
echo $ipv6_add_addr |sed -e 's/~/\n/g'|sed -n -e '/^$/!p'| \
awk '{ split($0,a,/[,]/)
       ipv6_addr=a[1]
       iface=a[2]
       split("Bridge/LAN,WLAN", ifnames, ",");
       print "<option value=" $0 ">" ipv6_addr " - " ifnames[iface+1] "</option>"
    }'
fi

cat << EOF_HTML
</select></td></tr></table></DIV>
<DIV id=c_last><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(5) onmouseout=i_showElem(0) ><td width=335></td>
<td><input type="button" name="del" value="Delete" onclick="subm(11);"></td>
</tr></table></DIV>
</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>additional IPv6 addresses</h2>
<p>Add or remove additional IPv6 addresses.</p>
</DIV>
<DIV id=i_content1>
<h2>Add address</h2>
<p><b>Address</b></p>
<p>Type the new IPv6 address.</p>
</DIV>
<DIV id=i_content2>
<h2>Add address</h2>
<p><b>Device</b></p>
<p>Choose a device to assignt the new IPv6 address.</p>
</DIV>
<DIV id=i_content3>
<h2>Delete address</h2>
<p><b>Address</b></p>
<p>Choose a IPv6 address to delete.</p>
</DIV>
<DIV id=i_content4>
<h2>Add address</h2>
<p><b>Add</b></p>
<p>Add this IPv6 address.</p>
</DIV>
<DIV id=i_content5>
<h2>Delete address</h2>
<p><b>Delete</b></p>
<p>Delete this IPv6 address.</p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML

a=0
