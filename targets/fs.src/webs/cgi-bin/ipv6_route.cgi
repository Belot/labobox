#!/bin/sh

action=0
mustsave=0

. ./functions.sh
. ./login.cgi

HOME="ipv6_index.cgi"

ipv6_routes=`nvram get ipv6_add_routes`
[ "$ipv6_routes" = "Failed" ] && ipv6_routes=""

err=0
########################### Add #####################################
if [ "$action" -eq 10 ]; then
  
  http_unquote address
  http_unquote gateway

  [ "$address" == "" ] && errmsg="$errmsg IPv6 address/route not specified!"'\n' && err=1
  
  if [ "$err" = "0" ]; then
    is_ipv6_addr_with_prefix $address
    ret=$?
    [ "$ret" != "0" ] && ipv6_errmsg=`get_ipv6_err_string $ret` && errmsg="$errmsg Invalid IPv6 address/route (${addr} -- ${ipv6_errmsg})!"'\n' && err=1
  fi


  
  if [ "$option" != "2" ]; then
    if [ "$err" = "0" ] && [ "$gateway" != "" ]; then
      is_ipv6_addr $gateway
      ret=$?
      [ "$ret" != "0" ] && ipv6_errmsg=`get_ipv6_err_string $ret` && errmsg="$errmsg Invalid gateway address (${gateway} -- $ipv6_errmsg)!"'\n' && err=1
    fi
    [ "$device" = "" ] && errmsg="$errmsg Unknown error!"'\n' && err=1

    [ "$device" = "-1" ] && [ "$gateway" = "" ] && errmsg="$errmsg You must specify a gateway and/or a device!"'\n' && err=1
    
    if [ "$metric" != "" ]; then
      isNumeric "$metric"
      if [ "$?" -eq "0" ]; then
        [ "$metric" -lt 0 ] && errmsg="$errmsg Metric is invalid ($metric)!"'\n' && err=1
      else
        errmsg="$errmsg Metric is invalid ($metric)!"'\n'
        err=1
      fi 
    fi
  fi
  
  if [ "$option" != "0" ]; then
    isNumeric "$rad_lifetime"
    if [ "$?" -eq "0" ]; then
      [ "$rad_lifetime" -lt 0 ] && errmsg="$errmsg Valid lifetime is invalid ($rad_lifetime) set it to 2592000!"'\n' && rad_lifetime="2592000"
      
      [ "$rad_lifetime" -gt 4294967295 ] && errmsg="$errmsg Valid lifetime is invalid ($rad_lifetime) set it to 2592000!"'\n' && rad_lifetime="2592000"
    else
      errmsg="$errmsg Valid lifetime is invalid ($rad_lifetime) set it to 2592000!"'\n'
      rad_lifetime="2592000"
    fi 
    
    isNumeric "$rad_preftime"
    if [ "$?" -eq "0" ]; then
    [ "$rad_preftime" -lt 0 ] && errmsg="$errmsg Preferred lifetime is invalid ($rad_preftime) set it to 604800!"'\n' && rad_preftime="604800"
    
    [ "$rad_preftime" -gt 4294967295 ] && errmsg="$errmsg Preferred lifetime is invalid ($rad_preftime) set it to 604800!"'\n' && rad_preftime="604800"
    else
      errmsg="$errmsg Preferred lifetime is invalid ($rad_preftime) set it to 604800!"'\n'
      rad_preftime="604800"
    fi
  fi
  
  case $device in
    0) device="br0" ;;
    1) device="wl0" ;;
    *) device="none" ;;
  esac
  
  if [ "$err" = "0" ]; then
    ipv6_routes=$ipv6_routes"$option,$address,$rad_onlink,$rad_mobile,$rad_auto,$rad_lifetime,$rad_preftime,$gateway,$device,$metric~"
    res=`nvram set ipv6_add_routes=$ipv6_routes`
    mustsave=1 
    [ "$res" != "OK" ] && errmsg="$errmsg Write to NVRam failed!"'\n'

    # duplikat check
    dup=`echo $ipv6_routes|sed -e 's/~/\n/g'|cut -d ',' -f 2|sort|awk 'BEGIN{h=""}{if(h==""){h=$1; next;} if(h==$1){print $1;} h=$1;}'|tr  '\n' ' '`
    [ "$dup" != "" ] && errmsg="$errmsg Warning duplicate route: $dup"'\n'
  fi
fi


################################## Del #####################################

if [ "$action" -eq 11 ] && [ "$select" != "" ]; then
  http_unquote select
  X=`echo "$select"|sed -e 's/%2C/,/g'|sed -e 's/%2c/,/g'`

  ipv6_routes=`echo $ipv6_routes|sed -e "s-$X~--"`
  res=`nvram set ipv6_add_routes=$ipv6_routes`
  [ "$res" != "OK" ] && errmsg="$errmsg Write to NVRam failed!"'\n' || mustsave=1
fi

################################ Save & Run #################################
if [ "$action" -eq 3 ] || [ "$action" -eq 2 ]; then
  [ "$action" -eq 3 ] && /etc/start_scripts/ipv6.sh restart >/dev/null 2>/dev/null
  [ "$mustsave" = "1" ] && nvram commit >/dev/null
fi

ipv6_add_routes=`nvram get ipv6_add_routes`
[ "$ipv6_add_routes" = "Failed" ] && ipv6_add_routes=""

rad_auto_sel='checked="checked"'
rad_lifetime="2592000"
rad_preftime="604800"

sel='selected="selected"'
case "$option" in
  0) sel_0=$sel
    ;;
  1) sel_1=$sel
    ;;
  2) sel_2=$sel
    ;;
  *) sel_1=$sel
     option=1
    ;;
esac

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function showErr() {
var err="$errmsg";
if(err!=""){alert(err);} }
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>IPv6 settings / IPv6 routes</DIV>
<DIV id=c_titel>Add route</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<input type="hidden" name="mustsave" value="$mustsave">
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0) >
<td width="120">Route options:</td>
<td><select name="option" size="1" onchange="subm(5);">
<option value="0" $sel_0>don't advertise this route</option>
<option value="1" $sel_1>advertise this route</option>
<option value="2" $sel_2>advertise but set no route</option>
</select></td></tr>
</table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0) >
<td width="120">Address/Route:</td>
<td>
<input type="input" name="address" size="40" maxlength="35" value=""></td>
</td></tr>
</table>
</DIV>
EOF_HTML

if [ "$option" -gt 0 ]; then  # advertise
cat << EOF_HTML
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0) >
<td width="120">On-Link:</td><td>
<input type="checkbox" name="rad_onlink" value="1" $rad_onlink></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0) >
<td width="120">Mobile:</td><td>
<input type="checkbox" name="rad_mobile" value="1" $rad_mobile_sel></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(5) onmouseout=i_showElem(0) >
<td width="120">Auto configuration:</td><td>
<input type="checkbox" name="rad_auto" value="1" $rad_auto_sel></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(6) onmouseout=i_showElem(0) >
<td width="120">Valid lifetime:</td><td>
<input type="input" name="rad_lifetime" size="12" maxlength="10" value="$rad_lifetime"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(7) onmouseout=i_showElem(0) >
<td width="120">Preferred lifetime:</td><td>
<input type="input" name="rad_preftime" size="12" maxlength="10" value="$rad_preftime"></td>
</tr></table>
</DIV>
EOF_HTML
fi

if [ "$option" != "2" ]; then  # route
cat << EOF_HTML
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(8) onmouseout=i_showElem(0) >
<td width="120">Gateway:</td><td>
<input type="input" name="gateway" size="40" maxlength="35" value=""></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(9) onmouseout=i_showElem(0) >
<td width="120">Device:</td>
<td><select name="device" size="1">
<option value="-1" $sel_dev_none></option>
<option value="0" $sel_bridge>Bridge/LAN</option>
<option value="1" $sel_wlan>WLAN</option>
</select></td></tr>
</table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(10) onmouseout=i_showElem(0) >
<td width="120">Metric:</td><td>
<input type="input" name="metric" size="10" maxlength="5" value="$metric"></td>
</tr></table>
</DIV>
EOF_HTML
fi

cat << EOF_HTML
<DIV id=c_last><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(12) onmouseout=i_showElem(0) ><td width=335></td>
<td><input type="button" name="add" value="Add" onclick="subm(10);"></td>
</tr></table>
</DIV>
<div id="c_leer"></div>
<DIV id=c_titel>Delete route</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(11) onmouseout=i_showElem(0) >
<td width="50">Address:</td>
<td><select name="select" size="1" style="width:340px">
EOF_HTML

if [ "$ipv6_add_routes" != "" ]; then
 echo $ipv6_add_routes |sed -e 's/~/\n/g'|sed -n -e '/^$/!p'| \
 awk '{ split($0,a,/[,]/)
        ipv6_route=a[2]
        print "<option value=" $0 ">" ipv6_route "</option>"
     }'
fi

cat << EOF_HTML
</select></td></tr></table></DIV>
<DIV id=c_last><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(13) onmouseout=i_showElem(0) ><td width=335></td>
<td><input type="button" name="del" value="Delete" onclick="subm(11);"></td>
</tr></table></DIV>
</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>IPv6 routes</h2>
<p>Add or remove IPv6 routes.</p>
</DIV>
<DIV id=i_content1>
<h2>Add route</h2>
<p><b>Route options</b></p>
<p><i>don't advertise this route</i> - Add this route to the route table, but don't send router
advertisements for this route.</p>
<p><i>advertise this route</i> - Add this route to the route table and send router
advertisements for this route.</p>
<p><i>advertise but set no route</i> - Don't add this route to the route table, but send router 
advertisements for this route.</p>
</DIV>
<DIV id=i_content2>
<h2>Add route</h2>
<p><b>Address/Route</b></p>
<p>Address of the IPv6 subnet with prefix length.</p>
<p>Example: 2001:1234:abcd::/64</p>
</DIV>
<DIV id=i_content3>
<h2>Add route</h2>
<p><b>On-Link</b></p>
<p>When set, indicates that this prefix can be used for on-link determination. 
When not set the advertisement makes no statement about on-link or off-link 
properties of the prefix. For instance, the prefix might be used for address 
configuration with some of the addresses belonging to the prefix being on-link 
and others being off-link.</p>
</DIV>
<DIV id=i_content4>
<h2>Add route</h2>
<p><b>Mobile</b></p>
<p>When set, indicates that the address of interface is sent instead of network 
prefix, as is required by Mobile IPv6. When set, minimum limits specified by 
Mobile IPv6 are used for MinRtrAdvInterval and MaxRtrAdvInterval. </p>
<p>Default: off</p>
</DIV>
<DIV id=i_content5>
<h2>Add route</h2>
<p><b>Auto configuration</b></p>
<p>When set, indicates that this prefix can be used for autonomous address 
configuration as specified in RFC 2462. </p>
<p>Default: on</p>
</DIV>
<DIV id=i_content6>
<h2>Add route</h2>
<p><b>Valid lifetime</b></p>
<p>The length of time in seconds (relative to the time the packet is sent) 
that the prefix is valid for the purpose of on-link determination. The symbolic 
value infinity represents infinity (i.e. a value of all one bits 
(0xffffffff = 4294967295)).</p>
<p>Default: 2592000 seconds (30 days)</p>
</DIV>
<DIV id=i_content7>
<h2>Add route</h2>
<p><b>Preferred lifetime</b></p>
<p>The length of time in seconds (relative to the time the packet is sent) 
that addresses generated from the prefix via stateless address autoconfiguration 
remain preferred. The symbolic value infinity represents infinity (i.e. a value 
of all one bits (0xffffffff = 4294967295)).</p>
<p>Default: 604800 seconds (7 days)</p>
</DIV>
<DIV id=i_content8>
<h2>Add route</h2>
<p><b>Gateway</b></p>
<p>This gateway is used to send data for this route.</p>
<p>IPv6 address without prefix length.</p>
<p>The gateway and/or a device must be set.</p>
<p>Example: 2001:1234:abc0::1</p>
</DIV>
<DIV id=i_content9>
<h2>Add route</h2>
<p><b>Device</b></p>
<p>This device is used to send data for this route.</p>
<p>The gateway and/or a device must be set.</p>
</DIV>
<DIV id=i_content10>
<h2>Add route</h2>
<p><b>Metric</b></p>
<p>The metric for this route. Let the metric field empty to use the default metric or
type a numeric value. The lower the metric, the higher is the route's priority.
</p>
<p>Values: >=0, empty</p>
<p>Default: empty</p>
</DIV>
<DIV id=i_content11>
<h2>Delete route</h2>
<p><b>Address</b></p>
<p>Choose a route to delete.</p>
</DIV>
<DIV id=i_content12>
<h2>Add route</h2>
<p><b>Add</b></p>
<p>Add this route.</p>
</DIV>
<DIV id=i_content13>
<h2>Delete route</h2>
<p><b>Delete</b></p>
<p>Delete this route.</p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML

a=0
