#!/bin/sh

action=0

. ./login.cgi

f_pfad="Firewall Settings / Custom rules"
f_name="Custom rules"
HOME="fw_index.cgi"

PATH=.:$PATH

errmsg=""
mustsave=0


global=""

http_unquote()
{
  eval local_string="\$$1"
  local_string=`echo "$local_string"|sed -e 's/%/\\\x/g'`
  local_string=`echo "$local_string"|sed -e 's/+/ /g'`
  local_string=`echo "$local_string"|sed -e 's/*/\\\x2a/g'`
  eval $1=\"$local_string\"
}


####### Save & Run #######
if [ "$action" -eq 2 ] || [ "$action" -eq 3 ]; then
  [ "$form_custom_start" != "`nvram get fw_custom_start`" ] && res=`nvram set fw_custom_start=$form_custom_start` && mustsave=1 && [ "$res" != "OK" ] && err=1
  
  http_unquote "text_custom_script"
  text_custom_script=`echo -e "$text_custom_script"|tr -d '\r'`
  [ "$text_custom_script" != "`nvram get fw_custom_script`" ] && res=`nvram set fw_custom_script="$text_custom_script"` && mustsave=1 && [ "$res" != "OK" ] && err=1
  
  [ "$err" -ne 0 ] && errmsg="$errmsg Write to NVRam failed!"'\n'
  
  [ "$mustsave" -eq 1 ] && nvram commit >/dev/null && [ "$action" -eq 3 ] && /etc/start_scripts/firewall.sh restart >/dev/null 2>/dev/null
  
fi

ON='checked="checked"'
SEL="selected"

fw_custom_start=`nvram get fw_custom_start`
[ "$fw_custom_start" = "Failed" ] && fw_custom_start=0
[ "$fw_custom_start" = "1" ] && fw_custom_start_on=$ON || fw_custom_start_off=$ON

fw_custom_script=`nvram get fw_custom_script`
[ "$fw_custom_script" = "Failed" ] && fw_custom_script=""


cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function showErr() {
  var err="$errmsg";
  if(err!=""){alert(err);}
}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>$f_pfad</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<DIV id=c_titel>Custom rules control</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0)><td width="200">Start custom script</td>
<td width="20"><input type="radio" name="form_custom_start" value="0" $fw_custom_start_off></td>
<td width="40">Off</td>
<td width="20"><input type="radio" name="form_custom_start" value="1" $fw_custom_start_on></td>
<td>On</td>
</tr></table>
</DIV>
<fieldset onmouseover=i_showElem(2) onmouseout=i_showElem(0) style="width: 400px; margin-left: 15px;">
<legend> Custom script content</legend>
<textarea id="text_custom_script" name="text_custom_script" rows="15" cols="60" style="font-family:Courier, Courier New" wrap="off">$fw_custom_script</textarea>
</fieldset>
</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but2 onclick="subm(2);" onmouseover=rahmen(1,2) onmouseout=rahmen(0,2)>Save</DIV>
<DIV id=t_but3 onclick="subm(3);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Custom rules</h2>
<p>Control startup and edit your custom firewall rules.</p>
</DIV>
<DIV id=i_content1>
<h2>Start custom rules</h2>
<p>Turn the startup of your custom rules <b>on</b> or <b>off</b>. If <b>on</b> the content of the custom script will 
be executed by the firewall-script.</p>
</DIV>
<DIV id=i_content2>
<h2>Custom rules content</h2>
<p>Enter your custom rules or whatever else, which should be executed after setting or changing the firewall.</p>
</DIV>
</DIV></BODY></HTML>
