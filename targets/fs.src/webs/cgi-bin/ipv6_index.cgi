#!/bin/sh

ipv6_start=`/bin/nvram get ipv6_start`
ipv6_on='Off'
if [ "$ipv6_start" = "Failed" ]; then
ipv6_start="0"
fi
[ "$ipv6_start" -eq "1" ] && ipv6_on='On'

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY>
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Network settings / IPv6</DIV>

<DIV id=c_titel>IPv6 settings</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td width="200"><a href="ipv6_basic.cgi" onmouseover=i_showElem(1) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>Basic configuration</a></td><td>$ipv6_on</td></tr>
</table>
</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr> <td width="200"><a href="ipv6_address.cgi" onmouseover=i_showElem(6) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>additional IPv6 addresses</a></td></tr>
</table>
</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr> <td width="200"><a href="ipv6_route.cgi" onmouseover=i_showElem(2) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>IPv6 routes</a></td></tr>
</table>
</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr> <td width="200"><a href="ipv6_auto_tunnel.cgi" onmouseover=i_showElem(7) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>Automatic IPv6 6to4 tunnel</a></td></tr>
</table>
</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr> <td width="200"><a href="ipv6_tunnel.cgi" onmouseover=i_showElem(3) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>IPv6 6to4 tunnels</a></td></tr>
</table>
</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr> <td width="200"><a href="ipv6_overview.cgi" onmouseover=i_showElem(5) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>IPv6 overview</a></td></tr>
</table>
</DIV>
<DIV id="c_foot"></DIV>
</DIV></DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>IPv6 settings</h2>
<p>Setup IPv6.</p>
</DIV>
<DIV id=i_content1>
<h2>IPv6 settings</h2>
<p><b>Basic configuration</b><br>
</p>
</DIV>
<DIV id=i_content2>
<h2>IPv6 settings</h2>
<p><b>IPv6 routes</b><br>
</p>
</DIV>
<DIV id=i_content3>
<h2>IPv6 settings</h2>
<p><b>IPv6 6to4 tunnels</b><br>
</p>
</DIV>
<DIV id=i_content5>
<h2>IPv6 settings</h2>
<p><b>IPv6 overview</b><br>
</p>
</DIV>
<DIV id=i_content6>
<h2>IPv6 settings</h2>
<p><b>additional IPv6 addresses</b><br>
</p>
</DIV>
<DIV id=i_content7>
<h2>IPv6 settings</h2>
<p><b>Automatic IPv6 6to4 tunnel</b></p>
<p>Automatic IPv6 6to4 tunnel assign a 2002::/16 IPv6 address in combination with the
public IPv4 address to the router and uses public relay routers (192.88.99.1) to gain 
access to the IPv6 internet.</p>
</DIV></BODY></HTML>
EOF_HTML

a=0
