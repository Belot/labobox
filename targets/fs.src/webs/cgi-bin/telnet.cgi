#!/bin/sh

from=telnet.cgi
. ./login.cgi

f_pfad="Services / Telnet"
f_name="Telnet"
f_text="The feature  &quot;Telnet;&quot; is not included!"
f_bin="/usr/sbin/telnetd"
. ./feature.cgi
. ./functions.sh

mustsave=0

#### Formular-Auswertung #######################################################
#SAVE 
if [ "$action" -eq 2 ] || [ "$action" -eq 3 ]; then 
  [ "$form_telnet_start" != "`nvram get telnet_start`" ] && res=`nvram set telnet_start=$form_telnet_start` && mustsave=1 && [ "$res" != "OK" ] && err=1
  
  if [ "$form_telnet_fw_open" = "1" ]; then
    is_fwservice_port "tcp" "23" || add_fwservice_port "tcp" "23" && mustsave=1 && fw_restart=1
  else
    is_fwservice_port "tcp" "23" && remove_fwservice_port "tcp" "23" && mustsave=1 && fw_restart=1
  fi
  [ "$err" -ne "0" ] && errmsg="$errmsg Write to NVRam failed!"'\n'

  [ "$mustsave" -eq 1 ] && nvram commit >/dev/null && [ "$action" -eq 3 ] && /etc/start_scripts/telnet.sh restart >/dev/null 2>/dev/null && [ "$fw_restart" = "1" ] && /etc/start_scripts/firewall.sh services_restart >/dev/null 2>/dev/null

fi
################################################################################

[ "$mustsave" -eq 1 ] && nvram commit >/dev/null

telnet_start=`/bin/nvram get telnet_start`
[ "$telnet_start" = "Failed" ] && telnet_start=1
[ "$telnet_start" = "1" ] && telnet_on='checked="checked"' || telnet_off='checked="checked"'

is_fwservice_port "tcp" "23" && fw_open='checked="checked"' || fw_closed='checked="checked"'

cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>

<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function telnet_change() {
  if(document.getElementById("form_telnet_start_off").checked==true){
    document.getElementById("form_telnet_fw_open_off").disabled=true;
    document.getElementById("form_telnet_fw_open_on").disabled=true;
  }
  else{
    document.getElementById("form_telnet_fw_open_off").disabled=false;
    document.getElementById("form_telnet_fw_open_on").disabled=false;
  }
}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="telnet_change();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Services</DIV>
<DIV id=c_titel>Telnet</DIV>
<DIV id=c_std>
<form id="xform" name="xform" method="post">
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0)>
<td width="90">Telnet</td>
<td width="20"><input type="radio" id="form_telnet_start_off" name="form_telnet_start" value="0" onchange="telnet_change();" $telnet_off></td>
<td width="40">Off</td>
<td width="20"><input type="radio" id="form_telnet_start_on" name="form_telnet_start" value="1" onchange="telnet_change();" $telnet_on></td>
<td>On</td>
</tr></table>
</DIV>
<DIV id=c_last>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0)>
<td width="90">Firewall</td>
<td width="20"><input type="radio" id="form_telnet_fw_open_off" name="form_telnet_fw_open" value="0" $fw_closed></td>
<td width="40">Close</td>
<td width="20"><input type="radio" id="form_telnet_fw_open_on" name="form_telnet_fw_open" value="1" $fw_open></td>
<td>Open</td>
<input id="action" type="hidden" name="action" value="0">
</tr></table></form>
</DIV>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but3 onclick="subm(2);" onmouseover="rahmen(1,3);i_showElem(6)" onmouseout="rahmen(0,3);i_showElem(0)">Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover="rahmen(1,4);i_showElem(7)" onmouseout="rahmen(0,4);i_showElem(0)">Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Telnet</h2>
<p>Start/Stop telnet-service and control telnet-access on public interface</p>
</DIV>
<DIV id=i_content1>
<h2>Telnet</h2>
<p><b>Start/Stop</b><br>Start Telnet-service Yes/No</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Setting is checked on next reboot</p>
</DIV>
<DIV id=i_content2>
<h2>Firewall</h2>
<p><b>Firewall</b><br>Open Telnet-Port in firewall</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Setting controls only access for public IP-adresses</p>
</DIV>
<DIV id=i_content6>
<h2>Telnetd</h2>
<p><b>Save</b><br>Saves changed settings to NVRAM</p>
</DIV>
<DIV id=i_content7>
<h2>Telnetd</h2>
<p><b>Save&amp;Run</b><br>Save changed settings to NVRAM and restart telnetd/firewall</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Changes are safed permanently</p>
</DIV>
</DIV></BODY></HTML>

