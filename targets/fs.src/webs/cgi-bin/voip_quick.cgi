#!/bin/sh

from=voip_quick.cgi
#how much free entries after last valid will be showed
SHOW_MORE=20

. ./login.cgi

HOME="voip_index.cgi"
. ./functions.sh

 #SAVE
if [ "$action" -eq 2 ] || [ "$action" -eq 3 ]; then
  
 commit=0
 i=0
 while [ $i -le $last_entry ]
 do
  eval number=\$form_qn$i
  eval desc=\$form_qd$i
  eval line=\$form_qc$i

  if [ "$number" != "" ]; then
    [ "$line" == "voip" ] && number=`echo $number|sed -e 's/[^a-z0-9A-Z_.]//g'` || number=`echo $number|sed -e 's/[^0-9]//g'`
  fi
  
  ret=`nvram get ata_qn$i`
  if [ "$number" = "" ]; then 
    [ "$ret" != "Failed" ] && hret=`nvram set ata_qn$i=$number` && commit=1 
  else if [ "$number" != "$ret" ]; then
          hret=`nvram set ata_qn$i=$number` && commit=1 
        fi
  fi
  
  ret=`nvram get ata_qd$i`
  if [ "$desc" = "" ]; then 
    [ "$ret" != "Failed" ] && hret=`nvram set ata_qd$i=$desc` && commit=1 
  else if [ "$desc" != "$ret" ]; then
          http_unquote "desc"
          [ "$desc" != "$ret" ] && hret=`nvram set "ata_qd$i=$desc"` && commit=1 
        fi
  fi

  ret=`nvram get ata_qc$i`
  if [ "$line" = "" ]; then 
    [ "$ret" != "Failed" ] && hret=`nvram set ata_qc$i=$line` && commit=1 
  else if [ "$line" != "$ret" ]; then
          hret=`nvram set ata_qc$i=$line` && commit=1 
        fi
  fi
 
  let i=$i+1
 done
 if [ $commit -eq 1 ]; then
   LAST_QUICK=$last_entry /etc/start_scripts/ata.sh update_psi >/dev/null 2>/dev/null
   nvram commit >/dev/null
 fi
fi

# RUN
if [ "$action" -eq 3 -a $commit -eq 1 ]; then
  #took this into background because sometimes this script doesnt return properly
  /etc/start_scripts/ata.sh restart >/dev/null 2>/dev/null &
  sleep 5
fi

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>

<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function openPrint() {
  win = window.open("quick_print.cgi","quick_print");
  win.focus();
}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</head>

<BODY>
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Phone settings / speed calling</DIV>
<form id="xform" name="xform" method="post" onmouseover=i_showElem(1) onmouseout=i_showElem(0)>
<input id="action" type="hidden" name="action" value="0">
<table border="0" cellpadding="0" cellspacing="2" style="text-align: center;">
  <tr><td width="35">QD</td><td width="130">Phone number</td><td width="130">Description</td><td width="35">Connection</td></tr>
</table>

<div style="overflow: auto; height: 350px;"
EOF_HTML

i=0
end=$SHOW_MORE
SEL="selected"
while [ $i -le 99 -a $i -le $end ]
do  
    number=`nvram get ata_qn$i`
    desc=`nvram get ata_qd$i`
    line=`nvram get ata_qc$i`
    
    [ "$number" = "Failed" ] && number=""
    [ "$desc" = "Failed" ] && desc=""
    [ "$line" = "Failed" ] && line=0
    
    voip=""
    pstn=""
    both=""
    [ $line -eq 0 ] && voip=$SEL
    [ $line -eq 1 ] && pstn=$SEL
    [ $line -eq 2 ] && both=$SEL
    
    qd=$i
    [ $qd -le 9 ] && qd="0$qd"

    echo '   <DIV id=c_std>'
    echo '   <table border="0" cellpadding="0" cellspacing="2">'
    echo "   <tr>"
    echo "     <td width=\"35\">$qd#</td>"
    echo "     <td width=\"130\"><input size=\"16\" maxlength=\"16\" value=\"$number\" type=\"text\" name=\"form_qn$i\" ></td>"
    echo "     <td width=\"130\"><input size=\"16\" maxlength=\"27\" value=\"$desc\" type=\"text\" name=\"form_qd$i\" ></td>"
    echo "     <td width=\"50\">"
    echo "       <select name=\"form_qc$i\" size=\"1\">"
    echo "         <option value=\"0\" $voip>VoIP</option><option value=\"1\" $pstn>PSTN</option><option value=\"2\" $both>both</option>"
    echo "       </select>"
    echo "     </td>"
    echo "   </tr></table></DIV>"
    
    if [ "$number" != "" -o "$desc" != "" ]; then
      let end=$i+$SHOW_MORE
    fi
    
    let i=$i+1
done  

cat << EOF_HTML
</div>
<input id="last_entry" name="last_entry" type="hidden" value="$end">
</form>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but2 onclick=openPrint()  onmouseover=rahmen(1,2) onmouseout=rahmen(0,2)>Print  &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save &lt;&lt;</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>speed calling</h2>
<p></p>
</DIV>
<DIV id=i_content1>
<h2>speed calling</h2>
<p><b>QD</b><br>
quick dialing phone number</p>
<p><b>Phone number</b><br>
Phone number to diall</p>
<p><b>Description</b><br>
Your description for the phone number</p>
<p><b>Connection</b><br>
Allow to call the phone number via VoIP or PSTN or both</p>
</DIV>
</DIV></BODY></HTML>
