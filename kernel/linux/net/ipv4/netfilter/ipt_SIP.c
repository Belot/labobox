/* SIP modification module for IP tables
 * (C) 2010 by Patrick Schmidt <coolman1982@users.sourcforge.net>
 *             Michael Finsterbusch <fibu@users.sourcforge.net>
 *
 *
 *
 * This software is distributed under the terms of GNU GPL
 */

#include <linux/module.h>
#include <linux/skbuff.h>
#include <linux/ip.h>
#include <linux/in.h>
#include <linux/udp.h>
#include <net/checksum.h>

#include <linux/netfilter_ipv4/ip_tables.h>
#include <linux/netfilter_ipv4/ipt_SIP.h>

MODULE_AUTHOR("Patrick Schmidt <coolman1982@users.sourcforge.net\nMichael Finsterbusch <fibu@users.sourcforge.net");
MODULE_DESCRIPTION("IP tables SIP modification module");
MODULE_LICENSE("GPL");

//#define SIP_DEBUG

static u8 workbuf[1500];

#ifdef SIP_DEBUG
static u8 type_name[2][20]={ "IPT_SIP_TYPE_HEADER", "IPT_SIP_TYPE_BODY" };
static u8 mode_name[3][20]={ "IPT_SIP_DEL", "IPT_SIP_REPLACE", "IPT_SIP_ADD" };
#endif

static int str_getline(u8* buf,u32 len)
{
  static u8 delimiter[]={0x0d,0x0a};
  int ret=0;
  
  if(!buf)
    return -1;
   
  while(len>=sizeof(delimiter))
  {
    if(memcmp(buf,delimiter,sizeof(delimiter))==0)
    {
      ret+=sizeof(delimiter);
      return ret;
    }
    ret++;
    len--;
    buf++;
  }
    
  return ret;
}


//adds a new line to sip packet, type specifies if attr_name is added into the SIP-header or if 
//it is added at the packets end as an attribute
static int sip_add_attribute(u_int8_t type,u8 *buf, u32 data_len, u32 buf_len, const u8 *attr_name)
{
  u8* pos=buf;
  u32 line_len;
  u32 attr_len=strlen(attr_name);
  u8 tmp[1024];
  
  if( data_len+attr_len>buf_len )
    return 0;
  
  //if we have to add an attribute we simply have to add it at the end
  if( type==IPT_SIP_TYPE_BODY )
  {
      memcpy(pos+data_len,attr_name,attr_len);
      return attr_len;
  }
  else if( type==IPT_SIP_TYPE_HEADER )
  {
    while((line_len=str_getline(pos,data_len))>0)
    {
      //the end of the header is an empty line ('\r\n')
      if( line_len==2 )
      {
	//check if the rest of the data can be copied to tmp
	if( (data_len > sizeof(tmp)) )
	  return 0;
	
	//first save the rest of the data in tmp
	memcpy(tmp,pos,data_len);
	//insert the new attribute into the buffer
	memcpy(pos,attr_name,attr_len);
	//copy the rest data from tmp after the inserted attribute
	memcpy(pos+attr_len,tmp,data_len);
	
	return attr_len;
	
      }
      pos+=line_len;
      data_len-=line_len;
    }
  }
  
  return 0;
}

//search and replace attr_name with new_attr
//returns the difference between the old and the resulting data_len
static int sip_replace_attribute(u8 *buf, u32 data_len, u32 buf_len, const u8 *attr_name, const u8 *new_attr)
{
  u8* pos=buf;
  u32 line_len;
  u32 rest_len;
  u32 attr_len=strlen(attr_name);
  u32 new_attr_len=strlen(new_attr);
  u8 tmp[1024];

  while((line_len=str_getline(pos,data_len))>0)
  {
    if(line_len>=attr_len && memcmp(pos,attr_name,attr_len)==0)
    {
      //check if we have to only remove the attribute
      if( new_attr_len==0 )
      {
	//move the rest of the data to the start of the attribute to replace
	memmove(pos,pos+line_len,data_len-line_len);
	return -line_len;
      }
      
      //check if the rest of the data can be copied to tmp
      //and if the data buffer is big enough for the new attribute
      rest_len=data_len-line_len;
      if( (rest_len > sizeof(tmp)) || (rest_len+new_attr_len>buf_len) )
	return 0;

      //first save the rest of the data in tmp
      memcpy(tmp,pos+line_len,rest_len);
      //copy the new attribute to over the attribute to replace
      memcpy(pos,new_attr,new_attr_len);
      //copy the rest data from tmp after the inserted attribute
      memcpy(pos+new_attr_len,tmp,rest_len);
      
      //return the difference between old and new data_len
      return new_attr_len-line_len;

    }
    
    pos+=line_len;
    data_len-=line_len;
  }
  
  return 0;
}

//sets the Content-length field inside the SIP-header
static int sip_set_length(u8 *buf,u32 data_len,u32 buf_len,int len_diff)
{
  u8 attr_name[]="Content-Length: ";
  u32 attr_len=strlen(attr_name);
  u8* pos=buf;
  u32 ret;
  u32 old_len=0;
  u32 orig_data_len=data_len;
  u32 mod_data_len=data_len;
  u8 tmp[512];
  u8 body_flag=0;
  u16 body_len=0;
  
  while((ret=str_getline(pos,data_len))>0)
  {
    if(ret>=attr_len && memcmp(pos,attr_name,attr_len)==0)
    {
      //get the old content length from packet as integer
      sscanf(pos+attr_len,"%i",&old_len);
      if( old_len>0 )
      {
	//write the new Content-Length string to tmp
	snprintf(tmp,sizeof(tmp),"%s%i\r\n",attr_name,old_len+len_diff);
	return sip_replace_attribute(pos,data_len,buf_len,attr_name,tmp);
      }
      
      //if the content length is zero delete the attribute and recalc the length
      mod_data_len+=sip_replace_attribute(pos,data_len,buf_len,attr_name,"");
      goto RECALC;
    }
    
    pos+=ret;
    data_len-=ret;
  }
  
RECALC:
  pos=buf;
  data_len=mod_data_len;

  //calc the body length and insert a new content length line into the sip-header
  while((ret=str_getline(pos,data_len))>0)
  {
    if( ret==2 )
      body_flag=1;
    else if( body_flag==1 )
      body_len+=ret;
    
    pos+=ret;
    data_len-=ret;
  }
  
  snprintf(tmp,sizeof(tmp),"%s%i\r\n",attr_name,body_len);
  mod_data_len+=sip_add_attribute(IPT_SIP_TYPE_HEADER,buf,mod_data_len,buf_len,tmp);
  return mod_data_len-orig_data_len;
}

static unsigned int
ipt_sip_target(struct sk_buff **pskb,
		const struct net_device *in,
		const struct net_device *out,
		unsigned int hooknum,
		const void *targinfo,
		void *userinfo)
{
	int len_diff=0;
	int content_len=0;
	struct iphdr *iph = (*pskb)->nh.iph;
	struct udphdr *udp=(struct udphdr *) (*pskb)->h.uh;
	u32 data_len=ntohs(udp->len)-sizeof(struct udphdr);
	u8 *data=((u8*)udp)+sizeof(struct udphdr);
	const struct ipt_SIP_info *info = targinfo;
	
	if( iph->protocol!=IPPROTO_UDP )
	  return IPT_CONTINUE;
	
	//if packet is too long
	if( ntohs(udp->len)+sizeof(struct udphdr)>sizeof(workbuf) )
	  return IPT_CONTINUE;
	
	if(memcmp(data,info->sip_method,strlen(info->sip_method))==0)
	{
	  memset(workbuf,0,sizeof(workbuf));
	  //copy udp-hdr + udp-data into workbuf
	  memcpy(workbuf,(u8*)udp,ntohs(udp->len)+sizeof(struct udphdr));
	  
	  data=workbuf+sizeof(struct udphdr);
	  if( info->mode==IPT_SIP_DEL )
	    len_diff=sip_replace_attribute(data,data_len,sizeof(workbuf),info->attr_name,"");
	  else if( info->mode==IPT_SIP_REPLACE )
	    len_diff=sip_replace_attribute(data,data_len,sizeof(workbuf),info->attr_name,info->attr_replace);
	  else if( info->mode==IPT_SIP_ADD )
	    len_diff=sip_add_attribute(info->type,data,data_len,sizeof(workbuf),info->attr_name);
	  else
	    return IPT_CONTINUE;
	  
	  if( info->type==IPT_SIP_TYPE_BODY && len_diff!=0 )
	  {
	    content_len=sip_set_length(data,data_len+len_diff,sizeof(workbuf),len_diff);
	    len_diff+=content_len;
	  }
#ifdef SIP_DEBUG
	  printk(KERN_ERR "ipt_SIP: %s %d, %s--%s--%s, len_diff=%d content_len=%d\n",__func__,__LINE__,type_name[info->type-1],mode_name[info->mode-1], info->sip_method, len_diff, content_len);
#endif
	  //check if we have to expand or to trim the skb
	  if( len_diff>0 )
	  {

	    if( skb_tailroom(*pskb)<len_diff )
	    {
	      struct sk_buff *new_skb=skb_copy_expand(*pskb,0,len_diff,GFP_ATOMIC);
	      if( !new_skb )
		return IPT_CONTINUE;

	      dev_kfree_skb_any(*pskb);
	      *pskb = new_skb;
	    }
	    //expand the skb
	    if( !skb_put(*pskb,len_diff) )
	      return IPT_CONTINUE;
	  }
	  else
	    pskb_trim(*pskb,(*pskb)->len+len_diff);
	  
	  //copy the workbuf into the expanded skbuf
	  memcpy((u8*)udp,workbuf,sizeof(struct udphdr)+(data_len+len_diff));

          //set ip total length
	  iph->tot_len=htons(ntohs(iph->tot_len)+len_diff);
          //recalc ip header checksum
          iph->check = 0;
	  iph->check = ip_fast_csum((unsigned char *)iph, iph->ihl);
	  
          //set udp-header length
          udp->len=htons(ntohs(udp->len)+len_diff);
	  //recalc the udp checksum
	  udp->check=0;
	  udp->check=csum_tcpudp_magic(iph->saddr, iph->daddr,ntohs(udp->len), IPPROTO_UDP, csum_partial((char *)udp,ntohs(udp->len), 0));
	  
	  (*pskb)->nfcache |= NFC_ALTERED;
	}
	

	return IPT_CONTINUE;
}

static int ipt_sip_checkentry(const char *tablename,
		const struct ipt_entry *e,
		void *targinfo,
		unsigned int targinfosize,
		unsigned int hook_mask)
{
	if (targinfosize != IPT_ALIGN(sizeof(struct ipt_SIP_info))) {
		printk(KERN_WARNING "SIP: targinfosize %u != %Zu\n",
				targinfosize,
				IPT_ALIGN(sizeof(struct ipt_SIP_info)));
		return 0;	
	}	

	if (strcmp(tablename, "mangle")) {
		printk(KERN_WARNING "SIP: can only be called from \"mangle\" table, not \"%s\"\n", tablename);
		return 0;
	}
	return 1;
}

static struct ipt_target ipt_SIP = {
  .name		= "SIP",
  .target	= ipt_sip_target,
  .checkentry	= ipt_sip_checkentry,
  .me		= THIS_MODULE,
};

static int __init init(void)
{
	return ipt_register_target(&ipt_SIP);
}

static void __exit fini(void)
{
	ipt_unregister_target(&ipt_SIP);
}

module_init(init);
module_exit(fini);
