/* SIP modification module for IP tables
 * (C) 2010 by Patrick Schmidt <coolman1982@users.sourcforge.net>
 *             Michael Finsterbusch <fibu@users.sourcforge.net> */

#ifndef _IPT_SIP_H
#define _IPT_SIP_H

enum {
	IPT_SIP_DEL = 1,
	IPT_SIP_REPLACE,
        IPT_SIP_ADD,
};

enum {
  IPT_SIP_TYPE_HEADER = 1,
  IPT_SIP_TYPE_BODY,
};

struct ipt_SIP_info {
	u_int8_t 	mode;
        u_int8_t 	type;
	unsigned char	sip_method[50];
	unsigned char	attr_name[100];
	unsigned char	attr_replace[500];
};


#endif
