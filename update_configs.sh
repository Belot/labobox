#!/bin/bash

config4=${KERNEL_DIR}/config_ipv4
config6=${KERNEL_DIR}/config_ipv6

if [ "${BRCM_CHIP}" = "6338" ]
then
  config4=${config4}_96332
  config6=${config6}_96332
fi

case "$1" in
        kernel)
	        if [ "${BUILD_IPv6}" = "0" ]
        	then
			echo "updating config $config4"
	               diff ${KERNEL_DIR}/.config $config4 > /dev/null
        	       [ "$?" != "0" ] &&  cp -f $config4 ${KERNEL_DIR}/.config
	        else
	               diff ${KERNEL_DIR}/.config $config6 > /dev/null
	               [ "$?" != "0" ] &&  cp -f $config6 ${KERNEL_DIR}/.config
	        fi
                ;;
        busybox)
		if [ "${BUILD_IPv6}" = "0" ]
		then
			diff ${OPENSOURCE_DIR}/busybox/.config ${OPENSOURCE_DIR}/busybox/config_ipv4 > /dev/null 
			[ "$?" != "0" ] && cp -f ${OPENSOURCE_DIR}/busybox/config_ipv4 ${OPENSOURCE_DIR}/busybox/.config 
		else
			diff ${OPENSOURCE_DIR}/busybox/.config ${OPENSOURCE_DIR}/busybox/config_ipv6 > /dev/null
                        [ "$?" != "0" ] && cp -f ${OPENSOURCE_DIR}/busybox/config_ipv6 ${OPENSOURCE_DIR}/busybox/.config
		fi
                ;;
        *)
                ;;
esac


exit 0

