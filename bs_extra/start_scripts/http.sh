#!/bin/sh
#
# Start/stops thttpd
#
#
PID="/var/run/thttpd.pid"
HTTPRUN="/var/httprun.sh"

case "$1" in
	start)
	        echo "Starting thttpd"
                http_start=`/bin/nvram get http_start`
                [ "$http_start" != "Failed" ] && [ "$http_start" != "1" ] && exit 0
                
                port=`/bin/nvram get http_port`
                [ "$port" = "Failed" ] && port=80
                
		echo "#!/bin/sh" >$HTTPRUN
		echo "while [ 1 -ne 2 ]; do" >>$HTTPRUN
		echo " /bin/thttpd -nos -p $port -d /webs -c '**.cgi' -l /dev/null -i $PID -u root -D" >>$HTTPRUN
		echo "done" >>$HTTPRUN
		chmod +x $HTTPRUN
		$HTTPRUN &>/dev/null 2>/dev/null
		;;
	stop)
	        echo "Stopping thttpd"
		killall httprun.sh >/dev/null 2>/dev/null
		killall -9 httprun.sh >/dev/null 2>/dev/null
		kill `cat $PID` >/dev/null 2>/dev/null
		kill -9 `cat $PID` >/dev/null 2>/dev/null
		;;
	restart)
		$0 stop
		usleep 300000
		$0 start
		;;
	*)
		echo "Usage: /etc/start_scripts/http.sh {start|stop|restart}"
		exit 1
		;;
esac

exit 0



