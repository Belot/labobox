#!/bin/sh
#
# Start/stops firewall

exec_cmd()
{
  echo "$1" >>$DEBUG_FILE 2>>$DEBUG_FILE && /bin/sh -c "$1" >>$DEBUG_FILE 2>>$DEBUG_FILE
  return 0
}

#usage: get_external_interface <var_name>
#puts name of external interface to var_name
get_external_interface()
{
  echo "get_external_interface()" >>$DEBUG_FILE
  wan_if=`nvram get fw_wan_if`
  [ "$wan_if" = "Failed" ] && wan_if="ppp"
  if [ "$wan_if" = "ppp" ] || [ "$wan_if" = "dsl" ] || [ "$wan_if" = "atm" ]; then
    atm_vpi=`nvram get atm_vpi`
    [ "$atm_vpi" = "Failed" ] && atm_vpi=1
    atm_vci=`nvram get atm_vci`
    [ "$atm_vci" = "Failed" ] && atm_vci=32
    wan_if=ppp_"$atm_vpi"_"$atm_vci"_1
  fi
  echo "get_external_interface() external interface is $wan_if" >>$DEBUG_FILE
  eval $1=\"$wan_if\" 2>/dev/null
}

#setting up kernel params
setup_kernel_params()
{
  echo "setup_kernel_params()" >>$DEBUG_FILE
  exec_cmd "echo 1 > /proc/sys/net/ipv6/conf/all/forwarding" #Routing
}

setup_kernel_params_filter()
{
  echo "setup_kernel_params()" >>$DEBUG_FILE
  exec_cmd "echo 0 > /proc/sys/net/ipv6/conf/all/accept_redirects"     # ICMP redirects off
}

#sets up a chain called SERVICES_INPUT and fills it with portbased rules ffrom NVRAM fw_services_tcp and fw_services_udp
#these lists should contain port numbers from local running services like ssh, telnet voip if they should be accessible from the internet
#setup_services_input() is called by setup_filter_table() and from main function in case: services_restart
setup_services_input()
{
  echo "setup_services_input()" >>$DEBUG_FILE
  exec_cmd "ip6tables -N SERVICES_INPUT"
  exec_cmd "ip6tables -F SERVICES_INPUT"

  ssh_port=`nvram get ssh_port`
  [ "$ssh_port" = "Failed" ] && ssh_port="22"

  tcp_ports=`nvram get fw_services_tcp`
  [ "$tcp_ports" = "Failed" ] && tcp_ports="$ssh_port"

  for port in $tcp_ports
  do
    exec_cmd "ip6tables -A SERVICES_INPUT -p tcp --dport $port -m state --state NEW -j ACCEPT"
  done

  udp_ports=`nvram get fw_services_udp`
  [ "$udp_ports" = "Failed" ] && udp_ports=""
  for port in $udp_ports
  do
    exec_cmd "ip6tables -A SERVICES_INPUT -p udp --dport $port -m state --state NEW -j ACCEPT"
  done

  #return the rest to INPUT chain
  exec_cmd "ip6tables -A SERVICES_INPUT -j RETURN"
}


#sets up INPUT,FORWARD,OUTPUT chains in filter table
setup_filter_table()
{
  echo "setup_filter_table()" >>$DEBUG_FILE
  exec_cmd "ip6tables -F"

  #accept all during filter table is modified, maybe dangerous!!
  exec_cmd "ip6tables -P INPUT ACCEPT"
  exec_cmd "ip6tables -P OUTPUT ACCEPT"


  get_external_interface "EXT_IF"

#################setup INPUT chain########################
  #allow all from local interfaces
  exec_cmd "ip6tables -A INPUT -i lo -j ACCEPT"
  [ "$EXT_IF" != "br0" ] && exec_cmd "ip6tables -A INPUT -i br0 -j ACCEPT"
  [ "$EXT_IF" != "wl0" ] && exec_cmd "ip6tables -A INPUT -i wl0 -j ACCEPT"

  exec_cmd "ip6tables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT"


  exec_cmd "ip6tables -N SERVICES_INPUT"

  #let the traffic passing the special INPUT chains
  exec_cmd "ip6tables -A INPUT -i $EXT_IF -j SERVICES_INPUT"

  #check if wan_ping is enabled
  wan_ping=`nvram get fw_wan_ping`
  [ "$wan_ping" = "Failed" ] && wan_ping="1"
  [ "$wan_ping" = "1" ] && exec_cmd "ip6tables -A INPUT -i $EXT_IF -p icmpv6 --icmpv6-type echo-request -j ACCEPT"
##########################################################
  

################setup OUTPUT chain########################

  #allow all from local interfaces
  exec_cmd "ip6tables -A OUTPUT -o lo -j ACCEPT"
  [ "$EXT_IF" != "br0" ] && exec_cmd "ip6tables -A OUTPUT -o br0 -j ACCEPT"
  [ "$EXT_IF" != "wl0" ] && exec_cmd "ip6tables -A OUTPUT -o wl0 -j ACCEPT"

  exec_cmd "ip6tables -A OUTPUT -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT"

##########################################################

  #at last set default policy to drop for INPUT and OUTPUT
  exec_cmd "ip6tables -P INPUT DROP"
  exec_cmd "ip6tables -P OUTPUT DROP"
  exec_cmd "ip6tables -P FORWARD ACCEPT"
}



#setting up debugging
dbg=`nvram get fw6_debug`
if [ "$dbg" = "1" ]; then
  DEBUG_FILE=`nvram get fw6_debug_file`
  [ "$DEBUG_FILE" = "Failed" ] && DEBUG_FILE="/var/firewall6.log"
elif [ "$DEBUG" = "1" ]; then 
  [ "$DEBUG_FILE" != "" ] && DEBUG_FILE="/var/firewall6.log"
else
  DEBUG_FILE="/dev/null"
fi

echo >$DEBUG_FILE
echo "calling IPv6 firewall with parameter: $1" >>$DEBUG_FILE

ipv6_start=`nvram get ipv6_start`
[ "$ipv6_start" = "Failed" ] && ipv6_start=0
[ "$ipv6_start" != "1" ] && echo "Ignoring Firewall IPv6...IPv6 disabled" && exit 0


case "$1" in
	start)
		echo "Starting Firewall IPv6"
		setup_kernel_params

		fw6_start=`nvram get fw6_start`
		[ "$fw6_start" = "Failed" ] && fw6_start=1
		#only if firewall should be started setup filter table
		if [ "$fw6_start" = "1" ]; then
		  echo "firewall should be started...now set up filter table" >>$DEBUG_FILE
		  setup_kernel_params_filter
		  setup_filter_table
		  setup_services_input
		else
		  echo "firewall should not start..." >>$DEBUG_FILE
		  exec_cmd "ip6tables -F"
		  exec_cmd "ip6tables -P INPUT ACCEPT"
		  exec_cmd "ip6tables -P FORWARD ACCEPT"
		  exec_cmd "ip6tables -P OUTPUT ACCEPT"
		fi

		#setup max conntrack
		max_ip=`nvram get fw_max_conntrack`
		[ "$max_ip" = "Failed" ] && max_ip=1024
		exec_cmd "echo $max_ip >/proc/sys/net/ipv6/ip6_conntrack_max"
		;;
	stop)
		echo "Stopping Firewall IPv6"
		exec_cmd "ip6tables -F"
		exec_cmd "ip6tables -P INPUT ACCEPT"
		exec_cmd "ip6tables -P FORWARD ACCEPT"
		exec_cmd "ip6tables -P OUTPUT ACCEPT"
		;;
	restart)
		$0 stop
		$0 start
		;;
	services_restart)
		echo "Reinitializing service rules IPv6"
		setup_services_input
		;;

	*)
		echo "Usage: /etc/start_scripts/firewall_ipv6.sh {start|stop|restart|services_restart}"
		exit 1
		;;
esac

exit 0