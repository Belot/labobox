#!/bin/sh
#
# Start/stops firewall

voip_present=0
[ -f "/bin/vodsl" ] && voip_present=1

ppp_start_sec=`nvram get ppp_start_sec`
[ "$ppp_start_sec" = "Failed" ] && ppp_start_sec=0

ppp_dev_sec=""
if [ "$ppp_start_sec" = "1" ]; then
  atm_vpi_sec=`nvram get atm_vpi_sec`
  [ "$atm_vpi_sec" = "Failed" ] && atm_vpi_sec=1
  atm_vci_sec=`nvram get atm_vci_sec`
  [ "$atm_vci_sec" = "Failed" ] && atm_vci_sec=34
  ppp_dev_sec="ppp_"$atm_vpi_sec"_"$atm_vci_sec"_1"
fi

NAT_LOOPBACK_MARK="77"


exec_cmd()
{
  echo "$1" >>$DEBUG_FILE 2>>$DEBUG_FILE && /bin/sh -c "$1" >>$DEBUG_FILE 2>>$DEBUG_FILE
  return 0
}

#usage: get_external_interface <var_name>
#puts name of external interface to var_name
get_external_interface()
{
  echo "get_external_interface()" >>$DEBUG_FILE
  wan_if=`nvram get fw_wan_if`
  [ "$wan_if" = "Failed" ] && wan_if="ppp"
  if [ "$wan_if" = "ppp" ] || [ "$wan_if" = "dsl" ] || [ "$wan_if" = "atm" ]; then
    atm_vpi=`nvram get atm_vpi`
    [ "$atm_vpi" = "Failed" ] && atm_vpi=1
    atm_vci=`nvram get atm_vci`
    [ "$atm_vci" = "Failed" ] && atm_vci=32
    wan_if=ppp_"$atm_vpi"_"$atm_vci"_1
  fi
  echo "get_external_interface() external interface is $wan_if" >>$DEBUG_FILE
  eval $1=\"$wan_if\" 2>/dev/null
}

#setting up kernel params
setup_kernel_params()
{
  echo "setup_kernel_params()" >>$DEBUG_FILE
  exec_cmd "echo 1 > /proc/sys/net/ipv4/ip_forward" #Routing
  exec_cmd "echo 1 > /proc/sys/net/ipv4/ip_dynaddr" # Rewrite new address
}

setup_kernel_params_filter()
{
  echo "setup_kernel_params()" >>$DEBUG_FILE
#exec_cmd "echo 1 > /proc/sys/net/ipv4/tcp_syncookies" # Synflood protecion
  exec_cmd "echo 1 > /proc/sys/net/ipv4/conf/all/log_martians" # Spoof/route/redir
  exec_cmd "echo 0 > /proc/sys/net/ipv4/tcp_timestamps"                # Uptime/GB Ethernet
  exec_cmd "echo 0 > /proc/sys/net/ipv4/conf/all/accept_redirects"     # ICMP redirects off
  exec_cmd "echo 0 > /proc/sys/net/ipv4/conf/all/send_redirects"       # ICMP redirects off
  exec_cmd "echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts" # No bcast response
  exec_cmd "echo 0 > /proc/sys/net/ipv4/conf/all/accept_source_route" # No return path mod
  exec_cmd "echo 1 > /proc/sys/net/ipv4/icmp_ignore_bogus_error_responses" # No bad msgs
  #reducing tcp_timer to reduce DOS attacks
  exec_cmd "echo 30 > /proc/sys/net/ipv4/tcp_fin_timeout"
  exec_cmd "echo 1800 > /proc/sys/net/ipv4/tcp_keepalive_time"

  get_external_interface "EXT_IF"
  #address spoofing protection
  exec_cmd "echo 1 > /proc/sys/net/ipv4/conf/$EXT_IF/rp_filter"
}

#new since 0.3.4
load_module()
{
  load=`nvram get fw_ct_$1`
  [ "$load" = "Failed" ] && load="$2"
  if [ "$load" = "1" ]; then
    exec_cmd "modprobe ip_conntrack_$1"
    exec_cmd "modprobe ip_nat_$1"
  else
    exec_cmd "modprobe -r ip_nat_$1"
    exec_cmd "modprobe -r ip_conntrack_$1"
  fi
}

#new since 0.3.4
setup_ct_modules()
{
  echo "setup_ct_modules()" >>$DEBUG_FILE

  load_module "ftp" 1
  load_module "irc" 0
  load_module "pptp" 0
  load_module "rtsp" 0
  load_module "tftp" 0
  load_module "gre" 1
}

setup_dns_nat()
{
  echo "setup_dns_nat()" >>$DEBUG_FILE
  get_external_interface "EXT_IF"

  exec_cmd "iptables -t nat -N DNS_NAT"
  exec_cmd "iptables -t nat -F DNS_NAT"

  #set up dns forwarding if dns server is not running
  dns_start=`/bin/nvram get dns_start`
  [ "$dns_start" = "Failed" ] && dns_start="1"
  if [ "$dns_start" = "0" ]; then
    dns_list=`cat /var/fyi/sys/dns 2>/dev/null|grep nameserver|awk {'print $2;'}`
    dest_opt=""
    br0_ip=`nvram get lan_ipaddr`
    [ "$br0_ip" = "Failed" ] && br0_ip="192.168.2.1"
    [ "$br0_ip" = "dhcp" ] && br0_ip=`ifconfig br0|grep "inet addr"|sed -n -e 's/^.*addr:\([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\).*Bcast.*/\1/p'`
    set_wl=0
    if [ "`nvram get wl_up`" = "1" ] && [ "`nvram get wl_wet`" = "0" ]; then
      set_wl=1
      wl0_ip=`nvram get wl_ipaddr`
      [ "$wl0_ip" = "Failed" ] && wl0_ip="192.168.10.1"
      [ "$wl0_ip" = "dhcp" ] && wl0_ip=`ifconfig wl0|grep "inet addr"|sed -n -e 's/^.*addr:\([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\).*Bcast.*/\1/p'`
      [ "$wl0_ip" = "" ] && set_wl=0
    fi
    #go through list to load balance through all dns_servers
    for i in $dns_list
    do
       dest_opt=$dest_opt" --to-destination $i"
    done
    [ "$dest_opt" != "" ] && [ "$EXT_IF" != "br0" ] && exec_cmd "iptables -t nat -A DNS_NAT -i br0 -d $br0_ip -p udp --dport 53 -j DNAT$dest_opt"
    [ "$dest_opt" != "" ] && [ "$set_wl" = "1" ] && [ "$EXT_IF" != "wl0" ] && exec_cmd "iptables -t nat -A DNS_NAT -i wl0 -d $wl0_ip -p udp --dport 53 -j DNAT$dest_opt"
  fi

  #return the rest to PREROUTING chain
  exec_cmd "iptables -t nat -A DNS_NAT -j RETURN"
}

setup_portfw()
{
  echo "setup_portfw()" >>$DEBUG_FILE
  get_external_interface "EXT_IF"

  exec_cmd "iptables -t nat -N WAN_PORTFW_NAT"
  exec_cmd "iptables -t nat -F WAN_PORTFW_NAT"
  exec_cmd "iptables -N PORTFW_FORWARD"
  exec_cmd "iptables -F PORTFW_FORWARD"
  exec_cmd "iptables -N PORTFW_INPUT"
  exec_cmd "iptables -F PORTFW_INPUT"


  lan_ipaddr=`nvram get lan_ipaddr`
  [ "$lan_ipaddr" = "Failed" ] && lan_ipaddr="192.168.2.1"
  [ "$lan_ipaddr" = "dhcp" ] && lan_ipaddr=`ifconfig br0|grep "inet addr"|sed -n -e 's/^.*addr:\([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\).*Bcast.*/\1/p'`

  rule_list=`nvram get fw_port_forwarding`
  [ "$rule_list" = "Failed" ] && rule_list=""
  rule_list=`echo $rule_list|sed -e 's/ /%20/g'|sed -e 's/;/ /g'`
  #go through each rule
  for rule in $rule_list
  do
    rule=`echo $rule|sed -e 's/%20/ /g'`
    ena=`echo $rule|sed -e 's/.*#.*#.*#.*#.*#\(.*\)/\1/g'`
    [ "$ena" != "1" ] && continue
    port=`echo $rule|sed -e 's/.*#\(.*\)#.*#.*#.*#.*/\1/g'`
    prot=`echo $rule|sed -e 's/.*#.*#\(.*\)#.*#.*#.*/\1/g'`
    dst_ip=`echo $rule|sed -e 's/.*#.*#.*#\(.*\)#.*#.*/\1/g'`
    dst_port=`echo $rule|sed -e 's/.*#.*#.*#.*#\(.*\)#.*/\1/g'`
    is_range=`echo $port|grep ":"`
    if [ "$is_range" != "" ]; then
      if [ "$prot" = "tcp" ] || [ "$prot" = "both" ]; then 
	exec_cmd "iptables -t nat -A WAN_PORTFW_NAT -p tcp --dport $port -j DNAT --to-destination $dst_ip"
	[ "$dst_ip" = "$lan_ipaddr" ] && exec_cmd "iptables -A PORTFW_INPUT -p tcp --dport $port -m state --state NEW -j ACCEPT" || exec_cmd "iptables -A PORTFW_FORWARD -d $dst_ip -p tcp --dport $port -m state --state NEW -j ACCEPT"
      fi
      if [ "$prot" = "udp" ] || [ "$prot" = "both" ]; then 
	exec_cmd "iptables -t nat -A WAN_PORTFW_NAT -p udp --dport $port -j DNAT --to-destination $dst_ip"
	[ "$dst_ip" = "$lan_ipaddr" ] && exec_cmd "iptables -A PORTFW_INPUT -p udp --dport $port -m state --state NEW -j ACCEPT" || exec_cmd "iptables -A PORTFW_FORWARD -d $dst_ip -p udp --dport $port -m state --state NEW -j ACCEPT"
      fi
    else
      if [ "$prot" = "tcp" ] || [ "$prot" = "both" ]; then 
	exec_cmd "iptables -t nat -A WAN_PORTFW_NAT -p tcp --dport $port -j DNAT --to-destination $dst_ip:$dst_port"
	[ "$dst_ip" = "$lan_ipaddr" ] && exec_cmd "iptables -A PORTFW_INPUT -p tcp --dport $dst_port -m state --state NEW -j ACCEPT" || exec_cmd "iptables -A PORTFW_FORWARD -d $dst_ip -p tcp --dport $dst_port -m state --state NEW -j ACCEPT"
      fi
      if [ "$prot" = "udp" ] || [ "$prot" = "both" ]; then 
	exec_cmd "iptables -t nat -A WAN_PORTFW_NAT -p udp --dport $port -j DNAT --to-destination $dst_ip:$dst_port"
	[ "$dst_ip" = "$lan_ipaddr" ] && exec_cmd "iptables -A PORTFW_INPUT -p udp --dport $dst_port -m state --state NEW -j ACCEPT" || exec_cmd "iptables -A PORTFW_FORWARD -d $dst_ip -p udp --dport $dst_port -m state --state NEW -j ACCEPT"
      fi
    fi
  done
  
  #return the rest to upper chains
  exec_cmd "iptables -t nat -A WAN_PORTFW_NAT -j RETURN"
  exec_cmd "iptables -A PORTFW_FORWARD -j RETURN"
  exec_cmd "iptables -A PORTFW_INPUT -j RETURN"
}


setup_protfw()
{
  echo "setup_protfw_nat()" >>$DEBUG_FILE

  exec_cmd "iptables -t nat -N WAN_PROTFW_NAT"
  exec_cmd "iptables -t nat -F WAN_PROTFW_NAT"
  exec_cmd "iptables -N PROTFW_FORWARD"
  exec_cmd "iptables -F PROTFW_FORWARD"
  exec_cmd "iptables -N PROTFW_INPUT"
  exec_cmd "iptables -F PROTFW_INPUT"

  lan_ipaddr=`nvram get lan_ipaddr`
  [ "$lan_ipaddr" = "Failed" ] && lan_ipaddr="192.168.2.1"
  [ "$lan_ipaddr" = "dhcp" ] && lan_ipaddr=`ifconfig br0|grep "inet addr"|sed -n -e 's/^.*addr:\([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\).*Bcast.*/\1/p'`

  rule_list=`nvram get fw_prot_forwarding`
  [ "$rule_list" = "Failed" ] && rule_list=""
  rule_list=`echo $rule_list|sed -e 's/ /%20/g'|sed -e 's/;/ /g'`
  #go through each rule
  for rule in $rule_list
  do
    rule=`echo $rule|sed -e 's/%20/ /g'`
    ena=`echo $rule|sed -e 's/.*#.*#.*#\(.*\)/\1/g'`
    [ "$ena" != "1" ] && continue
    prot=`echo $rule|sed -e 's/.*#\(.*\)#.*#.*/\1/g'`
    dst_ip=`echo $rule|sed -e 's/.*#.*#\(.*\)#.*/\1/g'`
    exec_cmd "iptables -t nat -A WAN_PROTFW_NAT -i $EXT_IF -p $prot -j DNAT --to-destination $dst_ip"
    [ "$dst_ip" = "$lan_ipaddr" ] && exec_cmd "iptables -A PROTFW_INPUT -p $prot -j ACCEPT" || exec_cmd "iptables -A PROTFW_FORWARD -p $prot -d $dst_ip -j ACCEPT"
  done

  #return the rest to PREROUTING chain
  exec_cmd "iptables -t nat -A WAN_PROTFW_NAT -j RETURN"
  exec_cmd "iptables -A PROTFW_FORWARD -j RETURN"
  exec_cmd "iptables -A PROTFW_INPUT -j RETURN"
}

#new since 0.3.9
setup_dmz()
{
  echo "setup_dmz()" >>$DEBUG_FILE

  exec_cmd "iptables -t nat -N WAN_DMZ_NAT"
  exec_cmd "iptables -t nat -F WAN_DMZ_NAT"
  exec_cmd "iptables -N DMZ_FORWARD"
  exec_cmd "iptables -F DMZ_FORWARD"

  dmz=`nvram get fw_dmz`
  [ "$dmz" = "Failed" ] && dmz=0
  if [ "$dmz" = "1" ]; then
    dmz_ip=`nvram get fw_dmz_ip`
    if [ "$dmz_ip" != "Failed" ]; then
      exec_cmd "iptables -t nat -A WAN_DMZ_NAT -m state --state NEW -j DNAT --to-destination $dmz_ip"
      exec_cmd "iptables -A DMZ_FORWARD -d $dmz_ip -m state --state NEW -j ACCEPT"
    fi
  fi

  #return the rest to PREROUTING chain
  exec_cmd "iptables -t nat -A WAN_DMZ_NAT -j RETURN"
  exec_cmd "iptables -A DMZ_FORWARD -j RETURN"
}

#new since 0.3.9
setup_nat_loopback()
{
  echo "setup_nat_loopback()" >>$DEBUG_FILE
  
  exec_cmd "iptables -t mangle -F NAT_LOOPBACK_MANGLE_PRE"
  exec_cmd "iptables -t mangle -F NAT_LOOPBACK_MANGLE_POST"
  exec_cmd "iptables -t nat -N NAT_LOOPBACK_PRE_NAT"
  exec_cmd "iptables -t nat -N NAT_LOOPBACK_POST_NAT"
  exec_cmd "iptables -t nat -F NAT_LOOPBACK_PRE_NAT"
  exec_cmd "iptables -t nat -F NAT_LOOPBACK_POST_NAT"
  
  nat_loopback=`nvram get fw_nat_loopback`
  [ "$nat_loopback" = "Failed" ] && nat_loopback=0
  if [ "$nat_loopback" = "1" ]; then
    get_external_interface "EXT_IF"
    if [ "`echo $EXT_IF|grep ppp`" != "" ]; then
      wan_ip=`ifconfig $EXT_IF|grep "inet addr"|sed -n -e 's/^.*addr:\([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\).*P-t-P.*/\1/p'`
    else
      wan_ip=`ifconfig $EXT_IF|grep "inet addr"|sed -n -e 's/^.*addr:\([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\).*Bcast.*/\1/p'`
    fi
    lan_if="br0"
    [ "$EXT_IF" = "br0" ] && lan_if="wl0"
    lan_ip=`ifconfig $lan_if|grep "inet addr"|sed -n -e 's/^.*addr:\([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\).*Bcast.*/\1/p'`
    if [ "$wan_ip" != "" -a "$lan_ip" != "" ]; then
      #restore the mark for this connection if it was marked before
      exec_cmd "iptables -t mangle -A NAT_LOOPBACK_MANGLE_PRE -d $wan_ip -j CONNMARK --restore-mark"
      #if the connection is already marked RETURN immediatly and dont traverse the rest of this chain
      exec_cmd "iptables -t mangle -A NAT_LOOPBACK_MANGLE_PRE -d $wan_ip -m mark ! --mark 0 -j RETURN"
      #mark this packet with LOOPBACK_MARK
      exec_cmd "iptables -t mangle -A NAT_LOOPBACK_MANGLE_PRE -d $wan_ip -j MARK --set-mark $NAT_LOOPBACK_MARK"
      #and save the mark to the hole connection
      exec_cmd "iptables -t mangle -A NAT_LOOPBACK_MANGLE_PRE -d $wan_ip -j CONNMARK --save-mark"

      #restore the mark on outgoing packets so the nat-table later will see that this is a loopback connection
      exec_cmd "iptables -t mangle -A NAT_LOOPBACK_MANGLE_POST -j CONNMARK --restore-mark"

      #if the packet is marked as loopback and the port-forwarding haven't NAT'ed it make DNAT to the router's local address
      exec_cmd "iptables -t nat -A NAT_LOOPBACK_PRE_NAT -m mark --mark $NAT_LOOPBACK_MARK -j DNAT --to-destination $lan_ip"
      #SNAT outgoing packets to the routers local address which are marked as loopback
      exec_cmd "iptables -t nat -A NAT_LOOPBACK_POST_NAT -m mark --mark $NAT_LOOPBACK_MARK -j SNAT --to-source $wan_ip"
    fi
  fi
  
  #return the rest to the upper chains
  exec_cmd "iptables -t mangle -A NAT_LOOPBACK_MANGLE_PRE -j RETURN"
  exec_cmd "iptables -t mangle -A NAT_LOOPBACK_MANGLE_POST -j RETURN"
  exec_cmd "iptables -t nat -A NAT_LOOPBACK_PRE_NAT -j RETURN"
  exec_cmd "iptables -t nat -A NAT_LOOPBACK_POST_NAT -j RETURN"
}

#sets up NAT-table including port forwarding rules ( look also at setup_forward_chain() ) and dns forwarding
setup_nat_table()
{
  echo "setup_nat_table()" >>$DEBUG_FILE
  exec_cmd "iptables -t nat -F"
  exec_cmd "iptables -t nat -P PREROUTING ACCEPT"
  exec_cmd "iptables -t nat -P POSTROUTING ACCEPT"
  exec_cmd "iptables -t nat -N WAN_PORTFW_NAT"
  exec_cmd "iptables -t nat -N WAN_PROTFW_NAT"
  exec_cmd "iptables -t nat -N WAN_DMZ_NAT"
  exec_cmd "iptables -t nat -N SIP_NAT"
  exec_cmd "iptables -t nat -N DNS_NAT"
  exec_cmd "iptables -t nat -N NAT_LOOPBACK_PRE_NAT"
  exec_cmd "iptables -t nat -N NAT_LOOPBACK_POST_NAT"
  
  get_external_interface "EXT_IF"

  exec_cmd "iptables -t nat -A PREROUTING -j DNS_NAT"

  #send packets which were marked by the mangle-rules as loopback to the normal PORT/PROT-forwarding chains
  exec_cmd "iptables -t nat -A PREROUTING -m mark --mark $NAT_LOOPBACK_MARK -j WAN_PORTFW_NAT"
  exec_cmd "iptables -t nat -A PREROUTING -m mark --mark $NAT_LOOPBACK_MARK -j WAN_PROTFW_NAT"

  exec_cmd "iptables -t nat -A PREROUTING -i $EXT_IF -j WAN_PORTFW_NAT"
  exec_cmd "iptables -t nat -A PREROUTING -i $EXT_IF -j WAN_PROTFW_NAT"
  exec_cmd "iptables -t nat -A PREROUTING -i $EXT_IF -j WAN_DMZ_NAT"

  #packets which come not from the WAN-interface are maybe candidate loopback packets
  exec_cmd "iptables -t nat -A PREROUTING -i ! $EXT_IF -j NAT_LOOPBACK_PRE_NAT"
  
  exec_cmd "iptables -t nat -A OUTPUT -j SIP_NAT"

  #outgoing packets are maybe candidate loopback packets
  exec_cmd "iptables -t nat -A POSTROUTING -o ! $EXT_IF -j NAT_LOOPBACK_POST_NAT"
  
  #masquerade all outgoing traffic
  exec_cmd "iptables -t nat -A POSTROUTING -o $EXT_IF -j MASQUERADE"
  
  #masquerade traffic outgoing on secondary ppp-connection
  [ "$ppp_dev_sec" != "" ] && exec_cmd "iptables -t nat -A POSTROUTING -o $ppp_dev_sec -j MASQUERADE"
}


#sets up a chain called SERVICES_INPUT and fills it with portbased rules from NVRAM fw_services_tcp and fw_services_udp
#these lists should contain port numbers from local running services like ssh, telnet voip if they should be accessible from the internet
#setup_services_input() is called by setup_filter_table() and from main function in case: services_restart
setup_services_input()
{
  echo "setup_services_input()" >>$DEBUG_FILE
  exec_cmd "iptables -N SERVICES_INPUT"
  exec_cmd "iptables -F SERVICES_INPUT"

  #new since 0.3.6
  ssh_port=`nvram get ssh_port`
  [ "$ssh_port" = "Failed" ] && ssh_port="22"

  ssh_intervall=`nvram get fw_ssh_intervall`
  [ "$ssh_intervall" = "Failed" ] && ssh_intervall="60"
  
  ssh_limit=`nvram get fw_ssh_limit`
  [ "$ssh_limit" = "Failed" ] && ssh_limit="8"

  tcp_ports=`nvram get fw_services_tcp`
  [ "$tcp_ports" = "Failed" ] && tcp_ports="$ssh_port"

  for port in $tcp_ports
  do
    #new since 0.3.6
    if [ "$ssh_port" = "$port" ]; then
      exec_cmd "iptables -A SERVICES_INPUT -p tcp --dport $port -m state --state NEW -m recent --name SSH_SCAN --rsource --update --seconds $ssh_intervall --hitcount $ssh_limit -j LOG --log-level info --log-prefix \"SSH_SCAN blocked:\""
      exec_cmd "iptables -A SERVICES_INPUT -p tcp --dport $port -m state --state NEW -m recent --name SSH_SCAN --rsource --update --seconds $ssh_intervall --hitcount $ssh_limit -j DROP"
      exec_cmd "iptables -A SERVICES_INPUT -p tcp --dport $port -m state --state NEW -m recent --name SSH_SCAN --rsource --set"
    fi
    exec_cmd "iptables -A SERVICES_INPUT -p tcp --dport $port -m state --state NEW -j ACCEPT"
  done

  udp_ports=`nvram get fw_services_udp`
  [ "$udp_ports" = "Failed" ] && udp_ports=""
  for port in $udp_ports
  do
    exec_cmd "iptables -A SERVICES_INPUT -p udp --dport $port -m state --state NEW -j ACCEPT"
  done

  #return the rest to INPUT chain
  exec_cmd "iptables -A SERVICES_INPUT -j RETURN"
}

#new since 0.3.4
setup_web_and_port_rules()
{
  echo "setup_web_and_port_rules()" >>$DEBUG_FILE
  exec_cmd "iptables -N PORT_AND_WEB_FILTER"
  exec_cmd "iptables -F PORT_AND_WEB_FILTER"

  get_external_interface "EXT_IF"
  
  #new since 0.3.6
  web_ports=`nvram get fw_web_ports|sed -e 's/-/:/g'`
  [ "$web_ports" = "Failed" ] && web_ports="80"

  content_prot=`nvram get fw_content_prot`
  [ "$content_prot" = "Failed" ] && content_prot="tcp"

  rule_list=`nvram get fw_web_port_rules`
  [ "$rule_list" = "Failed" ] && rule_list=""
  rule_list=`echo $rule_list|sed -e 's/ /%20/g'|sed -e 's/;/ /g'`
  #go through each rule
  for rule in $rule_list
  do
    prot=""
    rule=`echo $rule|sed -e 's/%20/ /g'`
    ena=`echo $rule|sed -e 's/.*#.*#.*#.*#.*#\(.*\)/\1/g'`
    [ "$ena" != "1" ] && continue

    ip_range=`echo $rule|sed -e 's/.*#\(.*\)#.*#.*#.*#.*/\1/g' #|sed -e 's/,/ || /g'`
    range_check=`echo $ip_range|awk '{split($0,ip,"-"); print ip[2];}'`
    

    cmd="iptables -A PORT_AND_WEB_FILTER -o $EXT_IF"
    
    #new since 0.3.6
    range=0
    if [ "$range_check" = "" ]; then
      ip_match_src=" -s $ip_range"
      ip_match_dst=" -d $ip_range"
      cmd=$cmd"$ip_match_src"
    else
      ip_match_src=" -m iprange --src-range $ip_range"
      ip_match_dst=" -m iprange --dst-range $ip_range"
      range=1
    fi
    cmd_udp=$cmd

    type=`echo $rule|sed -e 's/.*#.*#\(.*\)#.*#.*#.*/\1/g'`
    if [ "$type" = "port" ]; then
      #dst_port filter rule
      prot=`echo $rule|sed -e 's/.*#.*#.*#\(.*\)#.*#.*/\1/g'`
      [ "$prot" = "tcp" ] && cmd=$cmd" -p tcp"
      [ "$prot" = "udp" ] && cmd=$cmd" -p udp"
      [ "$prot" = "both" ] && cmd=$cmd" -p tcp" && cmd_udp=$cmd_udp" -p udp"
      #new since 0.3.6
      [ $range -eq 1 ] && cmd=$cmd"$ip_match_src" && cmd_udp=$cmd_udp"$ip_match_src"

      dst_port=`echo $rule|sed -e 's/.*#.*#.*#.*#\(.*\)#.*/\1/g'|sed -e 's/-/:/g'`
      cmd=$cmd" -m mport --dports $dst_port"
      cmd_udp=$cmd_udp" -m mport --dports $dst_port"
      [ "$prot" = "tcp" ] && cmd=$cmd" -j REJECT --reject-with tcp-reset" 
      [ "$prot" = "udp" ] && cmd=$cmd" -j DROP"
      [ "$prot" = "both" ] && cmd=$cmd" -j REJECT --reject-with tcp-reset" && cmd_udp=$cmd_udp" -j DROP"
    else
      #webstr filter rule
      webstr_type=`echo $rule|sed -e 's/.*#.*#.*#\(.*\)#.*#.*/\1/g'`


      if [ "$webstr_type" = "content" ]; then
	content=`echo $rule|sed -e 's/.*#.*#.*#.*#\(.*\)#.*/\1/g'`
	content=`echo "$content"|sed -e 's/,/ /g'`
	for keyword in $content 
	do
	  if [ $range -eq 1 ]; then
	    cmd_tcp_out="iptables -A PORT_AND_WEB_FILTER -o $EXT_IF  -p tcp$ip_match_src -m mport --dports $web_ports -m string --string \"$keyword\" -j REJECT --reject-with tcp-reset"
	    cmd_udp_out="iptables -A PORT_AND_WEB_FILTER -o $EXT_IF  -p udp$ip_match_src -m mport --dports $web_ports -m string --string \"$keyword\" -j DROP"
	    cmd_tcp_in="iptables -A PORT_AND_WEB_FILTER -i $EXT_IF  -p tcp$ip_match_dst -m mport --sports $web_ports -m string --string \"$keyword\" -j REJECT --reject-with tcp-reset"
	    cmd_udp_in="iptables -A PORT_AND_WEB_FILTER -i $EXT_IF  -p udp$ip_match_dst -m mport --sports $web_ports -m string --string \"$keyword\" -j DROP"
	  else
	    cmd_tcp_out="iptables -A PORT_AND_WEB_FILTER -o $EXT_IF$ip_match_src -p tcp -m mport --dports $web_ports -m string --string \"$keyword\" -j REJECT --reject-with tcp-reset"
	    cmd_udp_out="iptables -A PORT_AND_WEB_FILTER -o $EXT_IF$ip_match_src -p udp -m mport --dports $web_ports -m string --string \"$keyword\" -j DROP"
	    cmd_tcp_in="iptables -A PORT_AND_WEB_FILTER -i $EXT_IF$ip_match_dst -p tcp -m mport --sports $web_ports -m string --string \"$keyword\" -j REJECT --reject-with tcp-reset"
	    cmd_udp_in="iptables -A PORT_AND_WEB_FILTER -i $EXT_IF$ip_match_dst -p udp -m mport --sports $web_ports -m string --string \"$keyword\" -j DROP"
	  fi
	  if [ "$content_prot" = "tcp" -o  "$content_prot" = "both" ]; then
	    exec_cmd "$cmd_tcp_out"
	    exec_cmd "$cmd_tcp_in"
	  fi
	  if [ "$content_prot" = "udp" -o  "$content_prot" = "both" ]; then
	    exec_cmd "$cmd_udp_out"
	    exec_cmd "$cmd_udp_in"
	  fi
	done
	continue
      else
	#new since 0.3.6
	[ $range -eq 1 ] && cmd=$cmd" -p tcp$ip_match_src -m mport --dports $web_ports" || cmd=$cmd" -p tcp -m mport --dports $web_ports"

	content=`echo $rule|sed -e 's/.*#.*#.*#.*#\(.*\)#.*/\1/g'`
	content=`echo "$content"|sed -e 's/,/<\&nbsp;>/g'`
	cmd=$cmd" -m webstr --$webstr_type \"$content\" -j REJECT --reject-with tcp-reset"
      fi
    fi
    exec_cmd "$cmd"
    [ "$prot" = "both" ] && exec_cmd "$cmd_udp"
  done
  exec_cmd "iptables -A PORT_AND_WEB_FILTER -j RETURN"
}

#sets up forward chain
setup_forward_chain()
{
  echo "setup_portfw_forward()" >>$DEBUG_FILE
  #exec_cmd "iptables -N FORWARD"
  exec_cmd "iptables -F FORWARD"
  exec_cmd "iptables -N PORT_AND_WEB_FILTER"
  exec_cmd "iptables -N PORTFW_FORWARD"
  exec_cmd "iptables -N PROTFW_FORWARD"
  exec_cmd "iptables -N DMZ_FORWARD"

  get_external_interface "EXT_IF"

  exec_cmd "iptables -A FORWARD -o $EXT_IF -j PORT_AND_WEB_FILTER"
  exec_cmd "iptables -A FORWARD -i $EXT_IF -j PORT_AND_WEB_FILTER"

  exec_cmd "iptables -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT"
  if [ "$EXT_IF" != "br0" ]; then
    #avoid connection tracking for local connections
    exec_cmd "iptables -A FORWARD -i br0 -o br0 -j ACCEPT"
    #only track connections from lan to wan
    exec_cmd "iptables -A FORWARD -i br0 -o $EXT_IF -m state --state NEW -j ACCEPT "
  fi
  if [ "$EXT_IF" != "wl0" ]; then
    #avoid connection tracking for local connections
    exec_cmd "iptables -A FORWARD -i wl0 -o wl0 -j ACCEPT"
    #only track connections from lan to wan
    exec_cmd "iptables -A FORWARD -i wl0 -o $EXT_IF -m state --state NEW -j ACCEPT"
  fi
  if [ "$EXT_IF" != "br0" ] && [ "$EXT_IF" != "wl0" ]; then
    #avoid connection tracking for lan-to-lan connections
    exec_cmd "iptables -A FORWARD -i br0 -o wl0 -j ACCEPT"
    exec_cmd "iptables -A FORWARD -i wl0 -o br0 -j ACCEPT"
  fi
  
  [ "$ppp_dev_sec" != "" ] && exec_cmd "iptables -A FORWARD -o $ppp_dev_sec -j ACCEPT"
  
  #accept packets marked by NAT_LOOPBACK rules, normally this rule should not be passed
  exec_cmd "iptables -A FORWARD -m mark --mark $NAT_LOOPBACK_MARK -j ACCEPT"
  
  exec_cmd "iptables -A FORWARD -j PORTFW_FORWARD"
  exec_cmd "iptables -A FORWARD -j PROTFW_FORWARD"
  exec_cmd "iptables -A FORWARD -j DMZ_FORWARD"

  #drop the rest with default policy
  exec_cmd "iptables -P FORWARD DROP"
}

#sets up INPUT,FORWARD,OUTPUT chains in filter table
setup_filter_table()
{
  echo "setup_filter_table()" >>$DEBUG_FILE
  exec_cmd "iptables -F"

  #accept all during filter table is modified, maybe dangerous!!
  exec_cmd "iptables -P INPUT ACCEPT"
  exec_cmd "iptables -P OUTPUT ACCEPT"


  get_external_interface "EXT_IF"

#################setup INPUT chain########################
  #allow all from local interfaces
  exec_cmd "iptables -A INPUT -i lo -j ACCEPT"
  [ "$EXT_IF" != "br0" ] && exec_cmd "iptables -A INPUT -i br0 -j ACCEPT"
  [ "$EXT_IF" != "wl0" ] && exec_cmd "iptables -A INPUT -i wl0 -j ACCEPT"
  [ "$ppp_dev_sec" != "" ] && exec_cmd "iptables -A INPUT -i $ppp_dev_sec -j ACCEPT"
  

  exec_cmd "iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT"
  #accept packets marked by NAT_LOOPBACK rules, normally this rule should not be passed
  exec_cmd "iptables -A INPUT -m mark --mark $NAT_LOOPBACK_MARK -j ACCEPT"
  

  exec_cmd "iptables -N SERVICES_INPUT"
  exec_cmd "iptables -N SIP_INPUT"
  exec_cmd "iptables -N PORTFW_INPUT"
  exec_cmd "iptables -N PROTFW_INPUT"

  #let the traffic passing the special INPUT chains
  exec_cmd "iptables -A INPUT -i $EXT_IF -j SERVICES_INPUT"
  exec_cmd "iptables -A INPUT -i $EXT_IF -j SIP_INPUT"
  exec_cmd "iptables -A INPUT -i $EXT_IF -j PORTFW_INPUT"
  exec_cmd "iptables -A INPUT -i $EXT_IF -j PROTFW_INPUT"

  #check if wan_ping is enabled
  wan_ping=`nvram get fw_wan_ping`
  [ "$wan_ping" = "Failed" ] && wan_ping="1"
  [ "$wan_ping" = "1" ] && exec_cmd "iptables -A INPUT -i $EXT_IF -p icmp -j ACCEPT"
##########################################################

  setup_forward_chain

################setup OUTPUT chain########################

  #allow all from local interfaces
  exec_cmd "iptables -A OUTPUT -o lo -j ACCEPT"
  [ "$EXT_IF" != "br0" ] && exec_cmd "iptables -A OUTPUT -o br0 -j ACCEPT"
  [ "$EXT_IF" != "wl0" ] && exec_cmd "iptables -A OUTPUT -o wl0 -j ACCEPT"

  exec_cmd "iptables -A OUTPUT -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT"
##########################################################

  #at last set default policy to drop for INPUT and OUTPUT
  exec_cmd "iptables -P INPUT DROP"
  exec_cmd "iptables -P OUTPUT DROP"
}

setup_sip()
{
  [ $voip_present -eq 0 ] && return
  
  echo "setup_sip()" >>$DEBUG_FILE
  
  exec_cmd "iptables -N SIP_INPUT"
  exec_cmd "iptables -F SIP_INPUT"
  exec_cmd "iptables -t nat -N SIP_NAT"
  exec_cmd "iptables -t nat -F SIP_NAT"

  voip_start=`nvram get voip_start`
  if [ "$voip_start" = "1" ]; then
    ata_regport=`nvram get ata_regport`
    [ "$ata_regport" == "Failed" ] && ata_regport=5060
    exec_cmd "iptables -A SIP_INPUT -p udp --dport $ata_regport -m state --state NEW -j ACCEPT"
    # the original w500v opens the ports 16384..16400 too, sometimes an incomming call uses this ports
    exec_cmd "iptables -A SIP_INPUT -p udp --dport 16384:16400 -m state --state NEW -j ACCEPT"

    #sets up DNAT for SIP-registrar connection if not to port 5060
    if [ "$ata_regport" != "5060" ]; then
      ata_reg=`nvram get ata_reg`
      [ "$ata_reg" = "Failed" ] && ata_reg="0.0.0.0"
      exec_cmd "iptables -t nat -A SIP_NAT -p udp -d $ata_reg --dport 5060 -j DNAT --to-destination $ata_reg:$ata_regport"
    fi
  fi

  exec_cmd "iptables -A SIP_INPUT -j RETURN"
  exec_cmd "iptables -t nat -A SIP_NAT -j RETURN"
}

#experimental
#new since 0.3.8
setup_sip_arcor_fix()
{
  [ $voip_present -eq 0 ] && return
  
  echo "setup_sip_arcor_fix()" >>$DEBUG_FILE
  exec_cmd "iptables -t mangle -N SIP_MANGLE"
  exec_cmd "iptables -t mangle -F SIP_MANGLE"

  if [ `nvram get ata_arcor_fix` = "1" ]; then
    exec_cmd "modprobe ipt_SIP"

    IPT="iptables -t mangle -A SIP_MANGLE -j SIP" 
    USER='User-Agent: DSL-EasyBox/DSL-EasyBox-10.02.506' 
    ATTR_REPLACE='a=silenceSupp:off' 
    ATTR1='a=ptime:20' 
    ATTR2='a=sendrecv' 
    methods_all="INVITE SIP/2.0 ACK BYE CANCEL REGISTER REFER NOTIFY OPTIONS SUBSCRIBE INFO"
    

    #go through each method
    for m in $methods_all
    do 
      exec_cmd "$IPT --sip-method \"$m\" --sip-header --sip-replace 'User-Agent: ' --sip-with \"$USER\""
      exec_cmd "$IPT --sip-method \"$m\" --sip-body --sip-replace \"$ATTR_REPLACE\" --sip-with \"${ATTR1}%n${ATTR2}\""
    done
  fi

  exec_cmd "iptables -t mangle -A SIP_MANGLE -j RETURN"
}


setup_mangle_table()
{
  echo "setup_mangle_table()" >>$DEBUG_FILE
  exec_cmd "iptables -t mangle -F"
  exec_cmd "iptables -t mangle -N SIP_MANGLE"
  exec_cmd "iptables -t mangle -N NAT_LOOPBACK_MANGLE_PRE"
  exec_cmd "iptables -t mangle -N NAT_LOOPBACK_MANGLE_POST"
  
  get_external_interface "EXT_IF"
 
  #send incoming packets which come not from the WAN-interface  to loopback chain
  exec_cmd "iptables -t mangle -A PREROUTING -i ! $EXT_IF -j NAT_LOOPBACK_MANGLE_PRE"
  #send outgoing packets which go not through the WAN-interface  to loopback chain
  exec_cmd "iptables -t mangle -A POSTROUTING -j NAT_LOOPBACK_MANGLE_POST"
  exec_cmd "iptables -t mangle -A OUTPUT -p udp --sport 5060 -j SIP_MANGLE"
}

setup_custom_rules()
{
  echo "setup_custom_rules()" >>$DEBUG_FILE
  [ "`nvram get fw_custom_start`" != "1" ] && return
  /bin/nvram getfile fw_custom_script=/var/fw_custom_script.sh >/dev/null && /bin/sh /var/fw_custom_script.sh $1 || echo "fw_custom_start=1, but no fw_custom_script found" >>$DEBUG_FILE
}

###############################MAIN FUNCTION###########################################

#setting up debugging
dbg=`nvram get fw_debug`
if [ "$dbg" = "1" ]; then
  DEBUG_FILE=`nvram get fw_debug_file`
  [ "$DEBUG_FILE" = "Failed" ] && DEBUG_FILE="/var/firewall.log"
elif [ "$DEBUG" = "1" ]; then 
  [ "$DEBUG_FILE" != "" ] && DEBUG_FILE="/var/firewall.log"
else
  DEBUG_FILE="/dev/null"
fi


echo >$DEBUG_FILE
echo "calling firewall with parameter: $1" >>$DEBUG_FILE


case "$1" in
	start)
		echo "Starting Firewall"
		setup_kernel_params

		fw_start=`nvram get fw_start`
		[ "$fw_start" = "Failed" ] && fw_start=1
		#only if firewall should be started setup filter table
		if [ "$fw_start" = "1" ]; then
		  echo "firewall should be started...now set up filter table" >>$DEBUG_FILE
		  setup_kernel_params_filter
		  setup_filter_table
		  setup_web_and_port_rules
		  setup_services_input
		else
		  echo "firewall should not start...only setting up masquerading and port forwarding" >>$DEBUG_FILE
		  exec_cmd "iptables -F"
		  exec_cmd "iptables -P INPUT ACCEPT"
		  exec_cmd "iptables -P FORWARD ACCEPT"
		  exec_cmd "iptables -P OUTPUT ACCEPT"
		fi
		#new since 0.3.2, set MSS to MTU of outgoing/incoming interface for routed packets
		exec_cmd "iptables -I FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu"
		setup_nat_table
		setup_dns_nat
		setup_sip
		setup_portfw
		setup_protfw
		setup_dmz

		setup_mangle_table
		setup_sip_arcor_fix
		setup_nat_loopback

		#setup max conntrack
		max_ip=`nvram get fw_max_conntrack`
		[ "$max_ip" = "Failed" ] && max_ip=1024
		exec_cmd "echo $max_ip >/proc/sys/net/ipv4/netfilter/ip_conntrack_max"

		#setup tcp timeout
		tcp_timeout=`nvram get fw_tcp_timeout`
		[ "$tcp_timeout" = "Failed" ] && tcp_timeout=4096
		exec_cmd "echo $tcp_timeout >/proc/sys/net/ipv4/netfilter/ip_conntrack_tcp_timeout_established" 

		#setup udp timeout
		udp_timeout=`nvram get fw_udp_timeout`
		[ "$udp_timeout" = "Failed" ] && udp_timeout=120
		exec_cmd "echo $udp_timeout >/proc/sys/net/ipv4/netfilter/ip_conntrack_udp_timeout_stream"

		#new since 0.3.3
		setup_custom_rules $1

		#new since 0.3.4
		setup_ct_modules

		;;
	stop)
		echo "Stopping Firewall"
		exec_cmd "iptables -F"
		exec_cmd "iptables -P INPUT ACCEPT"
		exec_cmd "iptables -P FORWARD ACCEPT"

		#new since 0.3.2, set MSS to MTU of outgoing/incoming interface for routed packets
		exec_cmd "iptables -I FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu"

		exec_cmd "iptables -P OUTPUT ACCEPT"
		exec_cmd "iptables -t nat -F PREROUTING"
		;;
	restart)
		$0 stop
		$0 start
		;;
	services_restart)
		echo "Reinitializing service rules"
		setup_services_input
		;;
	portfw_restart)
		echo "Reinitializing port forwarding rules"
		setup_portfw
		;;
	protfw_restart)
		echo "Reinitializing protocol forwarding rules"
		setup_protfw
		;;
	dns_restart)
		echo "Reinitializing DNS forwarding"
		setup_dns_nat
		;;
	web_and_port_restart)
		echo "Reinitializing Web and Port filter rules"
		setup_web_and_port_rules
		;;
	sip_restart)
		echo "Reinitializing SIP filter rules"
		setup_sip
		setup_sip_arcor_fix
		;;
	sip_mangle_restart)
		echo "Reinitializing SIP mangle rules"
		setup_sip_arcor_fix
		;;
	dmz_restart)
		echo "Reinitializing DMZ"
		setup_dmz
		;;
	nat_loopback_restart)
		echo "Reinitializing NAT-loopback"
		setup_nat_loopback
		;;
		*)
		echo "Usage: /etc/start_scripts/firewall.sh {start|stop|restart|services_restart|portfw_restart|dns_restart|web_and_port_restart|sip_restart|dmz_restart|nat_loopback_restart}"
		exit 1
		;;
esac

exit 0
