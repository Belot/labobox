#!/bin/sh
#
# Start/stops syslogd and klogd
#
#
case "$1" in
	start)
	        echo "Starting syslogd"
		sys_remote=`/bin/nvram get syslog_remote`
		[ "$sys_remote" = "Failed" ] && sys_remote=0

		if [ "$sys_remote" = "1" ]; then
		  sys_remote_ip=`/bin/nvram get syslog_remote_ip`
		  sys_remote_port=`/bin/nvram get syslog_remote_port`
		  [ "$sys_remote_port" = "Failed" ] && sys_remote_port="514"

		  if [ "$sys_remote_ip" != "Failed" ]; then
		    syslogd -C -l 7 -R $sys_remote_ip:$sys_remote_port -L
		  fi
		else
		  syslogd -C -l 7
		fi
		echo "Starting klogd"
		klogd
		;;
	stop)
	        echo "Stopping klogd"
		killall klogd >/dev/null 2>/dev/null
		echo "Stopping syslogd"
		killall syslogd >/dev/null 2>/dev/null
		;;
	restart)
		$0 stop
		usleep 300000
		$0 start
		;;
	*)
		echo "Usage: /etc/start_scripts/syslog.sh {start|stop|restart}"
		exit 1
		;;
esac

exit 0