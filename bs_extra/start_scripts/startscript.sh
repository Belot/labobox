#!/bin/sh

ledtool 1 4
export STARTUP=1
export PATH=$PATH:/opt/etc/start_scripts/:etc/start_scripts/

/bin/nvram set startup=1

mkdir -p /var/run
[ -f "/bin/reset_button" ] && reset_button.sh start
passwd.sh

#new since 0.3.3
prestart_custom.sh start $$

lan.sh start
wlan.sh start
telnet.sh start
ssh.sh start
[ -f "/lib/modules/2.6.8.1/kernel/net/ipv6/ipv6.ko" ] && ipv6.sh start 

#new since 0.3.8
syslog.sh start

#new since 0.3.10, always start crond
crond.sh start

date.sh
stproxy.sh start
dsl.sh start
ledtool 1 5
ledtool 3 4 
dhcp_dns.sh start
disconnect.sh start
ledtool 3 0
firewall.sh start
[ -f "/lib/modules/2.6.8.1/kernel/net/ipv6/ipv6.ko" ] && firewall_ipv6.sh start 
[ -f "/bin/vodsl" ] && ata.sh start &
[ -f "/bin/vodsl" ] && call_mon.sh start

#new since 0.3.3, sometimes eth0 leaves the bridge sporadicly, 
#no reason for that found
[ "`brctl show br0|grep eth0`" = "" ] && brctl addif br0 eth0 

custom.sh start
http.sh start &
ledtool 1 0
/bin/nvram del startup
/bin/nvram commit >/dev/null
export STARTUP=0
