#!/bin/sh
#
# Start/stops telnetd
#
#
case "$1" in
	start)
	        echo "Starting telnetd"
		telnet_start=`/bin/nvram get telnet_start`
                [ "$telnet_start" = "Failed" ] || [ "$telnet_start" = "1" ] && /usr/sbin/telnetd
		;;
	stop)
	        echo "Stopping telnetd"
		killall telnetd >/dev/null 2>/dev/null
		;;
	restart)
		$0 stop
		usleep 300000
		$0 start
		;;
	*)
		echo "Usage: /etc/start_scripts/telnet.sh {start|stop|restart}"
		exit 1
		;;
esac

exit 0
