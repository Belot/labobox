#!/bin/sh

PATH=/bin:/usr/bin:/sbin:/usr/sbin

RESOLV_CONF="/etc/resolv.conf"

mkdir -p /var/fyi/sys
echo "" >/var/fyi/sys/dns

case "$1" in
  bound)
    #update_interface
    [ -n "$broadcast" ] && BROADCAST="broadcast $broadcast"
    [ -n "$subnet" ] && NETMASK="netmask $subnet"
    ifconfig $interface $ip $BROADCAST $NETMASK

    #update_routes
    if [ -n "$router" ]
    then
      echo "deleting routes"
      while route del default gw 0.0.0.0 dev $interface
      do :
      done

      for i in $router
      do
        route add default gw $i dev $interface
      done
    fi

    #Update dns
    echo -n > $RESOLV_CONF
    [ -n "$domain" ] && echo domain $domain >> $RESOLV_CONF
    for i in $dns
    do
      echo adding dns $i
      echo nameserver $i >> $RESOLV_CONF
    done

    #update time since Bs-0.3.3
    /etc/start_scripts/date.sh

    [ -f "/lib/modules/2.6.8.1/kernel/net/ipv6/ipv6.ko" ] && /etc/start_scripts/ipv6.sh update_dyn_6to4 $interface
  ;;

  renew)
    $0 bound
  ;;

  deconfig)
    ifconfig $interface 0.0.0.0
  ;;

  *)
    echo "Usage: $0 {bound|renew|deconfig}"
    exit 1
    ;;
esac

exit 0
