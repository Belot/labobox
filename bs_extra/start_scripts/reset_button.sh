#!/bin/sh
#
# Start/stops reset_button
#
#
STANDARD="/usr/bin/button_handler.sh"


case "$1" in
	start)
	        echo "Starting reset_button"
		rbutton_start=`/bin/nvram get rbutton_start`
                if [ "$rbutton_start" = "Failed" -o "$rbutton_start" = "1" ]; then
		  handler_start=`/bin/nvram get rbutton_handler_start`
		  if [ "$handler_start" != "1" ]; then
		    handler="$STANDARD"
		  else
		    nvram getfile rbutton_handler=/var/button_handler.sh
		    handler="/var/button_handler.sh"
		    chmod +x $handler
		  fi
		  reset_button -s $handler &
		fi


		;;
	stop)
	        echo "Stopping reset_button"
		killall -9 reset_button >/dev/null 2>/dev/null
		;;
	restart)
		$0 stop
		$0 start
		;;
	*)
		echo "Usage: /etc/start_scripts/reset_button.sh {start|stop|restart}"
		exit 1
		;;
esac

exit 0
