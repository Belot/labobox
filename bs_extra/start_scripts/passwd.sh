#!/bin/sh

PASSWD="/var/passwd"
ROOT_DEFAULT_PW="0000"
ROOT_HOME="/"
GROUP="/var/group"

nvram getfile etc_passwd=$PASSWD >/dev/null
if [ $? -ne 0 ]; then
   echo "root:!:0:0:Root:"$ROOT_HOME":/bin/sh" > $PASSWD
   echo "root:${ROOT_DEFAULT_PW}"|chpasswd -m >/dev/null
   nvram setfile etc_passwd=$PASSWD >/dev/null
   must_save=1
fi

nvram getfile etc_group=$GROUP >/dev/null
if [ $? -ne 0 ]; then
   echo "root::0:" > $GROUP
   nvram setfile etc_group=$GROUP >/dev/null
   must_save=1
fi

htpasswd=`nvram get use_htpasswd`
[ "$htpasswd" = "Failed" ] && htpasswd=1
[ "$htpasswd" = "1" ] && grep root /etc/passwd |cut -d : -f 1,2 > /var/.htpasswd


[ "$must_save" = "1" ] && [ "$STARTUP" != "1" ] && nvram commit >/dev/null

