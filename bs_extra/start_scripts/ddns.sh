#!/bin/sh

DDNS_CONFIG="/var/ddns.cfg"
DDNS_CACHE="/var/ddnsd.cache"


get_service_name()
{
  local service_name=""
  if [ "$1" = "0" ]; then
    service_name="dyndns"
    elif [ "$1" = "1" ]; then
    service_name="tzo"
    elif [ "$1" = "2" ]; then
    service_name="dhs"
    elif [ "$1" = "3" ]; then
    service_name="dyns"
    elif [ "$1" = "4" ]; then
    service_name="easydns"
    elif [ "$1" = "5" ]; then
    service_name="gnudip"
    elif [ "$1" = "6" ]; then
    service_name="heipv6tb"
    elif [ "$1" = "7" ]; then
    service_name="justlinux"
    elif [ "$1" = "8" ]; then
    service_name="ods"
    elif [ "$1" = "9" ]; then
    service_name="pgpow"
    elif [ "$1" = "10" ]; then
    service_name="ezip"
    elif [ "$1" = "11" ]; then
    service_name="hn"
    elif [ "$1" = "12" ]; then
    service_name="zoneedit"
    fi
    
    echo -n "$service_name"
}

start()
{
  ddns_service=`nvram get ddns_service`
  [ "$ddns_service" = "Failed" ] && ddns_service=0
  service_name=`get_service_name "$ddns_service"`
  
  if [ "$service_name" = "" ]; then
    echo "ERROR: ez-ipupdate not started...unknown ddns_service-number"
    exit 3
  fi

  options=""
  if [ "$ddns_service" = "5" ]; then
    options="max-interval=2073600"
  elif [ "$ddns_service" = "6" ]; then
    options="max-interval=2073600"
  fi
  
  username=`nvram get ddns_${service_name}_user`
  password=`nvram get ddns_${service_name}_pass`
  domain=`nvram get ddns_${service_name}_domain`

  if [ "$password" = "Failed" -o "$username" = "Failed" ]; then
    echo "ERROR: ez-ipupdate not started...username or password missing"
    exit 1
  fi

  #no domain for heipv6tb?
  if [ "$ddns_service" = "6" ]; then
    domain=""
  else
    if [ "$domain" = "Failed" ]; then
      echo "ERROR: ez-ipupdate not started...domain is missing"
      exit 2
    fi
  fi

  atm_vpi=`nvram get atm_vpi`
  [ "$atm_vpi" = "Failed" ] && atm_vpi=1
  atm_vci=`nvram get atm_vci`
  [ "$atm_vci" = "Failed" ] && atm_vci=32
  iface="ppp_${atm_vpi}_${atm_vci}_1"

  echo "service-type=$service_name" > $DDNS_CONFIG
  echo "user="$username":"$password >> $DDNS_CONFIG
  if [ "$domain" != "" ]; then
    echo "host=$domain" >> $DDNS_CONFIG
  fi
  echo "interface="$iface >> $DDNS_CONFIG
  if [ "$options" != "" ]; then
    echo -e "$options" >> $DDNS_CONFIG
  fi

  #common options
  echo "cache-file=$DDNS_CACHE" >> $DDNS_CONFIG
  echo "daemon" >> $DDNS_CONFIG

  ez-ipupdate -c $DDNS_CONFIG
}

stop()
{
  killall -QUIT ez-ipupdate >/dev/null 2>/dev/null
  killall -9 ez-ipupdate >/dev/null 2>/dev/null
  rm -f $DDNS_CONFIG
  #rm -f $DDNS_CACHE
}


case "$1" in
	start)
	        echo "Starting DDNS"
		[ -f $DDNS_CONFIG ] && echo "DDNS already running...exit" && exit 0
                ddns_start=`nvram get ddns_start`
                [ "$ddns_start" = "Failed" ] && ddns_start=0
		[ "$ddns_start" = "1" ] && start || echo "DDNS disabled"
		;;
	stop)
	        echo "Stopping DDNS"
		stop
		;;
	restart)
		$0 stop
		$0 start
		;;
	*)
		echo "Usage: /etc/start_scripts/ddns.sh {start|stop|restart}"
		exit 1
		;;
esac

exit 0
