#!/bin/sh
#
# Start/stops disconnect
#

CRONDIR="/var/spool/cron/crontabs"
CRONTAB=$CRONDIR"/root"

must_save=0

case "$1" in
	start)
	        echo "Setting disconnect"
		disconnect_start=`/bin/nvram get disconnect_start`
                [ "$disconnect_start" = "Failed" ] || [ "$disconnect_start" != "1" ] && exit 0
                
                disconnect_reboot=`/bin/nvram get disconnect_reboot`
                [ "$disconnect_reboot" = "Failed" ] && disconnect_reboot=0
		
                hour=`/bin/nvram get disconnect_hour`
		[ $hour -lt 0 ] || [ $hour -gt 23 ] || [ "$hour" = "Failed" ] && hour=4
                
                minute=`/bin/nvram get disconnect_minute`
		[ $minute -lt 0 ] || [ $minute -gt 59 ] || [ "$minute" = "Failed" ] && minute=0

                delay=`/bin/nvram get disconnect_delay`
                [ $delay -lt 1 ] || [ $delay -gt 3600 ] || [ "$delay" = "Failed" ] && delay=10

                mkdir -p $CRONDIR
                touch $CRONTAB
                #cronjob einfuegen
                if [ "$disconnect_reboot" = "1" ]; then
                  echo "$minute $hour * * * /bin/nvram reboot" >> $CRONTAB
                else
                  echo "$minute $hour * * * /etc/start_scripts/dsl.sh ppp_reconnect $delay" >> $CRONTAB
                fi
		;;
	stop)
	        echo "Stopping disconnect"
	        #remove our lines from crontab
	        cat $CRONTAB|grep -v 'nvram reboot'|grep -v 'dsl.sh ppp_reconnect' >/tmp/root
	        cp /tmp/root $CRONTAB
		;;
	restart)
		$0 stop
		$0 start
		;;
	*)
		echo "Usage: /etc/start_scripts/disconnect.sh {start|stop|restart}"
		exit 1
		;;
esac

exit 0
