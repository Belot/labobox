#!/bin/sh

default_server="europe.pool.ntp.org"
default_zone="UTC+1"
TZ="/var/TZ"

start=`nvram get ntp_start`
[ "$start" = "Failed" ] && start=0

if [ "$STARTUP" = "1" ]; then
  timezone=`nvram get ntp_timezone`
  [ "$timezone" = "Failed" ] && timezone=$default_zone
  echo $timezone > $TZ
fi

if [ "$start" = "1" ]; then
  ntpserver=`nvram get ntp_server`
  [ "$ntpserver" = "Failed" ] && ntpserver=$default_server

  ntpclient -h $ntpserver -s >/dev/null 2>/dev/null &
fi




