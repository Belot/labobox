#!/bin/sh
#
# Start/stops lan settings
#
#

setup_lan()
{
    ifconfig br0 down 2>/dev/null
    ifconfig eth0 down 2>/dev/null
    ifconfig eth1 down 2>/dev/null
    [ "`nvram get lan_mac_cloning`" = "1" ] && ifconfig eth0 hw ether `nvram get lan_mac` 2>/dev/null
    
    brctl addbr br0 2>/dev/null
    brctl addif br0 eth0 2>/dev/null
    brctl addif br0 eth1 2>/dev/null
    
    stp=`/bin/nvram get lan_stp`
    [ "$stp" = "Failed" ] && stp="0"
    brctl stp br0 $stp 2>/dev/null
    ifconfig eth0 up
    ifconfig eth1 up
    ip=`nvram get lan_ipaddr`
    [ "$ip" = "Failed" ] && ip="192.168.2.1"
    if [ "$ip" = "dhcp" ]; then
      kill -9 `cat /var/udhcp_lan.pid 2>/dev/null` >/dev/null 2>/dev/null
      sleep 1
      udhcpc -i br0 -b -p /var/run/udhcp_lan.pid -s /etc/start_scripts/udhcpc_set.sh
    else
      mask=`nvram get lan_ipmask`
      [ "$mask" = "Failed" ] && mask="255.255.255.0"
      ifconfig br0 $ip netmask $mask up
    fi
    
    hostname=`nvram get lan_hostname`
    [ "$hostname" = "Failed" ] && hostname="BS"
    echo "$hostname" >/proc/sys/kernel/hostname
    
    if [ "$ip" != "dhcp" ]; then
      gateway=`/bin/nvram get lan_gw`
      [ "$gateway" != "Failed" ] && [ "$gateway" != "" ] && ret=`route add default gw "$gateway" metric 10 br0 2>&1` && [ "$ret" != "" ] && ( echo $ret|grep "File exists" >/dev/null && ret=0 || ( errmsg="$errmsg Network is unreachable!"'\n' && ret=1 ) ) || ret=0
      mkdir -p /var/fyi/sys/
      >/var/fyi/sys/dns
      dns=`/bin/nvram get lan_dns1`
      [ "$dns" != "Failed" ] && [ "$dns" != "" ] && echo "nameserver $dns" >>/var/fyi/sys/dns
      dns=`/bin/nvram get lan_dns2`
      [ "$dns" != "Failed" ] && [ "$dns" != "" ] && echo "nameserver $dns" >>/var/fyi/sys/dns
      dns=`/bin/nvram get lan_dns3`
      [ "$dns" != "Failed" ] && [ "$dns" != "" ] && echo "nameserver $dns" >>/var/fyi/sys/dns
      [ "$ret" = "1" ] && echo "$errmsg"
    fi
    return 0
}


case "$1" in
	start)
	        echo "Starting LAN-Setting"
                errmsg=""
	        setup_lan	
                exit $ret
		;;
	stop)
	        echo "Stopping LAN-Setting senceless"
		;;
	restart)
		$0 start
		;;
	*)
		echo "Usage: /etc/start_scripts/lan.sh {start|stop|restart}"
		exit 1
		;;
esac

exit 0
