/*
 * Copyright (C) 2008 Michael Finsterbusch (fibu@users.sourceforge.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/socket.h>
#include <net/if.h>

#include <board_api.h>

#define PPP_STATUS_FORMAT_STR "/proc/var/fyi/wan/%s/daemonstatus"
#define DEFAULT_GW_FILE "/var/fyi/sys/gateway"

#define WAIT_DURING_AUTH  100000
#define WAIT_DOWN	  1000000
#define WAIT_UP		  5000000
#define WAIT_FAILED	  3000000

typedef enum
{
	STATE_PADO=0,   /* waiting for PADO */
	STATE_PADS,     /* got PADO, waiting for PADS */
	STATE_CONFIRMED,/* got PADS, session ID confirmed */
	STATE_DOWN,     /* totally down */
	STATE_UP,       /* totally up */
	SERVICE_AVAILABLE,    /* ppp service is available on the remote */
	AUTH_FAILED=7
} PPP_STATE;


int check_iface(char* iface);
int watch(char* file,int timeout,char* iface);
void set_default_dev(char* iface,int* first);
void update_gw();

char* s_command;

int myboardIoctl(int board_ioctl, BOARD_IOCTL_ACTION action, char *string, int strLen, int offset, char *buf)
{
    BOARD_IOCTL_PARMS IoctlParms;
    int boardFd = 0;

    boardFd = open("/dev/brcmboard", O_RDWR);
    if ( boardFd != -1 ) {
        IoctlParms.string = string;
        IoctlParms.strLen = strLen;
        IoctlParms.offset = offset;
        IoctlParms.action = action;
        IoctlParms.buf    = buf;
        ioctl(boardFd, board_ioctl, &IoctlParms);
        close(boardFd);
        boardFd = IoctlParms.result;
    }

    return boardFd;
}

void Led(BOARD_LED_STATE ledState)
{
    myboardIoctl(BOARD_IOCTL_LED_CTRL, 0, "",4, (int)ledState, "");
}



void print_help(char* name)
{
  printf("PPP watch 0.1.1 - look for PPP client daemon and update default gw after connect\n"
	"by Michael Finsterbusch <fibu@users.sourceforge.net>\n\n"
	"usage: %s [-h] [-c <connection>] [-s <command>]\n"
	"options:\n"
	"  -h                 this help screen\n"
	"  -c <connection>    connection string with 'ppp_<vpi>_<vci>_1'\n"
	"  -t                 pppd with idle timeout enabled\n"
        "  -s <command>	      A command to start if a new connection is established\n"
	"                     <command> is just one parameter, maybe it has to be\n"
	"                     quoted. The PPP watch process is jammed until <command>\n"
	"                     terminates.\n"
	"",
	name);
}

int main(int argc,char* argv[])
{
  char** argvh=argv;
  int opt,noopt=1;
  char status_file[100];
  int timeout=0;
  char* iface;
  s_command=NULL;
  iface=NULL;

  memset(status_file,0,100);
  while((opt=getopt(argc,argvh,"hc:s:t"))!=EOF)
  {
   noopt=0;
   switch(opt)
   {
     case 'c': 
         iface=optarg;
	 snprintf(status_file,100,PPP_STATUS_FORMAT_STR,optarg);
         break;
     case 's': 
	 s_command=optarg;
         break;
     case 't':
	 timeout=1;
	 break;
     case 'h':
     default:
	 print_help(argv[0]);
	 return 0;
	 break;
   }
  }
  if(noopt || strlen(status_file)<=strlen(PPP_STATUS_FORMAT_STR))
  {
    print_help(argv[0]);
    return 0;
  }

  watch(status_file,timeout,iface);

  return 0;
}


int watch(char* file,int timeout,char* iface)
{
  long wait=0;
  time_t mtime;
  struct stat filestat;
  char buf[10];
  int ret;
  int state;
  PPP_STATE ppp_state=-5;
  FILE *fd;
  int dev=0;
  int set_led=0;
  int UP=0;
  int first=1;

  memset(&mtime,0,sizeof(time_t));
  while(1)
  {
    usleep(wait);
    set_led++;
    if(set_led>25)
    {
      memset(&mtime,0,sizeof(time_t));
      ppp_state=-5;
    }

    if(stat(file,&filestat)==-1)
    {
      wait=WAIT_FAILED;
      continue;
    }
    if(filestat.st_mtime==mtime)
      continue;
    mtime=filestat.st_mtime;

    fd=fopen(file,"r");
    if(!fd)
    {
      wait=WAIT_FAILED;
      continue;
    }
    //fseek(fd,0,SEEK_SET);
    //clearerr(fd);
    ret=fread(buf,1,10,fd);
    ret=ferror(fd);
    fclose(fd);
    if(ret)
      continue;
    buf[1]=0;
    if(isdigit(buf[0]))
      state=atoi(buf);
    else 
      state=STATE_DOWN;

    if(ppp_state==state)
      continue;
    switch(state)
    {
	case STATE_PADO:
	case STATE_PADS:
	case STATE_CONFIRMED:	
	case SERVICE_AVAILABLE:
		UP=0;
		wait=WAIT_DURING_AUTH;
		Led(kLedStateFastBlinkContinues);
		break;
        case STATE_DOWN:
		wait=WAIT_DOWN;
		UP=0;
		Led(kLedStateOff);
                if(timeout)
		{
		  if(dev==0 && check_iface(iface))
                  {
                    set_default_dev(iface,&first);
		    dev=1;
                  }
                  else
                  {
                    memset(&mtime,0,sizeof(time_t));
                    ppp_state=-2;
		    continue;
                  }
                }
		break;
	case STATE_UP:
		wait=WAIT_UP;
		Led(kLedStateOn);
		if(!UP)
                {
		  update_gw();
		  UP=1;
                }
		dev=0;
		break;
	case AUTH_FAILED:
		wait=WAIT_FAILED;
		Led(kLedStateOff);
		break;
	default:
		UP=0;
		ppp_state=STATE_DOWN;
		Led(kLedStateOff);
		continue;
    }
    ppp_state=state;
    set_led=0;
  }
}

void set_default_dev(char* iface,int* first)
{
  char cmd[100];
  snprintf(cmd,100,"route add default dev %s",iface);
  system(cmd);
  if(*first)
  {
    *first=0;
    snprintf(cmd,100,"while [ ! -f /etc/resolv.conf ]; do ping -c 1 1.1.1.1 >/dev/null 2>/dev/null; done &");
    system(cmd);
  }
}

void update_gw()
{
  char cmd[100];
  snprintf(cmd,100,"route add default gw `cat %s`",DEFAULT_GW_FILE);
  system(cmd);
  if(s_command)
    system(s_command);
}

int check_iface(char *iface)
{
 int sock;
 int ret;
 struct ifreq intf;

 if((sock=socket(AF_INET, SOCK_DGRAM, 0)) < 0)
 {
   return 0;
 }

 strcpy (intf.ifr_name,iface);
 if(ioctl(sock,SIOCGIFFLAGS,&intf)==-1)
   ret=0;
 else
 {
   if((intf.ifr_flags & IFF_UP)!=0)
     ret = 1;
   else
     ret = 0;
 }
 close(sock);
 return ret;
}
