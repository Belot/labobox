#!/bin/sh

action=0
. ./login.cgi
. ./functions.sh
HOME="admin_index.cgi"

TMP1=/var/tmp1.gif
TMP2=/var/tmp2.gif
custom_color_theme_file="/var/custom_color_theme_file"

reset_to_default()
{
  cd /opt/mini_fo/webs 2>/dev/null
  if [ "$?" = "0" ]; then
    for i in pic_bg1.gif  pic_c_menu.gif  pic_c_verzoeg.gif  pic_m_head.gif \
       pic_m_marker.gif pic_bg2.gif  pic_c_stat.gif  pic_i_hinweis.gif \
       pic_m_linie.gif style.css
    do
      rm $i 2>/dev/null
    done
    nvram reboot
  fi
}

# $1: gif-file
# $2: color index
color_from_index()
{
  giftrans -l $1 2>&1|grep "Color ${2}:"| awk '{sub(/#/,"",$9); print $9}'
}

# $1: gif-file
# $2: list of color index and new color (index1:color1 index2:color2; e.g. "1:fe2153")
change_gif_colors()
{
  filename=$1
  color_list=$2
  cp $filename $TMP2

  for i in $color_list
  do 
    index=`echo $i|sed -e 's/:.*$//'`; 
    new_color=`echo $i|sed -e 's/^.*://'`; 

    old_color=`color_from_index $filename $index`
    cp $TMP2 $TMP1
    giftrans -g "#${old_color}=#${new_color}" $TMP1 > $TMP2
  done

  cp $TMP2 $filename
}


# $1: main_theme_color
# $2: logo_text_color
# $3: c_menu_color
# $4: c_stat_color
update_images()
{
  main_theme=$1
  logo_text=$2
  c_menu=$3
  c_stat=$4

  change_gif_colors /webs/pic_bg1.gif "0:${main_theme} 1:${logo_text}"
  change_gif_colors /webs/pic_bg2.gif "0:${main_theme} 1:${logo_text}"
  change_gif_colors /webs/pic_c_menu.gif "0:${c_menu}"
  #change_gif_colors /webs/pic_c_stat.gif "0:${c_stat}"
  change_gif_colors /webs/pic_c_verzoeg.gif "0:${main_theme}"
  change_gif_colors /webs/pic_i_hinweis.gif "0:${main_theme}"
  change_gif_colors /webs/pic_m_head.gif "0:${main_theme} 1:${c_stat}"
  change_gif_colors /webs/pic_m_marker.gif "0:${main_theme}"
}

update_css()
{
  main_theme=$1
  body_bg_color=$2
  text_color=$3
  link_hover_color=$4
  h2_color=$5
  h2_bg_color=$6
  i_frame_color=$7
  i_frame_bg_color=$8
  c_pfad_color=$9
  but_color=$10
  but_border_color=$11
  c_border_bg_color=$12

  cat << EOF_CSS > /webs/style.css
a:link {font-size:12px; font-family: Arial, sans-serif; text-decoration: none; color: #${text_color};}
a:visited {font-size:12px; font-family: Arial, sans-serif;  text-decoration: none; color: #${text_color};}
a:active {font-size:12px; font-family: Arial, sans-serif;  text-decoration: none; color: #${text_color};}
a:hover {font-size:12px; font-family: Arial, sans-serif;  text-decoration: none; color: #${link_hover_color};}

input {font-family:Courier New; font-size:12px;}
input.stylepwd {font-family:Courier New; font-size:12px;}
input.stylebut {font-family:Arial; font-size:12px; padding-left: 5px; text-align: left;}
select {font-family:Arial, sans-serif; font-size:12px;}
td {font-family:Arial, sans-serif; font-size:12px;}
li {font-family:Arial, sans-serif; font-size:12px;}

p {font-family:Arial, sans-serif; font-size:12px;}
h2 {font-family:Arial, sans-serif; font-size:12px; color: #${h2_color}; background-color:#${h2_bg_color}; padding:2px;}

body {
  font-family:Arial, sans-serif; font-size:12px;
  background-color: #${body_bg_color}; margin-left: 0px; margin-top: 0px; margin-bottom: 0px;
  color: #${text_color};
}

#c_banner {
	 background-color: #${main_theme}; 
	 POSITION: absolute; LEFT: 0px; WIDTH: 100%; TOP: 0px; HEIGHT: 100%;
}

#c_Frame {
	 OVERFLOW: auto; 
}
#t_Frame {
	POSITION: relative; LEFT: 0px; margin-right:190px; HEIGHT: 30px; /*OVERFLOW: hidden;*/
	background-color: #${body_bg_color}; font-weight: bold;
}
#i_Frame {
	position: fixed; right:0px; top: 0px; WIDTH: 190px; HEIGHT: 100%; OVERFLOW: hidden;
	color: #${i_frame_color}; background-color: #${i_frame_bg_color}; Z-INDEX: 1;
}

#c_bild {
	POSITION: absolute; LEFT: 15px;
}

#c_verzoeg1 {
	background-repeat:no-repeat; 
	POSITION: absolute; left: 15px; top: 37px; width: 402px; height: 75;
	border-color: #${main_theme}; border-width: 1px; border-style: solid; 
	background-color: #${c_border_bg_color}; background-image: url(pic_c_verzoeg.gif); VISIBILITY: hidden;
}

#c_verzoeg1_visible {
background-repeat:no-repeat;
POSITION: absolute; left: 15px; top: 37px; width: 402px; height: 75;
border-color: #${main_theme}; border-width: 1px; border-style: solid; 
	background-color: #${c_border_bg_color}; background-image: url(pic_c_verzoeg.gif); VISIBILITY: visible;
}

#c_verzoeg2 {
	POSITION: absolute; left: 13px; top: 112px; width: 406px; height: 371;
	background-color: #${body_bg_color}; VISIBILITY: hidden;
}
#c_verzoeg2_visible {
        POSITION: absolute; left: 13px; top: 112px; width: 406px; height: 371;
        background-color: #${body_bg_color}; VISIBILITY: visible;
}

#c_border {
	POSITION: relative; margin-left: 15px; margin-top: 10px; margin-right: 200px; overflow: auto;
	border-color: #${main_theme}; border-width: 1px; border-style: solid; background-color: #${c_border_bg_color};
}

#c_pfad {
	height: 25px; line-height: 25px; padding-left: 10px;
	border-bottom-color: #${c_pfad_color}; border-bottom-width: 1px; border-bottom-style: solid;
}

#c_titel {
	height: 25px; line-height: 25px; padding-left: 10px; font-weight: bold;
}

#c_link {
	height: 25px; line-height: 25px; padding-left: 6px; background-image: url(pic_c_menu.gif);
	background-repeat: no-repeat;
}

#c_linklast {
	height: 25px; line-height: 25px; padding-left: 6px;
	background-image: url(pic_c_menu.gif); background-repeat: no-repeat;
}

#c_std {
	height: 25px; line-height: 25px; padding-left: 10px;
}

#c_last {
	height: 25px; line-height: 25px; padding-left: 10px;
}

#c_text {
	line-height: 15px; padding-left: 10px;
}

#c_leer {
	height: 25px; line-height: 25px; border-bottom-color: #${c_pfad_color}; border-bottom-width: 1px; border-bottom-style: solid;
}

#c_foot {
	height: 25px; line-height: 25px;
}

#t_but1 {
	POSITION: absolute; top: 4px; left: 15px; width: 86px; text-align: center;
	border-color: #${main_theme}; border-width: 1px; border-style: solid;
	background-color: #${body_bg_color}; cursor:pointer;
}
#t_but1:hover {
	POSITION: absolute; top: 4px; left: 15px; width: 86px; text-align: center;
	border-color: #${but_border_color}; border-width: 1px; border-style: solid;
	color: #${but_color}; background-color: #${body_bg_color}; cursor:pointer;
}
#t_but1_hidden {
        POSITION: absolute; top: 4px; left: 15px; width: 86px; text-align: center;
        border-color: #${main_theme}; border-width: 1px; border-style: solid; visibility: hidden;
}

#t_but2 {
	POSITION: absolute; top: 4px; left: 120px; width: 86px; text-align: center;
	border-color: #${main_theme}; border-width: 1px; border-style: solid; background-color: #${body_bg_color}; cursor:pointer;
}
#t_but2:hover {
	POSITION: absolute; top: 4px; left: 120px; width: 86px; text-align: center;
	border-color: #${but_border_color}; border-width: 1px; border-style: solid; color: #${but_color}; background-color: #${body_bg_color}; cursor:pointer;
}
#t_but2_hidden {
        POSITION: absolute; top: 4px; left: 120px; width: 86px; text-align: center;
        border-color: #${main_theme}; border-width: 1px; border-style: solid; visibility: hidden;
}

#t_but3 {
	POSITION: absolute; top: 4px; left: 226px; width: 86px; text-align: center;
	border-color: #${main_theme}; border-width: 1px; border-style: solid; background-color: #${body_bg_color}; cursor:pointer;
}
#t_but3:hover {
	POSITION: absolute; top: 4px; left: 226px; width: 86px; text-align: center;
	border-color: #${but_border_color}; border-width: 1px; border-style: solid; color: #${but_color}; background-color: #${body_bg_color}; cursor:pointer;
}
#t_but4 {
	POSITION: absolute; top: 4px; left: 331px; width: 86px; text-align: center; 
	border-color: #${main_theme}; border-width: 1px; border-style: solid; background-color: #${body_bg_color}; cursor:pointer;
}
#t_but4:hover {
	POSITION: absolute; top: 4px; left: 331px; width: 86px; text-align: center; 
	border-color: #${but_border_color}; border-width: 1px; border-style: solid; color: #${but_color}; background-color: #${body_bg_color}; cursor:pointer;
}
#t_but3_hidden {
        POSITION: absolute; top: 4px; left: 226px; width: 86px; text-align: center; 
        border-color: #${main_theme}; border-width: 1px; border-style: solid; visibility: hidden;
}
#t_but4_hidden {
        POSITION: absolute; top: 4px; left: 331px; width: 86px; text-align: center;
        border-color: #${main_theme}; border-width: 1px; border-style: solid; visibility: hidden;
}

#i_content {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px;
}
#i_content1 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content2 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content3 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content4 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content5 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content6 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content7 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content8 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content9 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content10 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content11 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content12 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content13 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content14 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content15 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content16 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content17 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content18 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content19 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content20 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content21 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content22 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}
#i_content23 {
	Z-INDEX: 2; POSITION: absolute; LEFT: 10px; WIDTH: 180px; TOP: 10px; VISIBILITY: hidden;
}

#box_left {
	float:left; margin-right:1px;
} 
#box_right {
	float:left; margin-right:15px; margin-left:1px;
}
#index_box {
	margin-left: 15px; margin-top: 10px; overflow: auto;
	border-color: #${main_theme}; border-width: 1px; border-style: dashed; 
	background-color: #${c_border_bg_color}; padding: 5px; 	
}
#index_box2 {
	margin-left: 15px; margin-top: 10px; margin-bottom: 10px; overflow: auto;
	border-color: #${main_theme}; border-width: 1px; border-style: dashed; 
	background-color: #${c_border_bg_color}; padding: 5px; clear:both; float:left;
}

#m_Frame {
	POSITION: absolute; LEFT: 0px; WIDTH: 170px; TOP: 0px; HEIGHT: 100%; OVERFLOW: auto; background-color: #${menu_bg_color}; background-repeat: no-repeat;
}
#m_bild {
	height: 30px; padding-left: 20px;
}
#m_otitel {
	height: 21px; line-height: 21px; padding-left: 10px; color: #${menu_head_color}; 
	font-family: Arial, sans-serif; font-size:12px; font-weight: bold;
}
#m_leer {
	height: 10px; line-height: 15px;
}

#m_but a:hover{
	height: 16px; line-height: 16px; padding-left: 10px; color: #${menu_text_sel_color}; background-image: url(pic_m_marker.gif); background-repeat: no-repeat;
	font-family: Arial, sans-serif; font-size:12px; font-weight: bold;
	 text-decoration:none;
}

#m_but a{
	height: 16px; line-height: 16px; padding-left: 10px; color: #${menu_text_color}; 
	font-family: Arial, sans-serif; font-size:12px; font-weight: bold;
	 text-decoration:none;
}

/* SVG BAR */
rect.svgbar_bg { fill: #${svgbar_bg}; }
rect.svgbar_color { fill: #${svgbar_color}; }
rect.svgbar_border { stroke: #${svgbar_border}; }
text.svgbar_text { color: #${svgbar_text}; font-family:Verdana,sans-serif; font-size:11px; }

EOF_CSS
}

# get a color value from a css entry
# $1=entry (e.g. "body", "c_std")
# $2=attr (e.g. "color", "background-color")
get_css_entry()
{
  entry=$1
  attr=$2
  line=`cat /webs/style.css |tr -d "\r\n"|sed -e "s/.*\($entry[^}]*\).*/\1/"`
  echo $line|sed -e "s/;/\n/g"|grep " ${attr}:"|sed -e "s/.*${attr}:\W*#\([^;]*\).*/\1/g"
}


if [ "$action" = "3" ]; then
  echo 123 > /var/test
  update_images $main_theme_color $logo_text_color $c_menu_color $c_stat_color
  update_css $main_theme_color $body_bg_color $text_color $link_hover_color \
             $h2_color $h2_bg_color $i_frame_color $i_frame_bg_color $c_pfad_color \
	     $but_color $but_border_color $c_border_bg_color
fi

if [ "$action" = "4" ]; then
  reset_to_default
fi

if [ "$reset" = "1" ]; then
  reset_to_default
fi


if [ "$action" = "2" ]; then
cat << EOF_JS > $custom_color_theme_file
  var custom_theme=new Array('$main_theme_color','$logo_text_color','$c_menu_color','$body_bg_color',
			  '$text_color','$link_hover_color','$c_pfad_color','$but_color',
			  '$but_border_color','$h2_color','$h2_bg_color','$i_frame_color',
			  '$i_frame_bg_color','$c_stat_color','$menu_text_color','$menu_text_sel_color',
			  '$menu_bg_color','$menu_head_color','$svgbar_color','$svgbar_bg',
			  '$svgbar_text','$svgbar_border','$c_border_bg_color');
EOF_JS
nvram setfile custom_color_theme_file=$custom_color_theme_file >/dev/null 2>/dev/null && nvram commit
fi

main_theme_color=`color_from_index /webs/pic_bg1.gif 0`
logo_text_color=`color_from_index /webs/pic_bg1.gif 1`
c_menu_color=`color_from_index /webs/pic_c_menu.gif 0`
c_stat_color=`color_from_index /webs/pic_m_head.gif 1`

body_bg_color=`get_css_entry body background-color`
text_color=`get_css_entry a:link color`
link_hover_color=`get_css_entry "a:hover {font-size" color`
h2_color=`get_css_entry h2 color`
h2_bg_color=`get_css_entry h2 background-color`
i_frame_color=`get_css_entry i_Frame color`
[ "$i_frame_color" = "" ] && i_frame_color="000000"
i_frame_bg_color=`get_css_entry i_Frame background-color`
c_pfad_color=`get_css_entry c_pfad border-bottom-color`
but_color=`get_css_entry t_but1:hover color`
but_border_color=`get_css_entry t_but1:hover border-color`
menu_text_color=`get_css_entry "m_but a" color`
menu_text_sel_color=`get_css_entry "m_but a:hover" color`
menu_bg_color=`get_css_entry m_Frame background-color`
menu_head_color=`get_css_entry m_otitel color`
svgbar_color=`get_css_entry rect.svgbar_color fill`
svgbar_bg=`get_css_entry rect.svgbar_bg fill`
svgbar_text=`get_css_entry text.svgbar_text color`
svgbar_border=`get_css_entry rect.svgbar_border stroke`
c_border_bg_color=`get_css_entry c_border background-color`

# print custom layout to file
if [ "`nvram get custom_color_theme`" = "1" ]
then
cat << EOF_JS > /var/custom_color_theme
var custom_theme=new Array('$main_theme_color','$logo_text_color','$c_menu_color','$body_bg_color',
			  '$text_color','$link_hover_color','$c_pfad_color','$but_color',
			  '$but_border_color','$h2_color','$h2_bg_color','$i_frame_color',
			  '$i_frame_bg_color','$c_stat_color','$menu_text_color','$menu_text_sel_color',
			  '$menu_bg_color','$menu_head_color','$svgbar_color','$svgbar_bg',
			  '$svgbar_text','$svgbar_border','$c_border_bg_color');
EOF_JS
fi

custom_available=0
if [ "`nvram getfile custom_color_theme_file\=$custom_color_theme_file`" != "Failed" ]
then
  custom_available=1
fi


info="<br /><p><img src=\"../pic_i_hinweis.gif\" border=\"0\"><br />To make the changes working, it could be necessary to reload the whole site or restart your browser.</p>"

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../jscolor/jscolor.js" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
<SCRIPT type="text/JavaScript">
function showErr() {
var err="$ERROR";
if(err!=""){alert(err);}
}

var entry_ids=new Array('main_theme_color','logo_text_color','c_menu_color','body_bg_color',
			'text_color','link_hover_color','c_pfad_color','but_color',
			'but_border_color','h2_color','h2_bg_color','i_frame_color',
			'i_frame_bg_color','c_stat_color','menu_text_color',
			'menu_text_sel_color','menu_bg_color','menu_head_color',
			'svgbar_color','svgbar_bg','svgbar_text','svgbar_border',
			'c_border_bg_color');
var themes=new Array();
var default_theme=new Array('f68918','fefefe','ffffff','FFFFFF',
                          '000000','f68918','D0D0D0','F68918',
                          'F68918','333333','DDDDDD','000000',
                          'F6F6F6','cccccc','000000','FFFFFF',
                          'F6F6F6','999999','F68918','EEEEEE',
                          '000000','000000','F0F0F0');
var blue_theme=new Array('2019f6','fefefe','ffffff','FFFFFF',
                          '000000','2019F6','D0D0D0','2019F6',
                          '2019F6','333333','DDDDDD','000000',
                          'F6F6F6','cccccc','000000','FFFFFF',
                          'F6F6F6','999999','2019F6','EEEEEE',
                          '000000','000000','F0F0F0');
var dark_theme=new Array('4e4e63','fefefe','808080','B8B8B8',
                          'FCFCFC','F6F159','D0D0D0','F6F159',
                          'F6F159','C9C9C9','303030','E6E6E6',
                          '919191','cccccc','C4C4C4','FFFFFF',
                          '919191','595959','4E4E63','EEEEEE',
                          'FFFFFF','000000','6B6B6B');
var lime_theme=new Array('109e00','fefefe','f0f0f0','DAFFB0',
                          '000000','109E00','1B8C12','109E00',
                          '109E00','BEFF8C','1BC900','000000',
                          'CCFF99','31cc4b','000000','FFFFFF',
                          'CCFF99','999999','109E00','EEEEEE',
                          '000000','000000','CCFF99');
themes.push(default_theme);
themes.push(blue_theme);
themes.push(dark_theme);
themes.push(lime_theme);

EOF_HTML

if [ $custom_available -eq 1 ]; then
  cat $custom_color_theme_file
  echo "themes.push(custom_theme);"
fi

cat << EOF_HTML
function setup_theme(select) {
  var val=select.options[select.options.selectedIndex].value;
  if(val<1){return;}

  var colors=themes[val-1];
  for(i=0; i<entry_ids.length; i++)
  {
    var entry=document.getElementById(entry_ids[i]);
    entry.value=colors[i].toUpperCase();
    entry.style.backgroundColor = '#' + colors[i].toUpperCase();
  }
}
</SCRIPT>
</HEAD>
<BODY onload="showErr();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Administration / Web-Interface color theme</DIV>
<div id="c_titel">Predefined color themes</div>
<div id="c_std">
<table border="0" cellpadding="4" cellspacing="0">
    <tr onmouseover="i_showElem(20)" onmouseout="i_showElem(0)"> <td width="100">Color Theme:</td><td>
<select name="color_theme" id="color_theme" onchange="setup_theme(this);">
<option value="0" ></option>
<option value="1" >Default</option>
<option value="2" >Blue</option>
<option value="3" >Dark</option>
<option value="4" >Lime</option>
EOF_HTML

[ $custom_available -eq 1 ] && echo '<option value="5" >Custom</option>'

cat << EOF_HTML
</select>
</td></tr></table>
</div>

<div id="c_leer"></div>
<div id="c_titel">Main screen colors</div>
<form id="xform" name="xform" id="xform" method="post">
<input id="action" type="hidden" name="action" id="action" value="0">
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0) >
<td width="250">Theme color:</td>
<td width="20"><input type="input" class="color" name="main_theme_color" id="main_theme_color" size="10"  maxlength="6" value="$main_theme_color"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0) >
<td width="250">Logo text color:</td>
<td width="20"><input type="input" class="color" name="logo_text_color" id="logo_text_color" size="10"  maxlength="6" value="$logo_text_color"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0) >
<td width="250">Menu entry background color:</td>
<td width="20"><input type="input" class="color" name="c_menu_color" id="c_menu_color" size="10"  maxlength="6" value="$c_menu_color"></td>
</tr></table>
</DIV>


<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0) >
<td width="250">Background color:</td>
<td width="20"><input type="input" class="color" name="body_bg_color" id="body_bg_color" size="10"  maxlength="6" value="$body_bg_color"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(21) onmouseout=i_showElem(0) >
<td width="250">Box background color:</td>
<td width="20"><input type="input" class="color" name="c_border_bg_color" id="c_border_bg_color" size="10"  maxlength="6" value="$c_border_bg_color"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(5) onmouseout=i_showElem(0) >
<td width="250">Text color:</td>
<td width="20"><input type="input" class="color" name="text_color" id="text_color" size="10"  maxlength="6" value="$text_color"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(6) onmouseout=i_showElem(0) >
<td width="250">Link highlight color:</td>
<td width="20"><input type="input" class="color" name="link_hover_color" id="link_hover_color" size="10"  maxlength="6" value="$link_hover_color"></td>
</tr></table>
</DIV>

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(7) onmouseout=i_showElem(0) >
<td width="250">Delimiter color:</td>
<td width="20"><input type="input" class="color" name="c_pfad_color" id="c_pfad_color" size="10"  maxlength="6" value="$c_pfad_color"></td>
</tr></table>
</DIV>

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(8) onmouseout=i_showElem(0) >
<td width="250">Button highlight text color:</td>
<td width="20"><input type="input" class="color" name="but_color" id="but_color" size="10"  maxlength="6" value="$but_color"></td>
</tr></table>
</DIV>

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(9) onmouseout=i_showElem(0) >
<td width="250">Button highlight border color:</td>
<td width="20"><input type="input" class="color" name="but_border_color" id="but_border_color" size="10"  maxlength="6" value="$but_border_color"></td>
</tr></table>
</DIV>

<div id="c_leer"></div>
<div id="c_titel">Information screen colors</div>

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(10) onmouseout=i_showElem(0) >
<td width="250">Heading text color:</td>
<td width="20"><input type="input" class="color" name="h2_color" id="h2_color" size="10"  maxlength="6" value="$h2_color"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(11) onmouseout=i_showElem(0) >
<td width="250">Heading background color:</td>
<td width="20"><input type="input" class="color" name="h2_bg_color" id="h2_bg_color" size="10"  maxlength="6" value="$h2_bg_color"></td>
</tr></table>
</DIV>

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(12) onmouseout=i_showElem(0) >
<td width="250">Text color:</td>
<td width="20"><input type="input" class="color" name="i_frame_color" id="i_frame_color" size="10"  maxlength="6" value="$i_frame_color"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(13) onmouseout=i_showElem(0) >
<td width="250">Background color:</td>
<td width="20"><input type="input" class="color" name="i_frame_bg_color" id="i_frame_bg_color" size="10"  maxlength="6" value="$i_frame_bg_color:"></td>
</tr></table>
</DIV>

<div id="c_leer"></div>
<div id="c_titel">Main Menu colors</div>

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(14) onmouseout=i_showElem(0) >
<td width="250">Main Menu Image background color:</td>
<td width="20"><input type="input" class="color" name="c_stat_color" id="c_stat_color" size="10"  maxlength="6" value="$c_stat_color"></td>
</tr></table>
</DIV>

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(15) onmouseout=i_showElem(0) >
<td width="250">Text color:</td>
<td width="20"><input type="input" class="color" name="menu_text_color" id="menu_text_color" size="10"  maxlength="6" value="$menu_text_color"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(16) onmouseout=i_showElem(0) >
<td width="250">Text highlight color:</td>
<td width="20"><input type="input" class="color" name="menu_text_sel_color" id="menu_text_sel_color" size="10"  maxlength="6" value="$menu_text_sel_color"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(17) onmouseout=i_showElem(0) >
<td width="250">Background color:</td>
<td width="20"><input type="input" class="color" name="menu_bg_color" id="menu_bg_color" size="10"  maxlength="6" value="$menu_bg_color:"></td>
</tr></table>
</DIV>

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(18) onmouseout=i_showElem(0) >
<td width="250">Heading color:</td>
<td width="20"><input type="input" class="color" name="menu_head_color" id="menu_head_color" size="10"  maxlength="6" value="$menu_head_color"></td>
</tr></table>
</DIV>

<div id="c_leer"></div>
<div id="c_titel">Progress bar colors</div>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(19) onmouseout=i_showElem(0) >
<td width="250">Color:</td>
<td width="20"><input type="input" class="color" name="svgbar_color" id="svgbar_color" size="10"  maxlength="6" value="$svgbar_color"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(19) onmouseout=i_showElem(0) >
<td width="250">Background Color:</td>
<td width="20"><input type="input" class="color" name="svgbar_bg" id="svgbar_bg" size="10"  maxlength="6" value="$svgbar_bg"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(19) onmouseout=i_showElem(0) >
<td width="250">Text color:</td>
<td width="20"><input type="input" class="color" name="svgbar_text" id="svgbar_text" size="10"  maxlength="6" value="$svgbar_text"></td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(19) onmouseout=i_showElem(0) >
<td width="250">Border color:</td>
<td width="20"><input type="input" class="color" name="svgbar_border" id="svgbar_border" size="10"  maxlength="6" value="$svgbar_border"></td>
</tr></table>
</DIV>

<DIV id=c_leer></DIV>
<DIV id="c_foot"></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV></DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";'>Back &lt;&lt;</DIV>
<DIV id=t_but2 onclick="subm(2);" onmouseover=rahmen(1,2) onmouseout=rahmen(0,2)>Save as custom</DIV>
<DIV id=t_but3 onclick="subm(3);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save &amp; Run</DIV>
<!--<DIV id=t_but4 onclick="subm(4);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Reset</DIV>-->
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Color theme</h2>
<p>Configure the color theme of the web-interface to be in accordance with your flavor.</p>
$info
</DIV>
<DIV id=i_content1>
<h2>Main screen</h2>
<p>Main theme color of the images and buttons.</p>
$info
</DIV>
<DIV id=i_content2>
<h2>Main screen</h2>
<p>Text color of images and the logo.</p>
$info
</DIV>
<DIV id=i_content3>
<h2>Main screen</h2>
<p>Background color of links in sub menus.</p>
$info
</DIV>
<DIV id=i_content4>
<h2>Main screen</h2>
<p>Background color of the main screen.</p>
$info
</DIV>
<DIV id=i_content5>
<h2>Main screen</h2>
<p>Text color of the main screen.</p>
$info
</DIV>
<DIV id=i_content6>
<h2>Main screen</h2>
<p>This is the highlighting color of links the mouse points to.</p>
$info
</DIV>
<DIV id=i_content7>
<h2>Main screen</h2>
<p>This is the color of the deliters for different sections on a configuration form.</p>
$info
</DIV>
<DIV id=i_content8>
<h2>Main screen</h2>
<p>This is the text color of the buttons when the mouse is over it.</p>
$info
</DIV>
<DIV id=i_content9>
<h2>Main screen</h2>
<p>This is the border color of the buttons when the mouse is over it.</p>
$info
</DIV>

<DIV id=i_content10>
<h2>Information screen</h2>
<p>Text color of headings of the information screen on the right side.</p>
$info
</DIV>
<DIV id=i_content11>
<h2>Information screen</h2>
<p>Background color of headings of the information screen on the right side.</p>
$info
</DIV>
<DIV id=i_content12>
<h2>Information screen</h2>
<p>Text color of the information screen on the right side.</p>
$info
</DIV>
<DIV id=i_content13>
<h2>Information screen</h2>
<p>Background color of the information screen on the right side.</p>
$info
</DIV>


<DIV id=i_content14>
<h2>Main Menu</h2>
<p>This is the background color of the image on top of the main menu.</p>
$info
</DIV>
<DIV id=i_content15>
<h2>Main Menu</h2>
<p>Text color of the main menu.</p>
$info
</DIV>
<DIV id=i_content16>
<h2>Main Menu</h2>
<p>Text color of the main menu if the mouse points to an entry.</p>
$info
</DIV>
<DIV id=i_content17>
<h2>Main Menu</h2>
<p>Background color of the main menu.</p>
$info
</DIV>
<DIV id=i_content18>
<h2>Main Menu</h2>
<p>Text color of headings in the main menu.</p>
$info
</DIV>

<DIV id=i_content19>
<h2>Progress Bar</h2>
<p></p>
$info
</DIV>

<DIV id=i_content20>
<h2>Predefined color themes</h2>
<p>You can coose a predefined color theme.</p>
$info
</DIV>

<DIV id=i_content21>
<h2>Main screen</h2>
<p>Background color of the container box in the main screen.</p>
$info
</DIV>
</DIV></BODY></HTML>
EOF_HTML

a=0