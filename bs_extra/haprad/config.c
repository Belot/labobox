/*
 * hostapd / Configuration file
 * Copyright (c) 2003-2006, Jouni Malinen <j@w1.fi>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Alternatively, this software may be distributed under the terms of BSD
 * license.
 *
 * See README and COPYING for more details.
 */

#include "includes.h"
#ifndef CONFIG_NATIVE_WINDOWS
#include <grp.h>
#endif /* CONFIG_NATIVE_WINDOWS */

#include "hostapd.h"
#include "sha1.h"
#include "eap.h"



#define MAX_STA_COUNT 2007

#if 0
static int hostapd_config_read_vlan_file(struct hostapd_bss_config *bss,
					 const char *fname)
{
	FILE *f;
	char buf[128], *pos, *pos2;
	int line = 0, vlan_id;
	struct hostapd_vlan *vlan;

	f = fopen(fname, "r");
	if (!f) {
		printf("VLAN file '%s' not readable.\n", fname);
		return -1;
	}

	while (fgets(buf, sizeof(buf), f)) {
		line++;

		if (buf[0] == '#')
			continue;
		pos = buf;
		while (*pos != '\0') {
			if (*pos == '\n') {
				*pos = '\0';
				break;
			}
			pos++;
		}
		if (buf[0] == '\0')
			continue;

		if (buf[0] == '*') {
			vlan_id = VLAN_ID_WILDCARD;
			pos = buf + 1;
		} else {
			vlan_id = strtol(buf, &pos, 10);
			if (buf == pos || vlan_id < 1 ||
			    vlan_id > MAX_VLAN_ID) {
				printf("Invalid VLAN ID at line %d in '%s'\n",
				       line, fname);
				fclose(f);
				return -1;
			}
		}

		while (*pos == ' ' || *pos == '\t')
			pos++;
		pos2 = pos;
		while (*pos2 != ' ' && *pos2 != '\t' && *pos2 != '\0')
			pos2++;
		*pos2 = '\0';
		if (*pos == '\0' || strlen(pos) > IFNAMSIZ) {
			printf("Invalid VLAN ifname at line %d in '%s'\n",
			       line, fname);
			fclose(f);
			return -1;
		}

		vlan = malloc(sizeof(*vlan));
		if (vlan == NULL) {
			printf("Out of memory while reading VLAN interfaces "
			       "from '%s'\n", fname);
			fclose(f);
			return -1;
		}

		memset(vlan, 0, sizeof(*vlan));
		vlan->vlan_id = vlan_id;
		strncpy(vlan->ifname, pos, sizeof(vlan->ifname));
		if (bss->vlan_tail)
			bss->vlan_tail->next = vlan;
		else
			bss->vlan = vlan;
		bss->vlan_tail = vlan;
	}

	fclose(f);

	return 0;
}


static void hostapd_config_free_vlan(struct hostapd_bss_config *bss)
{
	struct hostapd_vlan *vlan, *prev;

	vlan = bss->vlan;
	prev = NULL;
	while (vlan) {
		prev = vlan;
		vlan = vlan->next;
		free(prev);
	}

	bss->vlan = NULL;
}
#endif

/* convert floats with one decimal place to value*10 int, i.e.,
 * "1.5" will return 15 */
#if 0
static int hostapd_config_read_int10(const char *value)
{
	int i, d;
	char *pos;

	i = atoi(value);
	pos = strchr(value, '.');
	d = 0;
	if (pos) {
		pos++;
		if (*pos >= '0' && *pos <= '9')
			d = *pos - '0';
	}

	return i * 10 + d;
}


static void hostapd_config_defaults_bss(struct hostapd_bss_config *bss)
{
	bss->logger_syslog_level = HOSTAPD_LEVEL_INFO;
	bss->logger_stdout_level = HOSTAPD_LEVEL_INFO;
	bss->logger_syslog = (unsigned int) -1;
	bss->logger_stdout = (unsigned int) -1;

	bss->auth_algs = HOSTAPD_AUTH_OPEN | HOSTAPD_AUTH_SHARED_KEY;

	bss->wep_rekeying_period = 300;
	/* use key0 in individual key and key1 in broadcast key */
	bss->broadcast_key_idx_min = 1;
	bss->broadcast_key_idx_max = 2;
	bss->eap_reauth_period = 3600;

	bss->wpa_group_rekey = 600;
	bss->wpa_gmk_rekey = 86400;
	bss->wpa_key_mgmt = WPA_KEY_MGMT_PSK;
	bss->wpa_pairwise = WPA_CIPHER_TKIP;
	bss->wpa_group = WPA_CIPHER_TKIP;

	bss->max_num_sta = MAX_STA_COUNT;

	bss->dtim_period = 2;

	bss->radius_server_auth_port = 1812;
	bss->ap_max_inactivity = AP_MAX_INACTIVITY;
	bss->eapol_version = EAPOL_VERSION;
}
#endif



#if 0
static int hostapd_parse_ip_addr(const char *txt, struct hostapd_ip_addr *addr)
{
	if (inet_aton(txt, &addr->u.v4)) {
		addr->af = AF_INET;
		return 0;
	}

#ifdef CONFIG_IPV6
	if (inet_pton(AF_INET6, txt, &addr->u.v6) > 0) {
		addr->af = AF_INET6;
		return 0;
	}
#endif /* CONFIG_IPV6 */

	return -1;
}


int hostapd_mac_comp(const void *a, const void *b)
{
	return memcmp(a, b, sizeof(macaddr));
}


int hostapd_mac_comp_empty(const void *a)
{
	macaddr empty = { 0 };
	return memcmp(a, empty, sizeof(macaddr));
}


static int hostapd_config_read_maclist(const char *fname, macaddr **acl,
				       int *num)
{
	FILE *f;
	char buf[128], *pos;
	int line = 0;
	u8 addr[ETH_ALEN];
	macaddr *newacl;

	if (!fname)
		return 0;

	f = fopen(fname, "r");
	if (!f) {
		printf("MAC list file '%s' not found.\n", fname);
		return -1;
	}

	while (fgets(buf, sizeof(buf), f)) {
		line++;

		if (buf[0] == '#')
			continue;
		pos = buf;
		while (*pos != '\0') {
			if (*pos == '\n') {
				*pos = '\0';
				break;
			}
			pos++;
		}
		if (buf[0] == '\0')
			continue;

		if (hwaddr_aton(buf, addr)) {
			printf("Invalid MAC address '%s' at line %d in '%s'\n",
			       buf, line, fname);
			fclose(f);
			return -1;
		}

		newacl = (macaddr *) realloc(*acl, (*num + 1) * ETH_ALEN);
		if (newacl == NULL) {
			printf("MAC list reallocation failed\n");
			fclose(f);
			return -1;
		}

		*acl = newacl;
		memcpy((*acl)[*num], addr, ETH_ALEN);
		(*num)++;
	}

	fclose(f);

	qsort(*acl, *num, sizeof(macaddr), hostapd_mac_comp);

	return 0;
}


static int hostapd_config_read_wpa_psk(const char *fname,
				       struct hostapd_ssid *ssid)
{
	FILE *f;
	char buf[128], *pos;
	int line = 0, ret = 0, len, ok;
	u8 addr[ETH_ALEN];
	struct hostapd_wpa_psk *psk;

	if (!fname)
		return 0;

	f = fopen(fname, "r");
	if (!f) {
		printf("WPA PSK file '%s' not found.\n", fname);
		return -1;
	}

	while (fgets(buf, sizeof(buf), f)) {
		line++;

		if (buf[0] == '#')
			continue;
		pos = buf;
		while (*pos != '\0') {
			if (*pos == '\n') {
				*pos = '\0';
				break;
			}
			pos++;
		}
		if (buf[0] == '\0')
			continue;

		if (hwaddr_aton(buf, addr)) {
			printf("Invalid MAC address '%s' on line %d in '%s'\n",
			       buf, line, fname);
			ret = -1;
			break;
		}

		psk = wpa_zalloc(sizeof(*psk));
		if (psk == NULL) {
			printf("WPA PSK allocation failed\n");
			ret = -1;
			break;
		}
		if (memcmp(addr, "\x00\x00\x00\x00\x00\x00", ETH_ALEN) == 0)
			psk->group = 1;
		else
			memcpy(psk->addr, addr, ETH_ALEN);

		pos = buf + 17;
		if (pos == '\0') {
			printf("No PSK on line %d in '%s'\n", line, fname);
			free(psk);
			ret = -1;
			break;
		}
		pos++;

		ok = 0;
		len = strlen(pos);
		if (len == 64 && hexstr2bin(pos, psk->psk, PMK_LEN) == 0)
			ok = 1;
		else if (len >= 8 && len < 64) {
			pbkdf2_sha1(pos, ssid->ssid, ssid->ssid_len,
				    4096, psk->psk, PMK_LEN);
			ok = 1;
		}
		if (!ok) {
			printf("Invalid PSK '%s' on line %d in '%s'\n",
			       pos, line, fname);
			free(psk);
			ret = -1;
			break;
		}

		psk->next = ssid->wpa_psk;
		ssid->wpa_psk = psk;
	}

	fclose(f);

	return ret;
}


int hostapd_setup_wpa_psk(struct hostapd_bss_config *conf)
{
	struct hostapd_ssid *ssid = &conf->ssid;

	if (ssid->wpa_passphrase != NULL) {
		if (ssid->wpa_psk != NULL) {
			printf("Warning: both WPA PSK and passphrase set. "
			       "Using passphrase.\n");
			free(ssid->wpa_psk);
		}
		ssid->wpa_psk = wpa_zalloc(sizeof(struct hostapd_wpa_psk));
		if (ssid->wpa_psk == NULL) {
			printf("Unable to alloc space for PSK\n");
			return -1;
		}
		wpa_hexdump_ascii(MSG_DEBUG, "SSID",
				  (u8 *) ssid->ssid, ssid->ssid_len);
		wpa_hexdump_ascii(MSG_DEBUG, "PSK (ASCII passphrase)",
				  (u8 *) ssid->wpa_passphrase,
				  strlen(ssid->wpa_passphrase));
		pbkdf2_sha1(ssid->wpa_passphrase,
			    ssid->ssid, ssid->ssid_len,
			    4096, ssid->wpa_psk->psk, PMK_LEN);
		wpa_hexdump(MSG_DEBUG, "PSK (from passphrase)",
			    ssid->wpa_psk->psk, PMK_LEN);
		ssid->wpa_psk->group = 1;

		memset(ssid->wpa_passphrase, 0,
		       strlen(ssid->wpa_passphrase));
		free(ssid->wpa_passphrase);
		ssid->wpa_passphrase = NULL;
	}

	if (ssid->wpa_psk_file) {
		if (hostapd_config_read_wpa_psk(ssid->wpa_psk_file,
						&conf->ssid))
			return -1;
		free(ssid->wpa_psk_file);
		ssid->wpa_psk_file = NULL;
	}

	return 0;
}
#endif

#ifdef EAP_SERVER
 int hostapd_config_read_eap_user(const char *fname,
					struct hostapd_bss_config *conf)
{
	FILE *f;
	char buf[512], *pos, *start, *pos2;
	int line = 0, ret = 0, num_methods;
	struct hostapd_eap_user *user, *tail = NULL;

	if (!fname)
		return 0;

	f = fopen(fname, "r");
	if (!f) {
		printf("EAP user file '%s' not found.\n", fname);
		return -1;
	}

	/* Lines: "user" METHOD,METHOD2 "password" (password optional) */
	while (fgets(buf, sizeof(buf), f)) {
		line++;

		if (buf[0] == '#')
			continue;
		pos = buf;
		while (*pos != '\0') {
			if (*pos == '\n') {
				*pos = '\0';
				break;
			}
			pos++;
		}
		if (buf[0] == '\0')
			continue;

		user = NULL;

		if (buf[0] != '"' && buf[0] != '*') {
			printf("Invalid EAP identity (no \" in start) on "
			       "line %d in '%s'\n", line, fname);
			goto failed;
		}

		user = wpa_zalloc(sizeof(*user));
		if (user == NULL) {
			printf("EAP user allocation failed\n");
			goto failed;
		}
		user->force_version = -1;

		if (buf[0] == '*') {
			pos = buf;
		} else {
			pos = buf + 1;
			start = pos;
			while (*pos != '"' && *pos != '\0')
				pos++;
			if (*pos == '\0') {
				printf("Invalid EAP identity (no \" in end) on"
				       " line %d in '%s'\n", line, fname);
				goto failed;
			}

			user->identity = malloc(pos - start);
			if (user->identity == NULL) {
				printf("Failed to allocate memory for EAP "
				       "identity\n");
				goto failed;
			}
			memcpy(user->identity, start, pos - start);
			user->identity_len = pos - start;

			if (pos[0] == '"' && pos[1] == '*') {
				user->wildcard_prefix = 1;
				pos++;
			}
		}
		pos++;
		while (*pos == ' ' || *pos == '\t')
			pos++;

		if (*pos == '\0') {
			printf("No EAP method on line %d in '%s'\n",
			       line, fname);
			goto failed;
		}

		start = pos;
		while (*pos != ' ' && *pos != '\t' && *pos != '\0')
			pos++;
		if (*pos == '\0') {
			pos = NULL;
		} else {
			*pos = '\0';
			pos++;
		}
		num_methods = 0;
		while (*start) {
			char *pos3 = strchr(start, ',');
			if (pos3) {
				*pos3++ = '\0';
			}
			user->methods[num_methods].method =
				eap_get_type(start, &user->methods[num_methods]
					     .vendor);
			if (user->methods[num_methods].vendor ==
			    EAP_VENDOR_IETF &&
			    user->methods[num_methods].method == EAP_TYPE_NONE)
			{
				printf("Unsupported EAP type '%s' on line %d "
				       "in '%s'\n", start, line, fname);
				goto failed;
			}

			num_methods++;
			if (num_methods >= EAP_USER_MAX_METHODS)
				break;
			if (pos3 == NULL)
				break;
			start = pos3;
		}
		if (num_methods == 0) {
			printf("No EAP types configured on line %d in '%s'\n",
			       line, fname);
			goto failed;
		}

		if (pos == NULL)
			goto done;

		while (*pos == ' ' || *pos == '\t')
			pos++;
		if (*pos == '\0')
			goto done;

		if (strncmp(pos, "[ver=0]", 7) == 0) {
			user->force_version = 0;
			goto done;
		}

		if (strncmp(pos, "[ver=1]", 7) == 0) {
			user->force_version = 1;
			goto done;
		}

		if (strncmp(pos, "[2]", 3) == 0) {
			user->phase2 = 1;
			goto done;
		}

		if (*pos == '"') {
			pos++;
			start = pos;
			while (*pos != '"' && *pos != '\0')
				pos++;
			if (*pos == '\0') {
				printf("Invalid EAP password (no \" in end) "
				       "on line %d in '%s'\n", line, fname);
				goto failed;
			}

			user->password = malloc(pos - start);
			if (user->password == NULL) {
				printf("Failed to allocate memory for EAP "
				       "password\n");
				goto failed;
			}
			memcpy(user->password, start, pos - start);
			user->password_len = pos - start;

			pos++;
		} else if (strncmp(pos, "hash:", 5) == 0) {
			pos += 5;
			pos2 = pos;
			while (*pos2 != '\0' && *pos2 != ' ' &&
			       *pos2 != '\t' && *pos2 != '#')
				pos2++;
			if (pos2 - pos != 32) {
				printf("Invalid password hash on line %d in "
				       "'%s'\n", line, fname);
				goto failed;
			}
			user->password = malloc(16);
			if (user->password == NULL) {
				printf("Failed to allocate memory for EAP "
				       "password hash\n");
				goto failed;
			}
			if (hexstr2bin(pos, user->password, 16) < 0) {
				printf("Invalid hash password on line %d in "
				       "'%s'\n", line, fname);
				goto failed;
			}
			user->password_len = 16;
			user->password_hash = 1;
			pos = pos2;
		} else {
			pos2 = pos;
			while (*pos2 != '\0' && *pos2 != ' ' &&
			       *pos2 != '\t' && *pos2 != '#')
				pos2++;
			if ((pos2 - pos) & 1) {
				printf("Invalid hex password on line %d in "
				       "'%s'\n", line, fname);
				goto failed;
			}
			user->password = malloc((pos2 - pos) / 2);
			if (user->password == NULL) {
				printf("Failed to allocate memory for EAP "
				       "password\n");
				goto failed;
			}
			if (hexstr2bin(pos, user->password,
				       (pos2 - pos) / 2) < 0) {
				printf("Invalid hex password on line %d in "
				       "'%s'\n", line, fname);
				goto failed;
			}
			user->password_len = (pos2 - pos) / 2;
			pos = pos2;
		}

		while (*pos == ' ' || *pos == '\t')
			pos++;
		if (strncmp(pos, "[2]", 3) == 0) {
			user->phase2 = 1;
		}

	done:
		if (tail == NULL) {
			tail = conf->eap_user = user;
		} else {
			tail->next = user;
			tail = user;
		}
		continue;

	failed:
		if (user) {
			free(user->password);
			free(user->identity);
			free(user);
		}
		ret = -1;
		break;
	}

	fclose(f);

	return ret;
}
#endif /* EAP_SERVER */

#if 0
static int
hostapd_config_read_radius_addr(struct hostapd_radius_server **server,
				int *num_server, const char *val, int def_port,
				struct hostapd_radius_server **curr_serv)
{
	struct hostapd_radius_server *nserv;
	int ret;
	static int server_index = 1;

	nserv = realloc(*server, (*num_server + 1) * sizeof(*nserv));
	if (nserv == NULL)
		return -1;

	*server = nserv;
	nserv = &nserv[*num_server];
	(*num_server)++;
	(*curr_serv) = nserv;

	memset(nserv, 0, sizeof(*nserv));
	nserv->port = def_port;
	ret = hostapd_parse_ip_addr(val, &nserv->addr);
	nserv->index = server_index++;

	return ret;
}


static int hostapd_config_parse_key_mgmt(int line, const char *value)
{
	int val = 0, last;
	char *start, *end, *buf;

	buf = strdup(value);
	if (buf == NULL)
		return -1;
	start = buf;

	while (start != '\0') {
		while (*start == ' ' || *start == '\t')
			start++;
		if (*start == '\0')
			break;
		end = start;
		while (*end != ' ' && *end != '\t' && *end != '\0')
			end++;
		last = *end == '\0';
		*end = '\0';
		if (strcmp(start, "WPA-PSK") == 0)
			val |= WPA_KEY_MGMT_PSK;
		else if (strcmp(start, "WPA-EAP") == 0)
			val |= WPA_KEY_MGMT_IEEE8021X;
		else {
			printf("Line %d: invalid key_mgmt '%s'\n",
			       line, start);
			free(buf);
			return -1;
		}

		if (last)
			break;
		start = end + 1;
	}

	free(buf);
	if (val == 0) {
		printf("Line %d: no key_mgmt values configured.\n", line);
		return -1;
	}

	return val;
}


static int hostapd_config_parse_cipher(int line, const char *value)
{
	int val = 0, last;
	char *start, *end, *buf;

	buf = strdup(value);
	if (buf == NULL)
		return -1;
	start = buf;

	while (start != '\0') {
		while (*start == ' ' || *start == '\t')
			start++;
		if (*start == '\0')
			break;
		end = start;
		while (*end != ' ' && *end != '\t' && *end != '\0')
			end++;
		last = *end == '\0';
		*end = '\0';
		if (strcmp(start, "CCMP") == 0)
			val |= WPA_CIPHER_CCMP;
		else if (strcmp(start, "TKIP") == 0)
			val |= WPA_CIPHER_TKIP;
		else if (strcmp(start, "WEP104") == 0)
			val |= WPA_CIPHER_WEP104;
		else if (strcmp(start, "WEP40") == 0)
			val |= WPA_CIPHER_WEP40;
		else if (strcmp(start, "NONE") == 0)
			val |= WPA_CIPHER_NONE;
		else {
			printf("Line %d: invalid cipher '%s'.", line, start);
			free(buf);
			return -1;
		}

		if (last)
			break;
		start = end + 1;
	}
	free(buf);

	if (val == 0) {
		printf("Line %d: no cipher values configured.", line);
		return -1;
	}
	return val;
}


static int hostapd_config_check_bss(struct hostapd_bss_config *bss,
				    struct hostapd_config *conf)
{
	if (bss->ieee802_1x && !bss->eap_server &&
	    !bss->radius->auth_servers) {
		printf("Invalid IEEE 802.1X configuration (no EAP "
		       "authenticator configured).\n");
		return -1;
	}

	return 0;
}


static int hostapd_config_check(struct hostapd_config *conf)
{
	size_t i;

	for (i = 0; i < conf->num_bss; i++) {
		if (hostapd_config_check_bss(&conf->bss[i], conf))
			return -1;
	}

	return 0;
}
#endif
#if 0
static int hostapd_config_read_wep(struct hostapd_wep_keys *wep, int keyidx,
				   char *val)
{
	size_t len = strlen(val);

	if (keyidx < 0 || keyidx > 3 || wep->key[keyidx] != NULL)
		return -1;

	if (val[0] == '"') {
		if (len < 2 || val[len - 1] != '"')
			return -1;
		len -= 2;
		wep->key[keyidx] = malloc(len);
		if (wep->key[keyidx] == NULL)
			return -1;
		memcpy(wep->key[keyidx], val + 1, len);
		wep->len[keyidx] = len;
	} else {
		if (len & 1)
			return -1;
		len /= 2;
		wep->key[keyidx] = malloc(len);
		if (wep->key[keyidx] == NULL)
			return -1;
		wep->len[keyidx] = len;
		if (hexstr2bin(val, wep->key[keyidx], len) < 0)
			return -1;
	}

	wep->keys_set++;

	return 0;
}


static int hostapd_parse_rates(int **rate_list, char *val)
{
	int *list;
	int count;
	char *pos, *end;

	free(*rate_list);
	*rate_list = NULL;

	pos = val;
	count = 0;
	while (*pos != '\0') {
		if (*pos == ' ')
			count++;
		pos++;
	}

	list = malloc(sizeof(int) * (count + 2));
	if (list == NULL)
		return -1;
	pos = val;
	count = 0;
	while (*pos != '\0') {
		end = strchr(pos, ' ');
		if (end)
			*end = '\0';

		list[count++] = atoi(pos);
		if (!end)
			break;
		pos = end + 1;
	}
	list[count] = -1;

	*rate_list = list;
	return 0;
}


static int hostapd_config_bss(struct hostapd_config *conf, const char *ifname)
{
	struct hostapd_bss_config *bss;

	if (*ifname == '\0')
		return -1;

	bss = realloc(conf->bss, (conf->num_bss + 1) *
		      sizeof(struct hostapd_bss_config));
	if (bss == NULL) {
		printf("Failed to allocate memory for multi-BSS entry\n");
		return -1;
	}
	conf->bss = bss;

	bss = &(conf->bss[conf->num_bss]);
	memset(bss, 0, sizeof(*bss));
	bss->radius = wpa_zalloc(sizeof(*bss->radius));
	if (bss->radius == NULL) {
		printf("Failed to allocate memory for multi-BSS RADIUS "
		       "data\n");
		return -1;
	}

	conf->num_bss++;
	conf->last_bss = bss;

	hostapd_config_defaults_bss(bss);
	snprintf(bss->iface, sizeof(bss->iface), "%s", ifname);
	memcpy(bss->ssid.vlan, bss->iface, IFNAMSIZ + 1);

	return 0;
}


static int valid_cw(int cw)
{
	return (cw == 1 || cw == 3 || cw == 7 || cw == 15 || cw == 31 ||
		cw == 63 || cw == 127 || cw == 255 || cw == 511 || cw == 1023);
}


enum {
	IEEE80211_TX_QUEUE_DATA0 = 0, /* used for EDCA AC_VO data */
	IEEE80211_TX_QUEUE_DATA1 = 1, /* used for EDCA AC_VI data */
	IEEE80211_TX_QUEUE_DATA2 = 2, /* used for EDCA AC_BE data */
	IEEE80211_TX_QUEUE_DATA3 = 3, /* used for EDCA AC_BK data */
	IEEE80211_TX_QUEUE_DATA4 = 4,
	IEEE80211_TX_QUEUE_AFTER_BEACON = 6,
	IEEE80211_TX_QUEUE_BEACON = 7
};

static int hostapd_config_tx_queue(struct hostapd_config *conf, char *name,
				   char *val)
{
	int num;
	char *pos;
	struct hostapd_tx_queue_params *queue;

	/* skip 'tx_queue_' prefix */
	pos = name + 9;
	if (strncmp(pos, "data", 4) == 0 &&
	    pos[4] >= '0' && pos[4] <= '9' && pos[5] == '_') {
		num = pos[4] - '0';
		pos += 6;
	} else if (strncmp(pos, "after_beacon_", 13) == 0) {
		num = IEEE80211_TX_QUEUE_AFTER_BEACON;
		pos += 13;
	} else if (strncmp(pos, "beacon_", 7) == 0) {
		num = IEEE80211_TX_QUEUE_BEACON;
		pos += 7;
	} else {
		printf("Unknown tx_queue name '%s'\n", pos);
		return -1;
	}

	queue = &conf->tx_queue[num];

	if (strcmp(pos, "aifs") == 0) {
		queue->aifs = atoi(val);
		if (queue->aifs < 0 || queue->aifs > 255) {
			printf("Invalid AIFS value %d\n", queue->aifs);
			return -1;
		}
	} else if (strcmp(pos, "cwmin") == 0) {
		queue->cwmin = atoi(val);
		if (!valid_cw(queue->cwmin)) {
			printf("Invalid cwMin value %d\n", queue->cwmin);
			return -1;
		}
	} else if (strcmp(pos, "cwmax") == 0) {
		queue->cwmax = atoi(val);
		if (!valid_cw(queue->cwmax)) {
			printf("Invalid cwMax value %d\n", queue->cwmax);
			return -1;
		}
	} else if (strcmp(pos, "burst") == 0) {
		queue->burst = hostapd_config_read_int10(val);
	} else {
		printf("Unknown tx_queue field '%s'\n", pos);
		return -1;
	}

	queue->configured = 1;

	return 0;
}


static int hostapd_config_wme_ac(struct hostapd_config *conf, char *name,
				   char *val)
{
	int num, v;
	char *pos;
	struct hostapd_wme_ac_params *ac;

	/* skip 'wme_ac_' prefix */
	pos = name + 7;
	if (strncmp(pos, "be_", 3) == 0) {
		num = 0;
		pos += 3;
	} else if (strncmp(pos, "bk_", 3) == 0) {
		num = 1;
		pos += 3;
	} else if (strncmp(pos, "vi_", 3) == 0) {
		num = 2;
		pos += 3;
	} else if (strncmp(pos, "vo_", 3) == 0) {
		num = 3;
		pos += 3;
	} else {
		printf("Unknown wme name '%s'\n", pos);
		return -1;
	}

	ac = &conf->wme_ac_params[num];

	if (strcmp(pos, "aifs") == 0) {
		v = atoi(val);
		if (v < 1 || v > 255) {
			printf("Invalid AIFS value %d\n", v);
			return -1;
		}
		ac->aifs = v;
	} else if (strcmp(pos, "cwmin") == 0) {
		v = atoi(val);
		if (v < 0 || v > 12) {
			printf("Invalid cwMin value %d\n", v);
			return -1;
		}
		ac->cwmin = v;
	} else if (strcmp(pos, "cwmax") == 0) {
		v = atoi(val);
		if (v < 0 || v > 12) {
			printf("Invalid cwMax value %d\n", v);
			return -1;
		}
		ac->cwmax = v;
	} else if (strcmp(pos, "txop_limit") == 0) {
		v = atoi(val);
		if (v < 0 || v > 0xffff) {
			printf("Invalid txop value %d\n", v);
			return -1;
		}
		ac->txopLimit = v;
	} else if (strcmp(pos, "acm") == 0) {
		v = atoi(val);
		if (v < 0 || v > 1) {
			printf("Invalid acm value %d\n", v);
			return -1;
		}
		ac->admission_control_mandatory = v;
	} else {
		printf("Unknown wme_ac_ field '%s'\n", pos);
		return -1;
	}

	return 0;
}
#endif

static struct hostapd_config * hostapd_config_defaults(void)
{
  struct hostapd_config *conf;
  struct hostapd_bss_config *bss;

  conf = wpa_zalloc(sizeof(*conf));
  bss = wpa_zalloc(sizeof(*bss));
  if (conf == NULL || bss == NULL) {
    printf("Failed to allocate memory for configuration data.\n");
    free(conf);
    free(bss);
    return NULL;
  }

  bss->eap_user=NULL;
  bss->radius_server_clients=strdup("haprad.radius_clients");
  bss->server_cert=strdup("keys/radserver_cert.crt");
  bss->ca_cert = strdup("keys/ca.crt");
  bss->private_key = strdup("keys/radserver_cert.key");
  bss->private_key_passwd = strdup("");
  bss->radius_server_auth_port = 1812;
  bss->radius_server_ipv6 = 0;
  bss->check_crl=0;
  
  conf->num_bss = 1;
  conf->bss = bss;
  
  return conf;
}


struct hostapd_config * hostapd_config_read(const char *fname)
{
	struct hostapd_config *conf;
	struct hostapd_bss_config *bss;
	FILE *f;
	char buf[256], *pos;
	int line = 0;
	int errors = 0;

	f = fopen(fname, "r");
	if (f == NULL) {
		printf("Could not open configuration file '%s' for reading.\n",
		       fname);
		return NULL;
	}

	conf = hostapd_config_defaults();
	if (conf == NULL) {
		fclose(f);
		return NULL;
	}
	bss = conf->last_bss = conf->bss;

	while (fgets(buf, sizeof(buf), f)) {
		bss = conf->last_bss;
		line++;

		if (buf[0] == '#')
			continue;
		pos = buf;
		while (*pos != '\0') {
			if (*pos == '\n') {
				*pos = '\0';
				break;
			}
			pos++;
		}
		if (buf[0] == '\0')
			continue;

		pos = strchr(buf, '=');
		if (pos == NULL) {
			printf("Line %d: invalid line '%s'\n", line, buf);
			errors++;
			continue;
		}
		*pos = '\0';
		pos++;


                if (strcmp(buf, "eap_user_file") == 0) {
			if (hostapd_config_read_eap_user(pos, bss))
				errors++;
		} else if (strcmp(buf, "ca_cert") == 0) {
			free(bss->ca_cert);
			bss->ca_cert = strdup(pos);
		} else if (strcmp(buf, "server_cert") == 0) {
			free(bss->server_cert);
			bss->server_cert = strdup(pos);
		} else if (strcmp(buf, "private_key") == 0) {
			free(bss->private_key);
			bss->private_key = strdup(pos);
		} else if (strcmp(buf, "private_key_passwd") == 0) {
			free(bss->private_key_passwd);
			bss->private_key_passwd = strdup(pos);
		} else if (strcmp(buf, "check_crl") == 0) {
			bss->check_crl = atoi(pos);
#ifdef EAP_SIM
		} else if (strcmp(buf, "eap_sim_db") == 0) {
			free(bss->eap_sim_db);
			bss->eap_sim_db = strdup(pos);
#endif /* EAP_SIM */
#ifdef RADIUS_SERVER
		} else if (strcmp(buf, "radius_server_clients") == 0) {
			free(bss->radius_server_clients);
			bss->radius_server_clients = strdup(pos);
		} else if (strcmp(buf, "radius_server_auth_port") == 0) {
			bss->radius_server_auth_port = atoi(pos);
#endif /* RADIUS_SERVER */
		} else {
			printf("Line %d: unknown configuration item '%s'\n",
			       line, buf);
			errors++;
		}
	}

	fclose(f);
        
        if(!bss->eap_user){
          printf("No EAP-Users configured\n");
          errors++;
        }


	if (errors) {
		printf("%d errors found in configuration file '%s'\n",
		       errors, fname);
		hostapd_config_free(conf);
		conf = NULL;
	}
	return conf;
}

#if 0
int hostapd_wep_key_cmp(struct hostapd_wep_keys *a, struct hostapd_wep_keys *b)
{
	int i;

	if (a->idx != b->idx || a->default_len != b->default_len)
		return 1;
	for (i = 0; i < NUM_WEP_KEYS; i++)
		if (a->len[i] != b->len[i] ||
		    memcmp(a->key[i], b->key[i], a->len[i]) != 0)
			return 1;
	return 0;
}
#endif

#if 0
static void hostapd_config_free_radius(struct hostapd_radius_server *servers,
				       int num_servers)
{
	int i;

	for (i = 0; i < num_servers; i++) {
		free(servers[i].shared_secret);
	}
	free(servers);
}
#endif

static void hostapd_config_free_eap_user(struct hostapd_eap_user *user)
{
	free(user->identity);
	free(user->password);
	free(user);
}

#if 0
static void hostapd_config_free_wep(struct hostapd_wep_keys *keys)
{
	int i;
	for (i = 0; i < NUM_WEP_KEYS; i++) {
		free(keys->key[i]);
		keys->key[i] = NULL;
	}
}
#endif 


static void hostapd_config_free_bss(struct hostapd_bss_config *conf)
{
	struct hostapd_eap_user *user, *prev_user;

	if (conf == NULL)
		return;
	user = conf->eap_user;
	while (user) {
		prev_user = user;
		user = user->next;
		hostapd_config_free_eap_user(prev_user);
	}
	free(conf->ca_cert);
	free(conf->server_cert);
	free(conf->private_key);
	free(conf->private_key_passwd);
	free(conf->eap_sim_db);
	free(conf->radius_server_clients);

}

void hostapd_config_free(struct hostapd_config *conf)
{
	size_t i;

	if (conf == NULL)
		return;

	for (i = 0; i < conf->num_bss; i++)
		hostapd_config_free_bss(&conf->bss[i]);
	free(conf->bss);

	free(conf);
}

const struct hostapd_eap_user *
hostapd_get_eap_user(const struct hostapd_bss_config *conf, const u8 *identity,
		     size_t identity_len, int phase2)
{
	struct hostapd_eap_user *user = conf->eap_user;

	while (user) {
		if (!phase2 && user->identity == NULL) {
			/* Wildcard match */
			break;
		}

		if (!phase2 && user->wildcard_prefix &&
		    identity_len >= user->identity_len &&
		    memcmp(user->identity, identity, user->identity_len) == 0)
		{
			/* Wildcard prefix match */
			break;
		}

		if (user->phase2 == !!phase2 &&
		    user->identity_len == identity_len &&
		    memcmp(user->identity, identity, identity_len) == 0) 
		    
			
			break;
		user = user->next;
	}

	return user;
}
