#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <board_api.h>
#include "multipart.h"

#define UINT32 unsigned int
#define  BLOCK_SIZE	(65536)
#define CRC32_INIT_VALUE (0xffffffff)

typedef enum
{
	FLASH_OK=0,
	FLASH_FAIL_CFM,
	FLASH_FAIL_ROOTFS,
	FLASH_CRC_ERROR,
	FLASH_INCOMPATIBLE,
	FLASH_WRONG_CHIP,
	FLASH_FILE_ERROR,
	FLASH_FILE_EREAD,
	FLASH_IMAGE_TO_BIG,
	FLASH_NO_MEM,
	FLASH_ERROR,
	FLASH_ERROR_READ_HTTP,
	FLASH_ERROR_MULTIPART
} FLASH_RESULT;

#define BCM_SIG_1   "Broadcom Corporation"
#define BCM_SIG_2   "ver. 2.0"          // was "firmware version 2.0" now it is split 6 char out for chip id.

#define BCM_TAG_VER         "6"
#define BCM_TAG_VER_LAST    "26"

// file tag (head) structure all is in clear text except validationTokens (crc, md5, sha1, etc). Total: 128 unsigned chars
#define TAG_LEN         256
#define TAG_VER_LEN     4
#define SIG_LEN         20
#define SIG_LEN_2       14   // Original second SIG = 20 is now devided into 14 for SIG_LEN_2 and 6 for CHIP_ID
#define CHIP_ID_LEN             6
#define IMAGE_LEN       10
#define ADDRESS_LEN     12
#define FLAG_LEN        2
#define TOKEN_LEN       20
#define BOARD_ID_LEN    16
#define RESERVED_LEN    (TAG_LEN - TAG_VER_LEN - SIG_LEN - SIG_LEN_2 - CHIP_ID_LEN - BOARD_ID_LEN - \
                        (4*IMAGE_LEN) - (3*ADDRESS_LEN) - (3*FLAG_LEN) - (2*TOKEN_LEN))
// TAG for downloadable image (kernel plus file system)
typedef struct _FILE_TAG
{
    unsigned char tagVersion[TAG_VER_LEN];       // tag version.  Will be 2 here.
    unsigned char signiture_1[SIG_LEN];          // text line for company info
    unsigned char signiture_2[SIG_LEN_2];        // additional info (can be version number)
    unsigned char chipId[CHIP_ID_LEN];                   // chip id
    unsigned char boardId[BOARD_ID_LEN];         // board id
    unsigned char bigEndian[FLAG_LEN];           // if = 1 - big, = 0 - little endia of the host
    unsigned char totalImageLen[IMAGE_LEN];      // the sum of all the following length
    unsigned char cfeAddress[ADDRESS_LEN];       // if non zero, cfe starting address
    unsigned char cfeLen[IMAGE_LEN];             // if non zero, cfe size in clear ASCII text.
    unsigned char rootfsAddress[ADDRESS_LEN];    // if non zero, filesystem starting address
    unsigned char rootfsLen[IMAGE_LEN];          // if non zero, filesystem size in clear ASCII text.
    unsigned char kernelAddress[ADDRESS_LEN];    // if non zero, kernel starting address
    unsigned char kernelLen[IMAGE_LEN];          // if non zero, kernel size in clear ASCII text.
    unsigned char dualImage[FLAG_LEN];           // if 1, dual image
    unsigned char inactiveLen[FLAG_LEN];         // if 1, the image is INACTIVE; if 0, active
    unsigned char reserved[RESERVED_LEN];        // reserved for later use
    unsigned char imageValidationToken[TOKEN_LEN];// image validation token - can be crc, md5, sha;  for
                                                 // now will be 4 unsigned char crc
    unsigned char tagValidationToken[TOKEN_LEN]; // validation token for tag(from signiture_1 to end of // mageValidationToken)
} *PFILE_TAG;

FLASH_RESULT update(char* file,int check);
FLASH_RESULT http_update(char* name);
UINT32 CRC32(char* data,UINT32 size, UINT32 crc);
int checkChipId(char *strTagChipId, char *sig2);
int flashImage(char *imagePtr,int imageLen,UINT32 offset);
char global_crc_error[100];

void print_help(char* name)
{
  printf("Firmware update tool - Version 2\n\n"
	"usage: %s -u <file> [-r]\n"
	"       %s -c <file> \n"
        "       %s -H <name> [-r]\n"
	"       %s -h\n"
	"options:\n"
	"  -h           this help screen\n"
	"  -c <file>    check integrity of the firmware image file\n"
	"  -u <file>    update; write firmware image file to flash memory\n"
	"  -H <name>    Get the firmware image via HTTP with content type 'multipart',\n"
	"               as defined in RFC 1867 and section 5.1.1 of RFC 2046.\n"
	"               <name> is the name of the firmware image part.\n"
	"  -r           reboot after update\n",
	name,name,name,name);
}

int main(int argc,char* argv[])
{
  char* file=NULL;
  int check=1;
  int reboot=0;
  FLASH_RESULT ret;
  char** argvh=argv;
  int opt,noopt=1;
  char* name=NULL;

  while((opt=getopt(argc,argvh,"hrc:u:H:"))!=EOF)
  {
   noopt=0;
   switch(opt)
   {
     case 'c': // check
         file=optarg; 
 	 check=1;
         break;
     case 'u': // update
	 file=optarg;
	 check=0;
	 break;
     case 'H': // update via http
	 name=optarg;
	 check=0;
	 break;
     case 'r':
	 reboot=1;
	 break;
     case 'h':
     default:
	 print_help(argv[0]);
	 return 0;
	 break;
   }
  }
  if(noopt)
  {
    print_help(argv[0]);
    return 0;
  }

  if(name)
  {
    ret=http_update(name);
  }
  else
    ret=update(file,check);
  switch(ret)
  {
    case FLASH_OK:
	printf("OK\n");
	break;
    case FLASH_FAIL_CFM:
	printf("Failed to flash CFE\n");
	break;
    case FLASH_FAIL_ROOTFS:
	printf("Failed to flash RootFS or Kernel\n");
	break;
    case FLASH_CRC_ERROR:
	printf("File is corrupt - CRC error (%s)\n",global_crc_error);
	break;
    case FLASH_INCOMPATIBLE:
	printf("Incompatible formware version\n");
	break;
    case FLASH_WRONG_CHIP:
	printf("Image is for an other Board or Processor\n");
	break;
    case FLASH_FILE_ERROR:
	printf("File I/O error: %s\n",strerror(errno));
	break;
    case FLASH_FILE_EREAD:
	printf("Error reading image file\n");
	break;
    case FLASH_IMAGE_TO_BIG:
	printf("Image file is to big for the flash\n");
	break;
    case FLASH_NO_MEM:
	printf("Not enough memory\n");
	break;
    case FLASH_ERROR_READ_HTTP:
	printf("Error reading HTTP stream\n");
	break;
    case FLASH_ERROR_MULTIPART-MULTIPART_ERROR_NO_BOUNDARY:
	printf("HTTP(Multipart) upload error: boundary not set in HTTP header!\n");
	break;
    case FLASH_ERROR_MULTIPART-MULTIPART_ERROR_NO_BOUNDARY_VAR:
	printf("HTTP(Multipart) upload error: environment variable %s not set!\n",CONTENT);
	break;
    case FLASH_ERROR_MULTIPART-MULTIPART_ERROR_BOUNDARY_TO_LONG:
	printf("HTTP(Multipart) upload error: boundary longer than 70 characters!\n");
	break;
    case FLASH_ERROR_MULTIPART-MULTIPART_ERROR_PARSING:
	printf("HTTP(Multipart) upload error: parsing error occurred!\n");
	break;
    case FLASH_ERROR_MULTIPART-MULTIPART_ERROR_NOT_FOUND:
	printf("HTTP(Multipart) upload error: image part not found!\n");
	break;
    case FLASH_ERROR:
    default:
	printf("Unknown error\n");
	break;
  }
  if(reboot && !check && ret==FLASH_OK)
    sysMipsSoftReset();

  return ret;
}


FLASH_RESULT update(char* file,int check)
{
  int ret;
  struct stat filestat;
  int imageLen;
  char Buffer[BLOCK_SIZE];
  FILE* file_handle;
  int h;
  PFILE_TAG pTag;
  UINT32 crc,imageCRC;
  int tagVer, curVer, tagVerLast;
  int offset;

  if(file==NULL)
    return FLASH_ERROR;

  if(stat(file,&filestat)==-1)
    return FLASH_FILE_ERROR;
  imageLen=filestat.st_size;
  if(imageLen>sysFlashSizeGet())
    return FLASH_IMAGE_TO_BIG;

  file_handle=fopen(file,"r");
  if(file_handle==NULL)
    return FLASH_FILE_ERROR;

  ret=fread(Buffer,1,BLOCK_SIZE,file_handle);
  if(ret<BLOCK_SIZE)
    return FLASH_FILE_EREAD;
  pTag=(PFILE_TAG)Buffer;

  // check tag validate token first
  crc = CRC32_INIT_VALUE;
  crc = CRC32((char *)pTag,(UINT32)TAG_LEN-TOKEN_LEN,crc);
  if(crc!=(UINT32)(*(UINT32 *) (pTag->tagValidationToken)))
  {
    memset(global_crc_error,0,100);
    snprintf(global_crc_error,100,"token crc= 0x%08x, calculated crc= 0x%08x",(UINT32)(*(UINT32 *) (pTag->tagValidationToken)),crc);
    return FLASH_CRC_ERROR;
  }

  // only allow same or greater tag versions (up to tagVerLast - 26) to be uploaded
  tagVer = atoi(pTag->tagVersion);
  curVer = atoi(BCM_TAG_VER);
  tagVerLast = atoi(BCM_TAG_VER_LAST);

  if(tagVer < curVer || tagVer > tagVerLast)
  {
    //printf("Firmware tag version [%d] is not compatible with the current Tag version [%d]\n", tagVer, curVer);
    return FLASH_INCOMPATIBLE;
  }

  if(checkChipId(pTag->chipId,pTag->signiture_2)!= 0)
    return FLASH_WRONG_CHIP;


  // check image crc
  imageCRC=*((UINT32 *) (pTag->imageValidationToken));
  crc = CRC32_INIT_VALUE;
  crc = CRC32((char *)Buffer+TAG_LEN,BLOCK_SIZE-TAG_LEN,crc);
  h=imageLen-BLOCK_SIZE;
  while(h>0)
  {
    ret=fread(Buffer,1,BLOCK_SIZE,file_handle);
    h-=ret;
    crc = CRC32((char *)Buffer,ret,crc);
    if(feof(file_handle) || ferror(file_handle))
      break;
  }
  if(h>0)
    return FLASH_FILE_EREAD;

  if(crc!=imageCRC)
  {
    memset(global_crc_error,0,100);
    snprintf(global_crc_error,100,"image crc= 0x%08x, calculated crc= 0x%08x",imageCRC,crc);
    return FLASH_CRC_ERROR;
  }

  if(check)
  {
    fclose(file_handle);
    return FLASH_OK;
  }


  // flash image
  h=imageLen;
  fseek(file_handle,0,SEEK_SET);
  clearerr(file_handle);
  offset=0;
  fprintf(stderr,"start flashing image\nimage size=%d byte in %dx64k blocks\n",h,(h/BLOCK_SIZE)+(h%BLOCK_SIZE!=0));
  while(h>0)
  {
    ret=fread(Buffer,1,BLOCK_SIZE,file_handle);
    h-=ret;
    offset=flashImage(Buffer,ret,offset);
    fprintf(stderr,".");
    if(offset<0)
    {
      fclose(file_handle);
      return offset==-1?FLASH_FAIL_CFM:FLASH_FAIL_ROOTFS;
    }
    if(feof(file_handle) || ferror(file_handle))
      break;
  }
  if(h>0)
    return FLASH_FILE_EREAD;

  return FLASH_OK;
}

FLASH_RESULT http_update(char* name)
{
  int ret;
  int imageLen;
  char* Buffer;
  int h;
  PFILE_TAG pTag;
  UINT32 crc,imageCRC;
  int tagVer, curVer, tagVerLast;
  int offset;

  Buffer=(char*)malloc(BLOCK_SIZE*64); // 4MB Buffer for the whole image
  if(!Buffer)
    return FLASH_NO_MEM;

  h=0;
  do
  {
   ret=fread(Buffer+h,1,BLOCK_SIZE*64-h,stdin);
   h+=ret;
   if(ferror(stdin))
   {
     free(Buffer);
     return FLASH_ERROR_READ_HTTP;
   }
  }while(!feof(stdin));

  ret=getmultipart_image(Buffer,BLOCK_SIZE*64,name);
  if(ret<0)
  {
    return FLASH_ERROR_MULTIPART-ret;
  }
  imageLen=ret;

  pTag=(PFILE_TAG)Buffer;

  // check tag validate token first
  crc = CRC32_INIT_VALUE;
  crc = CRC32((char *)pTag,(UINT32)TAG_LEN-TOKEN_LEN,crc);
  if(crc!=(UINT32)(*(UINT32 *) (pTag->tagValidationToken)))
  {
    memset(global_crc_error,0,100);
    snprintf(global_crc_error,100,"token crc= 0x%08x, calculated crc= 0x%08x",(UINT32)(*(UINT32 *) (pTag->tagValidationToken)),crc);
    return FLASH_CRC_ERROR;
  }

  // only allow same or greater tag versions (up to tagVerLast - 26) to be uploaded
  tagVer = atoi(pTag->tagVersion);
  curVer = atoi(BCM_TAG_VER);
  tagVerLast = atoi(BCM_TAG_VER_LAST);

  if(tagVer < curVer || tagVer > tagVerLast)
  {
    //printf("Firmware tag version [%d] is not compatible with the current Tag version [%d]\n", tagVer, curVer);
    return FLASH_INCOMPATIBLE;
  }

  if(checkChipId(pTag->chipId,pTag->signiture_2)!= 0)
    return FLASH_WRONG_CHIP;


  // check image crc
  imageCRC=*((UINT32 *) (pTag->imageValidationToken));
  crc = CRC32_INIT_VALUE;
  crc = CRC32((char *)Buffer+TAG_LEN,imageLen-TAG_LEN,crc);
  if(crc!=imageCRC)
  {
    memset(global_crc_error,0,100);
    snprintf(global_crc_error,100,"image crc= 0x%08x, calculated crc= 0x%08x",imageCRC,crc);
    return FLASH_CRC_ERROR;
  }

  // flash image
  h=imageLen;
  offset=0;
  fprintf(stderr,"start flashing image\nimage size=%d byte in %dx64k blocks\n",h,(h/BLOCK_SIZE)+(h%BLOCK_SIZE!=0));

  offset=flashImage(Buffer,imageLen,offset);
  if(offset<0)
    return offset==-1?FLASH_FAIL_CFM:FLASH_FAIL_ROOTFS;

  return FLASH_OK;
}





static UINT32 CRC32_table[256] = {
    0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA,
    0x076DC419, 0x706AF48F, 0xE963A535, 0x9E6495A3,
    0x0EDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988,
    0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91,
    0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE,
    0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7,
    0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC,
    0x14015C4F, 0x63066CD9, 0xFA0F3D63, 0x8D080DF5,
    0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172,
    0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B,
    0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940,
    0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59,
    0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116,
    0x21B4F4B5, 0x56B3C423, 0xCFBA9599, 0xB8BDA50F,
    0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924,
    0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D,
    0x76DC4190, 0x01DB7106, 0x98D220BC, 0xEFD5102A,
    0x71B18589, 0x06B6B51F, 0x9FBFE4A5, 0xE8B8D433,
    0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818,
    0x7F6A0DBB, 0x086D3D2D, 0x91646C97, 0xE6635C01,
    0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E,
    0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457,
    0x65B0D9C6, 0x12B7E950, 0x8BBEB8EA, 0xFCB9887C,
    0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65,
    0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2,
    0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB,
    0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0,
    0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9,
    0x5005713C, 0x270241AA, 0xBE0B1010, 0xC90C2086,
    0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
    0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4,
    0x59B33D17, 0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD,
    0xEDB88320, 0x9ABFB3B6, 0x03B6E20C, 0x74B1D29A,
    0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683,
    0xE3630B12, 0x94643B84, 0x0D6D6A3E, 0x7A6A5AA8,
    0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1,
    0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE,
    0xF762575D, 0x806567CB, 0x196C3671, 0x6E6B06E7,
    0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC,
    0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5,
    0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252,
    0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B,
    0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60,
    0xDF60EFC3, 0xA867DF55, 0x316E8EEF, 0x4669BE79,
    0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236,
    0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F,
    0xC5BA3BBE, 0xB2BD0B28, 0x2BB45A92, 0x5CB36A04,
    0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D,
    0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A,
    0x9C0906A9, 0xEB0E363F, 0x72076785, 0x05005713,
    0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38,
    0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21,
    0x86D3D2D4, 0xF1D4E242, 0x68DDB3F8, 0x1FDA836E,
    0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777,
    0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C,
    0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45,
    0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2,
    0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB,
    0xAED16A4A, 0xD9D65ADC, 0x40DF0B66, 0x37D83BF0,
    0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
    0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6,
    0xBAD03605, 0xCDD70693, 0x54DE5729, 0x23D967BF,
    0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94,
    0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D
};

UINT32 CRC32(char* data,UINT32 size, UINT32 crc) {
   while (size-- > 0)
      crc = (crc >> 8) ^ CRC32_table[(crc ^ *data++) & 0xff];
   return crc;
}

int checkChipId(char *strTagChipId, char *sig2)
{
  int tagChipId=0;
  unsigned int chipId=(int)sysGetChipId();
  int result=0;

  if(strstr(sig2, "Firmware"))
    return result;  // skip the pre 2_14L02 release, where the signiture_2 is "Firmware version 1.0"

  tagChipId=atoi(strTagChipId);

  switch (chipId)
  {
   case 0x6338:
     if(!(tagChipId == 6338))
       result=1;
     break;
   case 0x6345:
     if(!(tagChipId == 6345 || tagChipId == 6335))
       result=1;
     break;
   case 0x6348:
     if(tagChipId!=6348)
       result=-1;
     break;
   default:
     result=-1;
     break;
  }
  return result;
}

int flashImage(char *imagePtr,int imageLen,UINT32 offset)
{
  PFILE_TAG pTag = (PFILE_TAG) imagePtr;
  static int cfeSize, rootfsSize, kernelSize;
  static unsigned long kernelAddr,rootfsAddr,cfeAddr;
/*
  int tmplen = 0;
  char *tmpptr;
  FLASH_RESULT status = FLASH_OK;
  int block[2 * BLOCK_SIZE];
  int left_len=0;
*/
  int size;

  if(offset==0)
  {
    cfeSize = rootfsSize = kernelSize = 0;

    // check cfe's existence
    cfeAddr=(unsigned long) strtoul(pTag->cfeAddress, NULL, 10);
    cfeSize=atoi(pTag->cfeLen);

    // check kernel existence
    kernelAddr=(unsigned long) strtoul(pTag->kernelAddress, NULL,10);
    kernelSize=atoi (pTag->kernelLen);

    // check root filesystem existence
    rootfsAddr=(unsigned long)strtoul(pTag->rootfsAddress, NULL,10);
    rootfsSize = atoi (pTag->rootfsLen);

    if(cfeAddr && cfeSize)
    {
      imagePtr+=TAG_LEN;
      imageLen-=TAG_LEN;
    }
  }

  if(cfeAddr && (cfeSize-offset>0))
  {
    //Flashing CFE...
    size=cfeSize-offset;
    if ((sysFlashImageSet(imagePtr,size,(int)cfeAddr+offset,BCM_IMAGE_CFE))==-1)
	{
	  //Failed to flash CFE
	  return -1;
	}
    imageLen-=size;
    imagePtr+=size;
    offset+=size;
  }
  if(rootfsAddr && kernelAddr && imageLen>0)
  {
#ifdef BOARD_96332CG
    if((sysFlashImageSet(imagePtr,imageLen,(int)(rootfsAddr-TAG_LEN+offset-cfeSize),BCM_IMAGE_WHOLE))==-1)
#else
    if((sysFlashImageSet(imagePtr,imageLen,(int)(rootfsAddr-TAG_LEN+offset-cfeSize),BCM_IMAGE_TECOM))==-1)
#endif
    {
      //Failed to flash root file system
      return -2;
    }
    offset+=imageLen;
  }
 
  return offset;
}
