#ifndef MULTIPART_H
#define MULTIPART_H

#define CONTENT "CONTENT_TYPE"


#define MULTIPART_ERROR_NO_BOUNDARY 		-1
#define MULTIPART_ERROR_NO_BOUNDARY_VAR 	-2
#define MULTIPART_ERROR_BOUNDARY_TO_LONG 	-3
#define MULTIPART_ERROR_PARSING			-4
#define MULTIPART_ERROR_NOT_FOUND		-5


int getmultipart_image(char* imgbuffer,int imgbuffer_len,char* name);

#endif // MULTIPART_H
