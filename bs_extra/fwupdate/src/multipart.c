/*
 * Copyright (C) 2008  Michael Finsterbusch <fibu@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

#include "multipart.h"

#define BOUND_LEN 80
#define BUFFER_LEN 1024

#define NOT_FOUND -1
#define FOUND_DIFFERENT -2


int find_boundary(char* buffer,int buflen,char* boundary)
{
  char* h=buffer;
  int bound_len,cmp=1;

  bound_len=strlen(boundary);
  while(buflen>=bound_len)
  {
    cmp=memcmp(buffer,boundary,bound_len);
    if(cmp==0)
      break;
    buflen--;
    buffer++;
  }
  if(cmp!=0)
    return NOT_FOUND;

  return buffer-h;
}

int find_name(char* buffer,int buflen,char* name)
{
  char* str,*hname;

  str=strstr(buffer,"name=\"");
  if(!str)
    return NOT_FOUND;

  if((buflen-(str-buffer))<6)
    return NOT_FOUND;
  str+=6;
  hname=str;
  str=strstr(hname,"\"");
  if(!str)
    return NOT_FOUND;

  // found the name?
  if(strncmp(name,hname,strlen(name))!=0)
    return FOUND_DIFFERENT;
  return str-buffer;
}


int find(char* buffer,int buflen,char* boundary,char* name)
{
  char* str;
  char* data;
  int datalen,bound_len,err;
  char* buf=buffer;
  int b_len=buflen;

  bound_len=strlen(boundary);

  while((err=find_boundary(buf,b_len,boundary))!=NOT_FOUND)
    {
      // find name
      b_len-=err;
      buf+=err;
      err=find_name(buf,b_len,name);
      if(err==NOT_FOUND) 
	continue;

      if(err==FOUND_DIFFERENT)
      {
	buf+=1;
	b_len-=1;
        continue;
      }
      buf+=err;
      b_len-=err;

      // get body data
      str=strstr(buf,"\r\n\r\n");
      data=str+4;


      b_len-=data-buf;
      buf=data;
      if(b_len>=bound_len)
      {
	err=find_boundary(buf,b_len,boundary);
	if(err==NOT_FOUND)
	  return MULTIPART_ERROR_PARSING;
	buf+=err;
	b_len-=err;
	datalen=(buf-data)-2; //<CR><LF>
      }
      else
	return MULTIPART_ERROR_PARSING;
      
      // copy image data to the beginning of the buffer
      memmove(buffer,data,datalen);
      return datalen;
    }
  return MULTIPART_ERROR_NOT_FOUND;
}

int getmultipart_image(char* imgbuffer,int imgbuffer_len,char* name)
{
  char* con_type;
  char boundary[BOUND_LEN];
  char* str,*str2;

  con_type=getenv(CONTENT);
  if(!con_type)
    {
      //printf("ERROR: environment variable %s not set\n",CONTENT);
      return MULTIPART_ERROR_NO_BOUNDARY_VAR;
    }
  
  str=strstr(con_type,"boundary=");
  if(!str)
    {
      //printf("ERROR: boundary not set\n");
      return MULTIPART_ERROR_NO_BOUNDARY;
    }

  str+=9;
  if(*str=='"')
    str++;

  str2=str;
  str2+=strlen(str);

  str2--;
  if(*str2=='"')
    str2--;

  if(str2-str>70)
    {
      //printf("ERROR: boundary longer than 70 characters\n");
      return MULTIPART_ERROR_BOUNDARY_TO_LONG;
    }

  memset(boundary,0,BOUND_LEN);
  boundary[0]=boundary[1]='-';
  strncpy(boundary+2,str,str2-str+1);

#ifdef DEBUG
  printf("boundary=%s\n",boundary);
#endif

  return find(imgbuffer,imgbuffer_len,boundary,name);
}
