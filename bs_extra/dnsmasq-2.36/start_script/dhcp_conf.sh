#!/bin/sh

DHCP_CONF="/var/dhcp.dnsmasq"

must_save=0

start=`nvram get dhcp_start`
if [ "$start" = "Failed" ]; then

 gateway=`ifconfig br0|grep "inet addr"|sed -n -e 's/^.*addr:\([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\).*Bcast.*/\1/p'`
 fromip=`echo $gateway|sed -e 's/\([0-9]*\.[0-9]*\.[0-9]*\.\)[0-9]*/\1100/'`
 toip=`echo $gateway|sed -e 's/\([0-9]*\.[0-9]*\.[0-9]*\.\)[0-9]*/\1150/'`

 nvram set dhcp_start=1
 nvram set dhcp_fromip=$fromip
 nvram set dhcp_toip=$toip
 nvram set dhcp_lease=12
 nvram set dhcp_lease_t=h
 nvram set dhcphosts=""
 nvram set dhcp_gateway=$gateway
 nvram set dhcp_dns1=$gateway
 nvram set dhcp_dns2=""
 nvram set dhcp_dns3=""
 nvram set dhcp_dns4=""
 must_save=1

 start=1
fi

if [ "$start" != "1" ]; then
 echo "" > $DHCP_CONF
 [ $must_save -eq 1 ] && [ "$STARTUP" != "1" ] && /bin/nvram commit >/dev/null
 exit 0
fi

fromip=`nvram get dhcp_fromip`
[ "$fromip" = "Failed" ] && exit 1

toip=`nvram get dhcp_toip`
[ "$toip" = "Failed" ] && exit 1

dhcp_static=`nvram get dhcp_static`
[ "$dhcp_static" = "1" ] && toip="static"

lease=`nvram get dhcp_lease`
[ "$lease" = "Failed" ] && exit 1

lease_t=`nvram get dhcp_lease_t`
[ "$lease_t" = "Failed" ] && exit 1
[ "$lease" = "inf" ] && lease_t=""

dhcphosts=`nvram get dhcphosts`
[ "$dhcphosts" = "Failed" ] && dhcphosts=""

gateway=`nvram get dhcp_gateway`
[ "$gateway" = "Failed" ] && gateway=""

dns1=`nvram get dhcp_dns1`
[ "$dns1" = "Failed" ] && dns1=""
dns2=`nvram get dhcp_dns2`
[ "$dns2" = "Failed" ] && dns2=""
dns3=`nvram get dhcp_dns3`
[ "$dns3" = "Failed" ] && dns3=""
dns4=`nvram get dhcp_dns4`
[ "$dns4" = "Failed" ] && dns4=""

> $DHCP_CONF

dhcp_custom_start=`/bin/nvram get dhcp_custom_start`
if [ "$dhcp_custom_start" != "Failed" ]; then
   custom_start=$dhcp_custom_start
else
   /bin/nvram set dhcp_custom_start=0
   must_save=1
   custom_start="0"
fi
if [ "$custom_start" = "1" ]; then
  /bin/nvram getfile dhcp_custom=/var/dhcp_custom
  cat /var/dhcp_custom >> $DHCP_CONF
fi

echo "dhcp-range=${fromip},${toip},${lease}${lease_t}" >> $DHCP_CONF

echo $dhcphosts|sed -e 's/~/\n/g'|sed -n -e '/^$/!p'|sed -e 's/\(^[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]\)\(.*\)/dhcp-host=\1\2/' >> $DHCP_CONF

[ "$gateway" != "" ] && echo "dhcp-option=3,${gateway}" >> $DHCP_CONF

( [ "$dns1" != "" ] || [ "$dns2" != "" ] || [ "$dns3" != "" ] || [ "$dns4" != "" ] ) && echo -n "dhcp-option=6" >> $DHCP_CONF
[ "$dns1" != "" ] && echo -n ",${dns1}" >> $DHCP_CONF
[ "$dns2" != "" ] && echo -n ",${dns2}" >> $DHCP_CONF
[ "$dns3" != "" ] && echo -n ",${dns3}" >> $DHCP_CONF
[ "$dns4" != "" ] && echo -n ",${dns4}" >> $DHCP_CONF
echo "" >> $DHCP_CONF

echo -e "dhcp-lease-max=50"'\n'"dhcp-leasefile=/var/dnsmasq.leases"'\n'"dhcp-authoritative"'\n'"cache-size=50"'\n' >> $DHCP_CONF

[ $must_save -eq 1 ] && [ "$STARTUP" != "1" ] && /bin/nvram commit >/dev/null
