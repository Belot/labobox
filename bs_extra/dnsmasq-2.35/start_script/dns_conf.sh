#!/bin/sh

DNS_CONF="/var/dns.dnsmasq"
HOSTS="/var/hosts"

must_save=0
start=`nvram get dns_start`
if [ "$start" = "Failed" ]; then

 nvram set dns_start=1
 nvram set dns_domain="lan"
 nvram set dnshosts=""
 must_save=1
 start=1
fi

if [ "$start" != "1" ]; then
 echo "" > $DNS_CONF
 [ $must_save -eq 1 ] && [ "$STARTUP" != "1" ] && /bin/nvram commit >/dev/null
 exit 0
fi

domain=`nvram get dns_domain`
[ "$domain" = "Failed" ] && domain="lan"

dnshosts=`nvram get dnshosts`
[ "$dnshosts" = "Failed" ] && dnshosts=""

filtwin2k=`nvram get dns_filterwin2k`
filterwin2k="filterwin2k"
[ "$filtwin2k" = "0" ] && filterwin2k=""


> $DNS_CONF

dns_custom_start=`/bin/nvram get dns_custom_start`
if [ "$dns_custom_start" != "Failed" ]; then
   custom_start=$dns_custom_start
else
   /bin/nvram set dns_custom_start=0
   must_save=1
   custom_start="0"
fi
if [ "$custom_start" = "1" ]; then
  /bin/nvram getfile dns_custom=/var/dns_custom
  cat /var/dns_custom >> $DNS_CONF
fi

echo "local=/${domain}/" >> $DNS_CONF
echo "domain=${domain}" >>$DNS_CONF

echo -e "domain-needed"'\n'"bogus-priv"'\n'"${filterwin2k}"'\n'"addn-hosts=${HOSTS}"'\n'"expand-hosts"'\n'"resolv-file=/etc/resolv.conf"'\n' >> $DNS_CONF

IP=`ifconfig br0|grep "inet addr"|sed -n -e 's/^.*addr:\([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\).*Bcast.*/\1/p'`
echo "127.0.0.1 localhost" > $HOSTS
echo "$IP `cat /proc/sys/kernel/hostname`" >> $HOSTS
echo $dnshosts|sed -e 's/~/\n/g'|sed -n -e '/^$/!p'|sed -e 's/^\(.*\),\(.*\)$/\1 \2/' >> $HOSTS

[ $must_save -eq 1 ] && [ "$STARTUP" != "1" ] && /bin/nvram commit >/dev/null
