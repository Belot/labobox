#ifndef __REQERROR_H
#define __REQERROR_H
#define WEBSERVER_AUTH_ERROR 1
#define PROXY_AUTH_ERROR 2
#define DNS_ERROR 3
#define CONNECT_ERROR 4
#define NOTSUPPORTED_ERROR 5
#define MAXCLIENTS_ERROR 6
#define SYNTAX_ERROR 7
#define INTERNAL_ERROR 8
struct SOCK;
void request_error(struct SOCK *client, int error, char *param);
#endif
