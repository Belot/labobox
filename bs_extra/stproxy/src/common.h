#ifndef __COMMON_H
#define __COMMON_H

/* bind settings */
#define PROXY_PORT 1337
#define ADMIN_PORT 5000
#define BIND_ADDRESS "0.0.0.0"

/* performance settings */
#define MAX_CLIENTS 500
#define FD_SETSIZE 1024
#define BUF_LENGTH 8192

/* admin settings */
#define ADMIN_USER "stproxy"
#define ADMIN_PASS "stproxy"
#define ADMIN_MASK "127.0.0.1", NULL

/* user settings */
#define MAX_USERNAME_LENGTH 15
#define MAX_PASSWORD_LENGTH 15
#define MAX_IP_LENGTH 15
#define MAX_IP_COUNT 10

/* timeout settings */ 
#define CONNECT_TIMEOUT 30
#define REQUEST_TIMEOUT 10
#define IDLE_TIMEOUT 120
#define SELECT_TIMEOUT 2

/* dns settings */
#define DNS_SERVER "127.0.0.1"
#define DNS_PORT 53
#define CACHE_SIZE 4999
#define MAX_QUERIES 100
#define MAX_TRIES 3
#define QUERY_TIMEOUT 10

/* other settings */
#define PROXY_VERSION "stproxy/0.9"
#define MAX_LINE_LENGTH 1024
#define MAX_AUTH_LENGTH 64
#define MAX_ENCRYPTED_PASSWORD_LENGTH 120
#define CONFIG_FILE "stproxy.conf"
#define PASSWD_FILE "stproxy_passwd"

#define UNKNOWN_STATE 0
#define CLIENT_STATE 1
#define SERVER_CONNECTED_STATE 2
#define SERVER_CONNECTING_STATE 3
#define PROXY_SERVER_STATE 4
#define WEB_SERVER_STATE 5
#define UNKNOWN_METHOD 0
#define GET_METHOD 1
#define POST_METHOD 2
#define CONNECT_METHOD 3

#define string(s) str_func(s)
#define str_func(s) #s


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#ifdef HAVE_STDIO_H
#include <stdio.h>
#endif

#ifdef HAVE_INTTYPES_H
#include <inttypes.h>
#endif

#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#else
#ifdef HAVE_MALLOC_H
#include <malloc.h>
#endif
#endif

#ifdef DEBUG
#ifdef HAVE_STDARG_H
#include <stdarg.h>
#endif
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

#ifdef TIME_WITH_SYS_TIME
#include <sys/time.h>
#include <time.h>
#else
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#else
#include <time.h>
#endif
#endif

#ifdef HAVE_CTYPE_H
#include <ctype.h>
#endif

#ifdef HAVE_SIGNAL_H
#include <signal.h>
#endif

#ifdef HAVE_ERRNO_H
#include <errno.h>
#endif

#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif

#ifdef HAVE_SYS_STAT_H
#include <sys/stat.h>
#endif

#ifdef HAVE_SYS_SELECT_H
#include <sys/select.h>
#endif

#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif

#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif

#ifdef HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif

#ifdef HAVE_VFORK_H
#include <vfork.h>
#endif

#ifndef HAVE_WORKING_VFORK
#define vfork fork
#endif

#ifndef INADDR_NONE
#define INADDR_NONE ((unsigned long) -1)
#endif

#endif
