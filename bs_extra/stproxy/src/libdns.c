/*
 * Copyright (C) 2007 Adam Hurkala (ahurkala@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "common.h"
#include "libdns.h"
#include "debug.h"
#include "sock.h"
#include "strutils.h"

struct dnsrequest
{
   uint16_t id;
   query_callback callback;
   int arg;
   struct dnsrequest *next;
};

struct dnsquery
{
   struct dnsrequest request;
   char *name;
   int attempt;
   time_t time;
   int sent;
   unsigned long hash;
   struct dnsquery *next;
   struct dnsquery *prev;
   struct dnsrequest *requesttail;
};

struct dnscache
{
   char *name;
   struct in_addr in;
   uint16_t ttl;
   time_t querytime;
};

static int idcount;
static int querycount;
static char idtab[65536];
static struct dnsquery *queryhead, *querytail;
static int dnssocket;
static struct sockaddr_in ns;
static struct dnscache *cache;

static unsigned long get_hash(unsigned char *str)
{
   unsigned long hash = 5381;
   int c;

   while ((c = *str++)) hash = ((hash << 5) + hash) + c;
   return hash;
}

static void cache_add(char *name, unsigned long hash, struct in_addr in, int ttl)
{
   unsigned long index;

   print("Adding %s->%s (TTL %u) to cache", name, inet_ntoa(in), ttl);
   index = hash % CACHE_SIZE;
   if (cache[index].name) free(cache[index].name);
   cache[index].name = name;
   cache[index].in = in;
   cache[index].ttl = ttl;
   time(&cache[index].querytime);
}

static int cache_get(char *name, unsigned long hash, struct in_addr *in)
{
   unsigned long index;

   index = hash % CACHE_SIZE;
   if (cache[index].name && strcmp(cache[index].name, name) == 0 && difftime(time(NULL), cache[index].querytime) <= cache[index].ttl)
   {
      *in = cache[index].in;
      return 0;
   }
   return ECACHEMISS;
}

static int get_id(uint16_t *id)
{
   int i, j;

   if (idcount == 65536) return -1;
   i = random() % 65536;
   if (idtab[i] == 0)
   {
      idtab[i] = 1;
      *id = i;
      idcount++;
      return 0;
   }
   for (j = i + 1; j < 65536; j++)
   {
      if (idtab[j] == 0)
      {
         idtab[j] = 1;
         *id = j;
         idcount++;
         return 0;
      }
   }
   for (j = i - 1; j >= 0; j--)
   {
      if (idtab[j] == 0)
      {
         idtab[j] = 1;
         *id = j;
         idcount++;
         return 0;
      }
   }
   return -1;
}

static void release_id(uint16_t id)
{
   idtab[id] = 0;
   idcount--;
}

static int udp_create_packet(uint16_t id, char *name, char *buffer)
{
   char *ptr, *tmp, *packet = buffer;
   int len;

   print("Creating UDP packet (%s)", name);
   *(uint16_t *)packet = htons(id);
   packet += 2;
   *(uint16_t *)packet = htons(0x0100);
   packet += 2;
   *(uint16_t *)packet = htons(1);
   packet += 2;
   *(uint16_t *)packet = 0;
   packet += 2;
   *(uint16_t *)packet = 0;
   packet += 2;
   *(uint16_t *)packet = 0;
   packet +=2;
   tmp = name;
   while ((ptr = strchr(tmp, '.')))
   {
      len = ptr - tmp;
      *packet++ = len;
      memcpy(packet, tmp, len);
      packet += len;
      tmp = ptr + 1;
   }
   len = strlen(tmp);
   *packet++ = len;
   memcpy(packet, tmp, len);
   packet += len;
   *packet++ = '\0';
   *packet++ = 0;
   *packet++ = 1;
   *packet++ = 0;
   *packet++ = 1;
   return packet - buffer;
}

static int udp_parse_packet(char *packet, int packetlen, struct in_addr *in, uint16_t *id, uint32_t *ttl)
{
   uint16_t flags, type, rdlength, ancount;
   char qr, rcode, ra;
   int offset, tmp;

   print("Parsing UDP packet");
   if (packetlen < 13) return EPROTOCOL;
   *id = ntohs(*(uint16_t *)packet);
   packet += 2;
   flags = ntohs(*(uint16_t *)packet);  
   qr = (char)((flags & 0x8000) >> 15);
   if (!qr) return ENORESPONSE;
   ra = (char)((flags & 0x0080) >> 7);
   if (!ra) return ERECURSIVEQUERY;
   rcode = (char)(flags & 0x000F);
   if (rcode)
   {
      switch(rcode)
      {
         case 1: return EFORMAT;
         case 2: return ESERVERFAILURE;
         case 3: return ENAMEERROR;
         case 4: return ENOTSUPPORTED;
         case 5: return EREFUSED;
         default: return EUNKNOWN;
      }
   }
   packet += 4;
   ancount = ntohs(*(uint16_t *)packet);
   if (ancount == 0) return ENORESPONSE;
   packet += 6;
   offset = 12;
   while (*packet)
   {
      tmp = *packet + 1;
      offset += tmp;
      packet += tmp;
      if (tmp < 2 || tmp > 64 || offset >= packetlen) return EPROTOCOL;
   }
   offset += 5;
   packet += 5;
   if (offset >= packetlen) return EPROTOCOL;
   for (;;)
   {
      if (!((*packet) & 0xC0))
      {
         while (*packet)
         {
            tmp = *packet + 1;
            offset += tmp;
            packet += tmp;
            if (tmp < 2 || tmp > 64 || offset >= packetlen) return EPROTOCOL;
         }
         packet--;
         offset--;
      }
      offset += 2;
      packet += 2;
      if (offset >= packetlen - 1) return EPROTOCOL;
      type = *packet++ << 8;
      type |= *packet++;
      offset += 4;
      packet += 2;
      if (offset >= packetlen - 3) return EPROTOCOL;
      *ttl = (uint32_t)*packet++ << 24;
      *ttl |= (uint32_t)*packet++ << 16;
      *ttl |= (uint32_t)(*packet++ << 8);
      *ttl |= *packet++;
      offset += 4;
      if (offset >= packetlen - 1) return EPROTOCOL;
      rdlength = *packet++ << 8;
      rdlength |= *packet++;
      offset += 2;
      if (type == 1)
      {
         if (rdlength != sizeof(struct in_addr) || offset + rdlength > packetlen) return EPROTOCOL;
         memcpy(in, packet, sizeof(struct in_addr));
         return 0;
      }
      offset += rdlength;
      packet += rdlength;
      if (offset >= packetlen) return EPROTOCOL;
   }
   return EPROTOCOL;
}

static void udp_receive_packet(void)
{
   struct sockaddr_in sa;
   struct in_addr in;
   struct dnsquery *query;
   struct dnsrequest *request, *tmp;
   int res, count;
   socklen_t len; 
   uint16_t id;
   uint32_t ttl;
   static char buffer[512];

   len = sizeof(sa);
   count = recvfrom(dnssocket, buffer, sizeof(buffer), 0, (struct sockaddr *)&sa, &len);
   if (count == -1) return;
   print("Got DNS response from %s", inet_ntoa(sa.sin_addr)); 
   res = udp_parse_packet(buffer, count, &in, &id, &ttl);
   print("UDP packet parsed (return code %d)", res);
   if (res == EPROTOCOL) return;

   query = queryhead;
   while (query)
   {
      if (query->request.id == id)
      {
         if (res == 0) cache_add(query->name, query->hash, in, ttl);
         if (querycount > 0) querycount--;
         release_id(id);
         query->request.callback(query->request.id, res, query->name, res == 0 ? &in : NULL, query->request.arg);
         request = query->request.next;
         while (request)
         {
            release_id(request->id);
            request->callback(request->id, res, query->name, res == 0 ? &in : NULL, request->arg);
            tmp = request->next;
            free(request);
            request = tmp;
         }
         if (query->prev) query->prev->next = query->next;
         else queryhead = query->next;
         if (query->next) query->next->prev = query->prev;
         else querytail = query->prev;
         free(query);
         return;
      }
      query = query->next;
   }
}

static void udp_send_packet(void)
{
   struct dnsquery *query;
   static char buffer[512];
   int count;

   query = queryhead;
   if (!query) return;
   count = udp_create_packet(query->request.id, query->name, buffer);
   if (sendto(dnssocket, buffer, count, 0, (struct sockaddr *)&ns, sizeof(ns)) != -1)
   {
      print("UDP packet sent to %s (resolving %s)", inet_ntoa(ns.sin_addr), query->name);
      querycount++;
      query->attempt++;
      query->sent = 1;
      time(&query->time);
      if (query->next)
      {
         queryhead = query->next;
         queryhead->prev = NULL;
         querytail->next = query;
         query->prev = querytail;
         query->next = NULL;
         querytail = query;
      }
   }
   else printerror("UDP sendto failed");
}

static struct sockaddr_in get_name_server(void)
{
   FILE *f;
   struct sockaddr_in sa;

   print("Getting name server from /etc/resolv.conf");
   memset(&sa, 0, sizeof(struct sockaddr_in));
   sa.sin_family = AF_INET;
   sa.sin_port = htons(DNS_PORT);
   sa.sin_addr.s_addr = inet_addr(DNS_SERVER);
   f = fopen("/etc/resolv.conf", "r");
   if (f)
   {
      int len;
      in_addr_t tmp;
      char buffer[MAX_LINE_LENGTH+1], *s;
    
      while (fgets(buffer, MAX_LINE_LENGTH + 1, f))
      {
         len = strlen(buffer);
         init_tokenizer(buffer, NULL, len);
         if (!parse_line())
         {
            if (feof(f))
            {
               init_tokenizer(buffer, NULL, len + 1);
               parse_string();
            }
            else
            {
               print("/etc/resolv.conf parse error");
               break;
            }
         }
         if ((s = get_token()) && strcmp(s, "nameserver") == 0)
         {
            if ((s = get_token()) && (tmp = inet_addr(s)) != INADDR_NONE) sa.sin_addr.s_addr = tmp;
            else print("/etc/resolv.conf parse error");
            break;
         }
      }
      if (ferror(f)) printerror("Error reading /etc/resolv.conf");
      fclose(f);
   }
   return sa;
}

int gethostbyname_a(char *name, query_callback callback, int arg, uint16_t *queryid, struct in_addr *inp)
{
   struct dnsquery *query;
   int len;
   uint16_t id;
   unsigned long hash;

   len = strlen(name);
   if (len < 1 || len > MAX_HOST_LENGTH) return EHOSTLENGTH;
   hash = get_hash((unsigned char *)name);
   if (cache_get(name, hash, inp) == 0) return 0;
   if (get_id(&id) != 0) return EQUERYCOUNTEXCEEDED;
   
   query = queryhead;
   while (query)
   {
      if (query->hash == hash && strcmp(query->name, name) == 0)
      {
         struct dnsrequest *request;
         
         request = (struct dnsrequest *)malloc(sizeof(struct dnsrequest));
         if (!request)
         {
            release_id(id);
            return ENOMEMORY;
         }
         request->id = id;
         request->callback = callback;
         request->arg = arg;
         request->next = NULL;
         query->requesttail->next = request;
         query->requesttail = request;
         *queryid = id;
         return EQUERYINPROGRESS;
      }
      query = query->next;
   }

   query = (struct dnsquery *)malloc(sizeof(struct dnsquery));
   if (!query)
   {
      release_id(id);
      return ENOMEMORY;
   }
   query->name = (char *)malloc((len + 1) * sizeof(char));
   if (!query->name)
   {
      release_id(id);
      free(query);
      return ENOMEMORY;
   }
   strcpy(query->name, name);
   query->request.id = id;
   query->request.callback = callback;
   query->request.arg = arg;
   query->request.next = NULL;
   query->attempt = 0;
   time(&query->time);
   query->sent = 0;
   query->hash = hash;
   query->requesttail = &query->request; 
   if (!queryhead) queryhead = query;
   else querytail->next = query;
   query->next = NULL;
   query->prev = querytail;
   querytail = query;
   *queryid = id;
   return EQUERYINPROGRESS;
}

int libdns_init(struct sockaddr_in *dns)
{
   int i;

   srandom(time(NULL));
   dnssocket = socket(PF_INET, SOCK_DGRAM, 0);
   if (dnssocket < 0) return ESOCKET;
   if (set_noblocking(dnssocket) != 0)
   {
      close(dnssocket);
      return ENONBLOCK;
   }
   cache = (struct dnscache *)malloc(CACHE_SIZE * sizeof(struct dnscache));
   if (!cache)
   {
      close(dnssocket);
      return ENOMEMORY;
   }
   for (i = 0; i < CACHE_SIZE; i++) cache[i].name = NULL;
   if (dns) ns = *dns;
   else ns = get_name_server();
   print("Using name server %s:%d", inet_ntoa(ns.sin_addr), DNS_PORT);
   return 0;
}

void libdns_free(void)
{
   struct dnsquery *query;
   struct dnsrequest *request;
   int i;

   for (i = 0; i < 65536; i++) idtab[i] = 0;
   for (i = 0; i < CACHE_SIZE; i++) if (cache[i].name) free(cache[i].name);
   free(cache);
   idcount = 0;
   while (queryhead)
   {
      request = queryhead->request.next;
      while(request)
      {
         struct dnsrequest *tmp = request->next;
         free(request);
         request = tmp;
      } 
      query = queryhead;
      queryhead = queryhead->next;
      free(query->name);
      free(query);
   }
   querytail = NULL;
   querycount = 0;
   close(dnssocket);
}

void libdns_setdns(struct sockaddr_in dns)
{
   ns = dns;
}

int libdns_fds(fd_set *read_fds, fd_set *write_fds)
{
   struct dnsquery *query;
   int res = 0;

   query = queryhead;
   if (query)
   {
      FD_SET(dnssocket, read_fds);
      res = dnssocket + 1;
   }
   while (query)
   {
      if (querycount < MAX_QUERIES && (query->attempt == 0 || (query->attempt < MAX_TRIES && difftime(time(NULL), query->time) >= QUERY_TIMEOUT)))
      {
         FD_SET(dnssocket, write_fds);
         res = dnssocket + 1;
         break;
      }
      query = query->next;
   }
   return res;
}

void libdns_process_queue(void)
{
   struct dnsquery *query, *tmp1;
   struct dnsrequest *request, *tmp2;

   query = queryhead;
   while (query)
   {
      if (difftime(time(NULL), query->time) >= QUERY_TIMEOUT)
      {
         if (query->sent)
         {
            if (querycount > 0) querycount--;
            query->sent = 0;
         }
         if (query->attempt == MAX_TRIES)
         {
            release_id(query->request.id);
            query->request.callback(query->request.id, EQUERYTIMEOUT, query->name, NULL, query->request.arg);
            request = query->request.next;
            while (request)
            {
               release_id(request->id);
               request->callback(request->id, EQUERYTIMEOUT, query->name, NULL, request->arg);
               tmp2 = request->next;
               free(request);
               request = tmp2;
            }
            if (query->prev) query->prev->next = query->next;
            else queryhead = query->next;
            if (query->next) query->next->prev = query->prev;
            else querytail = query->prev;
            free(query->name);
            tmp1 = query->next;
            free(query);
            query = tmp1;
            continue;
         }
      }
      query = query->next;
   }
}

void libdns_process(fd_set *readfds, fd_set *writefds)
{
   if (FD_ISSET(dnssocket, readfds)) udp_receive_packet();
   if (FD_ISSET(dnssocket, writefds)) udp_send_packet();
   libdns_process_queue();
}
