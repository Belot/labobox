#ifndef __DEBUG_H
#define __DEBUG_H
#ifdef DEBUG
void __print(char *file, int line, char *format, ...);
void __printerror(char *file, int line, char *format, ...);
#define print(args...) __print(__FILE__, __LINE__, args)
#define printerror(args...) __printerror(__FILE__, __LINE__, args)
#else
#define print(args...) (void)(0)
#define printerror(args...) (void)(0)
#endif
#endif
