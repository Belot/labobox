#ifndef __AUTH_H
#define __AUTH_H
#define SUPER_USER 0
#define NORMAL_USER 1
#define ENOMEMORY -1
#define EUSERNAMELENGTH -2
#define EPASSWORDLENGTH -3
#define EIPLENGTH -4
#define EIPCOUNT -5
#define EUSEREXISTS -6
#define EIPEXISTS -7
#define EIPMASK -8
#define EUSERNOTFOUND -9
#define EIPNOTFOUND -10
#define EBADUSERNAME -11
#define EOPENDB -12
#define EADDTODB -13
#define ECORRUPTDB -14
#define EREADDB -15
#define EUPDATEDB -16
#define MAX_CMD_PARAMS 10

struct user
{
   char *username;
   char *password;
   char **ipmask;
   int authority;
   struct user *next;
   struct user *prev;
};
int register_update_cmd(char *cmd_string);
void exec_update_cmd();

int load_db(char *passwd_file);
int update_db(int rebuild);
void free_db(void);
int add_ip(char *username, char *ip);
int delete_ip(char *username, char *ip);
int get_user(char *username, struct user **user);
int add_user(char *username, char *password, int authority, int encrypt);
int delete_user(char *username);
int change_password(char *username, char *password);
int authorized_connection(char *ip);
int authorized_user(char *authstr, char *ip, int authority);

#endif

