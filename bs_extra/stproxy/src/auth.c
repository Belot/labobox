/*
 * Copyright (C) 2007 Adam Hurkala (ahurkala@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "common.h"
#include "debug.h"
#include "auth.h"
#include "md5.h"
#include "strutils.h"
//#include <unistd.h>


const char my_passwd_file[512+1];
char *cmd;
char **cmd_argv;

static struct user *userlist, *lastuser;

static int b64decode(char *str)
{
   char *cur, *start;
   int d, dlast, phase;
   unsigned char c;
   static int table[256] = {
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,62,-1,-1,-1,63,
     52,53,54,55,56,57,58,59,60,61,-1,-1,-1,-1,-1,-1,
     -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,
     15,16,17,18,19,20,21,22,23,24,25,-1,-1,-1,-1,-1,
     -1,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,
     41,42,43,44,45,46,47,48,49,50,51,-1,-1,-1,-1,-1,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
     -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
   };

   d = dlast = phase = 0;
   start = str;
   for (cur = str; *cur != '\0'; ++cur )
   {
      if (*cur == '\n' || *cur == '\r')
      {
        phase = dlast = 0;
        continue;
      }
      d = table[(int)*cur];
      if(d != -1)
      {
         switch(phase)
         {
            case 0:
                ++phase;
                break;
            case 1:
                c = ((dlast << 2) | ((d & 0x30) >> 4));
                *str++ = c;
                ++phase;
                break;
            case 2:
                c = (((dlast & 0xf) << 4) | ((d & 0x3c) >> 2));
                *str++ = c;
                ++phase;
                break;
            case 3:
                c = (((dlast & 0x03 ) << 6) | d);
                *str++ = c;
                phase = 0;
                break;
         }
         dlast = d;
      }
   }
   *str = '\0';
   return str - start;
}

static char *encrypt_password(char *password)
{
   char salt[9];

   srandom((unsigned int)time(NULL));
   to64(salt, random(), 8);
   salt[8] = '\0';
   return crypt_md5(password, salt);
}

static int compare_ip(char *ip, char **ipmask)
{
   int i, j;

   i = 0;
   while (ipmask[i])
   {
      //printf("ip=%s, ip_mask=%s\n",ip,ipmask[i]);
      if(strcmp(ipmask[i],"0.0.0.0")==0) return 1;
      for (j = 0; ipmask[i][j] && ipmask[i][j] == ip[j]; j++);
      if (ipmask[i][j] == '*' || (!ipmask[i][j] && !ip[j])) return 1;
      i++;
   }
   return 0;
}

static int find_ip(struct user *user, char *ip)
{
   int i;

   i = 0;
   while (user->ipmask[i])
   {
      if ( (strcmp(user->ipmask[i], ip) == 0) || (strcmp(user->ipmask[i],"0.0.0.0")==0) ) return 0;
      i++;
   }
   return EIPNOTFOUND;
}

static struct user *find_user(char *username)
{
   struct user *user;

   user = userlist;
   while (user)
   {
      if (strcmp(user->username, username) == 0) return user;
      user = user->next;
   }
   return NULL;
}

static int valid_username(char *username)
{
   if (strchr(username, ':')) return 0;
   return 1;
}

static int valid_mask(char *ip)
{
   int i, octets, digits;

   octets = 0;
   digits = 0;
   for (i = 0; ip[i]; i++)
   {
      if (isdigit(ip[i]))
      {
         if (++digits == 4 || (digits == 1 && ip[i] == '0' && ip[i+1] && ip[i+1] != '.')) return 0;
      }
      else if (ip[i] == '*')
      {
         if (digits != 0 || ip[i+1]) return 0;
         return 1;
      }
      else if (ip[i] == '.')
      {
         if (++octets == 4 || digits == 0 || (digits == 3 && 100*ip[i-3]+10*ip[i-2]+ip[i-1]-5328 > 255)) return 0;
         digits = 0;
      }
      else return 0;
   }
   if (++octets != 4 || digits == 0 || (digits == 3 && 100*ip[i-3]+10*ip[i-2]+ip[i-1]-5328 > 255)) return 0;
   return 1;
}

int load_db(char *passwd_file)
{
   FILE *f;
   char buf[MAX_LINE_LENGTH+1];

   print("Loading user db");
   strcpy(my_passwd_file,PASSWD_FILE);
   if(passwd_file==NULL) f = fopen(PASSWD_FILE, "r");
   else
   {
     if(strlen(passwd_file)>512) return ECORRUPTDB;
     strcpy(my_passwd_file,passwd_file);
     f = fopen(passwd_file, "r");
   }
   if (!f)
   {
      int i, res;
      char *ip[] = {ADMIN_MASK};

      if (errno != ENOENT) return EOPENDB;
      res = add_user(ADMIN_USER, ADMIN_PASS, SUPER_USER, 1);
      if (res != 0) return EADDTODB;
      i = 0;
      while (ip[i])
      {
         res = add_ip(ADMIN_USER, ip[i]);
         if (res != 0)
         {
            delete_user(ADMIN_USER);
            return EADDTODB;
         }
         i++;
      }
      return update_db(1);
   }
   while (fgets(buf, MAX_LINE_LENGTH + 1, f))
   {
      int i, count, res, len;
      char *user, *pass, *authority, *ipmask[MAX_IP_COUNT+1];
      char *s;

      user = NULL;
      pass = NULL;
      authority = NULL;
      for (i = 0; i < MAX_IP_COUNT + 1; i++) ipmask[i] = NULL;
      len = strlen(buf);
      init_tokenizer(buf, NULL, len);
      if (!parse_line())
      {
         if (feof(f))
         {
            init_tokenizer(buf, NULL, len + 1); 
            parse_string();
         }
         else
         {
            fclose(f);
            return ECORRUPTDB;
         }
      }
      i = 0;
      count = 0;
      while ((s = get_token()))
      {
         if (i == 0)
         {
            if (strlen(s) == 0) break;
            user = s;
         }
         else if (i == 1) authority = s;
         else if (i == 2) pass = s;
         else if (count < MAX_IP_COUNT) ipmask[count++] = s;
         i++;
      }
      if (user && authority && pass)
      {
         res = add_user(user, pass, atoi(authority), 0);
         if (res != 0)
         {
            fclose(f);
            return EADDTODB;
         }
         for (i = 0; i < count; i++)
         {
            res = add_ip(user, ipmask[i]);
            if (res != 0)
            {
               fclose(f);
               return EADDTODB;
            } 
         }
      }
      else if (user)
      {
         fclose(f);
         return ECORRUPTDB;
      }
   }
   if (!feof(f))
   {
      fclose(f);
      return EREADDB;
   } 
   fclose(f);
   return 0;
}

int update_db(int rebuild)
{
   struct user *user;
   int i;
   FILE *f;

   print("Updating user db");
   if (rebuild) f = fopen(my_passwd_file, "w");
   else f = fopen(my_passwd_file, "a");
   if (!f)
   {
      printerror("Cannot update user db");
      return EUPDATEDB;
   }
   if (rebuild) user = userlist;
   else user = lastuser;
   while (user)
   {
      i = 0;
      fprintf(f, "%s %d %s", user->username, user->authority, user->password);
      while (user->ipmask[i]) fprintf(f, " %s", user->ipmask[i++]); 
      fprintf(f, "\n");
      user = user->next;
   }
   fclose(f);
   if(cmd!=NULL) exec_update_cmd();
   return 0;
}

void free_db(void)
{
   struct user *tmp;
   int i;

   while (userlist)
   {
      i = 0; 
      tmp = userlist->next;
      while (userlist->ipmask[i]) free(userlist->ipmask[i++]);
      free(userlist->username);
      free(userlist->password);
      free(userlist->ipmask);
      free(userlist);
      userlist = tmp;
   }
   lastuser = NULL;
}

int add_ip(char *username, char *ip)
{
   int i, len;
   struct user *user;

   print("Adding IP \"%s\" to user \"%s\"", ip, username);
   len = strlen(username);
   if (len < 1 || len > MAX_USERNAME_LENGTH) return EUSERNAMELENGTH;
   len = strlen(ip);
   if (len < 1 || len > MAX_IP_LENGTH) return EIPLENGTH;
   if (!valid_username(username)) return EBADUSERNAME;
   if (!valid_mask(ip)) return EIPMASK;
   user = find_user(username);
   if (!user) return EUSERNOTFOUND;
   //if (find_ip(user, ip) != EIPNOTFOUND) return EIPEXISTS;
   i = 0;
   while (user->ipmask[i]) i++;
   if (i < MAX_IP_COUNT)
   {
      user->ipmask[i] = (char *)malloc((len + 1) * sizeof(char));
      if (!user->ipmask[i]) return ENOMEMORY;
      strcpy(user->ipmask[i], ip);
      return 0;
   }
   return EIPCOUNT;
}

int delete_ip(char *username, char *ip)
{
   struct user *user;
   int i, found;

   print("Removing IP \"%s\" from user \"%s\"", ip, username);
   i = strlen(username);
   if (i < 1 || i > MAX_USERNAME_LENGTH) return EUSERNAMELENGTH;
   i = strlen(ip);
   if (i < 1 || i > MAX_IP_LENGTH) return EIPLENGTH;
   if (!valid_username(username)) return EBADUSERNAME;
   if (!valid_mask(ip)) return EIPMASK;
   user = find_user(username);
   if (!user) return EUSERNOTFOUND;
   found = 0;
   i = 0;
   while (user->ipmask[i])
   {
      if (!found && strcmp(user->ipmask[i], ip) == 0)
      {
         free(user->ipmask[i]);
         user->ipmask[i] = NULL;
         found = 1;
      }
      else if (found)
      {
         user->ipmask[i-1] = user->ipmask[i];
         user->ipmask[i] = NULL;
      }
      i++;
   }
   if (found) return 0;
   return EIPNOTFOUND;
}

int get_user(char *username, struct user **user)
{
   int len;
   struct user *tmp;
 
   print("Getting information about user \"%s\"", username);
   len = strlen(username);
   if (len == 0 || len > MAX_USERNAME_LENGTH) return EUSERNAMELENGTH;
   if (!valid_username(username)) return EBADUSERNAME;
   tmp = userlist;
   while (tmp)
   {
      if (strcmp(username, tmp->username) == 0)
      {
         *user = tmp;
         return 0;
      }
      tmp = tmp->next;
   }
   return EUSERNOTFOUND;
}

int add_user(char *username, char *password, int authority, int encrypt)
{
   struct user *user;
   int i, ulen, plen;
   
   print("Adding user \"%s\" - %s", username, authority == 0 ? "ADMIN" : "NORMAL USER");
   ulen = strlen(username);
   if (ulen == 0 || ulen > MAX_USERNAME_LENGTH) return EUSERNAMELENGTH;
   plen = strlen(password);
   if (plen == 0 || (encrypt && plen > MAX_PASSWORD_LENGTH) || (!encrypt && plen > MAX_ENCRYPTED_PASSWORD_LENGTH)) return EPASSWORDLENGTH;
   if (!valid_username(username)) return EBADUSERNAME;
   if (find_user(username)) return EUSEREXISTS;
   user = (struct user *)malloc(sizeof(struct user));
   if (!user) return ENOMEMORY;
   user->username = (char *)malloc((ulen + 1) * sizeof(char));
   if (!user->username)
   {
      free(user);
      return ENOMEMORY;
   }
   strcpy(user->username, username);
   if (encrypt)
   {
      password = encrypt_password(password);
      plen = strlen(password);
   }
   user->password = (char *)malloc((plen + 1) * sizeof(char));
   if (!user->password)
   {
      free(user->username);
      free(user);
      return ENOMEMORY;
   }
   strcpy(user->password, password); 
   user->ipmask = (char **)malloc((MAX_IP_COUNT + 1) * sizeof(char *));
   if (!user->ipmask)
   {
      free(user->username);
      free(user->password);
      free(user);
      return ENOMEMORY;
   }
   for (i = 0; i < MAX_IP_COUNT + 1; i++) user->ipmask[i] = NULL;
   user->authority = authority;
   user->next = NULL;
   user->prev = lastuser;
   if (lastuser) lastuser->next = user;
   else userlist = user;
   lastuser = user;
   return 0;
}

int delete_user(char *username)
{
   struct user *user;
   int i;

   print("Removing user \"%s\"", username);
   i = strlen(username);
   if (i < 1 || i > MAX_USERNAME_LENGTH) return EUSERNAMELENGTH;
   if (!valid_username(username)) return EBADUSERNAME;
   user = find_user(username);
   if (!user) return EUSERNOTFOUND;
   if (user->prev) user->prev->next = user->next;
   else userlist = user->next;
   if (user->next) user->next->prev = user->prev;
   else lastuser = user->prev;
   i = 0;
   while (user->ipmask[i]) free(user->ipmask[i++]);
   free(user->username);
   free(user->password);
   free(user->ipmask);
   free(user);
   return 0;
}

int change_password(char *username, char *password)
{
   struct user *user;
   char *tmp;
   int i;

   print("Changing password for user \"%s\"", username);
   i = strlen(username);
   if (i < 1 || i > MAX_USERNAME_LENGTH) return EUSERNAMELENGTH;
   i = strlen(password);
   if (i < 1 || i > MAX_PASSWORD_LENGTH) return EPASSWORDLENGTH;
   if (!valid_username(username)) return EBADUSERNAME;
   user = find_user(username);
   if (!user) return EUSERNOTFOUND;
   password = encrypt_password(password);
   tmp = (char *)malloc((strlen(password) + 1) * sizeof(char));
   if (!tmp) return ENOMEMORY;
   strcpy(tmp, password);
   free(user->password);
   user->password = tmp;
   return 0;
}

int authorized_connection(char *ip)
{
   struct user *user = userlist;

   print("Checking if IP %s is allowed to connect", ip);
   while (user)
   {
      if (compare_ip(ip, user->ipmask)) return 1;
      user = user->next;
   }
   return 0;
}

int authorized_user(char *authstr, char *ip, int authority)
{
   char *username, *password, *tmp;
   struct user *user;
 
   print("Authorizing connection from IP %s", ip);
   b64decode(authstr);
   tmp = strchr(authstr, ':');
   if (!tmp) return 0;
   tmp[0] = '\0';
   username = authstr;
   password = tmp + 1;
   user = find_user(username);
   //printf("user=%s, user_auth=%d, auth=%d,cmp_ip=%d, cmp_md5=%d\n",user,user->authority,authority,compare_ip(ip, user->ipmask),strcmp(crypt_md5(password, user->password), user->password));
   if (user && user->authority <= authority && compare_ip(ip, user->ipmask) && strcmp(crypt_md5(password, user->password), user->password) == 0) return 1;
   return 0;
}

int register_update_cmd(char *cmd_string)
{
  print("cmd_string=%s",cmd_string);
  cmd=NULL;
  cmd_argv=NULL;
  if(cmd_string==NULL) return 0;
  
  cmd_argv=malloc((MAX_CMD_PARAMS+1)*sizeof(char*));
  if(!cmd_argv) return -1;
  char *pos=cmd_string;
  if(strchr(cmd_string,' ')==NULL)
  {
    cmd=malloc(strlen(pos)+1);
    if(!cmd) return -1;
    sprintf(cmd,"%s\0",pos);
    cmd_argv[0]=malloc(strlen(pos)+1);
    if(!cmd_argv[0]) return -1;
    sprintf(cmd_argv[0],"%s\0",pos);
    return 0;
  }
  
  int i=0;
  pos=strtok(cmd_string," ");
  while(pos!=NULL && i<MAX_CMD_PARAMS)
  {
    if(i==0)
    {
      cmd=malloc(strlen(pos)+1);
      if(!cmd) return -1;
      sprintf(cmd,"%s\0",pos);
    }
    //any cmd parameter
    cmd_argv[i]=malloc(strlen(pos)+1);
    if(!cmd_argv[i]) return -1;
    sprintf(cmd_argv[i],"%s\0",pos);
    i++;
    pos=strtok(NULL," ");
  }
  cmd_argv[++i]=NULL;
#ifdef DEBUG
  i=0;
  while(cmd_argv[i])
  {
    print("argv[%d]=%s",i,cmd_argv[i]);
    i++;
  }
#endif
  print("Register successful");
  return 0;
}

void exec_update_cmd()
{
  print("Executing Update cmd");
  if(cmd==NULL) return;
  int pid=fork();
  if(pid==0)
  {
    int ret=execv(cmd,cmd_argv);
    exit(ret);
  }
  print("ret_update_cmd=%d",ret);
}