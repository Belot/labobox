/*
 * Copyright (C) 2007 Adam Hurkala (ahurkala@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "common.h"
#include "debug.h"
#include "sock.h"

struct SOCK *socketlist, *lastsocket;
int clientcount;

int set_noblocking(int fd)
{
   int flags;

   flags = fcntl(fd, F_GETFL);
   if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) < 0)
   {
      printerror("Cannot set socket to non-blocking mode, fd %d", fd);
      return -1;
   }
   return 0;
}

struct SOCK *new_socket(int fd)
{
   struct SOCK *sock;

   if (fd == ALLOCATE_SOCKET)
   {
      fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
      if (fd < 0)
      {
         printerror("Cannot allocate new socket");
         return NULL;
      }
      if (set_noblocking(fd) != 0)
      {
         close(fd);
         return NULL;
      }
   }
   sock = (struct SOCK *)malloc(sizeof(struct SOCK));
   if (sock)
   {
      sock->readhandler = NULL;
      sock->writehandler = NULL;
      sock->forward = NULL;
      sock->next = NULL;
      sock->prev = lastsocket;
      sock->fd = fd;
      sock->state = UNKNOWN_STATE;
      sock->method = UNKNOWN_METHOD;
      sock->bufferlen = 0;
      sock->writeoffset = 0;
      sock->queryid = -1;
      if (lastsocket) lastsocket->next = sock;
      else socketlist = sock;
      lastsocket = sock;
   }
   else
   {
      printerror("Cannot allocate memory for socket structure");
      close(fd);
   }
   return sock;
}

void free_socket(struct SOCK *sock)
{
   print("Closing socket, fd %d", sock->fd);
   if (sock->state == CLIENT_STATE) clientcount--;
   if (close(sock->fd) != 0) printerror("Error closing socket, fd %d", sock->fd);
   if (sock->prev) sock->prev->next = sock->next;
   else socketlist = sock->next;
   if (sock->next) sock->next->prev = sock->prev;
   else lastsocket = sock->prev;
   print("Freeing socket, fd %d", sock->fd);
   free(sock);
}

int bind_socket(int fd, char *ip, unsigned int port)
{
   struct sockaddr_in sa;

   memset(&sa, 0, sizeof(struct sockaddr_in));
   sa.sin_family = AF_INET;
   sa.sin_port = htons(port);
   sa.sin_addr.s_addr = inet_addr(ip);
   if (bind(fd, (struct sockaddr *)&sa, sizeof(struct sockaddr)) == 0)
   {
      print("Binding socket to %s:%d (fd %d)", inet_ntoa(sa.sin_addr), port, fd);
      return 0;
   }
   printerror("Cannot bind socket to %s:%d (fd %d)", ip, port, fd);
   return 1;
}

int write_socket(struct SOCK *sock)
{
   int count;
   struct SOCK *fs = sock->forward;

   count = send(sock->fd, sock->buffer + sock->writeoffset, sock->bufferlen - sock->writeoffset, 0);
   if (count < 0)
   {
      printerror("Error writing to fd %d", sock->fd);
      return 1;
   }
   else
   {
      print("Sent %d bytes to fd %d (%s)", count, sock->fd, (sock->state == CLIENT_STATE) ? "CLIENT" : "SERVER");
      sock->writeoffset += count;
      time(&sock->idle);
      if (sock->writeoffset == sock->bufferlen)
      {
         sock->bufferlen = 0;
         sock->writeoffset = 0;
         sock->writehandler = NULL;
         if (fs && !fs->readhandler)
         {
            print("Setting read handler for %d fd (%s)", fs->fd, (fs->state == CLIENT_STATE) ? "CLIENT" : "SERVER");
            fs->readhandler = read_socket;
         }
      }
   }
   return 0;
}

int read_socket(struct SOCK *sock)
{
   int count;
   struct SOCK *fs = sock->forward;

   if (sock->state == CLIENT_STATE)
   {
      if (sock->method == GET_METHOD || (sock->method == CONNECT_METHOD && (!fs || fs->state != SERVER_CONNECTED_STATE)))
      {
         #ifdef DEBUG
           char c;
           count = recv(sock->fd, &c, 1, 0);
           if (count > 0) print("Unexpected data received from fd %d (CLIENT)", sock->fd);
           else if (count == 0) print("EOF received on fd %d (CLIENT)", sock->fd);
           else printerror("Error reading from fd %d (CLIENT)", sock->fd);
         #endif
         return 1;
      }
      if (sock->method == POST_METHOD && (!fs || fs->state != SERVER_CONNECTED_STATE))
      {
         count = recv(sock->fd, sock->buffer + sock->bufferlen, sizeof(sock->buffer) - sock->bufferlen, 0);
         if (count > 0)
         {
            print("Buffering %d bytes from fd %d (CLIENT)", count, sock->fd);
            sock->bufferlen += count;
            if (sock->bufferlen == sizeof(sock->buffer))
            {
               print("Receive buffer if full, resetting read handler for fd %d (CLIENT)", sock->fd);
               sock->readhandler = NULL;
            }
            time(&sock->idle);
            return 0;
         }
         else
         {
            if (count == 0) print("EOF received on fd %d (CLIENT)", sock->fd);
            else printerror("Error reading from fd %d (CLIENT)", sock->fd);
            return 1;
         } 
      }
   }
   count = recv(sock->fd, fs->buffer + fs->bufferlen, sizeof(fs->buffer) - fs->bufferlen, 0);
   if (count > 0)
   {
      print("Read %d bytes from fd %d (%s)", count, sock->fd, (sock->state == CLIENT_STATE) ? "CLIENT" : "SERVER");
      fs->bufferlen += count;
      if (fs->bufferlen == sizeof(fs->buffer))
      {
         print("Receive buffer if full, resetting read handler for fd %d (%s)", sock->fd, (sock->state == CLIENT_STATE) ? "CLIENT" : "SERVER");
         sock->readhandler = NULL;
      }
      fs->writehandler = write_socket;
      time(&sock->idle);
      return 0;
   }
   else
   {
      if (count == 0) print("EOF received on fd %d (%s)", sock->fd, (sock->state == CLIENT_STATE) ? "CLIENT" : "SERVER");
      else printerror("Error reading from fd %d (%s)", sock->fd, (sock->state == CLIENT_STATE) ? "CLIENT" : "SERVER");
      if (fs->bufferlen > 0)
      {
         sock->readhandler = NULL;
         sock->writehandler = NULL;
         fs->readhandler = NULL;
         fs->writehandler = flush_socket;
         return 0;
      }
      return 1;
   }
}

int flush_socket(struct SOCK *sock)
{
   int count;

   count = send(sock->fd, sock->buffer + sock->writeoffset, sock->bufferlen - sock->writeoffset, 0);
   if (count < 0)
   {
      printerror("Error writing to fd %d", sock->fd);
      return 1;
   }
   else
   {
      print("Flushing %d bytes to fd %d (%s)", count, sock->fd, (sock->state == CLIENT_STATE) ? "CLIENT" : "SERVER");
      sock->writeoffset += count;
      if (sock->writeoffset == sock->bufferlen) return 1;
   }
   return 0;
}

int close_socket(struct SOCK *sock)
{
   int count;

   count = send(sock->fd, sock->buffer + sock->writeoffset, sock->bufferlen - sock->writeoffset, 0);
   if (count < 0)
   {
      printerror("Error writing to fd %d (%s)", sock->fd, (sock->state == CLIENT_STATE) ? "CLIENT" : "SERVER");
      return 1;
   }
   else
   {
      print("Sent %d bytes to fd %d", count, sock->fd);
      sock->writeoffset += count;
      if (sock->writeoffset == sock->bufferlen) return 1;
   }
   return 0;
}
