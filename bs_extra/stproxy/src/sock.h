#ifndef __SOCK_H
#define __SOCK_H
#define ALLOCATE_SOCKET -1

struct SOCK;
typedef int (*handler_proc)(struct SOCK *);
struct SOCK *new_socket(int fd);
int set_noblocking(int fd);
void free_socket(struct SOCK *sock);
void disconnect_socket(struct SOCK *sock, char *msg, int n);
int bind_socket(int fd, char *hostname, unsigned int port);
int read_socket(struct SOCK *sock);
int write_socket(struct SOCK *sock);
int flush_socket(struct SOCK *sock);
int close_socket(struct SOCK *sock);

struct SOCK
{
   char buffer[BUF_LENGTH];
   struct sockaddr_in sockaddr;
   handler_proc readhandler;
   handler_proc writehandler;
   struct SOCK *forward;
   struct SOCK *next;
   struct SOCK *prev;
   int fd;
   int state;
   int method;
   int bufferlen;
   int writeoffset;
   int queryid;
   time_t idle;
   time_t timeout;
};
#endif
