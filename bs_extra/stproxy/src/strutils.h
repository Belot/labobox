#ifndef __STRUTILS_H
#define __STRUTILS_H
#define EBADREQUEST -1
#define ENOTFOUND -2
int unescape_url(char *url);
void init_tokenizer(char *s, char *delim, size_t n);
int parse_line(void);
int parse_line_crlf(void);
int parse_string(void);
char *get_token(void);
void *memfind(const void *block, size_t blen, const void *pat, size_t plen);
#endif
