#ifndef __WEBSERVER_H
#define __WEBSERVER_H
#define ROOT_PAGE 0
#define SHOWUSER_PAGE 1
#define ADDUSER_PAGE 2
#define DELUSER_PAGE 3
#define ADDIP_PAGE 4
#define DELIP_PAGE 5
#define CHPASS_PAGE 6
#define NOTFOUND_PAGE 7
struct SOCK;
int webserver_init(void);
int webserver_handle_client(struct SOCK *client);
#endif
