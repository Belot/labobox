/*
 * Copyright (C) 2007 Adam Hurkala (ahurkala@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "common.h"
#include "debug.h"
#include "sock.h"
#include "reqerror.h"

void request_error(struct SOCK *client, int error, char *param)
{
   char *s, tmp[16];
   struct SOCK *server = client->forward;

   switch(error)
   {
      case WEBSERVER_AUTH_ERROR:
        print("Handling WEBSERVER_AUTH_ERROR, fd %d", client->fd);
        strcpy(client->buffer,
               "HTTP/1.0 401 Authorization Required\r\n"
               "WWW-Authenticate: Basic realm=\"Restricted Area\"\r\n"
               "Content-Type: text/html\r\n\r\n"
               "<html>\n"
               "<head>\n"
               "<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">\n"
               "<title>Authorization Failed</title>\n"
               "</head>\n"
               "<body>\n"
               "<HR noshade size=\"1px\">\n"
               "<P><H3>Proxy Authorization Failed.</H3></P>\n"
               "<P>This server could not verify that you are authorized to access.</P>\n"
               "<P>Either you supplied the wrong credentials, or your browser doesn't understand how to supply the credentials required.</P>\n"
               "<HR noshade size=\"1px\">\n"
               PROXY_VERSION"\n"
               "</body>\n"
               "</html>\n");
        break;
      case PROXY_AUTH_ERROR:
        print("Handling PROXY_AUTH_ERROR, fd %d", client->fd);
        strcpy(client->buffer,
               "HTTP/1.0 407 Proxy Authorization Required\r\n"
               "Proxy-Authenticate: Basic realm=\"Restricted Area\"\r\n"
               "Content-Type: text/html\r\n\r\n"
               "<html>\n"
               "<head>\n"
               "<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">\n"
               "<title>Proxy Authorization Failed</title>\n"
               "</head>\n"
               "<body>\n"
               "<HR noshade size=\"1px\">\n"
               "<P><H3>Proxy Authorization Failed.</H3></P>\n"
               "<P>This server could not verify that you are authorized to access.</P>\n"
               "<P>Either you supplied the wrong credentials, or your browser doesn't understand how to supply the credentials required.</P>\n"
               "<HR noshade size=\"1px\">\n"
               PROXY_VERSION"\n"
               "</body>\n"
               "</html>\n");
        break;
      case DNS_ERROR:
        print("Handling DNS_ERROR, fd %d", client->fd);
        strcpy(client->buffer,
               "HTTP/1.0 404 Not Found\r\n"
               "Server: "PROXY_VERSION"\r\n"
               "Content-Type: text/html\r\n\r\n"
               "<html>\n"
               "<head>\n"
               "<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">\n"
               "<title>Error: The requested URL could not be checked</title>\n"
               "</head>\n"
               "<body>\n"
               "<H2>The requested URL could not be checked</H2>\n"
               "<HR noshade size=\"1px\">\n"
               "Following error occured while checking requested URL:\n"
               "<BLOCKQUOTE>Unable to resolve hostname ");
        strcat(client->buffer, param);
        strcat(client->buffer,
               ".</BLOCKQUOTE>\n"
               "<HR noshade size=\"1px\">\n"
               PROXY_VERSION"\n"
               "</body>\n"
               "</html>\n");
        break;
      case CONNECT_ERROR:
        print("Handling CONNECT_ERROR, fd %d", client->fd);
        strcpy(client->buffer,
               "HTTP/1.0 503 Service Unavailable\r\n"
               "Server: "PROXY_VERSION"\r\n"
               "Content-Type: text/html\r\n\r\n"
               "<html>\n"
               "<head>\n"
               "<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">\n"
               "<title>Error: The requested URL could not be checked</title>\n"
               "</head>\n"
               "<body>\n"
               "<H2>The requested URL could not be checked</H2>\n"
               "<HR noshade size=\"1px\">\n"
               "Following error occured:\n"
               "<BLOCKQUOTE>Unable to establish connection with ");
        sprintf(tmp, "%d", ntohs(server->sockaddr.sin_port));
        strcat(client->buffer, inet_ntoa(server->sockaddr.sin_addr));
        strcat(client->buffer, ":");
        strcat(client->buffer, tmp);
        s = strerror(errno);
        if (strlen(s) < 256)
        {
           strcat(client->buffer, " (");
           strcat(client->buffer, s);
           strcat(client->buffer, ")");
        }
        strcat(client->buffer,
               ".</BLOCKQUOTE>\n"
               "<HR noshade size=\"1px\">\n"
               PROXY_VERSION"\n"
               "</body>\n"
               "</html>\n");
        break;
      case NOTSUPPORTED_ERROR:
        print("Handling NOTSUPPORTED_ERROR, fd %d", client->fd);
        strcpy(client->buffer,
               "HTTP/1.0 501 Not Implemented\r\n"
               "Server: "PROXY_VERSION"\r\n"
               "Content-Type: text/html\r\n\r\n"
               "<html>\n"
               "<head>\n"
               "<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">\n"
               "<title>Error: The requested URL could not be checked</title>\n"
               "</head>\n"
               "<body>\n"
               "<H2>The requested URL could not be checked</H2>\n"
               "<HR noshade size=\"1px\">\n"
               "FTP protocol is currently not supported by this server, please use ftp client software instead.\n"
               "<HR noshade size=\"1px\">\n"
               PROXY_VERSION"\n"
               "</body>\n"
               "</html>\n");
        break;
      case MAXCLIENTS_ERROR:
        print("Handling MAXCLIENTS_ERROR, fd %d", client->fd);
        strcpy(client->buffer,
               "HTTP/1.0 503 Service Unavailable\r\n"
               "Server: "PROXY_VERSION"\r\n"
               "Content-Type: text/html\r\n\r\n"
               "<html>\n"
               "<head>\n"
               "<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">\n"
               "<title>Error: The requested URL could not be checked</title>\n"
               "</head>\n"
               "<body>\n"
               "<H2>The requested URL could not be checked</H2>\n"
               "<HR noshade size=\"1px\">\n"
               "Too many clients, please try again later.\n"
               "<HR noshade size=\"1px\">\n"
               PROXY_VERSION"\n"
               "</body>\n"
               "</html>\n");
        break;
      case SYNTAX_ERROR:
        print("Handling SYNTAX_ERROR, fd %d", client->fd);
        strcpy(client->buffer,
               "HTTP/1.0 400 Bad Request\r\n"
               "Server: "PROXY_VERSION"\r\n"
               "Content-Type: text/html\r\n\r\n"
               "<html>\n"
               "<head>\n"
               "<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">\n"
               "<title>Error: The requested URL could not be checked</title>\n"
               "</head>\n"
               "<body>\n"
               "<H2>The requested URL could not be checked</H2>\n"
               "<HR noshade size=\"1px\">\n"
               "Possible errors:<br>\n"
               "Your request has bad syntax.<br>\n"
               "You are trying to use method which is not supported.<br>\n"
               "Please check your request and try again.\n"
               "<HR noshade size=\"1px\">\n"
               PROXY_VERSION"\n"
               "</body>\n"
               "</html>\n");
        break;
      default:
        print("Handling INTERNAL_ERROR, fd %d", client->fd);
        strcpy(client->buffer,
               "HTTP/1.0 503 Service Unavailable\r\n"
               "Server: "PROXY_VERSION"\r\n"
               "Content-Type: text/html\r\n\r\n"
               "<html>\n"
               "<head>\n"
               "<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">\n"
               "<title>Error: The requested URL could not be checked</title>\n"
               "</head>\n"
               "<body>\n"
               "<H2>The requested URL could not be checked</H2>\n"
               "<HR noshade size=\"1px\">\n"
               "Following error occured while checking requested URL:\n"
               "<BLOCKQUOTE>Proxy error: Internal server error.</BLOCKQUOTE>\n"
               "<HR noshade size=\"1px\">\n"
               PROXY_VERSION"\n"
               "</body>\n"
               "</html>\n");
        break;
   }
   client->bufferlen = strlen(client->buffer);
   client->readhandler = NULL;
   client->writehandler = close_socket;
}
