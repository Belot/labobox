/*
 * Copyright (C) 2007 Adam Hurkala (ahurkala@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "common.h"
#include "debug.h"
#include "auth.h"
#include "sock.h"
#include "libdns.h"
#include "reqerror.h"
#include "strutils.h"
#include "webserver.h"
#include "proxy.h"
#include <signal.h>
#include <sys/wait.h>

extern struct SOCK *socketlist, *lastsocket;
extern int clientcount;

static void server_loop(void)
{
   int maxfd, res;
   fd_set read_set, write_set;
   struct SOCK *sock, *tmp;
   struct timeval select_timeout;
   time_t timer;

   for (;;)
   {
      FD_ZERO(&read_set);
      FD_ZERO(&write_set);
      maxfd = libdns_fds(&read_set, &write_set);
      sock = socketlist;
      while (sock)
      {
         if (sock->readhandler) FD_SET(sock->fd, &read_set);
         if (sock->writehandler) FD_SET(sock->fd, &write_set);
         if (sock->fd + 1 > maxfd) maxfd = sock->fd + 1;
         sock = sock->next;
      }
      select_timeout.tv_sec = SELECT_TIMEOUT;
      select_timeout.tv_usec = 100000;
      time(&timer);
      res = select(maxfd, &read_set, &write_set, NULL, &select_timeout);
      if (res < 0) printerror("select failed");
      else if (res == 0) print("select timeout");
      else
      {
         sock = socketlist;
         while (sock)
         {
            if (sock->readhandler && FD_ISSET(sock->fd, &read_set) && sock->readhandler(sock) != 0)
            {
               if (sock->forward) free_socket(sock->forward);
               tmp = sock->next;
               free_socket(sock);
               sock = tmp;
               continue;
            }
            if (sock->writehandler && FD_ISSET(sock->fd, &write_set) && sock->writehandler(sock) != 0)
            {
               if (sock->forward) free_socket(sock->forward);
               tmp = sock->next;
               free_socket(sock);
               sock = tmp;
               continue;
            }
            sock = sock->next;
         }
      }
      sock = socketlist;
      while (sock)
      {
         if (difftime(time(NULL), sock->idle) >= sock->timeout)
         {
            if (sock->state == SERVER_CONNECTING_STATE)
            {
               errno = ETIMEDOUT;
               printerror("Connection failed, fd %d", sock->fd);
               sock->writehandler = NULL;
               request_error(sock->forward, CONNECT_ERROR, NULL);
            }
            else if (sock->state != PROXY_SERVER_STATE && sock->state != WEB_SERVER_STATE)
            {
               print("Idle timeout on fd %d", sock->fd);
               if (sock->forward) free_socket(sock->forward);
               tmp = sock->next;
               free_socket(sock);
               sock = tmp;
               continue;
            }
         }
         sock = sock->next;
      }
      libdns_process(&read_set, &write_set);
   }
}

static int accept_client(struct SOCK *server)
{
   int fd;
   struct sockaddr_in sa;
   socklen_t len = sizeof(sa);
   struct SOCK *client;

   print("Accepting new connection on fd %d", server->fd);
   fd = accept(server->fd, (struct sockaddr *)&sa, &len);
   if (fd > 0)
   {
      if (authorized_connection(inet_ntoa(sa.sin_addr)))
      {
         print("Accepted connection from %s:%d, new fd %d", inet_ntoa(sa.sin_addr), ntohs(sa.sin_port), fd);
         client = new_socket(fd);
         if (client)
         {
            client->sockaddr = sa;
            client->state = CLIENT_STATE;
            client->timeout = REQUEST_TIMEOUT;
            time(&client->idle);
            clientcount++;
            if (clientcount > MAX_CLIENTS)
            {
               print("Cannot handle new client, fd %d (too many clients)", fd);
               request_error(client, MAXCLIENTS_ERROR, NULL);
            }
            else
            {
               if (server->state == PROXY_SERVER_STATE) client->readhandler = proxy_handle_client;
               else client->readhandler = webserver_handle_client;
            }
         }
         else
         {
            print("Cannot handle new client, fd %d", fd);
            close(fd);
         }
      }
      else
      {
         print("Illicit connection from %s:%d on fd %d", inet_ntoa(sa.sin_addr), ntohs(sa.sin_port), fd);
         close(fd);
      }
   }
   else printerror("Cannot accept new connection on fd %d", server->fd);
   return 0;
}

static int setup(char *ip, int proxyport, int adminport)
{
   struct SOCK *proxyserver;
   struct SOCK *webserver;

   proxyserver = new_socket(ALLOCATE_SOCKET);
   if (!proxyserver)
   {
      fprintf(stderr, "Cannot allocate new socket - %s\n", strerror(errno));
      return 1;
   }
   webserver = new_socket(ALLOCATE_SOCKET);
   if (!webserver)
   {
      fprintf(stderr, "Cannot allocate new socket - %s\n", strerror(errno));
      return 1;
   }
   if (bind_socket(proxyserver->fd, ip, proxyport) != 0)
   {
      fprintf(stderr, "Cannot bind socket to %s:%d - %s\n", ip, proxyport, strerror(errno));
      return 1;
   }
   if (bind_socket(webserver->fd, ip, adminport) != 0)
   {
      fprintf(stderr, "Cannot bind socket to %s:%d - %s\n", ip, adminport, strerror(errno));
      return 1;
   }
   if (listen(proxyserver->fd, SOMAXCONN) != 0)
   {
      fprintf(stderr, "Cannot listen on %s:%d - %s\n", ip, proxyport, strerror(errno));
      return 1;
   }
   if (listen(webserver->fd, SOMAXCONN) != 0)
   {
      fprintf(stderr, "Cannot listen on %s:%d - %s\n", ip, adminport, strerror(errno));
      return 1;
   }
   print("Listening on %s:%d (PROXY)", ip, proxyport);
   print("Listening on %s:%d (WEB INTERFACE)", ip, adminport);
   proxyserver->state = PROXY_SERVER_STATE;
   proxyserver->readhandler = accept_client;
   webserver->state = WEB_SERVER_STATE;
   webserver->readhandler = accept_client;
   return 0;
}

static int init(char *config_file, char *passwd_file, char *update_cmd)
{
   FILE *f;
   char buffer[MAX_LINE_LENGTH+1], ip[MAX_IP_LENGTH+1], *s;
   int proxyport, adminport, dns, len, res;
   struct sockaddr_in ns;

   proxyport = PROXY_PORT;
   adminport = ADMIN_PORT;
   strcpy(ip, BIND_ADDRESS);
   dns = 0;
   if(config_file==NULL) f = fopen(CONFIG_FILE, "r");
   else f = fopen(config_file, "r");
   if (!f)
   {
      if (errno != ENOENT)
      {
         fprintf(stderr, "Unable to open %s file - %s\n", CONFIG_FILE, strerror(errno));
         return 1;
      }
   }
   else
   {
      while (fgets(buffer, MAX_LINE_LENGTH + 1, f))
      {
         len = strlen(buffer);
         init_tokenizer(buffer, NULL, len);
         if (!parse_line())
         {
            if (feof(f))
            {
               init_tokenizer(buffer, NULL, len + 1);
               parse_string();
            }
            else
            {
               fprintf(stderr, "Error: %s is corrupt\n", CONFIG_FILE);
               fclose(f);
               return 1;
            }
         }
         s = get_token();
         if (!s || s[0] == '#') continue;
         if (strcmp(s, "BIND_ADDRESS") == 0)
         {
            if (!(s = get_token()) || strlen(s) > MAX_IP_LENGTH || inet_addr(s) == INADDR_NONE)
            {
               fprintf(stderr, "Error: %s - BIND_ADDRESS is not a valid ip address\n", CONFIG_FILE);
               fclose(f);
               return 1;
            }
            strcpy(ip, s);
         }
         else if (strcmp(s, "PROXY_PORT") == 0)
         {
            s = get_token();
            if (s) proxyport = atoi(s);
            if (!s || proxyport < 1 || proxyport > 65535)
            {
               fprintf(stderr, "Error: %s - PROXY_PORT is not a valid port number\n", CONFIG_FILE);
               fclose(f);
               return 1;
            }
         }
         else if (strcmp(s, "ADMIN_PORT") == 0)
         {
            s = get_token();
            if (s) adminport = atoi(s);
            if (!s || adminport < 1 || adminport > 65535)
            {
               fprintf(stderr, "Error: %s - ADMIN_PORT is not a valid port number\n", CONFIG_FILE);
               fclose(f);
               return 1;
            }
         }
         else if (strcmp(s, "DNS_SERVER") == 0)
         {
            in_addr_t tmp;

            if (!(s = get_token()) || (tmp = inet_addr(s)) == INADDR_NONE)
            {
               fprintf(stderr, "Error: %s - DNS_SERVER is not a valid ip address\n", CONFIG_FILE);
               fclose(f);
               return 1;
            }
            dns = 1;
            memset(&ns, 0, sizeof(struct sockaddr_in));
            ns.sin_addr.s_addr = tmp;
            ns.sin_family = AF_INET;
            ns.sin_port = htons(DNS_PORT);
         }
         else
         {
            fprintf(stderr, "Error: %s - unknown option \"%s\"\n", CONFIG_FILE, s);
            fclose(f);
            return 1;
         }
      }
      if (!feof(f))
      {
         fprintf(stderr, "Error reading %s - %s\n", CONFIG_FILE, strerror(errno));
         fclose(f);
         return 1;
      }
      fclose(f);
   }
   res = register_update_cmd(update_cmd);
   res = load_db(passwd_file);
   if (res != 0)
   {
      if (res == EOPENDB) fprintf(stderr, "Unable to open %s file - %s\n", PASSWD_FILE, strerror(errno));
      else if (res == EADDTODB) fprintf(stderr, "Unable to add user, %s file might be corrupt\n", PASSWD_FILE);
      else if (res == ECORRUPTDB) fprintf(stderr, "%s file is corrupt\n", PASSWD_FILE);
      else if (res == EREADDB) fprintf(stderr, "Error reading %s file - %s\n", PASSWD_FILE, strerror(errno));
      else if (res == EUPDATEDB) fprintf(stderr, "Unable to update %s file - %s\n", PASSWD_FILE, strerror(errno));
      return res;
   }
   res = libdns_init(dns == 0 ? NULL : &ns);
   if (res != 0)
   {
      if (res == ENOMEMORY) fprintf(stderr, "Unable to allocate memory - %s\n", strerror(errno));
      else if (res == ESOCKET || res == ENONBLOCK) fprintf(stderr, "Cannot allocate new socket - %s\n", strerror(errno));
      return res;
   }
   webserver_init();
   return setup(ip, proxyport, adminport);
}

void print_usage(char *prog)
{
  printf("Usage: %s [-c <config_file>] [-p <passwd_file>] [-u \"update_cmd_string\"] [--updatecmd \"update_cmd_string\"] [-h] [--help]\n",prog);
  return;
}

static void no_zombie (int signr) {
  pid_t pid;
  int ret;

  while ((pid = waitpid (-1, &ret, WNOHANG)) > 0)
    print("child process with pid=%d terminated\n",pid);
  return;
}

int main(int argc, char **argv)
{
   umask(077);
   int j;
   char *config_file=NULL;
   char *passwd_file=NULL;
   char *update_cmd=NULL;
   for(j=0;j<argc;j++)
   {
     if(strcmp("-h",argv[j])==0 || strcmp("--help",argv[j])==0)
     {
       print_usage(argv[0]);
       exit(0);
     }
     if(strcmp("-c",argv[j])==0 && j+1<argc)
     {
       config_file=malloc(strlen(argv[j+1])+1);
       if(!config_file)
       {
         fprintf(stderr,"invalid config_file_name\n");
         exit(-1);
       }
       sprintf(config_file,"%s\0",argv[j+1]);
     }
     if(strcmp("-p",argv[j])==0 && j+1<argc)
     {
       passwd_file=malloc(strlen(argv[j+1])+1);
       if(!passwd_file)
       {
         fprintf(stderr,"invalid passwd_file_name\n");
         exit(-1);
       }
       sprintf(passwd_file,"%s\0",argv[j+1]);
     }
     if( (strcmp("--updatecmd",argv[j])==0 || strcmp("-u",argv[j])==0) && j+1<argc)
     {
       update_cmd=malloc(strlen(argv[j+1])+1);
       if(!update_cmd)
       {
         fprintf(stderr, "invalid update_cmd_string\n");
         exit(-1);
       }
       sprintf(update_cmd,"%s\0",argv[j+1]);
     }
   }
   if (init(config_file,passwd_file,update_cmd) == 0)
   {
        //registering SIGNAL-handler child processes
        struct sigaction neu_sig, alt_sig;
        neu_sig.sa_handler = no_zombie;
        sigemptyset (&neu_sig.sa_mask);
        neu_sig.sa_flags = SA_RESTART;
        sigaction (SIGCHLD, &neu_sig, &alt_sig);
        
        
      #ifdef DEBUG
        server_loop();
      #else
        int i;

        switch(fork())
        {
           case -1:
             fprintf(stderr, "fork error - %s\n", strerror(errno));
             break;
           case 0:
             printf("Going into background, pid %d\n", getpid());
             setsid();
             signal(SIGPIPE, SIG_IGN);
             signal(SIGHUP, SIG_IGN);
             signal(SIGUSR1, SIG_IGN);
             signal(SIGUSR2, SIG_IGN);
             signal(SIGTERM, SIG_IGN);
             signal(SIGTSTP, SIG_IGN);
             signal(SIGTTOU, SIG_IGN);
             signal(SIGTTIN, SIG_IGN);
             close(0);
             close(1);
             close(2);
             i = open("/dev/null", O_RDWR);
             dup(i);
             dup(i);
             server_loop();
             break;
        }
      #endif
   }
   return 0;
}
