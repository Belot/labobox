/*
 * Copyright (C) 2007 Adam Hurkala (ahurkala@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "common.h"
#include "debug.h"
#include "auth.h"
#include "sock.h"
#include "libdns.h"
#include "reqerror.h"
#include "strutils.h"
#include "webserver.h"
#include "proxy.h"

extern struct SOCK *socketlist, *lastsocket;

static int connection_completed(struct SOCK *server);
static int initiate_connection(struct SOCK *client, struct in_addr in, int port);
static int initiate_communication(struct SOCK *client);
static int resolve_hostname(struct SOCK *client, char *name, int arg, struct in_addr *in);
static void hostname_resolved(uint16_t id, int result, char *name, struct in_addr *in, int arg);
static int relay_header(char *s);
static int connect_request(struct SOCK *client);
static int get_request(struct SOCK *client);
static int post_request(struct SOCK *client, int contentlen);

static int connection_completed(struct SOCK *server)
{
   int error;
   socklen_t len = sizeof(error);
   struct SOCK *client = server->forward;

   server->writehandler = NULL;
   if (getsockopt(server->fd, SOL_SOCKET, SO_ERROR, &error, &len) < 0)
   {
      printerror("Could not get errno from fd %d", server->fd);
      request_error(client, INTERNAL_ERROR, NULL);
      return 0;
   }
   if (error != 0)
   {
      errno = error;
      printerror("Connection failed, fd %d", server->fd);
      request_error(client, CONNECT_ERROR, NULL);
      return 0;
   }
   print("Connection established, fd %d", server->fd);
   server->state = SERVER_CONNECTED_STATE;
   server->readhandler = read_socket;
   time(&server->idle);
   server->timeout = IDLE_TIMEOUT;
   return initiate_communication(client);
}

static int initiate_connection(struct SOCK *client, struct in_addr in, int port)
{
   int tst;
   struct SOCK *server;

   server = new_socket(ALLOCATE_SOCKET);
   if (!server)
   {
      print("Unable to allocate socket for server connection, client fd %d", client->fd);
      request_error(client, INTERNAL_ERROR, NULL); 
      return 0;
   }
   time(&server->idle);
   server->state = SERVER_CONNECTING_STATE;
   server->timeout = CONNECT_TIMEOUT;
   server->forward = client;
   client->forward = server;
   server->sockaddr.sin_family = AF_INET;
   server->sockaddr.sin_addr = in;
   server->sockaddr.sin_port = htons(port);
   print("Connecting fd %d to %s:%d", server->fd, inet_ntoa(server->sockaddr.sin_addr), port);
   tst = connect(server->fd, (struct sockaddr *)&server->sockaddr, sizeof(server->sockaddr));
   if (tst < 0 && errno == EINPROGRESS)
   {
      server->writehandler = connection_completed;
      return 0;
   }
   if (tst == 0)
   {
      time(&server->idle);
      server->timeout = IDLE_TIMEOUT;
      server->state = SERVER_CONNECTED_STATE;
      server->readhandler = read_socket;
      return initiate_communication(client);
   }
   printerror("Connection failed, fd %d", server->fd);
   request_error(client, CONNECT_ERROR, NULL);
   return 0;
}

static int initiate_communication(struct SOCK *client)
{
   struct SOCK *fs = client->forward;

   if (client->method == CONNECT_METHOD)
   {
      memcpy(client->buffer, "HTTP/1.0 200 Connection established\r\n\r\n", 39);
      client->bufferlen = 39;
      client->writehandler = write_socket;
   }
   else if (client->method == GET_METHOD || client->method == POST_METHOD)
   {
      memcpy(fs->buffer, client->buffer, client->bufferlen);
      fs->writehandler = write_socket;
      fs->bufferlen = client->bufferlen;
      client->bufferlen = 0;
   }
   return 0;
}

static int resolve_hostname(struct SOCK *client, char *name, int arg, struct in_addr *in)
{
   uint16_t id;
   int res;
   in_addr_t addr;
   
   if ((addr = inet_addr(name)) != INADDR_NONE)
   {
      in->s_addr = addr;
      return 0;
   }
   print("Resolving hostname %s (fd %d)", name, client->fd);
   res = gethostbyname_a(name, hostname_resolved, arg, &id, in);
   if (res == 0) print("Hostname %s resolved to %s (cached)", name, inet_ntoa(*in));
   else if (res == EQUERYINPROGRESS)
   {
      print("DNS resolution in progress (ID %d)", id);
      client->queryid = id;
   }
   else print("gethostbyname_a failed (return code %d)", res);
   return res;
}

static void hostname_resolved(uint16_t id, int result, char *name, struct in_addr *in, int arg)
{
   struct SOCK *client = socketlist;
   
   while (client)
   {
      if (client->queryid == id)
      {
         client->queryid = -1;
         if (result == 0)
         {
            print("Hostname %s resolved to %s (request %d, fd %d)", name, inet_ntoa(*in), id, client->fd);
            if (initiate_connection(client, *in, arg) != 0)
            {
               if (client->forward) free_socket(client->forward);
               free_socket(client);
            }
         }
         else
         {
            print("Cannot resolve hostname %s (request %d, fd %d)", name, id, client->fd);
            request_error(client, DNS_ERROR, name);
         }
         return;
      }
      client = client->next;
   }
}

static int relay_header(char *s)
{
   if (strncasecmp(s, "keep-alive", 10) == 0 || strncasecmp(s, "proxy-connection", 16) == 0) return 0;
   return 1;
}

static int connect_request(struct SOCK *client)
{
   char *host, *port, *protocol, *authstr, *s;
   int dstport, colon, len, res, i;
   struct in_addr in;

   colon = 0;
   host = NULL;
   port = NULL;
   authstr = NULL;
   protocol = NULL;
   print("Proxy - CONNECT request, fd %d", client->fd);
   if (!(s = get_token()))
   {
     request_error(client, SYNTAX_ERROR, NULL);
     return 0;
   }
   len = strlen(s);
   for (i = 0; i < len; i++)
   {
      if (s[i] == ':')
      {
         colon = 1;
         s[i] = '\0'; 
         if (i + 1 < len) port = s + i + 1;
         break;
      }
   }
   host = s;
   if (!port)
   {
      if (!(s = get_token()))
      {
         request_error(client, SYNTAX_ERROR, NULL);
         return 0;
      }
      if (strcasecmp("http/1.0", s) == 0 || strcasecmp("http/1.1", s) == 0)
      {
         port = "443";
         protocol = s;
      }
      else if (colon) port = s;
      else if (strcmp(s, ":") == 0)
      {
         if (!(s = get_token()))
         {
            request_error(client, SYNTAX_ERROR, NULL);
            return 0;
         }
         if (strcasecmp("http/1.0", s) == 0 || strcasecmp("http/1.1", s) == 0) 
         {
            port = "443";
            protocol = s;
         }
         else port = s;
      }
      else if (s[0] == ':') port = s + 1;
      else
      {
         request_error(client, SYNTAX_ERROR, NULL);
         return 0;
      }
   }
   if (!protocol)
   {
      if (!(s = get_token()) || (strcasecmp("http/1.0", s) != 0 && strcasecmp("http/1.1", s) != 0))
      {
         request_error(client, SYNTAX_ERROR, NULL);
         return 0;
      }
      protocol = s;
   }
   if (get_token())
   {
      request_error(client, SYNTAX_ERROR, NULL);
      return 0;
   }
   len = strlen(host);
   dstport = atoi(port);
   if (len < 1 || len > MAX_HOST_LENGTH || dstport < 1 || dstport > 65535)
   {
      request_error(client, SYNTAX_ERROR, NULL);
      return 0;
   }
   while (parse_line_crlf())
   if ((s = get_token()) && strncasecmp(s, "proxy-authorization", 19) == 0)
   {
      if (s[19] == ':' && s[20] == '\0')
      {
         if ((s = get_token()) && strcasecmp(s, "basic") == 0)
         {
            authstr = get_token();
            if (authstr && !get_token()) break; 
         } 
      }
      else if (s[19] == '\0')
      {
         if ((s = get_token()) && ((strcmp(s, ":") == 0 && (s = get_token()) && strcasecmp(s, "basic") == 0) || strcasecmp(s, ":basic") == 0))
         { 
            authstr = get_token();
            if (authstr && !get_token()) break;
         }
      }
      request_error(client, SYNTAX_ERROR, NULL);
      return 0;
   }
   if (!authstr || !authorized_user(authstr, inet_ntoa(client->sockaddr.sin_addr), NORMAL_USER)) 
   {
      print("CONNECT - unauthorized access from %s, fd %d", inet_ntoa(client->sockaddr.sin_addr), client->fd);
      request_error(client, PROXY_AUTH_ERROR, NULL);
      return 0;
   }
   time(&client->idle);
   client->timeout = IDLE_TIMEOUT;
   client->readhandler = read_socket;
   res = resolve_hostname(client, host, dstport, &in);
   if (res == 0) return initiate_connection(client, in, dstport);
   else if (res != EQUERYINPROGRESS) request_error(client, INTERNAL_ERROR, NULL);
   return 0;
}

static int get_request(struct SOCK *client)
{
   char *s, *path, *tmp;
   char authstr[MAX_AUTH_LENGTH+1];
   char host[MAX_HOST_LENGTH+1];
   int dstport, offset, len, res, authheader;
   struct in_addr in;
   
   offset = 0;
   authheader = 0;
   authstr[0] = '\0';
   print("Proxy - GET request, fd %d", client->fd);
   if (!(s = get_token()))
   {
      request_error(client, SYNTAX_ERROR, NULL);
      return 0;
   }
   if (strncmp(s, "http://", 7) != 0)
   {
      if (strncmp(s, "ftp://", 6) == 0) request_error(client, NOTSUPPORTED_ERROR, NULL);
      else request_error(client, SYNTAX_ERROR, NULL);
      return 0;
   } 
   s += 7;
   tmp = s;
   s = strchr(tmp, '/');
   if (!s)
   {
      request_error(client, SYNTAX_ERROR, NULL);
      return 0;
   }
   s[0] = '\0';
   path = s + 1;
   s = strstr(tmp, ":");
   if (!s) dstport = 80;
   else
   {
      *s++ = '\0';
      dstport = atoi(s);
      if (dstport < 1 || dstport > 65535)
      {
         request_error(client, SYNTAX_ERROR, NULL);
         return 0;
      }
   }
   len = strlen(tmp);
   if (len < 1 || len > MAX_HOST_LENGTH)
   {
      request_error(client, SYNTAX_ERROR, NULL);
      return 0;
   }
   strcpy(host, tmp);
   if (!(s = get_token()) || (strcasecmp("http/1.0", s) != 0 && strcasecmp("http/1.1", s) != 0) || get_token())
   {
      request_error(client, SYNTAX_ERROR, NULL);
      return 0;
   }
   memcpy(client->buffer, "GET /", 5);
   offset += 5;
   len = strlen(path);
   memmove(client->buffer + offset, path, len);
   offset += len;
   memcpy(client->buffer + offset, " HTTP/1.0\r\n", 11);
   offset += 11;
   while (parse_line_crlf() && (s = get_token()))
   {
      if (!authheader && strncasecmp(s, "proxy-authorization", 19) == 0)
      {
         authheader = 1;
         if (s[19] == ':' && s[20] == '\0')
         {
            if ((s = get_token()) && strcasecmp(s, "basic") == 0)
            {
               if ((s = get_token()) && !get_token())
               {
                  if (strlen(s) <= MAX_AUTH_LENGTH) strcpy(authstr, s);
                  continue;
               }
            }
         }
         else if (s[19] == '\0')
         {
            if ((s = get_token()) && ((strcmp(s, ":") == 0 && (s = get_token()) && strcasecmp(s, "basic") == 0) || strcasecmp(s, ":basic") == 0))
            {
               if ((s = get_token()) && !get_token())
               {
                  if (strlen(s) <= MAX_AUTH_LENGTH) strcpy(authstr, s);
                  continue;
               }
            }
         }
         request_error(client, SYNTAX_ERROR, NULL);
         return 0;
      }
      if (relay_header(s))
      {
         do
         {
            len = strlen(s);
            memmove(client->buffer + offset, s, len);
            offset += len;
            client->buffer[offset] = ' ';
            offset += 1; 
         }
         while ((s = get_token()));
         client->buffer[offset-1] = '\r';
         client->buffer[offset] = '\n';
         offset += 1;
      }
   }
   memcpy(client->buffer + offset, "\r\n", 2);
   offset += 2;
   //printf("authorized=%d\n",authorized_user(authstr, inet_ntoa(client->sockaddr.sin_addr), NORMAL_USER));
   if (!authstr[0] || !authorized_user(authstr, inet_ntoa(client->sockaddr.sin_addr), NORMAL_USER))
   {
      //if(!authstr[0]) printf("test\n");
      print("GET - unauthorized access from %s, fd %d", inet_ntoa(client->sockaddr.sin_addr), client->fd);
      request_error(client, PROXY_AUTH_ERROR, NULL);
      return 0;
   }
   time(&client->idle);
   client->timeout = IDLE_TIMEOUT;
   client->bufferlen = offset;
   client->readhandler = read_socket;
   res = resolve_hostname(client, host, dstport, &in);
   if (res == 0) return initiate_connection(client, in, dstport);
   else if (res != EQUERYINPROGRESS) request_error(client, INTERNAL_ERROR, NULL);
   return 0;
}

static int post_request(struct SOCK *client, int contentlen)
{
   char *s, *path, *tmp;
   int dstport, offset, len, authheader, contentheader, res;
   char authstr[MAX_AUTH_LENGTH+1];
   char host[MAX_HOST_LENGTH+1];
   struct in_addr in;

   offset = 0;
   authheader = 0;
   contentheader = 0;
   authstr[0] = '\0';
   print("Proxy - POST request, fd %d", client->fd);
   if (!(s = get_token()))
   {
      request_error(client, SYNTAX_ERROR, NULL);
      return 0;
   }
   if (strncmp(s, "http://", 7) != 0)
   {
      if (strncmp(s, "ftp://", 6) == 0) request_error(client, NOTSUPPORTED_ERROR, NULL);
      else request_error(client, SYNTAX_ERROR, NULL);
      return 0;
   }
   s += 7;
   tmp = s;
   s = strchr(tmp, '/');
   if (!s)
   {
      request_error(client, SYNTAX_ERROR, NULL);
      return 0;
   }
   s[0] = '\0';
   path = s + 1;
   s = strstr(tmp, ":");
   if (!s) dstport = 80;
   else
   {
      *s++ = '\0';
      dstport = atoi(s);
      if (dstport < 1 || dstport > 65535)
      {
         request_error(client, SYNTAX_ERROR, NULL);
         return 0;
      }
   }
   len = strlen(tmp);
   if (len < 1 || len > MAX_HOST_LENGTH)
   {
      request_error(client, SYNTAX_ERROR, NULL);
      return 0;
   }
   strcpy(host, tmp);
   if (!(s = get_token()) || (strcasecmp("http/1.0", s) != 0 && strcasecmp("http/1.1", s) != 0) || get_token())
   {
      request_error(client, SYNTAX_ERROR, NULL);
      return 0;
   }
   memcpy(client->buffer, "POST /", 6);
   offset += 6;
   len = strlen(path);
   memmove(client->buffer + offset, path, len);
   offset += len;
   memcpy(client->buffer + offset, " HTTP/1.0\r\n", 11);
   offset += 11;
   while (parse_line_crlf() && (s = get_token()))
   {
      if (!authheader && strncasecmp(s, "proxy-authorization", 19) == 0)
      {
         authheader = 1;
         if (s[19] == ':' && s[20] == '\0')
         {
            if ((s = get_token()) && strcasecmp(s, "basic") == 0)
            {
               if ((s = get_token()) && !get_token())
               {
                  if (strlen(s) <= MAX_AUTH_LENGTH) strcpy(authstr, s);
                  continue;
               }
            }
         }
         else if (s[19] == '\0')
         {
            if ((s = get_token()) && ((strcmp(s, ":") == 0 && (s = get_token()) && strcasecmp(s, "basic") == 0) || strcasecmp(s, ":basic") == 0))
            {
               if ((s = get_token()) && !get_token())
               {
                  if (strlen(s) <= MAX_AUTH_LENGTH) strcpy(authstr, s);
                  continue;
               }
            }
         }
         request_error(client, SYNTAX_ERROR, NULL);
         return 0;
      }
      if (relay_header(s))
      {
         if (!contentheader && strncasecmp(s, "content", 7) == 0) contentheader = 1;
         do
         {
            len = strlen(s);
            memmove(client->buffer + offset, s, len);
            offset += len;
            client->buffer[offset] = ' ';
            offset += 1;
         }
         while ((s = get_token()));
         client->buffer[offset-1] = '\r';
         client->buffer[offset] = '\n';
         offset += 1;
      }
   }
   if (!contentheader)
   {
      print("POST request without CONTENT header, fd %d", client->fd);
      request_error(client, SYNTAX_ERROR, NULL);
      return 0;
   }
   memcpy(client->buffer + offset, "\r\n", 2);
   offset += 2;
   if (contentlen > 0)
   {
      memmove(client->buffer + offset, client->buffer + client->bufferlen - contentlen, contentlen);
      offset += contentlen;
   }
   if (!authstr[0] || !authorized_user(authstr, inet_ntoa(client->sockaddr.sin_addr), NORMAL_USER))
   {
      print("POST - unauthorized access from %s, fd %d", inet_ntoa(client->sockaddr.sin_addr), client->fd);
      request_error(client, PROXY_AUTH_ERROR, NULL);
      return 0;
   }
   time(&client->idle);
   client->timeout = IDLE_TIMEOUT;
   client->bufferlen = offset;
   client->readhandler = read_socket;
   res = resolve_hostname(client, host, dstport, &in);
   if (res == 0) return initiate_connection(client, in, dstport);
   else if (res != EQUERYINPROGRESS) request_error(client, INTERNAL_ERROR, NULL);
   return 0;
}

int proxy_handle_client(struct SOCK *client)
{
   int count;
   char *req, *s;

   count = recv(client->fd, client->buffer + client->bufferlen, sizeof(client->buffer) - client->bufferlen, 0);
   if (count > 0)
   {
      print("Read %d bytes from fd %d", count, client->fd);
      client->bufferlen += count;
      if ((req = (char *)memfind(client->buffer, client->bufferlen, "\r\n\r\n", 4)))
      {
         init_tokenizer(client->buffer, NULL, client->bufferlen);
         parse_line_crlf();
         if ((s = get_token()))
         {
            if (strncasecmp("connect", s, 7) == 0)
            {
               client->method = CONNECT_METHOD;
               return connect_request(client);
            }
            if (strncasecmp("get", s, 3) == 0)
            {
               client->method = GET_METHOD;
               return get_request(client);
            }
            if (strncasecmp("post", s, 4) == 0)
            {
               client->method = POST_METHOD;
               return post_request(client, client->bufferlen - (req - client->buffer) - 4);
            }
         }
         request_error(client, SYNTAX_ERROR, NULL);
         return 0;
      }
      if (client->bufferlen == sizeof(client->buffer))
      {
         print("Data buffer exceeded for fd %d - bad request", client->fd);
         return 1;
      }
      return 0;
   }
   else
   {
      if (count == 0) print("EOF received on fd %d", client->fd);
      else printerror("Error reading from fd %d", client->fd);
      return 1;
   }
}
