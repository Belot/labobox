#ifndef __LIBDNS_H
#define __LIBDNS_H
#define MAX_HOST_LENGTH 255
#define ENOMEMORY -1
#define ENORESPONSE -2
#define ERECURSIVEQUERY -3
#define EFORMAT -4
#define ESERVERFAILURE -5
#define ENAMEERROR -6
#define ENOTSUPPORTED -7
#define EREFUSED -8
#define EUNKNOWN -9
#define EPROTOCOL -10
#define EHOSTLENGTH -11
#define EQUERYCOUNTEXCEEDED -12
#define EQUERYTIMEOUT -13
#define ESOCKET -14
#define ENONBLOCK -15
#define ECACHEMISS -16
#define EQUERYINPROGRESS -17
typedef void (*query_callback)(uint16_t id, int result, char *name, struct in_addr *in, int arg);
int gethostbyname_a(char *name, query_callback callback, int arg, uint16_t *queryid, struct in_addr *inp);
int libdns_init(struct sockaddr_in *dns);
void libdns_setdns(struct sockaddr_in dns);
int libdns_fds(fd_set *read_fds, fd_set *write_fds);
void libdns_free(void);
void libdns_process(fd_set *readfds, fd_set *writefds);
#endif
