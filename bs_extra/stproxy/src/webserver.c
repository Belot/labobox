/*
 * Copyright (C) 2007 Adam Hurkala (ahurkala@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "common.h"
#include "debug.h"
#include "auth.h"
#include "sock.h"
#include "reqerror.h"
#include "strutils.h"
#include "webserver.h"

extern int clientcount;
static time_t start;

static void add_header(char *buffer, char *title)
{
   strcpy(buffer,
          "HTTP/1.0 200 OK\r\n"
          "Server: "PROXY_VERSION"\r\n"
          "Content-Type: text/html\r\n\r\n"
          "<html>\n"
          "<head>\n"
          "<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">\n"
          "<title>");
   strcat(buffer, title);
   strcat(buffer,
          "</title>\n"
          "</head>\n"
          "<body>\n"
          "<table width=\"100%%\" height=\"100%%\" frame=\"void\" rules=\"all\" border=\"1\">\n"
          "<tr>\n"
          "<td width=\"20%%\">\n"
          "<a href=\"showuser.html\">Show user</a><br>\n"
          "<a href=\"adduser.html\">Add user</a><br>\n"
          "<a href=\"deluser.html\">Delete user</a><br>\n"
          "<a href=\"addip.html\">Add IP</a><br>\n"
          "<a href=\"delip.html\">Delete IP</a><br>\n"
          "<a href=\"chpass.html\">Change password</a><br>\n"
          "<a href=\"status.html\">Proxy status</a><br>\n"
          "</td>\n"
          "<td align=\"center\">\n");
}

static void add_body(char *buffer, char *str)
{
   strcat(buffer, str);
}

static void add_footer(char *buffer)
{
   strcat(buffer,
          "</td>\n"
          "</tr>\n"
          "</table>\n"
          "</body>\n"
          "</html>\n");
}

static char *get_uptime(void)
{
   int days, minutes, hours;
   unsigned int seconds;
   static char uptime[64];
   char tmp[32];
   
   uptime[0] = '\0';
   seconds = difftime(time(NULL), start);
   days = seconds / 86400;
   seconds -= days * 86400;
   hours = seconds / 3600;
   seconds -= hours * 3600;
   minutes = seconds / 60;
   seconds -= minutes * 60;
   if (days > 0)
   {
      sprintf(tmp, "%d", days);
      if (days == 1) strcat(tmp, " day ");
      else strcat(tmp, " days ");
      strcat(uptime, tmp);
   }
   if (hours > 0)
   {
      sprintf(tmp, "%d", hours);
      if (hours == 1) strcat(tmp, " hour ");
      else strcat(tmp, " hours ");
      strcat(uptime, tmp);
   }
   if (minutes > 0)
   {
      sprintf(tmp, "%d", minutes);
      if (minutes == 1) strcat(tmp, " minute ");
      else strcat(tmp, " minutes ");
      strcat(uptime, tmp);
   }
   sprintf(tmp, "%d", seconds);
   if (seconds == 1) strcat(tmp, " second");
   else strcat(tmp, " seconds");
   strcat(uptime, tmp);
   return uptime;
}

static int show_page(struct SOCK *client, int page)
{
   switch(page)
   {
      case ROOT_PAGE:
        print("Showing root page, fd %d", client->fd);
        add_header(client->buffer, "Proxy configuration");
        add_body(client->buffer, "Proxy configuration interface\n");
        add_footer(client->buffer);
        break;
      case SHOWUSER_PAGE:
        print("Showing showuser page, fd %d", client->fd);
        add_header(client->buffer, "Show user");
        add_body(client->buffer,
                 "<form method=\"GET\" action=\"showuser\">\n"
                 "<table border=\"0\">\n"
                 "<tr><td>Username:</td><td><input type=\"text\" name=\"username\"></td></tr>\n"
                 "<tr><td colspan=\"2\"><center><input type=\"reset\" value=\"Reset\">&nbsp;<input type=\"submit\" value=\"Submit\"></center></td></tr>\n"
                 "</table>\n"
                 "</form>\n");
         add_footer(client->buffer);
         break;
      case ADDUSER_PAGE:
        print("Showing adduser page, fd %d", client->fd);
        add_header(client->buffer, "Add user");
        add_body(client->buffer,
                 "<form method=\"GET\" action=\"adduser\">\n"
                 "<table border=\"0\">\n"
                 "<tr><td>Username:</td><td><input type=\"text\" name=\"username\"></td></tr>\n"
                 "<tr><td>Password:</td><td><input type=\"password\" name=\"password\"></td></tr>\n"
                 "<tr><td>Privilege:</td><td><select name=\"privilege\"><option value=\"1\">Normal User<option value=\"0\">Admin</select>"
                 "</td></tr>\n"
                 "<tr><td>IP Mask 1:</td><td><input type=\"text\" name=\"ipmask1\"></td></tr>\n"
                 "<tr><td>IP Mask 2:</td><td><input type=\"text\" name=\"ipmask2\"></td></tr>\n"
                 "<tr><td>IP Mask 3:</td><td><input type=\"text\" name=\"ipmask3\"></td></tr>\n"
                 "<tr><td>IP Mask 4:</td><td><input type=\"text\" name=\"ipmask4\"></td></tr>\n"
                 "<tr><td colspan=\"2\"><center><input type=\"reset\" value=\"Reset\">&nbsp;<input type=\"submit\" value=\"Submit\"></center></td></tr>\n"
                 "</table>\n"
                 "</form>\n");
        add_footer(client->buffer);
        break;
      case DELUSER_PAGE:
        print("Showing deluser page, fd %d", client->fd);
        add_header(client->buffer, "Delete user");
        add_body(client->buffer,
                 "<form method=\"GET\" action=\"deluser\">\n"
                 "<table border=\"0\">\n"
                 "<tr><td>Username:</td><td><input type=\"text\" name=\"username\"></td></tr>\n"
                 "<tr><td colspan=\"2\"><center><input type=\"reset\" value=\"Reset\">&nbsp;<input type=\"submit\" value=\"Submit\"></center></td></tr>\n"
                 "</table>\n"
                 "</form>\n");
        add_footer(client->buffer);
        break;
      case ADDIP_PAGE:
        print("Showing addip page, fd %d", client->fd);
        add_header(client->buffer, "Add IP");
        add_body(client->buffer,
                 "<form method=\"GET\" action=\"addip\">\n"
                 "<table border=\"0\">\n"
                 "<tr><td>Username:</td><td><input type=\"text\" name=\"username\"></td></tr>\n"
                 "<tr><td>IP Mask:</td><td><input type=\"text\" name=\"ipmask\"></td></tr>\n"
                 "<tr><td colspan=\"2\"><center><input type=\"reset\" value=\"Reset\">&nbsp;<input type=\"submit\" value=\"Submit\"></center></td></tr>\n"
                 "</table>\n"
                 "</form>\n");
        add_footer(client->buffer);
        break;
      case DELIP_PAGE:
        print("Showing delip page, fd %d", client->fd);
        add_header(client->buffer, "Delete IP");
        add_body(client->buffer,
                 "<form method=\"GET\" action=\"delip\">\n"
                 "<table border=\"0\">\n"
                 "<tr><td>Username:</td><td><input type=\"text\" name=\"username\"></td></tr>\n"
                 "<tr><td>IP Mask:</td><td><input type=\"text\" name=\"ipmask\"></td></tr>\n"
                 "<tr><td colspan=\"2\"><center><input type=\"reset\" value=\"Reset\">&nbsp;<input type=\"submit\" value=\"Submit\"></center></td></tr>\n"
                 "</table>\n"
                 "</form>\n");
        add_footer(client->buffer);
        break;
      case CHPASS_PAGE:
        print("Showing chpass page, fd %d", client->fd);
        add_header(client->buffer, "Change password");
        add_body(client->buffer,
               "<form method=\"GET\" action=\"chpass\">\n"
               "<table border=\"0\">\n"
               "<tr><td>Username:</td><td><input type=\"text\" name=\"username\"></td></tr>\n"
               "<tr><td>New password:</td><td><input type=\"password\" name=\"password\"></td></tr>\n"
               "<tr><td colspan=\"2\"><center><input type=\"reset\" value=\"Reset\">&nbsp;<input type=\"submit\" value=\"Submit\"></center></td></tr>\n"
               "</table>\n"
               "</form>\n");
        add_footer(client->buffer);
        break;
      default:
        print("Requested page not found, fd %d", client->fd);
        strcpy(client->buffer,
               "HTTP/1.0 404 Not Found\r\n"
               "Server: "PROXY_VERSION"\r\n"
               "Content-Type: text/html\r\n\r\n"
               "<html>\n"
               "<head>\n"
               "<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">\n"
               "<title>404 Not Found</title>\n"
               "</head>\n"
               "<body>\n"
               "<H1>Not Found</H1>The requested URL was not found on this server.\n"
               "</body>\n"
               "</html>\n");
         break;
   }
   client->bufferlen = strlen(client->buffer);
   client->readhandler = NULL;
   client->writehandler = close_socket;   
   return 0;
}

static int showstatus_request(struct SOCK *client)
{
   static char buffer[512];

   print("Web server - showstatus request, fd %d", client->fd);
   sprintf(buffer,
           "<H3>Proxy status</H3>\n"
           "Connected users: %d<br>\n"
           "Uptime: %s\n", clientcount, get_uptime());

   add_header(client->buffer, "Proxy status");
   add_body(client->buffer, buffer);
   add_footer(client->buffer);
   client->bufferlen = strlen(client->buffer);
   client->readhandler = NULL;
   client->writehandler = close_socket;
   return 0;
}

static int showuser_request(struct SOCK *client, char *args)
{
   int res, i;
   char *username, buffer[BUF_LENGTH], tmp[12];
   struct user *user;

   print("Web server - showuser request, fd %d", client->fd);
   if (strncmp(args, "username=", 9) != 0)
   {
      request_error(client, SYNTAX_ERROR, NULL);
      return 0;
   }
   username = args + 9;
   add_header(buffer, "Show user - Result");
   res = get_user(username, &user);
   switch(res)
   {
      case 0:
        add_body(buffer, "Username: ");
        add_body(buffer, username);
        add_body(buffer, "<br>\n");
        if (user->authority == SUPER_USER) add_body(buffer, "Privilege: Admin<br>\n");
        else add_body(buffer, "Privilege: Normal User<br>\n");
        i = 0;
        while (user->ipmask[i])
        {
           sprintf(tmp, "%d", i + 1);
           add_body(buffer, "IP Mask ");
           add_body(buffer, tmp);
           add_body(buffer, ": ");
           add_body(buffer, user->ipmask[i]);
           add_body(buffer, "<br>\n");
           i++;
        }
        break;
      case EUSERNAMELENGTH: 
        add_body(buffer, "Username must be between 1 and "string(MAX_USERNAME_LENGTH)" characters.<br>\n");
        break;
      case EBADUSERNAME:
        add_body(buffer, "Username may not contain a colon (:).<br>\n");
        break;
      case EUSERNOTFOUND:
        add_body(buffer, "User ");
        add_body(buffer, username);
        add_body(buffer, " not found.<br>\n"); 
        break;
   }
   if (res != 0) add_body(buffer, "Please check your request and try again.\n");
   add_footer(buffer);
   strcpy(client->buffer, buffer);
   client->bufferlen = strlen(client->buffer);
   client->readhandler = NULL;
   client->writehandler = close_socket;
   return 0;
}

static int adduser_request(struct SOCK *client, char *args)
{
   int i, ipcount;
   char *username, *password, *privilege, *ipmask[MAX_IP_COUNT+1];
   char *s, c;

   print("Web server - adduser request, fd %d", client->fd);
   username = NULL;
   password = NULL;
   privilege = NULL;
   for (i = 0; i < MAX_IP_COUNT + 1; i++) ipmask[i] = NULL;
   ipcount = 0;
   c = '&';
   init_tokenizer(args, &c, strlen(args) + 1);
   parse_string();
   while ((s = get_token()))
   {
      if (strncmp(s, "username=", 9) == 0)
      {
         if (username)
         {
            request_error(client, SYNTAX_ERROR, NULL);
            return 0;
         }
         else username = s + 9;
      }
      else if (strncmp(s, "password=", 9) == 0)
      {
         if (password)
         {
            request_error(client, SYNTAX_ERROR, NULL);
            return 0;
         }
         password = s + 9;
      }
      else if (strncmp(s, "privilege=", 10) == 0)
      {
         if (privilege)
         {
            request_error(client, SYNTAX_ERROR, NULL);
            return 0;
         }
         privilege = s + 10;
      }
      else if (strncmp(s, "ipmask", 6) == 0)
      {
         char *mask, *tmp;

         s += 6;
         tmp = strchr(s, '=');
         if (!tmp)
         {
            request_error(client, SYNTAX_ERROR, NULL);
            return 0;
         }
         tmp[0] = '\0';
         if (atoi(s) == 0)
         { 
            request_error(client, SYNTAX_ERROR, NULL);
            return 0;
         }
         mask = tmp + 1;
         if (strlen(mask) == 0) continue;
         if (ipcount < MAX_IP_COUNT) ipmask[ipcount++] = mask;
         else
         {
            request_error(client, SYNTAX_ERROR, NULL);
            return 0;
         }
      }
      else
      {
         request_error(client, SYNTAX_ERROR, NULL);
         return 0;
      }
   }
   if (username && password && privilege)
   {
      int res1, res2; 
      char buffer[BUF_LENGTH];

      add_header(buffer, "Add user - Result");
      res1 = add_user(username, password, atoi(privilege), 1);
      switch(res1)
      {
         case 0:
           add_body(buffer, "User ");
           add_body(buffer, username);
           add_body(buffer, " successfully added.\n");
           for (i = 0; i < ipcount; i++)
           {
              res2 = add_ip(username, ipmask[i]);
              if (res2 == 0)
              { 
                 add_body(buffer, "<br>IP ");
                 add_body(buffer, ipmask[i]);
                 add_body(buffer, " successfully added to ");
                 add_body(buffer, username);
                 add_body(buffer, ".\n");
              }
              else if (res2 == EIPLENGTH)
              {
                 char tmp[12];

                 sprintf(tmp, "%d", i + 1);
                 add_body(buffer, "<br>IP Mask ");
                 add_body(buffer, tmp);
                 add_body(buffer, " exceeds maximum length of "string(MAX_IP_LENGTH)" characters.\n");
              }
              else if (res2 == EIPMASK)
              {
                 add_body(buffer, "<br>IP ");
                 add_body(buffer, ipmask[i]);
                 add_body(buffer, " is not valid.\n");
              }
              else if (res2 == EIPEXISTS)
              {
                 add_body(buffer, "<br>IP ");
                 add_body(buffer, ipmask[i]);
                 add_body(buffer, " is already added to "); 
                 add_body(buffer, username);
                 add_body(buffer, ".\n");
              }
              else if (res2 == EIPCOUNT)
              {
                 add_body(buffer, "<br>User ");
                 add_body(buffer, username);
                 add_body(buffer, " has maximum number of IPs ("string(MAX_IP_COUNT)") added.\n");
                 break;
              }
           } 
           update_db(0);
           break;
         case EUSERNAMELENGTH:
           add_body(buffer, "Username must be between 1 and "string(MAX_USERNAME_LENGTH)" characters.<br>\n");
           break;
         case EPASSWORDLENGTH:
           add_body(buffer, "Password must be between 1 and "string(MAX_PASSWORD_LENGTH)" characters.<br>\n");
           break;
         case EBADUSERNAME:
           add_body(buffer, "Username may not contain a colon (:).<br>\n");
           break;
         case EUSEREXISTS:
           add_body(buffer, "User ");
           add_body(buffer, username);
           add_body(buffer, " already exists.<br>\n");
           break;
      }
      if (res1 != 0) add_body(buffer, "Please check your request and try again.\n");
      add_footer(buffer);
      strcpy(client->buffer, buffer);
      client->bufferlen = strlen(client->buffer);
      client->readhandler = NULL;
      client->writehandler = close_socket;
   }
   else request_error(client, SYNTAX_ERROR, NULL);
   return 0;
}

static int deluser_request(struct SOCK *client, char *args)
{
   int res;
   char *username, buffer[BUF_LENGTH];

   print("Web server - deluser request, fd %d", client->fd);
   if (strncmp(args, "username=", 9) != 0)
   {
      request_error(client, SYNTAX_ERROR, NULL);
      return 0;
   }
   username = args + 9;
   add_header(buffer, "Delete user - Result");
   res = delete_user(username);
   switch(res)
   {
      case 0:
        add_body(buffer, "User ");
        add_body(buffer, username);
        add_body(buffer, " successfully deleted.\n");
        update_db(1);
        break;
      case EUSERNAMELENGTH:
        add_body(buffer, "Username must be between 1 and "string(MAX_USERNAME_LENGTH)" characters.<br>\n");
        break;
      case EBADUSERNAME:
        add_body(buffer, "Username may not contain a colon (:).<br>\n");
        break;
      case EUSERNOTFOUND: 
        add_body(buffer, "User ");
        add_body(buffer, username);
        add_body(buffer, " not found.<br>\n");
        break;
   }
   if (res != 0) add_body(buffer, "Please check your request and try again.\n");
   add_footer(buffer);
   strcpy(client->buffer, buffer);
   client->bufferlen = strlen(client->buffer);
   client->readhandler = NULL;
   client->writehandler = close_socket;
   return 0;
}

static int addip_request(struct SOCK *client, char *args)
{
   char *username, *ipmask;
   char *s, c;

   print("Web server - addip request, fd %d", client->fd);
   username = NULL;
   ipmask = NULL;
   c = '&';
   init_tokenizer(args, &c, strlen(args) + 1);
   parse_string();
   while ((s = get_token()))
   {
      if (strncmp(s, "username=", 9) == 0)
      {
         if (username)
         {
            request_error(client, SYNTAX_ERROR, NULL);
            return 0;  
         } 
         username = s + 9;
      }
      else if (strncmp(s, "ipmask=", 7) == 0)
      {
         if (ipmask)
         {
            request_error(client, SYNTAX_ERROR, NULL);
            return 0;
         }
         ipmask = s + 7;
      }
      else
      {
         request_error(client, SYNTAX_ERROR, NULL);
         return 0;
      }
   }
   if (username && ipmask)
   {
      int res;
      char buffer[BUF_LENGTH];

      add_header(buffer, "Add IP - Result");
      res = add_ip(username, ipmask);
      switch(res)
      {
         case 0:
           add_body(buffer, "IP ");
           add_body(buffer, ipmask);
           add_body(buffer, " successfully added.\n");
           update_db(1);
           break;
         case EUSERNAMELENGTH:
           add_body(buffer, "Username must be between 1 and "string(MAX_USERNAME_LENGTH)" characters.<br>\n");
           break;
         case EIPLENGTH:
           add_body(buffer, "IP must be between 1 and "string(MAX_IP_LENGTH)" characters.<br>\n");
           break;
         case EIPCOUNT:
           add_body(buffer, "User ");
           add_body(buffer, username);
           add_body(buffer, " has maximum number of IPs ("string(MAX_IP_COUNT)") added.<br>\n");
           break;
         case EIPMASK:
           add_body(buffer, 
                    "IP address must be specified as a.b.c.d where a, b, c and d are in the range of 0 to 255.<br>\n"
                    "Additionally the asterisk (*) may be used, for example \"127.0.0.*\", \"127.0.*\", \"127.*\", \"*\".<br>\n");
           break;
         case EIPEXISTS:
           add_body(buffer, "IP ");
           add_body(buffer, ipmask);
           add_body(buffer, " is already added.<br>\n");
           break;
         case EBADUSERNAME:
           add_body(buffer, "Username may not contain a colon (:).<br>\n");
           break;
         case EUSERNOTFOUND:
           add_body(buffer, "User ");
           add_body(buffer, username);
           add_body(buffer, " not found.<br>\n");
           break;
      }
      if (res != 0) add_body(buffer, "Please check your request and try again.\n");
      add_footer(buffer);
      strcpy(client->buffer, buffer);
      client->bufferlen = strlen(client->buffer);
      client->readhandler = NULL;
      client->writehandler = close_socket;
   }
   else request_error(client, SYNTAX_ERROR, NULL);
   return 0;
}

static int delip_request(struct SOCK *client, char *args)
{
   char *username, *ipmask;
   char *s, c;

   print("Web server - delip request, fd %d", client->fd);
   username = NULL;
   ipmask = NULL;
   c = '&'; 
   init_tokenizer(args, &c, strlen(args) + 1);
   parse_string();
   while ((s = get_token()))
   {
      if (strncmp(s, "username=", 9) == 0)
      {
         if (username)
         {
            request_error(client, SYNTAX_ERROR, NULL);
            return 0;
         }
         username = s + 9;
      }
      else if (strncmp(s, "ipmask=", 7) == 0)
      {
         if (ipmask)
         {
            request_error(client, SYNTAX_ERROR, NULL);
            return 0;
         }
         ipmask = s + 7;
      }
      else
      {
         request_error(client, SYNTAX_ERROR, NULL);
         return 0;
      }
   }
   if (username && ipmask)
   {
      int res;
      char buffer[BUF_LENGTH];

      add_header(buffer, "Delete IP - Result");
      res = delete_ip(username, ipmask);
      switch(res)
      {
         case 0:
           add_body(buffer, "IP ");
           add_body(buffer, ipmask);
           add_body(buffer, " successfully deleted.\n");
           update_db(1);
           break;
         case EUSERNAMELENGTH:
           add_body(buffer, "Username must be between 1 and "string(MAX_USERNAME_LENGTH)" characters.<br>\n");
           break;
         case EIPLENGTH:
           add_body(buffer, "IP must be between 1 and "string(MAX_IP_LENGTH)" characters.<br>\n");
           break;
         case EIPMASK:
           add_body(buffer, "IP ");
           add_body(buffer, ipmask);
           add_body(buffer, " is not valid.<br>\n");
           break; 
         case EBADUSERNAME:
           add_body(buffer, "Username may not contain a colon (:).<br>\n");
           break;
         case EUSERNOTFOUND:
           add_body(buffer, "User ");
           add_body(buffer, username);
           add_body(buffer, " not found.<br>\n");
           break;
         case EIPNOTFOUND: 
           add_body(buffer, "IP ");
           add_body(buffer, ipmask);
           add_body(buffer, " is not added.<br>\n");
           break;
      }
      if (res != 0) add_body(buffer, "Please check your request and try again.\n");
      add_footer(buffer);
      strcpy(client->buffer, buffer);
      client->bufferlen = strlen(client->buffer);
      client->readhandler = NULL;
      client->writehandler = close_socket;
   }
   else request_error(client, SYNTAX_ERROR, NULL);
   return 0;
}

static int chpass_request(struct SOCK *client, char *args)
{
   char *username, *password;
   char *s, c;

   print("Web server - chpass request, fd %d", client->fd);
   username = NULL;
   password = NULL;
   c = '&';
   init_tokenizer(args, &c, strlen(args) + 1);
   parse_string();
   while ((s = get_token()))
   {
      if (strncmp(s, "username=", 9) == 0)
      {
         if (username)
         {
            request_error(client, SYNTAX_ERROR, NULL);
            return 0;
         }
         username = s + 9;
      }
      else if (strncmp(s, "password=", 9) == 0)
      {
         if (password)
         {
            request_error(client, SYNTAX_ERROR, NULL);
            return 0;
         }
         password = s + 9;
      }
      else
      {
         request_error(client, SYNTAX_ERROR, NULL);
         return 0;
      }
   }
   if (username && password)
   {
      int res;
      char buffer[BUF_LENGTH];

      add_header(buffer, "Change password - Result");
      res = change_password(username, password);
      switch(res)
      {
         case 0: 
           add_body(buffer, "Password successfully changed for user ");
           add_body(buffer, username);
           add_body(buffer, ".\n");
           update_db(1);
           break;
         case EUSERNAMELENGTH:
           add_body(buffer, "Username must be between 1 and "string(MAX_USERNAME_LENGTH)" characters.<br>\n");
           break;
         case EPASSWORDLENGTH:
           add_body(buffer, "Password must be between 1 and "string(MAX_PASSWORD_LENGTH)" characters.<br>\n");
           break;
         case EBADUSERNAME:
           add_body(buffer, "Username may not contain a colon (:).<br>\n");
           break;
         case EUSERNOTFOUND:
           add_body(buffer, "User ");
           add_body(buffer, username);
           add_body(buffer, " not found.<br>\n");
           break;
      }
      if (res != 0) add_body(buffer, "Please check your request and try again.\n");
      add_footer(buffer);
      strcpy(client->buffer, buffer);
      client->bufferlen = strlen(client->buffer);
      client->readhandler = NULL;
      client->writehandler = close_socket;
   }
   else request_error(client, SYNTAX_ERROR, NULL);
   return 0;
}

static int get_request(struct SOCK *client)
{
   char *authstr, *s, *url;

   authstr = NULL;
   print("Web server - GET request, fd %d", client->fd);
   if (!(url = get_token()))
   {
      request_error(client, SYNTAX_ERROR, NULL);
      return 0;
   }
   if (!(s = get_token()) || (strcasecmp("http/1.0", s) != 0 && strcasecmp("http/1.1", s) != 0) || get_token())
   {
      request_error(client, SYNTAX_ERROR, NULL);
      return 0;
   }
   while (parse_line_crlf())
   if ((s = get_token()) && strncasecmp(s, "authorization", 13) == 0)
   {
      if (s[13] == ':' && s[14] == '\0')
      {
         if ((s = get_token()) && strcasecmp(s, "basic") == 0)
         {
            authstr = get_token();
            if (authstr && !get_token()) break;
         }
      }
      else if (s[13] == '\0')
      {
         if ((s = get_token()) && ((strcmp(s, ":") == 0 && (s = get_token()) && strcasecmp(s, "basic") == 0) || strcasecmp(s, ":basic") == 0))
         {
            authstr = get_token();
            if (authstr && !get_token()) break;
         }
      }
      request_error(client, SYNTAX_ERROR, NULL);
      return 0;
   }
   if (!authstr || !authorized_user(authstr, inet_ntoa(client->sockaddr.sin_addr), SUPER_USER))
   {
      request_error(client, WEBSERVER_AUTH_ERROR, NULL);
      return 0;
   }
   switch(unescape_url(url))
   {
      case EBADREQUEST:
        request_error(client, SYNTAX_ERROR, NULL);
        return 0;
      case ENOTFOUND: 
        return show_page(client, NOTFOUND_PAGE);
   }
   if (strcmp(url, "/") == 0) return show_page(client, ROOT_PAGE);
   if (strcmp(url, "/index.html") == 0) return show_page(client, ROOT_PAGE);
   if (strcmp(url, "/showuser.html") == 0) return show_page(client, SHOWUSER_PAGE);
   if (strcmp(url, "/adduser.html") == 0) return show_page(client, ADDUSER_PAGE);
   if (strcmp(url, "/deluser.html") == 0) return show_page(client, DELUSER_PAGE);
   if (strcmp(url, "/addip.html") == 0) return show_page(client, ADDIP_PAGE);
   if (strcmp(url, "/delip.html") == 0) return show_page(client, DELIP_PAGE);
   if (strcmp(url, "/chpass.html") == 0) return show_page(client, CHPASS_PAGE);
   if (strcmp(url, "/status.html") == 0) return showstatus_request(client);
   if (strncmp(url, "/showuser?", 10) == 0) return showuser_request(client, url + 10);
   if (strncmp(url, "/adduser?", 9) == 0) return adduser_request(client, url + 9);
   if (strncmp(url, "/deluser?", 9) == 0) return deluser_request(client, url + 9);
   if (strncmp(url, "/addip?", 7) == 0) return addip_request(client, url + 7);
   if (strncmp(url, "/delip?", 7) == 0) return delip_request(client, url + 7);
   if (strncmp(url, "/chpass?", 8) == 0) return chpass_request(client, url + 8);
   return show_page(client, NOTFOUND_PAGE);
}

int webserver_init(void)
{
   time(&start);
   return 0;
}

int webserver_handle_client(struct SOCK *client)
{
   int count;
   char *s;

   count = recv(client->fd, client->buffer + client->bufferlen, sizeof(client->buffer) - client->bufferlen, 0);
   if (count > 0)
   {
      print("Read %d bytes from fd %d", count, client->fd);
      client->bufferlen += count;
      if (client->bufferlen >= 4 && strncmp("\r\n\r\n", client->buffer + client->bufferlen - 4, 4) == 0)
      {
         init_tokenizer(client->buffer, NULL, client->bufferlen);
         parse_line_crlf();
         if ((s = get_token()) && strncasecmp("get", s, 3) == 0)
         {
            client->method = GET_METHOD;
            return get_request(client);
         }
         request_error(client, SYNTAX_ERROR, NULL);
         return 0;
      }
      else if (client->bufferlen == sizeof(client->buffer))
      {
         print("Data buffer exceeded for fd %d - bad request", client->fd);
         return 1;
      }
      return 0;
   }
   else
   {
      if (count == 0) print("EOF received on fd %d", client->fd);
      else printerror("Error reading from fd %d", client->fd);
      return 1;
   }
}
