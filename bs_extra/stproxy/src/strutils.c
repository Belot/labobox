/*
 * Copyright (C) 2007 Adam Hurkala (ahurkala@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "common.h"
#include "strutils.h"

static int buffoff;
static int bufflen;
static int lineoff;
static int linelen;
static char *str;
static char *delimiter;

static char x2c(const char *what)
{
   register char digit;

   digit = ((what[0] >= 'A') ? ((what[0] & 0xdf) - 'A') + 10 : (what[0] - '0'));
   digit *= 16;
   digit += (what[1] >= 'A' ? ((what[1] & 0xdf) - 'A') + 10 : (what[1] - '0'));
   return (digit);
}

int unescape_url(char *url)
{
   register int x, y, badesc, badpath;

   badesc = 0;
   badpath = 0;
   for (x = 0, y = 0; url[y]; ++x, ++y)
   {
      if (url[y] == '+') url[x] = ' ';
      else if (url[y] != '%') url[x] = url[y];
      else
      {
         if (!isxdigit(url[y + 1]) || !isxdigit(url[y + 2]))
         {
            badesc = 1;
            url[x] = '%';
         }
         else
         {
            url[x] = x2c(&url[y + 1]);
            y += 2;
            if (url[x] == '/' || url[x] == '\0') badpath = 1;
         }
      }
   }
   url[x] = '\0';
   if (badesc) return EBADREQUEST;
   else if (badpath) return ENOTFOUND;
   else return 0;
}

void *memfind(const void *block, size_t blen, const void *pat, size_t plen)
{
   unsigned char *bp, *pp, *endp;

   if (plen == 0 || plen > blen) return NULL;
   bp = (unsigned char *)block;
   pp = (unsigned char *)pat;
   endp = bp + (blen - plen) + 1;
   while (bp < endp)
   {
      if ((*bp == *pp) && (memcmp(bp, pp, plen) == 0))
      return bp;
      bp++;
   }
   return NULL;
}

void init_tokenizer(char *s, char *delim, size_t n)
{
   buffoff = 0;
   lineoff = -1;
   linelen = 0;
   delimiter = delim;
   bufflen = n;
   str = s;
}

int parse_line(void)
{
   int i;

   if (buffoff == -1) return 0;
   for (i = buffoff; i < bufflen; i++)
   if (str[i] == '\n')
   {
      lineoff = buffoff;
      if (i > buffoff && str[i-1] == '\r')
      {
         str[i-1] = '\0';
         linelen = i-1;
      }
      else
      {
         str[i] = '\0';
         linelen = i;
      }
      if (i + 1 < bufflen) buffoff = i + 1;
      else buffoff = -1;
      return 1;
   }
   buffoff = -1;
   lineoff = -1;
   return 0;
}

int parse_line_crlf(void)
{
   int i;

   if (buffoff == -1) return 0;
   for (i = buffoff; i < bufflen - 1; i++)
   if (str[i] == '\r' && str[i+1] == '\n')
   {
      str[i] = '\0';
      lineoff = buffoff;
      linelen = i;
      if (i + 2 < bufflen) buffoff = i + 2;
      else buffoff = -1;
      return 1;
   }
   buffoff = -1;
   lineoff = -1;
   return 0;
}

int parse_string(void)
{
   int i;

   if (buffoff == -1) return 0;
   for (i = buffoff; i < bufflen; i++)
   if (str[i] == '\0')
   {
      lineoff = buffoff;
      linelen = i;
      if (i + 1 < bufflen) buffoff = i + 1;
      else buffoff = -1;
      return 1;
   }
   buffoff = -1;
   lineoff = -1;
   return 0;
}

char *get_token(void)
{
   int i, index;

   if (lineoff == -1) return NULL;
   index = -1;
   for (i = lineoff; i < linelen; i++)
   {
      if (str[i] != ' ' && str[i] != 9 && (!delimiter || str[i] != *delimiter))
      {
         index = i;
         break;
      }
   }
   if (index == -1)
   {
      lineoff = -1;
      return NULL;
   }
   for (i = index + 1; i < linelen; i++)
   {
      if (str[i] == ' ' || str[i] == 9 || (delimiter && str[i] == *delimiter))
      {
         str[i] = '\0';
         if (i + 1 < linelen) lineoff = i + 1;
         else lineoff = -1;
         return str + index;
      }
   }
   lineoff = -1;
   return str + index;
}
