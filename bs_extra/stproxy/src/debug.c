/*
 * Copyright (C) 2007 Adam Hurkala (ahurkala@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "common.h"
#include "debug.h"

void __print(char *file, int line, char *format, ...)
{
   va_list ap;
   static char buff[1024];

   va_start(ap, format);
   vsnprintf(buff, sizeof(buff), format, ap);
   va_end(ap);
   fprintf(stderr, "%s:%d> %s\n", file, line, buff);
}

void __printerror(char *file, int line, char *format, ...)
{
   va_list ap;
   static char buff[1024];

   va_start(ap, format);
   vsnprintf(buff, sizeof(buff), format, ap);
   va_end(ap);
   fprintf(stderr, "%s:%d> %s (errno %d %s)\n", file, line, buff, errno, strerror(errno));
}
