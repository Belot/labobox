#!/bin/sh
#
# Start/stops stproxy
#
#

stproxy_config="/var/stproxy.conf"
stproxy_passwd="/var/stproxy_passwd"

write_config_file()
{
  ret=`/bin/nvram getfile stproxy_config=$stproxy_config`
  [ "$ret" != "Failed" ] && return 0
		  
  echo "BIND_ADDRESS 0.0.0.0" >$stproxy_config
  echo "PROXY_PORT 8080" >>$stproxy_config
  echo "ADMIN_PORT 6767" >>$stproxy_config
  echo "DNS_SERVER 127.0.0.1" >>$stproxy_config
}

case "$1" in
	start)
		echo "Starting stproxy"
		stproxy_start=`/bin/nvram get stproxy_start`
                [ "$stproxy_start" = "Failed" ] && stproxy_start=0
                [ "$stproxy_start" != "1" ] && exit 0
 
                write_config_file

                ret=`/bin/nvram getfile stproxy_passwd=$stproxy_passwd`
                [ "$ret" = "Failed" ] && echo "stproxy 0 \$1\$DE6e3/..\$7a68ski03Hxc/OjFhPEDk. 0.0.0.0" >$stproxy_passwd

                /bin/stproxy -c $stproxy_config -p $stproxy_passwd -u "/bin/nvram setfilecommit stproxy_passwd=/var/stproxy_passwd"
		;;
	stop)
		echo "Stopping stproxy"
		kill -9 `pidof stproxy` >/dev/null 2>/dev/null
		;;
	restart)
		$0 stop
		usleep 400000
		$0 start
		;;
	write_config)
	        write_config_file
	        ;;
	*)
		echo "Usage: /etc/start_scripts/stproxy.sh {start|stop|restart}"
		exit 1
		;;
esac

exit 0
