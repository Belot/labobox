/*
 * Copyright (C) 2008 Michael Finsterbusch (fibu@users.sourceforge.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <crypt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#define PASSWDFILE "/var/passwd"
#define BUFFLEN 128
#define USERLEN 32

void print_help(char* name)
{
 printf("checkpw by Michael Finsterbusch <fibu@users.sourceforge.net>\n\n"
        "usage (1): %s <username> <clear password>\n"
        "   or (2): %s -p <clear password>\n",name,name);
 printf("(1) return: ok   if user and password match with %s\n"
        "            wr   if the user/password pair is wrong\n",PASSWDFILE);
 printf("(2) return the en-crypt-ed password\n");
}

int readline(int fd,char* buffer,int blen)
{ 
  int ret,i;
  char buf[2];

  i=0;
  while((ret=read(fd,buf,1))>0)
  {
    if(buf[0]=='\n' || buf[0]=='\r')
      break;
    buffer[i++]=buf[0];
    if(i+1>=blen)
      break;
  }
  buffer[i]='\0';

  return ret;
}

void getuser(char* buffer,char* user,int ulen,char* pw,int pwlen)
{
  int i,j;

  for(i=0; i<ulen && buffer[i]; i++)
    if(buffer[i]==':') break;
    else user[i]=buffer[i];
  user[i]='\0';

  for(j=0,i++; j<pwlen && buffer[i]; i++,j++)
    if(buffer[i]==':') break;
    else pw[j]=buffer[i];
  pw[j]='\0';
}

int usercmp(char* usr1,char* usr2)
{
  while((*usr1) && (*(usr1++))==(*(usr2++)));

  return (*usr2)==(*usr1);
}


char salt_value(int x)
{
  if(x<=0)
    return '0';
  if(x<10)
    return '0'+x;
  if(x<36)
    return 'a'+x-10;
  if(x<62)
    return 'A'+x-36;
  if(x==62)
    return '.';
  return '/';
}


int main(int argc,char* argv[])
{
  char* user,*pass;
  int fd,ret;
  char buffer[BUFFLEN+1];
  char pwbuffer[BUFFLEN+1];
  char usrbuffer[USERLEN+1];
  char* cryptpw;
  char* key,*hash,salt[3];
  int r;


  if(argc<3){
    print_help(argv[0]);
    return -1;
  }

  if(!strcmp(argv[1],"-p"))
    {
       key=argv[2];
       srand(time(NULL));
       r=1+((int) (1000000000.0*(rand()/(RAND_MAX+1.0))));
       salt[0]=salt_value(((r>>24)^(r>>16))%64);
       salt[1]=salt_value(((r>>8)^r)%64);
       salt[2]='\0';
       hash=crypt(key,salt);
       if(!hash)
       {
         printf("Error: crypt\n");
	 return -6;
       }
       printf("%s",hash);
       return 0;
     }

  user=argv[1];
  pass=argv[2];

  fd=open(PASSWDFILE,O_RDONLY);
  if(fd==-1)
  {
   printf("Error open(%s): %s\n",PASSWDFILE,strerror(errno));
   return -2;
  }

  while(ret=readline(fd,buffer,BUFFLEN))
  {
   if(ret==-1)
   {
    printf("Error read(%s): %s\n",PASSWDFILE,strerror(errno));
    return -2;
   }

   getuser(buffer,usrbuffer,USERLEN,pwbuffer,BUFFLEN);
   if(usercmp(user,usrbuffer))
   {
     cryptpw=crypt(pass,pwbuffer);
     if(usercmp(cryptpw,pwbuffer))
     {
      printf("ok\n");
      return 0;
     }
     printf("wr\n");
     return 1;
   }

  }
  
  printf("wr\n");
  return 1; 
}
