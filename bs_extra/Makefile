#global Makefile for Bit-Switcher added applications
#called from main Makefile and therefore not for running alone
#by Michael Finsterbusch and Patrick Schmidt

export WITH_VOIP

SUBDIRS_BS_EXTRA = wsession \
	thttpd-2.25b \
	stproxy \
	checkpw \
	dropbear-0.50 \
	nvram \
	ledtool \
	etherwake \
	getmultipart \
	dnsmasq-2.35 \
	dnsmasq-2.36 \
	vodsl_monitor \
	tportknockd \
	mtd_erase \
	fwupdate \
	bcm_helper \
	ppp_watch \
	openssl \
	openvpn-2.1_rc13 \
	haprad \
	iperf-2.0.2 \
	reset_button \
	ntpclient-2007 \
	color_theme \
	radvd-1.8.3 \
	ez-ipupdate-3.0.11b7

clean: 
	for dir in $(SUBDIRS_BS_EXTRA); do $(MAKE) -C $$dir clean; done

.PHONY: all stproxy getmultipart webs_extra dropbear nvram ledtool thttpd dnsmasq etherwake start_scripts scripts banner clean subdirs vodsl_monitor tportknockd mtd_erase fwupdate bcm_helper ppp_watch openssl openvpn radius reset_button ntpclient color_theme radvd ez-ipupdate

all: create_dirs apps start_scripts scripts webs_extra nvram thttpd ledtool dnsmasq banner getmultipart vodsl_monitor mtd_erase bcm_helper fwupdate ppp_watch reset_button ntpclient color_theme radvd ez-ipupdate

apps: dropbear tportknockd openssl openvpn radius stproxy etherwake stproxy iperf

create_dirs:
	mkdir -p -m 755 $(INSTALL_DIR)/usr/lib
	mkdir -p -m 755 $(INSTALL_DIR)/usr/bin
	mkdir -p -m 755 $(INSTALL_DIR)/usr/sbin
	mkdir -p -m 755 $(INSTALL_DIR)/bin
	mkdir -p -m 755 $(INSTALL_DIR)/sbin
	mkdir -p -m 755 $(INSTALL_DIR)/etc
	mkdir -p -m 755 $(INSTALL_DIR)/rom
	rm -rf $(INSTALL_DIR)/etc/start_scripts
	rm -rf $(INSTALL_DIR)/lib/modules
	mkdir -p -m 755 $(INSTALL_DIR)/etc/start_scripts
	-cp -f profile $(INSTALL_DIR)/etc
	-cd $(INSTALL_DIR)/etc;ln -s /var/TZ TZ

dropbear:
	$(MAKE) -C dropbear-0.50 PROGRAMS="dropbear dropbearkey dbclient scp" STATIC=0 MULTI=1 SCPPROGRESS=1
	$(MAKE) -C dropbear-0.50 install
	-cd $(INSTALL_DIR)/etc;ln -s /var/dropbear dropbear

ppp_watch:
	$(MAKE) -C ppp_watch all
	$(MAKE) -C ppp_watch install

getmultipart:
	$(MAKE) -C getmultipart all
	$(MAKE) -C getmultipart install

fwupdate:
	$(MAKE) -C fwupdate all
	$(MAKE) -C fwupdate install

bcm_helper:
	$(MAKE) -C bcm_helper all
	$(MAKE) -C bcm_helper install

webs_extra:
	$(MAKE) -C webs_extra install

thttpd:
	$(MAKE) -C thttpd-2.25b all
	$(MAKE) -C thttpd-2.25b install

wsession:
	$(MAKE) -C wsession all
	$(MAKE) -C wsession install

checkpw:
	$(MAKE) -C checkpw all
	$(MAKE) -C checkpw install

nvram:
	$(MAKE) -C nvram all
	$(MAKE) -C nvram install

ledtool:
	$(MAKE) -C ledtool all
	$(MAKE) -C ledtool install

ifeq ($(strip $(BUILD_IPv6)),0)
dnsmasq:
	$(MAKE) -C dnsmasq-2.35 all
	$(MAKE) -C dnsmasq-2.35 install
else
dnsmasq:
	$(MAKE) -C dnsmasq-2.36 all
	$(MAKE) -C dnsmasq-2.36 install
endif

start_scripts:
	$(MAKE) -C start_scripts install

scripts:
	$(MAKE) -C scripts install


ifeq ($(strip $(BUILD_VOIP)),1)
vodsl_monitor:
	$(MAKE) -C vodsl_monitor all
	$(MAKE) -C vodsl_monitor install
else
vodsl_monitor:
endif

ifeq ($(strip $(BUILD_OPENSSL)),1)
openssl:
	$(MAKE) -C openssl all
	-cp openssl/libcrypto.so.0.9.8 $(INSTALL_DIR)/usr/lib
	-cp openssl/libssl.so.0.9.8 $(INSTALL_DIR)/usr/lib
	-cp /opt/toolchains/uclibc-crosstools_gcc-3.4.2_uclibc-20050502/mips-linux-uclibc/lib/libdl-0.9.27.so $(INSTALL_DIR)/usr/lib
	-cd $(INSTALL_DIR)/usr/lib;ln -s libcrypto.so.0.9.8 libcrypto.so;ln -s libssl.so.0.9.8 libssl.so;ln -s libdl-0.9.27.so libdl.so;ln -s libdl-0.9.27.so libdl.so.0
else
openssl:
endif

ifneq ($(strip $(BUILD_OPENVPN)),)
openvpn:
	$(MAKE) -C openvpn-2.1_rc13 all
	-cp openvpn-2.1_rc13/openvpn $(INSTALL_DIR)/usr/bin
	$(STRIP) $(INSTALL_DIR)/usr/bin/openvpn
else
openvpn:
endif


ifneq ($(strip $(BUILD_RADIUS)),)
radius:
	$(MAKE) -C haprad all
	-cp haprad/haprad $(INSTALL_DIR)/usr/bin
	$(STRIP) $(INSTALL_DIR)/usr/bin/haprad
else
radius:
endif

HAVE_SPACE=0
ifeq ($(strip $(PROFILE)),96348GWV_DT)
HAVE_SPACE=$(shell [ $(BUILD_OPENSSL) = "1" -a $(ANNEX_A) = 1 -a $(ANNEX_B) = 1 ] && echo "0" || echo "1")
endif

ifeq ($(HAVE_SPACE),0)
iperf:
stproxy:
tportknockd:
else
iperf:
	$(MAKE) -C iperf-2.0.2 all
	-cp iperf-2.0.2/src/iperf $(INSTALL_DIR)/usr/bin
	$(STRIP) $(INSTALL_DIR)/usr/bin/iperf

stproxy:
	$(MAKE) -C stproxy 
	$(MAKE) -C stproxy install

tportknockd:
	$(MAKE) -C tportknockd all
	$(MAKE) -C tportknockd install
endif

etherwake:
	$(MAKE) -C etherwake all
	$(MAKE) -C etherwake install

mtd_erase:
	$(MAKE) -C mtd_erase
	$(MAKE) -C mtd_erase install

ifeq ($(strip $(PROFILE)),96348GWV_DT)
reset_button:
	$(MAKE) -C reset_button
	$(MAKE) -C reset_button install
else
reset_button:
endif

ntpclient:
	$(MAKE) -C ntpclient-2007 
	$(MAKE) -C ntpclient-2007 install

ifeq ($(strip $(PROFILE)),96348GWV_DT)
color_theme:
	$(MAKE) -C color_theme
	$(MAKE) -C color_theme install
else
color_theme:
endif

ifeq ($(strip $(BUILD_IPv6)),1)
radvd:
	$(MAKE) -C radvd-1.8.3 all
	cp radvd-1.8.3/radvd $(INSTALL_DIR)/sbin/
	$(STRIP) $(INSTALL_DIR)/sbin/radvd
else
radvd:
endif

ez-ipupdate:
	$(MAKE) -C ez-ipupdate-3.0.11b7
	$(MAKE) -C ez-ipupdate-3.0.11b7 install

banner:
	cat banner.0 > banner
	echo "| bitswitcher - Version" `cat version`$(IMG_VER) "- build:" `date  +%d.%m.%Y` "                                                                                   |"|sed -e 's/\(.\{78\}\).*/\1|/'>> banner
	echo  '+-----------------------------------------------------------------------------+' >> banner
	cp banner $(INSTALL_DIR)/etc/issue.net
	echo `cat version`$(IMG_VER) > $(INSTALL_DIR)/etc/version
