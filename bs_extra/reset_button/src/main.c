/*
 * Copyright (C) 2008 Michael Finsterbusch (fibu@users.sourceforge.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>

#include <board_api.h>


#define WAIT_POLL 400000
#define WAIT_PUSH 800000

extern int boardIoctl(int board_ioctl, BOARD_IOCTL_ACTION action, char *string, int strLen, int offset, char *buf);

char script_name[100];

void print_help(char* name)
{
  printf("reset_button\n"
	"by Patrick Schmidt <coolman1982@users.sourceforge.net>\n\n"
	"usage: %s [-h] -s\n"
	"options:\n"
	"  -h                      this help screen\n"
	"  -s <script>             script gets <hold_time> as parameter\n"
	"",
	name);
}

void call_event_script(int count)
{
  char buf[2][100];
  pid_t pid;
  pid=fork();
  if(pid<0)
  {
    printf("fork() can not call event script\n");
    return;
  }

  if(pid>0) // parent
    return;

  // child
  snprintf(buf[0],100,"%s",script_name);
  snprintf(buf[1],100,"%d",count);
  execl(script_name,buf[0],buf[1],NULL);
#ifdef DEBUG
  printf("execl() %s\n",strerror(errno));
#endif
}

int main(int argc,char* argv[])
{
  char** argvh=argv;
  int state, wait;
  unsigned int count;
  char opt,noopt=1;
  
  while((opt=getopt(argc,argvh,"hs:"))!=EOF)
  {
    switch(opt)
    {
      case 's':
        noopt=0;
        strncpy(script_name,optarg,100);
        break;
      case 'h':
      default:
        goto out;
        break;
    }
  }
  
  if(noopt)
    goto out;
  
  count=0;
  wait=WAIT_POLL;
  while(1)
  {
    usleep(wait);
    state=sysGetResetStatus();
#ifdef DEBUG
    printf("state=%d,count=%d\n",state,count);
#endif
    if(state!=2)
    {
      system("ledtool 1 3");
      wait=WAIT_PUSH;
      count++;
    }
    else if(state==2 && count!=0)
    {
      call_event_script(count);
      wait=WAIT_POLL;
      count=0;
    }
  }
  return 0;
  
out:
    print_help(argv[0]);
    return 0;
}



