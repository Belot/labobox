#!/bin/sh

count="$1"


[ "$count" = "" ] && exit 0

if [ "$count" -ge "3" ] && [ "$count" -lt "10" ]; then
  ledtool 1 4
  sleep 4
  nvram reboot
elif [ "$count" -ge "10" ] && [ "$count" -lt "15" ]; then
  ledtool 0 0
  ledtool 1 4
  sleep 4
  nvram reset
  nvram commit
  nvram reboot
#new since 0.3.8
elif [ "$count" -ge "15" ] && [ "$count" -lt "20" ]; then
  ledtool 1 4
  sleep 4
  nvram set mini_fo_clear=1
  nvram commit
  nvram reboot
elif [ "$count" -ge "20" ] && [ "$count" -lt "25" ]; then
  ledtool 1 4
  sleep 4
  nvram set jffs_clear=1
  nvram commit
  nvram reboot
elif [ "$count" -ge "25" ] && [ "$count" -lt "30" ]; then
  ledtool 1 4
  sleep 4
  nvram set mini_fo=ram
  nvram commit
  nvram reboot
elif [ "$count" -ge "30" ] && [ "$count" -lt "35" ]; then
  ledtool 1 4
  sleep 4
  nvram set mini_fo=jffs
  nvram commit
  nvram reboot
elif [ "$count" -ge "35" ]; then
  ledtool 1 4
  sleep 4
  nvram set mini_fo=off
  nvram commit
  nvram reboot
fi
