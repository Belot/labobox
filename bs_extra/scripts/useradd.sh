#!/bin/sh
#script adds new users to NVRAM
#by Simon

if [ "$#" != "6" ]; then
	echo "Usage: $0 name password group complete-name homedir shell"
	exit
fi

let userid=-1
let maxuser=-1
let groupid=-1
let maxgroup=-1

PASSWD="/var/passwd"                                                                          
GROUP="/var/group"                                                                            
nvram getfile etc_passwd=$PASSWD >/dev/null 
nvram getfile etc_group=$GROUP >/dev/null

err=0
error=""

name=`echo "$1" | tr [:upper:] [:lower:]`
pass=$2
group=`echo "$3" | tr [:upper:] [:lower:]`
vname=$4
home=$5
shell=$6

if [ "$name" != "`echo $name | tr -d -- ':\n'`" ]; then
	err=1
	error="$error\nname may not contain ':' or newlines"
fi

if [ "$group" != "`echo $group | tr -d -- ':\n'`" ]; then
        err=1                                          
	error="$error\ngroup may not contain ':' or newlines"
fi

if [ "$vname" != "`echo $vname | tr -d -- ':\n'`" ]; then
	err=1
	error="$error\ncomplete-name may not contain ':' or newlines"
fi

if [ "$home" != "`echo $home | tr -d -- ':\n'`" ]; then           
        err=1                                                      
        error="$error\nhome directory may not contain ':' or newlines"
fi                                                                        
                    
if [ "$shell" != "`echo $shell | tr -d -- ':\n'`" ]; then           
        err=1                                                      
	error="$error\nshell may not contain ':' or newlines"
fi                                                                        
                    

oifs="$IFS"
IFS=":"
while read xname xpw xid xgid xvname xroot xshell
do
	if [ "$name" = "$xname" ]; then
		err=1
		error="$error\n$name already exists"
	fi
	
	if [ "$maxuser" -lt "$xid" ]; then
		let maxuser=$xid
	fi
done < $PASSWD 

while read gname gg gid grest
do
	if [ "$maxgroup" -lt "$gid" ]; then
		let maxgroup=$gid
	fi	
	
	if [ "$group" = "$gname" ]; then
		let groupid=$gid
	fi		
done < $GROUP 


newgroup=0

let userid=$maxuser+1
if [ "$groupid" = "-1" ]; then
	let groupid=$maxgroup+1
	newgroup=1
fi

if [ "$err" = "0" ]; then
	echo "$name":"!":"$userid":"$groupid":"$vname":"$home":"$shell" >> $PASSWD
	echo "${name}:${pass}"|chpasswd -m >/dev/null     
	nvram setfile etc_passwd=$PASSWD >/dev/null

	if [ "$newgroup" = "1" ]; then
		echo "$group::$groupid:" >> $GROUP
		nvram setfile etc_group=$GROUP >/dev/null
	fi
	echo "Saving..."
	nvram commit >/dev/null && echo "Ok" || echo "Failed"
fi                     

echo -e $error

exit 0