#!/bin/sh
# ROOT overlay for BitSwticher
# main functionality taken from "cfg.common" version 2.354
# hosted on http://spblinux.de/fbox.new/

##############################################################################
# Copyright (C) cfg.common,  C. Ostheimer and <dynamic>, licensed under
# GNU General Public License version 2 (GPL v2.0)
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details; spblinux.de/fbox.new/gpl-2.0.txt
#
# by dynamic and spblinux, 2008-05
# cfg.common
# - helper functions to control addons
# - to be sourced by cfg_xxxx installation scripts
# - all variables used(*) by this script have a trailing underline, e.g. i_
#   *) they may get changed, overwritten, deleted
####################################################################

errorexit()
{
 echo -e "$@"
 exit 1
}


########################################################################
# internal function: mkdir_rcrsv_ [ <base_dir>] <dir>
# - create all levels of required directory structure provided in <dir>
# - consider <dir> to be relative to <base-dir> if <base-dir> parm is set
# - if <base-dir> is omitted, <dir> is assumed to be an absolute directory path
########################################################################
mkdir_rcrsv_()
{
 if [ $# -gt 1 ];then
  create_dir_=$1
  parms_=$2
  [ -d $create_dir_ ] || errorexit "$funct_: Base directory $create_dir_ does not exist."
 else
  create_dir_=""
  parms_=$1
 fi 
 for d_ in `echo $parms_ | sed "s/\// /g"`;do
  create_dir_=$create_dir_/$d_
  if ! [ -d $create_dir_ ];then
   mkdir $create_dir_ || errorexit "$funct_: No RW File System ($create_dir_)"
  fi
 done        
}
########################################################################
# internal function: symlink_dir_contents_ <target_path> <source_path> <RW|RO> [RCRSV|NORCRSV]
# <target_path> : target path to create symlinks in
# <source_path> : source path with files & dirs to be symlinked to 
# <RW|RO>       : Indicates whether existing files in target_path should be overwritten by source_path contents
# RCRSV          : If RCRSV is sent subdirectory contents are recursively symlinked as well
#                 otherwise only contents of the first direcory level are symlinked
# Used to symlink files from source directories to RW root paths
# - RCRSVs through $source_path_ and creates directory structure in RW $target_path
#   at the same time symlinking files from source directories into created RW target directories
# - for source directories with ".default" extension the extension is omitted in target directory
# Dependencies:
#  $root_bind_ is epxetcted to point to mounted RW-Root path
#  $bind_on_   is epxetcted to point to RW-Root moint point
########################################################################
symlink_dir_contents_()
{
 [ -d $1 ] || mkdir_rcrsv_ $1
 local loop_=${4-"NORCRSV"}
 local buffer_=$new_sftlnks_
 local src_name_=""
 new_sftlnks_=0
 for src_name_ in `ls -1A $2`;do
  ### Determine src name wihtout possible ".default" extension - required for historical compatibility reasons
  local src_name_no_def_=${src_name_%*.default}
  ### If $2 is a file then ls -1A will already return absolute path inkl. filename instead just filename
  if [ -f $2 ];then
   local abs_src_name_=$2 
   src_name_no_def_=${src_name_no_def_##*/}
  else 
   local abs_src_name_=$2/$src_name_
  fi
  
  ### L: Source File is Softlink ####
  ### D: Source is Directory ####
  ### F: Source is regular file or other device file ####
  if [ -L $abs_src_name_ ];then
   [ -e $abs_src_name_.default ] && continue 
   stat_="L"
   soft_linked_to_=`ls -l $abs_src_name_`
   soft_linked_to_=${soft_linked_to_#*-> }
  else
   stat_="" 
  fi 
  [ -d $abs_src_name_ ] && stat_=$stat_"D:" || stat_=$stat_"F:"  
  
  [ -L $1/$src_name_no_def_ ] && stat_=$stat_"L" 
  if [ -d $1/$src_name_no_def_ ];then
   stat_=$stat_"D" 
  else  
   [ -e $1/$src_name_no_def_ ] && stat_=$stat_"F"  
  fi 
  case $stat_ in
  
  F:   ) ln -s $abs_src_name_ $1/$src_name_no_def_ 
         let new_sftlnks_+=1 
		 ;;
  D:   ) if [ "$loop_" = "RCRSV" ];then
          symlink_dir_contents_ $1/$src_name_no_def_ $abs_src_name_ $3 RCRSV
         else
          ln -s $abs_src_name_ $1/$src_name_no_def_
          let new_sftlnks_+=1
         fi
	 ;;
	 
  LD:  ) if [ "$loop_" = "RCRSV" ];then
          soft_linked_to_=`ls -l $abs_src_name_`
          soft_linked_to_=${soft_linked_to_#*-> }
          symlink_dir_contents_ $1/$src_name_no_def_ $soft_linked_to_ $3 RCRSV
         else
          cp -P $abs_src_name_ $1
         fi
	 ;;
	 
  LF: )  cp -P $abs_src_name_ $1
         # ln -s $soft_linked_to_ $1/$src_name_no_def_
         ;;
  
  D:D )  ### Source: Directory, Target: Existing RW Directory from previous installs ###
         ### Combine contents of existing and new directory in RW target ###
         symlink_dir_contents_ $1/$src_name_no_def_ $abs_src_name_ $3 $loop_
	 ;;
	 
  LD:D ) soft_linked_to_=`ls -l $abs_src_name_`
         soft_linked_to_=${soft_linked_to_#*-> }
         symlink_dir_contents_ $1/$src_name_no_def_ $soft_linked_to_ $3 $loop_
         ;;
         
  D:LD)  ### Source: Directory, Target: Symlink to a directory from previous installs ###
         ### Convert symlink in Target to true Directory and combine contents of both in newly created directory ###
         ### Skip if source and target are pointing to the same directory
         if [ "$soft_linked_to_" != "$abs_src_name_" ];then
          soft_linked_to_=`ls -l $1/$src_name_no_def_ `
          soft_linked_to_=${soft_linked_to_#*-> }
          rm $1/$src_name_no_def_
          ### Symlink only first level - no RCRSV required, as existing dir appears to be on RW device already
          symlink_dir_contents_ $1/$src_name_no_def_ $soft_linked_to_ $3
          symlink_dir_contents_ $1/$src_name_no_def_ $abs_src_name_ $3 $loop_
         fi
         ;;
         
  LD:F|LD:LF|D:F|D:LF ) 
         errorexit "$funct_: Source dir $src_name_def_ is conflicting with existing file $bind_on_${1#$root_bind_*}/${src_name_no_def_}. Aborting installation!"
         ;;

  LF:D|LF:LD|F:D|F:LD ) 
         errorexit "$funct_: Source file $src_name_def_ is conflicting with existing dir $bind_on_${1#$root_bind_*}/${src_name_no_def_}. Aborting installation!"
	 ;;
  
		 
  F:F|F:LF|LF:F|LF:LF ) 
  	 ### Source: File, Target: App File or symlink to App file ###
         ### Overwrite with symlink to new file, if RW flag was set ###
         if [ "$3" = "RW" ];then
          [ $verbose ] && echo " -> File $bind_on_${1#$root_bind_*}/$src_name_no_def_ replaced with SYMLINK to file $abs_src_name_"
          rm $1/$src_name_no_def_
          ln -s $abs_src_name_ $1/$src_name_no_def_
          let new_sftlnks_+=1
         else
          [ $verbose ] && echo " -> $bind_on_${1#$root_bind_*}/$src_name_no_def_ NOT REPLACED with SYMLINK to file $abs_src_name_, as RO parm was set."
         fi
	 ;;
  esac
 done
 [ $verbose ] && echo " -> $bind_on_${1#$root_bind_*} -  ADDED SOFTLINKS: $new_sftlnks_" || echo -n "."
 let tot_sftlnks_+=$new_sftlnks_
 let new_sftlnks_=$buffer_
}

#########################################################
# usage: _fct funct_name@parm1[:parm2][:parm3]...[:parm{n}]
#########################################################
_fct() 
{
# Determine function 
funct_=${1%%@*}
case $funct_ in
 #non verbose functions (which return their value by stdout) must be listed here
 modext | svrsub | path_abs)
 ;;
 *)
 [ $verbose ] && echo "EXECUTING:   _fct $1"
esac
i_=0
# Determine parameters for function and store in variables parm1_, parm2_, parm3_, ... parm{n}_
args_=`echo \${1#*@} | sed "s/:/ /g"`
for parm_ in $args_;do
 let i_+=1
 eval parm${i_}_="$parm_"
done 

case $funct_ in
 #########################################################
 ovrly) 
 # ovrly@[RO|RW|NL[,USB|SQF]:<ovr_dir>[:<abs_ref1>][:<abs_ref2>][:<abs_ref3]...[:<abs_ref{n}]
 # Binds RO-Directory <ovr_dir> into /var/_RW_/_<ovr_dir> and softlinks files and directories from <abs_dir{n}>
 # PARMS:
 # <RO|RW|NL>:If set to "RO" existing files in <ro_dir> will be kept and not replaced by another softlink
 #            If set to "RW" existing files in <ro_dir> will be replaced by subsequent files and directories
 #            If set to "NL" files and directores in ovr_dir will not by symlinked into overlayed structure
 # <USB|SQF> :If set to SQF items from abs_ref are recursively symlinked from RO source to RW root paths
 # <ovr_dir>: Read Only dir that will be overlayed and look as being Read Write
 # <abs_ref>: Absolute reference to file or directory which will be overlayed in / onto <ovr_dir>
 # Important Variables :
 # ro_source_ : variable pointing to path where "/" has been mounted to ( via mount -o bind )
 # rw_target_ : variable pointing to path where RW strcuture will be recreated
 #########################################################
 # Attention: 
 # Even though it is possible to change files in /lib /bin and /sbin etc. it is recommended not to do so,
 # unless you really know what you do and why you do it!
 # Be aware that <root_mnt> directory cannot be unmounted while other processes are accessing files on it.
 # A full uninstall will therefore only be possible after cfg_xxx applications have been put to "disable" mode
 # ( maintenance mode ) and the system has been rebooted. 
 #########################################################
  [ $i_ -ge 2 ] || errorexit "$funct_: Insufficient number of parameters"
 #set -x
  ro_source_=/var/_RO_
  rw_target_=/var/_RW_
  if ! mount | grep "on $ro_source_ " | sed "s/^\([^ ]*\) .*/\1/" | grep -q /dev/root;then
   [ -d $ro_source_ ] || mkdir_rcrsv_ $ro_source_
   mount -o bind / $ro_source_ || errorexit "$funct_: Failed to mount / to $ro_source_"
  fi
  let x_=3
  new_sftlnks_=0
  tot_sftlnks_=0
  mounted_=false
  case $parm2_ in
   /dev/pts* ) bind_on_="/dev/pts";;
   /etc* | /usr* | /bin* | /lib* | /sbin* | /webs/*) bind_on_=${parm2_%/${parm2_#?*/}};;
   * ) errorexit "$funct_: Overlaying not supported for directory $parm2_!";;
  esac
  root_bind_=$rw_target_/`echo $bind_on_ | sed "s/\//_/g"`
  ovr_dir_=$root_bind_${parm2_#$bind_on_*}
  [ -d $ovr_dir_ ] || mkdir_rcrsv_ $ovr_dir_
  
  loop_="NORCRSV";access_="RO";no_lnkng_=false
  for sub_parms_ in `echo $parm1_ | sed "s/,/ /g"`;do
   case $sub_parms_ in
    SQF ) loop_="RCRSV";;
    USB ) loop_="NORCRSV";;
    RW ) access_="RW";;
    RO ) access_="RO";;
    NL ) no_lnkng_=true;;
    *) errorexit "$funct_: Unknown parameter $sub_parms_ in function!";;
   esac 
  done

  #### Skip symlinking root dir if already mounted by another cfg_xxx application ###
  if ! mount | grep "on $bind_on_ " | sed "s/^\([^ ]*\) .*/\1/" | grep -q ramfs;then
   #### Skip symlinking root dir if NO LINKING ("NL") parm set ###
   $no_lnkng_ || symlink_dir_contents_ $root_bind_ $ro_source_$bind_on_ $access_
   mount --bind $root_bind_ $bind_on_ || errorexit "$funct_: Failed to mount --bind $root_bind_ to $bind_on_"
  fi
  while [ $x_ -le $i_ ]
  do
   ref_item_=`eval echo \\$parm${x_}_`
   symlink_dir_contents_ $ovr_dir_ $ref_item_ $access_ $loop_
   let x_+=1
  done
  [ $verbose ] && echo "INFORMATION: _fct $1: Total $tot_sftlnks_ softlinks added to $bind_on_/..."
 ;;
 esac
}



case "$1" in
	etc | usr | bin | lib | sbin )
		SRC=/$1
		;;
	/etc | /usr | /bin | /lib | /sbin )                                          
                SRC=$1                                                         
                ;;
        /dev/pts* ) 
       		SRC="/dev/pts"
       		;;
       	X)
       		if [ $# -ge 2 ]; then
	       		SRC=$2
	       	else
	       		echo "Zu wenig Parameter!"
	       		exit
	       	fi
	       	;;
	*)
		echo "Overlay the ro root file system"
		echo "Usage: $0 {etc|usr|bin|lib|sbin}"
		echo "       $0 {/etc|/usr|/bin|/lib|/sbin}"
		echo "       $0 {X <dir>}"
	        exit 1 		
		;;
esac 


DUMMY=/var/dhgsdfghadfadhzadgh
mkdir $DUMMY >/dev/null 2>/dev/null
_fct ovrly@RO:$SRC:$DUMMY
rm -rf $DUMMY >/dev/null 2>/dev/null


