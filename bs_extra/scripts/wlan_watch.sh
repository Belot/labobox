#!/bin/sh

usage="usage: $0 <join_cmd> <watch_intervall>"

[ "$1" = "" ] && echo $usage && exit 1
[ "$2" = "" ] && echo $usage && exit 2

while [ 1 -ne 2 ]; do
  bssid=`wlctl status 2>/dev/null|grep BSSID|awk {'print $2;'}`
  [ "$bssid" = "" -o "$bssid" = "00:00:00:00:00:00" ] && /bin/sh -c "$1"
  sleep $2
done

