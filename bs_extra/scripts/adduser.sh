#!/bin/sh
#script reads data to add new user from stdin

echo "name:"
read name
[ "$name" = "" ] && echo "no name specified...aborting" && exit 1
echo "password:"
read pass
echo "group [$name]:"
[ "$group" = "" ] && group="$name"
read group
echo "complete name:"
read vname
echo "home directory: [/opt]"
read home
[ "$home" = "" ] && home="/opt"
echo "login shell: [/bin/sh]"
read shell
[ "$shell" = "" ] && shell="/bin/sh"

useradd.sh "$name" "$pass" "$group" "$vname" "$home" "$shell"