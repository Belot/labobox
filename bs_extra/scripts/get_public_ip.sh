#!/bin/sh

ip=`wget -qO - http://monip.org|grep -o -e '[0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]\+' 2>/dev/null`
if [ "$ip" != "" ]; then
  echo -n "$ip"
  exit 0
fi

exit 1
