#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <time.h>

#define CREATE_SYMLINK   0x200
struct symlink {
        char src[32];
        char dst[32];
       };
int ppp_symlink(char* iface, int ppp_num);


#define CALL_LOG_FILE_IN0 "/var/tcm_incomingep0.log"
#define CALL_LOG_FILE_IN1 "/var/tcm_incomingep1.log"
#define CALL_LOG_FILE_OUT0 "/var/tcm_outgoingep0.log"
#define CALL_LOG_FILE_OUT1 "/var/tcm_outgoingep1.log"
int print_call_log(int inout);

void print_help(char* name)
{
  printf("bcm_helper by Michael Finsterbusch <fibu@users.sourceforge.net>\n"
	"some glue for router configuration\n\n"
	"usage: %s [-h] [-s <ppp-name>]\n"
	"options:\n"
	"  -h              this help screen\n"
	"  -s <ppp-name>   create the symlink <ppp-name> which point to\n"
        "                  /proc/var/fyi/wan/.ppp0\n",
        "  -t <ppp-name>   create the symlink <ppp-name> which point to\n"
        "                  /proc/var/fyi/wan/.ppp1\n",

	name);
}

int main(int argc,char* argv[])
{
  char** argvh=argv;
  int opt,noopt=1;
  int ret=0;
  int h=1;

  while((opt=getopt(argc,argvh,"hs:c:t:"))!=EOF)
  {
   noopt=0;
   h++;
   switch(opt)
   {
     case 's':
	return ppp_symlink(optarg,0);
	break;
     case 't':
        return ppp_symlink(optarg,1);
        break;
     case 'c':
	return print_call_log(atoi(optarg));
	break;
     case 'h':
     default:
	 print_help(argv[0]);
	 return 0;
	 break;
   }
  }
  if(noopt)
  {
    print_help(argv[0]);
    return 0;
  }

  return ret;
}


int ppp_symlink(char* iface,int ppp_num)
{
  char file[128];
  struct symlink sl;
  int fd,ret=0;

  snprintf(file, 128, "/proc/var/fyi/wan/%s", iface);
  if(!access(file, F_OK))
    return 0;

  if((fd = open("/dev/bcm",O_RDWR))<0)
  {
    printf("Error: can not open /dev/bcm - %s\n",strerror(errno));
    ret=1;
    goto ppp_sym_exit;
  }
  sprintf(sl.src,".ppp%d",ppp_num);
  snprintf(sl.dst,32,iface);
  if(ioctl(fd,CREATE_SYMLINK,(long)&sl)<0)
  {
    printf("Error: ioctl - %s\n",strerror(errno));
    ret=1;
  }
ppp_sym_exit:
  if(fd>0)
    close(fd);
  return ret;
}

char* print_time(time_t time)
{
 struct tm date;
 char* string;

 if(!localtime_r(&time,&date))
  return NULL;

 // "20.10.07 11:47:36"
 string=(char*)malloc(18);
 if(!string)
  return NULL;

 snprintf(string,18,"%02d.%02d.%02d %02d:%02d:%02d",
 	  date.tm_mday,date.tm_mon+1,date.tm_year-100,
	  date.tm_hour,date.tm_min,date.tm_sec);
 
 return string;
}

char* print_duration(time_t dur)
{
  char* string;
  int h,min,sec;

  string=(char*)malloc(20);
  memset(string,0,20);
  h=min=sec=0;
  if(dur>3600)
  {
    h=dur/3600;
    dur-=h*3600;
  }
  if(dur>60)
  {
    min=dur/60;
    dur-=min*60;
  }
  sec=dur;

  if(h)
    snprintf(string,20,"%dh ",h);
  if(min || h)
    snprintf(string,20,"%s %d min",string,min);
  snprintf(string,20,"%s %d sec",string,sec);

  return string;
}


int print_call_log(int inout)
{
  struct call_log_record{
	char number[64];
	time_t time_begin;    // starting time of call
	time_t time_end;      // end of call
	time_t call_duration; // duration of call
	int answered;              // call comes via SIP, ISDN, etc
	int line;             // line port on router (1/2)
	int via;
  } call_log[40],hl;
  char* files[2];
  int nodata=1;
  FILE* fd;
  int i,j,max,maxindex;

  // read call log files
  memset(call_log,0,sizeof(struct call_log_record)*40);
  if(inout) // incomming
  {
    files[0]=CALL_LOG_FILE_IN0;
    files[1]=CALL_LOG_FILE_IN1;
  }
  else
  {
    files[0]=CALL_LOG_FILE_OUT0;
    files[1]=CALL_LOG_FILE_OUT1;
  }

  fd=fopen(files[0],"r");
  if(fd)
  {
    fread(&call_log[0],1,sizeof(struct call_log_record)*20,fd);
    fclose(fd);
    fd=NULL;
    nodata=0;
  }
  fd=fopen(files[1],"r");
  if(fd)
  {
    fread(&call_log[20],1,sizeof(struct call_log_record)*20,fd);
    fclose(fd);
    nodata=0;
  }

  if(nodata)
  {
    printf("<tr><td id=\"nodata\">No data available</td></tr>\n");
    return 0;
  }

  // sort call log
  for(i=0; i<40; i++)
  { 
    max=0;
    maxindex=i;
    for(j=i; j<40; j++)
    {
      if(call_log[j].time_begin>max)
	{
	  max=call_log[j].time_begin;
	  maxindex=j;
	}
    }
    hl=call_log[maxindex];
    call_log[maxindex]=call_log[i];
    call_log[i]=hl;
  }

  for(i=0; i<40; i++)
  {
    if(call_log[i].number[0]=='\0')
      continue;
    if(inout)
    {
      printf("<tr><td id=\"number\">%s</td><td id=\"time_begin\">%s</td>"
	     "<td id=\"duration\">%s</td><td id=\"line\">%d</td>"
	     "<td id=\"via\">%s</td><td id=\"answered\">%s</td></tr>\n",
             call_log[i].number,
	     print_time(call_log[i].time_begin),
	     print_duration(call_log[i].call_duration),
	     call_log[i].line,
	     call_log[i].via==0?"PSTN":"SIP",
	     call_log[i].answered==0?"no":"yes");
    }
    else
    {
      printf("<tr><td id=\"number\">%s</td><td id=\"time_begin\">%s</td>"
	     "<td id=\"duration\">%s</td><td id=\"line\">%d</td>"
	     "<td id=\"via\">%s</td></tr>\n",
             call_log[i].number,
	     print_time(call_log[i].time_begin),
	     print_duration(call_log[i].call_duration),
	     call_log[i].line,
	     call_log[i].via==0?"PSTN":"SIP");
    }
  }
  return 0;
}


