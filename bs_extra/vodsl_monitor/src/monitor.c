#define _XOPEN_SOURCE 
#define _XOPEN_SOURCE_EXTENDED
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/klog.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <syslog.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/select.h>
#include <ctype.h>

#include "vodsl_monitor.h"
#include "monitor.h"
#include "sock_list.h"

// timediff between event and 'now' in seconds
#define TIMEDIFF 2
// maximum Delay between 2 events in usec
#define EVENT_DELAY 1500000
// mark in log-file to prevent calling an event many times (KLOG_POLL)
#define KLOG_MARK "VODSL_MONITOR_MARK"

// delay between two Ring-signals - after the fist Ring 
// if available the CLIP-signal is send
//#define CLIP_DELAY 800000

// Space for log file data
// one record is 88 byte large
// 20 records per file
// space for 4 files
#define FILES 4
#define RECORD_SIZE 88
#define RECORDS 20

#define FILE_BUF_LEN (RECORD_SIZE*RECORDS)
// 4 kByte KLOG_Buffer
#define KLOG_BUF_LEN 1<<12

// allocate Buffers on Heap
char FileBuffer[FILE_BUF_LEN];
char Klog_Buffer[KLOG_BUF_LEN];
volatile int global_time_fmt=TIME_UTC;

#define MONITOR_OFFSET 64
struct part_monitor{
	time_t time_begin;    // starting time of call
        time_t time_end;      // end of call
	time_t call_duration; // duration of call
	int answered;         // answered
	int line;             // line port on router (1/2)
	int via;              // call comes via SIP, ISDN, etc
};

#define _IN_  		0
#define _OUT_ 		1
#define _DISCONNECT_ 	2

volatile int global_shutdown=0;
#define THREAD_EXIT if(global_shutdown) pthread_exit(NULL);
pthread_t klog_th,file_th,server_th;

void shutdown_sighandler(int sig)
{
  signal(sig,SIG_IGN);
  DEBUG("received signal %d - shutting down...\n",sig);

  global_shutdown=1;
  pthread_cancel(server_th);
  pthread_cancel(file_th);
  pthread_cancel(klog_th);
}


char* get_date(time_t time)
{
 struct tm date;
 struct tm *pdate;
 char* string;
 
 // "20.10.07 11:47:36"
 string=(char*)malloc(18);
 if(!string)
   return NULL;
 
 if( global_time_fmt==TIME_UTC || global_time_fmt==TIME_CET )
 {

 if( global_time_fmt==TIME_CET )
   time+=3600;
 
  if(!gmtime_r(&time,&date))
    return NULL;
  
  snprintf(string,18,"%02d.%02d.%02d %02d:%02d:%02d",
           date.tm_mday,date.tm_mon+1,date.tm_year-100,
           date.tm_hour,date.tm_min,date.tm_sec);
 }
 else if( global_time_fmt==TIME_LOCAL )
 {
   pdate=localtime(&time);
   if(!pdate)
     return NULL;
   
   snprintf(string,18,"%02d.%02d.%02d %02d:%02d:%02d",
            pdate->tm_mday,pdate->tm_mon+1,pdate->tm_year-100,
            pdate->tm_hour,pdate->tm_min,pdate->tm_sec);
 }


 
 return string;
}

void send_event(char* message,struct monitor_conf conf)
{
 struct sock_list* list,*iter=NULL,*client;
 int ret,msg_len,flags;
 const int buf_len=10;
 char buf[buf_len];

 if(!message)
  return;
 
 if(!conf.private)
  return;

 DEBUG("send %s",message);

 list=(struct sock_list*)conf.private;
 msg_len=strlen(message);
 flags=MSG_DONTWAIT; 

 while((client=iter,iter=sock_list_iter(list,iter))!=NULL)
 {
  // check if connection is still alive
  ret=recv(iter->sock,buf,buf_len,flags);
  if(ret==0)
   {
      close(iter->sock);
      sock_list_del(list,iter->sock);
      iter=client;
      continue;
   }
  
  ret=send(iter->sock,message,msg_len,flags);
  if(ret!=-1)
   continue;
  
  ret=errno;
  switch(ret)
  {
	case EBADF:        // invalid descriptor
	case ECONNRESET:   // connection reset by peer
	case EINVAL:       // Invalid argument passed
	case ENOTCONN:     // socket is not connected, and no target has been given
	case ENOTSOCK:     // argument is not a socket
	case EPIPE:        // local end has been shut down
 	        close(iter->sock);
		sock_list_del(list,iter->sock);
		iter=client;
		continue;
		break;

	case EINTR:        // A signal occurred before any data was transmitted
		iter=client; //try again
		continue;
		break;
	default:
		ERROR(" sending message: %s",strerror(ret));
		break;
  }
 }
}

void call_event_script(char** param,char** envp,struct monitor_conf conf)
{
 pid_t pid;
 int i;
 if(!conf.program)
   return;

 if(global_debug)
 {
  printf("DEBUG: %s(%d): call %s ",__FILE__,__LINE__,conf.program);
  if(param)
    for(i=0; param[i]; i++)
      printf("%s ",param[i]);
   printf("\n");
 }

 pid=fork();
 if(pid<0)
 {
  ERROR("fork() can not call event script\n");
  return;
 }

 if(pid>0) // parent
   return;

 // child
 execve(conf.program,param,envp);
 ERROR("execve() %s\n",strerror(errno));
}

char* get_via(int via)
{
 switch(via)
 {
  case 0: return VIA_PSTN; break;
  case 1: return VIA_SIP; break;
 }

 return VIA_DEFAULT;
}


void handle_event(int inout,struct monitor record,struct monitor_conf conf)
{
 int duration;
 const int sendbuflen=256;
 char sendbuffer[sendbuflen];
 char event[3][15]={"RING","CALL","DISCONNECT"};
 char caller[33];
 char called[33];
 const int param_len=15;
 const int envp_len=30;
 char param[6][param_len];
 char envp[6][envp_len];
 char* paramh[7],*envph[6];

 if(inout<0||inout>2)
  return;

 duration=record.time_end-record.time_begin;
 if(duration<0)
   duration=0;

 memset(caller,0,sizeof(caller));
 memset(called,0,sizeof(called));
 memcpy(caller,record.caller,sizeof(caller));
 memcpy(called,record.called,sizeof(called));

 if(inout==_IN_)
 {
   // 20.10.07 11:47:37;RING;2;06158879136;916142;ISDN;
   snprintf(sendbuffer,sendbuflen,"%s;RING;%d;%s;%s;%s;\n",get_date(record.time_begin),
   	    record.line,caller,called,get_via(record.via));
   send_event(sendbuffer,conf);
 }

 if(inout==_OUT_)
 {
  // don't send an event to network
 }

 if(inout==_DISCONNECT_)
 {
  // 20.10.07 14:13:16;DISCONNECT;0;19;
  snprintf(sendbuffer,sendbuflen,"%s;DISCONNECT;%d;%d;\n",get_date(record.time_begin),
           record.line,duration);
  send_event(sendbuffer,conf);
 }

 // call script with parameters: "EVENT caller-number line date time duration"
 //             EVENT: RING, CALL, DISCONNECT

#define SET_PARAMETER(index,format,value,name) \
	snprintf(param[index],param_len,format,value); \
	snprintf(envp[index],envp_len,name"="format,value);

 SET_PARAMETER(0,"%s",event[inout],SCRIPT_ENV_EVENT);
 SET_PARAMETER(1,"%s",caller,SCRIPT_ENV_CALLER);
 SET_PARAMETER(2,"%d",record.line,SCRIPT_ENV_LINE);
 SET_PARAMETER(3,"%s",get_date(record.time_begin),SCRIPT_ENV_DATE);
 SET_PARAMETER(4,"%d",duration,SCRIPT_ENV_DURATION);

#undef SET_PARAMETER

#define SET_PARAMETER(index) \
	paramh[index+1]=param[index]; \
	envph[index]=envp[index];

 paramh[0]=conf.program;
 SET_PARAMETER(0);
 SET_PARAMETER(1);
 SET_PARAMETER(2);
 SET_PARAMETER(3);
 SET_PARAMETER(4);
 paramh[6]=NULL;
 envph[5]=NULL;

#undef SET_PARAMETER

 call_event_script(paramh,envph,conf);
}

void files_thread_cleanup(void* arg)
{
 FILE** fd=(FILE**)arg;
 int fcount;
 DEBUG("files thread shutting down...\n");
 for(fcount=0; fcount<FILES; fcount++)
   if(fd[fcount])
        fclose(fd[fcount]);
 DEBUG("files thread shutting down - done\n");
}
   
void* poll_files(void* config)
{
 struct monitor_conf* conf=(struct monitor_conf*)config;
 struct monitor record;
 struct part_monitor* rec;
 FILE *fd[FILES];     
 char files[FILES][strlen(MONITOR_FILE_OUT1)+10]={MONITOR_FILE_IN0,
					          MONITOR_FILE_IN1,
						  MONITOR_FILE_OUT0,
						  MONITOR_FILE_OUT1};
 int filetype[]={_DISCONNECT_,_DISCONNECT_,_OUT_,_OUT_}; 
 int fcount;
 FILE* f;
 int ret,h,i;
 time_t now;
 void* start;
 struct stat filestat;
 time_t mtime[FILES];

 memset(&fd,0,FILES*sizeof(int));
 memset(&mtime,0,FILES*sizeof(time_t));

 pthread_cleanup_push(files_thread_cleanup,(void*)fd);

 while(1)
 {
  THREAD_EXIT
  usleep(conf->usec);
  for(fcount=0; fcount<FILES; fcount++)
  {
   f=fd[fcount];
   if(f==NULL)  // file not open
   {
    f=fopen(files[fcount],"r");
    if(f)
      fd[fcount]=f;
    else
      continue;
   }

   if(stat(files[fcount],&filestat)==-1)
    continue;
   if(filestat.st_mtime==mtime[fcount])
    continue;
   mtime[fcount]=filestat.st_mtime;
   DEBUG("logfile modified %s\n",get_date(filestat.st_mtime));

   fseek(f,0,SEEK_SET);
   clearerr(f);
   h=FILE_BUF_LEN;
   while(h>0)
   {
    ret=fread(&FileBuffer[RECORD_SIZE*RECORDS-h],1,h,f);

    h-=ret;
    if(h && (feof(f) || ferror(f)))
      break;
   }
   if(h)  // error reading file
     continue;

   now=time(NULL);
   for(i=0; i<RECORDS; i++)
   {
    start=FileBuffer+(i*RECORD_SIZE);
    rec=(struct part_monitor*)(((void*)start)+MONITOR_OFFSET);
    // new event ?
    
    if( (now-rec->time_end)<=TIMEDIFF)
    {
     // copy data from buffer to record
     memcpy((void*)&record.caller,(void*)start,MONITOR_OFFSET/2);
     memcpy((void*)&record.called,(void*)(start+MONITOR_OFFSET/2),MONITOR_OFFSET/2);
     memcpy((void*)&record.time_begin,(void*)rec,sizeof(struct part_monitor));

     // call event handler
     handle_event(filetype[fcount],record,*conf);
     // wait some time - won't see many messages for one event
     usleep(EVENT_DELAY);
     break;
    }
   }
  }
 }

 pthread_cleanup_pop(0);
 pthread_exit(NULL);
}

void klog_thread_cleanup(void* arg)
{
 DEBUG("klog thread shutting down...\n");
 /* "Close the log. Currently a NOP." */
 klogctl(0, NULL, 0);
 DEBUG("klog thread shutting down - done\n");
}

// If KLOG_POLL is not set, we use klogctl(2,...) to read from log.
// This works only well if no other process (e.g. klogd) use 
// klogctl(2,...) or the file /proc/kmsg. If a kernel module 
// produce an "Oops" then the loging to klogctl(2,...) will stopped.
// A workaround is to read periodically up to the last 4k of log 
// (klogctl(3,...)) and search for new events. A mark (KLOG_MARK) 
// is set to find the last event. To do so set KLOG_POLL an hope 
// the mark is really put to log.
// A 3th method is to read the buffer an then clear the log. In this
// way we need no mark (see above). I call it KLOG_POLL_AND_CLEAR.
// With KLOG_POLL_CLEAR_AFTER_EVENT set it clears the buffer not after 
// every read - just after an event.
void* poll_klog(void* config)
{
 struct monitor_conf* conf=(struct monitor_conf*)config;
 int ret,len;
 char* from,*to,*buf;
 struct monitor record;
#ifdef KLOG_POLL
 int mark_len;
 mark_len=strlen(KLOG_MARK);
 openlog("vodsl_monitor",0,LOG_KERN);
#endif
 int via;

 /* "Open the log. Currently a NOP." */
 klogctl(1, NULL, 0);

#ifdef KLOG_POLL_AND_CLEAR
 // clear the buffer
 klogctl(5,Klog_Buffer,KLOG_BUF_LEN);
#endif

 pthread_cleanup_push(klog_thread_cleanup,NULL);

 while(1)
 {
#if defined(KLOG_POLL) || defined(KLOG_POLL_AND_CLEAR)
  THREAD_EXIT
  usleep(conf->usec);
#endif

#if defined(KLOG_POLL) || defined(KLOG_POLL_CLEAR_AFTER_EVENT)
  ret=klogctl(3,Klog_Buffer,KLOG_BUF_LEN);
#elif KLOG_POLL_AND_CLEAR
  ret=klogctl(4,Klog_Buffer,KLOG_BUF_LEN);
#else
  ret=klogctl(2,Klog_Buffer,KLOG_BUF_LEN);
#endif
  if(ret<0)
  {
   ERROR("klogctl() %s\n",strerror(errno));
   continue;
  }

  Klog_Buffer[ret>=KLOG_BUF_LEN?KLOG_BUF_LEN:ret]=0;
  buf=from=Klog_Buffer;
#ifdef KLOG_POLL
  while(from=strstr(buf,KLOG_MARK))
    buf=from+mark_len;
#endif    
  from=strstr(buf,"pPhone: ");
  if(!from)
   continue;
  from+=8;
  to=strstr(from," clidInfo->number");
  if(!to)
   continue;

  //fix if line looks like this: pPhone: PSTN clidInfo->number PSTN pName: O clidInfo->name O
  if( strstr(from,"PSTN clidInfo->number") || strstr(to+17," PSTN pName: O clidInfo->name O") )
  {
    len=0;
    DEBUG("goto PSTN\n");
    goto PSTN;
  }

  len=to-from;
  if(len<0)
    continue;
  if(len>32)
    len=32;
  
  // new Event
   via=1;

  if(len==0) // no calling number - PSTN ?
   if(strstr(to+17,"pName: O clidInfo->name O"))
   {
PSTN:
    via=0;
    to=from;
    // wait for transmit CLIP signal 
    usleep(conf->clip_delay*1000);
#if defined(KLOG_POLL) || defined(KLOG_POLL_CLEAR_AFTER_EVENT)
    ret=klogctl(3,Klog_Buffer,KLOG_BUF_LEN);
#elif KLOG_POLL_AND_CLEAR
    ret=klogctl(4,Klog_Buffer,KLOG_BUF_LEN);
#else
    ret=klogctl(2,Klog_Buffer,KLOG_BUF_LEN);
#endif
    Klog_Buffer[ret>=KLOG_BUF_LEN?KLOG_BUF_LEN:ret]=0;
    buf=from=Klog_Buffer;
#ifdef KLOG_POLL
    while(from=strstr(buf,KLOG_MARK))
      buf=from+mark_len;
#endif

    from=strstr(buf,"Number Field (up to 20 digits): ");
    if(from)
    {
      DEBUG("parsing PSTN Call-id\n");
      from+=32;
      to=from;
      while(isalnum(*to)&& to-from<20)to++;
      len=to-from;
      if(len>32)
        len=32;
    }
    else
     from=to;
   }

#ifdef KLOG_POLL
  // set mark to log
  syslog(LOG_ERR,KLOG_MARK);
#endif

#ifdef KLOG_POLL_AND_CLEAR
  // clear the buffer
  klogctl(5,Klog_Buffer,KLOG_BUF_LEN);
#endif
  // get event data and call event handler
  memset((void*)&record,0,sizeof(struct monitor));
  memcpy((void*)&record.caller,(void*)from,len);
  record.time_begin=time(NULL);
  record.via=via;
  record.line=1;
  record.answered=0;

  handle_event(_IN_,record,*conf);
 }

 pthread_cleanup_pop(0);
 pthread_exit(NULL);
}


// open a server socket and bind it to any addresses and port
int open_and_bind_socket(int port,struct sockaddr_in *addr)
{
  int sock;
  int opt = 1; 

#ifndef IPv6
  sock=socket(PF_INET,SOCK_STREAM,0);
#else
  sock=socket(PF_INET6,SOCK_STREAM,0);
#endif

  if(sock==-1)
  {
   return -1;
  }

  //local port reuse in TIME_WAIT
  setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&opt,sizeof(opt));

#ifndef IPv6
  addr->sin_family = PF_INET;
#else
  addr->sin_family = PF_INET6;
#endif
  addr->sin_port = htons(port);
  addr->sin_addr.s_addr = INADDR_ANY;

  if(bind(sock,(const struct sockaddr *)addr,sizeof(struct sockaddr_in))!=0)
  {
   close(sock);
   return -3;
  }

  if(listen(sock,5)!=0)
  {
   close(sock);
   return -2;
  }

  return sock;
}


struct server_thread_cleanup_arg
{
	struct sock_list** list;
	int sock;
};

void server_thread_cleanup(void* args)
{
 struct server_thread_cleanup_arg arg=*(struct server_thread_cleanup_arg*)args;
 struct sock_list* iter;


 DEBUG("server thread shutting down...\n");
 close(arg.sock);

 iter=NULL;
 while((iter=sock_list_iter(*arg.list,iter))!=NULL)
 {
   close(iter->sock);
   iter->sock=-1;
 }
 sock_list_free(arg.list);

 DEBUG("server thread shutting down - done\n");
}


void* server_thread(void* config)
{
 struct monitor_conf* conf=(struct monitor_conf*)config;
 int sock,ret,flags,retc;
 struct sockaddr_in addr;
 socklen_t addr_len=sizeof(addr);
 struct sock_list* list=NULL,*iter,*client;
#ifdef SOCK_SELECT
 fd_set fdset;
 struct timeval timeout;
#endif
 const int buf_len=10;
 char buf[buf_len];
 struct server_thread_cleanup_arg args; 

 while((list=sock_list_init())==NULL)
 {
  THREAD_EXIT
  ERROR("sock_list_init() - next try in on second\n");
  sleep(1); // next try in one second
 }
 conf->private=(void*)list;

 // open server socked an bind to address and port
 while((sock=open_and_bind_socket(conf->port,&addr))<0)
 {
  THREAD_EXIT
  ERROR("Error (%d/%d): open_and_bind_socket(%d) - %s - next try in on second\n",
  	sock,errno,conf->port,strerror(errno));
  sleep(1); // next try in one second
 }
 
 // set thread cleanup function
 args.list=&list;
 args.sock=sock;
 pthread_cleanup_push(server_thread_cleanup,(void*)&args);

#ifdef SOCK_SELECT
 timeout.tv_sec=1;
 timeout.tv_usec=conf->usec;
#endif
 flags=MSG_DONTWAIT;
 while(1)
 {
  THREAD_EXIT
#ifdef SOCK_SELECT
  FD_ZERO(&fdset);
  FD_SET(sock,&fdset);
  select(sock+1,&fdset,NULL,NULL,&timeout);
  if(!FD_ISSET(sock,&fdset))
    continue;
#endif
  ret=accept(sock,(struct sockaddr *)&addr,&addr_len);
  if(ret<0)
    continue;

  // delete closed connection
  iter=NULL;
  while((client=iter,iter=sock_list_iter(list,iter))!=NULL)
   {
    retc=recv(iter->sock,buf,buf_len,flags);
    if(retc==0)
    {
      close(iter->sock);
      sock_list_del(list,iter->sock);
      iter=client;
    }
   }  

  DEBUG("connection from %s:%d\n",inet_ntoa(addr.sin_addr),addr.sin_port);
  fcntl(ret, F_SETFL, O_NONBLOCK);
  if(sock_list_add(list,ret)!=0)
    close(ret);
 }

 pthread_cleanup_pop(0);
}

void main_thread(void* config)
{
 volatile struct monitor_conf conf=*(struct monitor_conf*)config;
 int err;

 conf.private=NULL;
 if(conf.port<0 || conf.port>65535)
  conf.port=DEFAULT_PORT;
 
 global_time_fmt=conf.time_fmt;

 if(pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,NULL))
   ERROR("pthread_setcancelstate() %s\n",strerror(errno));

 // start poll_klog in an extra thread
 DEBUG("start klog thread...\n");
 while((err=pthread_create(&klog_th,NULL,poll_klog,(void*)&conf))!=0)
 {
   ERROR("pthread_create() %s - next try in on second\n",strerror(errno));
   sleep(1); // next try in one second
 }

 // start poll_files in an extra thread
 DEBUG("start files thread...\n");
 while((err=pthread_create(&file_th,NULL,poll_files,(void*)&conf))!=0)
 {
   ERROR("pthread_create() %s - next try in on second\n",strerror(errno));
   sleep(1); // next try in one second
 }

 if(conf.start_server)
 {
  DEBUG("start server thread...\n");
  while((err=pthread_create(&server_th,NULL,server_thread,(void*)&conf))!=0)
  {
    ERROR("pthread_create() %s - next try in on second\n",strerror(errno));
    sleep(1); // next try in one second
  }
 }

 // register signal handlers
 signal(SIGHUP,SIG_IGN);
 signal(SIGINT,shutdown_sighandler);
 signal(SIGQUIT,shutdown_sighandler);
 signal(SIGABRT,shutdown_sighandler);
 signal(SIGTERM,shutdown_sighandler);
 signal(SIGUSR1,SIG_IGN);
 signal(SIGUSR2,SIG_IGN);
 signal(SIGCHLD,SIG_IGN);
 signal(SIGTRAP,SIG_IGN);
 signal(SIGALRM,SIG_IGN);
 signal(SIGURG,SIG_IGN);
 signal(SIGPIPE,SIG_IGN);
 signal(SIGVTALRM,SIG_IGN);

 pthread_join(file_th,NULL);
 pthread_join(klog_th,NULL);
 pthread_join(server_th,NULL);
}

