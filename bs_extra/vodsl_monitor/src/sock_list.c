#include <stdlib.h>

#ifdef SEMAPHORE
#include <stdio.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <errno.h>
#include <string.h>
#endif

#include "vodsl_monitor.h"
#include "sock_list.h"


// spezial value for head of list
#define HEAD_SOCK (-255)

#ifdef SEMAPHORE
// semaphore variables
#define LOCK       -1
#define UNLOCK      1
#define PERM 0666   
#define KEY 12131415 


int init_semaphore(void) 
{
  int semid;

  // extist semaphore?
  semid = semget (KEY, 0, IPC_PRIVATE);
  if(semid<0) 
  {
   // create semaphore
   umask(0);
   semid = semget(KEY,1,IPC_CREAT|IPC_EXCL|PERM);
   if(semid<0) 
   {
     ERROR("create semaphore\n");
     return -1;
   }

   // init semaphore
   if(semctl(semid,0,SETVAL,(int)1)==-1)
     return -1;
  }
  DEBUG("semget() %d\n",semid);
  return semid;
}

void set_semaphore(int semid,int op) 
{
  struct sembuf semaphore;

  if(semid<0)
   return;
  semaphore.sem_num =0;
  semaphore.sem_op = op;
  semaphore.sem_flg = SEM_UNDO;
  if(semop(semid,&semaphore,1)==-1) 
   ERROR("semop() %d %s\n",semid,strerror(errno));
  
  DEBUG("set semaphore %s\n",op==LOCK?"LOCK":"UNLOCK");
}
#endif

struct sock_list* sock_list_init(void)
{
 struct sock_list* list;

 // get memory for head of list
 list=(struct sock_list*)malloc(sizeof(struct sock_list));
 if(!list)
   return NULL;

 // init head of list
 list->next=NULL;
 list->sock=HEAD_SOCK;
#ifdef SEMAPHORE 
 list->sem=init_semaphore();
#endif

 return list;
}

int sock_list_add(struct sock_list* list,int sock)
{
 struct sock_list* h,*i;
 int count;

 if(!list)
  return 1;

 // get memory for new element
 h=(struct sock_list*)malloc(sizeof(struct sock_list));
 if(!h)
  return 2;

 // init element
 h->sock=sock;
 h->next=NULL;
#ifdef SEMAPHORE
 h->sem=-1;

 set_semaphore(list->sem,LOCK);
#endif
 // goto end of list
 count=0;
 for(i=list; i->next; i=i->next)
  count++;
 if(count>MAX_SOCK_LIST_LEN)
 {
#ifdef SEMAPHORE
  set_semaphore(list->sem,UNLOCK);
#endif
  free(h);
  return 3;
 }
 // appand element
 i->next=h;

#ifdef SEMAPHORE
 set_semaphore(list->sem,UNLOCK);
#endif

 return 0;
}


void sock_list_del(struct sock_list* list,int sock)
{
 struct sock_list* h,*i;

 if(!list)
   return;

#ifdef SEMAPHORE
 set_semaphore(list->sem,LOCK);
#endif
 // search element
 for(h=i=list; i; h=i,i=i->next)
 {
  if(i->sock==sock)
  {
   // delete element
   h->next=i->next;
   free(i);
   break;
  }
 }
#ifdef SEMAPHORE
 set_semaphore(list->sem,UNLOCK);
#endif
}

// set "iter" to NULL at begin of iteration
struct sock_list* sock_list_iter(struct sock_list* list,struct sock_list* iter)
{
 struct sock_list* i;

 if(!list)
  return NULL;

 if(iter==NULL)
 {
  return list->next;
 }

#ifdef SEMAPHORE
 set_semaphore(list->sem,LOCK);
#endif
 // search element
 for(i=list->next; i; i=i->next)
 {
  if(i==iter)
  {
#ifdef SEMAPHORE
    set_semaphore(list->sem,UNLOCK);
#endif    
    return i->next;
  }
 }
#ifdef SEMAPHORE
 set_semaphore(list->sem,UNLOCK);
#endif
 return NULL;
}

void sock_list_free(struct sock_list** list)
{
 struct sock_list* h;
#ifdef SEMAPHORE
 int sem=(*list)->sem;
#endif

 if(!list)
  return;

#ifdef SEMAPHORE
 set_semaphore(sem,LOCK);
#endif
 while(*list)
 {
  h=(*list)->next;
  free(*list);
  *list=h;
 }
#ifdef SEMAPHORE
 set_semaphore(sem,UNLOCK);
 // free semaphore
 semctl(sem,0,IPC_RMID,0); 
#endif
}

