#ifndef __MONITOR_H
#define __MONITOR_H

#include <time.h>

#define MONITOR_FILE_IN0 "/var/tcm_incomingep0.log"
#define MONITOR_FILE_IN1 "/var/tcm_incomingep1.log"
#define MONITOR_FILE_OUT0 "/var/tcm_outgoingep0.log"
#define MONITOR_FILE_OUT1 "/var/tcm_outgoingep1.log"

#define DEFAULT_PORT 1012

// names of environment variables for event script
#define SCRIPT_ENV_EVENT    "EVENT"
#define SCRIPT_ENV_CALLER   "CALLER"
#define SCRIPT_ENV_LINE     "LINE"
#define SCRIPT_ENV_DATE     "EVENT_DATE"
#define SCRIPT_ENV_DURATION "DURATION"

// names for VIA field
#define VIA_PSTN   "PSTN"
#define VIA_SIP    "SIP"
#define VIA_DEFAULT "SIP"

#define TIME_UTC 0
#define TIME_CET 1
#define TIME_LOCAL 2

struct monitor{
	char caller[32];      // caller phone number 
	char called[32];      // called party number
	time_t time_begin;    // starting time of call
	time_t time_end;      // end of call
	time_t call_duration; // duration of call
	int answered;         // call answered
	int line;             // line port on router (1/2)
	int via;              // call comes via SIP, ISDN, etc
};

struct monitor_conf{
	int port;             // Server port
        int time_fmt;         // Time format
        int clip_delay;       // CLIP-delay
	unsigned long usec;   // Delay for polling monitor-files
	char* program;        // Program to start on event
	int start_server;     // Start server
	void* private;	      // Private data
};

void main_thread(void*);


#endif //__MONITOR_H
