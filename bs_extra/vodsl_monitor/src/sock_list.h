#ifndef __SOCK_LIST_H
#define __SOCK_LIST_H

#define MAX_SOCK_LIST_LEN 50


struct sock_list{
	struct sock_list* next;
	int sock;
#ifdef SEMAPHORE	
	int sem;
#endif	
};

// init a list
struct sock_list* sock_list_init(void);

// add an element to list
int sock_list_add(struct sock_list* list,int sock);

// delete an element
void sock_list_del(struct sock_list* list,int sock);

// go through the list an get elements
// set "iter" to NULL at begin of iteration
// NULL is returned if end of list is reached, otherwise a list element
struct sock_list* sock_list_iter(struct sock_list* list,struct sock_list* iter);

// release the whole list
void sock_list_free(struct sock_list** list);

#endif //__SOCK_LIST_H

