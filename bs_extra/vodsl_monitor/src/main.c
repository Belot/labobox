#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <getopt.h>
#include <sys/types.h>

#include "vodsl_monitor.h"
#include "monitor.h"


void print_help(char* name)
{
 printf("VoDSL Monitor 0.1.1 by Michael Finsterbusch <fibu@users.sourceforge.net>\n\n"
        "Usage: %s [-p <port>] [-s <script>] [-t <utc|cet|local>] [-c <clip_delay>] [-n] [-d] [-D]\n" //[-P <pid-file>]\n"
 	"       %s [-h]\n\n"
	"Options:\n"
	" -p <port>          Set server port (default: 1012)\n"
	" -s <script>        Set a script or program to run on event\n"
        " -t <utc|cet|local> Set time format in events\n"
        " -c <clip_delay>    CLIP-delay in ms, default=800\n"
	" -n                 Don't start the server\n"
	" -d                 Debug\n"
	" -D                 Daemon mode\n",
//	" -P <pid-file>  Set a pid-file\n",
	name,name);
}


int main(int argc, char *argv[])
{
 char** argvh=argv;
 struct monitor_conf conf;
 int opt,daemon;
// char* pidfile=NULL;
 pid_t pid;

 daemon=0;
 global_debug=0;
 conf.usec=300000; // 0.3 sec - poll about 3 time per second
 conf.port=DEFAULT_PORT;
 conf.start_server=1;
 conf.program=NULL;
 conf.time_fmt=TIME_UTC;
 conf.clip_delay=800;

 while((opt=getopt(argc,argvh,"hp:t:ns:c:dD"))!=EOF)
 {
  switch(opt)
  {
   case 'p': // Port
	conf.port=atoi(optarg);
	if(conf.port<1||conf.port>65535)
	{
	 printf("invalid port %d\n",conf.port);
	 return 0;
	}
	break;
   case 's': // Script
   	conf.program=optarg;
	break;
   case 'c': // CLIP delay
        conf.clip_delay=atoi(optarg);
        if(conf.clip_delay<1||conf.clip_delay>10000)
        {
          printf("invalid CLIP-delay %d\n",conf.clip_delay);
          return 0;
        }
        break;
   case 'n': // no Server
	conf.start_server=0;
	break;
   case 'd': // Debug
   	global_debug=1;
	break;
   case 'D': // Daemon mode
   	daemon=1;
	break;
   case 't': // Daemon mode
        if( strncmp(optarg,"utc",3)==0 )
        {
          conf.time_fmt=TIME_UTC;
        }
        else if( strncmp(optarg,"cet",3)==0 )
        {
          conf.time_fmt=TIME_CET;
        }
        else if( strncmp(optarg,"local",5)==0 )
        {
          conf.time_fmt=TIME_LOCAL;
        }
        else
        {
          printf("invalid time format %s\n",optarg);
          return 0;
        }
        break;
/*	
   case 'P': // PID file
   	pidfile=optarg;
	break;
*/
   case 'h': // Help
   default:
   	print_help(argv[0]);
	return 0;
	break;
  }
 }

 if(daemon)
 {
  pid=fork();
  if(pid<0) // Error
  {
   ERROR("fork() %s\n",strerror(errno));
   return 1;
  }

  if(pid>0) // parent
   return 0;

  // child go on
 }

 main_thread((void*)&conf);
 DEBUG("all threads terminated proper\n");
 DEBUG("exit\n");
 
 return 0;
}
