#ifndef __VODSL_MONITOR_H
#define __VODSL_MONITOR_H


// debug 0=OFF 1=ON
volatile int global_debug;

#define DEBUG(...) if(global_debug) \
		    { printf("DEBUG: %s(%d): ",__FILE__,__LINE__);     \
                      printf(__VA_ARGS__); }

#define ERROR(...) { printf("ERROR: %s(%d): ",__FILE__,__LINE__);     \
		     printf(__VA_ARGS__); }


#endif //__VODSL_MONITOR_H
