#!/bin/sh

[ "$#" -lt "5" ] && exit 1

export PATH=/bin:/etc/start_scripts:$PATH
CUSTOM="/var/custom_call_event.sh"

load_custom_script ()
{
 if [ ! -e $CUSTOM ]
 then
   RET=`nvram getfile custom_call_event=$CUSTOM`
   [ "$RET" == "Failed" ] && exit 0
 fi
 chmod +x $CUSTOM
 #$CUSTOM $@
 /bin/sh $CUSTOM $@
}

echo "Call event..."
ledtool 1 3

if [ "$EVENT" == "CALL" ]
then
 case "$CALLER" in
  "77*0*")
  	echo "SSH start"
	start=`nvram get ssh_start`
        [ "$start" != "1" ] || nvram set ssh_start=1 >/dev/null 2>/dev/null
        ssh.sh restart
	;;
  "77*1*")
  	echo "SSH stop"
	ssh.sh stop
	;;
  "77*2*"*)
  	echo "set IP to br0"
  	IP=`echo "$CALLER"|sed -e 's/^77\*2\*//'|sed -e 's/[*]/./g'`
	IP=`/webs/cgi-bin/isip.sh $IP`
	echo $IP
	[ "$IP" != "ERROR" ] && ifconfig br0 $IP up
	;;
  "77*3*12345")
  	echo "flush firewall"
	iptables -F
	iptables -P INPUT ACCEPT
	iptables -P OUTPUT ACCEPT
	iptables -P FORWARD ACCEPT
        #iptables -t nat -F
        #iptables -t mangle -F
	;;
  "77*4*12345")
  	echo "flush ebtables"
	ebtables -F
	ebtables -P INPUT ACCEPT
	ebtables -P OUTPUT ACCEPT
	ebtables -P FORWARD ACCEPT
	ebtables -t nat -F
        #ebtables -t broute -F
	;;
  "77*5*")
  	echo "flash LEDs"
	ledtool X 7 
	ledtool X 10
	sleep 1
	ledtool 0 1
	;;
  "96*0*")
  	echo "WLAN start"
	wlan.sh restart
  	;;
  "96*1*")
  	echo "WLAN stop"
	wlan.sh stop
	;;
  "96*4*")
  	echo "Call monitor server start"
	nvram set call_mon_start=1
	call_mon.sh restart
	;;
  "96*5*")
        echo "Call monitor server stop"
	nvram set call_mon_start=0
        call_mon.sh restart
        ;;
  "96*7*")
  	echo "telnet start"
  	start=`nvram get telnet_start`
	[ "$start" != "1" ] && nvram set telnet_start=1 >/dev/null 2>/dev/null
	telnet.sh restart
	;;
  "96*8*")
  	echo "telnet stop"
	telnet.sh stop
	;;
  "990*15901590*")
  	echo "reboot"
	nvram reboot
	;;
  "991*15901590*")
  	echo "nvram reset and reboot"
	nvram reset
	nvram reboot
	;;
  *)
     load_custom_script $@
     ;;
 esac

 exit 0
fi

if [ "$EVENT" == "RING" ]
then
 case "$CALLER" in
   *)
      load_custom_script $@
      ;;
 esac
 exit 0
fi

if [ "$EVENT" == "DISCONNECT" ]
then
 case "$CALLER" in
   *)
      load_custom_script $@
      ;;
 esac
 exit 0
fi
