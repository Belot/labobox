/*
 * Copyright (C) 2008 Michael Finsterbusch (fibu at users.sourceforge.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/if.h>

#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

#include "capture.h"
#include "tportknockd.h"
#include "event.h"
#include "configparser.h"

static int sock_fd;

void shutdown_sighandler(int sig)
{
  signal(sig,SIG_IGN);
  DEBUG("received signal %d - shutting down...\n",sig);
  global_shutdown=SH_EXIT;
  deinit_capture(sock_fd);
  exit(0);
}

// strdup() - helper function
char* mstrdup(char* str)
{
  char* string;

  if(!str)
    return NULL;

  string=malloc(strlen(str)+1);
  if(!string)
    return NULL;

  strcpy(string,str);
  return string;
}

// get all available network device - for 'list' option
char** get_devices()
{
  int fd;
  struct ifconf ifc;
  struct ifreq *ifr;
  int num,i;
  char** dev_name;

  fd=socket(PF_PACKET, SOCK_DGRAM, 0);
  if(fd==-1)
    return NULL;

  ifc.ifc_buf = NULL;
  num=10;
  while(1)
  {
    ifc.ifc_len = sizeof(struct ifreq) * num;
    ifc.ifc_buf = realloc(ifc.ifc_buf, ifc.ifc_len);

    if(ioctl(fd, SIOCGIFCONF, &ifc) < 0) 
    {
      ERROR("ioctl(SIOCGIFCONF) - %s\n",strerror(errno));
      close(fd);
      return NULL;
    }
    if(ifc.ifc_len == sizeof(struct ifreq) * num) 
    {
      // possibly more than num devices 
      num+=10;
      continue;
    }
    break;
  }

  num=ifc.ifc_len/sizeof(struct ifreq)+1;
  dev_name=(char**)malloc(num*sizeof(char**));
  if(!dev_name)
    return NULL;

  ifr=ifc.ifc_req;
  for(i=0; i<num; i++,ifr++)
  {
    dev_name[i]=mstrdup(ifr->ifr_name);
  }
  dev_name[i]=NULL;
  return dev_name;
}

// check the byte order of this machine
void byte_order_test()
{
 union test{
   short value;
   char byte[2]; 
 }test;
 int big;
 
 test.value=0x1122;
 if(test.byte[0]==0x11)
   big=1;
 else
   big=0;
 
#ifdef BIG_ENDIAN
  if(!big)
    printf("Warning: this is a little-endian machine but this program\n" 
	   "         is build for big-endian!\n\n");
#else
  if(big)
    printf("Warning: this is a big-endian machine but this program\n"
	   "         is build for little-endian!\n\n");
#endif
}

void print_help(char* name)
{
  printf("tportknockd "VERSION" by Michael Finsterbusch <fibu at users.sourceforge.net>\n"
	 "tiny port knocking daemon\n\n"
         "Usage: %s -c <config> -i <device> [-r <sec>] [-d] [-D]\n"
         "       %s -C <config>\n"
         "       %s -l\n"
         "       %s -h\n\n"
         "Options:\n"
         " -c <config>   Give the configuration file with port knock secquences\n"
         " -C <config>   Check the configuration file with port knock secquences\n"
         "               for typos and syntax errors and display all sequences\n"
         " -i <device>   Interface to listen on\n"
         " -r <sec>      Refresh ip address - get every <sec> seconds the ip address\n"
	 "               from the device. This could be useful if the ip address\n"
	 "               changes periodically (e.g. 24h auto disconnect form internet\n"
	 "               service provider). If needed use a value about 60 seconds.\n"
         " -l            List all available network devices\n"
         " -d            Debug\n"
         " -D            Daemon mode\n"
         " -h            This help\n",
        name,name,name,name);
}

int main(int argc,char* argv[])
{
 int fd,err,check;
 char* dev=NULL,*config=NULL;
 char** devices=NULL,**h,**argvh=argv;
 int opt,daemon;
 pid_t pid;
 char* bin_name;
 long ip_refrech_interval=0;
 int retry;

 global_debug=0;
 global_shutdown=0;
 daemon=0;
 check=0;

 byte_order_test();
 // check for too long names on help screen
 if(strlen(argv[0])>26)
 {
   bin_name=mstrdup(argv[0]);
   bin_name+=strlen(bin_name)-26;
   memset((void*)bin_name,'.',3);
 }
 else
   bin_name=argv[0];

 if(argc<2)
 {
   print_help(bin_name);
   return 0;
 }

 while((opt=getopt(argc,argvh,"hli:c:C:dDr:"))!=EOF)
 {
  switch(opt)
  {
    case 'h':
	print_help(bin_name);
	return 0;
	break;
    case 'l':
	printf("available devices: ");
	devices=get_devices();
 	if(devices)
 	{
    	  h=devices;
   	  while(*h && strlen(*h)>0)
   	  {
	    if(h==devices)
     	      printf("%s\n",*h);
            else
	      printf("                   %s\n",*h);
            free(*h);
            h++;
   	  }
   	  free(devices);
 	}
 	else
          {
	    printf("\n");
            if(getuid())
              printf("you need root permissions\n");
          }
        return 0;
	break;
    case 'i':
	dev=mstrdup(optarg);
	break;
    case 'c':
	config=mstrdup(optarg);
	break;
    case 'C':
	config=mstrdup(optarg);
	check=1;
	break;
    case 'd':
	global_debug=1;
	break;
    case 'D':
	daemon=1;
	break;
    case 'r':
	ip_refrech_interval=atol(optarg);
	if(ip_refrech_interval<1)
	{
	  printf("the ip refresh interval must be greather than 0\n");
	  return 1;
	}
        if(ip_refrech_interval<20)
	  printf("warning: the ip refresh interval is very small\n");
	break;
    default:
	//print_help(bin_name);
	return 0;
	break;
  }
  if(check)
    break;
 }

 if(!dev && !check)
 {
   printf("no device specified!\n");
   return 1;
 }
 if(!config)
 {
   printf("no configuration file specified!\n");
   return 1;
 }

 err=parse(config);
 if(err>0)
 {
   printf("Errors: %d\nabort due to configuration errors\n",err);
   return 2;
 }
 if(err<0)
   return 3;

 if(check)
 {
   print_events();
   return 0;
 }

 // daemon mode
 if(daemon)
 {
  pid=fork();
  if(pid<0) // Error
  {
   ERROR("fork() - %s\n",strerror(errno));
   return 1;
  }

  if(pid>0) // parent
   return 0;

  // child go on
 }

 retry=0;
 do{
   global_shutdown=SH_DEFAULT;

   fd=init_capture(dev);
   if(fd<0)
   {
    switch(fd)
    {
     case -1:
      	ERROR("socket() - %s\n",strerror(errno));
      	break;
     case -2:
	  printf("No device specified\n");
	  break;
     case -3:
	  printf("No such device '%s'\n",dev);
	  break;
     case -4:
	  ERROR("unable to set device to promisc mode - %s\n",strerror(errno));
	  break;
     case -5:
	  ERROR("can not create ring buffer - %s\n",strerror(errno));
	  ERROR("you must enable PACKET_MMAP in your kernel\n");
	  break;
     case -6:
	  ERROR("mmap() failed - %s\n",strerror(errno));
	  break;
     case -7:
	  ERROR("bind() failed - %s\n",strerror(errno));
	  break;
     case -8:
	  ERROR("can not get local ip address - %s\n",strerror(errno));
	  break;
     default:
	  ERROR("unknown error\n");
	  break;
    }
    if(retry>10)
    {
      ERROR("can not init device...exit\n");
      return 5;
    }
    if(retry)
    {
      ERROR("Error while init device... next try in 10 seconds\n");
      retry++;
      sleep(10);
      continue;
    }
    if(getuid())
      printf("you need root permissions\n");
    return 1;
   }

   sock_fd=fd;
   // register signal handlers
   signal(SIGHUP,SIG_IGN);
   signal(SIGINT,shutdown_sighandler);
   signal(SIGQUIT,shutdown_sighandler);
   signal(SIGABRT,shutdown_sighandler);
   signal(SIGTERM,shutdown_sighandler);
   signal(SIGUSR1,SIG_IGN);
   signal(SIGUSR2,SIG_IGN);
   signal(SIGCHLD,SIG_IGN);
   signal(SIGTRAP,SIG_IGN);
   signal(SIGALRM,SIG_IGN);
   signal(SIGURG,SIG_IGN);
   signal(SIGPIPE,SIG_IGN);
   signal(SIGVTALRM,SIG_IGN);

   if(ip_refrech_interval>0)
     ip_refresh(ip_refrech_interval);

   retry=1;
   capture(fd);
   deinit_capture(sock_fd);
 }while(ip_refrech_interval>0 && global_shutdown==SH_NEW_IP);

 return 0;
}

