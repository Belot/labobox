/*
 * Copyright (C) 2008 Michael Finsterbusch (fibu at users.sourceforge.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <linux/if_ether.h>
#include <arpa/inet.h>
#include <linux/if.h>
#include <linux/if_packet.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/poll.h>

#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>

#include "capture.h"
#include "tportknockd.h"
#include "event.h"

static void* mmap_ptr=NULL;
static void* ring_buffer=NULL;
static unsigned long mmap_size;
static unsigned long ring_framesize;
static struct ifreq ifdev;
static struct in_addr dev_ipaddr;
static long ip_update_interval;
static int sock_fd;

// init socket, device, ring buffer and mmap
int init_capture(char* device)
{
 int fd,size;
 struct ifreq dev;
 struct tpacket_req req;
 struct sockaddr_ll sll;
 int mtu,block_size,frame_size;

 if(!device)
   return -2;

 // create socket - recieve all ip datagrams
 fd=socket(PF_PACKET, SOCK_DGRAM, htons(ETH_P_ALL));
 if(fd==-1)
   return -1;

 // get device index
 memset(&dev, 0, sizeof(dev));
 strncpy(dev.ifr_name,device,sizeof(dev.ifr_name));
 if(ioctl(fd,SIOCGIFINDEX,&dev)==-1)
 {
   close(fd);
   return -3;
 }

 // bind socket to device
 memset(&sll, 0, sizeof(sll));
 sll.sll_family= AF_PACKET;
 sll.sll_ifindex= dev.ifr_ifindex;
 sll.sll_protocol= htons(ETH_P_ALL);
 if(bind(fd,(struct sockaddr*)&sll,sizeof(sll)) == -1) 
 {
   close(fd);
   return -7;
 }

 if(ioctl(fd,SIOCGIFADDR,&dev) == -1) 
   {
     close(fd);
     return -8;
   }
 dev_ipaddr=(*(struct sockaddr_in *)&dev.ifr_addr).sin_addr;
 DEBUG("ip address is %s\n",inet_ntoa(dev_ipaddr));

 ifdev=dev;
 /*
 // set device in promisc mode
 if(!(dev.ifr_flags & IFF_PROMISC))
 {
   dev.ifr_flags |= IFF_PROMISC;
   if(ioctl(fd,SIOCSIFFLAGS,&dev) == -1) 
   {
     close(fd);
     return -4;
   }
 }
 */

 // get mtu
 if (ioctl(fd, SIOCGIFMTU, &dev) == -1)
    mtu = 1500;
 else
    mtu = dev.ifr_mtu;
 mtu+=sizeof(struct tpacket_hdr);

 // calculate size for ringbuffer and mmap
 block_size=sysconf(_SC_PAGESIZE);//getpagesize();
 while(block_size<mtu) block_size<<=1;
 frame_size=1;
 while(frame_size<mtu) frame_size<<=1;

 DEBUG("Block size=%d  Frame size=%d\n",block_size,frame_size);
 DEBUG("MTU=%lud  MTU+pckthdr=%d\n",mtu-sizeof(struct tpacket_hdr),mtu);

 req.tp_block_size=block_size;
 req.tp_block_nr=20;
 req.tp_frame_size=frame_size;
 req.tp_frame_nr=(req.tp_block_size/req.tp_frame_size)*req.tp_block_nr;

 size=req.tp_block_size*req.tp_block_nr;
 if(setsockopt(fd, SOL_PACKET, PACKET_RX_RING, (void *) &req, sizeof(req))==-1)
 {
   deinit_capture(fd);
   return -5;
 }

 mmap_ptr=mmap(0, size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
 if(mmap_ptr==MAP_FAILED)
 {
   deinit_capture(fd);
   return -6;
 }
 ring_buffer=mmap_ptr;
 mmap_size=size;
 ring_framesize=req.tp_frame_size;

 sock_fd=fd;

 return fd;
}

// restore device and close socket
void deinit_capture(int fd)
{
 alarm(0);
 //ioctl(fd,SIOCSIFFLAGS,&ifdev); 
 if(mmap_ptr)
   munmap(mmap_ptr,mmap_size);
 close(fd);
}

// parse captured frames and call handlers
inline void parse_packet(unsigned char* packet,int pkt_len,unsigned int sec,unsigned int usec)
{
  struct ip_hdr* ip=(struct ip_hdr*)packet;
  struct tcp_hdr* tcp=NULL;
  struct udp_hdr* udp=NULL;
  union tcp_flags dummy;

  if(ip->ip_version!=4) 
    return;

  if(memcmp(&ip->ip_dst,&dev_ipaddr,sizeof(dev_ipaddr)))
    return;

  if(ip->ip_protocol==IP_PROTOCOL_TCP)
  {
    tcp=(struct tcp_hdr*)(packet+ip->ip_hl*4);
    dummy.raw=0xff;
    dummy.flags.resvd=0;
    tcp->tcp_flag.raw&=dummy.raw; // clear reversed field

    handle_event(TCP_ENTRY,ip->ip_src,ntohs(tcp->tcp_dst),tcp->tcp_flag,sec);
    return;
  }

  if(ip->ip_protocol==IP_PROTOCOL_UDP)
  {
    udp=(struct udp_hdr*)(packet+ip->ip_hl*4);

    handle_event(UDP_ENTRY,ip->ip_src,ntohs(udp->udp_dst),dummy,sec);
    return;
  }
}

// take frames from ring buffer
inline void handle_packet()
{
  struct tpacket_hdr* pkt;

  while(*(unsigned long*)ring_buffer)
  {
   pkt=(struct tpacket_hdr*)ring_buffer;

   parse_packet((unsigned char*)(pkt)+pkt->tp_mac,pkt->tp_len,pkt->tp_sec,pkt->tp_usec);

   pkt->tp_status = 0;

   ring_buffer+=ring_framesize;
   if(ring_buffer>=mmap_ptr+mmap_size)
     ring_buffer=mmap_ptr;
  }
}

// wait for frames
void capture(int fd)
{
  struct pollfd pfd;
  int timeout=1; //no timeout
  int ret;

  pfd.fd = fd;
  pfd.revents = 0;
  pfd.events = POLLIN|POLLRDNORM;

just_a_signal:
  while((ret=poll(&pfd, 1, timeout))!=-1)
  { 
    if(global_shutdown)
      break;
    if(!ret)
      continue;
    handle_packet();
  }
  if(errno!=EINTR)
  {
    ERROR("poll() - %s\n",strerror(errno));
  }
  else if(!global_shutdown)
      goto just_a_signal;
}

// update ip address periodically
void update_ip(int sig)
{
  struct in_addr ip;

  signal(sig,update_ip);
  alarm(ip_update_interval);
  if(ioctl(sock_fd,SIOCGIFADDR,&ifdev) == -1) 
   {
     ERROR("can not get ip address - %s\n",strerror(errno));
     return;
   }

  ip=(*(struct sockaddr_in *)&ifdev.ifr_addr).sin_addr;
  if(memcmp((void*)&ip,(void*)&dev_ipaddr,sizeof(ip)))
  {
    DEBUG("ip address changed to %s\n",inet_ntoa(ip));
    dev_ipaddr=ip;
    global_shutdown=SH_NEW_IP;
  }
}

// set timer for periodically ip address update
void ip_refresh(long sec)
{
  ip_update_interval=sec;
  if(sec)
    signal(SIGALRM,update_ip);
  else
    signal(SIGALRM,SIG_IGN);
  alarm(ip_update_interval);
}

