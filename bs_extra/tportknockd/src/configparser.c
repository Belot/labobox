/*
 * Copyright (C) 2008 Michael Finsterbusch (fibu at users.sourceforge.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>

#include "configparser.h"
#include "tportknockd.h"
#include "event.h"

struct AttrValPair{
    char* attr;
    char* value;
};

// del comments
void delComment(char* string)
{
  char* pos,*str=string;

  if(!str)
    return;

  do
  {
    pos=strstr(str,COMMENT);
    if(pos)
    {
      if((pos-1>string) && (*(pos-1)=='\\')) // quotet COMMENT \#
      {
        memmove((void*)pos-1,pos,strlen(pos)+1);
	str=pos;
      }
      else  
        *pos='\0';
    }
    else
      return;
  }while(*pos);
}

// del whitespace 
char whitespace[]={' ','\t','\r','\n','\0'};
void delWhiteSpace(char** str)
{
  char* c;
  char* pos;
  int h;

  if(!str)
    return;

  //WhiteSpace at the beginnig
  while(*str)
    {
      h=0;
      for(c=whitespace; *c!='\0'; c++)
	{
	  if(**str==*c)
	    {
	      (*str)++;
	      h=1;
	      break;
	    }
	}
      if(!h)
	break;
    }

  //WhiteSpace at the end  
  while(*str)
    {
      h=0;
      pos=*str+strlen(*str)-1;
      for(c=whitespace; *c!='\0'; c++)
	{
	  if(*pos==*c)
	    {
	      *pos='\0';
	      pos--;
	      h=1;
	      break;
	    }
	}
      if(!h)
	break;
    }
}

// search attribute and values
int getAttrValue(char* line,struct AttrValPair* avp)
{
  char* pos;
  char* attr,*val;
  int len;
  
  if(!line)
    return 1;
  
  delWhiteSpace(&line);
  if(!strlen(line))
    return 1;  // empty line
  
  pos=strstr(line,"=");
  if(!pos) // no '=' found
    return -1;
  
  if(pos==line) // attribute with len zero
    return -2; 

  len=pos-line;
  attr=(char*)malloc(len+1);
  if(!attr)
  {
    ERROR("malloc() - not enough memory\n");
    return -3;
  }
  strncpy(attr,line,len);
  attr[len]='\0';

  len=strlen(line)-(pos-line);
  val=(char*)malloc(len);
  if(!val)
  {
    free(attr);
    ERROR("malloc() - not enough memory\n");
    return -3;
  }
  strncpy(val,pos+1,len-1);
  
  delWhiteSpace(&attr);
  delWhiteSpace(&val);

  avp->attr=attr;
  avp->value=val;

  return 0;
}

// check group bounds
char* getGroup(char* line,int* error)
{
  int ret,len;
  char* grp;
  
  delComment(line);
  delWhiteSpace(&line);

  if(strlen(line)==0)
  {
    *error=-1;   // empty line
    return NULL;  
  }

  if(strlen(line)>2 && line[0]=='<' && line[strlen(line)-1]=='>')
    {
      if(line[1]!='/') 
	ret=1; // start
      else
	ret=2; // end

      len=strlen(line)-ret;
      grp=(char*)malloc(len);
      if(!grp)
      {
        ERROR("malloc() - not enough memory\n");
	*error=-2;
        return NULL; 
      }
      strncpy(grp,line+ret,len-1);
      grp[len]='\0'; 
      if(strlen(grp)==0)
      {
	free(grp);
	*error=-3;
	return NULL;
      }

      *error=ret;
      return grp;
    }

  *error=0;
  return NULL;
}

#define PARSE_ERROR(...) {printf("%s(%d):",filename,linenr); \
                          printf(__VA_ARGS__); }
int parse_sequence(struct knock_list_head* head,char* seq,char* filename,int linenr)
{
  struct knock_list_entry* entry=NULL,*ent;
  char* pos;
  char protocol[4],flag_buffer[11],*flags;
  int port;

  pos=seq;
  while(*pos)
  {
    *pos=tolower(*pos);
    pos++;
  }

  while(*seq)
  {
    entry=(struct knock_list_entry*)malloc(sizeof(struct knock_list_entry));
    if(!entry)
    {
      ERROR("malloc() - not enough memory\n");
      return 4;
    }

    pos=strstr(seq," ");
    if(pos)
      *pos='\0';

    memset(protocol,0,4);
    memset(flag_buffer,0,11);
    entry->flags.raw=0;

    sscanf(seq,"%3s:%d:%10s",protocol,&port,flag_buffer);
    if(strcmp(protocol,"tcp") && strcmp(protocol,"udp"))
    {
      PARSE_ERROR("protocol %s not supported\n",protocol);
      free(entry);
      return 1;
    }

    if(strcmp(protocol,"tcp"))
    {
      entry->type=UDP_ENTRY;
      entry->flag_ignore=1;
    }
    else
      entry->type=TCP_ENTRY;

    if(port<1 || port>65535)
    {
      PARSE_ERROR("invalid port %d\n",port);
      free(entry);
      return 2; 
    }
    entry->port=(unsigned short)port;

    flags=flag_buffer;
    while(*flags)
    {
      switch(*flags)
      {
	case 'f':
	  entry->flags.flags.fin=1;
	  break;
	case 's':
	  entry->flags.flags.syn=1;
	  break;
	case 'r':
	  entry->flags.flags.rst=1;
	  break;
	case 'p':
	  entry->flags.flags.psh=1;
	  break;
	case 'a':
	  entry->flags.flags.ack=1;
	  break;
	case 'u':
	  entry->flags.flags.urg=1;
	  break;
	case 'x':
	  entry->flags.flags.fin=1;
	  entry->flags.flags.syn=1;
	  entry->flags.flags.rst=1;
	  entry->flags.flags.psh=1;
	  entry->flags.flags.ack=1;
	  entry->flags.flags.urg=1;
	  break;
	case 'n':
	  if(entry->flags.raw!=0 || strlen(flag_buffer)>1)
	  {
	    PARSE_ERROR("flag n (None) can not combined with other flags\n");
	    free(entry);
	    return 5;
	  }
	  break;
	case 'i':
	  entry->flag_ignore=1;
	  break;
	default:
	  PARSE_ERROR("unknown flag '%c'\n",*flags);
	  free(entry);
	  return 3;
	  break;
      }
      flags++;
    }

    if(head->first==NULL)
    {
      head->first=entry; 
    }
    else
    {
      ent=head->first;
      while(ent->next)
	ent=ent->next;
      ent->next=entry;
    }
    if(!pos)
      break;
    seq=pos+1;
  }

  return 0;
}

#undef PARSE_ERROR
#define PARSE_ERROR(...) {errors++; printf("%s(%d):",filename,linenr); \
                          printf(__VA_ARGS__); }

// parse config file
int parse(char* filename)
{
  const int bmax=8192;
  char Buffer[bmax];
  char* line,*group,*cur_group=NULL;
  int linenr=0,err,errors=0;
  int ingroup=0;
  struct knock_list_head* head=NULL;
  struct AttrValPair avp;
  FILE* file;

  if(!filename)
    return -1;

  file=fopen(filename,"r");
  if(!file)
  {
    ERROR("can not open file '%s' - %s\n",filename,strerror(errno));
    return -1;
  }

  while(!ferror(file) && !feof(file))
    {
      if(fgets(Buffer,bmax,file)==NULL)
        break;
      linenr++;
      line=Buffer;

      group=getGroup(line,&err);

      switch(err)
	{
	case -3:
	    PARSE_ERROR("no event name\n"); 
	    continue;
	    break;
	case -1: // empty line
	    continue;
	    break;

	case 1: // start group
	  if(ingroup==0)
	    {
	      ingroup=1;
	      cur_group=group;
	      head=(struct knock_list_head*)malloc(sizeof(struct knock_list_head));
              if(!head)
              {
                ERROR("malloc() - not enough memory\n");
                return -2;
              }
              memset((void*)head,0,sizeof(struct knock_list_head));
	      head->name=cur_group;
	      continue;
	    }
	  else
	    {
	      PARSE_ERROR("begin event (%s) in event context (%s)\n",group,cur_group); 
	      continue;
	    }
	  break;
	case 2: // end group
	  if(ingroup==0)
	    {
	      PARSE_ERROR("end of event (%s) but never startet\n",group); 
	      continue;
	    }
	  else
	    {
	      if(!strcmp(cur_group,group))
		{
		  err=0;
		  if(head->timeout<1)
		  {
		    err=1;
		    PARSE_ERROR("timeout not set\n"); 
		  }
		  if(!head->start_command)
		  {
		    err=1;
		    PARSE_ERROR("no start_command specified\n"); 
		  }
		  if(head->stop_command && head->command_delay<1)
		  {
		    err=1;
		    PARSE_ERROR("command_delay no specified\n"); 
		  }
		  if(!head->first)
		  {
		    err=1;
		    PARSE_ERROR("no knock sequence specified\n"); 
		  }
		  if(!err)
		    add_event(head);
		  ingroup=0;
		  continue;
		}
	      else
		{
		  PARSE_ERROR("found </%s> but </%s> expected\n",group,cur_group); 
		  continue;
		}
	    }
	  break;
	}

      err=getAttrValue(line,&avp);
      switch(err)
	{
	case -1:
	  PARSE_ERROR("no '=' found (Hint: comments start with '%s')\n",COMMENT);
	  continue;
	  break;
	case -2:
	  PARSE_ERROR("attribute is empty\n");
	  continue;
	  break;
	case 0:
	  break;
	default:
	  continue;
	  break;
	}

      if(ingroup)
	{
	  if(head)
          {
            if(strstr(avp.attr,"sequence"))
            {
              err=parse_sequence(head,avp.value,filename,linenr);
              if(err)
                errors++;
            }
            if(strstr(avp.attr,"timeout"))
	    {
	      head->timeout=atoi(avp.value);
	      if(head->timeout<1)
	      {
		PARSE_ERROR("timeout must be greater than 0\n");
		head->timeout=0;
	      }
	    }
            if(strstr(avp.attr,"start_command"))
	      head->start_command=mstrdup(avp.value);
            if(strstr(avp.attr,"stop_command"))
	      head->stop_command=mstrdup(avp.value);
            if(strstr(avp.attr,"command_delay"))
	    {
	      head->command_delay=atoi(avp.value);
	      if(head->command_delay<1)
	      {
		PARSE_ERROR("command_delay must be greater than 0\n");
		head->command_delay=0;
	      }
	    }
          }
	}
      else
	{
          // parameters not in group context
	}
    }
  if(ingroup)
    PARSE_ERROR("event <%s> not closed\n",cur_group);

  fclose(file);
  return errors;
};

