/*
 * Copyright (C) 2008 Michael Finsterbusch (fibu at users.sourceforge.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __TPORTKNOCKD_H
#define __TPORTKNOCKD_H

#include <stdio.h>

#define VERSION "0.1"

#define SH_DEFAULT	0
#define SH_EXIT		1
#define SH_NEW_IP	2

// debug 0=OFF 1=ON
volatile int global_debug;
volatile int global_shutdown;

#define DEBUG(...) if(global_debug) \
                    { printf("DEBUG: %s(%d): ",__FILE__,__LINE__);     \
                      printf(__VA_ARGS__); }

#define ERROR(...) { printf("ERROR: %s(%d): ",__FILE__,__LINE__);     \
                     printf(__VA_ARGS__); }

char* mstrdup(char* str);

#endif //__TPORTKNOCKD_H

