/*
 * Copyright (C) 2008 Michael Finsterbusch (fibu at users.sourceforge.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include "event.h"
#include "tportknockd.h"

static struct knock_event* kevent_udp=NULL;    // list with events start with udp
static struct knock_event* kevent_tcp=NULL;    // list with events start with udp
static struct knock_event* kevent_pending=NULL; // list of pending events

// add an event to the correspond event list
int add_event(struct knock_list_head* event)
{
  struct knock_event** head=NULL,*new;

  if(!event)
    return -1;

  if(!event->first)
    return -2;

  if(event->first->type!=TCP_ENTRY && event->first->type!=UDP_ENTRY)
    return -3;

  if(event->first->type==TCP_ENTRY)
    head=&kevent_tcp;
  if(event->first->type==UDP_ENTRY)
    head=&kevent_udp;

  if(*head)
    while((*head)->next)
      *head=(*head)->next;

  new=(struct knock_event*)malloc(sizeof(struct knock_event));
  if(!new)
  {
    ERROR("net enough memory\n");
    return -4;
  }

  memset(new,0,sizeof(struct knock_event));
  new->head=event;
  new->cur_entry=event->first;
  if(*head)
    (*head)->next=new;
  else
    *head=new;

  return 0;
}

// new event was triggered - add it to pending list
int add_event_pending(struct knock_list_head* event,struct in_addr ip_addr,unsigned int sec)
{
  struct knock_event** head,*new;

  if(!event)
    return -1;

  new=(struct knock_event*)malloc(sizeof(struct knock_event));
  if(!new)
  {
    ERROR("net enough memory\n");
    return -2;
  }

  memset(new,0,sizeof(struct knock_event));
  new->head=event;
  new->cur_entry=event->first->next;
  //memcpy((void*)&new->ip,(void*)&ip_addr,sizeof(struct in_addr));
  new->ip=ip_addr;
  new->timeout=sec+event->timeout;

  head=&kevent_pending;
  if(!*head)
  {
   kevent_pending=new;
   new->before=NULL;
   new->next=NULL;
  }
  else
  {
   (*head)->before=new;
   new->next=*head;
   new->before=NULL;
   *head=new;
  }

  return 0;
}

// replace the "%IP%" string with the ip address
char* replace_ip(char* string,char* ip)
{
  char* new=NULL,*h,*hn=NULL,*string_0=string;
  int first=0;
  if(!string || !ip)
    return string;

  while((h=strstr(string,"%IP%")))
  {
    new=(char*)realloc((void*)new,h-string+1+strlen(ip)+(new?strlen(new)+1:1));
    if(!new)
    {
      ERROR("realloc() - not enough memory\n");
      return string_0;
    }
    if(first==0)
    {
      hn=new;
      first=1;
    }
    memcpy((void*)hn,(void*)string,h-string);
    hn+=h-string;
    string=h+4;
    memcpy((void*)hn,(void*)ip,strlen(ip));
    hn+=strlen(ip);
  }

  new=(char*)realloc((void*)new,1+strlen(string)+(new?strlen(new)+1:1));
  if(!new)
    {
      ERROR("realloc() - not enough memory\n");
      return string_0;
    }
  if(first==0)
    hn=new;
  strcpy(hn,string);

  return new;
}

// whole sequence for an event received - execute start command
void start_event(struct knock_event* event)
{
  pid_t pid;
  char *argv[4];
  extern char **environ;
  char *cmd;

  if(!event->head->start_command)
    return;
  DEBUG("start event '%s'\n",event->head->name);

  pid=fork();
  if(pid==-1)
  {
    ERROR("fork() - %s\n",strerror(errno));
    return;
  }
  if(pid!=0) // parent
    return;

  if(event->head->stop_command)
  {
    pid=fork();
    if(pid==-1)
    {
      ERROR("fork() - %s\n",strerror(errno));
      return;
    }
    // parent - execute stop command after command delay
    if(pid!=0)
    {
      sleep(event->head->command_delay);
      cmd=replace_ip(event->head->stop_command,inet_ntoa(event->ip));
      DEBUG("execute '%s'\n",cmd);
      argv[0] = "sh";
      argv[1] = "-c";
      argv[2] = cmd;
      argv[3] = 0;
      execve ("/bin/sh", argv, environ);
      ERROR("execve() - %s\n",strerror(errno));
      return;
    }
  }

  if(pid==0) // child - execute start command
    {
      cmd=replace_ip(event->head->start_command,inet_ntoa(event->ip));
      DEBUG("execute '%s'\n",cmd);
      argv[0] = "sh";
      argv[1] = "-c";
      argv[2] = cmd;
      argv[3] = 0;
      execve ("/bin/sh", argv, environ);
      ERROR("execve() - %s\n",strerror(errno));
      return;
    }
}

void handle_event(unsigned char type,struct in_addr ip_addr,unsigned short port,union tcp_flags flags,unsigned int sec)
{
  struct knock_event* event=NULL,**head,*h;

#ifdef _DEBUG
    if(type==TCP_ENTRY) 
    {
      DEBUG("tcp time=%d dst=%d %s%s%s%s%s%s\n",sec,port,
		flags.flags.fin?"FIN ":"",
		flags.flags.syn?"SYN ":"",
		flags.flags.rst?"RST ":"",
		flags.flags.psh?"PSH ":"",
		flags.flags.ack?"ACK ":"",
		flags.flags.urg?"URG":"");
    }

    if(type==UDP_ENTRY)
    {
      DEBUG("udp time=%d dst=%d \n",sec,port);
    }
#endif

 // handle pending events
 event=kevent_pending;
 head=&kevent_pending;
 while(event)
 {
   // timeout?
   if(event->timeout<sec)
   {
     DEBUG("event '%s' timeout\n",event->head->name);

     if(event==*head)
     {
       if(event->next)
         event->next->before=NULL;
       *head=event->next;
     }
     else
     {
       event->before->next=event->next;
       if(event->next)
         event->next->before=event->before;
     }
     h=event->next;
     free(event);
     event=h;
     continue;
   }
   // same ip address?
   if(memcmp(&event->ip,&ip_addr,sizeof(ip_addr)))
     goto donext;
   // same protocoll?
   if(event->cur_entry->type!=type)
     goto donext;
   // same port?
   if(event->cur_entry->port!=port)
     goto donext;

   // same flags?
   if(!event->cur_entry->flag_ignore)
     if(flags.raw != event->cur_entry->flags.raw)
       goto donext;

   DEBUG("received frame for pending event '%s'\n",event->head->name);

   if(event->cur_entry->next==NULL)
   {
     // whole sequence done
     if(event==*head)
     {
       if(event->next)
         event->next->before=NULL;
       *head=event->next;
     }
     else
     {
       event->before->next=event->next;
       if(event->next)
         event->next->before=event->before;
     }
     h=event->next;
     start_event(event);
     free(event);
     event=h;
     continue;
   }
   event->cur_entry=event->cur_entry->next;

donext:
   event=event->next;
 }

 // new events
 event=NULL;
 if(type==TCP_ENTRY)
   event=kevent_tcp;
 if(type==UDP_ENTRY)
   event=kevent_udp;
 while(event)
  {
    if(!event->head)
      break;

    if(port!=event->head->first->port)
      goto next_event;

    if(!event->head->first->flag_ignore)
      if(flags.raw != event->head->first->flags.raw)
        goto next_event;

     DEBUG("received 1st frame for event '%s' from %s\n",event->head->name,inet_ntoa(ip_addr));

    if(!event->head->first->next)
    {
      // event with only one sequence entry 
      start_event(event);
      goto next_event;
    }

    add_event_pending(event->head,ip_addr,sec);
next_event:
    event=event->next;
  }
}

// print the udp and tcp event list - for 'check' option
void print_events(void)
{
  struct knock_event* event,*event_list[2];
  struct knock_list_entry* entry;
  int more_flags,list;

  event_list[0]=kevent_tcp;
  event_list[1]=kevent_udp;

  for(list=0; list<2; list++)
  {
    event=event_list[list];

    while(event)
    {
      printf("event=%s\n",event->head->name);
      printf("timeout=%d\n",event->head->timeout);
      printf("start_command=%s\n",event->head->start_command);
      if(event->head->stop_command)
      {
        printf("stop_command=%s\n",event->head->stop_command);
        printf("command_delay=%d\n",event->head->command_delay);
      }
      printf("sequence=");
      entry=event->cur_entry;
      while(entry)
      {
        printf("%s:%d",entry->type==TCP_ENTRY?"tcp":"udp",entry->port);
        if(entry->type==TCP_ENTRY)
        {
          more_flags=0;
          printf(":");
          if(entry->flag_ignore)
          {
	   printf("Ignore ");
	   entry=entry->next;
	   continue;
          }
          if(entry->flags.flags.fin)
	    more_flags=1,printf("FIN");
          if(entry->flags.flags.syn)
	    printf("%sSYN",more_flags?",":""),more_flags=1;
          if(entry->flags.flags.rst)
	    printf("%sRST",more_flags?",":""),more_flags=1;
          if(entry->flags.flags.psh)
	    printf("%sPSH",more_flags?",":""),more_flags=1;
          if(entry->flags.flags.ack)
	    printf("%sACK",more_flags?",":""),more_flags=1;
          if(entry->flags.flags.urg)
	    printf("%sURG",more_flags?",":""),more_flags=1;
          if(!more_flags)
	    printf("None");
        }
        printf(" ");
        entry=entry->next;
      }
      printf("\n\n");
      event=event->next;
    }
  }
}

