/*
 * Copyright (C) 2008 Michael Finsterbusch (fibu at users.sourceforge.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __EVENT_H
#define __EVENT_H

#include "capture.h"
#include "tportknockd.h"
#include "capture.h"

#define TCP_ENTRY 1
#define UDP_ENTRY 2


struct knock_list_entry {
	struct knock_list_entry* next;
	unsigned short port;
	unsigned char type;
	union tcp_flags flags;
	unsigned char flag_ignore; // if udp this flag must be set
};

struct knock_list_head {
	struct knock_list_entry* first;
	int timeout;
	char* name;
	char* start_command;
	char* stop_command;
	int command_delay;
};

struct knock_event {
	struct knock_event* next;
        struct knock_event* before;
	struct knock_list_head* head;
	struct knock_list_entry* cur_entry;
	struct in_addr ip;
	int timeout;
};

int add_event(struct knock_list_head* event);

void handle_event(unsigned char type,struct in_addr ip_addr,unsigned short port,union tcp_flags flags,unsigned int sec);

void print_events(void);

#endif // __EVENT_H

