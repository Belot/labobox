/*
 * Copyright (C) 2008 Michael Finsterbusch (fibu at users.sourceforge.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __CAPTURE_H
#define __CAPTURE_H

#include <linux/types.h>
#include <linux/net.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define IP_PROTOCOL_TCP 0x06
#define IP_PROTOCOL_UDP 0x11


struct ip_hdr {
#ifdef BIG_ENDIAN
        unsigned char   ip_version:4,   // version 
                        ip_hl:4;        // header length (incl any options) 
#else 
        unsigned char   ip_hl:4,
                        ip_version:4;
#endif
        unsigned char   ip_tos;         // type of service 
        unsigned short  ip_len;         // total length (incl header) 
        unsigned short  ip_id;          // identification 
        unsigned short  ip_offset;      // fragment offset and flags 
        unsigned char   ip_ttl;         // time to live 
        unsigned char   ip_protocol;    // protocol 
        unsigned short  ip_sum;         // checksum 
	struct in_addr  ip_src;
	struct in_addr  ip_dst;
        //unsigned int    ip_src;         // source address 
        //unsigned int    ip_dst;         // destination address 
};

union tcp_flags {
	unsigned char raw;
	struct s_flags{
#ifdef BIG_ENDIAN
        unsigned char   resvd:2, // reserved
			urg:1,   // urgent pointer flag
			ack:1,   // ack flag
			psh:1,   // push flag
			rst:1,   // reset flag
			syn:1,   // syn flag
			fin:1;   // finish flag
#else
	unsigned char   fin:1,   // finish flag
			syn:1,   // syn flag
			rst:1,   // reset flag
			psh:1,   // push flag
			ack:1,   // ack flag
			urg:1,   // urgent pointer flag
			resvd:2; // reserved		
#endif
	}flags;
};

struct tcp_hdr {
	unsigned short  tcp_src;        // source port
	unsigned short  tcp_dst;        // destination port
	unsigned int    tcp_seq;        // sequence number
        unsigned int    tcp_ack;	// ack number
#ifdef BIG_ENDIAN
        unsigned char   tcp_offset:4,   // data offset
	                tcp_resvd:4;    // reserved
#else
	unsigned char   tcp_resvd:4,    // reserved
			tcp_offset:4;   // data offset
#endif
	union tcp_flags tcp_flag;
	unsigned short  tcp_window;     // window size
	unsigned short  tcp_sum;	// checksum
	unsigned short  tcp_urgp;	// urgent pointer

// Options and Date are not needed
};

struct udp_hdr {
	unsigned short  udp_src;	// source port
	unsigned short  udp_dst;	// destination port
	unsigned short  udp_len;	// length
	unsigned short  udp_sum;	// checksum
};

int init_capture(char* device);
void deinit_capture(int fd);
void capture(int fd);
void ip_refresh(long sec);

#endif //__CAPTURE_H

