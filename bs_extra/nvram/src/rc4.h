#ifndef __RC4__
#define __RC4__

int rc4(unsigned char* key,int keylen,unsigned char* plaintext,int len,unsigned char* ciphertext);

#endif //__RC4__
