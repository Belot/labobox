/*
 * Copyright (C) 2008 Patrick Schmidt (coolman1982@users.sourceforge.net)
 * 		      Michael Finsterbusch (fibu@users.sourceforge.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>      /* open */
#include <unistd.h>     /* exit */
#include <sys/ioctl.h>  /* ioctl */
#include <memory.h>


#include "board_api.h"
#include "nvram.h"
#include "rc4.h"
#include "shared_mem.h"

void print_usage(char *prog_name)
{
 printf("nvram tool by Patrick Schmidt <coolman1982@users.sourceforge.net>\n"
	 "              Michael Finsterbusch <fibu@users.sourceforge.net>\n\n"
	 "Usage: %s <command> <command-options>\n\n"
	 "Commands:\n"
	 " show                        --prints table_size and all saved values\n"
	 " showkeys                    --prints table_size and all available keys\n"
	 " dump                        --prints complete NVRAM\n"
	 " dumphex                     --prints complete hexdump of NVRAM\n"
	 " reset                       --resets NVRAM-table\n"
	 " get <key>                   --prints saved value for given <key>-name\n"
	 " getfile <key>=<target_file> --prints saved value for given\n" 
	 "                               <key>-name to <target_file>\n"
	 " set <key>=<value>           --saves new key-value-pair or sets <value>\n"
	 "                               if <key> exists\n\n"
	 " setfile <key>=<source_file> --sets value of given <key> to the content\n"
	 "                               of given <source_file>\n\n"
	 " del <key>                   --deletes a <key>=<value>-pair for a given\n"
	 "                               <key>\n\n"
	 " saveconfig <mode>=<smode> <key>=<enc-key> <file>=<target_file>\n"
	 "                             --save the router-configuration to\n"
         "                               <target_file>\n"
         "                               Save-Mode(<smode>) means:\n"
         "                                    mode=2 : save the NVRAM table\n"
         "                                    mode=3 : save the whole NVRAM (default)\n"
         "                               if no mode specified whole NVRAM is saved\n"
         "                               <enc-key> is an encryption passphrase to\n"
	 "                               protect your data.\n"
	 "                               No key, no protection (key=)\n\n"
	 " loadconfig <key>=<enc-key> <file>=<source_file>\n"
	 "                             --load the router-configuration from file\n"
	 "                               <source_file> to NVRAM. <enc-key> ist the\n"
	 "                               key to decrypt the <source_file>.\n\n"
         " compression <on>/<1>        --enable compression\n\n"
         " compression <off>/<0>       --disable compression\n\n"
         " compression <status>/<info> --prints compression status\n\n"
         " commit                      --commits changes to NVRAM\n"
#ifdef DEBUG
         " unlock                      --force unlock of shared_mem\n"
         " destroy                     --force free of shared_mem_segment to force shm_reinit\n"
#endif
         "\n",
	 prog_name);
 
  return;
}

int print_reset()
{
  printf("Resetting NVRAM-table...");
  int ret=0;
  ret=nvram_reset();
  if(ret){
    printf("Failed\n");
    return ret;
  }
  else printf("OK\n");
  return 0;
}

int print_info(char keys_only)
{
  char buf[SHARED_MEM_SIZE+512];
  int buf_size=SHARED_MEM_SIZE+512;
  printf("Printing NVRAM-Info...");
  int ret=0;
  ret=nvram_get_info(buf,&buf_size,keys_only);
  if(ret)
  {
    printf("Failed\n",ret);
    return ret;
  }
  printf("OK\n");
  int i;
  for(i=0;i<buf_size;i++)
    printf("%c",buf[i]);
  printf("\n");
  return 0;
}

int print_set(char *argument,char from_file)
{
  char key[MAX_KEY_LENGTH+1];
  char value[MAX_VALUE_SIZE+1];
  char *pos;
  pos=strchr(argument,'=');
  if(!pos)
  {
    printf("Error: wrong argument\n");
    print_usage("nvram");
    return;
  }
  else
  {
    if(pos-argument>MAX_KEY_LENGTH)
    {
      printf("Failed...MAX_KEY_LENGTH=%d\n",(int) MAX_KEY_LENGTH);
      return ERR_VALUE_TO_LONG;
    }
    snprintf(key,pos-argument+1,"%s\0",argument);
#ifdef DEBUG
    printf("key=%s\n",key);
#endif
    pos++;
    int ret=0;
    //Value is given from command line
    if(!from_file)
    {
      strcpy(value,pos);
      //printf("value=%s\n",value);
      ret=nvram_set_value(key,value,strlen(value));
      if(ret<0){
        printf("Failed\n");
        exit(-1);
      }
      else printf("OK\n");
      return 0;
    }
    //Value should be read from file
    else
    {
      FILE *f=fopen(pos,"r");
      if(!f){
        printf("failed...cant open file: %s\n",pos);
        return ERR_UNDEFINED;
      }
      ret=fread(value,1,MAX_VALUE_SIZE,f);
#ifdef DEBUG
      printf("read %d bytes from file\n",ret);
#endif
      fclose(f);
      //everything all right
      if(ret>0 && ret<MAX_VALUE_SIZE)
      {
        value[ret]='\0';
        ret=nvram_set_value(key,value,ret);
        if(ret<0){
          printf("Failed\n");
          return ret;
        }
        else printf("OK\n");
        return 0;
      }
      //buffer too small
      else if(ret>0 && ret>=MAX_VALUE_SIZE)
      {
        printf("Failed...file contains more byte than max allowed value size (%d bytes)\n",(int) MAX_VALUE_SIZE);
        return ERR_VALUE_TO_LONG;
      }
      //read failed
      else
      {
        printf("Failed...while reading from file\n");
        return ERR_UNDEFINED;
      }
    }
  }
}

int print_get(char *arg,char to_file)
{
  char buf[MAX_VALUE_SIZE+1];
  unsigned int value_size=MAX_VALUE_SIZE;
  int ret=0;
  if(to_file)
  {
    char key[MAX_KEY_LENGTH+1];
    char *pos;
    pos=strchr(arg,'=');
    if(!pos)
    {
      printf("Error: wrong argument...missing '=' in statement\n");
      print_usage("nvram");
      return;
    }
    if(pos-arg>MAX_KEY_LENGTH)
    {
      printf("Failed...MAX_KEY_LENGTH=%d\n",(int) MAX_KEY_LENGTH);
      return ERR_VALUE_TO_LONG;
    }
    snprintf(key,pos-arg+1,"%s\0",arg);
#ifdef DEBUG
    printf("key=%s\n",key);
#endif
    ret=nvram_get_value(key,buf,&value_size);
    if(ret<0)
    {
      printf("Failed\n");
      return ret;
    }
    pos++;
#ifdef DEBUG
    printf("file_name=%s\n",pos);
#endif
    FILE *f=fopen(pos,"w+");
    if(!f){
      printf("failed...cant open file: %s\n",pos);
      return ERR_UNDEFINED;
    }
    ret=fwrite(buf,1,value_size,f);
#ifdef DEBUG
    printf("wrote %d bytes from file\n",ret);
#endif
    fclose(f);
    if(ret<0)
    {
      printf("failed...cant write to file: %s\n",pos);
      return ret;
    }
   if(ret<value_size)
   {
     printf("Error: not enough space\n");
     return 1;
   } 
  }
  else
  {
    ret=nvram_get_value(arg,buf,&value_size);
    if(ret<0)
    {
      printf("Failed\n");
      return ret;
    }
    //if binary value, not null terminated at right position
    if(value_size!=strlen(buf))
    {
      int i;
      for(i=0;i<value_size;i++)
        printf("%c",buf[i]);
      printf("\n");
    }
    else printf("%s\n",buf);
  }
  return 0;
}

int print_del(char *arg)
{
  int ret=nvram_del_value(arg);
  if(ret)
  {
    printf("Failed\n");
    return ret;
  }
  else printf("OK\n");
  return 0;
}

int print_dump(char hexdump)
{
  int i,h=0,l=0,j=0;
  char *buf=malloc(PSI_SIZE+1);
  unsigned char string[16];
  int ret=nvram_dump_direct(buf,PSI_SIZE);
  if(ret){
    printf("Failed\n");
    return ret;
  }
  if(!hexdump)
  {
    printf("Printing complete NVRAM:\n");

    for(i=0; i<PSI_SIZE+1; i++)
    {
      if(&buf[i]==0)
        continue;
      i+=printf("%s\n",&buf[i]);
    }
    printf("\n");  
  }
  else
  {
    h=0;
    printf("Printing complete hexdump of NVRAM:\n");
    for(i=0; i<PSI_SIZE+1; i++)
    {
      if(!h)
      {
        h=1, printf("%08X:  ",i);
        l=0;
      }
      printf("%02X ",(unsigned char)buf[i]);
      string[l++]=buf[i];
      if(((i+1)%8)==0)
        printf(" ");
      if(((i+1)%16)==0)
      {
        printf("   ");
        for(j=0; j<l; j++)
          printf("%c",(string[j]>31 && string[j]<128)?string[j]:'.');
        h=0;
        printf("\n");
      }
    }
    if(h!=0)
    {
      printf("   ");
      for(j=0; j<l; j++)
        printf("%c",(string[j]>32 || string[j]<128)?string[j]:'.');
    }
  }
  return 0;
}

int print_commit()
{
  int err;
  err=nvram_commit();
  if(err)
    printf("Failed\n");
  else printf("OK\n");
  return 0;
}

int print_setcompression(char *cmd)
{
  int err,len;
  char *buf;
  err=0;
  //set compression on
  if(strcmp(cmd,"on")==0 || strcmp(cmd,"1")==0)
    shm_set_compression(1);
  //set compression off
  else if(strcmp(cmd,"off")==0 || strcmp(cmd,"0")==0)
  {
    //first check if table is small enough to turn compression off
    if(nvram_get_table_size()>MAX_TABLE_SIZE)
    {
      printf("Failed\ntable_size=%d, too big to turn compression off\n\n",nvram_get_table_size());
      return -1;
    }
    shm_set_compression(0);
  }
  //compression info
  else if(strcmp(cmd,"info")==0 || strcmp(cmd,"status")==0)
  {
    printf("Printing Compression-Info...");
    if( shm_is_compression_on()==1 )
    {
      len=SHARED_MEM_SIZE;
      buf=malloc(len);
      if(!buf)
      {
        err=1;
        goto error;
      }
      err=shm_compress_table(buf,&len);
      if(err)
        goto error;
      printf("OK\n");
      printf("table_size=%d\n",nvram_get_table_size());
      printf("compressed_table_size=%d\n",len);
      printf("compression_ratio= 1 : %.2f\n\n",(double)(1.0*nvram_get_table_size()/len));
      return 0;
    }
    else
    {
      printf("Failed\n\ncompression is off\n\n");
      return -1;
    }
    return;
  }
  else err=1;
error:
  if(err)
    printf("Failed\n");
  else printf("OK\n");
  return err;
}


#define GETOPT_OK         0
#define GETOPT_NOVALUE    1
#define GETOPT_NOEQUAL    2
int getopts(int* argc,char* argv[],char** parameter,char** value)
{
 char* str;
 char* pos;

 if(*argc<1)
  return -1;
 if(!argv)
  return -2;

 (*argc)--;
 str=argv[*argc];

 *parameter=NULL;
 *value=NULL;

 pos=strchr(str,'=');
 if(!pos)
 {
  *parameter=str;
  return GETOPT_NOEQUAL;
 }
 *pos='\0';
 *parameter=str;
 *value=pos+1;
 if(strlen(*value)>0)
  return GETOPT_OK;
 return GETOPT_NOVALUE;
}

unsigned int elf_hash(char* buffer,int buflen)
{
 unsigned int h=0;
 unsigned int g;
 char* end=buffer+buflen;

 while(buffer<end)
 {
  h=(h<<4)+(int)*(buffer++);
  g=h & 0xF0000000;
  if(g!=0)
   h=h^g>>24;
  h=h & ~g ;
 }

 return h;
}

#define SAVE_MODE_PSI   1
#define SAVE_MODE_NVRAM 2
#define SAVE_MODE_ALL   3
/*
  save_config - Configuration file

  C - Configuration (1 Byte)
  M - Mode (1 Byte)
  H - Hash over Configuration+Mode (1 Byte)
  all data is possibly encrypted

  config file structure = CCCCCCCC...CCCCCCCMHHHH
*/
int save_config(int argc,char* argv[])
{
 char* para,*val;
 int ret;
 unsigned char smode=SAVE_MODE_ALL;
 char* target_file=NULL;
 char* enc_key=NULL;
 char buffer[PSI_SIZE+5];
 int buflen;
 unsigned int hash;
 char encbuffer[PSI_SIZE+5];
 FILE* file;
 char* buf;
 
// Parameters
 while((ret=getopts(&argc,argv,&para,&val))>=GETOPT_OK)
 {
  if(ret==GETOPT_NOEQUAL)
  {
    printf("Error: missing '=' in '%s' statement\n",para);
    return 1;
  }
  if(strcmp(para,"mode")==0)
  {
    int h=atoi(val);
    if(h<0 || h>255)
    {
      printf("Error: unknown mode '%d' specified\n",h);
      return 1;
    }
    smode=(unsigned char)h;
  }
  else if(strcmp(para,"file")==0)
  {
    if(ret==GETOPT_NOVALUE)
    {
      printf("Error: no file specified\n");
      return 1;
    }
    target_file=strdup(val);
  }
  else if(strcmp(para,"key")==0)
  {
    if(ret==GETOPT_NOVALUE)
      enc_key=NULL;
    else
      enc_key=strdup(val);
  }
 }
#ifdef DEBUG
 printf("Mode=%d\nFile=%s\nKey=%s\n",smode,target_file,enc_key);
#endif
 
 // Mode
 if(smode!=SAVE_MODE_PSI && smode!=SAVE_MODE_NVRAM && smode!=SAVE_MODE_ALL)
 {
   printf("Error: unknown mode '%d' specified\n",smode);
   return 1;
 }
 else if(smode==SAVE_MODE_PSI)
 {
   printf("Error: SAVE_MODE_PSI specified, but not longer supported\n");
   return 1;
 }
 switch(smode)
 {
   case SAVE_MODE_NVRAM:
     if(shm_is_compression_on()==1)
     {
       buflen=PSI_SIZE+5;
       ret=shm_compress_table(buffer,&buflen);
       if(ret)
       {
         printf("Error: compression failed\n");
         return 1;
       }
       int comp_len=buflen|(1<<COMPRESSION_BIT);
       //printf("%02x %02x",buflen,comp_len);
       //set compression bit in buffer
       memcpy(buffer+buflen,(void *)&comp_len,4);
       buflen+=4;
     }
     else
     {
      buflen=nvram_get_table_size();
      if(buflen<0)
      {
        printf("Error: NVRAM is corrupt\n");
        return 1;
      }
      buflen+=4;
      if(nvram_dump_table(buffer,buflen)!=0)
      {
        printf("Error: reading NVRAM\n");
        return 1;
      }
     }
     break;
   case SAVE_MODE_ALL:
     nvram_dump_direct(buffer,PSI_SIZE);
     buflen=PSI_SIZE;
     break;
   default:
     printf("Error: unknown error\n");
     return 1;
     break;
 }
 buffer[buflen]=smode;
 buflen+=1;
 
 // Hash
 hash=elf_hash(buffer,buflen);
 memcpy(buffer+buflen,&hash,4);
 buflen+=4;

 // encryption
 buf=buffer;
 if(enc_key)
 {
  rc4(enc_key,strlen(enc_key),buffer,buflen,encbuffer);
  buf=encbuffer;
 }

// write to file
 file=fopen(target_file,"w+");
 if(!file)
 {
  printf("Error: can not open file: %s\n",target_file);
  return 1;
 }
 ret=fwrite(buf,1,buflen,file);

#ifdef DEBUG
    printf("wrote %d bytes to file\n",ret);
#endif
 fclose(file);
 if(ret<0)
 {
  printf("Error: failed to write to %s\n",target_file);
  return ret;
 }
 if(ret<buflen)
 {
  printf("Error: not enough space\n");
  return 1;
 } 

 printf("OK\n");

 return 0;
}

int load_config(int argc,char* argv[])
{
 char* para,*val;
 int ret;
 unsigned char smode=0;
 char* source_file=NULL;
 char* enc_key=NULL;
 char buffer[PSI_SIZE+5];
 int buflen;
 unsigned int hash,file_hash;
 char encbuffer[PSI_SIZE+5];
 FILE* file;
 char* buf;
 
// Parameters
 while((ret=getopts(&argc,argv,&para,&val))>=GETOPT_OK)
 {
  if(ret==GETOPT_NOEQUAL)
   {
    printf("Error: missing '=' in '%s' statement\n",para);
    return 1;
   }

  if(strcmp(para,"file")==0)
  {
   if(ret==GETOPT_NOVALUE)
   {
    printf("Error: no file specified\n");
    return 1;
   }
   source_file=strdup(val);
  }
  else if(strcmp(para,"key")==0)
  {
   if(ret==GETOPT_NOVALUE)
    enc_key=NULL;
   else
    enc_key=strdup(val);
  }
 }
#ifdef DEBUG
 printf("File=%s\nKey=%s\n",source_file,enc_key);
#endif
 
 file=fopen(source_file,"r");
 if(!file)
 {
   printf("Error: can not open file: %s\n",source_file);
   return 1;
 }
 ret=fread(buffer,1,PSI_SIZE+5,file);
#ifdef DEBUG
      printf("read %d bytes from file\n",ret);
#endif
 fclose(file);
 //everything right?
 if(ret<0 || ret>PSI_SIZE+5)
 {
  printf("Error: failed to read file: %s\n",source_file);
  return 1;
 }
 buflen=ret;

// decryption
 buf=buffer;
 if(enc_key)
 {
  rc4(enc_key,strlen(enc_key),buffer,buflen,encbuffer);
  buf=encbuffer;
 }

// Hash
 buflen-=4;
 hash=elf_hash(buf,buflen);
 
 memcpy(&file_hash,buf+buflen,4);
 if(hash!=file_hash)
 {
  printf("Error: file %s is corrupt\n",source_file);
  return 1;
 }

  // Mode
 buflen-=1;
 memcpy(&smode,buf+buflen,1);
 if(smode!=SAVE_MODE_PSI && smode!=SAVE_MODE_NVRAM && smode!=SAVE_MODE_ALL)
 {
   printf("Error: unknown mode '%d' specified\n",smode);
   return 1;
 }
 else if(smode==SAVE_MODE_PSI){
   printf("Error: SAVE_MODE_PSI specified, but not longer supported\n");
   return 1;
 }

 ret=0;
 switch(smode)
 {
   case SAVE_MODE_NVRAM:
#ifdef DEBUG
     printf("found mode=SAVE_MODE_NVRAM\n");
#endif
     sysPersistentSet(buf,buflen,PSI_SIZE-buflen);
     ret=shm_reload();
     break;
   case SAVE_MODE_ALL:
#ifdef DEBUG
     printf("found mode=SAVE_MODE_ALL\n");
#endif
     sysPersistentSet(buf,buflen,0);
     ret=shm_reload();
     break;
   default:
     printf("Error: unknown error\n");
     return 1;
     break;
 }
 if(ret)
   printf("Failed\n");
 else printf("OK\n");
 return ret;
}

int main(int argc,char *argv[])
{
  int err=0;
  if(argc<2){
    print_usage(argv[0]);
    return -1;
  }
  if( shm_init() )
  {
    printf("error: shm_init()\n");
    return -1;
  }
  if(strcmp(argv[1],"reset")==0) err=print_reset();
  else if(strcmp(argv[1],"show")==0) err=print_info(0);
  else if(strcmp(argv[1],"showkeys")==0) err=print_info(1);
  else if(strcmp(argv[1],"set")==0 && argc>2) err=print_set(argv[2],0);
  else if(strcmp(argv[1],"get")==0 && argc>2) err=print_get(argv[2],0);
  else if(strcmp(argv[1],"del")==0 && argc>2) err=print_del(argv[2]);
  else if(strcmp(argv[1],"setfile")==0 && argc>2) err=print_set(argv[2],1);
  else if(strcmp(argv[1],"setfilecommit")==0 && argc>2)
  {
    err=print_set(argv[2],1);
    err+=print_commit();
  }
  else if(strcmp(argv[1],"getfile")==0 && argc>2) err=print_get(argv[2],1);
  else if(strcmp(argv[1],"dump")==0) err=print_dump(0);
  else if(strcmp(argv[1],"dumphex")==0) err=print_dump(1);
  else if(strcmp(argv[1],"saveconfig")==0 && argc>2) err=save_config(argc-2,argv+2);
  else if(strcmp(argv[1],"loadconfig")==0 && argc>2) err=load_config(argc-2,argv+2);
  else if(strcmp(argv[1],"commit")==0) err=print_commit();
  else if(strcmp(argv[1],"reload")==0) err=shm_reload();
#ifdef DEBUG
  else if(strcmp(argv[1],"unlock")==0) shm_force_unlock();
  else if(strcmp(argv[1],"destroy")==0) shm_destroy();
#endif
  else if(strcmp(argv[1],"compression")==0 && argc>2) err=print_setcompression(argv[2]);
  else if(strcmp(argv[1],"reboot")==0) sysMipsSoftReset();
  else print_usage(argv[0]);

  shm_deinit();
  return err;
}

