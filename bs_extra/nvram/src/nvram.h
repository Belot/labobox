/*
 * Copyright (C) 2008 Patrick Schmidt (Patrick.Schmidt@hftl.de)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#if !defined(_NVRAM_H_)
#define _NVRAM_H_

#ifdef BOARD_96332CG
#define PSI_SIZE 24576
#define MAX_TABLE_SIZE 24572
#define MAX_KEY_LENGTH 255
#define MAX_VALUE_SIZE 24572
#else

#define PSI_SIZE 32767

#define MAX_TABLE_SIZE 16383
//#define MAX_TABLE_SIZE 32763
#define MAX_KEY_LENGTH 255
#define MAX_VALUE_SIZE 32767
//#define MAX_VALUE_SIZE 32763

#endif

#define ERR_INVALID_TABLE_SIZE -1
#define ERR_MALLOC -2
#define ERR_VALUE_NOT_FOUND -3
#define ERR_UNDEFINED_PARAM -4
#define ERR_INCONSISTENT_VALUE -5
#define ERR_BUF_SIZE_TOO_SMALL -6
#define ERR_VALUE_CONTAINS_SEPERATOR -7
#define ERR_VALUE_TO_LONG -8
#define ERR_INCONSISTENT_TABLE -9

#define ERR_UNDEFINED -127


int nvram_get_table_size();
int nvram_set_table_size(int table_size);
int nvram_reset();
int nvram_dump(char *buf,int buf_size);
int nvram_dump_direct(char *buf,int buf_size);
int nvram_dump_table(char *buf,int buf_size);
int nvram_get_value(char *key_name,char *buf,unsigned int *buf_size);
int nvram_set_value(char *key_name,char *value,int value_length);
int nvram_del_value(char *key_name);
int nvram_get_info(char *buf,int *buf_size, char keys_only);
int nvram_commit();

#endif
