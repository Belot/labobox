#include<stdio.h>

#include "rc4.h"

#define swap(x,y) ({ typeof(x) h = (x); (x) = (y); (y) = h; })

int rc4(unsigned char* key,int keylen,unsigned char* plaintext,int len,unsigned char* ciphertext)
{
 int i,j;
 unsigned char S[256];
 int l;
 unsigned char c;

 // check input
 if(!key) return 1;
 if(!plaintext) return 2;
 if(!ciphertext) return 3;

 // init S-Box
 for(i=0; i<256; i++) S[i]=i;
 j=0;
 for(i=0; i<256; i++)
 {
  j=(j + S[i] + key[i%keylen]) % 256;
  swap(S[i],S[j]);
 }

 // en- or decrypt
 i=j=0;
 for(l=0; l<len; l++)
 {
  i = (i + 1) % 256;
  j = (j + S[i]) %  256;
  swap(S[i],S[j]);
  c=S[(S[i] + S[j]) % 256];
  ciphertext[l]=plaintext[l]^c;
 }

 return 0;
}

