/*
 * Copyright (C) 2008 Patrick Schmidt (Patrick.Schmidt@hftl.de)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>      /* open */
#include <unistd.h>     /* exit */
#include <sys/ioctl.h>  /* ioctl */
#include <netinet/in.h>
#include <memory.h>


#include "board_api.h"
#include "nvram.h"
#include "shared_mem.h"

//internally used for check a new calculated tablesize
//which depends on compression status
static int nvram_check_tablesize(int new_tablesize)
{
#ifdef DEBUG
  printf("nvram_check_tablesize()\n");
#endif
  if( shm_is_compression_on()==1 )
  {
     if( new_tablesize<=SHARED_MEM_SIZE-4 )
       return 0;
  }
  else if( new_tablesize<=MAX_TABLE_SIZE )
    return 0;

  //size if incorrect
#ifdef DEBUG
  printf("nvram_check_tablesize() return SIZE_INCORRECT\n");
#endif
  return -1;
}

//returns size of nvram_table, size is a 4 byte value at the end of psi_mem
int nvram_get_table_size()
{
#ifdef DEBUG
  printf("nvram_get_table_size()\n");
#endif
  int err;
  //die letzten vier Byte geben die Gr��e der Table an
  int table_size=-1;
  //sysPersistentGet((char*)&table_size,4,PSI_SIZE-4);
  err=shm_rw((char*)&table_size,4,SHARED_MEM_SIZE-4,SHM_READ);
  if(err)
    return err;
  table_size&=~(1<<COMPRESSION_BIT);
#ifdef DEBUG
  printf("nvram_get_table_size() read size=%d\n",table_size);
#endif
  if(table_size<0 || table_size>SHARED_MEM_SIZE) return ERR_INVALID_TABLE_SIZE;
  else return table_size;
}

//sets the size of nvram_table, size is a 4 byte value at the end of psi_mem
int nvram_set_table_size(int table_size)
{
#ifdef DEBUG
  printf("nvram_set_table_size=%d\n",table_size);
#endif
  if(table_size<0 || table_size>SHARED_MEM_SIZE) return ERR_INVALID_TABLE_SIZE;
  if(shm_is_compression_on()==1)
    table_size|=(1<<COMPRESSION_BIT);
  //sysPersistentSet((char*)&table_size,4,PSI_SIZE-4);
  return shm_rw((char*)&table_size,4,SHARED_MEM_SIZE-4,SHM_WRITE);
}

//Resets the whole nvram_table including the table_size with 0
int nvram_reset()
{
  char *buf=NULL;
  int err;
  buf=malloc(SHARED_MEM_SIZE);
  if(!buf) return ERR_MALLOC;
  memset(buf,255,SHARED_MEM_SIZE);
  //sysPersistentSet(buf,MAX_TABLE_SIZE+4,PSI_SIZE-(MAX_TABLE_SIZE+4));
  err=shm_rw(buf,SHARED_MEM_SIZE,0,SHM_WRITE);
  free(buf);
  nvram_set_table_size(0);
  return err;
}

//prints the whole psi_mem into buf if buf_size is big enough
static int __nvram_dump(char *buf,int buf_size,int direct_from_nvram)
{
  int err,table_size, offset;
  if(!buf || buf_size<0) return ERR_UNDEFINED_PARAM;
  if(direct_from_nvram)
  {
#ifdef DEBUG
    printf("nvram_dump_direct...\n");
#endif
    if(buf_size<PSI_SIZE) return ERR_UNDEFINED_PARAM;
    sysPersistentGet(buf,PSI_SIZE,0);
    return 0;
  }
#ifdef DEBUG
  printf("nvram_dump...\n");
#endif
  if(buf_size<(PSI_SIZE-MAX_TABLE_SIZE)+SHARED_MEM_SIZE) return ERR_UNDEFINED_PARAM;
  sysPersistentGet(buf,PSI_SIZE-MAX_TABLE_SIZE,0);
  //table_size=nvram_get_table_size();
  err=shm_rw(buf+(PSI_SIZE-MAX_TABLE_SIZE),SHARED_MEM_SIZE,0,SHM_READ);
  return err;
}

int nvram_dump(char *buf,int buf_size)
{
  return __nvram_dump(buf,buf_size,0);
}
int nvram_dump_direct(char *buf,int buf_size)
{
  return __nvram_dump(buf,buf_size,1);
}

//prints the whole nvram_table into buf if buf_size is big enough
int nvram_dump_table(char *buf,int buf_size)
{
#ifdef DEBUG
  printf("nvram_dump_table...\n");
#endif
  if(!buf || buf_size<0) return ERR_UNDEFINED_PARAM;
  int table_size=nvram_get_table_size();
  if(table_size<0) return table_size;
  //sysPersistentGet(buf,buf_size,PSI_SIZE-4-table_size);
  if(buf_size<table_size) return 0;
  return shm_rw(buf,buf_size,SHARED_MEM_SIZE-(table_size+4),SHM_READ);
}

//copies value to a given key_name to buf, return=table_offset which points to key_length
int nvram_get_value(char *key_name,char *buf,unsigned int *buf_size)
{
#ifdef DEBUG
  printf("nvram_get_value...\n");
#endif
  if(!buf || buf_size<0 || strlen(key_name)<1 || strlen(key_name)>MAX_KEY_LENGTH) return ERR_UNDEFINED_PARAM;
  int table_size=nvram_get_table_size();
  if(table_size<0) return table_size;
  else if(table_size==0) return ERR_VALUE_NOT_FOUND;

#ifdef DEBUG
  printf("nvram_get_value: table_size=%d\n",table_size);
#endif
  char *table_buf=NULL;
  table_buf=malloc(table_size);
  if(!table_buf) return ERR_MALLOC;

  if(nvram_dump_table(table_buf,table_size)) return -1;
  char *key_buf;
  key_buf=malloc(MAX_KEY_LENGTH+1);
  if(!key_buf) return ERR_MALLOC;
  
  char *pos=table_buf;
  unsigned char key_length=0;
  unsigned short int value_length=0;
  while(pos<table_buf+table_size)
  {
#ifdef DEBUG
    printf("key_length=%d\n",*pos);
#endif
    key_length=(unsigned char) *pos;
    if(key_length<1) return ERR_INCONSISTENT_TABLE;
    snprintf(key_buf,key_length+1,"%s\0",pos+1);
#ifdef DEBUG
    printf("key_buf=%s\n",key_buf);
#endif
    //value_length=(unsigned short int) *(pos+1+key_length);
    memcpy(&value_length,pos+1+key_length,sizeof(value_length));
    if(value_length>MAX_VALUE_SIZE) return ERR_INCONSISTENT_TABLE;
#ifdef DEBUG
    printf("value_length: %d\n",value_length);
#endif
    //right key found
    if(strcmp(key_buf,key_name)==0)
    {
      if(*buf_size<value_length+1) return ERR_BUF_SIZE_TOO_SMALL;
      memcpy(buf,pos+1+key_length+2,value_length);
      buf[value_length]='\0';
      *buf_size=value_length;
#ifdef DEBUG
      printf("value found: %s\n",buf);
#endif
      break;
    }
    pos+=1+key_length+2+value_length;
  }
  int ret=ERR_VALUE_NOT_FOUND;
  if(pos<table_buf+table_size) ret=pos-table_buf;
  free(table_buf);
  free(key_buf);
  return ret;
}

int nvram_set_value(char *key_name,char *value,int value_length)
{
#ifdef DEBUG
  printf("nvram_set_value...\n");
#endif
  if(!value) return ERR_UNDEFINED_PARAM;
  if(value_length>MAX_VALUE_SIZE) return ERR_VALUE_TO_LONG;
  
  int table_size=nvram_get_table_size();
  if(table_size<0) return table_size;

  int err;
  char temp[MAX_VALUE_SIZE+1];
  unsigned int old_value_size=MAX_VALUE_SIZE;
  int offset=nvram_get_value(key_name,temp,&old_value_size);
  //Value already saved
  if(offset>=0)
  {
#ifdef DEBUG
    printf("nvram_set_value: value found at offset=%d\n",offset);
#endif
    char *old_table,*backup_table;
    old_table=malloc(table_size);
    backup_table=malloc(table_size);
    if(!old_table || !backup_table) return ERR_MALLOC;
    if(nvram_dump_table(old_table,table_size)<0) return ERR_UNDEFINED;
    if(nvram_dump_table(backup_table,table_size)<0) return ERR_UNDEFINED;
    
    char *new_table;
    int new_table_size;
    //old value bigger than new value
    if(old_value_size>value_length)
    {
      new_table_size=table_size-(old_value_size-value_length);
#ifdef DEBUG
      printf("nvram_set_value: shrinking value from length=%d to length=%d with new_table_size=%d\n",old_value_size,value_length,new_table_size);
#endif
    }
    //old value smaller than new value
    else if(old_value_size<value_length)
    {
      new_table_size=table_size+(value_length-old_value_size);
#ifdef DEBUG
      printf("nvram_set_value: enlarge value from length=%d to length=%d with new_table_size=%d\n",old_value_size,value_length,new_table_size);
#endif
    }
    //size is equal
    else
    {
      //sprintf(old_table+offset,"%s=%s;",key_name,buf);
      memcpy(old_table+offset+1+strlen(key_name)+2,value,value_length);
      //sysPersistentSet(old_table,table_size,PSI_SIZE-4-table_size);
      err=shm_rw(old_table,table_size,SHARED_MEM_SIZE-(table_size+4),SHM_WRITE);
      //if compression_size would be to big with new data
      //write old_table back and return error
      if( shm_check_compression_size() )
      {
        err=shm_rw(backup_table,table_size,SHARED_MEM_SIZE-(table_size+4),SHM_WRITE);
        err=-1;
      }
      free(old_table);
      free(backup_table);
      if(err) return err;
      return 0;
    }
    new_table=malloc(new_table_size);
    if(!new_table) return ERR_MALLOC;
    if(offset>0){
      memcpy(new_table,old_table,offset);
#ifdef DEBUG
      printf("offset-char=%c\n",*(old_table+offset));
#endif
    }
    //sprintf(new_table+offset,"%s=%s;\0",key_name,buf);
    //set key_length
    unsigned char key_length=(unsigned char) strlen(key_name);
    memcpy(new_table+offset,&key_length,1);
    //set key_name
    memcpy(new_table+offset+1,key_name,strlen(key_name));
    //set value_length
    unsigned short int my_value_length=(unsigned short int) value_length;
     //we have to write byte for byte because writing 2 bytes on one step does turn the byte order
    memcpy(new_table+offset+1+strlen(key_name),&my_value_length,sizeof(my_value_length));
    //set value
    memcpy(new_table+offset+1+strlen(key_name)+2,value,value_length);

#ifdef DEBUG
    printf("new_table=%s\n",new_table);
    printf("nvram_set_value: saving value:%s at byte-offset=%d\n",value,PSI_SIZE-4-(new_table_size+offset));
#endif
    int rest_length=table_size-((old_table+offset+1+strlen(key_name)+2+old_value_size)-old_table);
    if(rest_length>0)
    {
#ifdef DEBUG
      printf("copying old_table to offset=%d from offset=%d with length=%d\n",offset+1+strlen(key_name)+2+value_length,offset+1+strlen(key_name)+2+old_value_size,rest_length);
#endif
      memcpy(new_table+offset+1+strlen(key_name)+2+value_length,old_table+offset+1+strlen(key_name)+2+old_value_size,rest_length);
    }
    //printf("new_table_start_char=%c, new_table_offset=%d\n",*new_table,PSI_SIZE-4-new_table_size);
    //sysPersistentSet(new_table,new_table_size,PSI_SIZE-4-new_table_size);

    //check if new_table_size is correct
    if( nvram_check_tablesize(new_table_size) )
    {
#ifdef DEBUG
      printf("nvram_set_value() error new_table_size=%d is too big, cannot set value\n",new_table_size);
#endif
      return -1;
    }
    
    err=shm_rw(new_table,new_table_size,SHARED_MEM_SIZE-4-new_table_size,SHM_WRITE);
    nvram_set_table_size(new_table_size);
    if( shm_check_compression_size() )
    {
      err=shm_rw(backup_table,table_size,SHARED_MEM_SIZE-(table_size+4),SHM_WRITE);
      nvram_set_table_size(table_size);
      err=-1;
    }
    free(old_table);
    free(new_table);
    free(backup_table);
    if(err) return err;
    return 0;
  }
  //really new value
  else if(offset==ERR_VALUE_NOT_FOUND)
  {
#ifdef DEBUG
    printf("nvram_set_value: creating new value with key_len=%d, value_len=%d\n",strlen(key_name),value_length);
#endif
     char *target;
     int target_size=strlen(key_name)+value_length+3;
     target=malloc(target_size+1);
     if(!target) return ERR_MALLOC;
     //snprintf(target,target_size+1,"%s=%s;",key_name,buf);
     
     //set key_length
     unsigned char key_length=(unsigned char) strlen(key_name);
     memcpy(target,&key_length,1);
    //set key_name
     memcpy(target+1,key_name,strlen(key_name));
    //set value_length
     unsigned short int my_value_length=value_length;
#ifdef DEBUG
     printf("value_length=%hhx %hhx %hd\n",*(&my_value_length),*(&my_value_length+1),(unsigned short int) my_value_length);
#endif
     memcpy(target+1+strlen(key_name),&my_value_length,2);
     memcpy(&my_value_length,target+1+strlen(key_name),sizeof(my_value_length));
#ifdef DEBUG
     printf("value_length in buffer=%hhx %hhx %hd\n",*(&my_value_length),*(&my_value_length+1),(unsigned short int) my_value_length);
#endif
    //set value
     memcpy(target+1+strlen(key_name)+sizeof(my_value_length),value,value_length);
#ifdef DEBUG
     target[target_size]='\0';
     printf("nvram_set_value: saving target:%s at byte-offset=%d with length=%d\n",target,PSI_SIZE-4-table_size-target_size,target_size);
#endif
     //sysPersistentSet(target,target_size,PSI_SIZE-4-table_size-target_size);
 
     //check if new_table_size is correct
     if( nvram_check_tablesize(table_size+target_size) )
     {
#ifdef DEBUG
       printf("nvram_set_value() error new_table_size=%d is too big, cannot set value\n",table_size+target_size);
#endif
       return -1;
     }

     err=shm_rw(target,target_size,SHARED_MEM_SIZE-4-table_size-target_size,SHM_WRITE);
     nvram_set_table_size(table_size+target_size);
     if( shm_check_compression_size() )
     {
       //simply set table_size back to old value
       nvram_set_table_size(table_size);
       err=-1;
     }
     if(err) return err;
     free(target);
  }
  //error
  else return offset;
}

int nvram_del_value(char *key_name)
{
  int err;
  if(!key_name) return ERR_UNDEFINED_PARAM;
  int table_size=nvram_get_table_size();
  if(table_size<0) return ERR_INVALID_TABLE_SIZE;
  char temp[MAX_VALUE_SIZE+1];
  unsigned int value_size=MAX_VALUE_SIZE;
  int offset=nvram_get_value(key_name,temp,&value_size);
  if(offset>=0)
  {
#ifdef DEBUG
    printf("nvram_del_value: value found at offset=%d\n",offset);
#endif
    char *old_table=malloc(table_size);
    if(!old_table) return ERR_MALLOC;
    if(nvram_dump_table(old_table,table_size)<0) return ERR_UNDEFINED;
    int new_table_size=table_size-strlen(key_name)-value_size-3;
    char *new_table=malloc(new_table_size);
    if(!new_table) return ERR_MALLOC;
    memcpy(new_table,old_table,offset);
    int rest_length=table_size-((old_table+offset+strlen(key_name)+value_size+3)-old_table);
    if(rest_length>0)
    {
#ifdef DEBUG
      //printf("copying old_table to offset=%d from offset=%d with length=%d\n",offset+1+strlen(key_name)+2+value_length,offset+strlen(key_name)+strlen(temp)+2,rest_length);
#endif
      memcpy(new_table+offset,old_table+offset+strlen(key_name)+value_size+3,rest_length);
    }
    //sysPersistentSet(new_table,new_table_size,PSI_SIZE-4-new_table_size);
    err=shm_rw(new_table,new_table_size,SHARED_MEM_SIZE-4-new_table_size,SHM_WRITE);
    nvram_set_table_size(new_table_size);
    free(old_table);
    free(new_table);
    if(err) return err;
    return 0;
  }
  else if(offset==ERR_VALUE_NOT_FOUND)
  {
#ifdef DEBUG
    printf("nvram_del_value: error cant delete cause value not found...\n");
#endif
    return offset;
  }
  //error
  else return offset;
}

int nvram_get_info(char *buf,int *buf_size, char keys_only)
{
  if(*buf_size<strlen("Error: unitialised NVRAM-table\n\n")+1) return ERR_BUF_SIZE_TOO_SMALL;
  int table_size=nvram_get_table_size();
  if(table_size<0)
  {
    sprintf(buf,"Error: unitialised NVRAM-table\n\n\0");
    return table_size;
  }
  else
  {
    if(*buf_size<strlen("table_size=16384\n\n")+table_size+100+1) return ERR_BUF_SIZE_TOO_SMALL;
    char *pos=buf;
    sprintf(buf,"table_size=%d\n\n\0",table_size);
    pos+=strlen(buf);
    if(table_size==0)
    {
      sprintf(pos,"no values in table\n\n\0");
      *buf_size=0;
      return 0;
    }
    else
    {
      char *table;
      char *ptr;
      table=malloc(table_size+1);
      if(!table) return ERR_MALLOC;
      table[table_size]='\0';
      if(nvram_dump_table(table,table_size)) return ERR_UNDEFINED;
      ptr=table;
      
      unsigned char key_length=0;
      unsigned short int value_size=0;
      while(ptr<table+table_size)
      {
        key_length=(unsigned char) *ptr;
        if(key_length<1) return ERR_INCONSISTENT_TABLE;
        memcpy(pos,ptr+1,key_length);
        pos+=key_length;
#ifdef DEBUG
        printf("nvram_get_info: key_length=%hhd\n",key_length);
#endif
        if(!keys_only) sprintf(pos++,"=\0");
        memcpy(&value_size,ptr+1+key_length,sizeof(value_size));
#ifdef DEBUG
        printf("nvram_get_info: %hhx %hhx value_size=%hd\n",ptr,ptr+1,value_size);
#endif
        if(value_size>MAX_VALUE_SIZE) return ERR_INCONSISTENT_TABLE;
        if(!keys_only){
          memcpy(pos,ptr+1+key_length+sizeof(value_size),value_size);
          pos+=value_size;
        }
        sprintf(pos++,"\n\0");
        ptr=ptr+1+key_length+2+value_size;
      }
      *buf_size=pos-buf;
      free(table);
      return 0;
    }
  }
}

int nvram_commit()
{
  return shm_commit();
}
