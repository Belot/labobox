/*
 * Copyright (C) 2008 Patrick Schmidt (coolman1982@users.sourceforge.net)
 * 		      Michael Finsterbusch (fibu@users.sourceforge.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#if !defined(_SHARED_MEM_H_)
#define _SHARED_MEM_H_ 

#include "nvram.h"

#define SHARED_MEM_KEY 979797
#define SEMAPHORE_KEY 797979

//since we have introduced compression we assume an uncompressed size
//of 64k
#define SHARED_MEM_SIZE (MAX_TABLE_SIZE*4+4)
#define SHM_READ 0
#define SHM_WRITE 1

#define COMPRESSION_BIT 31

//init shared_mem
int shm_init();
//deinit shared mem
void shm_deinit();
//read from or write to shared_mem, mode could be SHM_READ or SHM_WRITE
int shm_rw(char *buf, int len, int offset, char mode);
//sync shared_mem with PSI
int shm_commit();
//reloads shared_mem from nvram, should be used when someone directly wrotes to nvram
int shm_reload();

//returns if compression is on or not
int shm_is_compression_on();
//compresses complete nvram-table including table_length from shared_memory to buf
int shm_compress_table(char *out_buf,int *out_len);
//decompresses data from NVRAM to out_buf
int shm_decompress_table(char *out_buf,int *out_len);
//sets compression on/off
void shm_set_compression(int flag);
//checks if actual table in shared_mem fits into MAX_TABLE_SIZE when compressed
int shm_check_compression_size();

#ifdef DEBUG
//forces unlock of rw-semaphore
void shm_force_unlock();
void shm_destroy();
#endif

#endif //_SHARED_MEM_H_

