/*
 * Copyright (C) 2008 Patrick Schmidt (coolman1982@users.sourceforge.net)
 * 		      Michael Finsterbusch (fibu@users.sourceforge.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "minilzo.h"
#include "shared_mem.h"
#include "board_api.h"


#define HEAP_ALLOC(var,size) \
    lzo_align_t __LZO_MMODEL var [ ((size) + (sizeof(lzo_align_t) - 1)) / sizeof(lzo_align_t) ]

static HEAP_ALLOC(wrkmem,LZO1X_1_MEM_COMPRESS);

int shm_id=0;
char *shm_buf=NULL;
int lock=-1;


//init global lock_semaphore
static int sem_init()
{
#ifdef DEBUG
  printf("sem_init()\n");
#endif
  int err,init=1;
  err=semget(SEMAPHORE_KEY,1,IPC_CREAT|IPC_EXCL|SHM_R|SHM_W);
  if(errno==EEXIST)
  {
#ifdef DEBUG
    printf("sem_init() semaphore exists, getting id\n");
#endif
    err=semget(SEMAPHORE_KEY,1,SHM_R|SHM_W);
    init=0;
  }
  if(err==-1)
    goto err;
  
  lock=err;
  if(init)
    err=semctl(lock,0,SETVAL,(int) 1);
  if(err<0)
    goto err;
  
  return 0;
err:
    perror("sem_init()");
    return err;
}

#ifdef DEBUG

void shm_force_unlock()
{
  int err;
  printf("shm_force_unlock()\n");
  if(lock==-1)
  {
    err=sem_init();
    if(err) printf("shm_force_unlock() failed\n");
    goto err;
  }
  semctl(lock,0,SETVAL,(int) 1);
err:
  return;
}

void shm_destroy()
{
  int err;
  printf("shm_destroy()\n");
  if(shm_id==-1 || !shm_buf)
  {
    printf("shm_destroy() error shared_mem not initialized, shm_id=%d, shm_buf=%x\n",shm_id,shm_buf);
    return;
  }
  struct shmid_ds shm_info;
  err=shmctl(shm_id,IPC_RMID,&shm_info);
  if(err<0)
  {
    printf("shm_destroy() shmctl() failed\n");
    return;
  }
}

#endif

//lock semaphore
static int sem_lock()
{
  if(lock==-1) return -1;
  struct sembuf op_lock[1] ={ 0, -1, SEM_UNDO };
  return semop(lock, &op_lock[0], 1);
}

//unlock semaphore
static int sem_unlock()
{
  if(lock==-1) return -1;
  struct sembuf op_unlock[1] ={ 0, 1, SEM_UNDO };
  return semop(lock, &op_unlock[0], 1);
}

//init shared_mem
int shm_init()
{
  int err,init,len,compression_flag;
  char *buf;
  init=1;
#ifdef DEBUG
  printf("shm_init()\n");
#endif
  err=shmget(SHARED_MEM_KEY,SHARED_MEM_SIZE,IPC_CREAT|IPC_EXCL);
  //check if already exists
  if(errno==EEXIST)
  {
#ifdef DEBUG
    printf("shm_init() shared_mem exists, trying to get id\n");
#endif
    err=shmget(SHARED_MEM_KEY,SHARED_MEM_SIZE,0);
    init=0;
  }
  if(err==-1)
  {
#ifdef DEBUG
    printf("shm_init() shmget() failed\n");
#endif
    goto err;
  }
  
  shm_id=err;
  struct shmid_ds shm_info;
  err=shmctl(shm_id,SHM_LOCK,&shm_info);
  if(err<0)
  {
#ifdef DEBUG
    printf("shm_init() shmctl() failed\n");
#endif
    goto err;
  }
  
  shm_buf=(char*)shmat(shm_id,NULL,0);
  if(!shm_buf)
  {
#ifdef DEBUG
    printf("shm_init() shmat() failed\n");
#endif
    goto err;
  }
  
  //first read global compression_flag from NVRAM
  sysPersistentGet((char*)&compression_flag,4,PSI_SIZE-4);
#ifdef DEBUG
  printf("shm_init() read from NVRAM, compression=\n");
#endif
  len=compression_flag;
  len&=~(1<<COMPRESSION_BIT);
    compression_flag=0;
#ifdef DEBUG
  printf("shm_init() read from NVRAM, compression=%d, len=%d\n",compression_flag,len);
#endif
  
  //now initialize if necessary and copy nvram-table to shared mem
  if(init)
  {
    sysPersistentGet(shm_buf+(SHARED_MEM_SIZE-(MAX_TABLE_SIZE+4)),MAX_TABLE_SIZE+4,PSI_SIZE-4-MAX_TABLE_SIZE);
  }
  return 0;
err:
  perror("shm_init()");
  return err;
}

void shm_deinit()
{
  if(shm_id && shm_buf)
    shmdt(shm_buf);
}

int shm_rw(char *buf, int len, int offset, char mode)
{
  int err;
#ifdef DEBUG
  printf("shm_rw() len=%d, offset=%d,mode=%d, shared_mem_size=%d\n",len,offset,mode,SHARED_MEM_SIZE);
#endif
  if(shm_id==-1 || !shm_buf)
  {
#ifdef DEBUG
    printf("shm_rw() error shared_mem not initialized, shm_id=%d, shm_buf=%x\n",shm_id,shm_buf);
#endif
    return -1;
  }
   if(offset>SHARED_MEM_SIZE)
   {
#ifdef DEBUG
     printf("shm_rw() error: invalid offset\n");
#endif
     return -1;
   }
  if(len+offset>SHARED_MEM_SIZE)
    len=SHARED_MEM_SIZE-offset;
#ifdef DEBUG
  printf("shm_rw() len=%d, offset=%d,mode=%d\n",len,offset,mode);
#endif
  if(lock==-1)
  {
    err=sem_init();
    if(err) return err;
  }
  err=sem_lock();
  if(err) return err;
  if(mode==SHM_WRITE)
    memcpy(shm_buf+offset,buf,len);
  else memcpy(buf,shm_buf+offset,len);
  err=sem_unlock();
  if(err)
  {
    perror("shm_rw()");
    return err;
  }
  return 0;
}

int shm_commit()
{
#ifdef DEBUG
  printf("shm_commit()\n");
#endif
  int tablesize,err,len;
  char *buf;
  if(shm_id==-1 || !shm_buf)
  {
#ifdef DEBUG
    printf("shm_commit() error shared_mem not initialized\n");
#endif
    return -1;
  }
  if(lock==-1)
  {
    err=sem_init();
    if(err) return err;
  }
  memcpy(&tablesize,shm_buf+(SHARED_MEM_SIZE-4),4);
  tablesize&=~(1<<COMPRESSION_BIT);

  //if compression is enabled
  if(shm_is_compression_on()==1)
  {
    len=MAX_TABLE_SIZE+4;
    buf=malloc(len);
    if(!buf) return -1;
    err=shm_compress_table(buf,&len);
    if(err)
    {
#ifdef DEBUG
      printf("shm_commit() compression failed\n");
#endif
      return err;
    }
    sysPersistentSet(buf,len,PSI_SIZE-4-len);
    //set compression bit in len
    len|=(1<<COMPRESSION_BIT);
    //set compression len to NVRAM
    sysPersistentSet((char*)&len,4,PSI_SIZE-4);
  }
  else
  {
    err=sem_lock();
    if(err) return err;
    sysPersistentSet(shm_buf+(SHARED_MEM_SIZE-4-tablesize),tablesize+4,PSI_SIZE-4-tablesize);
    err=sem_unlock();
  }
  if(err)
  {
    perror("shm_commit()");
    return err;
  }
  
  return 0;
}

int shm_reload()
{
#ifdef DEBUG
  printf("shm_reload()\n");
#endif
  int err,len,compression_flag;
  char *buf;
   
  //first read global compression_flag from NVRAM
  sysPersistentGet((char*)&compression_flag,4,PSI_SIZE-4);
    compression_flag=0;
  

#ifdef DEBUG
    printf("shm_reload() compression is disabled\n");
#endif
    len=MAX_TABLE_SIZE+4;
    buf=malloc(len);
    if(!buf) return -1;
    sysPersistentGet(buf,MAX_TABLE_SIZE+4,PSI_SIZE-4-MAX_TABLE_SIZE);
    err=shm_rw(buf,len,SHARED_MEM_SIZE-len,SHM_WRITE);
    if(err)
    {
      free(buf);
      goto err;
    }
    shm_set_compression(0);
    free(buf);

err:  
  if(err)
  {
    perror("shm_reload()");
    return err;
  }

  return 0;
}


//returns "1" if compression is on or "0" if not
int shm_is_compression_on()
{
#ifdef DEBUG
  printf("shm_is_compression_on()\n");
#endif
//  int err;
  int compression_flag;
//  err=shm_rw((char*)&compression_flag,4,SHARED_MEM_SIZE-4,SHM_READ);
//  if( (compression_flag&=(1<<COMPRESSION_BIT)) )
//  {
//#ifdef DEBUG
//    printf("shm_is_compression_on() return COMPRESSION_ON\n");
//#endif
//    return 1;
//  }
//  else
//  {
#ifdef DEBUG
    printf("shm_is_compression_on() return COMPRESSION_OFF\n");
#endif
    return 0;
//  }
}

//compresses complete nvram-table including table_length from shared_memory to buf
int shm_compress_table(char *out_buf,int *out_len)
{
#ifdef DEBUG
  printf("shm_compress_table()\n");
#endif
  int tablesize,err;
  
  if(shm_is_compression_on()!=1 || !out_buf || !out_len || *out_len<=0) return -1;
  if(shm_id==-1 || !shm_buf)
  {
#ifdef DEBUG
    printf("shm_compress_table() error shared_mem not initialized\n");
#endif
    return -1;
  }
  if(lock==-1)
  {
    err=sem_init();
    if(err) return err;
  }
  
  if (lzo_init() != LZO_E_OK)
  {
#ifdef DEBUG
    printf("shm_compress_table() internal error - lzo_init() failed !!!\n");
#endif
    return -1;
  }
  err=sem_lock();
  if(err) return err;
  memcpy(&tablesize,shm_buf+SHARED_MEM_SIZE-4,4);
  //zero compression bit
  tablesize&=~(1<<COMPRESSION_BIT);
  err=lzo1x_1_compress(shm_buf+(SHARED_MEM_SIZE-4-tablesize),tablesize+4,out_buf,(lzo_uint*)out_len,wrkmem);
  err=sem_unlock();
  if(err) return err;
#ifdef DEBUG
  if(err == LZO_E_OK)
  {
    printf("shm_compress_table() compressed %d bytes into %d bytes\n",
           tablesize+4, *out_len);
  }
  else
  {
    /* this should NEVER happen */
    printf("shm_compress_table() internal error - compression failed: %d\n", err);
    return -1;
  }
  /* check for an incompressible block */
  if (*out_len >= tablesize+4)
  {
    printf("shm_compress_table() This block contains incompressible data.\n");
    return -1;
  }
#endif
  return err;
}

//decompresses data from NVRAM to out_buf
int shm_decompress_table(char *out_buf,int *out_len)
{
#ifdef DEBUG
  printf("shm_decompress_table()\n");
#endif
  int compressed_tablesize,err,compression_flag;
  char *tmp;
  
  sysPersistentGet((char*)&compression_flag,4,PSI_SIZE-4);
  if( (compression_flag&=(1<<COMPRESSION_BIT)) )
    compression_flag=1;
  else
    compression_flag=0;
  
  if(compression_flag!=1 || !out_buf || !out_len || *out_len<=0) return -1;
  
  if (lzo_init() != LZO_E_OK)
  {
#ifdef DEBUG
    printf("shm_decompress_table() internal error - lzo_init() failed !!!\n");
#endif
    return -1;
  }
  
  //first get compressed_tablesize
  sysPersistentGet((char*)&compressed_tablesize,4,PSI_SIZE-4);
  //zero the compression_bit
  compressed_tablesize&=~(1<<COMPRESSION_BIT);
#ifdef DEBUG
  printf("shm_decompress_table() read compressed_tablesize=%d\n",compressed_tablesize);
#endif
  if(*out_len<compressed_tablesize)
  {
#ifdef DEBUG
    printf("shm_decompress_table() error, out_buf too small\n");
#endif
    return -1;
  }
  //allocate buffer for compressed data
  tmp=malloc(compressed_tablesize);
  if(!tmp) return -1;
  //get compressed data from nvram
  sysPersistentGet(tmp,compressed_tablesize,PSI_SIZE-4-compressed_tablesize);
  err=lzo1x_decompress(tmp,compressed_tablesize,out_buf,(lzo_uint*)out_len,NULL);
#ifdef DEBUG
  if(err == LZO_E_OK)
  {
    printf("shm_decompress_table() decompressed %d bytes to %d bytes\n",
           compressed_tablesize, *out_len);
  }
  else
  {
    /* this should NEVER happen */
    printf("shm_decompress_table() internal error - decompression failed: %d\n", err);
    return -1;
  }
#endif
  return err;
}

//sets compression on/off
void shm_set_compression(int flag)
{
  int err;
#ifdef DEBUG
  printf("shm_set_compression()\n");
#endif
  int tablesize;
  err=shm_rw((char*)&tablesize,4,SHARED_MEM_SIZE-4,SHM_READ);
  if(err)
    return;
  if(flag)
    tablesize|=(1<<COMPRESSION_BIT);
  else
    tablesize&=~(1<<COMPRESSION_BIT);
  err=shm_rw((char*)&tablesize,4,SHARED_MEM_SIZE-4,SHM_WRITE);
}

//checks if actual table in shared_mem fits into MAX_TABLE_SIZE when compressed
int shm_check_compression_size()
{
#ifdef DEBUG
  printf("shm_check_compression_size()\n");
#endif
  int err,len;
  char *buf;
  //always return ok when compression is off
  if(shm_is_compression_on()!=1) return 0;
  len=SHARED_MEM_SIZE;
  buf=malloc(SHARED_MEM_SIZE);
  if(!buf) return -1;
  err=shm_compress_table(buf,&len);
#ifdef DEBUG
  printf("shm_check_compression_size() shm_compress_table() returned %d with compressed len=%d\n",err,len);
#endif
  if(err || len>MAX_TABLE_SIZE)
    return -1;

  return 0;
}

