#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <errno.h>
#include <error.h>
#include <time.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/mount.h>
#include <linux/version.h>
#include <mtd/mtd-user.h>
#include <string.h>

void print_usage()
{
  printf("Usage: mtd_erase <mtd_device_path>\n"
         "                 <mtd_device_path> is usually '/dev/mtd/3>'\n"
	 );
}

int mtd_erase(const char *mtd)
{
	int fd;
	struct mtd_info_user mtdInfo;
	struct erase_info_user mtdEraseInfo;

	fd=open(mtd, O_RDWR);
	if(fd < 0) {
		perror("error:");
		fprintf(stderr, "Could not open mtd device: %s\n", mtd);
		return -1;
	}

	if(ioctl(fd, MEMGETINFO, &mtdInfo)) {
		fprintf(stderr, "Could not get MTD device info from %s\n", mtd);
		close(fd);
		return -1;
	}
        
        fprintf(stdout, "Unlocking %s ...\n", mtd);
        mtdEraseInfo.start = 0;
        mtdEraseInfo.length = mtdInfo.size;
        if(ioctl(fd, MEMUNLOCK, &mtdEraseInfo)) {
          perror("error: ");
          fprintf(stderr, "Failed to unlock %s\n", mtd);
          close(fd);
          return -1;
        }

	fprintf(stdout, "Erasing %s ...\n", mtd);
	mtdEraseInfo.length = mtdInfo.erasesize;

	for (mtdEraseInfo.start = 0;
		 mtdEraseInfo.start < mtdInfo.size;
		 mtdEraseInfo.start += mtdInfo.erasesize) {
		
		ioctl(fd, MEMUNLOCK, &mtdEraseInfo);
		if(ioctl(fd, MEMERASE, &mtdEraseInfo)) {
                        perror("error: ");
			fprintf(stderr, "Could not erase MTD device: %s\n", mtd);
			close(fd);
			return -1;
		}
	}		

	close(fd);
	return 0;
}

int main (int argc, char **argv)
{
  
  char device[50];
  if(argc>1)
    strncpy(device,argv[1],50);
  else
  {
    print_usage();
    return 1;
  }
  return mtd_erase(device);
}
