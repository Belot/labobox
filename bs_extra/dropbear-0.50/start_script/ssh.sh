#!/bin/sh
#
# Start/stops dropbear
#
#

must_save=0

dropbear_datadir="/var/dropbear"
ssh_dss=${dropbear_datadir}"/dropbear_dss_host_key"
ssh_rsa=${dropbear_datadir}"/dropbear_rsa_host_key"
ssh_aut=${dropbear_datadir}"/authorized_keys"

case "$1" in
  start)
    echo "Starting dropbear"
    ssh_start=`/bin/nvram get ssh_start`
    [ "$ssh_start" != "Failed" ] && [ "$ssh_start" != "1" ] && exit 0

    port=`/bin/nvram get ssh_port`
    [ "$port" = "Failed" ] && port=22

    [ ! -d $dropbear_datadir ] && mkdir -m 700 $dropbear_datadir

    /bin/nvram getfile ssh_dss_key=$ssh_dss >/dev/null
    if [ $? -ne 0 ]; then
      rm -f $ssh_dss
      /bin/dropbearkey -t dss -s 1024 -f $ssh_dss >/dev/null 2>/dev/null
      /bin/nvram setfile ssh_dss_key=$ssh_dss >/dev/null
      must_save=1
    fi

    /bin/nvram getfile ssh_rsa_key=$ssh_rsa >/dev/null
    if [ $? -ne 0 ]; then
      rm -f $ssh_rsa
      /bin/dropbearkey -t rsa -s 1024 -f $ssh_rsa  >/dev/null 2>/dev/null
      /bin/nvram setfile ssh_rsa_key=$ssh_rsa >/dev/null
      must_save=1
    fi

    /bin/nvram getfile ssh_authorized_keys=$ssh_aut >/dev/null
    if [ $? -eq 0 ]; then
      chmod 600 $ssh_aut
    else
      rm -f $ssh_aut >/dev/null
    fi

    plogin=`/bin/nvram get ssh_disable_plogin`
    if [ "$plogin" = "1" ]; then
      plogin="-s"
    else
      plogin=""
    fi

    issue=`/bin/nvram get ssh_issue`
    [ "$issue" = "0" ] && issue="" || issue="-b /etc/issue.net"

    /bin/dropbear $issue -p $port $plogin
    [ $must_save -eq 1 ] && [ "$STARTUP" != "1" ] && /bin/nvram commit >/dev/null
    ;;
  stop)
    if [ -e /var/run/dropbear.pid ]; then
      echo "Stopping dropbear"
      kill $(cat /var/run/dropbear.pid)
    fi
    ;;
  restart)
    $0 stop
    usleep 400000
    $0 start
    ;;
  forcestart)
    kill -9 `pidof dropbear` >/dev/null 2>/dev/null
    usleep 400000
    rm -f $ssh_dss
    rm -f $ssh_rsa
    /bin/dropbearkey -t dss -s 1024 -f $ssh_dss >/dev/null 2>/dev/null
    /bin/dropbearkey -t rsa -s 1024 -f $ssh_rsa >/dev/null 2>/dev/null
    /bin/dropbear -b /etc/issue.net -p 22
    ;;
  *)
    echo "Usage: /etc/start_scripts/ssh.sh {start|stop|restart}"
    exit 1
    ;;
esac

exit 0
