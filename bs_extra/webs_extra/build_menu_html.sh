#!/bin/bash


cat << EOF_HTML
<HTML>
<HEAD>
<TITLE>Menu</TITLE>
<base target="hcti">
<LINK rel="stylesheet" href="style.css" type="text/css">
<STYLE type="text/css">
<!--
body {font-family:Arial, sans-serif; font-size:8px;}

a{display: block;}
-->
</STYLE>

</HEAD>

<BODY>

<DIV id=m_frame>

<DIV id=m_bild><img border="0" src="pic_m_head.gif"></DIV>


<DIV id=m_but><a href="cgi-bin/index.cgi">&gt; Home</a></DIV>
<DIV id=m_leer></DIV>

<DIV id=m_otitel>System Settings</DIV>
<DIV id=m_but><a href="cgi-bin/admin_index.cgi">&gt; Administration </a></DIV>
<DIV id=m_but><a href="cgi-bin/save_load_index.cgi">&gt; Save and Load </a></DIV>
<DIV id=m_but><a href="cgi-bin/fwupdate.cgi">&gt; Firmware update </a></DIV>

<DIV id=m_leer></DIV>
<DIV id=m_otitel>Network Settings</DIV>
<DIV id=m_but><a href="cgi-bin/lan.cgi">&gt; Lan </a></DIV>
<DIV id=m_but><a href="cgi-bin/wlan_index.cgi">&gt; Wireless LAN </a></DIV>
<DIV id=m_but><a href="cgi-bin/wan_index.cgi">&gt; DSL/WAN </a></DIV>
EOF_HTML

if [ "$BUILD_IPv6" = "1" ]; then
  cat << EOF_HTML
<DIV id=m_but><a href="cgi-bin/ipv6_index.cgi">&gt; IPv6 </a></DIV>
EOF_HTML
fi

cat << EOF_HTML
<DIV id=m_but><a href="cgi-bin/fw_index.cgi">&gt; Firewall </a></DIV>

EOF_HTML

if [ "$BUILD_VOIP" = "1" ]; then
  cat << EOF_HTML
<DIV id=m_leer></DIV>
<DIV id=m_otitel>Phone Settings</DIV>
<DIV id=m_but><a href="cgi-bin/voip_index.cgi">&gt; Phone </a></DIV>
<DIV id=m_but><a href="cgi-bin/call_mon.cgi">&gt; Call monitor </a></DIV>

EOF_HTML
fi

cat << EOF_HTML
<DIV id=m_leer></DIV>
<DIV id=m_otitel>Services</DIV>
<DIV id=m_but><a href="cgi-bin/dhcp.cgi">&gt; DHCP </a></DIV>
<DIV id=m_but><a href="cgi-bin/dns.cgi">&gt; DNS </a></DIV>
<DIV id=m_but><a href="cgi-bin/telnet.cgi">&gt; Telnet </a></DIV>
<DIV id=m_but><a href="cgi-bin/ssh.cgi">&gt; SSH </a></DIV>
<DIV id=m_but><a href="cgi-bin/stproxy.cgi">&gt; HTTP-Proxy </a></DIV>
<DIV id=m_but><a href="cgi-bin/additional_index.cgi">&gt; Additional </a></DIV>

<DIV id=m_leer></DIV>
<DIV id=m_otitel>Status/Info</DIV>
<DIV id=m_but><a href="cgi-bin/status_index.cgi">&gt; Status </a></DIV>
<DIV id=m_but><a href="cgi-bin/info_index.cgi">&gt; Information </a></DIV>

<DIV id=m_leer></DIV>
<DIV id=m_otitel>Plugins</DIV>
<DIV id=m_but><a href="cgi-bin/plugin.cgi">&gt; Plugin Overview </a></DIV>

</DIV>

</BODY>

</HTML>
EOF_HTML


