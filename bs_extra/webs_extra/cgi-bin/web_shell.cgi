#!/bin/sh
#NAME: Web-Shell
#DESCRIPTION: Execute a shell command via the web interface.

HOME="admin_index.cgi"
. /webs/cgi-bin/login.cgi

if [ "$action" = "3" ]; then
  cmd=`echo $cmd|sed -e 's/+/ /g'|sed -e 's/%/\\\x/g'|sed -e 's/[\]x0[dD][\]x0[aA]//g'`
  cmd_html=`echo -e $cmd " "|sed -e 's/\"/\&quot;/g'`
fi

cat << EOF_HTML 
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../../ie.css);</style><![endif]-->
<SCRIPT type="text/JavaScript">
function enter(event)
{
  if (event)
  {
   if(navigator.appName=="Netscape" && event.which == 13)
	subm(3);
   else if(event.keyCode==13)
	subm(3);
  }
  else
      	return true;
}
</SCRIPT>
</HEAD>
<BODY onload="document.getElementById('cmd').focus();">
<DIV id=c_border>
<DIV id=c_pfad>Administration / Web-Shell</DIV>
<DIV id=c_titel>Web-Shell</DIV>
EOF_HTML
if [ "$action" = "3" ]; then
  echo '<pre style="margin:15px;">'
  echo -e "Web-Shell> " $cmd 
  eval `echo -e $cmd 2>&1` 
  echo '</pre>'
fi

cat << EOF_HTML 
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<DIV id=c_std>
<table border="0" cellpaddig="0" cellspacing="2">
<tr onmouseover="i_showElem(1);" onmouseout="i_showElem(0);" >
<td width="80">Command:</td>
<td><input id="cmd" name="cmd" size="70" type="text" onkeypress="return enter(event);" value="
EOF_HTML
 echo $cmd_html
cat << EOF_HTML
"></td>
</tr></table></DIV>
</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Run  &lt;&lt;</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Web-Shell</h2>
<p>Execute a shell command.</p>
</DIV>
<DIV id=i_content1>
<h2>Web-Shell</h2>
<p><b>Command</b></p>
<p>Enter the command to execute.</p>
</DIV>
</BODY></HTML>
EOF_HTML

a=0




