#!/bin/sh

action=0

. ./login.cgi

f_pfad="Administration / Web interface"
f_name="Web interface"
HOME="admin_index.cgi"
. ./functions.sh

PATH=.:$PATH

errmsg=""
mustsave=0

#### Formular-Auswertung #######################################################
#SAVE 
if [ "$action" -eq 2 ] || [ "$action" -eq 3 ]; then 
  [ "$form_http_start" != "`nvram get http_start`" ] && res=`nvram set http_start=$form_http_start` && mustsave=1 && [ "$res" != "OK" ] && err=1
  
  http_port=`/bin/nvram get http_port`
  if [ "$form_http_port" != "$http_port" ]; then
    if [ "$form_http_port" -ge "1" -o "$form_http_port" -le "65534" ]; then
	is_fwservice_port "tcp" "$http_port" && remove_fwservice_port "tcp" "$http_port" && fw_restart=1 && mustsave=1
	http_port="$form_http_port"
	res=`/bin/nvram set http_port=$form_http_port` && mustsave=1 && [ "$res" != "OK" ] && err=1
    fi
  fi
  
  if [ "$form_fw_wan_web" = "1" ]; then
    is_fwservice_port "tcp" "$http_port" || add_fwservice_port "tcp" "$http_port" && mustsave=1
  else
    is_fwservice_port "tcp" "$http_port" && remove_fwservice_port "tcp" "$http_port" && mustsave=1
  fi
  
  [ "$err" -ne "0" ] && errmsg="$errmsg Write to NVRam failed!"'\n'

  [ "$mustsave" -eq 1 ] && nvram commit >/dev/null && [ "$action" -eq 3 ] && [ "$fw_restart" = "1" ] && /etc/start_scripts/firewall.sh services_restart >/dev/null 2>/dev/null && /etc/start_scripts/http.sh restart >/dev/null 2>/dev/null
fi
################################################################################

[ "$mustsave" -eq 1 ] && nvram commit >/dev/null

http_start=`/bin/nvram get http_start`
[ "$http_start" = "Failed" ] && http_start=1
[ "$http_start" = "1" ] && http_on='checked="checked"' || http_off='checked="checked"'

http_port=`/bin/nvram get http_port`
[ "$http_port" = "Failed" ] && http_port="80"

is_fwservice_port "tcp" "$http_port" && fw_wan_web_on='checked="checked"' || fw_wan_web_off='checked="checked"'

cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>

<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function http_change() {
  if(document.getElementById("form_http_start_off").checked==true){
    document.getElementById("http_port").disabled=true;
    document.getElementById("wan_web_off").disabled=true;
    document.getElementById("wan_web_on").disabled=true;
}
  else{
    document.getElementById("http_port").disabled=false;
    document.getElementById("wan_web_off").disabled=false;
    document.getElementById("wan_web_on").disabled=false;
}
}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="http_change();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>$f_pfad</DIV>
<DIV id=c_titel>Web interface</DIV>
<DIV id=c_std>
<form id="xform" name="xform" method="post">
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0)>
<td width="200">Web interface</td>
<td width="20"><input type="radio" id="form_http_start_off" name="form_http_start" value="0" onchange="http_change();" $http_off></td>
<td width="40">Off</td>
<td width="20"><input type="radio" id="form_http_start_on" name="form_http_start" value="1" onchange="http_change();" $http_on></td>
<td>On</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0)>
<td width="200">HTTP-port</td>
<td ><input type="text" id="http_port" name="form_http_port" size="5" maxlength="5" value="$http_port"></td>
</tr></table>
</DIV>
<DIV id=c_last>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0)>
<td width="200">Allow WAN web management</td>
<td width="20"><input type="radio" id="wan_web_off" name="form_fw_wan_web" value="0" $fw_wan_web_off></td>
<td width="40">Off</td>
<td width="20"><input type="radio" id="wan_web_on" name="form_fw_wan_web" value="1" $fw_wan_web_on></td>
<td>On</td>
<input id="action" type="hidden" name="action" value="0">
</tr></table>
</DIV>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover="rahmen(1,3);i_showElem(4)" onmouseout="rahmen(0,3);i_showElem(0)">Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover="rahmen(1,4);i_showElem(5)" onmouseout="rahmen(0,4);i_showElem(0)">Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>http</h2>
<p>Start/Stop http-service and control http-Access on public interface</p>
</DIV>
<DIV id=i_content1>
<h2>Web interface</h2>
<p><b>Start/Stop</b><br>Start http-service Yes/No</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Setting is checked on next Reboot</p>
</DIV>
<DIV id=i_content2>
<h2>Web interface</h2>
<p><b>HTTP-port</b><br>Set on which TCP-Port web interface should be reachable</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Enter Valid Port-Number from 1-65534, default=80</p>
</DIV>
<DIV id=i_content3>
<h2>Allow WAN web management</h2>
<p>Set this <b>ON</b> if your router web interface should be reachable from the internet/WAN interface.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> default=OFF</p>
</DIV>
<DIV id=i_content4>
<h2>Web interface</h2>
<p><b>Save</b><br>Saves changed settings to NVRAM</p>
</DIV>
<DIV id=i_content5>
<h2>Web interface</h2>
<p><b>Save&amp;Run</b><br>Save changed settings to NVRAM and restart HTTPd with new settings</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Changes are safed permanently</p>
</DIV>
</DIV></BODY></HTML>

