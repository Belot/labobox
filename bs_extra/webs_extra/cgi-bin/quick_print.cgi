#!/bin/sh

from=quick_print.cgi

. ./login.cgi

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
EOF_HTML

echo '<style type="text/css">' \
     '<!--' \
'body {' \
'	background-color:#FFFFFF;'\
'	color:#000000;'\
'	font-size:11pt;'\
'	font-family:Arial;'\
'}'\
'table {'\
'        border:1pt;'\
'	border-spacing:2px;'	\
'        background-color:#FFFFFF;'\
'        border-style:solid;'\
'        padding:0pt;'\
'        border-color:#111010;'\
'        border-width:1pt;'\
'        font-size:11pt;'\
' 	margin-left:auto; margin-right:auto;'\
'}'\
'th {'\
'	font-size:13pt;'\
'	border:1px  solid;' \
'	border-width:1pt;'\
'	border-top-color:#E1E1E1;'\
'	border-bottom-color:#CBCBCB;'\
'	border-left-color:#E1E1E1;'\
'	border-right-color:#CBCBCB;'\
'       padding: 5pt;'\
'	text-align: center;'\
'}'\
'td {'\
'	border:1px  solid;' \
'	border-width:1pt;'\
'	border-top-color:#E1E1E1;'\
'	border-bottom-color:#CBCBCB;'\
'	border-left-color:#E1E1E1;'\
'	border-right-color:#CBCBCB;'\
'       padding: 2pt;'\
'	text-align: left;'\
'}'\
     '-->' \
    '</style>' \
    '</HEAD>' \
    '<BODY>' \
    '<center><table border="0" cellpadding="0" cellspacing="2" style="text-align: center;" >' \
    '<tr><th>QD</td><th>Phone number</td><th>Description</td></tr>'
    
i=0
while [ $i -le 99 ]
do  
    number=`nvram get ata_qn$i`
    desc=`nvram get ata_qd$i`

    [ "$number" = "Failed" ] && number=""
    [ "$desc" = "Failed" ] && desc=""

    [ "$number" = "" ] && [ "$desc" = "" ] && let i=$i+1 && continue
    qd=$i
    [ $qd -le 9 ] && qd="0$qd"

echo "<tr><td>$qd#</td>" \
    "<td>$number</td>" \
    "<td>$desc</td></tr>"

    let i=$i+1
done
    
echo '</table></center></BODY></HTML>'
