#!/bin/sh

HOME="info_index.cgi"

uname=`uname -a`
bs_version=`cat /etc/version`
busybox=`busybox |grep "BusyBox v"|awk '{print $2" "$3" "$4" "$5;}'`
date=`date`;
uptime=`uptime |sed -e 's/, load.*//'`
hostname=`cat /proc/sys/kernel/hostname`
system_type=`cat /proc/cpuinfo |grep 'system type'|sed -e 's/.*://'`
cpu_model=`cat /proc/cpuinfo |grep 'cpu model'|sed -e 's/.*://' `
bogo=`cat /proc/cpuinfo |grep 'BogoMIPS'|sed -e 's/.*://'`
wlan="yes"
/etc/start_scripts/wlan_present.sh
[ $? -ne 0 ] && wlan="no"
[ -f "/lib/modules/2.6.8.1/kernel/net/ipv6/ipv6.ko" ] && ipv6="yes" || ipv6="no"

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onselectstart="return false">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Information / System information</DIV>
<div id="c_titel">System information</div>
<div id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr><td width="100">BS Version:</td><td>$bs_version</td></tr>
</table>
</div>
<div id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr><td width="100">Kernel:</td><td>$uname</td></tr>
</table>
</div>
<div id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr><td width="100">BusyBox:</td><td>$busybox</td></tr>
</table>
</div>
<div id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr><td width="100">System time:</td><td>$date</td></tr>
</table>
</div>
<div id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr><td width="100">Uptime:</td><td>$uptime</td></tr>
</table>
</div>
<div id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr><td width="100">Hostname:</td><td>$hostname</td></tr>
</table>
</div>
<div id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr><td width="100">System type:</td><td>$system_type</td></tr>
</table>
</div>
<div id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr><td width="100">CPU model:</td><td>$cpu_model</td></tr>
</table>
</div>
<div id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr><td width="100">BogoMIPS:</td><td>$bogo</td></tr>
</table>
</div>
<div id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr><td width="100">WLAN:</td><td>$wlan</td></tr>
</table>
</div>
<div id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr><td width="100">IPv6:</td><td>$ipv6</td></tr>
</table>
</div>
<DIV id="c_foot"></DIV>
</DIV></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>System information</h2>
<p></p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML

a=0