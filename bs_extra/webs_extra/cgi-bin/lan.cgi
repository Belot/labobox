#!/bin/sh

from=lan.cgi
action=0

. ./login.cgi

PATH=".:"$PATH
HOME="index.cgi"

mustsave=0

if [ "$action" -eq 2 ] || [ "$action" -eq 3 ]; then

  lan_hostname=`echo "$text_hostname"|sed -n -e 's/\([^A-Za-z0-9\-]\)*//gp'`
  if [ ${#lan_hostname} -ne ${#text_hostname} ]; then
    errmsg="$errmsg Invalid hostname!"'\n'
  else
    [ "$lan_hostname" != "`nvram get lan_hostname`" ] && res=`nvram set lan_hostname=$lan_hostname` && mustsave=1 && [ "$res" != "OK" ] && err=1
  fi
  [ "$form_lan_stp" != "`nvram get lan_stp`" ] && res=`nvram set lan_stp=$form_lan_stp` && mustsave=1 && [ "$res" != "OK" ] && err=1

  if [ "$form_lan_dhcp" = "1" ]; then
    [ "dhcp" != "`nvram get lan_ipaddr`" ] && res=`nvram set lan_ipaddr=dhcp` && mustsave=1 && [ "$res" != "OK" ] && err=1
  else
    if [ "$text_ip" != "" ]; then
      ip=`isip.sh $text_ip`
      [ "$ip" = "ERROR" ] && errmsg="$errmsg Invalid IP-address!"'\n' && myerr=1
    fi
    [ "$myerr" != "1" ] && [ "$ip" != "`nvram get lan_ipaddr`" ] && res=`nvram set lan_ipaddr=$ip` && mustsave=1 && [ "$res" != "OK" ] && err=1
    myerr=0
    if [ "$text_ipmask" != "" ]; then
      ipmask=`isip.sh $text_ipmask`
      [ "$ipmask" = "ERROR" ] && errmsg="$errmsg Invalid subnet mask!"'\n' && myerr=1
    fi
    [ "$myerr" != "1" ] && [ "$ipmask" != "`nvram get lan_ipmask`" ] && res=`nvram set lan_ipmask=$ipmask` && mustsave=1 && [ "$res" != "OK" ] && err=1
    myerr=0
    if [ "$text_gateway" != "" ]; then
      gateway=`isip.sh $text_gateway`
      [ "$gateway" = "ERROR" ] && errmsg="$errmsg Invalid Gateway IP-address!"'\n' && myerr=1
    fi
    [ "$myerr" != "1" ] && [ "$gateway" != "`nvram get lan_gw`" ] && res=`nvram set lan_gw=$gateway` && mustsave=1 && [ "$res" != "OK" ] && err=1
    myerr=0
    if [ "$text_dns1" != "" ]; then
      dns1=`isip.sh $text_dns1`
      [ "$dns1" = "ERROR" ] && errmsg="$errmsg Invalid DNS-Server_1 IP-address!"'\n' && myerr=1
    fi
    [ "$myerr" != "1" ] && [ "$dns1" != "`nvram get lan_dns1`" ] && res=`nvram set lan_dns1=$dns1` && mustsave=1 && [ "$res" != "OK" ] && err=1
    myerr=0
    if [ "$text_dns2" != "" ]; then
      dns2=`isip.sh $text_dns2`
      [ "$dns" = "ERROR" ] && errmsg="$errmsg Invalid DNS-Server_2 IP-address!"'\n' && myerr=1
    fi
    [ "$myerr" != "1" ] && [ "$dns2" != "`nvram get lan_dns2`" ] && res=`nvram set lan_dns2=$dns2` && mustsave=1 && [ "$res" != "OK" ] && err=1
    myerr=0
    if [ "$text_dns3" != "" ]; then
      dns3=`isip.sh $text_dns3`
      [ "$dns3" = "ERROR" ] && errmsg="$errmsg Invalid DNS-Server_3 IP-address!"'\n' && myerr=1
    fi
    [ "$myerr" != "1" ] && [ "$dns3" != "`nvram get lan_dns3`" ] && res=`nvram set lan_dns3=$dns3` && mustsave=1 && [ "$res" != "OK" ] && err=1
    myerr=0
  fi

  [ "$check_mac" != "`nvram get lan_mac_cloning`" ] && res=`nvram set lan_mac_cloning=$check_mac` && mustsave=1 && [ "$res" != "OK" ] && err=1
  
  if [ "$check_mac" = "1" ]; then
    lanmac=`echo "$text_mac"|sed -e 's/%3A/:/g'|sed -e 's/%3a/:/g'`
    lanmac=`echo "$lanmac"|sed -n -e 's/\([0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]\)/\1/p'`
    if [ "$lanmac" != "" ]; then
      [ "$lanmac" != "`nvram get lan_mac`" ] && res=`nvram set lan_mac=$lanmac` && mustsave=1 && [ "$res" != "OK" ] && err=1
    else
      errmsg="Invalid MAC address!"'\n'
      err=1
    fi
  fi


  [ "$err" -ne 0 ] && errmsg="$errmsg Write to NVRam failed!\n"
  [ "$mustsave" -eq 1 ] && nvram commit >/dev/null && [ "$action" -eq 3 ] && /etc/start_scripts/lan.sh restart >/dev/null 2>/dev/null

fi


ON='checked="checked"'

if [ "$action" = "5" ]; then
  if [ "$form_lan_dhcp" = "1" ]; then
    lan_ipaddr="dhcp"
  else
    lan_ipaddr=`nvram get lan_ipaddr`
    [ "$lan_ipaddr" = "Failed" -o "$lan_ipaddr" = "dhcp" ] && lan_ipaddr="192.168.2.1"
  fi
  lan_hostname=$text_hostname
  [ "$lan_hostname" = "" ] && lan_hostname=`nvram get lan_hostname`
  [ "$lan_hostname" = "Failed" ] && lan_hostname="BS"
else
  lan_ipaddr=`nvram get lan_ipaddr`
  [ "$lan_ipaddr" = "Failed" ] && lan_ipaddr="192.168.2.1"

  lan_hostname=`nvram get lan_hostname`
  [ "$lan_hostname" = "Failed" ] && lan_hostname="BS"
fi
  
[ "$lan_ipaddr" = "dhcp" ] && lan_dhcp_on=$ON || lan_dhcp_off=$ON

lan_stp=`nvram get lan_stp`
[ "$lan_stp" = "Failed" ] && lan_stp="0"
[ "$lan_stp" = "1" ] && lan_stp_on=$ON || lan_stp_off=$ON

if [ "$lan_ipaddr" != "dhcp" ]; then
  lan_ipmask=`nvram get lan_ipmask`
  [ "$lan_ipmask" = "Failed" ] && lan_ipmask="255.255.255.0"
  lan_gw=`nvram get lan_gw`
  [ "$lan_gw" = "Failed" ] && lan_gw=""
  
  lan_dns1=`nvram get lan_dns1`
  [ "$lan_dns1" = "Failed" ] && lan_dns1=""
  lan_dns2=`nvram get lan_dns2`
  [ "$lan_dns2" = "Failed" ] && lan_dns2=""
  lan_dns3=`nvram get lan_dns3`
  [ "$lan_dns3" = "Failed" ] && lan_dns3=""
fi

lan_mac_cloning=`nvram get lan_mac_cloning`
[ "$lan_mac_cloning" = "Failed" ] && lan_mac_cloning="0"
[ "$lan_mac_cloning" = "1" ] && lan_mac_clone_check=$ON || lan_mac_clone_check=""

lan_mac=`nvram get lan_mac`
[ "$lan_mac" = "Failed" ] && lan_mac=""

cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function subm(x){
  if(document.getElementById("form_lan_dhcp_off").value="0" && document.getElementById("text_gateway")){
    var gateway=document.getElementById("text_gateway").value;
    var dns1=document.getElementById("text_dns1").value;
    var dns2=document.getElementById("text_dns2").value;
    var dns3=document.getElementById("text_dns3").value;
    var subnet=document.getElementById("text_ipmask").value;
    var ip=document.getElementById("text_ip").value;
    if(gateway!="" && !isValidIpAddress(gateway)){alert("Gateway address is invalid!");return;}
    if(gateway!="" && !isSameSubNet(ip,subnet,gateway,subnet)){alert("Gateway is in wrong subnet!");return;}
    if(dns1!="" && !isValidIpAddress(dns1)){alert("DNS-Server_1 address is invalid!");return;}
    if(dns2!="" && !isValidIpAddress(dns2)){alert("DNS-Server_2 address is invalid!");return;}
    if(dns3!="" && !isValidIpAddress(dns3)){alert("DNS-Server_3 address is invalid!");return;}
    var lanmac=document.getElementById("text_mac").value;
    var lancheck=document.getElementById("check_mac").checked;
    if(lancheck==true && !isValidMacAddress(lanmac)){alert("MAC: invalid MAC address!");return;}
  }
  load();
  document.getElementById("action").value=x;
  document.getElementById("xform").submit();
}
function showErr() { 
  var err="$errmsg";
  if(err!=""){alert(err);} 
}
function enable_clone(state)
{
        var input = document.getElementById('text_mac');
        if(!input) return;

        if (state == true)
	{
                input.disabled=false;
		input.focus();
	}	
        else
                input.disabled=true;

}
</SCRIPT> 
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD> 
<BODY onload="showErr();">
<DIV id=c_Frame> 
<DIV id=c_border> 
<DIV id=c_pfad>Network settings / LAN</DIV> 
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<DIV id=c_titel>LAN settings</DIV>
<DIV id=c_std>
<table border="0" cellpaddig="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0) >
<td width="200">Hostname</td>
<td><input id="text_hostname" name="text_hostname" size="25" maxlength="63" type="text" value="$lan_hostname"></td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0)><td width="200">Get settings via DHCP</td>
<td width="20"><input type="radio" id="form_lan_dhcp_off" name="form_lan_dhcp" value="0" $lan_dhcp_off onchange="subm(5);"></td>
<td width="40">Off</td>
<td width="20"><input type="radio" id="form_lan_dhcp_on" name="form_lan_dhcp" value="1" $lan_dhcp_on onchange="subm(5);"></td>
<td>On</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(7) onmouseout=i_showElem(0)><td width="200">Spanning Tree Protocol (STP)</td>
<td width="20"><input type="radio" name="form_lan_stp" value="0" $lan_stp_off></td>
<td width="40">Off</td>
<td width="20"><input type="radio" name="form_lan_stp" value="1" $lan_stp_on></td>
<td>On</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(8) onmouseout=i_showElem(0)><td width="200">MAC-Address cloning</td>
HTML

[ "$lan_mac_cloning" = "1" ] && echo "<td><input type=\"text\" id=\"text_mac\" name=\"text_mac\" size=\"25\" maxlength=\"17\" value=\"$lan_mac\"></td>" || echo "<td><input type=\"text\" id=\"text_mac\" name=\"text_mac\" size=\"25\" maxlength=\"17\" disabled=\"true\" value=\"$lan_mac\"></td>"
echo "<td width=\"20\"><input type=\"checkbox\" name=\"check_mac\" id=\"check_mac\" value=\"1\" $lan_mac_clone_check onchange=\"enable_clone(this.checked);\"></td>"
echo '<td width="40">Enable</td>'
echo '</tr></table></DIV>'

if [ "$lan_ipaddr" != "dhcp" ]; then
  echo '<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(3) onmouseout=i_showElem(0)>' \
  '<td width="200">IP-address</td>'
  echo "<td><input type=\"text\" id=\"text_ip\" name=\"text_ip\" size=\"15\" maxlength=\"15\" value=\"$lan_ipaddr\"></td>"
  echo '</tr></table></DIV>'
  echo '<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(4) onmouseout=i_showElem(0)>' \
  '<td width="200">Subnet mask</td>'
  echo "<td><input type=\"text\" id=\"text_ipmask\" name=\"text_ipmask\" size=\"15\" maxlength=\"15\" value=\"$lan_ipmask\"></td>"
  echo '</tr></table></DIV>'
  echo '<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(5) onmouseout=i_showElem(0)>' \
  '<td width="200">Standard gateway</td>'
  echo "<td><input type=\"text\" id=\"text_gateway\" name=\"text_gateway\" size=\"15\" maxlength=\"15\" value=\"$lan_gw\"></td>"
  echo '</tr></table></DIV>'
  echo '<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(6) onmouseout=i_showElem(0)>' \
  '<td width="200">DNS-Server 1</td>'
  echo "<td><input type=\"text\" id=\"text_dns1\" name=\"text_dns1\" size=\"15\" maxlength=\"15\" value=\"$lan_dns1\"></td>"
  echo '</tr></table></DIV>'
  echo '<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(6) onmouseout=i_showElem(0)>' \
  '<td width="200">DNS-Server 2</td>'
  echo "<td><input type=\"text\" id=\"text_dns2\" name=\"text_dns2\" size=\"15\" maxlength=\"15\" value=\"$lan_dns2\"></td>"
  echo '</tr></table></DIV>'
  echo '<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(6) onmouseout=i_showElem(0)>' \
  '<td width="200">DNS-Server 3</td>'
  echo "<td><input type=\"text\" id=\"text_dns3\" name=\"text_dns3\" size=\"15\" maxlength=\"15\" value=\"$lan_dns3\"></td>"
  echo '</tr></table></DIV>'
fi

cat <<HTML
<DIV id=c_foot></DIV> 
</DIV> 
<DIV id=c_verzoeg1></DIV> 
<DIV id=c_verzoeg2></DIV> 
</DIV> 
<DIV id=t_Frame> 
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV> 
</form></DIV>
<DIV id=i_Frame> 
<DIV id=i_content> 
<h2>LAN</h2> 
<p>Configure your LAN settings like IP-adresses and gateways.</p> 
</DIV>
<DIV id=i_content1> 
<h2>Hostname</h2> 
<p>Enter the hostname for your router. This name will be displayed on shell terminals and could be used as DNS name.</p> 
</DIV> 
<DIV id=i_content2> 
<h2>Get settings via DHCP</h2> 
<p>Enable this to get settings on LAN interface (br0) via DHCP.</p> 
</DIV> 
<DIV id=i_content3> 
<h2>IP-address</h2> 
<p>Enter a static IP-address to use on LAN-interface.</p> 
</DIV>
<DIV id=i_content4> 
<h2>Subnet mask</h2>
<p>Enter subnet mask corresponding with entered IP-address.</p>
</DIV> 
<DIV id=i_content5> 
<h2>Standard Gateway</h2> 
<p>If you use a static gateway then enter it here.</p><p><b>This option is naturally needed if the ADSL-Modem is not in use.</b></p> 
</DIV> 
<DIV id=i_content6> 
<h2>DNS-Server 1-3</h2> 
<p>If you want to set one or more different DNS-Server(s) then enter it here.</p><p><b>This option is naturally needed if the ADSL-Modem is not in use.</b></p> 
</DIV>
<DIV id=i_content7> 
<h2>Spanning Tree Protocol</h2> 
<p>STP is used by bridges or switches and protects your LAN from network loops but sends STP datagrams every 
few seconds through the whole network. If your LAN is certainly loop free, disable this to avoid senceless traffic.</p> 
</DIV>
<DIV id=i_content8> 
<h2>MAC-Address cloning</h2> 
<p>Enable this and you can force the router to use a custom defined MAC-address on LAN interface (br0).</p>
<p><b>This option is naturally needed if your provider accepts only one specific MAC-address.</b></p>
</DIV>
</DIV></BODY></HTML>


