#!/bin/sh

do_fw="/bin/iptables -L -nv"
do_fw_nat="/bin/iptables -t nat -L -nv"
do_fw_mangle="/bin/iptables -t mangle -L -nv"

HOME="fw_index.cgi"


cat << EOF_HTML 
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt"type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY>
<DIV id=c_border>
<DIV id=c_pfad>Firewall Settings / Statistics</DIV>
<DIV id=c_titel>Filter table</DIV>
<pre style="padding-left:10px; margin-top:1px">
EOF_HTML

$do_fw

cat << EOF_HTML
</pre>
<div id=c_leer></div>
<DIV id=c_titel>NAT table</DIV>
<pre style="padding-left:10px;">
EOF_HTML

$do_fw_nat

cat << EOF_HTML
</pre>
<div id=c_leer></div>
<DIV id=c_titel>Mangle table</DIV>
<pre style="padding-left:10px;">
EOF_HTML

$do_fw_mangle

cat << EOF_HTML
</pre>
</div>
<DIV id=c_foot></DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Firewall statistics</h2>
<p>Show firewall rules and corresponding counters.</p>
</DIV></DIV>
</BODY></HTML>
EOF_HTML

a=0