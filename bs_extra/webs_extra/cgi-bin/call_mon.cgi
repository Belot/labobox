#!/bin/sh

from=call_mon.cgi
action=0
R1=''
. ./login.cgi


f_pfad="Call monitor"
f_name="Call monitor"


[ "$port" = "" ] && port=1012

err=0
write=0
############################## Save & Run ##################################
if [ "$action" -eq 3 ]; then
  [ "$R1" = "V1" ] && call_mon_start="0"
  [ "$R1" = "V2" ] && call_mon_start="1"

 ( [ "$port" -lt "0" ] || [ "$port" -gt "65535" ] )  && errmsg="$errmsg  Invalid Port: '$port'!"'\n'  && port="" && err=1
 ( [ "$clip_delay" -lt "1" ] || [ "$clip_delay" -gt "10000" ] )  && errmsg="$errmsg  Invalid CLIP-delay '$clip_delay'!"'\n'  && clip_delay="" && err=1

 if [ "$err" -eq 0 ]; then
  [ "$call_mon_start" != "`nvram get call_mon_start`" ] && write=1 && res=`nvram set call_mon_start=$call_mon_start` && [ "$res" != "OK" ] && err=1
  
  [ "$port" != "`nvram get call_mon_port`" ] && write=1 && res=`nvram set call_mon_port=$port` && [ "$res" != "OK" ] && err=1
  
  [ "$clip_delay" != "`nvram get call_mon_clipdelay`" ] && write=1 && res=`nvram set call_mon_clipdelay=$clip_delay` && [ "$res" != "OK" ] && err=1
  
  [ "$sel_time" != "`nvram get call_mon_timefmt`" ] && write=1 && res=`nvram set call_mon_timefmt=$sel_time` && [ "$res" != "OK" ] && err=1

  [ "$err" -ne 0 ] && errmsg="$errmsg Write to NVRam failed!"'\n'
  [ "$write" -eq 1 ] && nvram commit >/dev/null
  /etc/start_scripts/call_mon.sh restart >/dev/null 2>/dev/null
  fi
fi

SEL="selected"

call_mon_start=`nvram get call_mon_start`
[ "$call_mon_start" = "Failed" ] && call_mon_start="0"

[ "$call_mon_start" -eq 1 ] && call_mon_on='checked="checked"' && call_mon_off=''
[ "$call_mon_start" -ne 1 ] && call_mon_on='' && call_mon_off='checked="checked"'

port=`nvram get call_mon_port`
( [ "$port" = "Failed" ] || [ "$port" = "" ] ) && port=1012

clip_delay=`nvram get call_mon_clipdelay`
( [ "$clip_delay" = "Failed" ] || [ "$clip_delay" = "" ] ) && clip_delay=800

time_fmt=`nvram get call_mon_timefmt`
( [ "$time_fmt" = "Failed" ] || [ "$time_fmt" = "" ] ) && time_fmt="utc"

case "$time_fmt" in
                utc)
                    sel_time_utc=$SEL
                    ;;
                cet)
                    sel_time_cet=$SEL
                    ;;
                local)
                    sel_time_local=$SEL
                    ;;
                *)
                    ;;
esac

echo 'Content-type: text/html'
echo 'Connection: close'
echo ''
echo '<HTML><HEAD>'

echo '<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>' \
    '<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>' \
    '<SCRIPT type="text/JavaScript">' \
'function showErr() {' \
"var err=\"$errmsg\";" \
'if(err!=""){alert(err);} }' \
    '</SCRIPT>' \
    '<LINK rel="stylesheet" href="../style.css" type="text/css">' \
    '<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->' \
    '</HEAD>' \
    '<BODY onload="showErr();">' \
    '<DIV id=c_Frame>' \
    '<DIV id=c_border>' \
    "<DIV id=c_pfad>$f_pfad</DIV>" \
    "<DIV id=c_titel></DIV>" \
    '<DIV id=c_std>' \
    '<form id="xform" name="xform" method="post">' \
    '<table border="0" cellpadding="0" cellspacing="2">' \
    '<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0)>' \
    '<td width="150">Call monitor server</td>' \
    "<td width=\"20\"><input type=\"radio\" name=\"R1\" value=\"V1\" $call_mon_off></td>" \
    '<td width="80">Off</td>' \
    "<td width=\"20\"><input type=\"radio\" name=\"R1\" value=\"V2\" $call_mon_on></td>" \
    '<td>On</td>' \
    '</tr></table>' \
    '</DIV>' \
    '<DIV id=c_std>' \
    '<table border="0" cellpadding="0" cellspacing="2">' \
    '<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0)>' \
    '<td width="150">Time format</td>' \
    '<td>' \
    '<select id="sel_time" name="sel_time" size="1">' \
    "<option value=\"utc\" $sel_time_utc>UTC</option>" \
    "<option value=\"cet\" $sel_time_cet>CET</option>" \
    "<option value=\"local\" $sel_time_local>Local timezone</option>" \
    '</select>' \
    '</td>' \
    '</tr></table>' \
    '</DIV>' \
    '<DIV id=c_std>' \
    '<table border="0" cellpadding="0" cellspacing="2">' \
    '<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0)>' \
    '<td width="150">CLIP-delay</td>' \
    "<td width=\"100\"><input type=\"text\" name=\"clip_delay\" size=\"5\" maxlength=\"5\" value=\"$clip_delay\">ms</td>" \
    '<td width="20"></td>' \
    '</tr></table>' \
    '</DIV>' \
    '<DIV id=c_last>' \
    '<table border="0" cellpadding="0" cellspacing="2">' \
    '<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0)>' \
    '<td width="150">Port</td>' \
    "<td width=\"100\"><input type=\"text\" name=\"port\" size=\"5\" maxlength=\"5\" value=\"$port\"></td>" \
    '<td width="20"></td>' \
    '<input id="action" type="hidden" name="action" value="0">' \
    '</tr></table></form>' \
    '</DIV>' \
    '<DIV id=c_leer></DIV>' \
    '<DIV id=c_foot></DIV>' \
    '</DIV>' \
    '<DIV id=c_verzoeg1></DIV>' \
    '<DIV id=c_verzoeg2></DIV>' \
    '</DIV>' \
    '<DIV id=t_Frame>' \
    '<DIV id=t_but4 onclick="subm(3);" onmouseover="rahmen(1,4);i_showElem(7)" onmouseout="rahmen(0,4);i_showElem(0)">Save &amp; Run</DIV>' \
    '</DIV>' \
    '<DIV id=i_Frame>' \
    '<DIV id=i_content>' \
    "<h2>$f_name</h2>" \
    "<p>Configure settings for $f_name.</p>" \
    '<p>If you want to get notified in real-time about incoming calls, you may want to use the Call monitor server.</p>' \
    '<p><img src="../pic_i_hinweis.gif" border="0"><br />To receive call notifications a client program is required.</p>' \
    '</DIV>' \
    '<DIV id=i_content1>' \
    "<h2>$f_name</h2>" \
    '<p>Turn the Call monitor server <b>on</b> or <b>off</b>.</p>' \
    '</DIV>' \
    '<DIV id=i_content2>' \
    "<h2>$f_name</h2>" \
    '<p><b>Port</b><br>Set on which TCP-Port Call monitor server should listen.</p>' \
    '<p><img src="../pic_i_hinweis.gif" border="0"><br /> Enter Valid Port-Number from 1-65534 (default=1012)</p>' \
    '</DIV>' \
    '<DIV id=i_content3>' \
    "<h2>$f_name</h2>" \
    '<p><b>Time format</b><br>Set which timezone the Call Monitor should use in event messages.</p>' \
    '<p><img src="../pic_i_hinweis.gif" border="0"><br /> Default: UTC</p>' \
    '</DIV>' \
    '<DIV id=i_content4>' \
    "<h2>$f_name</h2>" \
    '<p><b>CLIP delay</b><br>Configure how long call_monitor should wait for CLIP information in ms.</p>' \
    '<p><img src="../pic_i_hinweis.gif" border="0"><br /> Enter Valid CLIP-delay from 1-10000ms (default=800)</p>' \
    '</DIV>' \
    '</DIV></BODY></HTML>'
