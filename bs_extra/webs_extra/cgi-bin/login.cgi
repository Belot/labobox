#!/bin/sh

VARS=""
default_timeout="600"
export PATH="/sbin:/bin:/usr/bin:/usr/sbin:/etc/start_scripts:/opt:/opt/bin:/opt/sbin"
export LD_LIBRARY_PATH="/lib:/usr/lib:/opt/lib:/opt:/opt/usr/lib"

if [ "$REQUEST_METHOD" = "GET" ]; then
  VARS=$QUERY_STRING
else
  VARS=`cat -`
fi
#clear \r\n for internet-explorer bug
#VARS=`echo $VARS|sed -e 's/\W+$//g'`
VARS=`echo $VARS|tr -d '\r\n'`
eval `echo $VARS|sed -e 's/&/\n/g'` 2>/dev/null

