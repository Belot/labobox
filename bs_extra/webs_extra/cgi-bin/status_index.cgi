#!/bin/sh

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onselectstart="return false">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Status/Info / Status</DIV>
<div id="c_titel">Router status</div>
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr> <td width="200"><a href="conn_status.cgi" onmouseover="i_showElem(1)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>Connections</a></td>
<td></td></tr></table>
</div>
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr> <td width="200"><a href="mem_status.cgi" onmouseover="i_showElem(4)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>System utilization</a></td>
<td></td></tr></table>
</div>


<div id="c_titel">DSL/WAN status</div>
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr> <td width="200"><a href="wan_status.cgi" onmouseover="i_showElem(2)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>PPP status</a></td>
<td></td></tr></table>
</div>
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr> <td width="200"><a href="dsl_status.cgi" onmouseover="i_showElem(3)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>DSL status</a></td>
<td></td></tr></table>
</div>

<div id="c_titel">DHCP status</div>
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr> <td width="200"><a href="dhcp_status.cgi" onmouseover="i_showElem(5)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>DHCP</a></td>
<td></td></tr></table>
</div>


<DIV id="c_foot"></DIV>
</DIV></DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Status</h2>
<p>Some status information.</p>
</DIV>
<DIV id=i_content1>
<h2>Router status</h2>
<p><b>Connections</b><br>
Show all network connections.</p>
</DIV>
<DIV id=i_content2>
<h2>DSL/WAN status</h2>
<p><b>PPP status</b><br>
Show PPP/Dial-In status.</p>
</DIV>
<DIV id=i_content3>
<h2>DSL/WAN status</h2>
<p><b>DSL status</b><br>
Show DSL status, bit and frequency spectrum.</p>
</DIV>
<DIV id=i_content4>
<h2>Router status</h2>
<p><b>System utilization</b><br>
Show momory usage and CPU utilization.</p>
</DIV>
<DIV id=i_content5>
<h2>DHCP status</h2>
<p><b>DHCP</b><br>
Show DHCP leases.</p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML

a=0