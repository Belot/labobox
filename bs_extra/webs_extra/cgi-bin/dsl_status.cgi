#!/bin/sh

cmd_snr="/bin/adslctl info --SNR"
cmd_bit="/bin/adslctl2 info --Bits"
cmd_info="/bin/adslctl info --stats"
bit_bin="/bin/adslctl2"

up_down_cutoff=31

dsl_mod=`nvram get dsl_mod`
[ "$dsl_mod" = "Failed" ] && dsl_mod="a"

case "$dsl_mod" in
                m|2m|m2|pm|mp)
                    up_down_cutoff=63
					;;
esac 

# TODO: If Annex_B up_down_cutoff=63;

HOME="status_index.cgi"

svg_head()
{
cat << EOF_SVG
<?xml version="1.0" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

<defs>
<!-- Style -->
<style type="text/css">
  <![CDATA[
        line {stroke:black; stroke-width:1px;}
	text {font-family:Verdana,sans-serif; font-size:11px; fill:#000000;}
  ]]>
</style>

</defs>

<!--Hintergrund-->
<rect x="0" y="0" width="560" height="145" fill="#FFFFFF" />
<rect x="25" y="20" width="515" height="90" fill="#91B7C4" />

<!-- X-Y-Achsen -->
<g id="hs">
<line x1="25" y1="110" x2="540" y2="110" style="stroke:#717180" />
<line x1="20" y1="110" x2="25" y2="110" />
<line x1="23" y1="102.5" x2="25" y2="102.5" />
</g>
<use xlink:href="#hs" y="-13" />
<use xlink:href="#hs" y="-26" />
<use xlink:href="#hs" y="-39" />
<use xlink:href="#hs" y="-52" />
<use xlink:href="#hs" y="-65" />
<use xlink:href="#hs" y="-78" />
<line id="vs" x1="25" y1="110" x2="25" y2="115" />
<use xlink:href="#vs" x="32" />
<use xlink:href="#vs" x="64" />
<use xlink:href="#vs" x="96" />
<use xlink:href="#vs" x="128" />
<use xlink:href="#vs" x="160" />
<use xlink:href="#vs" x="192" />
<use xlink:href="#vs" x="224" />
<use xlink:href="#vs" x="256" />
<use xlink:href="#vs" x="288" />
<use xlink:href="#vs" x="320" />
<use xlink:href="#vs" x="352" />
<use xlink:href="#vs" x="384" />
<use xlink:href="#vs" x="416" />
<use xlink:href="#vs" x="448" />
<use xlink:href="#vs" x="480" />
<use xlink:href="#vs" x="512" />
<line x1="25" y1="110" x2="540" y2="110" />
<line x1="25" y1="20" x2="25" y2="110" />

<!--Beschriftung-->
<text x="21" y="125">0</text>
<text x="50" y="125">16</text>
<text x="82" y="125">32</text>
<text x="114" y="125">48</text>
<text x="146" y="125">64</text>
<text x="178" y="125">80</text>
<text x="210" y="125">96</text>
<text x="239" y="125">112</text>
<text x="270" y="125">128</text>
<text x="302" y="125">144</text>
<text x="334" y="125">160</text>
<text x="366" y="125">176</text>
<text x="398" y="125">192</text>
<text x="430" y="125">208</text>
<text x="462" y="125">224</text>
<text x="494" y="125">240</text>
<text x="526" y="125">256</text>

<text x="260" y="138">Tone</text>
EOF_SVG
}

svg_snr_leg()
{
 cat << EOF_SVG
<text x="3" y="12">Signal to Noise Ratio [dB]</text>
<text x="10" y="114">0</text>
<text x="3" y="101">10</text>
<text x="3" y="88">20</text>
<text x="3" y="75">30</text>
<text x="3" y="62">40</text>
<text x="3" y="49">50</text>
<text x="3" y="36">60</text>
<!--Daten-->
EOF_SVG
}

svg_bit_leg()
{
 cat << EOF_SVG
<text x="3" y="12">Bits</text>
<text x="10" y="114">0</text>
<text x="10" y="101">2</text>
<text x="10" y="88">4</text>
<text x="10" y="75">6</text>
<text x="10" y="62">8</text>
<text x="3" y="49">10</text>
<text x="3" y="36">12</text>
<text x="200" y="147">Upload</text>
<text x="320" y="147">Download</text>
<rect x="185" y="137" width="10" height="10" fill="#00F000" />
<rect x="305" y="137" width="10" height="10" fill="#0000F0" />
<!--Daten-->
EOF_SVG
}

svg_head512()
{
cat << EOF_SVG
<?xml version="1.0" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

<defs>
<!-- Style -->
<style type="text/css">
  <![CDATA[
        line {stroke:black; stroke-width:1px;}
	text {font-family:Verdana,sans-serif; font-size:11px; fill:#000000;}
  ]]>
</style>

</defs>

<!--Hintergrund-->
<rect x="0" y="0" width="560" height="145" fill="#FFFFFF" />
<rect x="25" y="10" width="515" height="100" fill="#91B7C4" />

<!-- X-Y-Achsen -->
<g id="hs">
<line x1="25" y1="110" x2="540" y2="110" style="stroke:#717180" />
<line x1="20" y1="110" x2="25" y2="110" />
<line x1="23" y1="102.5" x2="25" y2="102.5" />
</g>
<use xlink:href="#hs" y="-13" />
<use xlink:href="#hs" y="-26" />
<use xlink:href="#hs" y="-39" />
<use xlink:href="#hs" y="-52" />
<use xlink:href="#hs" y="-65" />
<use xlink:href="#hs" y="-78" />
<use xlink:href="#hs" y="-91" />
<line id="vs" x1="25" y1="110" x2="25" y2="115" />
<use xlink:href="#vs" x="32" />
<use xlink:href="#vs" x="64" />
<use xlink:href="#vs" x="96" />
<use xlink:href="#vs" x="128" />
<use xlink:href="#vs" x="160" />
<use xlink:href="#vs" x="192" />
<use xlink:href="#vs" x="224" />
<use xlink:href="#vs" x="256" />
<use xlink:href="#vs" x="288" />
<use xlink:href="#vs" x="320" />
<use xlink:href="#vs" x="352" />
<use xlink:href="#vs" x="384" />
<use xlink:href="#vs" x="416" />
<use xlink:href="#vs" x="448" />
<use xlink:href="#vs" x="480" />
<use xlink:href="#vs" x="512" />
<line x1="25" y1="110" x2="540" y2="110" />
<line x1="25" y1="10" x2="25" y2="110" />

<!--Beschriftung-->
<text x="21" y="125">0</text>
<text x="50" y="125">32</text>
<text x="82" y="125">64</text>
<text x="114" y="125">96</text>
<text x="146" y="125">128</text>
<text x="178" y="125">160</text>
<text x="210" y="125">192</text>
<text x="239" y="125">224</text>
<text x="270" y="125">256</text>
<text x="302" y="125">288</text>
<text x="334" y="125">320</text>
<text x="366" y="125">352</text>
<text x="398" y="125">384</text>
<text x="430" y="125">416</text>
<text x="462" y="125">448</text>
<text x="494" y="125">480</text>
<text x="526" y="125">512</text>

<text x="260" y="138">Tone</text>
EOF_SVG
}

svg_snr_leg512()
{
 cat << EOF_SVG
<text x="3" y="8">Signal to Noise Ratio [dB]</text>
<text x="10" y="114">0</text>
<text x="3" y="101">10</text>
<text x="3" y="88">20</text>
<text x="3" y="75">30</text>
<text x="3" y="62">40</text>
<text x="3" y="49">50</text>
<text x="3" y="36">60</text>
<text x="3" y="23">70</text>
<!--Daten-->
EOF_SVG
}

svg_bit_leg512()
{
 cat << EOF_SVG
<text x="3" y="10">Bits</text>
<text x="10" y="114">0</text>
<text x="10" y="101">2</text>
<text x="10" y="88">4</text>
<text x="10" y="75">6</text>
<text x="10" y="62">8</text>
<text x="3" y="49">10</text>
<text x="3" y="36">12</text>
<text x="3" y="23">14</text>
<text x="200" y="147">Upload</text>
<text x="320" y="147">Download</text>
<rect x="185" y="137" width="10" height="10" fill="#00F000" />
<rect x="305" y="137" width="10" height="10" fill="#0000F0" />
<!--Daten-->
EOF_SVG
}


print_content_type()
{
 echo 'Content-type: image/svg+xml'
 echo 'Connection: close'
 echo ''
}

print_snr()
{
 print_content_type
 svg_head
 svg_snr_leg
 $cmd_snr|awk 'BEGIN{h=3;}{if(h>0){h--; next;} printf "<rect x=\"%s\" y=\"%s\" width=\"2\" height=\"%s\" fill=\"#FFB600\" />",25+$1*2,110-$2*1.3,$2*1.3;}'
 echo "</svg>" 
}

print_snr512()
{
 print_content_type
 svg_head512
 svg_snr_leg512
 $cmd_snr|awk 'BEGIN{h=3;}{if(h>0){h--; next;} printf "<rect x=\"%s\" y=\"%s\" width=\"1\" height=\"%s\" fill=\"#FFB600\" />",25+$1,110-$2*1.3,$2*1.3;}'
 echo "</svg>" 
}

print_bit()
{
 print_content_type
 svg_head
 svg_bit_leg
 [ -f $bit_bin ] && $cmd_bit|awk 'BEGIN{h=3;;c='$up_down_cutoff'}{if(h>0){h--; next;} printf "<rect x=\"%s\" y=\"%s\" width=\"2\" height=\"%s\" fill=\"#%s\" />\n",25+$1*2,110-$2*6.6,$2*6.6,$1<c?"00F000":"0000F0";}'
 echo "</svg>"
}

print_bit512()
{
 print_content_type
 svg_head512
 svg_bit_leg512
 [ -f $bit_bin ] && $cmd_bit|awk 'BEGIN{h=3;c='$up_down_cutoff'}{if(h>0){h--; next;} printf "<rect x=\"%s\" y=\"%s\" width=\"1\" height=\"%s\" fill=\"#%s\" />\n",25+$1,110-$2*6.6,$2*6.6,$1<c?"00F000":"0000F0";}'
 echo "</svg>"
}

print_info()
{
 from=dsl_status.cgi
 . ./login.cgi

cat << EOF_HTML 
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt"type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<style type="text/css">
a{
 color: #ff0000; 
 font-family: Arial, sans-serif; font-size:12px; font-weight: bold;
 text-decoration:none;
}
</style>
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY>
<DIV id=c_border>
<DIV id=c_pfad>Status / DSL information</DIV>
<pre style="padding-left:10px; margin-top:1px">
EOF_HTML

if [ "$more_info" = "1" ]; then
  $cmd_info|grep -v "/bin/adslctl:"
  cat << EOF_HTML
</pre>
</div>
<DIV id=c_foot></DIV>
EOF_HTML
else
  $cmd_info|grep "Status: Showtime" >/dev/null 2>/dev/null
  if [ "$?" = "0" ]; then
     $cmd_info|awk 'BEGIN{h=3;i=0;}{if(h>0){h--; next;} print $0; i++; if(i>10)exit;}'
  else
     $cmd_info|awk 'BEGIN{h=1;i=0;}{if(h>0){h--; next;} print $0; i++; if(i>0)exit;}'
  fi

  cat << EOF_HTML
</pre>
</div>
<DIV id=c_foot></DIV>
<div style="margin-left:15px;">
      <object data="$from?svg=snr" type="image/svg+xml" width="560" height="150">
        <p>
          This document contains SVG content. To view the SVG content
          you need a plugin. E.g. 
          <a href="http://www.adobe.com/svg/viewer/install/main.html" target="_blank">
            Adobe SVG Viewer - Browser Plugin</a>.
        </p>
      </object>
</div>
<div style="margin-left:15px; margin-bottom:10px;">
      <object data="$from?svg=bit" type="image/svg+xml" width="560" height="150">
        <p>
          <a href="http://www.adobe.com/svg/viewer/install/main.html" target="_blank">
            Adobe SVG Viewer - Browser Plugin</a>.
        </p>
      </object>
</div>
EOF_HTML
fi
cat << EOF_HTML
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick='window.location.href="$from?more_info=1";' onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Info &lt;&lt;</DIV>
<DIV id=t_but4 onclick='window.location.href="$from";' onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Update &lt;&lt;</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>DSL Status</h2>
<p>DSL status, bit and frequency spectrum.</p>
</DIV></DIV>
</BODY></HTML>
EOF_HTML
}

print_not_available()
{
 from=dsl_status.cgi
 . ./login.cgi

cat << EOF_HTML 
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt"type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<style type="text/css">
a{
 color: #ff0000; 
 font-family: Arial, sans-serif; font-size:12px; font-weight: bold;
 text-decoration:none;
}
</style>
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY>
<DIV id=c_border>
<DIV id=c_pfad>Status / DSL information</DIV>
<pre style="padding-left:10px; margin-top:1px">
DSL is not enabled!
</pre>
</div>
<DIV id=c_foot></DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick='window.location.href="$from?more_info=1";' onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Info &lt;&lt;</DIV>
<DIV id=t_but4 onclick='window.location.href="$from";' onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Update &lt;&lt;</DIV>
</DIV>
</BODY></HTML>
EOF_HTML
}


do_snr=0
do_bit=0
do_info=1
if [ "$REQUEST_METHOD" = "GET" ]; then
  echo $QUERY_STRING|grep "svg=snr" >/dev/null 2>/dev/null && do_snr=1 && do_info=0
  echo $QUERY_STRING|grep "svg=bit" >/dev/null 2>/dev/null && do_bit=1 && do_info=0
fi

/bin/adslctl2 info --SNR|grep 510 > /dev/null
do_512=$?

/sbin/lsmod|grep adsldd > /dev/null
if [ $? -eq 0 ]; then
  [ $do_info -eq 1 ] && print_info
  [ $do_snr -eq 1 ] && ( [ $do_512 -eq 1 ] && print_snr || print_snr512 )
  [ $do_bit -eq 1 ] && ( [ $do_512 -eq 1 ] && print_bit || print_bit512 )
else
  print_not_available
fi
