#!/bin/sh

. ./login.cgi

[ "$reboot" = "1" ] && /bin/nvram reboot

[ -f /bin/giftrans ] && have_giftrans=1

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
<SCRIPT type="text/JavaScript">
function reboot() {
 check=confirm("Reboot?");
 if(check==true)
   location.href="admin_index.cgi?reboot=1";
}
</SCRIPT>
</HEAD>
<BODY onselectstart="return false">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>System Settings / Administration</DIV>
<div id="c_titel">Administration</div>
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr> <td width="200"><a href="date_time.cgi" onmouseover="i_showElem(1)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>Date &amp; Time</a></td>
<td></td></tr></table>
</div>
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr><td width="200"><a href="password.cgi" onmouseover="i_showElem(2)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>System password</a></td>
</tr></table>
</div>
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr><td width="200"><a href="reset_button.cgi" onmouseover="i_showElem(5)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>Reset button</a></td>
</tr></table>
</div>
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr><td width="200"><a href="jffs_mini_fo.cgi" onmouseover="i_showElem(7)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>JFFS2 and mini_fo</a></td>
</tr></table>
</div>
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr><td width="200"><a href="webif.cgi" onmouseover="i_showElem(8)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>Web interface</a></td>
</tr></table>
</div>
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr><td width="200"><a href="syslog.cgi" onmouseover="i_showElem(9)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>Syslog server</a></td>
</tr></table>
</div>
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr><td width="200"><a href="custom_script.cgi" onmouseover="i_showElem(3)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>Custom script</a></td>
</tr></table>
</div>
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr><td width="200"><a href="web_shell.cgi" onmouseover="i_showElem(6)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>Web-Shell</a></td>
</tr></table>
</div>
EOF_HTML

if [ "$have_giftrans" = "1" ]; then
  cat << EOF_HTML
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr><td width="200"><a href="color_theme.cgi" onmouseover="i_showElem(10)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>Web-Interface color theme</a></td>
</tr></table>
</div>
EOF_HTML
fi

cat << EOF_HTML
<div id="c_linklast">
<table border="0" cellpadding="4" cellspacing="0">
    <tr><td width="200"><a href="javascript:reboot();" onmouseover="i_showElem(4)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>Reboot</a></td>
</tr></table>
</div>
<DIV id="c_foot"></DIV>
</DIV></DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Administration</h2>
<p></p>
</DIV>
<DIV id=i_content1>
<h2>Date &amp; Time</h2>
<p><b>Setup Date und Time.</b><br>
</DIV>
<DIV id=i_content2>
<h2>Administration</h2>
<p><b>System password</b><br>
Change the system password.</p>
</DIV>
<DIV id=i_content3>
<h2>Administration</h2>
<p><b>Custom script</b><br>
Edit and control the custom script. If enabled custom script can be used to do user 
specified settings after startup.</p>
</DIV>
<DIV id=i_content4>
<h2>Administration</h2>
<p><b>Reboot</b><br>
Reboot the system.</p>
</DIV>
<DIV id=i_content5>
<h2>Administration</h2>
<p><b>Reset button</b><br>
Control the settings for the reset button. 
You can define custom commands for use with the reset button.</p>
</DIV>
<DIV id=i_content6>
<h2>Administration</h2>
<p><b>Web-Shell</b><br>
Execute shell commands via web interface.
</p>
</DIV>
<DIV id=i_content7>
<h2>Administration</h2>
<p><b>JFFS2 and mini_fo</b></p>
<p>Administration of JFFS2- and mini_fo-Filesystem.</p>
</DIV>
<DIV id=i_content8>
<h2>Administration</h2>
<p><b>Web interface</b></p>
<p>Administration of web interface.</p>
</DIV>
<DIV id=i_content9>
<h2>Administration</h2>
<p><b>Syslog server</b></p>
<p>Configure a syslog server.</p>
</DIV>
EOF_HTML

if [ "$have_giftrans" = "1" ]; then
  cat << EOF_HTML
<DIV id=i_content10>
<h2>Administration</h2>
<p><b>Web-Interface color theme</b></p>
<p>Change the colors of the Web-Interface.</p>
</DIV>
EOF_HTML
fi

cat << EOF_HTML
</DIV></BODY></HTML>
EOF_HTML

a=1