#!/bin/sh

. ./login.cgi
. ./functions.sh
HOME="wan_index.cgi"
NUMBER_OF_SERVICES=13

get_service_name()
{
  local service_name=""
  if [ "$1" = "0" ]; then
    service_name="dyndns"
  elif [ "$1" = "1" ]; then
    service_name="tzo"
  elif [ "$1" = "2" ]; then
    service_name="dhs"
  elif [ "$1" = "3" ]; then
    service_name="dyns"
  elif [ "$1" = "4" ]; then
    service_name="easydns"
  elif [ "$1" = "5" ]; then
    service_name="gnudip"
  elif [ "$1" = "6" ]; then
    service_name="heipv6tb"
  elif [ "$1" = "7" ]; then
    service_name="justlinux"
  elif [ "$1" = "8" ]; then
    service_name="ods"
  elif [ "$1" = "9" ]; then
    service_name="pgpow"
  elif [ "$1" = "10" ]; then
    service_name="ezip"
  elif [ "$1" = "11" ]; then
    service_name="hn"
  elif [ "$1" = "12" ]; then
    service_name="zoneedit"
  fi
  
  echo -n "$service_name"
}

if [ "$action" = "3" ]; then
 http_unquote_all "password"
 http_unquote_all "domain"
 http_unquote_all "username"

 mustsave=0
 [ "$ddns_start" != "`nvram get ddns_start`" ] && res=`nvram set ddns_start=$ddns_start` && mustsave=1
 [ "$service" != "`nvram get ddns_service`" ] && res=`nvram set ddns_service=$service` && mustsave=1
 
 service_name=`get_service_name "$service"`
 [ "$domain" != "`nvram get ddns_${service_name}_domain`" ] && res=`nvram set ddns_${service_name}_domain="$domain"` && mustsave=1
 [ "$password" != "`nvram get ddns_${service_name}_pass`" ] && res=`nvram set ddns_${service_name}_pass="$password"` && mustsave=1
 [ "$username" != "`nvram get ddns_${service_name}_user`" ] && res=`nvram set ddns_${service_name}_user="$username"` && mustsave=1
 
 [ "$mustsave" -eq 1 ] && nvram commit >/dev/null 
 [ "$mustsave" -eq 1 ] && /etc/start_scripts/ddns.sh restart >/dev/null 2>/dev/null 
fi


[ "$action" = "5" ] && choose_service=$service

ddns_off='checked="checked"'
ddns_on=""
ddns_start=`nvram get ddns_start`
[ "$ddns_start" = "Failed" ] && ddns_start=0
[ "$ddns_start" = "1" ] && ddns_on='checked="checked"' && ddns_off=''

ddns_service=`nvram get ddns_service`
[ "$ddns_service" = "Failed" ] && ddns_service=0
if [ "$choose_service" = "" ]; then
  choose_service=$ddns_service
else
  ddns_service=$choose_service
fi


service_show0="DynDNS.org"
service_show1="TZO.com"
service_show2="DHS.org"
service_show3="DyNS.cx"
service_show4="easyDNS.com"
service_show5="gnudip"
service_show6="heipv6tb"
service_show7="justlinux.com"
service_show8="ODS.org"
service_show9="penguinpowered.com (pgow)"
service_show10="ez-ip.net"
service_show11="hn.org"
service_show12="zoneedit.com"

html_user="Username"
#Username=Email-Address at TZO.com
if [ "$ddns_service" = "1" ]; then
  html_user="Email"
fi


service_name=`get_service_name "$ddns_service"`
username=`nvram get ddns_${service_name}_user`
password=`nvram get ddns_${service_name}_pass`
domain=`nvram get ddns_${service_name}_domain`

[ "$domain" = "Failed" ] && domain=""
[ "$password" = "Failed" ] && password=""
[ "$username" = "Failed" ] && username=""

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
<SCRIPT type="text/JavaScript">
function showErr() {
var err="$ERROR";
if(err!=""){alert(err);}
}
function do_pass_show(state)
{
  if(state) {
    document.getElementById("password").type="text";
  }
  else {
    document.getElementById("password").type="password";
  }
}
</SCRIPT>
</HEAD>
<BODY onload="showErr();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Network Settings / Dynamic DNS</DIV>
<div id="c_titel">Dynamic DNS</div>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0) >
<td width="100">Dynamic DNS:</td>
<td width="20"><input type="radio" name="ddns_start" value="1" $ddns_on></td>
<td width="40">On</td>
<td width="20"><input type="radio" name="ddns_start" value="0" $ddns_off></td>
<td>Off</td>
</tr></table>
</DIV>
<div id="c_std">
<table border="0" cellpadding="4" cellspacing="0">
    <tr onmouseover="i_showElem(2)" onmouseout="i_showElem(0)"> <td width="100">Provider:</td><td>
<select name="service" onchange="subm(5);">
EOF_HTML
i=0
while [ $i -lt $NUMBER_OF_SERVICES ]
do
  sel=""
  if [ "$ddns_service" = "$i" ]; then
    sel='selected=\"checked\"'
  fi
  eval "service_show=\$service_show${i}"
  echo "<option value=\"$i\" $sel>$service_show</option>"
  let i++
done
cat << EOF_HTML
</select>
</td></tr></table>
</div>
EOF_HTML

#hide domain field for heipv6tb
if [ "$ddns_service" != "6" ]; then
cat << EOF_HTML
<div id="c_std">
<table border="0" cellpadding="4" cellspacing="0">
    <tr onmouseover="i_showElem(3)" onmouseout="i_showElem(0)"> <td width="100">Domain:</td><td><input type="input" name="domain" size="25" maxlength="50" value="$domain"></td></tr></table>
</div>
EOF_HTML
fi

cat << EOF_HTML
<div id="c_std">
<table border="0" cellpadding="4" cellspacing="0">
    <tr onmouseover="i_showElem(4)" onmouseout="i_showElem(0)"> <td width="100">$html_user:</td><td><input type="input" name="username" size="25" maxlength="50" value="$username"></td></tr></table>
</div>
<div id="c_std">
<table border="0" cellpadding="4" cellspacing="0">
    <tr onmouseover="i_showElem(5)" onmouseout="i_showElem(0)"> <td width="100">Password:</td><td><input id="password" type="password" name="password" size="25" maxlength="50" value="$password"></td>
<td><input id="pass_show" type="checkbox" onchange="do_pass_show(this.checked);">show</td>
</tr></table>
</div>
<DIV id=c_leer></DIV>
<DIV id="c_foot"></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV></DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Dynamic DNS</h2>
<p>Dynamic DNS is a method to notify a domain name server to change the active DNS configuration of its configured hostnames, addresses or other information stored in DNS.</p>
</DIV>
<DIV id=i_content1>
<h2>Dynamic DNS</h2>
<p>Switch the dynamic DNS service <b>on</b> or <b>off</b>.</p>
</DIV>
<DIV id=i_content2>
<h2>Dynamic DNS</h2>
<p><b>Provider</b><br>
Choose your dynamic DNS provider.</p>
</DIV>
<DIV id=i_content3>
<h2>Dynamic DNS</h2>
<p><b>Domain</b><br>
Enter your domain address.</p>
<p>Example: myhost.dyndns.org</p>
</DIV>
<DIV id=i_content4>
<h2>Dynamic DNS</h2>
<p><b>$html_user</b><br>
Enter your $html_user.</p>
</DIV>
<DIV id=i_content5>
<h2>Dynamic DNS</h2>
<p><b>Password</b><br>
Enter your password.</p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML

a=0
