#!/bin/sh

. ./login.cgi

HOME="save_load_index.cgi"

echo 'Content-type: text/html'
echo 'Connection: close'
echo ''
echo '<HTML><HEAD><TITLE>Additional Features</TITLE>'

echo '<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>' \
'<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>' \
'<SCRIPT type="text/JavaScript">' \
'function subm(x) {' \
'load();' \
'document.getElementById("xform").submit();' \
'}' \
'function do_pass_show(state)' \
'{' \
'  if(state) {' \
'    document.getElementById("key").type="text";' \
'  }' \
'  else {' \
'    document.getElementById("key").type="password";' \
'  }' \
'}' \
'function showErr() {' \
"var err=\"$ERROR\";" \
'if(err!=""){alert(err);}}' \
'</SCRIPT>' \
'<LINK rel="stylesheet" href="../style.css" type="text/css">' \
'<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->' \
'</HEAD>' \
'<BODY onload="showErr();">' \
'<DIV id=c_Frame>' \
'<DIV id=c_border>' \
'<DIV id=c_pfad>Save &amp; Load / Load Settings</DIV>' \
'<DIV id=c_titel>Load Settings</DIV>' \
'<form id="xform" name="xform" method="post" action="loadcfg.cgi" enctype="multipart/form-data">' \
'<DIV id=c_std>' \
'<table border="0" cellpadding="0" cellspacing="2">' \
'<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0) >' \
'<td width="60">File:</td>' \
'<td><input type="file" name="filename" size="30" maxlength="33000"></td></tr></table>' \
'</DIV>' \
'<DIV id=c_last></DIV>' \
'<DIV id=c_last>' \
'<table border="0" cellpadding="0" cellspacing="2">' \
'<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0) >' \
'<td width="60">Password:</td>' \
'<td><input id="key" name="key" size="22" maxlength="20" type="password" value=""></td>' \
'<td><input id="pass_show" type="checkbox" onchange="do_pass_show(this.checked);">show</td>' \
'<input id="action" type="hidden" name="action" value="0">' \
'</tr></table>' \
'</DIV>' \
'<DIV id=c_last>' 
[ "$REBOOT" = "1" ] && echo "<b>$ERROR</b>"
echo '</DIV>' \
'</form>' \
'<DIV id=c_leer></DIV>' \
'<DIV id=c_foot></DIV>' \
'</DIV>' \
'<DIV id=c_verzoeg1></DIV>' \
'<DIV id=c_verzoeg2></DIV>' \
'</DIV>' \
'<DIV id=t_Frame>' \
"<DIV id=t_but1 onclick='window.location.href=\"$HOME\";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>" \
'<DIV id=t_but4 onclick="subm();" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Load &lt;&lt;</DIV>' \
'</DIV>' \
'<DIV id=i_Frame>' \
'<DIV id=i_content>' \
'<h2>Load Settings</h2>' \
'<p>Load a saved config file to the router.</p>' \
'</DIV>' \
'<DIV id=i_content1>' \
'<h2>Load Settings</h2>' \
'<p><b>File</b><br>' \
'Choose the file to upload.</p>' \
'<p><img src="../pic_i_hinweis.gif" border="0"><br /> The type of config-file is automatically detected.</p>' \
'</DIV>' \
'<DIV id=i_content2>' \
'<h2>Load Settings</h2>' \
'<p><b>Password</b><br>' \
'Type in the password that was used for saving the config.</p>' \
'</DIV>' \
'</DIV></BODY></HTML>'

[ "$REBOOT" != "" ] && (sleep 5;/bin/nvram reboot >/dev/null) &

exit 0

