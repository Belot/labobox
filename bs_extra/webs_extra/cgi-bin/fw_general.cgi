#!/bin/sh

action=0

. ./login.cgi

f_pfad="Firewall Settings / Basic settings"
f_name="Basic settings"
HOME="fw_index.cgi"
. ./functions.sh

PATH=.:$PATH

errmsg=""
mustsave=0
mustsave6=0

ipv6=0
[ -f "/lib/modules/2.6.8.1/kernel/net/ipv6/ipv6.ko" ] && ipv6=1


####### Save & Run #######
if [ "$action" -eq 2 ] || [ "$action" -eq 3 ]; then
  [ "$form_fw_start" != "`nvram get fw_start`" ] && res=`nvram set fw_start=$form_fw_start` && mustsave=1 && [ "$res" != "OK" ] && err=1
  if [ $ipv6 -eq 1 ]; then
    [ "$form_fw6_start" != "`nvram get fw6_start`" ] && res=`nvram set fw6_start=$form_fw6_start` && mustsave6=1 && [ "$res" != "OK" ] && err=1
  fi
  [ "$sel_wan_if" != "`nvram get fw_wan_if`" ] && res=`nvram set fw_wan_if=$sel_wan_if` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$form_fw_wan_ping" != "`nvram get fw_wan_ping`" ] && res=`nvram set fw_wan_ping=$form_fw_wan_ping` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$form_fw_nat_loopback" != "`nvram get fw_nat_loopback`" ] && res=`nvram set fw_nat_loopback=$form_fw_nat_loopback` && mustsave=1 && [ "$res" != "OK" ] && err=1
  
  text_max_ip=`echo $text_max_ip|sed -e 's/[^0-9]//g'`
  if [ "$text_max_ip" -lt "1" -o "$text_max_ip" -gt "4096" ]; then 
    errmsg="$errmsg Maximum number of IP-connections is invalid"'\n'
  else
    [ "$text_max_ip" != "`nvram get fw_max_conntrack`" ] && res=`nvram set fw_max_conntrack=$text_max_ip` && mustsave=1 && [ "$res" != "OK" ] && err=1
  fi
  text_tcp_timeout=`echo $text_tcp_timeout|sed -e 's/[^0-9]//g'`
  if [ "$text_tcp_timeout" -lt "1" -o "$text_tcp_timeout" -gt "4096" ]; then 
    errmsg="$errmsg TCP timeout is invalid"'\n'
  else
    [ "$text_tcp_timeout" != "`nvram get fw_tcp_timeout`" ] && res=`nvram set fw_tcp_timeout=$text_tcp_timeout` && mustsave=1 && [ "$res" != "OK" ] && err=1
  fi
  text_udp_timeout=`echo $text_udp_timeout|sed -e 's/[^0-9]//g'`
  if [ "$text_udp_timeout" -lt "1" -o "$text_udp_timeout" -gt "86400" ]; then 
    errmsg="$errmsg UDP timeout is invalid"'\n'
  else
    [ "$text_udp_timeout" != "`nvram get fw_udp_timeout`" ] && res=`nvram set fw_udp_timeout=$text_udp_timeout` && mustsave=1 && [ "$res" != "OK" ] && err=1
  fi

  #new since 0.3.4
  [ "$check_ct_ftp" = "on" ] && check_ct_ftp="1" || check_ct_ftp="0"
  [ "$check_ct_pptp" = "on" ] && check_ct_pptp="1" || check_ct_pptp="0"
  [ "$check_ct_tftp" = "on" ] && check_ct_tftp="1" || check_ct_tftp="0"
  [ "$check_ct_irc" = "on" ] && check_ct_irc="1" || check_ct_irc="0"
  [ "$check_ct_gre" = "on" ] && check_ct_gre="1" || check_ct_gre="0"
  [ "$check_ct_rtsp" = "on" ] && check_ct_rtsp="1" || check_ct_rtsp="0"

  [ "$check_ct_ftp" != "`nvram get fw_ct_ftp`" ] && res=`nvram set fw_ct_ftp=$check_ct_ftp` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$check_ct_irc" != "`nvram get fw_ct_irc`" ] && res=`nvram set fw_ct_irc=$check_ct_irc` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$check_ct_pptp" != "`nvram get fw_ct_pptp`" ] && res=`nvram set fw_ct_pptp=$check_ct_pptp` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$check_ct_rtsp" != "`nvram get fw_ct_rtsp`" ] && res=`nvram set fw_ct_rtsp=$check_ct_rtsp` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$check_ct_tftp" != "`nvram get fw_ct_tftp`" ] && res=`nvram set fw_ct_tftp=$check_ct_tftp` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$check_ct_gre" != "`nvram get fw_ct_gre`" ] && res=`nvram set fw_ct_gre=$check_ct_gre` && mustsave=1 && [ "$res" != "OK" ] && err=1


  [ "$err" -ne 0 ] && errmsg="$errmsg Write to NVRam failed!"'\n'

  [ "$mustsave" -eq 1 ] && nvram commit >/dev/null && [ "$action" -eq 3 ] && /etc/start_scripts/firewall.sh restart >/dev/null 2>/dev/null
  [ "$mustsave6" -eq 1 ] && nvram commit >/dev/null && [ "$action" -eq 3 ] && /etc/start_scripts/firewall_ipv6.sh restart >/dev/null 2>/dev/null
fi

ON='checked=\"checked\"'
SEL="selected"

set_select()
{
  var=$1
  default=$2
  value=`nvram get $var`
  [ "$value" = "Failed" ] && value=$default 
  if [ $value -eq 1 ]; then
    eval `echo ${var}_on=$ON`
    eval `echo ${var}_off=''`
  else
    eval `echo ${var}_on=''`
    eval `echo ${var}_off=$ON`
  fi
}

set_check()
{
  var=$1
  default=$2
  value=`nvram get $var`
  [ "$value" = "Failed" ] && value=$default 
  if [ $value -eq 1 ]; then
    eval `echo check_${var}=$ON`
  else
    eval `echo check_${var}=''`
  fi
}

set_select "fw_start" 1
[ $ipv6 -eq 1 ] && set_select "fw6_start" 1
set_select "fw_wan_ping" 1
set_select "fw_nat_loopback" 0


wan_if=`nvram get fw_wan_if`
[ "$wan_if" = "Failed" ] && wan_if="ppp"
case "$wan_if" in
              "ppp")
                  sel_wan_if_ppp=$SEL
                  ;;
              "br0")
                  sel_wan_if_bridge=$SEL
                  ;;
              "wl0")
                  sel_wan_if_wlan=$SEL
                  ;;
              *)
                  ;;
esac

max_ip=`nvram get fw_max_conntrack`
[ "$max_ip" = "Failed" ] && max_ip=1024

tcp_timeout=`nvram get fw_tcp_timeout`
[ "$tcp_timeout" = "Failed" ] && tcp_timeout=4096

udp_timeout=`nvram get fw_udp_timeout`
[ "$udp_timeout" = "Failed" ] && udp_timeout=120

set_check "fw_ct_ftp" 1
set_check "fw_ct_irc" 0
set_check "fw_ct_gre" 1
set_check "fw_ct_pptp" 0
set_check "fw_ct_tftp" 0
set_check "fw_ct_rtsp" 0


cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function showErr() {
  var err="$errmsg";
  if(err!=""){alert(err);}
}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>$f_pfad</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<DIV id=c_titel>General settings</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0)><td width="200">SPI firewall</td>
<td width="20"><input type="radio" name="form_fw_start" value="0" $fw_start_off></td>
<td width="40">Off</td>
<td width="20"><input type="radio" name="form_fw_start" value="1" $fw_start_on></td>
<td>On</td>
</tr></table>
</DIV>
HTML
if [ $ipv6 -eq 1 ]; then
cat <<HTML
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(13) onmouseout=i_showElem(0)><td width="200">IPv6 SPI firewall</td>
<td width="20"><input type="radio" name="form_fw6_start" value="0" $fw6_start_off></td>
<td width="40">Off</td>
<td width="20"><input type="radio" name="form_fw6_start" value="1" $fw6_start_on></td>
<td>On</td>
</tr></table>
</DIV>
HTML
fi

cat <<HTML  
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0)>
<td width="200">WAN interface</td>
<td>
  <select name="sel_wan_if" size="1">
    <option value="ppp" $sel_wan_if_ppp>DSL/PPP</option>
    <option value="br0" $sel_wan_if_bridge>LAN/BRIDGE</option>
HTML
[ "`nvram get wl_wet`" = "0" ] && echo "<option value=\"wl0\" $sel_wan_if_wlan>WLAN</option>"
cat <<HTML  
  </select>
</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(6) onmouseout=i_showElem(0)><td width="200">Allow WAN ping</td>
<td width="20"><input type="radio" name="form_fw_wan_ping" value="0" $fw_wan_ping_off></td>
<td width="40">Off</td>
<td width="20"><input type="radio" name="form_fw_wan_ping" value="1" $fw_wan_ping_on></td>
<td>On</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(13) onmouseout=i_showElem(0)><td width="200">NAT-loopback</td>
<td width="20"><input type="radio" name="form_fw_nat_loopback" value="0" $fw_nat_loopback_off></td>
<td width="40">Off</td>
<td width="20"><input type="radio" name="form_fw_nat_loopback" value="1" $fw_nat_loopback_on></td>
<td>On</td>
</tr></table>
</DIV>
<DIV id=c_std>
<table border="0" cellpaddig="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0) >
<td width="200">Maximum IP-connections</td>
<td><input id="text_max_ip" name="text_max_ip" size="10" maxlength="4" type="text" value="$max_ip"></td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpaddig="0" cellspacing="2">
<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0) >
<td width="200">TCP timeout</td>
<td><input id="text_tcp_timeout" name="text_tcp_timeout" size="10" maxlength="4" type="text" value="$tcp_timeout"></td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpaddig="0" cellspacing="2">
<tr onmouseover=i_showElem(5) onmouseout=i_showElem(0) >
<td width="200">UDP timeout</td>
<td><input id="text_udp_timeout" name="text_udp_timeout" size="10" maxlength="5" type="text" value="$udp_timeout"></td>
</tr></table></DIV>
<DIV id=c_leer></DIV>
<DIV id=c_titel>Advanced connection tracking</DIV>
<DIV id=c_std>
<table border="0" cellpaddig="0" cellspacing="2">
<tr onmouseover=i_showElem(7) onmouseout=i_showElem(0) >
<td width="200">FTP</td>
<td><input type="checkbox" name="check_ct_ftp" $check_fw_ct_ftp></td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpaddig="0" cellspacing="2">
<tr onmouseover=i_showElem(8) onmouseout=i_showElem(0) >
<td width="200">IRC</td>
<td><input type="checkbox" name="check_ct_irc" $check_fw_ct_irc></td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpaddig="0" cellspacing="2">
<tr onmouseover=i_showElem(9) onmouseout=i_showElem(0) >
<td width="200">PPTP</td>
<td><input type="checkbox" name="check_ct_pptp" $check_fw_ct_pptp></td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpaddig="0" cellspacing="2">
<tr onmouseover=i_showElem(10) onmouseout=i_showElem(0) >
<td width="200">GRE</td>
<td><input type="checkbox" name="check_ct_gre" $check_fw_ct_gre></td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpaddig="0" cellspacing="2">
<tr onmouseover=i_showElem(11) onmouseout=i_showElem(0) >
<td width="200">TFTP</td>
<td><input type="checkbox" name="check_ct_tftp" $check_fw_ct_tftp></td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpaddig="0" cellspacing="2">
<tr onmouseover=i_showElem(12) onmouseout=i_showElem(0) >
<td width="200">RTSP</td>
<td><input type="checkbox" name="check_ct_rtsp" $check_fw_ct_rtsp></td>
</tr></table></DIV>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>General settings</h2>
<p>Configure general firewall parameters like the WAN-interface to use and other basic options.</p>
</DIV>
<DIV id=i_content1>
<h2>SPI firewall</h2>
<p>Turn the Stateful packet inspection (SPI) firewall <b>on</b> or <b>off</b>. SPI watches the state of all connections 
and accepts only packets from established connections coming from the internet.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Deactivating the firewall means that only filtering is deactivated, but Network Address Translation (NAT) to the WAN interface is always on so your local computers can still connect to the internet.</p>
</DIV>
<DIV id=i_content2>
<h2>WAN interface</h2>
<p>Select the interface you use for internet connection.</p>
</DIV>
<DIV id=i_content3>
<h2>Maximum IP-connections</h2>
<p>Enter the maximum count of connections the firewall could track.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> <b>range 256-4096, default=4096</b></p>
</DIV>
<DIV id=i_content4>
<h2>TCP timeout</h2>
<p>Enter timeout in seconds when a tcp connection is deleted from the firewall state machine.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> In most cases editing is only neccessary for some P2P appliactions. <br /> <b>range 1-4096, default=4096</b></p>
</DIV>
<DIV id=i_content5>
<h2>UDP timeout</h2>
<p>Enter timeout in seconds when a udp stream is deleted from the firewall state machine.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> In most cases editing is only neccessary for some P2P appliactions. <br /> <b>range 1-86400, default=120</b></p>
</DIV>
<DIV id=i_content6>
<h2>Allow WAN ping</h2>
<p>Set this <b>ON</b> if your router should be reachable via PING or other ICMP-packets from the internet/WAN interface.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> default=ON</p>
</DIV>
<DIV id=i_content7>
<h2>Conntrack/NAT: FTP</h2>
<p>Enable this if you have problems with file transfers via passive/active FTP.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> default=ON</p>
</DIV>
<DIV id=i_content8>
<h2>Conntrack/NAT: IRC</h2>
<p>Enable this if your IRC-Client doesnt work properly behind the router.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> default=OFF</p>
</DIV>
<DIV id=i_content9>
<h2>Conntrack/NAT: PPTP</h2>
<p>Enable this if you use a PPTP-Client (e.g. Windows-VPN Client) behind the router.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> default=OFF</p>
</DIV>
<DIV id=i_content10>
<h2>Conntrack/NAT: GRE</h2>
<p>Enable this if you start multiple VPN connections (e.g. Windows-VPN) from the local LAN.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> default=ON</p>
</DIV>
<DIV id=i_content11>
<h2>Conntrack/NAT: TFTP</h2>
<p>Enable this if you have problems when using TFTP from the inside LAN.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> default=OFF</p>
</DIV>
<DIV id=i_content12>
<h2>Conntrack/NAT: RTSP</h2>
<p>Enable this if you have problems with multimedia connections from the inside LAN.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> default=OFF</p>
</DIV>
<DIV id=i_content13>
<h2>NAT-loopback</h2>
<p>Enable this if you want to use the external router address from the inside LAN.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> default=OFF</p>
</DIV>
</DIV></BODY></HTML>
