#!/bin/sh

from=wol.cgi
action=0

. ./login.cgi

HOME="additional_index.cgi"


f_pfad="Additional functions / Wake on LAN"
f_name="Wake on LAN"
f_text="The feature  &quot;Wake on LAN&quot; is not included!"
f_bin="/bin/etherwake"
. ./feature.cgi


mac=`echo $mac|sed -e 's/%3A/:/g'|sed -e 's/%3a/:/g'`
if [ "$action" -eq 3 ]; then
    etherwake $mac >/dev/null 2>/dev/null && etherwake $mac >/dev/null 2>/dev/null && etherwake $mac >/dev/null 2>/dev/null
fi

if [ "$action" -eq 2 ]; then
    ret=`isip.sh $ip`
    [ "$ret" != "ERROR" ] && /usr/bin/arping -I br0 -c 1 $ip >/dev/null 2>/dev/null && ret="$?"
    [ "$ret" = "ERROR" ] && ret=2
    [ "$ret" = "0" ] && MSG="Host with IP-address $ip is online."
    [ "$ret" = "1" ] && MSG="No answer from $ip."
    [ "$ret" = "2" ] && MSG="" && ERROR="Bad address '$ip'." && ip=""
fi

DHCPHOSTS=`nvram get dhcphosts`
[ "$DHCPHOSTS" = "Failed" ] && DHCPHOSTS=""

echo 'Content-type: text/html'
echo 'Connection: close'
echo ''
echo '<HTML><HEAD><TITLE>Additional Features</TITLE>'

echo '<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>' \
'<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>' \
'<SCRIPT type="text/JavaScript">' \
'function edit(select) {' \
'var wert = select.options[select.options.selectedIndex].value;' \
'document.getElementById("mac").value = wert;' \
'var Wert=select.options[select.options.selectedIndex].text;' \
'var Ausdruck = /(\d+\.\d+\.\d+\.\d+)/;' \
'var ip="";' \
'if(Ausdruck.test(Wert)){' \
'Ausdruck.exec(Wert);' \
'ip=RegExp.$1;' \
'}' \
'document.getElementById("ip").value=ip;' \
'}' \
'function showErr() {' \
"var err=\"$ERROR\";" \
'if(err!=""){alert(err);} }' \
'</SCRIPT>' \
'<LINK rel="stylesheet" href="../style.css" type="text/css">' \
'<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->' \
'</HEAD>' \
'<BODY onload="showErr();">' \
'<DIV id=c_Frame>' \
'<DIV id=c_border>' \
'<DIV id=c_pfad>Additional functions / Wake on LAN</DIV>' \
'<DIV id=c_titel>Wake on LAN</DIV>' \
'<form id="xform" name="xform" method="post">' \
'<DIV id=c_std>' \
'<table border="0" cellpadding="0" cellspacing="2">' \
'<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0) >' \
'<td width="90">known Hosts:</td>' \
'<td><select name="select" size="1" onchange="edit(this)" style="width:295px">' \
'<option value=""></option>' 
echo $DHCPHOSTS|sed -e 's/~/\n/g'|sed -n -e '/^$/!p'|sed -e 's/\(^[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]\)\(.*\)/<option value="\1">\1\2<\/option>/'
echo '</select></td></tr></table>' \
'</DIV>' \
'<DIV id=c_std>' \
'<table border="0" cellpadding="0" cellspacing="2">' \
'<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0) >' \
'<td width="90">MAC-Address:</td>' \
"<td><input id=\"mac\" name=\"mac\" size=\"22\" maxlength=\"17\" type=\"text\" value=\"$mac\"></td>" \
'<input id="action" type="hidden" name="action" value="0">' \
'</tr></table>' \
'</DIV>' \
'<DIV id=c_std>' \
'<table border="0" cellpadding="0" cellspacing="2">' \
'<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0) >' \
'<td width="90">IP-Address:</td>' \
"<td><input id=\"ip\" name=\"ip\" size=\"22\" maxlength=\"15\" type=\"text\" value=\"$ip\"></td>" \
'</tr></table>' \
'</DIV>' \
'<DIV id=c_last><p><b>' \
"$MSG" \
'</b><p></DIV>' \
'</form>' \
'<DIV id=c_leer></DIV>' \
'<DIV id=c_foot></DIV>' \
'</DIV>' \
'<DIV id=c_verzoeg1></DIV>' \
'<DIV id=c_verzoeg2></DIV>' \
'</DIV>' \
'<DIV id=t_Frame>' \
"<DIV id=t_but1 onclick='window.location.href=\"$HOME\";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>" \
'<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>ARP-Ping</DIV>' \
'<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Run &lt;&lt;</DIV>' \
'</DIV>' \
'<DIV id=i_Frame>' \
'<DIV id=i_content>' \
'<h2>Wake on LAN</h2>' \
'<p>Start a &quot;Wake-on-LAN&quot;-able Computer across the network.</p>' \
'</DIV>' \
'<DIV id=i_content1>' \
'<h2>Wake on LAN</h2>' \
'<p><b>known Hosts</b><br>' \
'Known Hosts from the DHCP-Configuration.</p>' \
'<p><img src="../pic_i_hinweis.gif" border="0"><br /> The DHCP-Server must not be activated.</p>' \
'</DIV>' \
'<DIV id=i_content2>' \
'<h2>Wake on LAN</h2>' \
'<p><b>MAC-Address</b><br>' \
'The MAC-Address of the computer to start.</p>' \
'<p><img src="../pic_i_hinweis.gif" border="0"><br /> Specify the Ethernet address as <b>00:11:22:33:44:55</b>.</p>' \
'</DIV>' \
'<DIV id=i_content3>' \
'<h2>Wake on LAN</h2>' \
'<p><b>IP-Address</b><br>' \
'You can ARP-Ping the host to check if it is up or down.</p>' \
'</DIV>' \
'</DIV></BODY></HTML>'



