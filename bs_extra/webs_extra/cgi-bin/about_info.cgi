#!/bin/sh

HOME="info_index.cgi"

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
<style type="text/css">
a:link { text-decoration:none; font-weight:bold; color:#F68918; }
a:visited { text-decoration:none; font-weight:bold; color:#F68918; }
a:hover { text-decoration:none; font-weight:bold; background-color:#F68918; color:#FFFFFF; }
a:active { text-decoration:none; font-weight:bold; background-color:#F8F8F8; }
a:focus { text-decoration:none; font-weight:bold; background-color:#F68918; color:#FFFFFF;}
p { margin:10px; margin-right:25px;}
</style>
</HEAD>
<BODY onselectstart="return false">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Information / About</DIV>
<div id="c_titel">BitSwitcher</div>
<p style=>
BitSwitcher is an alternative firmware for BCM6348 based routers namely e.g.
<i>TARGA WR500 VoIP</i> or the <i>Speedport W500V</i>.</p>
<p>
BitSwitcher's aim is to use the capabilities of the hardware better than the
common firmware and make the router to do all the things you need in a
SOHO.</p>
<br />
<p>
<table border="0" cellpadding="4" cellspacing="0">
    <tr> <td width="150">Developers:</td><td>Patrick Schmidt</td></tr>
    <tr> <td width="150"></td><td>Michael Finsterbusch</td></tr>
    <tr> <td width="150">Project Web Page:</td>
         <td><a href="http://bitswitcher.sourceforge.net" target="_blank">  http://bitswitcher.sourceforge.net</a></td></tr>
</table>
</p>
<DIV id="c_foot"></DIV>
</DIV></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>About</h2>
<p></p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML

a=0
