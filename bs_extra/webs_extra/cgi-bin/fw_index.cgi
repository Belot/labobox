#!/bin/sh

. ./login.cgi
. ./functions.sh

fw_start=`/bin/nvram get fw_start`
fw_on='Off'
[ "$fw_start" = "Failed" ] && fw_start="1"
[ "$fw_start" -eq "1" ] && fw_on='On'

fw_services_tcp=`/bin/nvram get fw_services_tcp`
if [ "$fw_services_tcp" = "Failed" ]; then
  fw_services_tcp="22"
elif [ "$fw_services_tcp" = "" ]; then
  fw_services_tcp="no ports opened" 
else
  fw_services_tcp=`echo $fw_services_tcp|sed -e 's/ /, /g'`
fi
fw_services_udp=`/bin/nvram get fw_services_udp`
voip_start=`nvram get voip_start`
if [ "$voip_start" = "1" ]; then
 [ "$fw_services_udp" = "Failed" ] && fw_services_udp="5060" || fw_services_udp="5060 $fw_services_udp"
fi
[ "$fw_services_udp" = "Failed" -o "$fw_services_udp" = "Failed" ] && fw_services_udp="no ports opened" || fw_services_udp=`echo $fw_services_udp|sed -e 's/ /, /g'`

rule_list=`/bin/nvram get fw_port_forwarding`
[ "$rule_list" = "Failed" ] && rule_list=""
rule_list=`echo $rule_list|sed -e 's/ /%20/g'|sed -e 's/;/ /g'`

count_forward=0
count_forward_deact=0
count_range_forward=0
count_range_deact=0
for rule in $rule_list
do
  rule=`echo $rule|sed -e 's/%20/ /g'`
  is_range=`echo $rule|grep ":"`
  ena=`echo $rule|sed -e 's/.*#.*#.*#.*#.*#\(.*\)/\1/g'`
  if [ "$is_range" != "" ]; then
    [ "$ena" = "1" ] && let "count_range_forward+=1" || let "count_range_deact+=1"
  else
    [ "$ena" = "1" ] && let "count_forward+=1" || let "count_forward_deact+=1"
  fi
done

rule_list=`/bin/nvram get fw_prot_forwarding`
[ "$rule_list" = "Failed" ] && rule_list=""
rule_list=`echo $rule_list|sed -e 's/ /%20/g'|sed -e 's/;/ /g'`

count_prot=0
count_prot_deact=0
for rule in $rule_list
do
  rule=`echo $rule|sed -e 's/%20/ /g'`
  ena=`echo $rule|sed -e 's/.*#.*#.*#\(.*\)/\1/g'`
  [ "$ena" = "1" ] && let "count_prot+=1" || let "count_prot_deact+=1"
done

rule_list=`/bin/nvram get fw_web_port_rules`
[ "$rule_list" = "Failed" ] && rule_list""
rule_list=`echo $rule_list|sed -e 's/ /%20/g'|sed -e 's/;/ /g'`

count_port_filter=0
count_port_filter_deact=0;
count_web_filter=0
count_web_filter_deact=0;
for rule in $rule_list
do
  rule=`echo $rule|sed -e 's/%20/ /g'`
  type=`echo $rule|sed -e 's/.*#.*#\(.*\)#.*#.*#.*/\1/g'`
  ena=`echo $rule|sed -e 's/.*#.*#.*#\(.*\)/\1/g'`
  if [ "$type" = "port" ]; then
    [ "$ena" = "1" ] && let "count_port_filter+=1" || let "count_port_filter_deact+=1"
  elif [ "$type" = "web" ]; then
    [ "$ena" = "1" ] && let "count_web_filter+=1" || let "count_web_filter_deact+=1"
  fi
done

wan_if=`nvram get fw_wan_if`
[ "$wan_if" = "Failed" ] && wan_if="ppp"
case "$wan_if" in
              "ppp")
                  wan_if="DSL/PPP"
                  ;;
              "br0")
                  wan_if="LAN/BRIDGE"
                  ;;
              "wl0")
                  wan_if="WLAN"
                  ;;
              *)
                  ;;
esac

ip_total=`cat /proc/sys/net/ipv4/netfilter/ip_conntrack_max 2>/dev/null`
ip_use=`wc -l /proc/net/ip_conntrack 2>/dev/null|awk {'print $1;'}`
if [ "$ip_total" = "" ] || [ "$ip_total" = "0" ]; then
  ip_total="no limit specified"
  ip_per=0
else
  let ip_per=$ip_use*100 2>/dev/null
  let ip_per=$ip_per/$ip_total 2>/dev/null
fi

dmz_text="not active"
dmz=`nvram get fw_dmz`
[ "$dmz" = "Failed" ] && dmz=0
if [ "$dmz" = "1" ]; then
  dmz_ip=`nvram get fw_dmz_ip`
  [ "$dmz_ip" != "Failed" ] && dmz_text="active, DMZ-host=$dmz_ip"
fi

nat_loopback_text="not active"
nat_loopback=`nvram get fw_nat_loopback`
[ "$nat_loopback" = "Failed" ] && nat_loopback=0
if [ "$nat_loopback" = "1" ]; then
  nat_loopback_text="active"
fi
  
ipv6=0
if [ -f "/lib/modules/2.6.8.1/kernel/net/ipv6/ipv6.ko" ]; then
  ipv6=1
  fw6_start=`/bin/nvram get fw6_start`
  fw6_on='Off'
  [ "$fw6_start" = "Failed" ] && fw6_start="1"
  [ "$fw6_start" -eq "1" ] && fw6_on='On'
fi

cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>

<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onselectstart="return false">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Firewall configuration</DIV>
<DIV id=c_titel>Firewall settings</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td width="200"><a href="fw_general.cgi" onmouseover=i_showElem(1) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>General Settings</a></td>
</tr>
</table>
</DIV>
<DIV id=c_leer></DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td width="200"><a href="fw_dmz.cgi" onmouseover=i_showElem(8) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>DMZ</a></td>
</tr>
</table>
</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td width="200"><a href="fw_portfw.cgi" onmouseover=i_showElem(2) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>Port Forwarding</a></td>
</tr>
</table>
</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td width="200"><a href="fw_rangeportfw.cgi" onmouseover=i_showElem(3) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>Port Range Forwarding</a></td>
</tr>
</table>
</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td width="200"><a href="fw_protfw.cgi" onmouseover=i_showElem(5) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>Protocol Forwarding</a></td>
</tr>
</table>
</DIV>
<DIV id=c_leer></DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td width="200"><a href="fw_port_filter.cgi" onmouseover=i_showElem(6) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>Port filtering</a></td>
</tr>
</table>
</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td width="200"><a href="fw_webfilter.cgi" onmouseover=i_showElem(7) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>Web Filter</a></td>
</tr>
</table>
</DIV>
<DIV id=c_leer></DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td width="200"><a href="fw_custom.cgi" onmouseover=i_showElem(4) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>Custom rules</a></td>
</tr>
</table>
</DIV>
<DIV id=c_leer></DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td width="200"><a href="fw_statistic.cgi" onmouseover=i_showElem(9) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>Statistics</a></td>
</tr>
</table>
</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td width="200"><a href="fw_debug.cgi" onmouseover=i_showElem(10) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>Debug log</a></td>
</tr>
</table>
</DIV>
HTML
if [ $ipv6 -eq 1 ]; then
cat <<HTML
<DIV id=c_linklast>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td width="200"><a href="fw6_debug.cgi" onmouseover=i_showElem(11) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>IPv6 debug log</a></td>
</tr>
</table>
</DIV>
HTML
fi
cat <<HTML
<DIV id=c_leer></DIV>
<DIV id=c_titel>Status</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="150">Status:</td>
<td>$fw_on</td>
</tr></table></DIV>
HTML
if [ $ipv6 -eq 1 ]; then
cat <<HTML
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="150">Status IPv6:</td>
<td>$fw6_on</td>
</tr></table></DIV>
HTML
fi
cat <<HTML
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="150">WAN interface:</td>
<td>$wan_if</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="150">unfiltered TCP-ports:</td>
<td>$fw_services_tcp</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="150">unfiltered UDP-ports:</td>
<td>$fw_services_udp</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="150">DMZ:</td>
<td>$dmz_text</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="150">NAT-loopback:</td>
<td>$nat_loopback_text</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="150">Port Forwarding:</td>
<td>$count_forward active rules, $count_forward_deact deactivated rules</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="150">Port range forwarding:</td>
<td>$count_range_forward active rules, $count_range_deact deactivated rules</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="150">Protocol Forwarding:</td>
<td>$count_prot active rules, $count_prot_deact deactivated rules</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="150">Port filtering:</td>
<td>$count_port_filter active rules, $count_port_filter_deact deactivated rules</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="150">Web filtering:</td>
<td>$count_web_filter active rules, $count_web_filter_deact deactivated rules</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="150">Max IP-connections:</td>
<td>$ip_total</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="150">Active IP-connections:</td>
<td width="80">$ip_use</td>
<td>
HTML
HBar 250 20 $ip_per
cat <<HTML
</td>
</tr></table></DIV>
<DIV id=c_leer></DIV>
</DIV>
<DIV id="c_foot"></DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Firewall configuration</h2>
<p></p>
</DIV>
<DIV id=i_content1>
<h2>Firewall configuration</h2>
<p><b>General settings</b><br>
Turn the firewall off and tune some basic filtering options.</p>
</DIV>
<DIV id=i_content2>
<h2>Firewall configuration</h2>
<p><b>Port forwarding</b><br>
Here you can configure rules for translating single ports to private IP-addresses.</p>
</DIV>
<DIV id=i_content3>
<h2>Firewall configuration</h2>
<p><b>Port range forwarding</b><br>
Here you can configure rules for translating whole port ranges to private IP-addresses.</p>
</DIV>
<DIV id=i_content4>
<h2>Firewall configuration</h2>
<p><b>Custom rules</b><br>
Here you can type in custom firewall rules which are automatically executed after every rule change.</p>
</DIV>
<DIV id=i_content5>
<h2>Firewall configuration</h2>
<p><b>Protocol forwarding</b><br>
Here you configure rules to forward specific protocols like GRE or IPsec.</p>
</DIV>
<DIV id=i_content6>
<h2>Firewall configuration</h2>
<p><b>Port filtering</b><br>
Here you can deny access for different LAN-IP's to outgoing ports.</p>
</DIV>
<DIV id=i_content7>
<h2>Firewall configuration</h2>
<p><b>Web filtering</b><br>
Here you can deny access to URL's, Web-Adresses and Web-Content to several IP's.</p>
</DIV>
<DIV id=i_content8>
<h2>Firewall configuration</h2>
<p><b>DMZ</b><br>
Here you can specify a private host-IP which should be in a demilitarized zone (DMZ).</p>
</DIV>
<DIV id=i_content9>
<h2>Firewall configuration</h2>
<p><b>Statistics</b><br>
Here you can see all firewall rules and the corressponding rule/packet/byte counters.</p>
</DIV>
<DIV id=i_content10>
<h2>Firewall configuration</h2>
<p><b>Debug log</b><br>
Here you can enable the debug output of the firewall script to look at the commands generated 
by the script. This option is only for experts!</p>
</DIV>
</DIV></BODY></HTML>
