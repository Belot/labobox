#!/bin/sh

from=voip_options.cgi

. ./login.cgi

HOME="voip_index.cgi"

#### Default-Werte ####
[ "$form_pri_select" = "" ] && form_pri_select="0"
#[ "$form_sec_select" = "" ] && form_sec_select="on"
[ "$form_enbl_2line_dial" = "" ] && form_enbl_2line_dial="on"
[ "$form_number_hide" = "" ] && form_number_hide="off"
[ "$form_enable_clip" =  "" ] && form_enable_clip="off"
[ "$form_enable_anklopfen" = "" ] && form_enable_anklopfen="off"
[ "$form_enable_analog" = "" ] && form_enable_analog="on"
[ "$form_enable_fallback" = "" ] && form_enable_fallback="on"
[ "$form_ata_ppp_restart" = "" ] && form_ata_ppp_restart="on"
[ "$form_ata_arcor_fix" = "" ] && form_ata_arcor_fix="off"

#### Formular-Auswertung #######################################################
[ "$form_pri_select" = "0" ] && pri_select=0 || pri_select=1
#[ "$form_sec_select" = "on" ] && sec_select=1 || sec_select=0
[ $pri_select -eq 0 ] && sec_select=1
[ $pri_select -eq 1 ] && sec_select=0
[ "$form_enbl_2line_dial" = "on" ] && enbl_2line_dial=1 || enbl_2line_dial=0
[ "$form_number_hide" = "on" ] && number_hide=1 || number_hide=0
[ "$form_enable_clip" = "on" ] && enable_clip=1 || enable_clip=0
[ "$form_enable_anklopfen" = "on" ] && enable_anklopfen=1 || enable_anklopfen=0
[ "$form_enable_analog" = "on" ] && enable_analog=1 || enable_analog=0
[ "$form_enable_fallback" = "on" ] && enable_fallback=1 || enable_fallback=0
[ "$form_ata_ppp_restart" = "on" ] && ata_ppp_restart=1 || ata_ppp_restart=0
[ "$form_ata_arcor_fix" = "on" ] && ata_arcor_fix=1 || ata_arcor_fix=0

 #SAVE
 if [ "$action" -eq 2 ] || [ "$action" -eq 3 ]; then 
   update=0
   commit=0
   fw_restart=0
   [ "$pri_select" != `nvram get ata_pri_select` ] && nvram set ata_pri_select=$pri_select >/dev/null && update=1
   [ "$sec_select" != `nvram get ata_sec_select` ] && nvram set ata_sec_select=$sec_select >/dev/null && update=1
   [ "$enbl_2line_dial" != `nvram get ata_enbl_2line_dial` ] && nvram set ata_enbl_2line_dial=$enbl_2line_dial >/dev/null && update=1
   [ "$number_hide" != `nvram get ata_number_hide` ] && nvram set ata_number_hide=$number_hide >/dev/null && update=1
   [ "$enable_clip" != `nvram get ata_enable_clip` ] && nvram set ata_enable_clip=$enable_clip >/dev/null && update=1
   [ "$enable_anklopfen" != `nvram get ata_enable_anklopfen` ] && nvram set ata_enable_anklopfen=$enable_anklopfen >/dev/null && update=1
   [ "$enable_analog" != `nvram get ata_enable_analog` ] && nvram set ata_enable_analog=$enable_analog >/dev/null && update=1
   [ "$enable_fallback" != `nvram get ata_enable_fallback` ] && nvram set ata_enable_fallback=$enable_fallback >/dev/null && update=1
   [ "$ata_ppp_restart" != `nvram get ata_ppp_restart` ] && nvram set ata_ppp_restart=$ata_ppp_restart >/dev/null && commit=1
   [ "$ata_arcor_fix" != `nvram get ata_arcor_fix` ] && nvram set ata_arcor_fix=$ata_arcor_fix >/dev/null && fw_restart=1 && commit=1

   [ $update -eq 1 ] && /etc/start_scripts/ata.sh update_psi >/dev/null 2>/dev/null && commit=1
   [ $commit -eq 1 ] && nvram commit >/dev/null
 fi
################################################################################

# RUN
if [ "$action" -eq 3 ]; then
  [ $update -eq 1 ] && /etc/start_scripts/ata.sh restart >/dev/null 2>/dev/null &
  [ $update -ne 1 ] && [ $fw_restart -eq 1 ] && /etc/start_scripts/firewall.sh sip_mangle_restart
fi

ON='checked="checked"'
set_select()
{
  var=$1
  default=$2
  value=`nvram get ata_$var`
  [ "$value" = "Failed" ] && value=$default 
  if [ $value -eq 1 ]; then
    eval `echo ${var}_on=$ON`
    eval `echo ${var}_off=''`
  else
    eval `echo ${var}_on=''`
    eval `echo ${var}_off=$ON`
  fi
}

SEL="selected"
pri_select=`nvram get ata_pri_select`
[ "$pri_select" = "Failed" ] && pri_select=0
voip=""
pstn=""
[ $pri_select -eq 0 ] && pstn=$SEL
[ $pri_select -eq 1 ] && voip=$SEL

set_select "enbl_2line_dial" 1
set_select "number_hide" 0
set_select "enable_clip" 0
set_select "enable_anklopfen" 0
set_select "enable_analog" 1 
set_select "enable_fallback" 1
set_select "ppp_restart" 1
set_select "arcor_fix" 0

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>

<SCRIPT language="JavaScript" src="../js_fade.txt"
type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY>
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Phone settings / Phone options</DIV>
<DIV id=c_titel></DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0)><td width="250">primary connection</td>
<td width="50"><select name="form_pri_select" size="1">
<option value="0" $pstn>PSTN</option><option value="1" $voip>VoIP</option>
</select></td></tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0)><td width="250">choose manually second connection</td>
<td width="20"><input type="radio" name="form_enbl_2line_dial" value="on" $enbl_2line_dial_on></td>
<td width="40">On</td>
<td width="20"><input type="radio" name="form_enbl_2line_dial" value="off" $enbl_2line_dial_off></td>
<td>Off</td></tr>
</table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0)><td width="250">hide phone number</td>
<td width="20"><input type="radio" name="form_number_hide" value="on" $number_hide_on></td>
<td width="40">On</td>
<td width="20"><input type="radio" name="form_number_hide" value="off" $number_hide_off></td>
<td>Off</td></tr>
</table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(5) onmouseout=i_showElem(0)><td width="250">enable CLIP</td>
<td width="20"><input type="radio" name="form_enable_clip" value="on" $enable_clip_on></td>
<td width="40">On</td>
<td width="20"><input type="radio" name="form_enable_clip" value="off" $enable_clip_off></td>
<td>Off</td></tr>
</table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(6) onmouseout=i_showElem(0)><td width="250">call waiting</td>
<td width="20"><input type="radio" name="form_enable_anklopfen" value="on" $enable_anklopfen_on></td>
<td width="40">On</td>
<td width="20"><input type="radio" name="form_enable_anklopfen" value="off" $enable_anklopfen_off></td>
<td>Off</td></tr>
</table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(7) onmouseout=i_showElem(0)><td width="250">enable PSTN</td>
<td width="20"><input type="radio" name="form_enable_analog" value="on" $enable_analog_on></td>
<td width="40">On</td>
<td width="20"><input type="radio" name="form_enable_analog" value="off" $enable_analog_off></td>
<td>Off</td></tr>
</table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(8) onmouseout=i_showElem(0)><td width="250">connection fallback</td>
<td width="20"><input type="radio" name="form_enable_fallback" value="on" $enable_fallback_on></td>
<td width="40">On</td>
<td width="20"><input type="radio" name="form_enable_fallback" value="off" $enable_fallback_off></td>
<td>Off</td></tr>
</table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(9) onmouseout=i_showElem(0)><td width="250">ATA-restart on PPP-restart</td>
<td width="20"><input type="radio" name="form_ata_ppp_restart" value="on" $ppp_restart_on></td>
<td width="40">On</td>
<td width="20"><input type="radio" name="form_ata_ppp_restart" value="off" $ppp_restart_off></td>
<td>Off</td></tr>
</table></DIV>
<DIV id=c_last>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(10) onmouseout=i_showElem(0)><td width="250">enable Arcor-VoIP-fix</td>
<td width="20"><input type="radio" name="form_ata_arcor_fix" value="on" $arcor_fix_on></td>
<td width="40">On</td>
<td width="20"><input type="radio" name="form_ata_arcor_fix" value="off" $arcor_fix_off></td>
<td>Off</td></tr>
</table></DIV>

</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save &lt;&lt;</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Phone options</h2>
<p></p>
</DIV>
<DIV id=i_content1>
<h2>primary connection</h2>
<p>Choose a connection that should used preferred.</p>
</DIV>
<DIV id=i_content2>
<h2></h2>
<p></p>
</DIV>
<DIV id=i_content3>
<h2>choose manually second connection</h2>
<p>Choose manually the second connection by typing <b>2#</b>.</p>
</DIV>
<DIV id=i_content4>
<h2>hide phone number</h2>
<p>Hide the phone number on outgoing calls.</p>
</DIV>
<DIV id=i_content5>
<h2>enable CLIP</h2>
<p>With CLIP enabled you can see the phone number of the calling party on incomming calls.</p>
</DIV>
<DIV id=i_content6>
<h2>call waiting</h2>
<p>If call waiting is enabled the called party is able to suspend the current telephone call and switch to the new incoming call. In Germany this feature is known as "anklopfen".</p>
</DIV>
<DIV id=i_content7>
<h2>enable PSTN</h2>
<p>Allow the use of the PSTN connection.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br />Emergency calls just work if a PSTN connection is available.</p>
</DIV>
<DIV id=i_content8>
<h2>connection fallback</h2>
<p>Use automatically the second connection if the primary connection is in use.</p>
</DIV>
<DIV id=i_content9>
<h2>ATA-restart on PPP-restart</h2>
<p>Automatically restart VoIP when PPP-connection is restartet.</p>
</DIV>
<DIV id=i_content10>
<h2>enable Arcor-VoIP-fix</h2>
<p>Automatically manipulate the header of SIP-packets to fix problems when talking to an Arcor SIP-server.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> This option is experimental. If you are unsure turn it off.</p>
</DIV>
</DIV></BODY></HTML>

EOF_HTML

