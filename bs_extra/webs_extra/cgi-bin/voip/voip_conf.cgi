#!/bin/sh

. ./login.cgi

HOME="voip_index.cgi"
. ./functions.sh

f_pfad="Phone settings / VoIP settings"
f_name="Analog telephone adapter"

PATH=".:$PATH"

errmsg=""
mustsave=0
####### Save & Run #######
if [ "$action" -eq 3 ]; then

[ "$R1" = "V1" ] && ata_start="0"
[ "$R1" = "V2" ] && ata_start="1"

[ "$form_voip_start" = "0" ] && voip_start="0"
[ "$form_voip_start" = "1" ] && voip_start="1"

 if [ "$ata_start" = "1" ]; then
   [ "$reg" = "" ] && errmsg="$errmsg Registrar: Invalid address!"'\n' && err=1
 fi

 [ "$regport" = "" ] && regport=5060
 ( [ "$regport" -lt "0" ] || [ "$regport" -gt "65535" ] ) && regport="" && errmsg="$errmsg Registrar: Invalid Port!"'\n' && err=1
 
 [ "$proxport" = "" ] && proxport=5060
 ( [ "$proxport" -lt "0" ] || [ "$proxport" -gt "65535" ] ) &&  proxport="" && errmsg="$errmsg Proxy: Invalid Port!"'\n' && err=1

 http_unquote_all domain
 domain=`echo "$domain"|sed -n -e 's/\([^A-Za-z0-9.-]\)*//gp'`
 http_unquote_all ext1
 http_unquote_all uname
 http_unquote_all pass

 [ "$vad" = "on" ] && vad="1" || vad="0"
 [ "$bovc" = "on" ] && bovc="1" || bovc="0"
 [ "$dsl_wait" = "on" ] && dsl_wait="1" || dsl_wait="0"

 if [ "$err" -eq "0" ]; then
 err=0
 [ "$ata_start" != "`nvram get ata_start`" ] && mustsave=1 && res=`nvram set ata_start=$ata_start` && [ "$res" != "OK" ] && err=1

 [ "$voip_start" != "`nvram get voip_start`" ] && mustsave=1 && res=`nvram set voip_start=$voip_start` && [ "$res" != "OK" ] && err=1

 [ "$reg" != "`nvram get ata_reg`" ] && mustsave=1 && res=`nvram set ata_reg=$reg` && [ "$res" != "OK" ] && err=1
 [ "$prox" != "`nvram get ata_prox`" ] && mustsave=1 && res=`nvram set ata_prox=$prox` && [ "$res" != "OK" ] && err=1
 [ "$regport" != "`nvram get ata_regport`" ] && mustsave=1 && res=`nvram set ata_regport=$regport` && [ "$res" != "OK" ] && err=1
 [ "$proxport" != "`nvram get ata_proxport`" ] && mustsave=1 && res=`nvram set ata_proxport=$proxport` && [ "$res" != "OK" ] && err=1
 [ "$pass" != "`nvram get ata_pass`" ] && mustsave=1 && res=`nvram set ata_pass=$pass` && [ "$res" != "OK" ] && err=1
 [ "$uname" != "`nvram get ata_uname`" ] && mustsave=1 && res=`nvram set ata_uname=$uname` && [ "$res" != "OK" ] && err=1
 [ "$domain" != "`nvram get ata_domain`" ] && mustsave=1 && res=`nvram set ata_domain=$domain` && [ "$res" != "OK" ] && err=1
 [ "$ext1" != "`nvram get ata_ext1`" ] && mustsave=1 && res=`nvram set ata_ext1=$ext1` && [ "$res" != "OK" ] && err=1
 [ "$vad" != "`nvram get ata_vad`" ] && mustsave=1 && res=`nvram set ata_vad=$vad` && [ "$res" != "OK" ] && err=1
 [ "$bovc" != "`nvram get ata_bovc`" ] && mustsave=1 && res=`nvram set ata_bovc=$bovc` && [ "$res" != "OK" ] && err=1
 [ "$country" != "`nvram get voip_country`" ] && mustsave=1 && res=`nvram set voip_country=$country` && [ "$res" != "OK" ] && err=1
 [ "$dsl_wait" != "`nvram get voip_dsl_wait`" ] && mustsave=1 && res=`nvram set voip_dsl_wait=$dsl_wait` && [ "$res" != "OK" ] && err=1

 [ "$err" -ne 0 ] && errmsg="$errmsg Write to NVRam failed!"'\n'
 fi
 [ "$mustsave" -eq 1 ] && nvram commit >/dev/null && /etc/start_scripts/ata.sh restart >/dev/null 2>/dev/null &
fi
#############


OK='checked="checked"'
ata_start=`nvram get ata_start`
ata_on=''
ata_off=$OK
if [ "$ata_start" = "Failed" ]; then
  ata_start="0"
  ata_off=$OK
  ata_on=''
fi
[ "$ata_start" -eq 1 ] && ata_on=$OK && ata_off=''
[ "$ata_start" -ne 1 ] && ata_on='' && ata_off=$OK

voip_start=`nvram get voip_start`
[ "$voip_start" = "Failed" ] && voip_start=0
voip_start_on=""
voip_start_off=""
[ $voip_start -eq 1 ] && voip_start_on=$OK
[ $voip_start -eq 0 ] && voip_start_off=$OK

ata_ext1=`nvram get ata_ext1`
[ "$ata_ext1" = "Failed" ] && ata_ext1=""

ata_uname=`nvram get ata_uname`
[ "$ata_uname" = "Failed" ] && ata_uname=""

ata_pass=`nvram get ata_pass`
[ "$ata_pass" = "Failed" ] && ata_pass=""

ata_reg=`nvram get ata_reg`
[ "$ata_reg" = "Failed" ] && ata_reg=""

ata_regport=`nvram get ata_regport`
[ "$ata_regport" = "Failed" ] && ata_regport="5060"

ata_prox=`nvram get ata_prox`
[ "$ata_prox" = "Failed" ] && ata_prox="$ata_reg"

ata_proxport=`nvram get ata_proxport`
[ "$ata_proxport" = "Failed" ] && ata_proxport="5060"

ata_domain=`nvram get ata_domain`
[ "$ata_domain" = "Failed" ] && ata_domain=""

ata_vad=`nvram get ata_vad`
[ "$ata_vad" = "Failed" ] && ata_vad=0 && vad_on="" 
[ "$ata_vad" -eq "1" ] && vad_on='checked="checked"' || vad_on=""

ata_bovc=`nvram get ata_bovc`
[ "$ata_bovc" = "Failed" ] && ata_bovc=0 && bovc_on="" 
[ "$ata_bovc" -eq "1" ] && bovc_on='checked="checked"' || bovc_on=""


SEL="selected"
voip_country=`nvram get voip_country`
[ "$voip_country" = "Failed" ] && voip_country="DEU"
[ "$voip_country" = "DEU" ] && cntry_sel_deu=$SEL
[ "$voip_country" = "FRA" ] && cntry_sel_fra=$SEL
[ "$voip_country" = "NLD" ] && cntry_sel_nld=$SEL
[ "$voip_country" = "BEL" ] && cntry_sel_bel=$SEL
[ "$voip_country" = "GBR" ] && cntry_sel_gbr=$SEL
[ "$voip_country" = "SWE" ] && cntry_sel_swe=$SEL
[ "$voip_country" = "FIN" ] && cntry_sel_fin=$SEL
[ "$voip_country" = "ETS" ] && cntry_sel_ets=$SEL
[ "$voip_country" = "ITA" ] && cntry_sel_ita=$SEL
[ "$voip_country" = "USA" ] && cntry_sel_usa=$SEL
[ "$voip_country" = "JPN" ] && cntry_sel_jpn=$SEL
[ "$voip_country" = "CHN" ] && cntry_sel_chn=$SEL

voip_dsl_wait=`nvram get voip_dsl_wait`
[ "$voip_dsl_wait" = "Failed" ] && voip_dsl_wait=0 && dsl_wait_on="" 
[ "$voip_dsl_wait" -eq "1" ] && dsl_wait_on='checked="checked"' || voip_dsl_wait=""


cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>

<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function do_pass_show(state)
{
  if(state) {
    document.getElementById("pass").type="text";
  }
  else {
    document.getElementById("pass").type="password";
  }
}
function showErr() {
var err="$errmsg";
if(err!=""){alert(err);} }
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>$f_pfad</DIV>
<form id="xform" name="xform" method="post">
<DIV id=c_titel>Phone</DIV>
<DIV id=c_leer>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(10) onmouseout=i_showElem(0) >
<td width="20"><input type="radio" name="form_voip_start" value="0" $voip_start_off></td>
<td width="40">Off</td>
<td width="20"><input type="radio" name="form_voip_start" value="1" $voip_start_on></td>
<td>On</td>
</tr></table>
</DIV>
<DIV id=c_titel>$f_name</DIV>
<DIV id=c_leer>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0) >
<input id="action" type="hidden" name="action" value="0">
<td width="20"><input type="radio" name="R1" value="V1" $ata_off></td>
<td width="40">Off</td>
<td width="20"><input type="radio" name="R1" value="V2" $ata_on></td>
<td>On</td>
</tr></table>
</DIV>
<DIV id=c_titel>SIP account information</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0) >
<td width="180">Subscriber:</td>
<td><input id="ext1" name="ext1" size="25" maxlength="50" type="text"
 value="$ata_ext1"></td>
</td></tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0) >
<td width="180">User name:</td>
<td><input id="uname" name="uname" size="25" maxlength="50" type="text" value="$ata_uname"></td>
</td></tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0) >
<td width="180">Password:</td>
<td><input id="pass" name="pass" size="25" maxlength="20" type="password" value="$ata_pass"></td>
<td><input id="pass_show" type="checkbox" onchange="do_pass_show(this.checked);">show</td></tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(5) onmouseout=i_showElem(0) >
<td width="180">Registrar Address:</td>
<td><input id="reg" name="reg" size="25" maxlength="64" type="text" value="$ata_reg"></td>
</td></tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(6) onmouseout=i_showElem(0) >
<td width="180">Registrar Port:</td>
<td><input id="regport" name="regport" size="6" maxlength="5" type="text" value="$ata_regport"></td>
</td></tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(13) onmouseout=i_showElem(0) >
<td width="180">Proxy IP:</td>
<td><input id="prox" name="prox" size="25" maxlength="64" type="text" value="$ata_prox"></td>
</td></tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(14) onmouseout=i_showElem(0) >
<td width="180">Proxy Port:</td>
<td><input id="proxport" name="proxport" size="6" maxlength="5" type="text" value="$ata_proxport"></td>
</td></tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(7) onmouseout=i_showElem(0) >
<td width="180">Domain:</td>
<td><input id="domain" name="domain" size="25" maxlength="64" type="text" value="$ata_domain"></td>
</td></tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(8) onmouseout=i_showElem(0) >
<td width="280">Voice Activity Detection (VAD):</td>
<td><input id="vad" name="vad" type="checkbox" $vad_on></td>
</td></tr></table></DIV>
<DIV id=c_last>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(9) onmouseout=i_showElem(0) >
<td width="280">Bandwidth Optimized Voice Compression:</td>
<td><input id="bovc" name="bovc" type="checkbox" $bovc_on></td>
</td></tr></table></DIV>
<DIV id=c_leer></DIV>
<DIV id=c_titel>Operational modes</DIV>
<div id="c_std">
<table border="0" cellpadding="4" cellspacing="0">
<tr onmouseover=i_showElem(11) onmouseout=i_showElem(0)><td width="170">Country:</td><td>
<select name="country">
<option value="DEU" $cntry_sel_deu>Germany</option>
<option value="FRA" $cntry_sel_fra>France</option>
<option value="NLD" $cntry_sel_nld>Netherlands</option>
<option value="BEL" $cntry_sel_bel>Belgium</option>
<option value="GBR" $cntry_sel_gbr>Great Britain</option>
<option value="SWE" $cntry_sel_swe>Sweden</option>
<option value="FIN" $cntry_sel_fin>Finland</option>
<option value="ETS" $cntry_sel_ets>ETS</option>
<option value="ITA" $cntry_sel_ita>Italy</option>
<option value="USA" $cntry_sel_usa>United States of America</option>
<option value="JPN" $cntry_sel_jpn>Japan</option>
<option value="CHN" $cntry_sel_chn>China</option>
</select>
</td></tr></table>
</div>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(12) onmouseout=i_showElem(0) >
<td width="175">DSL wait:</td>
<td><input id="dsl_wait" name="dsl_wait" type="checkbox" $dsl_wait_on></td>
</td></tr></table></DIV>
<DIV id=c_leer></DIV>
</form>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Phone settings</h2>
<p>Setup the VoIP configuration.</p>
</DIV>
<DIV id=i_content1>
<h2>Analog telephone adapter</h2>
<p>Turn the ATA <b>on</b> or <b>off</b>.</p>
<p>Analog telephone adapter (ATA) for using telephone capabilities without DSL-connection.</p>
</DIV>
<DIV id=i_content2>
<h2>Subscriber</h2>
<p>Enter here the telephone-subscriber part of the SIP URI.</p>
<p>E.g. sip:<b>subscriber</b>@domain</p>
<p> sip:<b>7684352</b>@sipgate.de</p>
<p> sip:<b>7684352</b>@t-tel.t-online.de</p>
<p> sip:<b>name.name</b>@freenet.de</p>
</DIV>
<DIV id=i_content3>
<h2>Username</h2>
<p>Enter your SIP-username</p>
</DIV>
<DIV id=i_content4>
<h2>Password</h2>
<p>Enter your SIP-password</p>
</DIV>
<DIV id=i_content5>
<h2>Registrar Address</h2>
<p>Enter the IP address or hostname of your SIP-Registrar-Server.</p>
</DIV>
<DIV id=i_content6>
<h2>Registrar-Port</h2>
<p>Enter the UDP-Port of your SIP-Registrar server. Default-Port: 5060</p>
</DIV>
<DIV id=i_content7>
<h2>Domain</h2>
<p>Enter your SIP-Domain.</p>
</DIV>
<DIV id=i_content8>
<h2>Voice Activity Detection (VAD)</h2>
<p>Turn VAD <b>on</b> or <b>off</b>.</p>
</DIV>
<DIV id=i_content9>
<h2>Bandwidth Optimized Voice Compression</h2>
<p>Turn Compression <b>on</b> or <b>off</b>.</p>
</DIV>
<DIV id=i_content10>
<h2>Phone</h2>
<p>Turn the phone capabilities of the router <b>on</b> or <b>off</b>.</p>
</DIV>
<DIV id=i_content11>
<h2>Country</h2>
<p>Select your country. This option has influence to the phone signal tones.</p>
</DIV>
<DIV id=i_content12>
<h2>DSL wait</h2>
<p>If DSL is enabled, wait until the DSL and PPP connection is established before
starting the VoIP connection.</p>
</DIV>
<DIV id=i_content13>
<h2>Proxy Address</h2>
<p>Enter the IP address or hostname of your SIP-Proxy for outbound calls.</p>
</DIV>
<DIV id=i_content14>
<h2>Proxy-Port</h2>
<p>Enter the UDP-Port of your SIP-Proxy for outbound calls. Default-Port: 5060</p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML



