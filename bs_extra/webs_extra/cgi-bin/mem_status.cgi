#!/bin/sh

. ./functions.sh
HOME="status_index.cgi"

df -h|grep "/dev/mtdblock/3" >/dev/null 2>/dev/null
if [ "$?" = "0" ]; then
  j_size=`df -h|grep "/dev/mtdblock/3"|awk '{print $2;}'`
  j_used=`df -h|grep "/dev/mtdblock/3"|awk '{print $3;}'`
  j_free=`df -h|grep "/dev/mtdblock/3"|awk '{print $4;}'`
  j_per=`df -h|grep "/dev/mtdblock/3"|awk '{gsub("%",""); print $5;}'`
else
  no_jffs2=1
fi

mem_total=`free |grep Mem|awk '{print $2;}'`
mem_used=`free |grep Mem|awk '{print $3;}'`
mem_free=`free |grep Mem|awk '{print $4;}'`
mem_shared=`free |grep Mem|awk '{print $5;}'`
mem_buffers=`free |grep Mem|awk '{print $6;}'`
let mem_per=$mem_used*100
let mem_per=$mem_per/$mem_total

nvram_use=`nvram show|grep "table_size="|sed -e 's/table_size=//'`
nvram_comp_enabled="disabled"
nvram_comp_size=`nvram compression info|grep "compressed_table_size="|sed -e 's/compressed_table_size=//'`
if [ "$nvram_comp_size" != "" ]; then
 nvram_uncomp_size=$nvram_use
 nvram_use=$nvram_comp_size
 nvram_comp_ratio=`nvram compression info|grep "compression_ratio="|sed -e 's/compression_ratio=//'`
 nvram_comp_enabled="enabled"
fi

nvram_total=32767
let nvram_free=$nvram_total-$nvram_use
let nvram_per=$nvram_use*100
let nvram_per=$nvram_per/$nvram_total



u_1=`uptime|awk '{ gsub("[,]","",$7); print $7*100;}'`
u_5=`uptime|awk '{ gsub("[,]","",$8); print $8*100;}'`
u_15=`uptime|awk '{ gsub("[,]","",$9); print $9*100;}'`

cat << EOF_HTML 
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt"type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY>
<DIV id=c_border>
<DIV id=c_pfad>Status / System utilization</DIV>
<DIV id=c_titel>Memory</DIV>
<table border="0" cellpadding="1" cellspacing="2">
<tr><td width="10"></td><td width="100">Size:</td><td>$mem_total kByte</td></tr>
<tr><td width="10"></td><td width="100">Used:</td><td>$mem_used kByte</td><td width="20"></td><td>
EOF_HTML
HBar 200 20 $mem_per
cat << EOF_HTML 
</td></tr>
<tr><td width="10"></td><td width="100">Free:</td><td>$mem_free kByte</td></tr>
<tr><td width="10"></td><td width="100">Shared:</td><td>$mem_shared kByte</td></tr>
<tr><td width="10"></td><td width="100">Buffers:</td><td>$mem_buffers kByte</td></tr>
</table>
<div id=c_leer></div>
<DIV id=c_titel>NVRAM</DIV>
<table border="0" cellpadding="1" cellspacing="2">
<tr><td width="10"></td><td width="100">Size:</td><td>$nvram_total Byte</td></tr>
<tr><td width="10"></td><td width="100">Used:</td><td>$nvram_use Byte</td>
<td width="20"></td><td>
EOF_HTML
HBar 200 20 $nvram_per
cat << EOF_HTML 
</tr>
<tr><td width="10"></td><td width="100">Free:</td><td>$nvram_free Byte</td></tr>
<tr><td width="10"></td><td width="100">Compression:</td><td>$nvram_comp_enabled</td></tr>
EOF_HTML
if [ "$nvram_comp_size" != "" ]; then
cat << EOF_HTML
<tr><td width="10"></td><td width="100">Uncomp. size:</td><td>$nvram_uncomp_size Byte</td></tr>
<tr><td width="10"></td><td width="100">Comp. ratio:</td><td>$nvram_comp_ratio</td></tr>
EOF_HTML
fi

cat << EOF_HTML
</table>
<div id=c_leer></div>
<DIV id=c_titel>JFFS2 partition</DIV>
EOF_HTML
if [ "$no_jffs2" = "1" ]; then
  echo "<div id=c_std>JFFS2 partition not available</div>"
else
cat << EOF_HTML
<table border="0" cellpadding="1" cellspacing="2">
<tr><td width="10"></td><td width="100">Size:</td><td>${j_size}Byte</td></tr>
<tr><td width="10"></td><td width="100">Used:</td><td>${j_used}Byte</td>
<td width="20"></td><td>
EOF_HTML
HBar 200 20 $j_per
cat << EOF_HTML 
</tr>
<tr><td width="10"></td><td width="100">Available:</td><td>${j_free}Byte</td></tr>
</table>
EOF_HTML
fi

cat << EOF_HTML
<div id=c_leer></div>
<DIV id=c_titel>CPU load average</DIV>
<table border="0" cellpadding="1" cellspacing="2">
<tr><td width="10"></td><td width="100">last 1 minute:</td><td>
EOF_HTML
HBar 200 20 $u_1 "+"
cat << EOF_HTML 
</td></tr>
<tr><td width="10"></td><td width="100">last 5 minutes:</td><td>
EOF_HTML
HBar 200 20 $u_5 "+"
cat << EOF_HTML 
</td></tr>
<tr><td width="10"></td><td width="100">last 15 minutes:</td><td>
EOF_HTML
HBar 200 20 $u_15 "+"
cat << EOF_HTML 
</td></tr>
</table>
<DIV id=c_foot></DIV>
</div>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>System utilization</h2>
<p></p>
</DIV></DIV>
</BODY></HTML>
EOF_HTML

a=0
