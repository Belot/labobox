#!/bin/sh

. ./login.cgi

PATH=".:"$PATH

IP=`ifconfig br0|grep "inet addr"|sed -n -e 's/^.*addr:\([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\).*Bcast.*/\1/p'`
MASK=`ifconfig br0|grep "inet addr"|sed -n -e 's/^.*Mask:\([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\).*/\1/p'`
DEFAULT=`route|grep 'default'|grep br0|sed -n -e 's/default\W*\([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\).*[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*.*/\1/p'`

GW=$DEFAULT
if [ "$DEFAULT" = "" ]; then
GW=$IP
fi

DHCPHOSTS=`nvram get dhcphosts`
[ "$DHCPHOSTS" = "Failed" ] && DHCPHOSTS=""

err=0
########################### Add #####################################
if [ "$action" -eq 10 ]; then
  hostmac=`echo "$hostmac"|sed -e 's/%3A/:/g'|sed -e 's/%3a/:/g'`
  hostmac=`echo "$hostmac"|sed -n -e 's/\([0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]\)/\1/p'`
  [ "$hostmac" = "" ] && errmsg="Invalid MAC address!"'\n' && err=1

  if [ "$hostip" != "" ]; then
  hostip=`isip.sh $hostip`
  [ "$hostip" = "ERROR" ] && hostip="" && errmsg="$errmsg Invalid IP address!"'\n' && err=1
  fi

  hostname=`echo "$hostname"|sed -n -e 's/\([^A-Za-z0-9\-]\)*//gp'`

  [ "$hostip" = "" ] && [ "$hostname" = "" ] && errmsg="$errmsg You must specify a IP adresses and/or a Hostname!"'\n' && err=1

 if [ "$err" -eq 0 ]; then
 DHCPHOSTS=$DHCPHOSTS"$hostmac"
 [ "$hostname" != "" ] && DHCPHOSTS=$DHCPHOSTS",$hostname"
 [ "$hostip" != "" ] && DHCPHOSTS=$DHCPHOSTS",$hostip"
 DHCPHOSTS=$DHCPHOSTS"~"
 res=`nvram set dhcphosts=$DHCPHOSTS`
 nvram commit
 [ "$res" != "OK" ] && errmsg="$errmsg Write to NVRam failed!"'\n'

 # duplikat check
 dup=`echo $DHCPHOSTS|sed -e 's/~/\n/g'|cut -d ',' -f 1|sort|awk 'BEGIN{h=""}{if(h==""){h=$1; next;} if(h==$1){print $1;} h=$1;}'|tr  '\n' ' '`
 [ "$dup" != "" ] && errmsg="$errmsg Warning duplicate MAC-Address: $dup"'\n'
 dup=`echo $DHCPHOSTS|sed -e 's/~/\n/g'|cut -d ',' -f 2|sort|awk 'BEGIN{h=""}{if(h==""){h=$1; next;} if(h==$1){print $1;} h=$1;}'|tr  '\n' ' '`
 [ "$dup" != "" ] && errmsg="$errmsg Warning duplicate Hostname: $dup"'\n'
 dup=`echo $DHCPHOSTS|sed -e 's/~/\n/g'|cut -d ',' -f 3|sort|awk 'BEGIN{h=""}{if(h==""){h=$1; next;} if(h==$1){print $1;} h=$1;}'|tr  '\n' ' '`
 [ "$dup" != "" ] && errmsg="$errmsg Warning duplicate IP-Address: $dup"'\n'

 hostip=""
 hostname=""
 hostmac=""
 fi
fi


################################## Del #####################################
if [ "$action" -eq 11 ] && [ "$select" != "" ]; then
 X=`echo "$select"|sed -e 's/%3A/:/g'|sed -e 's/%3a/:/g'`
 DHCPHOSTS=`echo $DHCPHOSTS|sed -e "s/$X\\(,[a-zA-Z0-9\\-]*\\)\\(,[0-9.]*\\)~//"|sed -e "s/$X\\(,[a-zA-Z0-9\\-]*\\)~//"|sed -e "s/$X\\(,[0-9.]*\\)~//"`
 res=`nvram set dhcphosts=$DHCPHOSTS`
 [ "$res" != "OK" ] && errmsg="$errmsg Write to NVRam failed!"'\n' || nvram commit
fi

############################## Save & Run ##################################
if [ "$action" -eq 3 ]; then
  [ "$R1" = "V1" ] && dhcp_start="0"
  [ "$R1" = "V2" ] && dhcp_start="1"

  [ "$sel_lease" = "h" ] && dhcp_lease_t="h" || dhcp_lease_t="m"

  dhcp_lease=`echo $lease|sed -e 's/\([^0-9]\)//g'`
  [ "$ch_infinity" = "1" ] && dhcp_lease="inf"

  fromip=`isip.sh $fromip`
  [ "$fromip" = "ERROR" ] && fromip="" && errmsg="$errmsg IP-Range \"from\": IP address was invalid!"'\n' && err=1

  toip=`isip.sh $toip`
  [ "$toip" = "ERROR" ] && toip="" && errmsg="$errmsg IP-Range \"to\": IP address was invalid!"'\n' && err=1

  if [ "$gateway" != "" ]; then
  gateway=`isip.sh $gateway`
  [ "$gateway" = "ERROR" ] && gateway="" && errmsg="$errmsg Gateway: IP address was invalid!"'\n' && err=1
  fi

  if [ "$dns1" != "" ]; then
  dns1=`isip.sh $dns1`
  [ "$dns1" = "ERROR" ] && dns1="" && errmsg="$errmsg DNS 1: IP address was invalid!"'\n' && err=1
  fi
  if [ "$dns2" != "" ]; then
  dns2=`isip.sh $dns2`
  [ "$dns2" = "ERROR" ] && dns2="" && errmsg="$errmsg DNS 2: IP address was invalid!"'\n' && err=1
  fi
  if [ "$dns3" != "" ]; then
  dns3=`isip.sh $dns3`
  [ "$dns3" = "ERROR" ] && dns3="" && errmsg="$errmsg DNS 3: IP address was invalid!"'\n' && err=1
  fi
  if [ "$dns4" != "" ]; then
  dns4=`isip.sh $dns4`
  [ "$dns4" = "ERROR" ] && dns4="" && errmsg="$errmsg DNS 4: IP address was invalid!"'\n' && err=1
  fi

  if [ $dhcp_static == "on" ]; then
    dhcp_static=1
  else
    dhcp_static=0
  fi

  if [ "$err" -eq 0 ]; then
  err=0
  mustsave=0
  [ "$dhcp_start" != "`nvram get dhcp_start`" ] && res=`nvram set dhcp_start=$dhcp_start` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$dhcp_lease_t" != "`nvram get dhcp_lease_t`" ] && res=`nvram set dhcp_lease_t=$dhcp_lease_t` && mustsave=1  && [ "$res" != "OK" ] && err=1
  [ "$dhcp_lease" != "`nvram get dhcp_lease`" ] && res=`nvram set dhcp_lease=$dhcp_lease` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$fromip" != "`nvram get dhcp_fromip`" ] && res=`nvram set dhcp_fromip=$fromip` && mustsave=1  && [ "$res" != "OK" ] && err=1
  [ "$toip" != "`nvram get dhcp_toip`" ] && res=`nvram set dhcp_toip=$toip` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$gateway" != "`nvram get dhcp_gateway`" ] && res=`nvram set  dhcp_gateway=$gateway` && mustsave=1  && [ "$res" != "OK" ] && err=1
  [ "$dns1" != "`nvram get dhcp_dns1`" ] && res=`nvram set dhcp_dns1=$dns1` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$dns2" != "`nvram get dhcp_dns2`" ] && res=`nvram set dhcp_dns2=$dns2` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$dns3" != "`nvram get dhcp_dns3`" ] && res=`nvram set dhcp_dns3=$dns3` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$dns4" != "`nvram get dhcp_dns4`" ] && res=`nvram set dhcp_dns4=$dns4` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$dhcp_static" != "`nvram get dhcp_static`" ] && res=`nvram set dhcp_static=$dhcp_static` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$mustsave" -eq 1 ] && nvram commit >/dev/null 
  [ "$err" -ne 0 ] && errmsg="$errmsg Write to NVRam failed!"'\n'

  /etc/start_scripts/dhcp_dns.sh restart >/dev/null 2>/dev/null
  fi

fi


dhcp_start=`nvram get dhcp_start`
dhcp_on=''
dhcp_off='checked="checked"'
if [ "$dhcp_start" = "Failed" ]; then
  dhcp_start="1"
  dhcp_on='checked="checked"'
  dhcp_off=''
fi
[ "$dhcp_start" -eq 1 ] && dhcp_on='checked="checked"' && dhcp_off=''
[ "$dhcp_start" -ne 1 ] && dhcp_on='' && dhcp_off='checked="checked"'

if [ `nvram get dhcp_lease_t` != "m" ]; then
 dhcp_lease_h='selected="selected"'
 dhcp_lease_m=''
else
 dhcp_lease_h=''
 dhcp_lease_m='selected="selected"'
fi

ch_infinity=""
lease=`nvram get dhcp_lease`
[ "$lease" = "Failed" ] && lease=12 && ch_infinity=""
[ "$lease" = "inf" ] && ch_infinity='checked="checked"' && lease=""

fromip=`nvram get dhcp_fromip`
[ "$fromip" = "Failed" ] && fromip=""
toip=`nvram get dhcp_toip`
[ "$toip" = "Failed" ] && toip=""

gateway=`nvram get dhcp_gateway`
[ "$gateway" != "Failed" ] && GW=$gateway
dns1=`nvram get dhcp_dns1`
[ "$dns1" = "Failed" ] && dns1=""
dns2=`nvram get dhcp_dns2`
[ "$dns2" = "Failed" ] && dns2=""
dns3=`nvram get dhcp_dns3`
[ "$dns3" = "Failed" ] && dns3=""
dns4=`nvram get dhcp_dns4`
[ "$dns4" = "Failed" ] && dns4=""

dhcp_static=`nvram get dhcp_static`
check_static='checked="checked"'
[ "$dhcp_static" = "Failed" ] && dhcp_static=0
[ "$dhcp_static" = "0" ] && check_static=""

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
EOF_HTML
echo '<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>' \
'<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>' \
'<SCRIPT type="text/JavaScript">' \
'function subm(x) {' \
'var lease=document.getElementById("lease").value;' \
'var fromip=document.getElementById("fromip").value;' \
'var toip=document.getElementById("toip").value;' \
"var subnet='$MASK';" \
"var ip='$IP';" \
'var gateway=document.getElementById("gateway").value;' \
'var dns1=document.getElementById("dns1").value;' \
'var dns2=document.getElementById("dns2").value;' \
'var dns3=document.getElementById("dns3").value;' \
'var dns4=document.getElementById("dns4").value;' \
'var msg=""; ' \
'if((isInvalidNumber(lease)!=false || lease==0)&&(document.getElementById("ch_infinity").checked==false)){alert("Lease is invalid!"); return;}' \
'if(!isValidIpAddress(fromip)){alert("IP-Range \"from\": IP address is invalid!");return;}' \
'if(!isValidIpAddress(toip)){alert("IP-Range \"to\": IP address is invalid!");return;}' \
'if(!isSameSubNet(ip,subnet,fromip,subnet)){msg+="IP-Range \"from\": IP is in wrong subnet!\n";}' \
'if(!isSameSubNet(ip,subnet,toip,subnet)){msg+="IP-Range \"to\": IP is in wrong subnet!\n";}' \
'if(gateway!="" && !isValidIpAddress(gateway)){alert("Gateway: IP address is invalid!");return;}' \
'if(gateway!="" && !isSameSubNet(ip,subnet,gateway,subnet)){msg+="Gateway: IP is in wrong subnet!\n";}' \
'if(dns1!="" && !isValidIpAddress(dns1)){alert("DNS 1: IP address is invalid!");return;}' \
'if(dns2!="" && !isValidIpAddress(dns2)){alert("DNS 2: IP address is invalid!");return;}' \
'if(dns3!="" && !isValidIpAddress(dns3)){alert("DNS 3: IP address is invalid!");return;}' \
'if(dns4!="" && !isValidIpAddress(dns4)){alert("DNS 4: IP address is invalid!");return;}' \
'if(msg!=""){' \
'  msg+="\nContinue and save or abort?";' \
'  check=confirm("WARNING:\n" + msg);' \
'  if(check!=true)' \
'   return;' \
'}' \
'load();' \
'document.getElementById("action").value=x;' \
'document.getElementById("xform").submit();' \
'}' \
'function js_lease(state) {' \
'if(state) {' \
'document.getElementById("lease").setAttribute("readonly","readonly");}' \
'else { document.getElementById("lease").removeAttribute("readonly");}' \
'}' \
'function doAdd() {' \
"var subnet='$MASK';" \
"var ip='$IP';" \
'var hostmac=document.getElementById("hostmac").value;' \
'var hostip=document.getElementById("hostip").value;' \
'var hostname=document.getElementById("hostname").value;' \
'if(!isValidMacAddress(hostmac)){alert("MAC: invalid MAC address!");return;}' \
'if(hostip!="" && !isValidIpAddress(hostip)){alert("IP address is invalid!");return;}' \
'if(hostip!="" && !isSameSubNet(ip,subnet,hostip,subnet)){alert("IP is in wrong subnet!");return;}' \
'if(hostname!="" && !isNumorCharwiDash(hostname)){alert("Invalid Hostname!");return;}' \
'if(hostip=="" && hostname==""){alert("You must specify a IP adresses and/or a Hostname!");return;}' \
'load();' \
'document.getElementById("action").value=10;' \
'document.getElementById("xform").submit();' \
'}' \
'function doDel() {' \
'load();' \
'document.getElementById("action").value=11;' \
'document.getElementById("xform").submit();' \
'}' \
'function showErr() {' \
"var err=\"$errmsg\";" \
'if(err!=""){alert(err);} }' \
'</SCRIPT>' \
'<LINK rel="stylesheet" href="../style.css" type="text/css">' \
'<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->' \
'</HEAD>' \
'<BODY onload="showErr();">' \
'<DIV id=c_Frame>' \
'<DIV id=c_border>' \
'<DIV id=c_pfad>Network settings / DHCP</DIV>' \
'<DIV id=c_titel>DHCP-Server</DIV>' \
'<form id="xform" name="xform" method="post">' \
'<DIV id=c_leer>' \
'<table border="0" cellpadding="0" cellspacing="2">' \
'<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0) >' \
'<input id="action" type="hidden" name="action" value="0">' \
"<input type=\"hidden\" name=\"session_id\" value=\"$session_id\">" \
"<td width=\"20\"><input type=\"radio\" name=\"R1\" value=\"V1\" $dhcp_off></td>" \
'<td width="40">Off</td>' \
"<td width=\"20\"><input type=\"radio\" name=\"R1\" value=\"V2\" $dhcp_on></td>" \
'<td>On</td>' \
'</tr></table>' \
'</DIV>' \
'<DIV id=c_titel>DHCP Server Options</DIV>' \
'<DIV id=c_std>' \
'<table border="0" cellpadding="0" cellspacing="2">' \
'<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0) >' \
'<td width="60">Lease:</td>' \
"<td><input id=\"lease\" name=\"lease\" size=\"5\" maxlength=\"3\" type=\"text\" value=\"$lease\"></td>" \
"<td><select name=\"sel_lease\" size=\"1\" $dhcp_lease_h><option value=\"h\">Hours</option><option value=\m\" $dhcp_lease_m>Minutes</option></select></td><td width=\"20\"></td>" \
"<td><input type=\"checkbox\" id=\"ch_infinity\"name=\"ch_infinity\" value=\"1\" onchange=\"js_lease(this.checked);\" $ch_infinity> Infinity" \
'</td></tr></table></DIV>' \
'<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2">' \
'<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0) >' \
'<td width="50">Range</td><td width="40">from:</td>' \
"<td><input type=\"text\" id=\"fromip\" name=\"fromip\" size=\"16\" maxlength=\"15\" value=\"$fromip\"></td>" \
'<td width="25">to:</td>' \
"<td><input type=\"text\" id=\"toip\" name=\"toip\" size=\"16\" maxlength=\"15\" value=\"$toip\"></td>" \
'</tr></table></DIV>' \
'<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2">' \
'<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0) >' \
'<td width="60">Gateway:</td>' \
"<td><input type=\"text\" id=\"gateway\" name=\"gateway\" size=\"16\" maxlength=\"15\" value=\"$GW\"></td>" \
'</tr></table></DIV>' \
'<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2">' \
'<tr onmouseover=i_showElem(5) onmouseout=i_showElem(0) >' \
'<td width="60">DNS 1:</td>' \
"<td><input type=\"text\" id=\"dns1\" name=\"dns1\" size=\"16\" maxlength=\"15\" value=\"$dns1\"></td>" \
'<td width="50">DNS 2:</td>' \
"<td><input type=\"text\" id=\"dns2\" name=\"dns2\" size=\"16\" maxlength=\"15\" value=\"$dns2\"></td>" \
'</tr></table></DIV>' \
'<DIV id=c_last><table border="0" cellpadding="0" cellspacing="2">' \
'<tr onmouseover=i_showElem(5) onmouseout=i_showElem(0) >' \
'<td width="60">DNS 3:</td>' \
"<td><input type=\"text\" id=\"dns3\" name=\"dns3\" size=\"16\" maxlength=\"15\" value=\"$dns3\"></td>" \
'<td width="50">DNS 4:</td>' \
"<td><input type=\"text\" id=\"dns4\" name=\"dns4\" size=\"16\" maxlength=\"15\" value=\"$dns4\"></td>" \
'</tr></table></DIV>' \
'<DIV id=c_leer></DIV>' \
'<DIV id=c_titel>Known Hosts</DIV>' \
'<DIV id=c_last>' \
'<table border="0" cellpadding="0" cellspacing="2">' \
'<tr onmouseover=i_showElem(7) onmouseout=i_showElem(0) >' \
'<td width="40">MAC:</td>' \
"<td><input id=\"hostmac\" name=\"hostmac\" size=\"22\" maxlength=\"17\" type=\"text\" value=\"$hostmac\"></td>" \
'</tr></table></DIV>' \
'<DIV id=c_last>' \
'<table border="0" cellpadding="0" cellspacing="2">' \
'<tr onmouseover=i_showElem(8) onmouseout=i_showElem(0) >' \
'<td width="40">IP:</td>' \
"<td><input id=\"hostip\" name=\"hostip\" size=\"22\" maxlength=\"15\" type=\"text\" value=\"$hostip\"></td>" \
'</tr></table></DIV>' \
'<DIV id=c_leer>' \
'<table border="0" cellpadding="0" cellspacing="2">' \
'<tr onmouseover=i_showElem(9) onmouseout=i_showElem(0) >' \
'<td width="9"></td><td width="40">Name:</td>' \
"<td><input id=\"hostname\" name=\"hostname\" size=\"22\" maxlength=\"20\" type=\"text\" value=\"$hostname\"></td>" \
'<td width="120"></td><td><input type="button" name="add" value="Add" onclick="doAdd();"></td>' \
'</tr></table></DIV>' \
'<DIV id=c_last>' \
'<table border="0" cellpadding="0" cellspacing="2">' \
'<tr onmouseover=i_showElem(6) onmouseout=i_showElem(0) >' \
'<td width="40">Host:</td>' \
'<td><select name="select" size="1" style="width:340px">' 
echo $DHCPHOSTS|sed -e 's/~/\n/g'|sed -n -e '/^$/!p'|sed -e 's/\(^[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]\)\(.*\)/<option value="\1">\1\2<\/option>/'
echo '</select></td></tr></table></DIV>' \
'<DIV id=c_last><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(6) onmouseout=i_showElem(0) ><td width=335></td>' \
'<td><input type="button" name="del" value="Del" onclick="doDel();"></td>' \
'</tr></table></DIV>' \
'<DIV id=c_std>' \
'<table border="0" cellpadding="0" cellspacing="2">' \
'<tr onmouseover=i_showElem(10) onmouseout=i_showElem(0) >' \
"<td><input type=\"checkbox\" name=\"dhcp_static\" $check_static> provide only known hosts" \
'</td></tr></table></DIV>' \
'</form>' \
'<DIV id=c_foot></DIV>' \
'</DIV>' \
'<DIV id=c_verzoeg1></DIV>' \
'<DIV id=c_verzoeg2></DIV>' \
'</DIV>' \
'<DIV id=t_Frame>' \
'<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>' \
'</DIV>' \
'<DIV id=i_Frame>' \
'<DIV id=i_content>' \
'<h2>DHCP</h2>' \
'<p>The Dynamic Host Configuration Protocol (DHCP) is used to configure hosts (clients) automaticaly to operate in the local network.</p>' \
'</DIV>' \
'<DIV id=i_content1>' \
'<h2>DHCP Server</h2>' \
'Turn the DHCP server <b>on</b> or <b>off</b>.</p>' \
'</DIV>' \
'<DIV id=i_content2>' \
'<h2>DHCP Server Options</h2>' \
'<p><b>Lease</b><br>' \
'The <b>Lease</b>-time is a period of time the client is provided a lease on an IP address.</p>' \
'</DIV>' \
'<DIV id=i_content3>' \
'<h2>DHCP Server Options</h2>' \
'<p><b>Range</b><br>' \
'You need to supply the range of addresses available for lease.</p>' \
'</DIV>' \
'<DIV id=i_content4>' \
'<h2>DHCP Server Options</h2>' \
'<p><b>Gateway</b><br>' \
'Specify the default gateway for the local network.</p>' \
'</DIV>' \
'<DIV id=i_content5>' \
'<h2>DHCP Server Options</h2>' \
'<p><b>DNS</b><br>' \
'Specify up to 4 DNS (Domain Name Service) servers for address resolution. Naturally one DNS server is sufficient.</p>' \
'</DIV>' \
'<DIV id=i_content6>' \
'<h2>Known Hosts</h2>' \
'<p><b>Host</b><br>' \
'Delete a host from the DHCP database.</p>' \
'</DIV>' \
'<DIV id=i_content7>' \
'<h2>Known Hosts</h2>' \
'<p>Supply parameters for specified hosts using DHCP.</p><p><b>MAC</b><br>' \
'Specify a MAC-Address to identify the host.</p><p><img src="../pic_i_hinweis.gif" border="0"><br /> Specify the Ethernet address as <b>00:11:22:33:44:55</b>.</p>' \
'</DIV>' \
'<DIV id=i_content8>' \
'<h2>Known Hosts</h2>' \
'<p>Supply parameters for specified hosts using DHCP.</p><p><b>IP</b><br>' \
'Specify the IP-Address for the host.</p>' \
'<p>Note that IP addresses DO NOT have to be in the range given above, they just need to be on the same network.</p>' \
'</DIV>' \
'<DIV id=i_content9>' \
'<h2>Known Hosts</h2>' \
'<p>Supply parameters for specified hosts using DHCP.</p><p><b>Name</b><br>' \
'Give the host an hostname.</p>' \
'</DIV>' \
'<DIV id=i_content10>' \
'<h2>Known Hosts</h2>' \
'<p>Only hosts which have static addresses will be served.</p>' \
'</DIV>' \
'</DIV></BODY></HTML>'

