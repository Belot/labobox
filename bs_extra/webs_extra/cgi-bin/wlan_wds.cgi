#!/bin/sh

from=wlan_index.cgi
action=0

. ./login.cgi

/etc/start_scripts/wlan_present.sh
if [ $? -ne 0 ]; then
# WLAN hardware is not present!
cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY>
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>WLAN configuration</DIV>
<DIV id=c_title></div>
<DIV id=c_std><b style="color: rgb(255, 17, 17);">WLAN hardware is not present!</b></div>
<DIV id=c_leer></DIV>
</DIV>
<DIV id="c_foot"></DIV>
<DIV id=i_Frame>
</DIV>
</DIV></BODY></HTML>
HTML
exit 0
fi

f_pfad="WLAN Settings / WDS"
f_name="WDS"
HOME="wlan_index.cgi"
. ./functions.sh

PATH=.:$PATH

errmsg=""
mustsave=0

########################### Add #####################################
if [ "$action" -eq 10 ]; then
  hostmac=`echo "$text_newmac"|sed -e 's/%3A/:/g'|sed -e 's/%3a/:/g'`
  hostmac=`echo "$hostmac"|sed -n -e 's/\([0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]\)/\1/p'`
  [ "$hostmac" = "" ] && errmsg="Invalid MAC address!"'\n' && err=1

 if [ "$err" -eq 0 ]; then
  maclist=`nvram get wl_wds`
  [ "$maclist" = "Failed" ] && maclist=""
  if [ "$maclist" != "" ]; then
    maclist="$maclist $hostmac"
  else
    maclist="$hostmac"
  fi
  res=`nvram set wl_wds="$maclist"`
  [ "$res" != "OK" ] && errmsg="$errmsg Write to NVRam failed!"'\n' || nvram commit >/dev/null

  hostmac=""
 fi
fi


################################## Del #####################################
if [ "$action" -eq 11 ] && [ "$sel_maclist" != "" ]; then
 X=`echo "$sel_maclist"|sed -e 's/%3A/:/g'|sed -e 's/%3a/:/g'`
 if [ "$X" != "" ]; then
  maclist=`nvram get wl_wds`
  [ "$maclist" = "Failed" ] && maclist=""
  maclist=`echo $maclist|sed -e "s/\W*$X//g"`
  [ ${#maclist} -eq 18 ] 2>/dev/null && maclist=`echo $maclist|sed -e "s/ //g"`
  res=`nvram set wl_wds="$maclist"`
  [ "$res" != "OK" ] && errmsg="$errmsg Write to NVRam failed!"'\n' || nvram commit >/dev/null
 fi
fi

####### Save & Run #######
if [ "$action" -eq 2 ] || [ "$action" -eq 3 ]; then

  [ "$form_wl_wds_on" != "`nvram get wl_wds_on`" ] && res=`nvram set wl_wds_on=$form_wl_wds_on` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$form_wl_lazywds" != "`nvram get wl_lazywds`" ] && res=`nvram set wl_lazywds=$form_wl_lazywds` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$err" -ne 0 ] && errmsg="$errmsg Write to NVRam failed!"'\n'
  
  [ "$mustsave" -eq 1 ] && nvram commit >/dev/null && [ "$action" -eq 3 ] && /etc/start_scripts/wlan.sh restart >/dev/null 2>/dev/null
fi
#############

ON='checked=\"checked\"'
SEL="selected"

set_select()
{
  var=$1
  default=$2
  value=`nvram get $var`
  [ "$value" = "Failed" ] && value=$default 
  if [ $value -eq 1 ]; then
    eval `echo ${var}_on=$ON`
    eval `echo ${var}_off=''`
  else
    eval `echo ${var}_on=''`
    eval `echo ${var}_off=$ON`
  fi
}

set_select "wl_wds_on" 0
set_select "wl_lazywds" 1

maclist=`nvram get wl_wds`
[ "$maclist" = "Failed" ] && maclist=""

cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function showErr() {
  var err="$errmsg";
  if(err!=""){alert(err);}
}
function wds_change() {
  if(document.getElementById("form_wl_wds_off").checked==true){
    document.getElementById("btn_add").disabled=true;
    document.getElementById("text_newmac").disabled=true;
    document.getElementById("sel_maclist").disabled=true;
    document.getElementById("btn_del").disabled=true;
    document.getElementById("form_wl_lazywds_off").disabled=true;
    document.getElementById("form_wl_lazywds_on").disabled=true;
}
  else{
    document.getElementById("btn_add").disabled=false;
    document.getElementById("text_newmac").disabled=false;
    document.getElementById("sel_maclist").disabled=false;
    document.getElementById("btn_del").disabled=false;
    document.getElementById("form_wl_lazywds_off").disabled=false;
    document.getElementById("form_wl_lazywds_on").disabled=false;
}
}
function doAdd() {
  var hostmac=document.getElementById("text_newmac").value;
  if(!isValidMacAddress(hostmac)){
    alert("MAC: invalid MAC address!");
    return;
  }
  load();
  document.getElementById("action").value=10;
  document.getElementById("xform").submit();
}
function doDel() {
  load();
  document.getElementById("action").value=11;
  document.getElementById("xform").submit();
}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();wds_change();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>$f_pfad</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<DIV id=c_titel>Settings</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0)><td width="150">WDS</td>
<td width="20"><input type="radio" id="form_wl_wds_off" name="form_wl_wds_on" value="0" $wl_wds_on_off onchange="wds_change();"></td>
<td width="40">OFF</td>
<td width="20"><input type="radio" id="form_wl_wds_on" name="form_wl_wds_on" value="1" $wl_wds_on_on onchange="wds_change();"></td>
<td>ON</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0)><td width="150">Lazy-WDS</td>
<td width="20"><input type="radio" id="form_wl_lazywds_off" name="form_wl_lazywds" value="0" $wl_lazywds_off></td>
<td width="40">OFF</td>
<td width="20"><input type="radio" id="form_wl_lazywds_on" name="form_wl_lazywds" value="1" $wl_lazywds_on></td>
<td>ON</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpaddig="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0) >
<td width="150">MAC-address</td>
<td><input id="text_newmac" name="text_newmac" size="17" maxlength="17" type="text"></td>
<td><input type="button" id="btn_add" name="btn_add" value="Add" onclick="doAdd();"></td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0)>
<td width="150">WDS list</td>
<td>
  <select id="sel_maclist" name="sel_maclist" size="1">
HTML
echo "$maclist"|sed -e 's/ /\n/g'|sed -n -e '/^$/!p'|sed -e 's/\(^[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]\)\(.*\)/<option value="\1">\1\2<\/option>/'
cat <<HTML

  </select>
</td>
<td><input type="button" id="btn_del" name="btn_del" value="Del" onclick="doDel();"></td>
</tr></table></DIV>
</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>WDS setting</h2>
<p>Configure MAC addresses of other AP's that should be repeated by the router.</p>
</DIV>
<DIV id=i_content1>
<h2>WDS</h2>
<p>Turn WDS <b>off</b> or <b>on</b>.</p>
</DIV>
<DIV id=i_content3>
<h2>MAC-address</h2>
<p>Enter a valid MAC-address and push <b>add</b> to add it to the WDS list.</p>
</DIV>
<DIV id=i_content4>
<h2>WDS list</h2>
<p>Shows the addresses of your WDS list. Push <b>del</b> to delete the selected address from the list.</p>
</DIV>
</DIV></BODY></HTML>
