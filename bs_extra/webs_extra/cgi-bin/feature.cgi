#!/bin/sh

if [ ! -f "$f_bin" ]; then
echo 'Content-type: text/html'
echo 'Connection: close'
echo ''
echo   '<LINK rel="stylesheet" href="../style.css" type="text/css">' \
       '</HEAD>' \
       '<BODY>' \
       '<DIV id=c_Frame>' \
       '<DIV id=c_border>' \
       "<DIV id=c_pfad>$f_pfad</DIV>" \
       "<DIV id=c_titel>$f_name is not implemented!</DIV>" \
       '<DIV id=c_last>' \
       "$f_text" \
       '</DIV>' \
       '<DIV id=c_leer></DIV>' \
       '<DIV id=c_foot></DIV>' \
       '</DIV>' \
       '</DIV>' \
       '<DIV id=t_Frame>' \
       "<DIV id=t_but1 onclick='window.location.href=\"$HOME?session_id=$session_id\";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Zur&uuml;ck &lt;&lt;</DIV>" \ \
       '</DIV>' \
       '</BODY>' \
       '</HTML>'
exit 0
fi