#!/bin/sh

. ./login.cgi
. ./functions.sh
HOME="admin_index.cgi"
default_server="europe.pool.ntp.org"
default_zone="UTC+1"
default_index=10

if [ "$action" = "3" ]; then
 err=0
 echo $timezone > /tmp/debug
 ntp_index=`echo $timezone|sed -e 's/^.*%3[bB]//g'`
 http_unquote ntpserver
 http_unquote timezone_edit

 [ "$ntpserver" = "" ] && [ "$ntp_start" = "1" ] && err=1 && ERROR=$ERROR"\nNo NTP server specified."
 if [ "$err" -eq 0 ]; then
   mustsave=0
   [ "$ntp_start" != "`nvram get ntp_start`" ] && res=`nvram set ntp_start=$ntp_start` && mustsave=1
   [ "$timezone_edit" != "`nvram get ntp_timezone`" ] && res=`nvram set ntp_timezone=$timezone_edit` && mustsave=1
   [ "$ntpserver" != "`nvram get ntp_server`" ] && res=`nvram set ntp_server=$ntpserver` && mustsave=1
   [ "$ntp_index" != "`nvram get ntp_index`" ] && res=`nvram set ntp_index=$ntp_index` && mustsave=1
   [ "$mustsave" -eq 1 ] && nvram commit >/dev/null 
   [ "$mustsave" -eq 1 ] && STARTUP=1 /etc/start_scripts/date.sh >/dev/null 2>/dev/null 
 fi
fi

ntp_off='checked="checked"'
ntp_on=""
ntp_start=`nvram get ntp_start`
[ "$ntp_start" = "Failed" ] && ntp_start=0
[ "$ntp_start" = "1" ] && ntp_on='checked="checked"' && ntp_off=''

ntpserver=`nvram get ntp_server`
[ "$ntpserver" = "Failed" ] && ntpserver=$default_server

timezone_val=`nvram get ntp_timezone`
[ "$timezone_val" = "Failed" ] && timezone_val=$default_zone

tz_index=`nvram get ntp_index`
[ "$tz_index" = "Failed" ] && tz_index=$default_index

date=`date`

timezones="ACST-9:30ACDT-10:30,M10.5.0/02:00:00,M4.1.0/03:00:00;Adelaide AKST9AKDT,M3.2.0,M11.1.0;Alaska&Time CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00;Amsterdam,&Netherlands UTC+3;Argentina EET-2EEST-3,M3.5.0/03:00:00,M10.5.0/04:00:00;Athens,&Greece AST4ADT;Atlantic&Time AST4ADT,M4.1.0/00:01:00,M10.5.0/00:01:00;Atlantic&Time&(New&Brunswick) NZST-12NZDT-13,M10.1.0/02:00:00,M3.3.0/03:00:00;Auckland,&Wellington CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00;Barcelona,&Spain CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00;Berlin,&Germany BRST+3BRDT+2,M10.3.0,M2.3.0;Brazil,&Sao&Paulo AEST-10;Brisbane CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00;Brussels,&Belgium CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00;Budapest,&Hungary CST+6;Central&America CST6CDT,M3.2.0,M11.1.0;Central&Time UTC+5;Colombia CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00;Copenhagen,&Denmark ACST-9:30;Darwin GMT+0IST-1,M3.5.0/01:00:00,M10.5.0/02:00:00;Dublin,&Ireland EST5EDT,M3.2.0,M11.1.0;Eastern&Time CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00;Geneva,&Switzerland HAW10;Hawaii&Time EET-2EEST-3,M3.5.0/03:00:00,M10.5.0/04:00:00;Helsinki,&Finland AEST-10AEDT-11,M10.1.0/02:00:00,M4.1.0/03:00:00;Hobart HKT-8;Hong&Kong WIB-7;Jakarta EET-2EEST,M3.5.0/3,M10.5.0/4;Kyiv,&Ukraine WET-0WEST-1,M3.5.0/01:00:00,M10.5.0/02:00:00;Lisbon,&Portugal GMT+0BST-1,M3.5.0/01:00:00,M10.5.0/02:00:00;London,&Great&Britain CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00;Madrid,&Spain AEST-10AEDT-11,M10.5.0/02:00:00,M4.1.0/03:00:00;Melbourne,Canberra,Sydney MSK-3MSD,M3.5.0/2,M10.5.0/3;Moscow,&Russia MST7MDT,M3.2.0,M11.1.0;Mountain&Time MST7;Mountain&Time&(Arizona,&no&DST) NST+3:30NDT+2:30,M3.2.0/00:01:00,M11.1.0/00:01:00;Newfoundland&Time&(Updated&DST&for&2007) CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00;Oslo,&Norway PST8PDT,M3.2.0,M11.1.0;Pacific&Time CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00;Paris,&France AWST-8AWDT-9,M10.5.0,M3.5.0/03:00:00;Perth CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00;Prague,&Czech&Republic CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00;Roma,&Italy SGT-8;Singapore EET-2EEST-3,M3.5.0/03:00:00,M10.5.0/04:00:00;Sofia,&Bulgaria MST-3MDT,M3.5.0/2,M10.5.0/3;St.Petersburg,&Russia CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00;Stockholm,&Sweden EET-2EEST-3,M3.5.0/03:00:00,M10.5.0/04:00:00;Tallinn,&Estonia ULAT-8ULAST,M3.5.0/2,M9.5.0/2;Ulaanbaatar,&Mongolia CET-1CEST,M3.5.0,M10.5.0/3;Warsaw,&Poland UTC-12;UTC-12 UTC-11;UTC-11 UTC-10;UTC-10 UTC-9;UTC-9 UTC-8;UTC-8 UTC-7;UTC-7 UTC-6;UTC-6 UTC-5;UTC-5 UTC-4;UTC-4 UTC-3;UTC-3 UTC-2;UTC-2 UTC-1;UTC-1 UTC;UTC UTC+1;UTC+1 UTC+2;UTC+2 UTC+3;UTC+3 UTC+4;UTC+4 UTC+5;UTC+5 UTC+6;UTC+6 UTC+7;UTC+7 UTC+8;UTC+8 UTC+9;UTC+9 UTC+10;UTC+10 UTC+11;UTC+11 UTC+12;UTC+12 UTC+12;UTC+13 UTC+14;UTC+14 UTC+1;Custom"



cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
<SCRIPT type="text/JavaScript">
function showErr() {
var err="$ERROR";
if(err!=""){alert(err);}
}
function edit(select) {
 var val = select.options[select.options.selectedIndex].value;
 var temp = new Array();
 temp = val.split(';');
 document.getElementById("timezone_edit").value = temp[0];
}
function jump_to_custom(){
  document.getElementById("timezone").options.selectedIndex=76
}
</SCRIPT>
</HEAD>
<BODY onload="showErr();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Administration / Date &amp; Time</DIV>
<div id="c_titel">Date &amp; Time</div>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<div id="c_std">
Current date &amp; time: $date
</div>
<div id="c_std"></div>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0) >
<td width="130">NTP client:</td>
<td width="20"><input type="radio" name="ntp_start" value="1" $ntp_on></td>
<td width="40">On</td>
<td width="20"><input type="radio" name="ntp_start" value="0" $ntp_off></td>
<td>Off</td>
</tr></table>
</DIV>
<div id="c_std">
<table border="0" cellpadding="4" cellspacing="0">
    <tr onmouseover="i_showElem(2)" onmouseout="i_showElem(0)"> <td width="130">NTP server:</td><td><input type="input" name="ntpserver" size="25" maxlength="80" value="$ntpserver"></td></tr></table>
</div>
<div id="c_std">
<table border="0" cellpadding="4" cellspacing="0">
    <tr onmouseover="i_showElem(1)" onmouseout="i_showElem(0)"> <td width="130">Time zones:</td><td>
<select id="timezone" name="timezone" onchange="edit(this)">
EOF_HTML


echo $timezones|awk -v xindex=$tz_index '{ n=split($0,a," "); for(i=1; i<=n; i++){split(a[i],zone,";"); sel=""; name=zone[1]; data=zone[2]; gsub(/&/," ",zone[1]); gsub(/&/," ",zone[2]); if(i==xindex){sel="selected";} print "<option value=\"" zone[1] ";" i "\" " sel ">" zone[2] "</option>"; } }'



#[ $tz_select -eq 0 ] && echo "<option value=\"$timezone_val;$i\" selected>Custom</option>" || echo "<option value=\"$timezone_val;$i\">Custom</option>"
cat << EOF_HTML
</select>
</td></tr></table>
</div>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0) >
<td width="130">Used time zone:</td>
<td><input id="timezone_edit" name="timezone_edit" onchange="jump_to_custom()" size="50" maxlength="80" type="text" value="$timezone_val"></td>
</tr></table>
</DIV>
<DIV id=c_leer></DIV>
<DIV id="c_foot"></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Date &amp; Time</h2>
<p>Setup system time settings.</p>
</DIV>
<DIV id=i_content1>
<h2>Date &amp; Time</h2>
<p><b>Time zones</b><br>
This is a predefined list of time zones. The list also includes some spezial
time zone configuration for daylight saving time of some countries.</p>
<p><b>UTC</b> - West European Time or Greenwich Mean Time</p>
<p><b>UTC+1</b> - Middle European Time</p>
<p><b>UTC+2</b> - East European Time</p>
</DIV>
<DIV id=i_content2>
<h2>Date &amp; Time</h2>
<p><b>NTP server</b><br>
Set an NTP server address.</p>
<p><b>NTP</b> - The network time protocol</p>
<p>Default: europe.pool.ntp.org</p>
</DIV>
<DIV id=i_content3>
<h2>Date &amp; Time</h2>
<p><b>NTP client</b><br>
Switch the NTP client <b>on</b> or <b>off</b></p>
</DIV>
<DIV id=i_content4>
<h2>Date &amp; Time</h2>
<p><b>Used Time zone</b><br>
Enter here your time zone.</p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML

a=0
