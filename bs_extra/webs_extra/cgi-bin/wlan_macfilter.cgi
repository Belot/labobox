#!/bin/sh

from=wlan_basic.cgi
action=0

. ./login.cgi

/etc/start_scripts/wlan_present.sh
if [ $? -ne 0 ]; then
# WLAN hardware is not present!
cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY>
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>WLAN configuration</DIV>
<DIV id=c_title></div>
<DIV id=c_std><b style="color: rgb(255, 17, 17);">WLAN hardware is not present!</b></div>
<DIV id=c_leer></DIV>
</DIV>
<DIV id="c_foot"></DIV>
<DIV id=i_Frame>
</DIV>
</DIV></BODY></HTML>
HTML
exit 0
fi

f_pfad="WLAN Settings / MAC filter"
f_name="MAC filter"
HOME="wlan_index.cgi"
. ./functions.sh

PATH=.:$PATH

errmsg=""
mustsave=0

########################### Add #####################################
if [ "$action" -eq 10 ]; then
  hostmac=`echo "$text_newmac"|sed -e 's/%3A/:/g'|sed -e 's/%3a/:/g'`
  hostmac=`echo "$hostmac"|sed -n -e 's/\([0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]\)/\1/p'`
  [ "$hostmac" = "" ] && errmsg="Invalid MAC address!"'\n' && err=1

 if [ "$err" -eq 0 ]; then
  maclist=`nvram get wl_mac`
  [ "$maclist" = "Failed" ] && maclist=""
  if [ "$maclist" != "" ]; then
    maclist="$maclist $hostmac"
  else
    maclist="$hostmac"
  fi
  res=`nvram set wl_mac="$maclist"`
  [ "$res" != "OK" ] && errmsg="$errmsg Write to NVRam failed!"'\n' || nvram commit >/dev/null

  hostmac=""
 fi
fi


################################## Del #####################################
if [ "$action" -eq 11 ] && [ "$sel_maclist" != "" ]; then
 X=`echo "$sel_maclist"|sed -e 's/%3A/:/g'|sed -e 's/%3a/:/g'`
 if [ "$X" != "" ]; then
  maclist=`nvram get wl_mac`
  [ "$maclist" = "Failed" ] && maclist=""
  maclist=`echo $maclist|sed -e "s/\W*$X//g"`
  [ ${#maclist} -eq 18 ] 2>/dev/null && maclist=`echo $maclist|sed -e "s/ //g"`
  res=`nvram set wl_mac="$maclist"`
  [ "$res" != "OK" ] && errmsg="$errmsg Write to NVRam failed!"'\n' || nvram commit >/dev/null
 fi
fi

####### Save & Run #######
if [ "$action" -eq 2 ] || [ "$action" -eq 3 ]; then

  [ "$sel_macmode" != "`nvram get wl_macmode`" ] && res=`nvram set wl_macmode=$sel_macmode` && mustsave=1 && [ "$res" != "OK" ] && err=1

  [ "$err" -ne 0 ] && errmsg="$errmsg Write to NVRam failed!"'\n'
  
  [ "$mustsave" -eq 1 ] && nvram commit >/dev/null && [ "$action" -eq 3 ] && /etc/start_scripts/wlan.sh restart >/dev/null 2>/dev/null
fi
#############

ON='checked=\"checked\"'
SEL="selected"

macmode=`nvram get wl_macmode`
[ "$macmode" = "Failed" ] && macmode=0
[ "$macmode" = "0" ] && sel_macmode_off=$SEL
[ "$macmode" = "1" ] && sel_macmode_deny=$SEL
[ "$macmode" = "2" ] && sel_macmode_allow=$SEL
maclist=`nvram get wl_mac`
[ "$maclist" = "Failed" ] && maclist=""

DHCPHOSTS=`nvram get dhcphosts`
[ "$DHCPHOSTS" = "Failed" ] && DHCPHOSTS=""

cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>

<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function showErr() {
  var err="$errmsg";
  if(err!=""){alert(err);}
}
function macmode_change() {
  if(document.getElementById("sel_macmode").value==0){
    document.getElementById("btn_add").disabled=true;
    document.getElementById("text_newmac").disabled=true;
    document.getElementById("sel_maclist").disabled=true;
    document.getElementById("btn_del").disabled=true;
}
  else{
    document.getElementById("btn_add").disabled=false;
    document.getElementById("text_newmac").disabled=false;
    document.getElementById("sel_maclist").disabled=false;
    document.getElementById("btn_del").disabled=false;
}
}
function doAdd() {
  var hostmac=document.getElementById("text_newmac").value;
  if(!isValidMacAddress(hostmac)){
    alert("MAC: invalid MAC address!");
    return;
  }
  load();
  document.getElementById("action").value=10;
  document.getElementById("xform").submit();
}
function doDel() {
  load();
  document.getElementById("action").value=11;
  document.getElementById("xform").submit();
}

function edit(select) {
var wert = select.options[select.options.selectedIndex].value;
document.getElementById("text_newmac").value = wert;
var Wert=select.options[select.options.selectedIndex].text;
}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();macmode_change();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>$f_pfad</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<DIV id=c_titel>Settings</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0)>
<td width="150">MAC filter mode</td>
<td>
  <select id="sel_macmode" name="sel_macmode" size="1" onchange="macmode_change();">
    <option value="0" $sel_macmode_off>Disabled</option>
    <option value="1" $sel_macmode_deny>Deny</option>
    <option value="2" $sel_macmode_allow>Allow</option>
  </select>
</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpaddig="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0) >
<td width="150">MAC-address</td>
<td><input id="text_newmac" name="text_newmac" size="17" maxlength="17" type="text"></td>
<td><input type="button" id="btn_add" name="btn_add" value="Add" onclick="doAdd();"></td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0)>
<td width="150">MAC filter list</td>
<td>
  <select id="sel_maclist" name="sel_maclist" size="1">
HTML
echo "$maclist"|sed -e 's/ /\n/g'|sed -n -e '/^$/!p'|sed -e 's/\(^[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]\)\(.*\)/<option value="\1">\1\2<\/option>/'
cat <<HTML

  </select>
</td>
<td><input type="button" id="btn_del" name="btn_del" value="Del" onclick="doDel();"></td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0)>
<td width="150">known Hosts</td>
<td><select name="select" size="1" onchange="edit(this)" style="width:295px">
<option value=""></option>
HTML
echo $DHCPHOSTS|sed -e 's/~/\n/g'|sed -n -e '/^$/!p'|sed -e 's/\(^[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]:[0-9a-fA-F][0-9a-fA-F]\)\(.*\)/<option value="\1">\1\2<\/option>/'
cat <<HTML
</select></td></tr></table>
</DIV>
</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>MAC filter</h2>
<p>Configure MAC addresses that are allowed or denied for your WLAN.</p>
</DIV>
<DIV id=i_content1>
<h2>MAC filter mode</h2>
<p>Select if MAC filter is <b>off</b>, if MAC list is <b>allowed</b> or <b>denied</b> to access.</p>
</DIV>
<DIV id=i_content2>
<h2>MAC-address</h2>
<p>Enter a valid MAC-address and push <b>add</b> to add it to the filter list.</p>
</DIV>
<DIV id=i_content3>
<h2>MAC filter list</h2>
<p>Shows the addresses of your filter list. Push <b>del</b> to delete the selected address from the list.</p>
</DIV>
<DIV id=i_content4>
<h2>known Hosts</h2>
<p>Shows the known hosts from DHCP configuration. It only should help you and when selecting one entry the MAC will be automatically copied to the MAC-address field.</p>
</DIV>
</DIV></BODY></HTML>
