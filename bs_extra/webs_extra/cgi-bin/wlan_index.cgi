#!/bin/sh

from=wlan_index.cgi

. ./login.cgi
HOME="index.cgi"

. ./functions.sh

/etc/start_scripts/wlan_present.sh
if [ $? -ne 0 ]; then
# WLAN hardware is not present!
cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY>
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>WLAN configuration</DIV>
<DIV id=c_title></div>
<DIV id=c_std><b style="color: rgb(255, 17, 17);">WLAN hardware is not present!</b></div>
<DIV id=c_leer></DIV>
</DIV>
<DIV id="c_foot"></DIV>
<DIV id=i_Frame>
</DIV>
</DIV></BODY></HTML>
HTML
exit 0
fi

modul=`lsmod|grep wl`
if [ "$modul" = "" ]; then
  wl_up=""
else
  wl_up=`wlctl isup 2>/dev/null`
fi

if [ "$wl_up" != "" ]; then
  [ "$wl_up" = "1" ] && wl_up="WLAN is on" || wl_up="WLAN is off/not connected"
  wl0_mac=`ifconfig wl0|grep HWaddr|awk {'print $5;'} 2>/dev/null`
  [ "$wl0_mac" = "" ] && wl0_mac="not in use"
  essid=`wlctl essid|sed -e 's/.*\"\(.*\)\".*/\1/g' 2>/dev/null`
  ap=`wlctl ap 2>/dev/null`
  mode=`wlctl infra 2>/dev/null`
  if [ "$mode" = "0" ]; then
    mode="AdHoc"
  elif [ "$ap" = "1" ]; then 
    mode="AP"
  else
    mode="Client"
  fi
  channel=`wlctl channel|grep current|awk {'print $4;'} 2>/dev/null`
  pwr=`wlctl pwr_percent1 2>/dev/null`
  pwr="$pwr %"
  rate=`wlctl rate 2>/dev/null`
  wsec=`wlctl wsec 2>/dev/null`
  if [ "$wsec" = 0 ]; then
    wsec="no encryption"
  elif [ "$wsec" = "1" ]; then
     wsec="WEP"
  else
    wpa_auth=`wlctl wpa_auth|sed -e 's/[^0-9]*//g' 2>/dev/null`
    case "$wpa_auth" in
                        2)
                          wpa_auth="WPA/"
                          ;;
                        4)
                          wpa_auth="WPA-PSK/"
                          ;;
                        64)
                          wpa_auth="WPA2/"
                          ;;
                        66)
                          wpa_auth="WPA+WPA2/"
                          ;;
                        128)
                          wpa_auth="WPA2-PSK/"
                          ;;
                        132)
                          wpa_auth="WPA-PSK+WPA2-PSK/"
                          ;;
                        *)
                          wpa_auth="unknown"
                          ;;
    esac
    case "$wsec" in
                        2)
                          wsec="TKIP"
                          ;;
                        4)
                          wsec="AES"
                          ;;
                        6)
                          wsec="TKIP+AES"
                          ;;
                        *)
                          wsec="unkown"
                          ;;
    esac
  fi
  bridge=`brctl show br0|grep wl0 2>/dev/null`
  if [ "$bridge" != "" ]; then 
    bridge="bridge"
    ifname="br0"
  else
    bridge="seperate"
    ifname="wl0"
  fi
  IP=`ifconfig $ifname 2>/dev/null|grep "inet addr"|sed -n -e 's/^.*addr:\([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\).*Bcast.*/\1/p'`
  MASK=`ifconfig $ifname 2>/dev/null|grep "inet addr"|sed -n -e 's/^.*Mask:\([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\).*/\1/p'`
  [ "$IP" = "" ] && bridge="$bridge, no IP-address" || bridge="$bridge, $IP/$MASK"
else
  wl_up="WLAN module isnt loaded"
  wl0_mac="unknown"
fi

cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>

<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onselectstart="return false">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>WLAN configuration</DIV>
<DIV id=c_titel>WLAN settings</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td width="200"><a href="wlan_basic.cgi" onmouseover=i_showElem(1) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>Basic Settings</a></td>
</tr>
</table>
</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td width="200"><a href="wlan_security.cgi" onmouseover=i_showElem(2) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>Security Settings</a></td>
</tr>
</table>
</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td width="200"><a href="wlan_advanced.cgi" onmouseover=i_showElem(4) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>Advanced Settings</a></td>
</tr>
</table>
</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td width="200"><a href="wlan_macfilter.cgi" onmouseover=i_showElem(3) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>MAC filter</a></td>
</tr>
</table>
</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr> <td width=\"200\"><a href="wlan_wds.cgi" onmouseover=i_showElem(5) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>WDS</a></td>
</tr>  </table>
</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr> <td width=\"200\"><a href="wlan_scan.cgi" onmouseover=i_showElem(6) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>Scan</a></td>
</tr>  </table>
</DIV>
<DIV id=c_link>
<table border="0" cellpadding="4" cellspacing="0">
<tr> <td width=\"200\"><a href="wlan_debug.cgi" onmouseover=i_showElem(7) onmouseout=i_showElem(0) target="hcti"><b>&gt;&gt; </b>Debug log</a></td>
</tr>  </table>
</DIV>
<DIV id=c_leer></DIV>
<DIV id=c_titel>Status</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="100">MAC-Adress:</td>
<td>$wl0_mac</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="100">Status:</td>
<td>$wl_up</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="100">Mode:</td>
<td>$mode</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="100">SSID:</td>
<td>$essid</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<td width="100">Channel:</td>
<td>$channel</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="100">TX power:</td>
<td>$pwr</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="100">Data rate:</td>
<td>$rate</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="100">Encryption:</td>
<td>$wpa_auth$wsec</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<td width="100">Network mode:</td>
<td>$bridge</td>
</tr></table></DIV>
HTML
#get wl mac adress
if [ "$wl_up" != "" ]; then
  if [ "$mode" = "AdHoc" ] || [ "$ap" = "0" ]; then
    bssid=`wlctl status 2>/dev/null|grep BSSID|awk {'print $2;'}`
    rssi=`wlctl status 2>/dev/null|grep RSSI|sed -e 's/.*RSSI: \(.*\) dBm.noise.*/\1/g'`
    noise=`wlctl status 2>/dev/null|grep RSSI|sed -e 's/.*noise: \(.*\) dBm.*/\1/g'`
    let "rssi*=-1" 2>/dev/null
    let "noise*=-1" 2>/dev/null
    let "qual=$noise-$rssi" 2>/dev/null
    echo '<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr><td width="100">Connected with:</td>'
    echo "<td>$bssid</td>"
    echo '</tr></table></DIV><DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr><td width="100">Signal:</td>'
    echo "<td>$rssi dBm</td>"
    echo '</tr></table></DIV><DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr><td width="100">Noise:</td>'
    echo "<td>$noise dBm</td>"
    echo '</tr></table></DIV><DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr><td width="100">SNR:</td><td>'
    HBar2 250 25 $qual 40 "dBm"
    #HBar 250 25 $qual "dBm"
    echo '</td></tr></table></DIV>'
  else
    noise=`wlctl noise 2>/dev/null`
    let "noise*=-1" 2>/dev/null
    list=`wlctl assoclist 2>/dev/null|sed -n -e 's/[^0-9,A-F]*\([0-9,A-F]*:[0-9,A-F]*:[0-9,A-F]*:[0-9,A-F]*:[0-9,A-F]*:[0-9,A-F]*\)[^0-9,A-F]*/\1/p' 2>/dev/null`
    if [ "$list" = "" ]; then
      echo '<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr><td width="100">Clients:</td><td>no clients associated</td></tr></table></DIV>'
    fi
    j=1
    for i in $list
    do
      rssi=`wlctl rssi $i 2>/dev/null`
      let "rssi*=-1" 2>/dev/null
      let "qual=$noise-$rssi" 2>/dev/null
      echo "<DIV id=c_std><table border=\"0\" cellpadding=\"0\" cellspacing=\"2\"><tr><td width=\"100\">Client($j):</td>"
      echo "<td>$i</td>"
      echo '</tr></table></DIV><DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr><td width="100">Signal:</td>'
      echo "<td>$rssi dBm</td>"
      echo '</tr></table></DIV><DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr><td width="100">Noise:</td>'
      echo "<td>$noise dBm</td>"
      echo '</tr></table></DIV><DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr><td width="100">SNR:</td><td>'
      HBar2 250 25 $qual 40 "dBm"
      #HBar 250 25 $qual "dBm"
      echo '</tr></table></DIV>'
      let "j+=1"
    done
  fi
fi

cat <<HTML
<DIV id=c_leer></DIV>
</DIV>
<DIV id="c_foot"></DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>WLAN configuration</h2>
<p></p>
</DIV>
<DIV id=i_content1>
<h2>WLAN configuration</h2>
<p><b>Basic settings</b><br>
Configure basic settings like operation mode, network name etc.</p>
</DIV>
<DIV id=i_content2>
<h2>WLAN configuration</h2>
<p><b>Security Settings</b><br>
Configure encryption and authentication.</p>
</DIV>
<DIV id=i_content3>
<h2>WLAN configuration</h2>
<p><b>MAC filter</b><br>
Configure MAC-Adresses that are allowed to access your WLAN.</p>
</DIV>
<DIV id=i_content4>
<h2>WLAN configuration</h2>
<p><b>Advanced settings</b><br>
Configure specific parameters like preamble, thresholds etc.</p>
</DIV>
<DIV id=i_content5>
<h2>WLAN configuration</h2>
<p><b>WDS</b><br>
Configure repeater settings.</p>
</DIV>
<DIV id=i_content6>
<h2>WLAN configuration</h2>
<p><b>Scan</b><br>
Here you can do a scan for other WLAN-networks in your neighbourhood.</p>
</DIV>
<DIV id=i_content7>
<h2>WLAN configuration</h2>
<p><b>Debug log</b><br>
Here you can enable the debug output of the wlan script to look at the commands generated 
by the script. This option is only for experts!</p>
</DIV>
</DIV></BODY></HTML>



