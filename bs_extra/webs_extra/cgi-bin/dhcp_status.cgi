#!/bin/sh


HOME="status_index.cgi"
LEASE_FILE="/var/dnsmasq.leases"


cat << EOF_HTML 
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt"type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
<style type="text/css">
table { margin-left:15px; }
</style>
</HEAD>
<BODY>
<DIV id=c_border>
<DIV id=c_pfad>Status / DHCP</DIV>
<DIV id=c_titel>DHCP</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
</form>
<table border="0" cellpadding="4" cellspacing="0">
    <tr><td width="130"><b>MAC address</b></td><td width="120"><b>IP address</b></td>
    <td width="120"><b>Hostname</b></td><td width="120"><b>Lease</b> [h:mm:ss]</td></tr>
EOF_HTML
if [ -f $LEASE_FILE ]; then
  awk "BEGIN{t0=`date +%s`;}"'{lease=$1; mac=$2; ip=$3; hostname=$4; t=lease-t0; vz=""; if(t<0){t=-t; vz="-";} h=int(t/3600); t=t-h*3600; m=int(t/60); t=t-m*60; s=t; printf "<tr><td>%s</td><td>%s</td><td>%s</td><td>%s%d:%02d:%02d</td></tr>\n",mac,ip,hostname,vz,h,m,s;}' $LEASE_FILE
else
  echo "<tr><td>No data available</td></tr>"
fi
cat << EOF_HTML 
</table>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</div>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>DHCP status</h2>
<p></p>
</DIV>
</BODY></HTML>
EOF_HTML






