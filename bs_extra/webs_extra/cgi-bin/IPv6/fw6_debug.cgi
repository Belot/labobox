#!/bin/sh

. ./login.cgi

HOME="fw_index.cgi"
. ./functions.sh

PATH=.:$PATH

errmsg=""
mustsave=0


####### Save & Run #######
if [ "$action" -eq 2 ] || [ "$action" -eq 3 ]; then
  err=0
  [ "$form_fw_debug" != "`nvram get fw6_debug`" ] && res=`nvram set fw6_debug=$form_fw_debug` && mustsave=1

  [ "$mustsave" -eq 1 ] && nvram commit >/dev/null && [ "$action" -eq 3 ] && /etc/start_scripts/firewall_ipv6.sh restart >/dev/null 2>/dev/null
fi

ON='checked=\"checked\"'
SEL="selected"

set_select()
{
  var=$1
  default=$2
  value=`nvram get $var`
  [ "$value" = "Failed" ] && value=$default 
  if [ $value -eq 1 ]; then
    eval `echo ${var}_on=$ON`
    eval `echo ${var}_off=''`
  else
    eval `echo ${var}_on=''`
    eval `echo ${var}_off=$ON`
  fi
}
set_select "fw6_debug" 0

cat << EOF_HTML 
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt"type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY>
<DIV id=c_border>
<DIV id=c_pfad>Firewall Settings / Debug log</DIV>
<DIV id=c_titel>Debug settings</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0"></>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0)><td width="200">Debugging</td>
<td width="20"><input type="radio" id="debug_off" name="form_fw_debug" value="0"  $fw6_debug_off></td>
<td width="40">Off</td>
<td width="20"><input type="radio" id="debug_on" name="form_fw_debug" value="1" $fw6_debug_on></td>
<td>On</td>
</tr></table>
</DIV>
</form>
<DIV id=c_titel>Debug log</DIV>
<pre style="padding-left:10px; margin-top:1px">
EOF_HTML

[ -f "/var/firewall6.log" -a "$fw6_debug_off" = "" ] && cat /var/firewall6.log || echo "No debugging output"

cat << EOF_HTML
</pre>
</div>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>IPv6 firewall debugging</h2>
<p>Show debugging output of IPv6 firewall script.</p>
</DIV></DIV>
</BODY></HTML>
EOF_HTML

a=0