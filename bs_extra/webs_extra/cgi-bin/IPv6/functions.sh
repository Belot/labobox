#!/bin/sh

isNumeric()
{
  [ $1 -eq 0 ] 2>/dev/null && return 0
  a="$1"
  let "a-=1" 2>/dev/null || return 1
  [ $a -eq -1 ] && return 1 || return 0
}

# is_ipaddr <IP>
# return 0 => ok
# return 1 => error
is_ipaddr()
{
  local_ip=`echo "$1"|sed -n -e 's/[^0-9]*\([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\)[^0-9]*/\1/p'`
  [ "$local_ip" = "" ] && return 1
  local_ip1=`echo "$local_ip"|sed -n -e 's/^\([0-9]*\).*/\1/p'`
  local_ip2=`echo "$local_ip"|sed -n -e 's/.*\.\([0-9]*\)\..*\..*/\1/p'`
  local_ip3=`echo "$local_ip"|sed -n -e 's/.*\.\([0-9]*\)\..*$/\1/p'`
  local_ip4=`echo "$local_ip"|sed -n -e 's/.*\.\([0-9]*\)$/\1/p'`

  ( [ "$local_ip1" = "" ] || [ "$local_ip2" = "" ] || [ "$local_ip3" = "" ] || [ "$local_ip4" = "" ] || [ "$local_ip1" -gt "255" ] || [ "$local_ip2" -gt "255" ] || [ "$local_ip3" -gt "255" ] || [ "$local_ip4" -gt "255" ] ) && return 1 || return 0
}

# http_unquote <Var-Name>
# modifiziert die Variable Var
http_unquote()
{
  eval local_string="\$$1"
  local_string=`echo "$local_string"|sed -e 's/%3A/:/g'|sed -e 's/%3a/:/g'`
  local_string=`echo "$local_string"|sed -e 's/%2C/,/g'|sed -e 's/%2c/,/g'`
  local_string=`echo "$local_string"|sed -e 's/%40/@/g'`
  local_string=`echo "$local_string"|sed -e 's/%20/ /g'`
  local_string=`echo "$local_string"|sed -e 's/+/ /g'`
  local_string=`echo "$local_string"|sed -e 's/%2B/+/g'|sed -e 's/%2b/+/g'`
  local_string=`echo "$local_string"|sed -e 's/%2F/\//g'|sed -e 's/%2f/\//g'`
  local_string=`echo "$local_string"|sed -e 's/%23/#/g'`
# alle anderen Sonderzeichen entfernen
  local_string=`echo "$local_string"|sed -r -e  's/((%25)|%..)/\2/g'|sed -e 's/%25/\%/g'`
  eval $1=\"$local_string\" 2>/dev/null
}

http_unquote_all()
{
  eval local_string="\$$1"
  local_string=`echo "$local_string"|sed -e 's/%/\\\x/g'`
  local_string=`echo "$local_string"|sed -e 's/+/ /g'`
  local_string=`echo "$local_string"|sed -e 's/*/\\\x2a/g'`
  local_string=`echo -e $local_string`
  eval $1=\"$local_string\"
}

# zeichnet horizontale Prozentanzeige
# Parameter: Breite Hoehe Wert
HBar()
{
  einheit=""
 [ "$4" != "" ] && einheit="&einheit=$4"
 let wval=$1*$3/100
 echo '<object data="svg_bar.cgi?w='$1'&h='$2'&val='$3$einheit'" type="image/svg+xml" width="'$1'" height="'$2'" ><param name="src" value="svg_bar.svg">
 <div style="background-color:#f0f0f0; border-style: solid; border-color:#000000; border-width:1px; height:'$2'px; width:'$1'px; padding:0px; margin:0px;"><div style="color:#f68918; background:#f68918; height:'$2'px; width:'$wval'px; left:0px; top:0px;"></div></div>
  </object>'
}

# zeichnet horizontale Prozentanzeige
# Parameter: Breite Hoehe Wert MAX
HBar2()
{
  einheit=""
 [ "$5" != "" ] && einheit="&einheit=$5"
 let wval=$1*$3/$4
 echo '<object data="svg_bar.cgi?w='$1'&h='$2'&val='$3'&max='$4$einheit'" type="image/svg+xml" width="'$1'" height="'$2'" ><param name="src" value="svg_bar.svg">
 <div style="background-color:#f0f0f0; border-style: solid; border-color:#000000; border-width:1px; height:'$2'px; width:'$1'px; padding:0px; margin:0px;"><div style="color:#f68918; background:#f68918; height:'$2'px; width:'$wval'px; left:0px; top:0px;"></div></div>
  </object>'
}

#check if a portnumber is in fw_services in nvram
#is_fwservice_port <tcp|udp> <portnumber>
#return 0=found
#return 1=not found
is_fwservice_port()
{
  [ "$1" != "tcp" ] && [ "$1" != "udp" ] || [ "$2" = "" ] && return 1
  search=`nvram get fw_services_$1`
  [ "$search" = "Failed" ] && return 1
  ret=`echo $search|grep -e " $2"`
  [ "$ret" != "" ] && return 0
  ret=`echo $search|grep -e "^$2"`
  [ "$ret" != "" ] && return 0
  return 1
}

#remove a portnumber from fw_services in nvram
#remove_fwservice_port <tcp|udp> <portnumber>
#return 0=ok
#return 1=error
remove_fwservice_port()
{
  [ "$1" != "tcp" ] && [ "$1" != "udp" ] || [ "$2" = "" ] && return 1
  mylist=`nvram get fw_services_$1`
  [ "$mylist" = "Failed" ] && return 0
  mylist=`echo $mylist|sed -e "s/$2//g"|sed -e 's/^ //g'|sed -e 's/ $//g'`
  ret=`nvram set fw_services_$1="$mylist"`
  [ "$ret" = "Failed" ] && return 1 || return 0
}

#add a portnumber to fw_services in nvram
#add_fwservice_port <tcp|udp> <portnumber>
#return 0=ok
#return 1=error
add_fwservice_port()
{
  [ "$1" != "tcp" ] && [ "$1" != "udp" ] || [ "$2" = "" ] && return 1
  list=`nvram get fw_services_$1`
  [ "$list" = "Failed" ] && list=""
  echo "add list=$list"
#remove port if already in list to avoid multiples
  is_fwservice_port "$1" "$2" && remove_fwservice_port "$1" "$2"
  echo "add list=$list"
  [ "$list" = "" ] && list=$2 || list=$list" $2"
  ret=`nvram set fw_services_$1="$list"`
  [ "$ret" = "Failed" ] && return 1 || return 0
}


#################### IPv6 ########################

ipv6_error_strings_0='OK' # 0
ipv6_error_strings_1='invalid character(s) in IPv6 address' # 1
ipv6_error_strings_2='too much "::" in IPv6 address' # 2
ipv6_error_strings_3='too less ":" in IPv6 address' # 3
ipv6_error_strings_4='a character is missing at beginning of the IPv6 address' # 4
ipv6_error_strings_5='a character is missing at the end of the IPv6 address' # 5
ipv6_error_strings_6='the given address is too short' # 6
ipv6_error_strings_7='the given address is too long' # 7
ipv6_error_strings_8='' # 8
ipv6_error_strings_9='' # 9
ipv6_error_strings_10='invalid prefix' # 10

get_ipv6_err_string()
{
  eval echo \$ipv6_error_strings_$1
}


is_ipv6_addr()
{
  echo $1|awk 'BEGIN{}
	{
	  if(match($0,/[^0-9a-fA-F:]/)){exit 1}
	  if(match($0,/:::/)){exit 2}
	  x=match($0,/::/); 
	  ip_end=substr($0,RSTART+RLENGTH);
	  ip_start=substr($0,0,RSTART-1);
	  if(match(ip_end,/::/)){exit 2}

	  start_num=split(ip_start,a,/[:]/);
	  for(i=1; i<=start_num; i++)
          {
            if(length(a[i])>4){exit 3}
	    if(length(a[i])<1){exit 4}
          }

	  end_num=split(ip_end,a,/[:]/);
	  for(i=1; i<=end_num; i++)
          {
            if(length(a[i])>4){exit 3}
	    if(length(a[i])<1){exit 5}
          }

	  if(x==0 && end_num<8){exit 6}
	  if(x==0 && end_num>8){exit 7}
	  if(x!=0 && start_num+end_num>7){exit 7}

	  exit 0
	}'
  return $?
}

is_ipv6_addr_with_prefix()
{
  x=$1
  ipv6=`echo $x|sed -e 's/^\(.*\)\/.*$/\1/'`
  prefix=`echo $x|sed -e 's/^.*\/\(.*\)$/\1/'`
  [ "$ipv6" = "$prefix" ] && return 10
  [ "$prefix" = "" ]      && return 10

  h=`echo $prefix|sed -e 's/[^0-9]//'`
  [ "$h" != "$prefix" ]   && return 10

  [ "$prefix" -lt 0 ]     && return 10
  [ "$prefix" -gt 128 ]   && return 10

  is_ipv6_addr "$ipv6"
  return $?
}


# # # is_ipv6_addr_with_prefix $1
# # # x=`get_ipv6_err_string $?`
# # # echo $x	  	 
