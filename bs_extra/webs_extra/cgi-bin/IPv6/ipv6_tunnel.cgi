#!/bin/sh

action=0
mustsave=0

. ./functions.sh
. ./login.cgi

HOME="ipv6_index.cgi"

ipv6_tunnel=`nvram get ipv6_6to4_tunnel`
[ "$ipv6_tunnel" = "Failed" ] && ipv6_tunnel=""

err=0
########################### Add #####################################
if [ "$action" -eq 10 ]; then
  
  http_unquote remote_addr
  http_unquote local_addr
  http_unquote local_ipv6_addr
#  http_unquote local_ipv6_net
  http_unquote route

  [ "$remote_addr" = "" ] && errmsg="$errmsg Remote IPv4 address not specified!"'\n' && err=1
  if [ "$err" = "0" ]; then
    is_ipaddr $remote_addr
    ret=$?
    [ "$ret" != "0" ] && errmsg="$errmsg Remote IPv4 address invalid (${remote_addr})!"'\n' && err=1
  fi

  if [ "$local_stat_dyn" = "0" ]; then
	[ "local_addr" = "" ] && errmsg="$errmsg Local IPv4 address not specified!"'\n' && err=1
	if [ "$err" = "0" ]; then
	is_ipaddr $local_addr
	ret=$?
	[ "$ret" != "0" ] && errmsg="$errmsg Local IPv4 address invalid (${local_addr})!"'\n' && err=1
	fi
  else
	[ "$device" = "" ] && errmsg="$errmsg Unknown error!"'\n' && err=1
	local_addr="dyn:${device}"
  fi

  [ "$local_ipv6_addr" = "" ] && errmsg="$errmsg Local IPv6 address not specified!"'\n' && err=1
  if [ "$err" = "0" ]; then
    is_ipv6_addr_with_prefix $local_ipv6_addr
    ret=$?
    [ "$ret" != "0" ] && ipv6_errmsg=`get_ipv6_err_string $ret` && errmsg="$errmsg Invalid local IPv6 address (${local_ipv6_addr} -- $ipv6_errmsg)!"'\n' && err=1
  fi

#   [ "$local_ipv6_net" = "" ] && errmsg="$errmsg Local IPv6 subnet not specified!"'\n' && err=1
#   if [ "$err" = "0" ]; then
#     is_ipv6_addr_with_prefix $local_ipv6_net
#     ret=$?
#     [ "$ret" != "0" ] && ipv6_errmsg=`get_ipv6_err_string $ret` && errmsg="$errmsg Invalid local IPv6 subnet address (${local_ipv6_net} -- $ipv6_errmsg)!"'\n' && err=1
#   fi

  [ "$route" = "" ] && errmsg="$errmsg Route not specified!"'\n' && err=1
  if [ "$err" = "0" ]; then
    is_ipv6_addr_with_prefix $route
    ret=$?
    [ "$ret" != "0" ] && ipv6_errmsg=`get_ipv6_err_string $ret` && errmsg="$errmsg Invalid route (${route} -- $ipv6_errmsg)!"'\n' && err=1
  fi

  if [ "$metric" != "" ]; then
    isNumeric "$metric"
    if [ "$?" -eq "0" ]; then
      [ "$metric" -lt 0 ] && errmsg="$errmsg Metric is invalid ($metric)!"'\n' && err=1
    else
      errmsg="$errmsg Metric is invalid ($metric)!"'\n'
      err=1
    fi 
  fi

  isNumeric "$ttl"
  if [ "$?" -eq "0" ]; then
    [ "$ttl" -lt 1 ] && errmsg="$errmsg TTL is too small ($ttl) set it to 64!"'\n' && ttl=64
    [ "$ttl" -gt 255 ] && errmsg="$errmsg TTL is too large ($ttl) set it to 64!"'\n' && ttl=64
  else
    errmsg="$errmsg TTL is invalid ($ttl)!"'\n'
    err=1
  fi 

  isNumeric "$mtu"
  if [ "$?" -eq "0" ]; then
    [ "$mtu" -lt 1280 ] && errmsg="$errmsg MTU is too small ($ttl) set it to 1280!"'\n' && mtu=1280
    [ "$mtu" -gt 1500 ] && errmsg="$errmsg MTU is too large ($ttl) set it to 1280!"'\n' && mtu=1280
  else
    errmsg="$errmsg MTU is invalid ($mtu)!"'\n'
    err=1
  fi 

#  [ "$advertise" != "1" ] && advertise="0"
  
  if [ "$err" = "0" ]; then
    ipv6_tunnel=$ipv6_tunnel"$remote_addr,$local_addr,$local_ipv6_addr,$local_ipv6_net,$route,$ttl,$advertise,$metric,$mtu~"
    res=`nvram set ipv6_6to4_tunnel=$ipv6_tunnel`
    mustsave=1 
    [ "$res" != "OK" ] && errmsg="$errmsg Write to NVRam failed!"'\n'

    # duplikat check
    dup=`echo $ipv6_tunnel|sed -e 's/~/\n/g'|cut -d ',' -f 1|sort|awk 'BEGIN{h=""}{if(h==""){h=$1; next;} if(h==$1){print $1;} h=$1;}'|tr  '\n' ' '`
    [ "$dup" != "" ] && errmsg="$errmsg Warning duplicate route (duplikat remote address): $dup"'\n'
    dup=`echo $ipv6_tunnel|sed -e 's/~/\n/g'|cut -d ',' -f 3|sort|awk 'BEGIN{h=""}{if(h==""){h=$1; next;} if(h==$1){print $1;} h=$1;}'|tr  '\n' ' '`
    [ "$dup" != "" ] && errmsg="$errmsg Warning duplicate route (duplikat local IPv6 address): $dup"'\n'

  fi
fi


################################## Del #####################################

if [ "$action" -eq 11 ] && [ "$select" != "" ]; then
  http_unquote select
  X=`echo "$select"|sed -e 's/%2C/,/g'|sed -e 's/%2c/,/g'`
echo $X > /tmp/test
  ipv6_tunnel=`echo $ipv6_tunnel|sed -e "s-$X~--"`
  res=`nvram set ipv6_6to4_tunnel=$ipv6_tunnel`
  [ "$res" != "OK" ] && errmsg="$errmsg Write to NVRam failed!"'\n' || mustsave=1
fi

################################ Save & Run #################################
if [ "$action" -eq 3 ] || [ "$action" -eq 2 ]; then
  [ "$action" -eq 3 ] && /etc/start_scripts/ipv6.sh restart >/dev/null 2>/dev/null
  [ "$mustsave" = "1" ] && nvram commit >/dev/null
fi

ipv6_tunnel=`nvram get ipv6_6to4_tunnel`
[ "$ipv6_tunnel" = "Failed" ] && ipv6_tunnel=""

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function showErr() {
var err='$errmsg';
if(err!=""){alert(err);} }
function js_local(state) {
 if(state) {
  document.getElementById("local_addr").style.visibility="hidden";
  document.getElementById("device").style.visibility="visible";
 }
 else {
  document.getElementById("local_addr").style.visibility="visible";
  document.getElementById("device").style.visibility="hidden";
 }
}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>IPv6 settings / IPv6 6to4 tunnels</DIV>
<DIV id=c_titel>Add 6to4 tunnel</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<input type="hidden" name="mustsave" value="$mustsave">
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0) >
<td width="140">Remote IPv4 address:</td>
<td><input type="input" name="remote_addr" size="17" maxlength="15" value=""></td>
</td></tr>
</table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0) >
<td width="140">Local IPv4 address:</td>
<td width="20"><input type="radio" name="local_stat_dyn" value="0" checked="checked" onchange="js_local(false);"></td>
<td width="100">static address</td>
<td width="20"><input type="radio" name="local_stat_dyn" value="1" onchange="js_local(true);"></td>
<td>address from device</td>
</td></tr>
</table>
</div>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0) >
<td width="140"></td>
<td><input type="input" name="local_addr" id='local_addr' size="17" maxlength="15" value=""></td>
<td><select style="visibility:hidden" name="device" id="device" size="1">
<option value="0">Bridge/LAN</option>
<option value="1">WLAN</option>
<option value="2">PPP0</option>
</select></td></tr>
</table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0) >
<td width="140">Local IPv6 address:</td>
<td><input type="input" name="local_ipv6_addr" size="40" maxlength="35" value=""></td>
</td></tr>
</table>
</DIV>
<!--
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(4) onmouseout=i_showElem(0) >
<td width="140">Local IPv6 subnet:</td>
<td><input type="input" name="local_ipv6_net" size="40" maxlength="35" value=""></td>
</td></tr>
</table>
</DIV>
-->
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(5) onmouseout=i_showElem(0) >
<td width="140">Route:</td>
<td><input type="input" name="route" size="40" maxlength="35" value=""></td>
</td></tr>
</table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(6) onmouseout=i_showElem(0) >
<td width="140">TTL:</td>
<td><input type="input" name="ttl" size="5" maxlength="3" value="64"></td>
</td></tr>
</table>
</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(16) onmouseout=i_showElem(0) >
<td width="140">MTU:</td>
<td><input type="input" name="mtu" size="5" maxlength="4" value="1280"></td>
</td></tr>
</table>
</DIV>
<!--
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(7) onmouseout=i_showElem(0) >
<td width="140">Advertise:</td><td>
<input type="checkbox" name="advertise" value="1"></td>
</tr></table>
</DIV>
-->
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(8) onmouseout=i_showElem(0) >
<td width="140">Metric:</td>
<td><input type="input" name="metric" size="5" maxlength="5"></td>
</td></tr>
</table>
</DIV>
<DIV id=c_last><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(10) onmouseout=i_showElem(0) ><td width=335></td>
<td><input type="button" name="add" value="Add" onclick="subm(10);"></td>
</tr></table>
</DIV>
<div id="c_leer"></div>
<DIV id=c_titel>Delete 6to4 tunnel</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(9) onmouseout=i_showElem(0) >
<td width="50">Tunnel:</td>
<td><select name="select" size="1" style="width:420px">
EOF_HTML

if [ "$ipv6_tunnel" != "" ]; then
 echo $ipv6_tunnel |sed -e 's/~/\n/g'|sed -n -e '/^$/!p'| \
 awk '{ split($0,a,/[,]/)
        remote_addr=a[1]
        local_addr=a[2]
        ipv6=a[3]

	if(local_addr ~ /^dyn/)
	{
	  split(local_addr,h,/:/)
          split("Bridge/LAN,WLAN,PPP0", ifnames, ",");
          local_addr=ifnames[h[2]+1];
        }
        print "<option value=\"" $0 "\">remote=" remote_addr " local=" local_addr " IPv6=" ipv6 "</option>"
     }'
fi

cat << EOF_HTML
</select></td></tr></table></DIV>
<DIV id=c_last><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(11) onmouseout=i_showElem(0) ><td width=335></td>
<td><input type="button" name="del" value="Delete" onclick="subm(11);"></td>
</tr></table></DIV>
</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>IPv6 6to4 tunnels</h2>
<p>Add or remove IPv6 6to4 tunnels.</p>
<p>6to4 does not facilitate interoperation between IPv4-only hosts and 
IPv6-only hosts. 6to4 is simply a transparent mechanism used as a transport layer 
between IPv6 nodes.</p>
</DIV>
<DIV id=i_content1>
<h2>Add 6to4 tunnel</h2>
<p><b>Remote IPv4 address</b></p>
<p>The IPv4 address of the remote tunnel endpoint.</p>
</DIV>
<DIV id=i_content2>
<h2>Add 6to4 tunnel</h2>
<p><b>Local IPv4 address</b></p>
<p>The IPv4 address of the local tunnel endpoint. You can set a static IP address,
or choose a device to dynamically query the ip address to use.</p>
<p>Use the <i>address from device</i> option if you use a dynamically allocated
local address from DHCP or PPP.</p>
</DIV>
<DIV id=i_content3>
<h2>Add 6to4 tunnel</h2>
<p><b>Local IPv6 address</b></p>
<p>Set the local IPv6 address.</p>
<p>Example: 2001:abcd:1234::1/64<p>
</DIV>
<DIV id=i_content4>
<h2>Add 6to4 tunnel</h2>
<p><b>Local IPv6 subnet</b></p>
<p>Set the local IPv6 subnet address.</p>
<p>Example: 2001:abcd:1234::/64</p>
</DIV>
<DIV id=i_content5>
<h2>Add 6to4 tunnel</h2>
<p><b>Route</b></p>
<p>Set the route entry for this tunnel.</p>
<p>Example: ::/0 (all IPv6 traffic through the tunnel)</p>
</DIV>
<DIV id=i_content6>
<h2>Add 6to4 tunnel</h2>
<p><b>TTL</b></p>
<p>The TTL field is reduced by one on every hop. If it reach the value 0 
the packet will be droped.</p>
<p>Values: 1-255</p>
<p>Default: 64</p>
</DIV>
<DIV id=i_content7>
<h2>Add 6to4 tunnel</h2>
<p><b>Advertise</b></p>
<p>Set it to advertise this route for auto configuration.</p>
</DIV>
<DIV id=i_content8>
<h2>Add 6to4 tunnel</h2>
<p><b>Metric</b></p>
<p>The metric for this route. Let the metric field empty to use the default metric or
type a numeric value. The lower the metric, the higher is the route's priority.
</p>
<p>Values: >=0, empty</p>
<p>Default: empty</p>
</DIV>
<DIV id=i_content9>
<h2>Add 6to4 tunnel</h2>
<p><b></b></p>
<p></p>
</DIV>
<DIV id=i_content9>
<h2>Delete 6to4 tunnel</h2>
<p><b>Tunnel</b></p>
<p>Choose a tunnel to delete.</p>
</DIV>
<DIV id=i_content10>
<h2>Add route</h2>
<p><b>Add</b></p>
<p>Add this tunnel.</p>
</DIV>
<DIV id=i_content11>
<h2>Delete route</h2>
<p><b>Delete</b></p>
<p>Delete this tunnel.</p>
</DIV>
<DIV id=i_content12>
<h2>Add 6to4 tunnel</h2>
<b>MTU</b>
<p>The MTU of the auto 6to4 tunnel. </p>
<p>Values: 1280-1500</p>
<p>Default: 1280</p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML

a=0
