#!/bin/sh

ERROR=""
REBOOT=""
LOGFILE="/var/fwupdate.log"

echo "prepair upload" > $LOGFILE
/etc/start_scripts/ata.sh stop >/dev/null 2>/dev/null
umount /opt/ >/dev/null 2>/dev/null

cleanup()
{
  echo "cleanup" >> $LOGFILE
  mount /opt/ >/dev/null 2>/dev/null
  /etc/start_scripts/ata.sh start >/dev/null 2>/dev/null
}


if [ "$REQUEST_METHOD" = "GET" ]; then
  echo 'Content-type: text/html'
  echo ''
  echo '<HTML><HEAD><TITLE>ERROR</TITLE></head><body>'
  echo 'You must not call this site this way!</body></html>'
  exit 0;
else
  ret=`fwupdate -H filename -r` 2>> $LOGFILE
  echo $ret >> $LOGFILE
  echo "Error Code: " $? >> $LOGFILE
fi

export REQUEST_METHOD="GET"
# Normalerweise werden die folgenden Zeilen nicht ausgefuehrt
if [ "$ret" != "OK" ]; then
 ERROR=$ret
 cleanup
 . ./fwupdate.cgi  
fi


