#!/bin/sh

if [ "$REQUEST_METHOD" = "GET" ]; then
  VARS=$QUERY_STRING
else
  VARS=`cat -`
fi
eval `echo $VARS|sed -e 's/&/\n/g'`

[ "$max" = "" ] && max=100
let wt=$w/2-10
let ht=$h/2+4
let wval=$w*$val/$max
[ "$einheit" = "" ] && einheit="%"
[ "$einheit" = "+" ] && einheit=" "

cat << EOF_SVG
Content-type: image/svg+xml
Connection: close

<?xml version="1.0" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<?xml-stylesheet type="text/css" href="../style.css" ?>
<svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

<rect class="svgbar_bg" x="0" y="0" width="$w" height="$h" />
<rect class="svgbar_color" x="0" y="0" width="$wval" height="$h" />
<rect class="svgbar_border" x="0" y="0" width="$w" height="$h" fill="none" />
<text class="svgbar_text" x="$wt" y="$ht" >$val$einheit</text>
</svg>
