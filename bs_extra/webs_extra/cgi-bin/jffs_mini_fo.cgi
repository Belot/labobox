#!/bin/sh

action=0

. ./login.cgi

f_pfad="Administration / JFFS2 and mini_fo"
f_name="JFFS2 and mini_fo"
HOME="admin_index.cgi"

PATH=.:$PATH

errmsg=""
mustsave=0


####### Save & Reboot #######
if [ "$action" -eq 2 ] || [ "$action" -eq 3 ]; then
   [ "$form_mini_fo" != "`nvram get mini_fo`" ] && res=`nvram set mini_fo=$form_mini_fo` && mustsave=1 && [ "$res" != "OK" ] && err=1
   
   if [ "$form_jffs" = "on" ] && [ "`nvram get jffs_clear`" != "1" ] ; then
	nvram set jffs_clear=1 >/dev/null 2>/dev/null
	mustsave=1
   elif [ "$form_jffs" != "on" ] && [ "`nvram get jffs_clear`" = "1" ] ; then
	nvram del jffs_clear >/dev/null 2>/dev/null
	mustsave=1
   fi  
   
   if [ "$form_mini_fo_erase" = "on" ] && [ "`nvram get mini_fo_clear`" != "1" ] ; then
	nvram set mini_fo_clear=1 >/dev/null 2>/dev/null
	mustsave=1
   elif [ "$form_mini_fo_erase" != "on" ] && [ "`nvram get mini_fo_clear`" = "1" ] ; then
	nvram del mini_fo_clear >/dev/null 2>/dev/null
	mustsave=1
   fi  

   [ "$err" -ne 0 ] && errmsg="$errmsg Write to NVRam failed!"'\n'

   [ "$mustsave" -eq 1 ] && nvram commit >/dev/null 
   [ "$action" -eq 3 ] && nvram reboot
fi

ON='checked="checked"'
SEL="selected"


jffs_check=`nvram get jffs_clear`
[ "$jffs_check" = "1" ] && jffs_check=$ON || jffs_check=""

mini_fo=`nvram get mini_fo`
[ "$mini_fo" = "Failed" ] && mini_fo="jffs"

if [ "$mini_fo" = "jffs" ]; then 
  mini_fo_jffs=$ON
elif [ "$mini_fo" = "ram" ]; then
  mini_fo_ram=$ON
else
  mini_fo_off=$ON
fi

mini_fo_erase=`nvram get mini_fo_clear`
[ "$mini_fo_erase" = "1" ] && mini_fo_erase=$ON || mini_fo_erase=""

cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function showErr() {
  var err="$errmsg";
  if(err!=""){alert(err);}
}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>$f_pfad</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<DIV id=c_titel>JFFS2</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0) >
<td width="150">Erase JFFS2 Partition:</td>
<td><input id="form_jffs" name="form_jffs" type="checkbox" $jffs_check></td>
</tr></table></DIV>
<DIV id=c_leer></DIV>

<DIV id=c_titel>mini_fo</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0)><td width="150">Enable mini_fo:</td>
<td width="20"><input type="radio" name="form_mini_fo" value="jffs" $mini_fo_jffs></td>
<td width="40">JFFS2</td>
<td width="20"><input type="radio" name="form_mini_fo" value="ram" $mini_fo_ram></td>
<td>RAM</td>
<td width="20"><input type="radio" name="form_mini_fo" value="off" $mini_fo_off></td>
<td>Off</td>
</tr></table>
</DIV>

<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(3) onmouseout=i_showElem(0) >
<td width="150">Erase mini_fo:</td>
<td><input id="form_mini_fo_erase" name="form_mini_fo_erase" type="checkbox" $mini_fo_erase></td>
</tr></table></DIV>


</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but2 onclick="subm(2);" onmouseover=rahmen(1,2) onmouseout=rahmen(0,2)>Save</DIV>
<DIV id=t_but3 onclick="subm(3);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Reboot</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>JFFS2 and mini_fo</h2>
<p>Administration of the JFFS2- and mini_fo-Filesystem.</p>
<p>JFFS2: Journalling Flash File System version 2</p>
<p>mini_fo: The mini fanout overlay file system</p>
</DIV>
<DIV id=i_content1>
<h2>JFFS2 and mini_fo</h2>
<p><b>Erase JFFS2 Partition</b></p>
<p>Erase the JFFS2 Partition. The whole Flash partition will be cleared.</p>
</DIV>
<DIV id=i_content2>
<h2>JFFS2 and mini_fo</h2>
<p><b>Enable mini_fo</b></p>
<p>Set how mini_fo should be used.</p>
<p>JFFS2: save all changes in the root filesystem permanently.</p>
<p>RAM: save all changes in the root filesystem in RAM until next reboot.</p>
<p>Off: don't use mini_fo. Read only root filesystem.</p>
<p>Default: JFFS2</p>
</DIV>
<DIV id=i_content3>
<h2>JFFS2 and mini_fo</h2>
<p><b>Erase mini_fo</b></p>
<p>Delete all files in mini_fo filesystem. Undo all changes on the root filesystem.</p>
</DIV>
</DIV></BODY></HTML>
