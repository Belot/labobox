#!/bin/sh

CONFIG_PART="/var/config.part"
CONFIG_SAVE="/var/config.save"

DEBUG=/var/config.debug
ERROR=""
REBOOT=""

> $DEBUG

if [ "$REQUEST_METHOD" = "GET" ]; then
  echo 'Content-type: text/html'
  echo ''
  echo '<HTML><HEAD><TITLE>ERROR</TITLE></HEAD><BODY>'
  echo 'You must not call this site this way!</BODY></HTML>'
  exit 0;
else
  cat - > $CONFIG_PART
fi


KEY=`/bin/getmultipart name=key file=$CONFIG_PART`
if [ "$?" != "0" ]; then
 rm $CONFIG_PART 2>/dev/null
 echo "Error: Key" >> $DEBUG
 ERROR=$ERROR"Error validating uploaded file!\n"
fi

/bin/getmultipart name=filename file=$CONFIG_PART > $CONFIG_SAVE
if [ "$?" != "0" ]; then
 rm $CONFIG_PART 2>/dev/null
 rm $CONFIG_SAVE 2>/dev/null
 ## Error
 echo "Error: $CONFIG_SAVE" >> $DEBUG
 ERROR=$ERROR"Error checking uploaded file!\n"
fi

ret=`/bin/nvram loadconfig "key=$KEY" file=$CONFIG_SAVE`
if [ "$ret" != "OK" ]; then
 echo "$ret" >> $DEBUG
 ret=`echo "$ret"|sed -e 's/.*\(corrupt\).*/\1/'`
 [ "$ret" = "corrupt" ] && ERROR=$ERROR"File corrupt or wrong password!\n" || ERROR="Error loading config-file!\n"
fi

rm $CONFIG_PART 2>/dev/null
rm $CONFIG_SAVE 2>/dev/null

if [ "$ERROR" = "" ]; then
 ERROR="Load configuration successfully. The Router will be automatically rebooted now!" 
 REBOOT="1"
fi

REQUEST_METHOD="GET"
. ./load.cgi 
