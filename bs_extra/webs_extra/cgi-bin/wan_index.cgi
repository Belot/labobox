#!/bin/sh

. ./login.cgi

[ "$ppp_restart" = "1" ] && /etc/start_scripts/dsl.sh ppp_reconnect >/dev/null 2>/dev/null

[ "$dsl_restart" = "1" ] && /etc/start_scripts/dsl.sh restart >/dev/null 2>/dev/null

wan_enabled=`nvram get dsl_start`
[ "$wan_enabled" = "Failed" ] && wan_enabled=0
[ "$wan_enabled" = "1" ] && wan_enabled="On" || wan_enabled="Off"

ppp_enabled=`nvram get ppp_start`
[ "$ppp_enabled" = "Failed" ] && ppp_enabled=0
[ "$ppp_enabled" = "1" ] && ppp_enabled="On" || ppp_enabled="Off"

ddns_enabled=`nvram get ddns_start`
[ "$ddns_enabled" = "Failed" ] && ddns_enabled=0
[ "$ddns_enabled" = "1" ] && ddns_enabled="On" || ddns_enabled="Off"

cat << EOF_HTML
Content-type: text/html
Connection: close

<html>
<head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
<SCRIPT type="text/JavaScript">
function ppp_restart() {
 check=confirm("PPP restart?");
 if(check==true)
   location.href="wan_index.cgi?ppp_restart=1";
}
function dsl_restart() {
 check=confirm("Restart the DSL and PPP connection?");
 if(check==true)
   location.href="wan_index.cgi?dsl_restart=1";
}
</SCRIPT>
</HEAD>
<BODY onselectstart="return false">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Network Settings / DSL/WAN</DIV>
<div id="c_titel">WAN Settings</div>
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr> <td width="200"><a href="dsl.cgi" onmouseover="i_showElem(1)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>DSL Settings</a></td>
<td>$wan_enabled</td></tr></table>
</div>
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr> <td width="200"><a href="ppp.cgi" onmouseover="i_showElem(5)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>PPP/Dial-In Settings</a></td>
<td>$ppp_enabled</td></tr></table>
</div>
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr> <td width="200"><a href="ddns.cgi" onmouseover="i_showElem(2)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>Dynamic DNS</a></td>
<td>$ddns_enabled</td></tr></table>
</div>
EOF_HTML

if [ "$ppp_enabled" = "On" ]; then
cat << EOF_HTML
<div id="c_link">
<table border="0" cellpadding="4" cellspacing="0">
    <tr><td width="200"><a href="javascript:ppp_restart();" onmouseover="i_showElem(3)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>PPP reconnect</a></td>
</tr></table>
</div>
EOF_HTML
fi

if [ "$wan_enabled" = "On" ]; then
cat << EOF_HTML
<div id="c_linklast">
<table border="0" cellpadding="4" cellspacing="0">
    <tr><td width="200"><a href="javascript:dsl_restart();" onmouseover="i_showElem(4)" onmouseout="i_showElem(0)" target="hcti"><b>&gt;&gt; </b>DSL restart</a></td>
</tr></table>
</div>
EOF_HTML
fi

cat << EOF_HTML
<DIV id="c_foot"></DIV>
</DIV></DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>DSL/WAN</h2>
<p></p>
</DIV>
<DIV id=i_content1>
<h2>DSL/WAN</h2>
<p><b>DSL Settings</b><br>
Configure DSL connection and parameters.
</p>
</DIV>
<DIV id=i_content2>
<h2>DSL/WAN</h2>
<p><b>Dynamic DNS</b><br>
Setup the dynamic DNS service.
</p>
</DIV>
<DIV id=i_content3>
<h2>DSL/WAN</h2>
<p><b>PPP reconnect</b><br>
Restart the PPP client and the PPP connection.
</p>
</DIV>
<DIV id=i_content4>
<h2>DSL/WAN</h2>
<p><b>DSL restart</b><br>
Stop and restart the DSL and PPP connection.
</p>
</DIV>
<DIV id=i_content5>
<h2>DSL/WAN</h2>
<p><b>PPP/Dial-In Settings</b><br>
Configure PPP/Dial-In settings e.g. for your DSL-Provider.
</p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML

a=0