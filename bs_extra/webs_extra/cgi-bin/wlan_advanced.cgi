#!/bin/sh

from=wlan_basic.cgi
action=0

. ./login.cgi

f_pfad="WLAN Settings / Advanced settings"
f_name="Advanced Settings"
HOME="wlan_index.cgi"
. ./functions.sh

PATH=.:$PATH

errmsg=""
mustsave=0

ap=0
( [ "`nvram get wl_infra`" = "1" ] || [ "`nvram get wl_infra`" = "Failed" ] ) && ( [ "`nvram get wl_ap`" = "1" ] || [ "`nvram get wl_ap`" = "Failed" ] ) && ap=1
bmode=0
[ "`nvram get wl_gmode`" = "0" ] && bmode=1

####### Save & Run #######
if [ "$action" -eq 2 ] || [ "$action" -eq 3 ]; then

  [ "$sel_rate" != "`nvram get wl_rate`" ] && res=`nvram set wl_rate=$sel_rate` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$sel_mrate" != "`nvram get wl_mrate`" ] && res=`nvram set wl_mrate=$sel_mrate` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$sel_wl_pwr_percent1" != "`nvram get wl_pwr_percent1`" ] && res=`nvram set wl_pwr_percent1=$sel_wl_pwr_percent1` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$sel_txant" != "`nvram get wl_txant`" ] && res=`nvram set wl_txant=$sel_txant` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$sel_antdiv" != "`nvram get wl_antdiv`" ] && res=`nvram set wl_antdiv=$sel_antdiv` && mustsave=1 && [ "$res" != "OK" ] && err=1
  [ "$sel_shortslot_override" != "`nvram get wl_shortslot_override`" ] && res=`nvram set wl_shortslot_override=$sel_shortslot_override` && mustsave=1 && [ "$res" != "OK" ] && err=1
  if [ "$text_wl_fragthresh" -ge "256" -o "$text_wl_fragthresh" -le "2346" ]; then 
    [ "$text_wl_fragthresh" != "`nvram get wl_fragthresh`" ] && res=`nvram set wl_fragthresh=$text_wl_fragthresh` && mustsave=1 && [ "$res" != "OK" ] && err=1
  else
    errmsg="$errmsg Invalid fragmentation threshold!"'\n'
  fi
  if [ "$text_wl_rtsthresh" -ge "0" -o "$text_wl_rtsthresh" -le "2347" ]; then 
    [ "$text_wl_rtsthresh" != "`nvram get wl_rtsthresh`" ] && res=`nvram set wl_rtsthresh=$text_wl_rtsthresh` && mustsave=1 && [ "$res" != "OK" ] && err=1
  else
    errmsg="$errmsg Invalid RTS threshold!"'\n'
  fi
  if [ "$text_cwmin" -ge "1" -o "$text_cwmin" -le "255" ]; then 
    [ "$text_cwmin" != "`nvram get wl_cwmin`" ] && res=`nvram set wl_cwmin=$text_cwmin` && mustsave=1 && [ "$res" != "OK" ] && err=1
  else
    errmsg="$errmsg Invalid CWmin threshold!"'\n'
  fi
  if [ "$text_cwmax" -ge "256" -o "$text_cwmax" -le "2047" ]; then 
    [ "$text_cwmax" != "`nvram get wl_cwmax`" ] && res=`nvram set wl_cwmax=$text_cwmax` && mustsave=1 && [ "$res" != "OK" ] && err=1
  else
    errmsg="$errmsg Invalid CWmax threshold!"'\n'
  fi
  if [ "$bmode" = "0" ]; then
    [ "$sel_gmode_protection_override" != "`nvram get wl_gmode_protection_override`" ] && res=`nvram set wl_gmode_protection_override=$sel_gmode_protection_override` && mustsave=1 && [ "$res" != "OK" ] && err=1
    [ "$sel_wl_gmode_protection_control" != "`nvram get wl_gmode_protection_control`" ] && res=`nvram set wl_gmode_protection_control=$sel_wl_gmode_protection_control` && mustsave=1 && [ "$res" != "OK" ] && err=1
    [ "$form_wl_gmode_protection_cts" != "`nvram get wl_gmode_protection_cts`" ] && res=`nvram set wl_gmode_protection_cts=$form_wl_gmode_protection_cts` && mustsave=1 && [ "$res" != "OK" ] && err=1
    [ "$form_wl_frameburst" != "`nvram get wl_frameburst`" ] && res=`nvram set wl_frameburst=$form_wl_frameburst` && mustsave=1 && [ "$res" != "OK" ] && err=1
  fi
  if [ "$ap" = "1" ]; then
    if [ "$text_wl_bi" -ge "10" -o "$text_wl_bi" -le "65535" ]; then 
      [ "$text_wl_bi" != "`nvram get wl_bi`" ] && res=`nvram set wl_bi=$text_wl_bi` && mustsave=1 && [ "$res" != "OK" ] && err=1
    else
      errmsg="$errmsg Invalid beacon interval!"'\n'
    fi
    if [ "$text_wl_dtim" -ge "1" -o "$text_wl_dtim" -le "255" ]; then 
      [ "$text_wl_dtim" != "`nvram get wl_dtim`" ] && res=`nvram set wl_dtim=$text_wl_dtim` && mustsave=1 && [ "$res" != "OK" ] && err=1
    else
      errmsg="$errmsg Invalid DTIM interval!"'\n'
    fi
    if [ "$text_wl_scb_timeout" -ge "1" -o "$text_wl_scb_timeout" -le "1440" ]; then 
      [ "$text_wl_scb_timeout" != "`nvram get wl_scb_timeout`" ] && res=`nvram set wl_scb_timeout=$text_wl_scb_timeout` && mustsave=1 && [ "$res" != "OK" ] && err=1
    else
      errmsg="$errmsg Invalid SCB timeout!"'\n'
    fi
    [ "$form_wl_shortslot_restrict" != "`nvram get wl_shortslot_restrict`" ] && res=`nvram set wl_shortslot_restrict=$form_wl_shortslot_restrict` && mustsave=1 && [ "$res" != "OK" ] && err=1
  else
    [ "$sel_wl_PM" != "`nvram get wl_PM`" ] && res=`nvram set wl_PM=$sel_wl_PM` && mustsave=1 && [ "$res" != "OK" ] && err=1
    [ "$form_wl_watch" != "`nvram get wl_watch`" ] && res=`nvram set wl_watch=$form_wl_watch` && mustsave=1 && [ "$res" != "OK" ] && err=1
    if [ "$form_wl_watch" = "1" ]; then
      if [ "$text_watch_int" -ge "1" -o "$text_watch_int" -le "999" ]; then 
        [ "$text_watch_int" != "`nvram get wl_watch_intervall`" ] && res=`nvram set wl_watch_intervall=$text_watch_int` && mustsave=1 && [ "$res" != "OK" ] && err=1
      else
        errmsg="$errmsg Invalid connection watch interval!"'\n'
      fi
    fi
  fi
  [ "$err" -ne 0 ] && errmsg="$errmsg Write to NVRam failed!"'\n'
  
  [ "$mustsave" -eq 1 ] && nvram commit >/dev/null && [ "$action" -eq 3 ] && /etc/start_scripts/wlan.sh restart >/dev/null 2>/dev/null
fi
#############

set_select()
{
  var=$1
  default=$2
  value=`nvram get $var`
  [ "$value" = "Failed" ] && value=$default 
  if [ $value -eq 1 ]; then
    eval `echo ${var}_on=$ON`
    eval `echo ${var}_off=''`
  else
    eval `echo ${var}_on=''`
    eval `echo ${var}_off=$ON`
  fi
}

ON='checked="checked"'
SEL="selected"


  wl_rate=`nvram get wl_rate`
  [ "$wl_rate" = "Failed" ] && wl_rate=-1
  wl_mrate=`nvram get wl_mrate`
  [ "$wl_mrate" = "Failed" ] && wl_mrate=-1
  if [ "$bmode" = 0 ]; then
    wl_gmode_protection_override=`nvram get wl_gmode_protection_override`
    [ "$wl_gmode_protection_override" = "Failed" ] && wl_gmode_protection_override=-1
    case "$wl_gmode_protection_override" in
                  -1)
                      sel_gmodeprotover_auto=$SEL
                      ;;
                  0)
                      sel_gmodeprotover_ena=$SEL
                      ;;
                  1)
                      sel_gmodeprotover_dis=$SEL
                      ;;
    esac
    wl_gmode_protection_control=`nvram get wl_gmode_protection_control`
    [ "$wl_gmode_protection_control" = "Failed" ] && wl_gmode_protection_control=0
    case "$wl_gmode_protection_control" in
                  0)
                      sel_gmodeprot_off=$SEL
                      ;;
                  1)
                      sel_gmodeprot_local=$SEL
                      ;;
                  2)
                      sel_gmodeprot_overlap=$SEL
                      ;;
    esac
    set_select "wl_gmode_protection_cts" 1
    set_select "wl_frameburst" 0
  fi
  
  if [ "$ap" = "1" ]; then
    wl_bi=`nvram get wl_bi`
    [ "$wl_bi" = "Failed" ] && wl_bi=100
    wl_dtim=`nvram get wl_dtim`
    [ "$wl_dtim" = "Failed" ] && wl_dtim=1
    wl_scb_timeout=`nvram get wl_scb_timeout`
    [ "$wl_scb_timeout" = "Failed" ] && wl_scb_timeout=60
    set_select "wl_shortslot_restrict" 0
  fi
  wl_fragthresh=`nvram get wl_fragthresh`
  [ "$wl_fragthresh" = "Failed" ] && wl_fragthresh=2346
  wl_rtsthresh=`nvram get wl_rtsthresh`
  [ "$wl_rtsthresh" = "Failed" ] && wl_rtsthresh=2347
  wl_cwmin=`nvram get wl_cwmin`
  [ "$wl_cwmin" = "Failed" ] && wl_cwmin=31
  wl_cwmax=`nvram get wl_cwmax`
  [ "$wl_cwmax" = "Failed" ] && wl_cwmax=1023
  
  wl_antdiv=`nvram get wl_antdiv`
  [ "$wl_antdiv" = "Failed" ] && wl_antdiv=3
  case "$wl_antdiv" in
                0)
                    sel_antdiv_ant0=$SEL
                    ;;
                1)
                    sel_antdiv_ant1=$SEL
                    ;;
                3)
                    sel_antdiv_auto=$SEL
                    ;;
  esac
  wl_txant=`nvram get wl_txant`
  [ "$wl_txant" = "Failed" ] && wl_txant=3
  case "$wl_txant" in
                0)
                    sel_txant_ant0=$SEL
                    ;;
                1)
                    sel_txant_ant1=$SEL
                    ;;
                3)
                    sel_txant_auto=$SEL
                    ;;
  esac
  wl_shortslot_override=`nvram get wl_shortslot_override`
  [ "$wl_shortslot_override" = "Failed" ] && wl_shortslot_override=-1
  case "$wl_shortslot_override" in
               -1)
                   sel_shorttover_auto=$SEL
                   ;;
               0)
                   sel_shorttover_ena=$SEL
                   ;;
               1)
                   sel_shorttover_dis=$SEL
                   ;;
  esac
  
  if [ "$ap" = "0" ]; then
    wl_PM=`nvram get wl_PM`
    [ "$wl_PM" = "Failed" ] && wl_PM=0
    case "$wl_PM" in
                0)
                    sel_pm_cam=$SEL
                    ;;
                1)
                    sel_pm_ps=$SEL
                    ;;
                2)
                    sel_pm_fast_ps=$SEL
                    ;;
    esac

    set_select "wl_watch" 1
    wl_watch_int=`nvram get wl_watch_intervall`
    [ "$wl_watch_int" = "Failed" ] && wl_watch_int=5
  fi


  wl_pwr_percent1=`nvram get wl_pwr_percent1`
  [ "$wl_pwr_percent1" = "Failed" ] && wl_pwr_percent1=100

  

cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function showErr() {
  var err="$errmsg";
  if(err!=""){alert(err);}
}
function enable_watch(state)
{
        var input = document.getElementById('text_watch_int');
        if(!input) return;

        if (state == true)
{
                input.disabled=false;
}	
        else
                input.disabled=true;
}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>$f_pfad</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<DIV id=c_titel>Data rate</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0)>
<td width="200">Fixed data rate</td>
<td>
  <select name="sel_rate" size="1">
HTML
sel=""
[ "$wl_rate" = "-1" ] && sel="selected"
echo "    <option value=\"-1\" $sel>auto</option>"

list="1 2 5.5 6 9 11 12 18 24 36 48 54"
[ "$bmode" = "1" ] && list="1 2 5.5 6 9 11"
sel=""
for i in $list
do
   [ "$i" = "$wl_rate" ] && sel="selected"
   echo "    <option value=\"$i\" $sel>$i Mbps</option>"
   sel=""
done

cat <<HTML

  </select>
</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0)>
<td width="200">Fixed multicast data rate</td>
<td>
  <select name="sel_mrate" size="1">
HTML
sel=""
[ "$wl_mrate" = "-1" ] && sel="selected"
echo "    <option value=\"-1\" $sel>auto</option>"

list="1 2 5.5 6 9 11 12 18 24 36 48 54"
[ "$bmode" = "1" ] && list="1 2 5.5 6 9 11"
sel=""
for i in $list
do
   [ "$i" = "$wl_mrate" ] && sel="selected"
   echo "    <option value=\"$i\" $sel>$i Mbps</option>"
   sel=""
done
cat <<HTML
  </select>
</td>
</tr></table></DIV>
HTML

echo '<DIV id=c_leer></DIV>'

if [ "$bmode" = "0" ]; then
  echo '<DIV id=c_titel>G-mode specific options</DIV>'
  echo '<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(3) onmouseout=i_showElem(0)>' \
  '<td width="200">G-mode protection override</td><td><select name="sel_gmode_protection_override" size="1">'
  echo "<option value=\"-1\" $sel_gmodeprotover_auto>Auto</option>"
  echo "<option value=\"0\" $sel_gmodeprotover_dis>Disabled</option>"
  echo "<option value=\"1\" $sel_gmodeprotover_ena>Enabled</option>"
  echo '</select></td></tr></table></DIV>'
  echo '<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(4) onmouseout=i_showElem(0)>' \
  '<td width="200">G-mode protection control</td><td><select name="sel_wl_gmode_protection_control" size="1">'
  echo "<option value=\"0\" $sel_gmodeprot_off>Always off</option>"
  echo "<option value=\"1\" $sel_gmodeprot_local>Monitor local association</option>"
  echo "<option value=\"2\" $sel_gmodeprot_overlap>Monitor overlapping BSS</option>"
  echo '</select></td></tr></table></DIV>'
  echo '<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(5) onmouseout=i_showElem(0)>' \
  '<td width="200">CTS protection mode</td>'
  echo "<td width=\"20\"><input type=\"radio\" name=\"form_wl_gmode_protection_cts\" value=\"0\" $wl_gmode_protection_cts_off></td>"
  echo '<td width="40">Off</td> '
  echo "<td width=\"20\"><input type=\"radio\" name=\"form_wl_gmode_protection_cts\" value=\"1\" $wl_gmode_protection_cts_on></td>"
  echo '<td>On</td></tr></table></DIV>'
  echo '<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(6) onmouseout=i_showElem(0)>' \
  '<td width="200">Frameburst</td>'
  echo "<td width=\"20\"><input type=\"radio\" name=\"form_wl_frameburst\" value=\"0\" $wl_frameburst_off></td>"
  echo '<td width="40">Off</td> '
  echo "<td width=\"20\"><input type=\"radio\" name=\"form_wl_frameburst\" value=\"1\" $wl_frameburst_on></td>"
  echo '<td>On</td></tr></table></DIV>'
  echo '<DIV id=c_leer></DIV>'
fi



if [ "$ap" = "1" ]; then
  echo '<DIV id=c_titel>AP specific options</DIV>'
  echo '<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2">'
  echo '<tr onmouseover=i_showElem(7) onmouseout=i_showElem(0)><td width="200">Beacon interval</td>'
  echo "<td><input id=\"text_wl_bi\" name=\"text_wl_bi\" size=\"5\" max_size=\"5\" value=\"$wl_bi\"></td>"
  echo '</tr></table></DIV>'
  echo '<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2">'
  echo '<tr onmouseover=i_showElem(8) onmouseout=i_showElem(0)><td width="200">DTIM interval</td>'
  echo "<td><input id=\"text_wl_dtim\" name=\"text_wl_dtim\" size=\"3\" max_size=\"3\" value=\"$wl_dtim\"></td>"
  echo '</tr></table></DIV>'
  echo '<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2">'
  echo '<tr onmouseover=i_showElem(9) onmouseout=i_showElem(0)><td width="200">SCB timeout</td>'
  echo "<td><input id=\"text_wl_scb_timeout\" name=\"text_wl_scb_timeout\" size=\"5\" max_size=\"5\" value=\"$wl_scb_timeout\"></td>"
  echo '</tr></table></DIV>'
  echo '<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(10) onmouseout=i_showElem(0)>' \
  '<td width="200">Shortslot restrict</td>'
  echo "<td width=\"20\"><input type=\"radio\" name=\"form_wl_shortslot_restrict\" value=\"0\" $wl_shortslot_restrict_off></td>"
  echo '<td width="40">Off</td> '
  echo "<td width=\"20\"><input type=\"radio\" name=\"form_wl_shortslot_restrict\" value=\"1\" $wl_shortslot_restrict_on></td>"
  echo '<td>On</td></tr></table></DIV>'
else
  echo '<DIV id=c_titel>Client specific options</DIV>'
  echo '<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(11) onmouseout=i_showElem(0)>' \
  '<td width="200">Power saving mode</td><td><select name="sel_wl_PM" size="1">'
  echo "<option value=\"0\" $sel_pm_cam>Constantly awake</option>"
  echo "<option value=\"1\" $sel_pm_ps>Power saving</option>"
  echo "<option value=\"2\" $sel_pm_fast_ps>Fast power saving</option>"
  echo '</select></td></tr></table></DIV>'
  
  echo '<DIV id=c_std><table border="0" cellpadding="0" cellspacing="2"><tr onmouseover=i_showElem(21) onmouseout=i_showElem(0)>' \
  '<td width="200">Connection watch</td>'
  echo "<td width=\"20\"><input type=\"radio\" id=\"form_wl_watch\" onchange=\"enable_watch(false);\" name=\"form_wl_watch\" value=\"0\" $wl_watch_off></td>"
  echo '<td width="40">Off</td> '
  echo "<td width=\"20\"><input type=\"radio\" id=\"form_wl_watch\" onchange=\"enable_watch(true);\" name=\"form_wl_watch\" value=\"1\" $wl_watch_on></td>"
  echo '<td>On</td></tr></table></DIV>'
  echo '<DIV id=c_std>'
  echo '<table border="0" cellpaddig="0" cellspacing="2">'
  echo '<tr onmouseover=i_showElem(22) onmouseout=i_showElem(0) >'
  echo '<td width="200">Watch intervall</td>'
  echo "<td><input id=\"text_watch_int\" name=\"text_watch_int\" size=\"3\" maxlength=\"3\" type=\"text\" value=\"$wl_watch_int\"></td>
  </tr></table></DIV>"
fi

cat <<HTML
<DIV id=c_leer></DIV>
<DIV id=c_titel>Thresholds/MAC-Layer</DIV>
<DIV id=c_std>
<table border="0" cellpaddig="0" cellspacing="2">
<tr onmouseover=i_showElem(12) onmouseout=i_showElem(0) >
<td width="200">Fragmentation threshold</td>
<td><input id="text_wl_fragthresh" name="text_wl_fragthresh" size="4" maxlength="4" type="text" value="$wl_fragthresh"></td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpaddig="0" cellspacing="2">
<tr onmouseover=i_showElem(13) onmouseout=i_showElem(0) >
<td width="200">RTS threshold</td>
<td><input id="text_wl_rtsthresh" name="text_wl_rtsthresh" size="4" maxlength="4" type="text" value="$wl_rtsthresh"></td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpaddig="0" cellspacing="2">
<tr onmouseover=i_showElem(14) onmouseout=i_showElem(0) >
<td width="200">CWmin</td>
<td><input id="text_cwmin" name="text_cwmin" size="3" maxlength="3" type="text" value="$wl_cwmin"></td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpaddig="0" cellspacing="2">
<tr onmouseover=i_showElem(15) onmouseout=i_showElem(0) >
<td width="200">CWmax</td>
<td><input id="text_cwmax" name="text_cwmax" size="4" maxlength="4" type="text" value="$wl_cwmax"></td>
</tr></table></DIV>
<DIV id=c_leer></DIV>
<DIV id=c_titel>Physical</DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(16) onmouseout=i_showElem(0)>
<td width="200">TX power</td>
<td>
  <select name="sel_wl_pwr_percent1" size="1">
HTML
list="1 5 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100"
sel=""
for i in $list
do
   [ "$i" = "$wl_pwr_percent1" ] && sel="selected"
   echo "    <option value=\"$i\" $sel>$i %</option>"
   sel=""
done
echo '</select></td></tr></table></DIV>'

cat <<HTML
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(17) onmouseout=i_showElem(0)>
<td width="200">TX antenna</td>
<td>
  <select name="sel_txant" size="1">
    <option value="0" $sel_txant_ant0>Antenna 0</option>
    <option value="1" $sel_txant_ant1>Antenna 1</option>
    <option value="3" $sel_txant_auto>Auto</option>
  </select>
</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(18) onmouseout=i_showElem(0)>
<td width="200">RX antenna</td>
<td>
  <select name="sel_antdiv" size="1">
    <option value="0" $sel_antdiv_ant0>Antenna 0</option>
    <option value="1" $sel_antdiv_ant1>Antenna 1</option>
    <option value="3" $sel_antdiv_auto>Auto</option>
  </select>
</td>
</tr></table></DIV>
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(19) onmouseout=i_showElem(0)>
<td width="200">Shortslot override</td>
<td>
  <select name="sel_shortslot_override" size="1">
    <option value="-1" $sel_shorttover_auto>Auto</option>
    <option value="0" $sel_shorttover_dis>Disabled</option>
    <option value="1" $sel_shorttover_ena>Enabled</option>
  </select>
</td>
</tr></table></DIV>
</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>WLAN Advanced</h2>
<p>Configure advanced wireless settings.</p>
</DIV>
<DIV id=i_content1>
<h2>Data rate</h2>
<p>Select the data rate for unicast traffic.</p>
</DIV>
<DIV id=i_content2>
<h2>Multicast data rate</h2>
<p>Select the data rate for multicast traffic.</p>
</DIV>
<DIV id=i_content3>
<h2>G-mode protection override</h2>
<p>Turn G-mode protection <br>on</br>, <br>off</br> or to <br>auto</br>.</p>
</DIV>
<DIV id=i_content4>
<h2>G-mode protection control</h2>
<p>Set G-mode protection monitoring.</p>
</DIV>
<DIV id=i_content5>
<h2>CTS protection mode</h2>
<p>Turn CTS protection <b>on</b> or <b>off</b>.</p>
</DIV>
<DIV id=i_content6>
<h2>Frameburst</h2>
<p>Turn framebursting <b>on</b> or <b>off</b>.</p>
</DIV>
<DIV id=i_content7>
<h2>Beacon interval</h2>
<p>Set interval to send Beacon frames in milliseconds.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> range 10-65535ms, default=100ms</p>
</DIV>
<DIV id=i_content8>
<h2>DTIM interval</h2>
<p>Set interval to send Beacon frames with DTIM field to wake up clients in powersave-mode.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> range 1-255, default=1</p>
</DIV>
<DIV id=i_content9>
<h2>SCB timeout</h2>
<p>Set inactivity timeout value for authenticated stas in minutes.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> range 1-1440, default=60min</p>
</DIV>
<DIV id=i_content10>
<h2>Shortslot restrict</h2>
<p>Turn this <br>on</br> to restrict clients with shortslot capability.</p>
</DIV>
<DIV id=i_content11>
<h2>Power saving mode</h2>
<p>Select the powersaving-mode when acting in client- or adhoc-mode.</p>
</DIV>
<DIV id=i_content12>
<h2>Fragmentation threshold</h2>
<p>Enter fragmentation threshold in bytes.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> range 256-2346, default=2346</p>
</DIV>
<DIV id=i_content13>
<h2>RTS threshold</h2>
<p>Enter RTS threshold.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> range 0-2347, default=2347</p>
</DIV>
<DIV id=i_content14>
<h2>CWmin</h2>
<p>Enter CWmin which influences the Backoff time for the MAC-layer.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> range 1-255, default=31</p>
</DIV>
<DIV id=i_content15>
<h2>CWmax</h2>
<p>Enter CWmax which influences the Backoff time for the MAC-layer.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> range 256-2047, default=1023</p>
</DIV>
<DIV id=i_content16>
<h2>TX power</h2>
<p>Select the transmit power in percent.</p>
</DIV>
<DIV id=i_content17>
<h2>TX antenna</h2>
<p>Select the transmit antenna.</p>
</DIV>
<DIV id=i_content18>
<h2>RX antenna</h2>
<p>Select the receive antenna.</p>
</DIV>
<DIV id=i_content19>
<h2>RX antenna</h2>
<p>Select the receive antenna.</p>
</DIV>
<DIV id=i_content20>
<h2>Shortslot override</h2>
<p>Set use of shortslot capability.</p>
</DIV>
<DIV id=i_content21>
<h2>Connection watch</h2>
<p>Turn this <br>on</br> to periodically check if the WLAN client connection is established and 
otherwise try to reconnect.</p>
<DIV id=i_content22>
<h2>Watch intervall</h2>
<p>Enter watch intervall in seconds.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> range 1-999, default=5</p>
</DIV>
</DIV></BODY></HTML>
