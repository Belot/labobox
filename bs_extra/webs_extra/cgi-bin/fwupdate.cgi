#!/bin/sh

. ./login.cgi


bs_version=`cat /etc/version`

cat << EOF_HTML
Content-type: text/html
Connection: close

<html><head>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function subm(x) {
load();
alert("The Router automatically reboot after successful firmware update!");
parent.banner.set_restart_timer();
document.getElementById("xform").submit();
}
function showErr() {
var err="$ERROR";
parent.banner.clear_restart_timer();
if(err!=""){alert(err);}
}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
  <!--[if IE]>
    <style type="text/css">@import url(../ie.css);</style>
  <![endif]-->
</HEAD>
<BODY onload="showErr();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Save &amp; Load / Firmware Update</DIV>
<DIV id=c_titel>Firmware Update <span style="text-decoration:none; font-weight:	normal;">(current version: $bs_version)</span></DIV>
<form id="xform" name="xform" method="post" action="fwupdatecgi.cgi" enctype="multipart/form-data">
<DIV id=c_std>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0)>
<td width="60">File:</td>
<td><input type="file" name="filename" size="30" maxlength="4000000"></td></tr></table>
</DIV>
</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but4 onclick="subm();" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Load &lt;&lt;</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Firmware Update</h2>
<p>Update the routers firmware.</p>
<p>During the firmware update the router stops all its services. The update takes about a minute and another minute to reboot.</p>
</DIV>
<DIV id=i_content1>
<h2>Firmware Update</h2>
<p><b>File</b><br>
Choose the firmware image file to upload.</p>
</DIV>
</DIV></BODY></HTML>
EOF_HTML

a=0
exit
