#!/bin/sh

action=0

. ./login.cgi

f_pfad="Firewall Settings / Port filter"
f_name="Port filter"
HOME="fw_index.cgi"
. ./functions.sh

PATH=.:$PATH

errmsg=""
mustsave=0

########################### Add #####################################
if [ "$action" -eq 10 ]; then
  rule_list=`nvram get fw_web_port_rules`
  [ "$rule_list" = "Failed" ] && rule_list=""
  rule_list=$rule_list"##port###;"
  nvram set fw_web_port_rules="$rule_list" >/dev/null
fi

################################## Del #####################################
if [ "$action" -eq 11 ] && [ "$rule_count" != "0" ]; then
  rule_list=`nvram get fw_web_port_rules`
  [ "$rule_list" = "Failed" ] && rule_list=""
  rule_list=`echo $rule_list|sed -e 's/ /%20/g'|sed -e 's/;/ /g'`
  i=0
  new_list=""
  for rule in $rule_list
  do
    rule=`echo $rule|sed -e 's/%20/ /g'`
    type=`echo $rule|sed -e 's/.*#.*#\(.*\)#.*#.*#.*/\1/g'`
    [ "$type" != "port" ] new_list=$new_list"$rule;" && continue
    [ "$i" = "$del_number" ] && let "i+=1" && continue
    new_list=$new_list"$rule;"
    let "i+=1"
  done
  nvram set fw_web_port_rules="$new_list" >/dev/null
  nvram commit >/dev/null
fi

####### Save & Run #######
if [ "$rule_count" != "0" ] && ( [ "$action" -eq 2 ] || [ "$action" -eq 3 ] ); then
  #first collect all range rules
  rule_list=`nvram get fw_web_port_rules`
  [ "$rule_list" = "Failed" ] && rule_list=""
  rule_list=`echo $rule_list|sed -e 's/ /%20/g'|sed -e 's/;/ /g'`
  new_list=""
  for rule in $rule_list
  do
    rule=`echo $rule|sed -e 's/%20/ /g'`
    type=`echo $rule|sed -e 's/.*#.*#\(.*\)#.*#.*#.*/\1/g'`
    [ "$type" != "port" ] && new_list=$new_list"$rule;"
  done

  #now go through all single port rules
  i=0
  myerr=0
  while [ $i -lt $rule_count ]
  do
    http_unquote "text_dstport$i"
    eval "port=\$text_dstport$i"
#port=`echo $port|sed -e 's/[^0-9]//g'`
    port=`echo $port|sed -e 's/[^0-9,\,,:,-]//g'`
#if [ "$port" -lt "1" -o "$port" -gt "65535" ]; then 
#      errmsg="$errmsg Dst-port in rule $i is invalid!"'\n'
#      myerr=1
#    fi
    http_unquote "text_src_ip$i"
    eval "ipsave=\$text_src_ip$i"
    ipsave=`echo $ipsave|sed -e 's/[^-0-9\/.]//g'`
    #[ "`echo $ipsave|awk '{split($0,a,"-"); print a[2];}'`" != "" ] && ipsave=`echo $ipsave|sed -e 's/,//g'`
    #iplist=`echo $ipsave|sed -e 's/,/ /g'`
    ip=$ipsave
    #for ip in $iplist
    #do
      ip1=`echo $ip|awk '{split($0,a,"-"); print a[1];}'`
      ip2=`echo $ip|awk '{split($0,a,"-"); print a[2];}'`
      ip=$ip1
      ip_check=`isip.sh $ip`
      [ "$ip_check" = "ERROR" ] && errmsg="$errmsg Invalid IP-address \'$ip\' in rule $i!"'\n' && myerr=1  && let "i+=1" && continue
      if [ "$ip2" != "" ]; then
        ip=$ip2
        ip_check=`isip.sh $ip`                                                                                           
        [ "$ip_check" = "ERROR" ] && errmsg="$errmsg Invalid IP-address \'$ip\' in rule $i!"'\n' && myerr=1  && let "i+=1" && continue
      fi  
    #done

    http_unquote "text_name$i"
    eval "name=\$text_name$i"
    eval "prot=\$sel_prot$i"
    eval "ena=\$check_ena$i"
    rule="$name#$ipsave#port#$prot#$port#$ena"
    new_list=$new_list"$rule;"
    let "i+=1"
  done
  [ "$myerr" = "0" ] && res=`nvram set fw_web_port_rules="$new_list"` && mustsave=1 && [ "$res" != "OK" ] && err=1

  [ "$err" -ne 0 ] && errmsg="$errmsg Write to NVRam failed!"'\n'

  [ "$mustsave" -eq 1 ] && nvram commit >/dev/null && [ "$action" -eq 3 ] && /etc/start_scripts/firewall.sh web_and_port_restart >/dev/null 2>/dev/null
elif [ "$rule_count" = "0" ] && [ "$action" -eq 3 ]; then
  /etc/start_scripts/firewall.sh web_and_port_restart
fi

ON='checked=\"checked\"'
SEL="selected"

rule_list=`nvram get fw_web_port_rules`
[ "$rule_list" = "Failed" ] && rule_list=""
rule_list=`echo $rule_list|sed -e 's/ /%20/g'|sed -e 's/;/ /g'`
#go through each rule

cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>
<SCRIPT language="JavaScript" src="../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function showErr() {
  var err="$errmsg";
  if(err!=""){alert(err);}
}
function doAdd() {
  load();
  document.getElementById("action").value=10;
  document.getElementById("xform").submit();
}
function doDel(number) {
  var count=document.getElementById("rule_count").value;
  if(count==0){
    alert("There's no rule to delete!");
    return;
  }
  else{
    load();
    document.getElementById("action").value=11;
    document.getElementById("del_number").value=number;
    document.getElementById("xform").submit();
  }
}
</SCRIPT>
<LINK rel="stylesheet" href="../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="showErr();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>$f_pfad</DIV>
<form id="xform" name="xform" method="post">
<input id="action" type="hidden" name="action" value="0">
<DIV id=c_titel>Port filtering rules</DIV>
<table border="0" cellpadding="0" cellspacing="2">
<tr>
<th width="30">Nr</th>
<th width="120">Rule name</th>
<th width="120">Src-IP</th>
<th width="90">Protocol</th>
<th width="150">Dst-Ports</th>
<th width="50">Enabled</th>
<th></th>
</tr>
HTML
i=0
if [ "$rule_list" = "" ]; then
  echo '<tr><td colspan="6" align="center">no rules specified</td></tr>'
else
  #Port filtering rules
  for rule in $rule_list
  do
    rule=`echo $rule|sed -e 's/%20/ /g'`
    type=`echo $rule|sed -e 's/.*#.*#\(.*\)#.*#.*#.*/\1/g'`
    [ "$type" != "port" ] && continue
    name=`echo $rule|sed -e 's/\(.*\)#.*#.*#.*#.*#.*/\1/g'`
    prot=`echo $rule|sed -e 's/.*#.*#.*#\(.*\)#.*#.*/\1/g'`
    sel_prot_tcp=""
    sel_prot_udp=""
    sel_prot_both=""
    case "$prot" in
              "tcp")
                  sel_prot_tcp=$SEL
                  ;;
              "udp")
                  sel_prot_udp=$SEL
                  ;;
              "both")
                  sel_prot_both=$SEL
                  ;;
              *)
                  ;;
    esac
    src_ip=`echo $rule|sed -e 's/.*#\(.*\)#.*#.*#.*#.*/\1/g'`
    dst_port=`echo $rule|sed -e 's/.*#.*#.*#.*#\(.*\)#.*/\1/g'`
    ena=`echo $rule|sed -e 's/.*#.*#.*#.*#.*#\(.*\)/\1/g'`
    check_ena_on=""
    [ "$ena" = "1" ] && check_ena_on=$ON
    echo '<tr>'
    echo "<td><input id=\"text_nr$i\" name=\"text_nr$i\" size=\"3\" maxlength=\"4\" type=\"text\" value=\"$i\" disabled></td>"
    echo "<td onmouseover=i_showElem(1) onmouseout=i_showElem(0)><input id=\"text_name$i\" name=\"text_name$i\" size=\"15\" maxlength=\"25\" type=\"text\" value=\"$name\"></td>"
    echo "<td onmouseover=i_showElem(2) onmouseout=i_showElem(0) align=\"center\"><input id=\"text_src_ip$i\" name=\"text_src_ip$i\" size=\"31\" maxlength=\"120\" type=\"text\" value=\"$src_ip\"></td>"
    echo "<td onmouseover=i_showElem(3) onmouseout=i_showElem(0) align=\"center\"><select id=\"sel_prot$i\" name=\"sel_prot$i\" size=\"1\">"
    echo "<option value=\"tcp\" $sel_prot_tcp>TCP</option>"
    echo "<option value=\"udp\" $sel_prot_udp>UDP</option>"
    echo "<option value=\"both\" $sel_prot_both>TCP+UDP</option>"
    echo '</select></td>'
    echo "<td onmouseover=i_showElem(4) onmouseout=i_showElem(0) align=\"center\"><input id=\"text_dstport$i\" name=\"text_dstport$i\" size=\"12\" maxlength=\"80\" type=\"text\" value=\"$dst_port\"></td>"
    echo "<td align=\"center\" onmouseover=i_showElem(6) onmouseout=i_showElem(0)><input type=\"checkbox\" id=\"check_ena$i\" name=\"check_ena$i\" value=\"1\" $check_ena_on></td>"
    echo "<td onmouseover=i_showElem(8) onmouseout=i_showElem(0) align=\"center\"><input type=\"button\" id=\"btn_del$i\" name=\"btn_del\" value=\"Delete\" onclick=\"doDel($i);\"></td>"
    echo '</tr>'
    let "i+=1"
  done
  [ "$i" = "0" ] && echo '<tr><td colspan="6" align="center">no rules specified</td></tr>'
fi

cat <<HTML
<tr>
<td onmouseover=i_showElem(7) onmouseout=i_showElem(0) colspan="7" align="center"><input type="button" id="btn_add" name="btn_add" value="Add" onclick="doAdd();"></td>
</tr>
</table>
<input id="rule_count" name="rule_count" type="hidden" value="$i">
<input id="del_number" name="del_number" type="hidden" value="$i">
</form>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but1 onclick='window.location.href="$HOME";' onmouseover=rahmen(1,1) onmouseout=rahmen(0,1)>Back &lt;&lt;</DIV>
<DIV id=t_but3 onclick="subm(2);" onmouseover=rahmen(1,3) onmouseout=rahmen(0,3)>Save</DIV>
<DIV id=t_but4 onclick="subm(3);" onmouseover=rahmen(1,4) onmouseout=rahmen(0,4)>Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Port filtering</h2>
<p>Block outgoing connections from LAN-IP's.</p>
</DIV>
<DIV id=i_content1>
<h2>Rule name</h2>
<p>Enter a name for this rule. Only for better understanding when you watch this configuration later.</p>
</DIV>
<DIV id=i_content2>
<h2>Src-IP</h2>
<p>Enter the source IP-address for which to block access to dst-port.<br></>
You can also enter IP-ranges.</p>
<p>
<b>Examples:</b> 
<p>192.168.2.100</p>
<p>192.168.2.0/24</p>
<p>192.168.2.1-192.168.2.10</p>
</p>
</DIV>
<DIV id=i_content3>
<h2>Protocol</h2>
<p>Select the protocol type to use.</p>
</DIV>
<DIV id=i_content4>
<h2>Dst-port</h2>
<p>Enter single port or list of destination ports which should be blocked.</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Enter valid Port-Numbers from 1-65534</p>
<p><img src="../pic_i_hinweis.gif" border="0"><br /> Example: 22,138-140</p>
</DIV>
<DIV id=i_content6>
<h2>Enabled</h2>
<p>You can <b>enable</b> or <b>disable</b> a rule without deleting it.</p>
</DIV>
<DIV id=i_content7>
<h2>Add</h2>
<p>Add a new empty port filtering rule to the list you can edit.</p>
</DIV>
<DIV id=i_content8>
<h2>Delete</h2>
<p>Delete this rule from the list.</p>
</DIV>
</DIV></BODY></HTML>
