#!/bin/sh
#
# Start/stops stunnel
#
#
case "$1" in
	start)
	        echo "Starting stunnel"
		nvram set a=1
		stunnel_start=`/bin/nvram get stunnel_start`
		killall stunnel >/dev/null 2>/dev/null
                [ "$stunnel_start" = "Failed" ] || [ "$stunnel_start" = "1" ] && nvram set b=1 && /opt/stunnel/stunnel /opt/stunnel/stunnel.conf && nvram set c=1
		nvram set d=1
		;;
	stop)
	        echo "Stopping stunnel"
		killall stunnel >/dev/null 2>/dev/null
		[ "$(nvram get fw_services_tcp)" = "" ] && nvram del fw_services_tcp && nvram commit
		;;
	restart)
		$0 stop
		usleep 900000
		$0 start
		;;
	*)
		echo "Usage: /opt/stunnel.sh {start|stop|restart}"
		exit 1
		;;
esac

exit 0