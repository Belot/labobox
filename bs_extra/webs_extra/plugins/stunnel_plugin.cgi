#!/bin/sh
#NAME: Stunnel
#DESCRIPTION: Managing stunnel

HOME="../plugin.cgi"
. /webs/cgi-bin/login.cgi
. /webs/cgi-bin/functions.sh

mustsave=0

#### Formular-Auswertung #######################################################
#SAVE 
if [ "$action" -eq 3 ]; then 
  [ "$form_stunnel_start" != "`nvram get stunnel_start`" ] && res=`nvram set stunnel_start=$form_stunnel_start` && mustsave=1 && [ "$res" != "OK" ] && err=1
  
  if [ "$form_stunnel_fw_open" = "1" ]; then
    is_fwservice_port "tcp" "443" || add_fwservice_port "tcp" "443" && mustsave=1 && fw_restart=1
  else
    is_fwservice_port "tcp" "443" && remove_fwservice_port "tcp" "443" && mustsave=1 && fw_restart=1
  fi
  [ "$err" -ne "0" ] && errmsg="$errmsg Write to NVRam failed!"'\n'

  [ "$mustsave" -eq 1 ] && nvram commit >/dev/null && /opt/stunnel/stunnel.sh restart >/dev/null && nvram set test=1 && [ "$fw_restart" = "1" ] && /etc/start_scripts/firewall.sh services_restart >/dev/null 2>/dev/null

fi
################################################################################

[ "$mustsave" -eq 1 ] && nvram commit >/dev/null

stunnel_start=`/bin/nvram get stunnel_start`
[ "$stunnel_start" = "Failed" ] && stunnel_start=1
[ "$stunnel_start" = "1" ] && stunnel_on='checked="checked"' || stunnel_off='checked="checked"'

is_fwservice_port "tcp" "443" && fw_open='checked="checked"' || fw_closed='checked="checked"'

cat <<HTML
Content-type: text/html
Connection: close

<HTML><HEAD>

<SCRIPT language="JavaScript" src="../../js_fade.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT language="JavaScript" src="../../js_menu.txt" type="text/JavaScript"></SCRIPT>
<SCRIPT type="text/JavaScript">
function stunnel_change() {
  if(document.getElementById("form_stunnel_start_off").checked==true){
    document.getElementById("form_stunnel_fw_open_off").disabled=true;
    document.getElementById("form_stunnel_fw_open_on").disabled=true;
  }
  else{
    document.getElementById("form_stunnel_fw_open_off").disabled=false;
    document.getElementById("form_stunnel_fw_open_on").disabled=false;
  }
}
</SCRIPT>
<LINK rel="stylesheet" href="../../style.css" type="text/css">
<!--[if IE]><style type="text/css">@import url(../../ie.css);</style><![endif]-->
</HEAD>
<BODY onload="stunnel_change();">
<DIV id=c_Frame>
<DIV id=c_border>
<DIV id=c_pfad>Plugins</DIV>
<DIV id=c_titel>Stunnel</DIV>
<DIV id=c_std>
<form id="xform" name="xform" method="post">
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(1) onmouseout=i_showElem(0)>
<td width="90">Stunnel</td>
<td width="20"><input type="radio" id="form_stunnel_start_off" name="form_stunnel_start" value="0" onchange="stunnel_change();" $stunnel_off></td>
<td width="40">Off</td>

<td width="20"><input type="radio" id="form_stunnel_start_on" name="form_stunnel_start" value="1" onchange="stunnel_change();" $stunnel_on></td>
<td>On</td>
</tr></table>
</DIV>
<DIV id=c_last>
<table border="0" cellpadding="0" cellspacing="2">
<tr onmouseover=i_showElem(2) onmouseout=i_showElem(0)>
<td width="90">Firewall</td>
<td width="20"><input type="radio" id="form_stunnel_fw_open_off" name="form_stunnel_fw_open" value="0" $fw_closed></td>
<td width="40">Close</td>
<td width="20"><input type="radio" id="form_stunnel_fw_open_on" name="form_stunnel_fw_open" value="1" $fw_open></td>
<td>Open</td>
<input id="action" type="hidden" name="action" value="0">
</tr></table></form>
</DIV>
<DIV id=c_leer></DIV>
<DIV id=c_foot></DIV>
</DIV>
<DIV id=c_verzoeg1></DIV>
<DIV id=c_verzoeg2></DIV>
</DIV>
<DIV id=t_Frame>
<DIV id=t_but4 onclick="subm(3);" onmouseover="rahmen(1,4);i_showElem(7)" onmouseout="rahmen(0,4);i_showElem(0)">Save &amp; Run</DIV>
</DIV>
<DIV id=i_Frame>
<DIV id=i_content>
<h2>Stunnel</h2>
<p>Start/Stop Stunnel-service and control Stunnel-Access on Public Interface</p>
</DIV>
<DIV id=i_content1>
<h2>stunnel</h2>
<p><b>Start/Stop</b><br>Start Stunnel-service Yes/No</p>
<p><img src="../../pic_i_hinweis.gif" border="0"><br /> Setting is checked on next Reboot</p>
</DIV>
<DIV id=i_content2>
<h2>Firewall</h2>
<p><b>Firewall</b><br>Open Stunnel-Port (TCP 443) in Firewall</p>
<p><img src="../../pic_i_hinweis.gif" border="0"><br /> Setting controls only access for public IP-Adresses</p>
</DIV>
<DIV id=i_content6>
<h2>stunneld</h2>
<p><b>Save</b><br>Saves changed settings to NVRAM</p>
</DIV>
<DIV id=i_content7>
<h2>stunneld</h2>
<p><b>Save&amp;Run</b><br>Save changed settings to NVRAM and restart Stunnel/Firewall</p>
<p><img src="../../pic_i_hinweis.gif" border="0"><br /> Changes are safed permanently</p>
</DIV>
</DIV></BODY></HTML>