/*
 * Copyright (C) 2008  Michael Finsterbusch <fibu@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

#define CONTENT "CONTENT_TYPE"

#define BOUND_LEN 80
#define BUFFER_LEN 1024

#define NOT_FOUND -1
#define FOUND_DIFFERENT -2


int find_boundary(char* buffer,int buflen,char* boundary)
{
  char* h=buffer;
  int bound_len,cmp=1;

  bound_len=strlen(boundary);
  while(buflen>=bound_len)
  {
    cmp=memcmp(buffer,boundary,bound_len);
    if(cmp==0)
      break;
    buflen--;
    buffer++;
  }
  if(cmp!=0)
    return NOT_FOUND;

  return buffer-h;
}

int find_name(char* buffer,int buflen,char* name)
{
  char* str,*hname;

  str=strstr(buffer,"name=\"");
  if(!str)
    return NOT_FOUND;

  if((buflen-(str-buffer))<6)
    return NOT_FOUND;
  str+=6;
  hname=str;
  str=strstr(hname,"\"");
  if(!str)
    return NOT_FOUND;

  // found the name?
  if(strncmp(name,hname,strlen(name))!=0)
    return FOUND_DIFFERENT;
  return str-buffer;
}

int find(char* filename,char* boundary,char* name,int replace)
{
  int file;
  char Buffer[2*BUFFER_LEN+1];
  char* bfirst,*bsec,*buf;
  int blen,buflen;
  int status;
  int data=0,datalen=0,filepos;
  int pos_replace=0;
  char* str;
  int i,err,pos,hpos;

  bfirst=Buffer;
  bsec=&Buffer[BUFFER_LEN];

  file=open(filename,replace?O_RDWR:O_RDONLY);
  if(file==-1)
    {
      printf("Error: open(%s) - %s\n",filename,strerror(errno));
      return 1;
    }
  
  status=1;
  blen=0;
  pos=filepos=0;
  while((err=read(file,&Buffer[blen],BUFFER_LEN))!=0)
  {
    if(err==-1)
    { 
      close(file);
      printf("Error: read(%s) - %s\n",filename,strerror(errno));
      return 1;
    }
    hpos=blen;
    filepos+=err;
    blen+=err;
    Buffer[blen]='\0';
    buf=Buffer;
    buflen=blen;
    while(1)
    {
      switch(status)
      {
        case 1: // find boundary
	case 4:
		err=find_boundary(buf,buflen,boundary);
		if(err!=NOT_FOUND)
	  	{
                  status++;
		  buf+=err;
		  pos+=err;
		  buflen-=err;
		  datalen=(pos-data)-2;
		  continue;
                }
		break;
	case 2: // find name
		err=find_name(buf,buflen,name);
		if(err==NOT_FOUND)
		  break;

		if(err==FOUND_DIFFERENT)
		  {
		    status=1;
		    buf+=1;
		    pos+=1;
		    buflen-=1;
		    continue;
		  }
                status++;
		buf+=err;
		buflen-=err;
		pos+=err;
		continue;

		break;
	case 3: // get body data
      		str=strstr(buf,"\r\n\r\n");
		if(!str)
		  break;
		status++;
	
		buflen-=(str+4)-buf;
		pos+=(str+4)-buf;
		buf=str+4;
		data=pos;
		continue;
	default:
		break;
      }
      break;
    }
    if(status==5)
      break;

    if(buflen>BUFFER_LEN)
     {
       buflen-=BUFFER_LEN;
       buf+=BUFFER_LEN;
       pos+=BUFFER_LEN;
     }
    blen=buflen;
    memmove(Buffer,buf,buflen);
  }
  if(status!=5)
  {
    close(file);
    return 1;
  }

  if(replace)
    pos_replace=0;
  lseek(file,data,SEEK_SET);
  while((err=read(file,bfirst,BUFFER_LEN))!=0)
  {
    if(err==-1)
    { 
      close(file);
      printf("Error: read(%s) - %s\n",filename,strerror(errno));
      return 1;
    }

    blen=err;
    data+=err;
    datalen-=err;
    if(datalen<0)
      blen+=datalen;

    if(replace)
      lseek(file,pos_replace,SEEK_SET);

    for(i=0; i<blen; i++)
      if(replace)
	{
         if(write(file,bfirst+i,1)==-1)
	  {
            close(file);
            printf("Error: write(%s) - %s\n",filename,strerror(errno));
            return 1;
	  }
        }
      else 
        printf("%c",*(char*)(bfirst+i));

    if(replace)
    {
      pos_replace+=blen;
      lseek(file,data,SEEK_SET);
    }

    if(datalen<0)
      break;
  }

  if(replace)
    ftruncate(file,pos_replace);

  close(file);
  return 0;
}


#define GETOPT_OK         0
#define GETOPT_NOVALUE    1
#define GETOPT_NOEQUAL    2
int getopts(int* argc,char* argv[],char** parameter,char** value)
{
 char* str;
 char* pos;

 if(*argc<1)
  return -1;
 if(!argv)
  return -2;

 (*argc)--;
 str=argv[*argc];

 *parameter=NULL;
 *value=NULL;

 pos=strchr(str,'=');
 if(!pos)
 {
  *parameter=str;
  return GETOPT_NOEQUAL;
 }
 *pos='\0';
 *parameter=str;
 *value=pos+1;
 if(strlen(*value)>0)
  return GETOPT_OK;
 return GETOPT_NOVALUE;
}

void print_help(char* name)
{
  printf("getmultipart 0.2 by Michael Finsterbusch <fibu@users.sourceforge.net>\n\n"
	 "Usage: %s file=<filename> name=<search-for> [replace]\n"
	 "Options:\n"
	 "  replace        replace the file with the content of search part\n",
	 name);
}


int main(int argc,char* argv[])
{
  char* filename=NULL;
  char* con_type;
  char boundary[BOUND_LEN];
  char* str,*str2;
  char* name=NULL;
  char* para,*val;
  int ret;
  int optargc=argc-1;
  char** optargv=argv+1;
  int replace=0;

// Parameters
 while((ret=getopts(&optargc,optargv,&para,&val))>=GETOPT_OK)
 {
  if(strcmp(para,"replace")==0)
  {
    replace=1;
    continue;
  }
  if(ret==GETOPT_NOEQUAL)
   {
    printf("Error: missing '=' in '%s' statement\n",para);
    return 1;
   }

  if(strcmp(para,"name")==0)
  {
   if(ret==GETOPT_NOVALUE)
   {
    printf("Error: no name specified\n");
    return 1;
   }
   name=strdup(val);
  }
  else if(strcmp(para,"file")==0)
  {
   if(ret==GETOPT_NOVALUE)
   {
    printf("Error: no file specified\n");
    return 1;
   }
   filename=strdup(val);
  }
 }
 
 if(!name || !filename)
   {
     print_help(argv[0]);
     return 0;
   }

  con_type=getenv(CONTENT);
  if(!con_type)
    {
      printf("ERROR: environment variable %s not set\n",CONTENT);
      return 1;
    }
  
  str=strstr(con_type,"boundary=");
  if(!str)
    {
      printf("ERROR: boundary not set\n");
      return 1;
    }

  str+=9;
  if(*str=='"')
    str++;

  str2=str;
  str2+=strlen(str);


  str2--;
  if(*str2=='"')
    str2--;

  if(str2-str>70)
    {
      printf("ERROR: boundary longer than 70 characters\n");
      return 1;
    }

  memset(boundary,0,BOUND_LEN);
  boundary[0]=boundary[1]='-';
  strncpy(boundary+2,str,str2-str+1);

#ifdef DEBUG
  printf("boundary=%s\n",boundary);
#endif

  return find(filename,boundary,name,replace);
}
