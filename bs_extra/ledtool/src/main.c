/*
 * Copyright (C) 2008 Michael Finsterbusch (fibu@users.sourceforge.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>      /* open */
#include <unistd.h>     /* exit */
#include <sys/ioctl.h>  /* ioctl */
#include <memory.h>

#include <bcmtypes.h>
#include <board_api.h>

int boardIoctl(int board_ioctl, BOARD_IOCTL_ACTION action, char *string, int strLen, int offset, char *buf)
{
    BOARD_IOCTL_PARMS IoctlParms;
    int boardFd = 0;

    boardFd = open("/dev/brcmboard", O_RDWR);
    if ( boardFd != -1 ) {
        IoctlParms.string = string;
        IoctlParms.strLen = strLen;
        IoctlParms.offset = offset;
        IoctlParms.action = action;
        IoctlParms.buf    = buf;
        ioctl(boardFd, board_ioctl, &IoctlParms);
        close(boardFd);
        boardFd = IoctlParms.result;
    } else
        printf("Unable to open device /dev/brcmboard.\n");

    return boardFd;
}


void print_help(char* name)
{
 printf("ledtool by Michael Finsterbusch <fibu@users.sourceforge.net>\n\nusage: %s <LED> <State>\n",name);
 printf("LED: 0 Power green\n"
        "     1 Power red\n"
    	"     2 ADSL\n"
    	"     3 Online\n"
    	"     4 PSTN\n"
    	"     5 Internet\n");
    	
 printf("State: 0 Off\n"
    	"       1 On\n"
	"       2 Fail (red)\n"
	"       3 BlinkOnce\n"
	"       4 SlowBlinkContinues\n"
	"       5 FastBlinkContinues\n"
	"       State 2 to 5 is only available for LED 1, 2 and 3\n");
}

void sysLedCtrl(BOARD_LED_NAME ledName, BOARD_LED_STATE ledState)
{
    boardIoctl(BOARD_IOCTL_LED_CTRL, 0, "", (int)ledName, (int)ledState, "");
}

void _sysLedCtrl(BOARD_LED_NAME ledName, BOARD_LED_STATE ledState)
{
    boardIoctl(BOARD_IOCTL_SET_GPIO, 0, "", (int)ledName, (int)ledState, "");
}

int Led(int led,int state)
{
  switch(led)
  {
   case 0: _sysLedCtrl(0,!state); break;
   case 1: sysLedCtrl(6,state); break;
   case 2: sysLedCtrl(0,state); break;
   case 3: sysLedCtrl(4,state); break;
   case 4: _sysLedCtrl(28,!state); break;
   case 5: _sysLedCtrl(32,!state); break;
   default:
  	printf("Unknown Error!\n");
	return -2;
	break;
  }
  return 0;
}

void X(int x)
{
  int i,d=0;

  switch(x)
  {
   case 0: d+=100000;
   case 1: d+=100000;
   case 2: d+=100000;
  	for(i=0; i<6; i++)
  	{
   	 Led(i,1);
   	 usleep(d);
  	}
  	for(i--; i>=0; i--)
  	{
   	 usleep(d);
   	 Led(i,0);
  	}
	break;
   case 3: d+=100000;
   case 4: d+=100000;
   case 5: d+=100000;
  	for(i=0; i<6; i++)
  	{
   	 Led(i,1);
   	 usleep(d);
  	}
  	for(i=0; i<6; i++)
  	{
   	 usleep(d);
   	 Led(i,0);
  	}
	break;
   case 6: d+=100000;
   case 7: d+=100000;
   case 8: d+=100000;
  	for(i=0; i<6; i++)
  	{
   	 Led(i,1);
   	 usleep(d);
	 Led(i,0);
  	}
	break;
   case 9: d+=100000;
   case 10: d+=100000;
   case 11: d+=100000;
  	for(i=5; i>=0; i--)
  	{
   	 Led(i,1);
   	 usleep(d);
	 Led(i,0);
  	}
	break;
  default:
	break;
  }
}

int main(int ARGC,char *ARGV[])
{
  int led,state;

  if(ARGC<3){
    print_help(ARGV[0]);
    return -1;
  }

  led=atoi(ARGV[1]);
  state=atoi(ARGV[2]);
  if(ARGV[1][0]=='X')
  {
   X(state);
   return 0;
  }

  if(led<0 || led>5)
  {
   printf("Error: undefined LED!\n");
   print_help(ARGV[0]);
   return -1;
  }
  
  if(state<0 || state>5)
  {
   printf("Error: undefined State!\n");
   print_help(ARGV[0]);
   return -1;
  }	

  return Led(led,state); 
}
