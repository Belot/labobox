/* Shared library add-on to iptables for the SIP target
 * (C) 2010 by Patrick Schmidt <coolman1982@users.sourcforge.net>
 *             Michael Finsterbusch <fibu@users.sourcforge.net>
 *
 * $Id: libipt_SIP.c$
 *
 * This program is distributed under the terms of GNU GPL
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <iptables.h>

#include <linux/netfilter_ipv4/ip_tables.h>
#include <linux/netfilter_ipv4/ipt_SIP.h>

#define IPT_SIP_METHOD_SET 1
#define IPT_SIP_REMOVE_SET 2
#define IPT_SIP_REPLACE_SET 4
#define IPT_SIP_WITH_SET 8
#define IPT_SIP_ADD_SET 16
#define IPT_SIP_TYPE_SET 32

const char line_end[]="%n";
static char buf[1024];

static void str_replace_line_end(char *str)
{
  int i;
  
  for(i=0;i<strlen(str)-1;i++)
  {
    if( memcmp(str+i,line_end,2)==0 )
      memcpy(str+i,"\r\n",2);
  }
}

static char *str_remove_line_end(char *str)
{
  int i=0,j=0;
  memset(buf,0,sizeof(buf));
  while( j<sizeof(buf) && i<strlen(str) )
  {
    if( str[i]=='\r' )
    {
      buf[j++]='\\';
      buf[j++]='r';
    }
    else if( str[i]=='\n' )
    {
      buf[j++]='\\';
      buf[j++]='n';
    }
    else
      buf[j++]=str[i];
    
    i++;
  }
  
  return buf;
}

static void init(struct ipt_entry_target *t, unsigned int *nfcache) 
{
}

static void help(void) 
{
	printf(
"SIP target v%s options\n"
"  --sip-method			Match this SIP-method <string> (e.g. INVITE)\n"
"  --sip-header			Modify the SIP-header (flag only)\n"
"  --sip-body			Modify the SIP-body (flag only)\n"
"  --sip-add			Add the SIP-entry to SIP-header or -body\n"
"  --sip-remove			Remove this SIP-entry from SIP-header or -body <string> (e.g. a=silenceSupp:Off)\n"
"  --sip-replace			Replace this SIP-entry in SIP-header or -body  <string>\n"
"  --sip-with			Combined with --sip-replace <string>\n"
, IPTABLES_VERSION);
}

static int parse(int c, char **argv, int invert, unsigned int *flags,
		const struct ipt_entry *entry,
		struct ipt_entry_target **target)
{
	int len;
	struct ipt_SIP_info *info = (struct ipt_SIP_info *) (*target)->data;

	if ( c!=2 && c!=3 && c<7 && !optarg ) 
		exit_error(PARAMETER_PROBLEM,"SIP: You must specify a value c=%d",c);

	if (check_inverse(optarg, &invert, NULL, 0))
		exit_error(PARAMETER_PROBLEM,
				"SIP: unexpected `!'");
	

	switch (c) {

		//method
		case '1':
			if( strlen(optarg)>sizeof(info->sip_method) )
			  exit_error(PARAMETER_PROBLEM, "SIP: argument to long (max %d chars)",sizeof(info->sip_method));
			
			*flags |= IPT_SIP_METHOD_SET;
			strncpy(info->sip_method,optarg,sizeof(info->sip_method));
			break;
		//sip-header
		case '2':
                        if( info->type==IPT_SIP_TYPE_HEADER )
                          exit_error(PARAMETER_PROBLEM, "SIP: multiple --sip-header?");
                        if( info->type==IPT_SIP_TYPE_BODY )
                          exit_error(PARAMETER_PROBLEM, "SIP: --sip-header and --sip-body can't be used at the same time");
                        
			info->type=IPT_SIP_TYPE_HEADER;
                        *flags |= IPT_SIP_TYPE_SET;
			break;
		//sip-body
		case '3':
                        if( info->type==IPT_SIP_TYPE_BODY )
                          exit_error(PARAMETER_PROBLEM, "SIP: multiple --sip-body?");
                        if( info->type==IPT_SIP_TYPE_HEADER )
                          exit_error(PARAMETER_PROBLEM, "SIP: --sip-header and --sip-body can't be used at the same time");
                        
			info->type=IPT_SIP_TYPE_BODY;
                        *flags |= IPT_SIP_TYPE_SET;
			break;
		//add
                case '4':
                        if( info->mode==IPT_SIP_ADD )
                          exit_error(PARAMETER_PROBLEM, "SIP: multiple --sip-add?");
                        if( info->mode==IPT_SIP_REPLACE ||  info->mode==IPT_SIP_DEL )
                          exit_error(PARAMETER_PROBLEM, "SIP: --sip-add, --sip-replace and --sip-remove cant be used at the same time");
                        if( strlen(optarg)>sizeof(info->attr_name) )
                          exit_error(PARAMETER_PROBLEM, "SIP: argument to long (max %d chars)",sizeof(info->attr_name));
			if( strlen(optarg)<3 )
			  exit_error(PARAMETER_PROBLEM, "SIP: argument to short (min 3 chars, e.g. b\\r\\n)");
			
			str_replace_line_end(optarg);
			
                        info->mode = IPT_SIP_ADD;
                        *flags |= IPT_SIP_ADD_SET;
			
			len=strlen(optarg);
			if( optarg[len-2]!='\r' && optarg[len-1]!='\n' )
			  snprintf(info->attr_name,sizeof(info->attr_name),"%s\r\n",optarg);
			else
			  strncpy(info->attr_name,optarg,sizeof(info->attr_name));
                        break;
		//remove
		case '5':
                        if( info->mode==IPT_SIP_DEL )
			  exit_error(PARAMETER_PROBLEM, "SIP: multiple --sip-remove?");
                        if( info->mode==IPT_SIP_REPLACE || info->mode==IPT_SIP_ADD )
                          exit_error(PARAMETER_PROBLEM, "SIP: --sip-add, --sip-replace and --sip-remove cant be used at the same time");
			if( strlen(optarg)>sizeof(info->attr_name) )
			  exit_error(PARAMETER_PROBLEM, "SIP: argument to long (max %d chars)",sizeof(info->attr_name));

			info->mode = IPT_SIP_DEL;
			*flags |= IPT_SIP_REMOVE_SET;
			strncpy(info->attr_name,optarg,sizeof(info->attr_name));
			break;
		//replace
		case '6':
			if( info->mode==IPT_SIP_REPLACE )
			  exit_error(PARAMETER_PROBLEM, "SIP: multiple --sip-replace?");
                        if( info->mode==IPT_SIP_DEL || info->mode==IPT_SIP_ADD )
                          exit_error(PARAMETER_PROBLEM, "SIP: --sip-add, --sip-replace and --sip-remove cant be used at the same time");
			if( strlen(optarg)>sizeof(info->attr_name) )
			  exit_error(PARAMETER_PROBLEM, "SIP: argument to long (max %d chars)",sizeof(info->attr_name));
			
			info->mode = IPT_SIP_REPLACE;
			*flags |= IPT_SIP_REPLACE_SET;
			strncpy(info->attr_name,optarg,sizeof(info->attr_name));
			break;
		//with
		case '7':
                        if( info->mode==IPT_SIP_DEL || info->mode==IPT_SIP_ADD )
			  exit_error(PARAMETER_PROBLEM, "SIP: --sip-with is incompatible with --sip-remove or --sip-add");
			if( strlen(optarg)>sizeof(info->attr_replace) )
			  exit_error(PARAMETER_PROBLEM, "SIP: argument to long (max %d chars)",sizeof(info->attr_replace));		      
			if( strlen(optarg)<3 )
			  exit_error(PARAMETER_PROBLEM, "SIP: argument to short (min 3 chars, e.g. b\\r\\n)");		      
			
			str_replace_line_end(optarg);
			
			len=strlen(optarg);
			*flags |= IPT_SIP_WITH_SET;
			if( optarg[len-2]!='\r' && optarg[len-1]!='\n' )
			  snprintf(info->attr_replace,sizeof(info->attr_replace),"%s\r\n",optarg);
			else
			  strncpy(info->attr_replace,optarg,sizeof(info->attr_replace));
			break;
		default:
			return 0;

	}

	return 1;
}

static void final_check(unsigned int flags)
{
	if (!(flags & IPT_SIP_METHOD_SET))
		exit_error(PARAMETER_PROBLEM, "SIP: You must specify a SIP-method");
        if ( !(flags & IPT_SIP_TYPE_SET) )
          exit_error(PARAMETER_PROBLEM, "SIP: You must specify a --sip-header or --sip-body");
        if (!( (flags & IPT_SIP_REPLACE_SET) || (flags & IPT_SIP_REMOVE_SET) || (flags & IPT_SIP_ADD_SET) ) )
		exit_error(PARAMETER_PROBLEM, "SIP: You must specify a --sip-remove or --sip-replace or --sip-add");
	if ( (flags & IPT_SIP_REPLACE_SET) && !(flags & IPT_SIP_WITH_SET) )
		exit_error(PARAMETER_PROBLEM, "SIP: You must specify --sip-with when --sip-replace is specified");
}

static void save(const struct ipt_ip *ip,
		const struct ipt_entry_target *target)
{
	const struct ipt_SIP_info *info = 
		(struct ipt_SIP_info *) target->data;

	printf("--sip-method %s ",info->sip_method);
        switch (info->type)
        {
                case IPT_SIP_TYPE_HEADER:
                        printf("--sip-header ");
                        break;
                case IPT_SIP_TYPE_BODY:
                        printf("--sip-body ");
                        break;
        }
	switch (info->mode) 
        {
		case IPT_SIP_DEL:
			printf("--sip-remove %s",info->attr_name);
			break;
		case IPT_SIP_REPLACE:
			printf("--sip-replace %s --sip-with %s",info->attr_name,info->attr_replace);
			break;
                case IPT_SIP_ADD:
                      printf("--sip-add %s",info->attr_name);
                      break;
        }
}

static void print(const struct ipt_ip *ip,
		const struct ipt_entry_target *target, int numeric)
{
	const struct ipt_SIP_info *info =
		(struct ipt_SIP_info *) target->data;

	printf("SIP method %s: ",info->sip_method);
	if( info->mode==IPT_SIP_DEL )
        {
          if( info->type==IPT_SIP_TYPE_HEADER )
	    printf("delete sip-header line '%s'",str_remove_line_end(info->attr_name));
          else
	    printf("delete sip-body line '%s'",str_remove_line_end(info->attr_name));
        }
	else if( info->mode==IPT_SIP_REPLACE )
        {
          if( info->type==IPT_SIP_TYPE_HEADER )
	  {
            printf("replace sip-header line '%s' with ",str_remove_line_end(info->attr_name));
	    printf("'%s'",str_remove_line_end(info->attr_replace));
	  }
          else
          {
            printf("replace sip-body line '%s' with ",str_remove_line_end(info->attr_name));
	    printf("'%s'",str_remove_line_end(info->attr_replace));
	  }
        }
        else if( info->mode==IPT_SIP_ADD )
        {
          if( info->type==IPT_SIP_TYPE_HEADER )
            printf("append '%s' at sip-header\n",str_remove_line_end(info->attr_name));
          else
            printf("append '%s' at sip-body\n",str_remove_line_end(info->attr_name));
        }
	else
	  printf("Unknown mode\n");

}

static struct option opts[] = {
	{ "sip-method", 1, 0, '1' },
        { "sip-header", 0, 0, '2' },
        { "sip-body", 0, 0, '3' },
        { "sip-add", 1, 0, '4' },
        { "sip-remove", 1, 0, '5' },
	{ "sip-replace", 1, 0, '6' },
	{ "sip-with", 1, 0, '7' },
	{ 0 }
};

static
struct iptables_target SIP = { NULL, 
	"SIP",
	IPTABLES_VERSION,
	IPT_ALIGN(sizeof(struct ipt_SIP_info)),
	IPT_ALIGN(sizeof(struct ipt_SIP_info)),
	&help,
	&init,
	&parse,
	&final_check,
	&print,
	&save,
	opts 
};

void _init(void)
{
	register_target(&SIP);
}
