/* This is a generated file, don't edit */
const char applet_names[] ALIGN1 = "" 

"[" "\0"
"[[" "\0"
"arp" "\0"
"arping" "\0"
"ash" "\0"
"awk" "\0"
"cat" "\0"
"chmod" "\0"
"chpasswd" "\0"
"chroot" "\0"
"clear" "\0"
"cp" "\0"
"crond" "\0"
"cttyhack" "\0"
"cut" "\0"
"date" "\0"
"dd" "\0"
"df" "\0"
"dirname" "\0"
"dmesg" "\0"
"du" "\0"
"echo" "\0"
"env" "\0"
"expr" "\0"
"false" "\0"
"fgrep" "\0"
"free" "\0"
"ftpget" "\0"
"ftpput" "\0"
"getopt" "\0"
"getty" "\0"
"grep" "\0"
"head" "\0"
"hexdump" "\0"
"ifconfig" "\0"
"inetd" "\0"
"init" "\0"
"insmod" "\0"
"ip" "\0"
"kill" "\0"
"killall" "\0"
"klogd" "\0"
"length" "\0"
"less" "\0"
"linuxrc" "\0"
"ln" "\0"
"login" "\0"
"logread" "\0"
"ls" "\0"
"lsmod" "\0"
"mkdir" "\0"
"mkfifo" "\0"
"mknod" "\0"
"modprobe" "\0"
"mount" "\0"
"mv" "\0"
"nc" "\0"
"netstat" "\0"
"nice" "\0"
"nohup" "\0"
"nslookup" "\0"
"passwd" "\0"
"pidof" "\0"
"ping" "\0"
"ping6" "\0"
"pivot_root" "\0"
"printenv" "\0"
"printf" "\0"
"ps" "\0"
"pscan" "\0"
"pwd" "\0"
"reset" "\0"
"resize" "\0"
"rm" "\0"
"rmmod" "\0"
"route" "\0"
"sed" "\0"
"setconsole" "\0"
"sh" "\0"
"sleep" "\0"
"sort" "\0"
"split" "\0"
"strings" "\0"
"stty" "\0"
"syslogd" "\0"
"tail" "\0"
"tee" "\0"
"telnet" "\0"
"telnetd" "\0"
"test" "\0"
"time" "\0"
"top" "\0"
"touch" "\0"
"tr" "\0"
"true" "\0"
"tty" "\0"
"udhcpc" "\0"
"umount" "\0"
"uname" "\0"
"uptime" "\0"
"usleep" "\0"
"vi" "\0"
"wc" "\0"
"wget" "\0"
;
int (*const applet_main[])(int argc, char **argv) = {
test_main,
test_main,
arp_main,
arping_main,
ash_main,
awk_main,
cat_main,
chmod_main,
chpasswd_main,
chroot_main,
clear_main,
cp_main,
crond_main,
cttyhack_main,
cut_main,
date_main,
dd_main,
df_main,
dirname_main,
dmesg_main,
du_main,
echo_main,
env_main,
expr_main,
false_main,
grep_main,
free_main,
ftpgetput_main,
ftpgetput_main,
getopt_main,
getty_main,
grep_main,
head_main,
hexdump_main,
ifconfig_main,
inetd_main,
init_main,
insmod_main,
ip_main,
kill_main,
kill_main,
klogd_main,
length_main,
less_main,
init_main,
ln_main,
login_main,
logread_main,
ls_main,
lsmod_main,
mkdir_main,
mkfifo_main,
mknod_main,
modprobe_main,
mount_main,
mv_main,
nc_main,
netstat_main,
nice_main,
nohup_main,
nslookup_main,
passwd_main,
pidof_main,
ping_main,
ping6_main,
pivot_root_main,
printenv_main,
printf_main,
ps_main,
pscan_main,
pwd_main,
reset_main,
resize_main,
rm_main,
rmmod_main,
route_main,
sed_main,
setconsole_main,
ash_main,
sleep_main,
sort_main,
split_main,
strings_main,
stty_main,
syslogd_main,
tail_main,
tee_main,
telnet_main,
telnetd_main,
test_main,
time_main,
top_main,
touch_main,
tr_main,
true_main,
tty_main,
udhcpc_main,
umount_main,
uname_main,
uptime_main,
usleep_main,
vi_main,
wc_main,
wget_main,
};
const uint16_t applet_nameofs[] ALIGN2 = {
0x0000,
0x0002,
0x0005,
0x0009,
0x0010,
0x0014,
0x0018,
0x001c,
0x0022,
0x002b,
0x0032,
0x0038,
0x003b,
0x0041,
0x004a,
0x004e,
0x0053,
0x0056,
0x0059,
0x0061,
0x0067,
0x006a,
0x006f,
0x0073,
0x0078,
0x007e,
0x0084,
0x0089,
0x0090,
0x0097,
0x009e,
0x00a4,
0x00a9,
0x00ae,
0x00b6,
0x00bf,
0x00c5,
0x00ca,
0x00d1,
0x00d4,
0x00d9,
0x00e1,
0x00e7,
0x00ee,
0x00f3,
0x00fb,
0x80fe,
0x0104,
0x010c,
0x010f,
0x0115,
0x011b,
0x0122,
0x0128,
0x0131,
0x0137,
0x013a,
0x013d,
0x0145,
0x014a,
0x0150,
0x8159,
0x0160,
0x4166,
0x016b,
0x0171,
0x017c,
0x0185,
0x018c,
0x018f,
0x0195,
0x0199,
0x019f,
0x01a6,
0x01a9,
0x01af,
0x01b5,
0x01b9,
0x01c4,
0x01c7,
0x01cd,
0x01d2,
0x01d8,
0x01e0,
0x01e5,
0x01ed,
0x01f2,
0x01f6,
0x01fd,
0x0205,
0x020a,
0x020f,
0x0213,
0x0219,
0x021c,
0x0221,
0x0225,
0x022c,
0x0233,
0x0239,
0x0240,
0x0247,
0x024a,
0x024d,
};
