#!/bin/sh


#delete tmp files from editors like kate or kdevelop
find . -name *~ -type f -exec rm -f \{\} \;

#delete images
rm -f images/*

#cleanup source tree
make clean
